package com.ds.ins.services.validation.rules.towing

// For each vehicle/vehicle group: 
// if Vehicle Description is DP and Endorsement with endorsementCode=GDPC does not exist

rule "VR-TOW-OAP1-03" extends "VR-TOW-BASE"
	when
		$pt:    PolicyTransaction()
		$sp:    SubPolicy(subPolicyTypeCd == "AUTO" && (isActive() || isPending())) from $pt.subPolicies
	    // all DP vehicles
	    $vehs:   ArrayList() from collect (SpecialtyVehicleRisk(riskType == "AU" && vehicleDescription == "DP" && (isActive() || isPending())) from $sp.subPolicyRisks)
	    eval($vehs.size() > 0)
	    // DP vehicles have GDPC
		$dps:   Set() from accumulate (
			        $a: SpecialtyVehicleRisk($ends: coverageDetail.endorsements) from $vehs
			        and
   		            Endorsement(endorsementCd == "GDPC" && (isActive() || isPending())) from $ends;
			        collectSet( $a )
			     )
	    eval($vehs.size() != $dps.size())
	    // DP vehicles have no GDPC
	    $rs:    ArrayList() from collect (SpecialtyVehicleRisk(this not memberOf $dps) from $vehs)
	then
		ruleResult.addRuleEntities("VR-TOW-OAP1-03", $rs);
end