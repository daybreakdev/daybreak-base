package com.echelon.services.validation;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.policy.PolicyMessage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.services.validation.DSRuleEngineFactory;
import com.ds.ins.services.validation.DSValidationService;
import com.echelon.utils.SpecialtyAutoConstants;

public class ValidationService extends DSValidationService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4089074837864754286L;

	public ValidationService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSRuleEngineFactory getRuleEngineFactory() {
		// TODO Auto-generated method stub
		return RuleEngineFactory.getInstance();
	}

	@Override
	protected List<String> getRuleVersionStatuses() {
		// TODO Auto-generated method stub
		return SpecialtyAutoConstants.VALIDATIONRULLE_VERSIONSTATUSES;
	}

	@Override
	public List<PolicyMessage> runRuleGroup(PolicyTransaction<?> policyTransaction, String groupId) throws Exception {
		List<PolicyMessage> ruleMessages = new ArrayList<PolicyMessage>();
		
		ruleMessages = RuleEngineFactory.getInstance().newRuleEngine(
									policyTransaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
									policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
									groupId)
								.runRules(policyTransaction);
		
		return ruleMessages;
	}

}
