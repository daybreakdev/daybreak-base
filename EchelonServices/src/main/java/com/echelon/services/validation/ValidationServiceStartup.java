package com.echelon.services.validation;

import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.services.validation.DSRuleEngineFactory;
import com.ds.ins.services.validation.DSValidationServiceStartup;
import com.echelon.prodconf.ProductConfigFactory;

public class ValidationServiceStartup extends DSValidationServiceStartup {

	public ValidationServiceStartup() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSRuleEngineFactory<?> getRuleEngineFactory() {
		// TODO Auto-generated method stub
		return RuleEngineFactory.getInstance();
	}

	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		// TODO Auto-generated method stub
		return ProductConfigFactory.getInstance();
	}

}
