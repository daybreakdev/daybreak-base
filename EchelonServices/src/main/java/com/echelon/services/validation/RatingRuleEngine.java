package com.echelon.services.validation;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.io.ResourceType;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.io.ResourceFactory;

import com.ds.ins.domain.policy.PolicyMessage;
import com.ds.ins.services.validation.DSRuleEngine;
import com.echelon.prodconf.ConfigConstants;

public class RatingRuleEngine extends DSRuleEngine {

	public RatingRuleEngine(String productCd) {
		super(productCd);
	}

	@Override
	public List<PolicyMessage> runRules(Object object) throws Exception {
		if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(productCd)) {
			return super.runRules(object);
		}
		
		return new ArrayList<PolicyMessage>();		
	}

	@Override
	protected void includeRuleFiles(KnowledgeBuilder builder) {
		super.includeRuleFiles(builder);
		
		if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(productCd)) {
			builder.add(ResourceFactory.newClassPathResource("rules/towingnonfleet/VR-NF-BASE.drl"), ResourceType.DRL);
			builder.add(ResourceFactory.newClassPathResource("rules/towingnonfleet/VR-NF-COV-20.drl"), ResourceType.DRL);
			builder.add(ResourceFactory.newClassPathResource("rules/towingnonfleet/VR-NF-DRV-02.drl"), ResourceType.DRL);
			builder.add(ResourceFactory.newClassPathResource("rules/towingnonfleet/VR-NF-DRV-03.drl"), ResourceType.DRL);
		}
	}

}
