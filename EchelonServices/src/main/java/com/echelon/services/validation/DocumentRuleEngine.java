package com.echelon.services.validation;

import org.kie.api.io.ResourceType;
import org.kie.internal.builder.KnowledgeBuilder;
import org.kie.internal.io.ResourceFactory;

import com.ds.ins.services.validation.DSRuleEngine;

public class DocumentRuleEngine extends DSRuleEngine {

	public DocumentRuleEngine(String productCd) {
		super(productCd);
	}

	@Override
	protected void includeRuleFiles(KnowledgeBuilder builder) {
		super.includeRuleFiles(builder);
		
		builder.add(ResourceFactory.newClassPathResource("rules/towing/VR-TOW-BASE.drl"), ResourceType.DRL);
		builder.add(ResourceFactory.newClassPathResource("rules/towing/VR-TOW-OAP1-06.drl"), ResourceType.DRL);
	}

}
