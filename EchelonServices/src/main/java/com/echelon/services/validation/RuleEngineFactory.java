package com.echelon.services.validation;

import com.ds.ins.services.validation.DSRuleEngine;
import com.ds.ins.services.validation.DSRuleEngineFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class RuleEngineFactory extends DSRuleEngineFactory<DSRuleEngine> {

	private static RuleEngineFactory instance = new RuleEngineFactory();
	
	public static RuleEngineFactory getInstance() {
		return instance;
	}
	
	protected RuleEngineFactory() {
		//empty constructor
	}

	@Override
	protected DSRuleEngine createRuleEngine(String provCd, String productCd, String groupId) {
		if (groupId != null) {
			switch (groupId) {
			case SpecialtyAutoConstants.RULE_GROUP_DOCUMENT:
				return new DocumentRuleEngine(productCd);

			case SpecialtyAutoConstants.RULE_GROUP_RATING:
				return new RatingRuleEngine(productCd);

			default:
				break;
			}
		}
		
		return super.createRuleEngine(provCd, productCd, groupId);
	}
}
