package com.echelon.services;

import java.util.logging.Logger;

import javax.servlet.annotation.WebListener;

import com.ds.ins.services.DSServicesStartupServlet;
import com.ds.ins.services.validation.DSValidationServiceStartup;
//import com.ds.ins.services.premium.RoundingStrategyFactory;
import com.echelon.services.validation.ValidationServiceStartup;

@WebListener
public class ServicesStartupServlet extends DSServicesStartupServlet {
	private static final Logger	log = Logger.getLogger(ServicesStartupServlet.class.getName());

	public ServicesStartupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSValidationServiceStartup createValidationServiceStartup() {
		// TODO Auto-generated method stub
		return new ValidationServiceStartup();
	}
}
