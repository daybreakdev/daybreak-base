package com.echelon.services.conversion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.prodconf.baseclasses.DSConfigConstants;
import com.ds.ins.system.UserProfile;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.utils.SpecialtyAutoConstants;

public class SIMRatingConversion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3537120745611469396L;
	private static final Logger log	= Logger.getLogger(SIMRatingConversion.class.getName());

	private static final int    UWADJUSTMENT_DECIMALS = 20;
	private static final String STATUS_CONVERTED   	  = "CONVERTED";

	private class PremiumData {
		private String  coverageType;
		private Premium premium;
		
		public PremiumData(String coverageType, Premium premium) {
			super();
			this.coverageType = coverageType;
			this.premium = premium;
		}

		public String getCoverageType() {
			return coverageType;
		}

		public Premium getPremium() {
			return premium;
		}
	}
	
	public SIMRatingConversion() {
		super();
		// TODO Auto-generated constructor stub
	}

	private SpecialityAutoSubPolicy<Risk> getAutoSubPolicy(PolicyTransaction<Risk> policyTransaction) {
		SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = (SpecialityAutoSubPolicy<Risk>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		return spAutoSubPolicy;
	}
	
	private List<PremiumData> getPremiums(SpecialtyVehicleRisk vehicle) {
		final List<PremiumData> results = new ArrayList<PremiumData>();
		
		if (vehicle.getCoverageDetail() != null) {
			vehicle.getCoverageDetail().getCoverages().stream()
					.forEach(o -> {
						if (o.getCoveragePremium() != null) {
							results.add(new PremiumData(DSConfigConstants.COVERAGETYPE_COVERAGE, o.getCoveragePremium()));
						}
					});
			
			vehicle.getCoverageDetail().getEndorsements().stream()
					.forEach(o -> {
						if (o.getEndorsementPremium() != null) {
							results.add(new PremiumData(DSConfigConstants.COVERAGETYPE_ENDORSEMENT, o.getEndorsementPremium()));
						}
					});
		}
		
		return results;
	}

	//update CoverageDetails set SystemStatus = null where CoverageDetails.CoverageDetailsPK = 20856;
	//update Coverage set isPremiumOveride = 0, PriorWrittenPremium = 0 where Coverage.CoverageDetailsPK = 20856;

	// Step 1: save or restore original premium
	// Step 2: calculate full premium for groups
	// Step 3: rate with UW1
	// Step 4: rate with no UW
	// Step 5: analyze result
	// Step 6: rate if Adj UW is applied
	// Step 7: save 
	public PolicyTransaction<Risk> process(Long pcyTxnPK, IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		//GenericDAO genericDAO = new GenericDAO();
		//genericDAO.beginTransaction();
		//PolicyTransaction<Risk> policyTransaction = (PolicyTransaction<Risk>) genericDAO.findById(PolicyTransaction.class, pcyTxnPK);
		
		PolicyTransaction<Risk> policyTransaction = (PolicyTransaction<Risk>) (new PolicyTransactionDAO()).findById(pcyTxnPK);
		if (preRating(policyTransaction)) {
			policyTransaction = rateWithUW1(policyTransaction, policyProcessing, userProfile);
			
			policyTransaction = rate(policyTransaction, policyProcessing, userProfile);
			
			policyTransaction = analyzeRateResult(policyTransaction, policyProcessing, userProfile);
			
			policyTransaction = save(policyTransaction, policyProcessing, userProfile);
			//genericDAO.merge(policyTransaction);
		}
		
		//genericDAO.endTransaction();
		return policyTransaction;
	}
	
	private boolean preRating(PolicyTransaction<Risk> policyTransaction) {
		SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = getAutoSubPolicy(policyTransaction);
		if (spAutoSubPolicy != null && spAutoSubPolicy.getSubPolicyRisks() != null && !spAutoSubPolicy.getSubPolicyRisks().isEmpty()) {
			for(Risk risk : spAutoSubPolicy.getSubPolicyRisks()) {
				SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
				
				int units = 0;
				if (vehicle.getNumberOfUnits() != null && vehicle.getNumberOfUnits() > 1) {
					units = vehicle.getNumberOfUnits();
				}

				List<PremiumData> premiums = getPremiums(vehicle);
				
				backupOrRestore(vehicle, premiums);
				
				if (premiums != null && !premiums.isEmpty()) {
					calculateFullPremium(units, premiums);
				}
			}
			
			return true;
		}
		
		return false;
	}
	
	// save or restore original premium
	// SystemStatus != STATUS_SAVED; need to save the original premium
	// Otherwise restore the original premium
	private void backupOrRestore(SpecialtyVehicleRisk vehicle, List<PremiumData> premiums) {
		if (vehicle.getCoverageDetail() != null &&
			!STATUS_CONVERTED.equalsIgnoreCase(vehicle.getCoverageDetail().getSystemStatus())) {
			premiums.stream().forEach(o -> o.getPremium().setPriorWrittenPremium(o.getPremium().getOriginalPremium()));
			vehicle.getCoverageDetail().setSystemStatus(STATUS_CONVERTED);
		}
		else {
			premiums.stream().forEach(o -> o.getPremium().setOriginalPremium(o.getPremium().getPriorWrittenPremium()));
		}
		
		premiums.stream().forEach(o -> {
				o.getPremium().setPremiumModifier1(null);
				o.getPremium().setPriorNetPremiumChange(null);
				o.getPremium().setTargetPremium(null);
			});
	}
	
	// calculate full premium for groups
	private void calculateFullPremium(int units, List<PremiumData> premiums) {
		if (units > 1) {
			premiums.stream().forEach(o -> {
				if (o.getPremium().getOriginalPremium() != null && o.getPremium().getOriginalPremium().doubleValue() != 0) {
					double fullPrem = ((new BigDecimal(o.getPremium().getOriginalPremium())).multiply(new BigDecimal(units)))
										.setScale(0, BigDecimal.ROUND_HALF_UP)
										.doubleValue();
					// Keep annual premium
					o.getPremium().setPremiumOverride(true);
					o.getPremium().setAnnualPremium(fullPrem);
					o.getPremium().setPriorNetPremiumChange(fullPrem);
				}
			});
		}
	}
	
	// rate the policy with UW=1 and save the rated premium
	private PolicyTransaction<Risk> rateWithUW1(PolicyTransaction<Risk> policyTransaction,
												IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = getAutoSubPolicy(policyTransaction);
		for(Risk risk : spAutoSubPolicy.getSubPolicyRisks()) {
			SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
			
			List<PremiumData> premiums = getPremiums(vehicle);
			if (premiums != null && !premiums.isEmpty()) {
				premiums.stream().forEach(o -> o.getPremium().setPremiumModifier1(1d));
			}
		}
		
		policyTransaction = rate(policyTransaction, policyProcessing, userProfile);
		
		for(Risk risk : spAutoSubPolicy.getSubPolicyRisks()) {
			SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
			
			List<PremiumData> premiums = getPremiums(vehicle);
			if (premiums != null && !premiums.isEmpty()) {
				premiums.stream().forEach(o -> {
					o.getPremium().setTargetPremium(o.getPremium().getOriginalPremium());
					o.getPremium().setPremiumModifier1(null);
				});
			}
		}
		
		return policyTransaction;
	}

	// rate the policy - not saving
	private PolicyTransaction<Risk> rate(PolicyTransaction<Risk> policyTransaction, IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		policyTransaction = policyProcessing.ratePolicyTransaction(policyTransaction, userProfile);
		return policyTransaction;
	}
	
	// analyze results
	// Try to remove premium override flag; if original = annual or applied adj uw
	private PolicyTransaction<Risk> analyzeRateResult(PolicyTransaction<Risk> policyTransaction, 
														IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = getAutoSubPolicy(policyTransaction);
		for(int ii=0; ii<1; ii++) {
			boolean applyAdjUW = false;
			for(Risk risk : spAutoSubPolicy.getSubPolicyRisks()) {
				SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
				
				List<PremiumData> premiums = getPremiums(vehicle);
				if (premiums != null && !premiums.isEmpty()) {
					if (ii == 0) {
						if (analyzePremiumResult(premiums, vehicle.getNumberOfUnits())) {
							applyAdjUW = true;
						}
					}
					/*
					else {
						if (adjustUWAdjment(premiums)) {
							applyAdjUW = true;
						}
					}
					*/
				}
			}
			
			if (applyAdjUW) {
				policyTransaction = rate(policyTransaction, policyProcessing, userProfile);
			}
			else {
				break;
			}
		}
		
		return policyTransaction;
	}
	
	// UW is for coverage only
	private boolean analyzePremiumResult(List<PremiumData> premiums, Integer numberOfUnits) {
		boolean[] applyAdjUW = new boolean[] {false};
		premiums.stream().forEach(o -> {
			if (DSConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(o.coverageType) &&
				BooleanUtils.isTrue(o.getPremium().isPremiumOverride()) && o.getPremium().getOriginalPremium() != null && o.getPremium().getAnnualPremium() != null) {
				BigDecimal orig   = new BigDecimal(o.getPremium().getOriginalPremium()).setScale(0, BigDecimal.ROUND_HALF_UP);
				BigDecimal annual = new BigDecimal(o.getPremium().getAnnualPremium()).setScale(0, BigDecimal.ROUND_HALF_UP);
				if (orig.compareTo(annual) == 0) {
					o.getPremium().setPremiumOverride(false);
				}
				else if (orig.doubleValue() != 0) {
					BigDecimal origUnit   = orig.divide(new BigDecimal(numberOfUnits), 0, BigDecimal.ROUND_HALF_UP);
					BigDecimal annualUnit = annual.divide(new BigDecimal(numberOfUnits), 0, BigDecimal.ROUND_HALF_UP);
					o.getPremium().setPremiumModifier1(annualUnit.divide(origUnit, UWADJUSTMENT_DECIMALS, BigDecimal.ROUND_HALF_UP).doubleValue());
					o.getPremium().setPremiumOverride(false);
					applyAdjUW[0] = true;
				}
			}
		});
		
		return applyAdjUW[0];
	}
	
	/*
	private boolean adjustUWAdjment(List<Premium> premiums) {
		boolean applyAdjUW = false;

		BigDecimal factor = new BigDecimal(UWADJUSTMENT_FACTOR).setScale(UWADJUSTMENT_DECIMALS, BigDecimal.ROUND_HALF_UP);
		for(Premium prem: premiums) {
			if (prem.getPremiumModifier1() != null && prem.getPremiumModifier1() != 0 &&
				prem.getAnnualPremium() != null && 
				prem.getPriorNetPremiumChange() != null) {
				BigDecimal annual    = new BigDecimal(prem.getAnnualPremium()).setScale(0, BigDecimal.ROUND_HALF_UP);
				BigDecimal uwAdj     = new BigDecimal(prem.getPremiumModifier1()).setScale(UWADJUSTMENT_DECIMALS, BigDecimal.ROUND_HALF_UP);
				BigDecimal userPrem  = new BigDecimal(prem.getPriorNetPremiumChange()).setScale(0, BigDecimal.ROUND_HALF_UP);
				
				// Greater ; need to decrease UW Adjustment
				if (annual.compareTo(userPrem) == 1) {
					prem.setPremiumModifier1(uwAdj.subtract(factor).doubleValue());
					applyAdjUW = true;
				}
				// Less ; need to increase UW Adjustment
				else if (annual.compareTo(userPrem) == -1) {
					prem.setPremiumModifier1(uwAdj.add(factor).doubleValue());
					applyAdjUW = true;
				}
			}			
		}
		
		return applyAdjUW;
	}
	*/
	
	// save 
	private PolicyTransaction<Risk> save(PolicyTransaction<Risk> policyTransaction, IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		policyTransaction = policyProcessing.updatePolicyTransaction(policyTransaction, userProfile);
		return policyTransaction;
	}
}
