package com.echelon.services.conversion;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.utils.SpecialtyAutoConstants;

public class MailingAddressLocationFixes {
	private static ISpecialtyAutoPolicyProcessing policyProcessing;
	private static PolicyTransactionDAO ptDAO;
	
	public class AddressLocationData {
		private boolean 				isNB;
		private Integer					transactionSeq;
		private PolicyTransaction<Risk> policyTransaction;
		private PolicyLocation			locationA;
		private PolicyLocation			locationMailing;
		
		public AddressLocationData(boolean isNB, PolicyTransaction<Risk> policyTransaction, PolicyLocation locationA) {
			this(policyTransaction, locationA);
			this.isNB = isNB;
		}

		public AddressLocationData(PolicyTransaction<Risk> policyTransaction,
				PolicyLocation locationA) {
			super();
			this.policyTransaction 	= policyTransaction;
			this.locationA 			= locationA;
			this.transactionSeq 	= this.policyTransaction.getTransactionSeq();
		}

		public boolean isNB() {
			return isNB;
		}

		public void setNB(boolean isNB) {
			this.isNB = isNB;
		}

		public Integer getTransactionSeq() {
			return transactionSeq;
		}

		public void setTransactionSeq(Integer transactionSeq) {
			this.transactionSeq = transactionSeq;
		}

		public PolicyTransaction<Risk> getPolicyTransaction() {
			return policyTransaction;
		}

		public void setPolicyTransaction(PolicyTransaction<Risk> policyTransaction) {
			this.policyTransaction = policyTransaction;
		}

		public PolicyLocation getLocationA() {
			return locationA;
		}

		public void setLocationA(PolicyLocation locationA) {
			this.locationA = locationA;
		}

		public PolicyLocation getLocationMailing() {
			return locationMailing;
		}

		public void setLocationMailing(PolicyLocation locationMailing) {
			this.locationMailing = locationMailing;
		}
		
	}
	
	public void setPolicyProcessing(ISpecialtyAutoPolicyProcessing policyProcessing) {
		this.policyProcessing = policyProcessing;
	}

	
	// Since locationA may has coverages e.g. SP64
	// Keep the locationA and setCustomerMainAddressPK to null
	// clone InsuredAddress
	// clone locationA to Mailing Address
	// set setCustomerMainAddressPK of Location Mailing Address to new InsuredAddressPK
	public void main(String[] args) throws InsurancePolicyException {
		List<Long> pcyTxnPkList = new ArrayList<Long>();
		if (args != null && args.length > 0) {
			for(String arg : args) {
				try {
					Long pk = Long.valueOf(arg);
					if (pk != null) {
						pcyTxnPkList.add(pk);
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		
		HashMap<Long, Boolean> PolicyMap = new HashMap<Long, Boolean>();
		
		ptDAO = new PolicyTransactionDAO();
		// a list of NB transactions
		if (pcyTxnPkList == null || pcyTxnPkList.isEmpty()) {
			pcyTxnPkList = ptDAO.getPolicyTransactionPkListNotIgnored();
		}
		
		for (Long pcyTxnPk : pcyTxnPkList) {
			PolicyTransaction<Risk> pcyTxnNB = (PolicyTransaction<Risk>) ptDAO.findById(pcyTxnPk);
			if (PolicyMap.get(pcyTxnNB.getPolicyVersion().getInsurancePolicy().getInsurancePolicyPK()) != null) {
				continue;
			}
			
			pcyTxnNB = (PolicyTransaction<Risk>) policyProcessing.loadPolicyTransactionFull(pcyTxnNB, false);
			if (isOkToProcesse(pcyTxnNB)) {
				List<AddressLocationData> allTxnData = new ArrayList<AddressLocationData>();
				allTxnData.add(new AddressLocationData(true, pcyTxnNB, findLocatoinA(pcyTxnNB)));
				
				List<PolicyVersion> pvList = null;
				if (StringUtils.isNotBlank(pcyTxnNB.getPolicyVersion().getInsurancePolicy().getBasePolicyNum())) {
					pvList = policyProcessing.findAllPolicyVersionsByPolicyNumber(pcyTxnNB.getPolicyVersion().getInsurancePolicy().getBasePolicyNum());
				}
				else {
					pvList = policyProcessing.findAllPolicyVersionByQuoteNumberAndTerm(pcyTxnNB.getPolicyVersion().getInsurancePolicy().getQuoteNum(),1);
				}
				
				if (pvList != null && !pvList.isEmpty()) {
					Set<Long> otherTxnPkList = new HashSet<Long>();
					for(PolicyVersion pv : pvList) {
						for(PolicyTransaction<?> pt : pv.getPolicyTransactions()) {
							if (!pt.equals(pcyTxnNB)) {
								otherTxnPkList.add(pt.getPolicyTransactionPK());
							}
						}
					}
					
					for(Long othPcyTxnPk : otherTxnPkList) {
						PolicyTransaction<Risk> pcyTxnOTH = (PolicyTransaction<Risk>) ptDAO.findById(othPcyTxnPk);
						pcyTxnOTH = (PolicyTransaction<Risk>) policyProcessing.loadPolicyTransactionFull(pcyTxnOTH, false);
						PolicyLocation locationA = findLocatoinA(pcyTxnOTH);
						if (locationA != null) {
							allTxnData.add(new AddressLocationData(pcyTxnOTH, locationA));
						}
					}
				}

				
				allTxnData.sort(Comparator.comparing(AddressLocationData::getTransactionSeq));
				for(AddressLocationData locationData : allTxnData) {
					if (locationData.isNB) {
						processNB(locationData);
					}
					else {
						processOtherTxns(locationData);
					}
				}				
				setLocationMailingOriginatingPK(allTxnData);
				finalizeProcess(allTxnData);

				PolicyMap.put(pcyTxnNB.getPolicyVersion().getInsurancePolicy().getInsurancePolicyPK(), Boolean.TRUE);
			}
		}
	}
	
	private void processNB(AddressLocationData locationData) throws InsurancePolicyException {
		/* not necessary; PolicyLocationA.address != InsuredAddress
		try {
			pcyTxn.getPolicyVersion().getInsurancePolicy().setInsuredAddress(
						(Address) pcyTxn.getPolicyVersion().getInsurancePolicy().getInsuredAddress().clone());
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			throw new InsurancePolicyException("Failed to clone Insured Address",e);
		}
		*/
		
		PolicyTransaction<Risk> pcyTxn = locationData.getPolicyTransaction();
		PolicyLocation locationA   	   = locationData.getLocationA();
		
		PolicyLocation newLocation = cloneLocation(locationA);
		pcyTxn.addPolicyRisk(newLocation);

		locationA.setCustomerMainAddressPK(null);

		pcyTxn = (PolicyTransaction<Risk>) ptDAO.merge(pcyTxn);
		
		newLocation = findLocatoinMAILING(pcyTxn);
		newLocation.setCustomerMainAddressPK(pcyTxn.getPolicyVersion().getInsurancePolicy().getInsuredAddress().getAddressPK());

		pcyTxn = (PolicyTransaction<Risk>) ptDAO.merge(pcyTxn);
		
		locationA   = findLocatoinA(pcyTxn);
		newLocation = findLocatoinMAILING(pcyTxn);

		locationData.setLocationA(locationA);
		locationData.setLocationMailing(newLocation);
		locationData.setPolicyTransaction(pcyTxn);
	}
	
	// replace locationA with new mailing location for other transactions
	private void processOtherTxns(AddressLocationData locationData) throws InsurancePolicyException {
		PolicyTransaction<Risk> pcyTxn = locationData.getPolicyTransaction();
		PolicyLocation locationA 	   = locationData.getLocationA();
		
		PolicyLocation newLocation = cloneLocation(locationA);
		newLocation.setCustomerMainAddressPK(pcyTxn.getPolicyVersion().getInsurancePolicy().getInsuredAddress().getAddressPK());
		pcyTxn.addPolicyRisk(newLocation);
		
		pcyTxn = (PolicyTransaction<Risk>) ptDAO.merge(pcyTxn);

		locationA   = findLocatoinA(pcyTxn);
		newLocation = findLocatoinMAILING(pcyTxn);
		
		locationData.setLocationA(locationA);
		locationData.setLocationMailing(newLocation);
		locationData.setPolicyTransaction(pcyTxn);
	}
	
	private void setLocationMailingOriginatingPK(List<AddressLocationData> allTxnData) {
		for(AddressLocationData locationData : allTxnData) {
			if (!locationData.isNB) {
				PolicyLocation origLcationMailing = findOriginalLocationMAILING(locationData.locationA, allTxnData);
				if (origLcationMailing != null) {
					locationData.getLocationMailing().setOriginatingPK(origLcationMailing.getPolicyRiskPK());
				}
			}
		}
	}
	
	private void finalizeProcess(List<AddressLocationData> allTxnData) {
		for(AddressLocationData locationData : allTxnData) {
			PolicyTransaction<Risk> pcyTxn = locationData.getPolicyTransaction();
			PolicyLocation locationA 	   = locationData.getLocationA();
			
			if (locationA != null && locationA.getCustomerMainAddressPK() != null) {
				locationA.setCustomerMainAddressPK(null);
				
				pcyTxn = (PolicyTransaction<Risk>) ptDAO.merge(pcyTxn);
				
				locationA = findLocatoinA(pcyTxn);
				locationData.setLocationA(locationA);
				locationData.setPolicyTransaction(pcyTxn);
			}
		}
	}
	
	private boolean isOkToProcesse(PolicyTransaction<Risk> pcyTxn) {
		if (pcyTxn.getPolicyVersion().getInsurancePolicy().getInsuredAddress() != null) {
			PolicyLocation locationA = findLocatoinA(pcyTxn);
			if (locationA != null && locationA.getCustomerMainAddressPK() != null) {
				return true;
			}
		}
		
		return false;
	}
	
	private PolicyLocation findLocatoinMAILING(PolicyTransaction<Risk> pcyTxn) {
		List<PolicyLocation> locations = pcyTxn.getPolicyRisks()
										.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
														.collect(Collectors.toList())
										.stream().flatMap(o->Stream.of((PolicyLocation)o))
														 .collect(Collectors.toList());
		
		PolicyLocation location = locations.stream().filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId())).findFirst().orElse(null);
		
		return location;
	}
	
	private PolicyLocation findLocatoinA(PolicyTransaction<Risk> pcyTxn) {
		List<PolicyLocation> locations = pcyTxn.getPolicyRisks()
										.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
														.collect(Collectors.toList())
										.stream().flatMap(o->Stream.of((PolicyLocation)o))
														 .collect(Collectors.toList());
		
		PolicyLocation location = locations.stream().filter(o -> "A".equalsIgnoreCase(o.getLocationId()) &&
																 o.getCustomerMainAddressPK() != null).findFirst().orElse(null);
		
		return location;
	}
	
	private PolicyLocation cloneLocation(PolicyLocation location) throws InsurancePolicyException {
		// clone the location
		// Change location ID to mailing and remove all the coverages
		PolicyLocation newLocation = null;
		try {
			newLocation = (PolicyLocation) location.clone();
		} catch (CloneNotSupportedException e) {
			throw new InsurancePolicyException("Failed to clone the mailing location :"+e.getMessage(), e);
		}
		newLocation.setLocationId(SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT);
		newLocation.setCustomerMainAddressPK(null);
		newLocation.setRiskPremium(new Premium());
		newLocation.setCoverageDetail(new CoverageDetails());
		newLocation.setRateFactorsLink(new RateFactorsLink());
		newLocation.setOriginatingPK(null);
		
		return newLocation;
	}
	
	private PolicyLocation findOriginalLocationMAILING(PolicyLocation locationA, List<AddressLocationData> allTxnData) {
		PolicyLocation origLocation = null;
		
		if (locationA.getOriginatingPK() != null) {
			for(AddressLocationData locationData : allTxnData) {
				PolicyTransaction<Risk> polTransaction = locationData.getPolicyTransaction();
				// Determine the policy transaction has the original location 
				if (polTransaction.getPolicyRisks().stream().filter(o -> locationA.getOriginatingPK().longValue() == o.getPolicyRiskPK().longValue())
												.findFirst().isPresent()) {
					origLocation = polTransaction.getPolicyRisks()
													.stream().flatMap(o->Stream.of((PolicyLocation)o)).collect(Collectors.toList())
													.stream().filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId()))
																	.findFirst().orElse(null);
					break;
				}
			}
		}
		
		return origLocation;
	}
}
