package com.echelon.services.conversion;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyNotes;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;

public class MidtermChangeDataFixes {
	private static IGenericPolicyProcessing policyProcessing;
	
	private static final Map<String, String> PCYVER_BUS_TO_SYS_STATUS = Stream.of(new String[][] {
		  { "PENDING"		, "PENDING" }, 
		  { "QUOTED"  		, "PENDING" },
		  { "BOUND"   		, "PENDING" },
		  { "ISSUED"  		, "ACTIVE" },
		  { "DECLINED"		, "INACTIVE" },
		  { "NOTREQUIRED"	, "INACTIVE" }
	}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
	
	private static final Map<String, String> PCYVER_BUS_TO_PCYTXN_BUS = Stream.of(new String[][] {
			{"PENDING"		, "PENDING"},
			{"QUOTED"		, "PENDING"},
			{"BOUND"		, "PENDING"},
			{"ISSUED"		, "ISSUED"},
			{"DECLINED"		, "INACTIVE"},
			{"NOTREQUIRED"	, "INACTIVE"}
	}).collect(Collectors.toMap(data -> data[0], data -> data[1]));
	
	@SuppressWarnings("rawtypes")
	public static void setPolicyProcessing(IGenericPolicyProcessing policyProcessing) {
		MidtermChangeDataFixes.policyProcessing = policyProcessing;
	}

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws InsurancePolicyException {
		PolicyTransactionDAO ptDAO = new PolicyTransactionDAO();

		List<Long> pcyTxnPkList = ptDAO.getPolicyTransactionPkListNotIgnored();
		for (Long pcyTxnPk : pcyTxnPkList) {
			PolicyTransaction<?> pcyTxn = ptDAO.findById(pcyTxnPk);
			
			policyProcessing.loadPolicyTransactionFull(pcyTxn, false);
			
			String updSysStatus = PCYVER_BUS_TO_SYS_STATUS.get(pcyTxn.getPolicyVersion().getBusinessStatus());
			if (updSysStatus != null) {
				pcyTxn.getPolicyVersion().setSystemStatus(updSysStatus);
			}
			pcyTxn.getPolicyVersion().setVersionDate(pcyTxn.getPolicyTerm().getTermEffDate().toLocalDate());
			for (PolicyNotes pn : pcyTxn.getPolicyVersion().getPolicyNotes()) {
				pn.setBusinessStatus("NEW");
				pn.setSystemStatus(("ACTIVE".equalsIgnoreCase(pcyTxn.getPolicyVersion().getSystemStatus()) ? "ACTIVE" : "PENDING"));
				
				//ESL-1146 - START
				if (ObjectUtils.isEmpty(pn.getOriginatingDate())) {
					pn.setOriginatingDate(pn.getCreatedDate());
				}
				if (StringUtils.isBlank(pn.getOriginatingAuthor())) {
					pn.setOriginatingAuthor(pn.getCreatedBy());
				}
				//ESL-1146 - FINISH
			}
			
			pcyTxn.setSystemStatus(pcyTxn.getPolicyVersion().getSystemStatus());
			String pcyTxnBusStatus = PCYVER_BUS_TO_PCYTXN_BUS.get(pcyTxn.getPolicyVersion().getBusinessStatus());
			if (pcyTxnBusStatus != null) {
				pcyTxn.setBusinessStatus(pcyTxnBusStatus);
				
				String inSystemStatus = ("ACTIVE".equalsIgnoreCase(pcyTxn.getSystemStatus()) ? "ACTIVE" : "PENDING");
				String inBusinessStatus = "NEW";
				
				pcyTxn.getCoverageDetail().setSystemStatus(inSystemStatus);
				pcyTxn.getCoverageDetail().setBusinessStatus(inBusinessStatus);
				
				//Policy cov/end
				for (Coverage cov : pcyTxn.getCoverageDetail().getCoverages()) {
					cov.setSystemStatus(inSystemStatus);
					cov.setBusinessStatus(inBusinessStatus);
				}
				for (Endorsement end : pcyTxn.getCoverageDetail().getEndorsements()) {
					end.setSystemStatus(inSystemStatus);
					end.setBusinessStatus(inBusinessStatus);
				}

				//Policy Risk
				for (Risk risk : pcyTxn.getPolicyRisks()) {
					risk.setSystemStatus(inSystemStatus);
					risk.setBusinessStatus(inBusinessStatus);
				
					//Risk cov/end
					risk.getCoverageDetail().setSystemStatus(inSystemStatus);
					risk.getCoverageDetail().setBusinessStatus(inBusinessStatus);
					
					for (Coverage cov : risk.getCoverageDetail().getCoverages()) {
						cov.setSystemStatus(inSystemStatus);
						cov.setBusinessStatus(inBusinessStatus);
					}
					for (Endorsement end : risk.getCoverageDetail().getEndorsements()) {
						end.setSystemStatus(inSystemStatus);
						end.setBusinessStatus(inBusinessStatus);
					}					
				}
				
				//Sub-Policies
				for (SubPolicy<?> sp : pcyTxn.getSubPolicies()) {
					sp.setSystemStatus(inSystemStatus);
					sp.setBusinessStatus(inBusinessStatus);
					
					//Sub-Policy cov/end
					sp.getCoverageDetail().setSystemStatus(inSystemStatus);
					sp.getCoverageDetail().setBusinessStatus(inBusinessStatus);
					
					for (Coverage cov : sp.getCoverageDetail().getCoverages()) {
						cov.setSystemStatus(inSystemStatus);
						cov.setBusinessStatus(inBusinessStatus);
					}
					for (Endorsement end : sp.getCoverageDetail().getEndorsements()) {
						end.setSystemStatus(inSystemStatus);
						end.setBusinessStatus(inBusinessStatus);
					}
					
					//Sub-Policy Risks
					for (Risk risk : sp.getSubPolicyRisks()) {
						risk.setSystemStatus(inSystemStatus);
						risk.setBusinessStatus(inBusinessStatus);
						
						//Sub-Policy Risk cov/end
						risk.getCoverageDetail().setSystemStatus(inSystemStatus);
						risk.getCoverageDetail().setBusinessStatus(inBusinessStatus);

						for (Coverage cov : risk.getCoverageDetail().getCoverages()) {
							cov.setSystemStatus(inSystemStatus);
							cov.setBusinessStatus(inBusinessStatus);
						}
						for (Endorsement end : risk.getCoverageDetail().getEndorsements()) {
							end.setSystemStatus(inSystemStatus);
							end.setBusinessStatus(inBusinessStatus);
						}						
					}
					
					//Sub-Policy Additional Insureds
					for (AdditionalInsured ai : sp.getAdditionalInsureds()) {
						ai.setSystemStatus(inSystemStatus);
						ai.setBusinessStatus(inBusinessStatus);
						if (ai.getAdditionalInsuredAddress() != null) {							
							ai.getAdditionalInsuredAddress().setSystemStatus(inSystemStatus);
							ai.getAdditionalInsuredAddress().setBusinessStatus(inBusinessStatus);
						}
					}
					
					//Sub-Policy LienHolderLessors
					for (LienholderLessorSubPolicy lhlSP : sp.getLienholderLessors()) {
						lhlSP.setSystemStatus(inSystemStatus);
						lhlSP.setBusinessStatus(inBusinessStatus);
					}
					
					if ("CGL".equalsIgnoreCase(sp.getSubPolicyTypeCd())) {
						CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)sp;
						for (CGLLocation cglLoc : cglSP.getCglLocations()) {
							cglLoc.setSystemStatus(inSystemStatus);
							cglLoc.setBusinessStatus(inBusinessStatus);
							cglLoc.setCreatedBy(cglSP.getCreatedBy());
							cglLoc.setCreatedDate(cglSP.getCreatedDate());
							cglLoc.setUpdatedBy(cglSP.getUpdatedBy());
							cglLoc.setUpdatedDate(cglSP.getUpdatedDate());
						}
					}
				}
				
				//Policy Lien Holder Lessors
				for (LienholderLessor lhl : pcyTxn.getLienholderLessors()) {
					lhl.setSystemStatus(inSystemStatus);
					lhl.setBusinessStatus(inBusinessStatus);
				}
								
				//Policy Customer
				pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().setSystemStatus(inSystemStatus);
				pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().setBusinessStatus(inBusinessStatus);
				
				pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().setSystemStatus(inSystemStatus);
				pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().setBusinessStatus(inBusinessStatus);
				
				//update the PolicyLocation Address Status Fields
				List<Risk> locationList = (List<Risk>) pcyTxn.findRiskByType("LO");
				if (!locationList.isEmpty()) {
					for (Risk r : locationList) {
						PolicyLocation pl = (PolicyLocation)r;
						pl.getLocationAddress().setSystemStatus(inSystemStatus);
						pl.getLocationAddress().setBusinessStatus(inBusinessStatus);
					}
				}
				
				//update the AdditionalInsuredVehicles Status Fields
				SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) pcyTxn.findSubPolicyByType("AUTO");
				if (autoSP != null) {
					for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
						aiv.setSystemStatus(inSystemStatus);
						aiv.setBusinessStatus(inBusinessStatus);
					}
					for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
						lhlVD.setSystemStatus(inSystemStatus);
						lhlVD.setBusinessStatus(inBusinessStatus);
					}
				}
			}
			ptDAO.merge(pcyTxn);
		}
	}
}
