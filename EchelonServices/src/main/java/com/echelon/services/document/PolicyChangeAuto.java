package com.echelon.services.document;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.ControlChar;
import com.aspose.words.Document;
import com.aspose.words.Paragraph;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyChangeAuto extends EchelonDocument implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	protected String[] fieldNames;
	protected Object[] fieldValues;
	protected String brokerName = "";
	protected String master_broker_no = "";
	protected String insuredName = "";
	protected String polNo = "";
	protected String effectiveDate = "";
	protected String expiryDate = "";
	protected String insuredAddressStreet = "";
	protected String insuredAddressCity = "";
	protected String insuredAddressProvState = "";
	protected String insuredAddressPC = "";
	protected String expYear = "";
	protected String expMonth = "";
	protected String expDay = "";
	protected String opcf2_name = "";
	protected String opcf2_relationship = "";
	protected String lbLimit = "";
	protected String opcf8_ded = "";
	protected String opcf20_limit = "";
	protected String opcf20H_limit = "";
	protected boolean opcf20_exists = false;
	protected String fleetBasis = "";
	protected boolean opcf21B_prorata = false;
	protected boolean opcf21B_5050 = false;
	protected String cargo_prem = "";
	protected String cargo = "";
	protected boolean ebeExists = false;
	protected String emp_benefits = "";
	protected String ebe_prem = "";
	protected boolean opcf23A_dcpd_ck = false;
	protected String opcf23A_dcpd_ded = "";
	protected boolean opcf23A_sp_ck = false;
	protected String opcf23A_sp_ded = "";
	protected boolean opcf23A_cl_ck = false;
	protected String opcf23A_cl_ded = "";
	protected boolean opcf23A_cm_ck = false;
	protected String opcf23A_cm_ded = "";
	protected boolean opcf23A_ap_ck = false;
	protected String opcf23A_ap_ded = "";
	protected boolean opcf23ABL_dcpd_ck = false;
	protected String opcf23ABL_dcpd_ded = "";
	protected boolean opcf23ABL_sp_ck = false;
	protected String opcf23ABL_sp_ded = "";
	protected boolean opcf23ABL_cl_ck = false;
	protected String opcf23ABL_cl_ded = "";
	protected boolean opcf23ABL_cm_ck = false;
	protected String opcf23ABL_cm_ded = "";
	protected boolean opcf23ABL_ap_ck = false;
	protected String opcf23ABL_ap_ded = "";
	protected String opcf27_apded = "";
	protected String opcf27_limit = "";
	protected String opcf27B_apded = "";
	protected String businessType = "";
	protected LinkedHashSet<HashMap<String, String>> opcf27PersonsTable = new LinkedHashSet<HashMap<String, String>>();
	protected LinkedHashSet<Endorsement> opcf27BEnds = new LinkedHashSet<Endorsement>();
	protected String opcf28_pers = "";
	protected String opcf28_limit = "";
	protected String opcf28CL_ded = "";
	protected String opcf28AP_ded = "";
	protected boolean opcf28_coll = false;
	protected boolean opcf28_allperil = false;
	protected boolean opcf28_collNot = false;
	protected boolean opcf28_allperilNot = false;
	protected List<Endorsement> opcf28AEnds = new ArrayList<Endorsement>();
	protected LinkedHashSet<Endorsement> opcf38Ends = new LinkedHashSet<Endorsement>();
	protected String opcf44RPrintLimit = "";
	protected String eon114_name_ins = "";
	protected String eon102_ded = "";
	protected String eon102_sec7_ded = "";
	protected String cglcgo_ge_prem = "";
	protected Double cglcgo_ge_prem_total = 0.0;
	protected String gecglmtc_mod_type = "";
	protected String gecglmtc_prem = "";
	protected LinkedHashSet<HashMap<String, String>> gecglmtc_vehicles = new LinkedHashSet<HashMap<String, String>>();
	protected String gecgl_mod_type = "";
	protected LinkedHashSet<HashMap<String, String>> gecgl_vehicles = new LinkedHashSet<HashMap<String, String>>();
	protected String gecgl_prem = "";
	protected String gdpc_dealer_plate_no = "";
	protected String gdpc_exp_date = "";
	protected String aic_limit = "";
	protected String aic_ded = "";
	protected String aicc_limit = "";
	protected String aicc_ded = "";
	protected LinkedHashSet<Endorsement> eon109Ends = new LinkedHashSet<Endorsement>();
	protected String eon111_effective_date = "";
	protected String eon111_expiry_date = "";
	protected String per_occurrence_ded = "N/A";
	protected String wos_co_name = "";
	protected String wos_co_addr = "";
	protected String aoccgl_prem = "";
	protected String moccgl_prem = "";
	protected String aocc_prem = "";
	protected String mocc_prem = "";
	protected String mtcc_limit = "";
	protected String mtcc_ded = "";
	protected String tll_limit = "N/A";
	protected String tll_ded = "N/A";
	protected String cgl_lb_limit = "N/A";
	protected String cgl_ded = "N/A";
	protected String cgl_ge_prem = "";
	protected String cargo_ge_prem = "";
	protected LinkedHashSet<HashMap<String, String>> doccTypes = new LinkedHashSet<HashMap<String, String>>();
	protected String oef71_limit = "N/A";
	protected String oef71_ded = "N/A";
	protected LinkedHashSet<HashMap<String, String>> oef72AlterationsTable = new LinkedHashSet<HashMap<String, String>>();
	protected String oef72_prem = "$Incl.";
	protected String oef72_end_codes = "";
	protected String oef72_end_premA = "$Incl.";
	protected String oef72_end_premR = "$";
	protected String oef72_net_premA= "$Incl.";
	protected String oef72_net_premR = "";
	protected String oef73_other_party = "";
	protected String oef73_other_party_address = "";
	protected String cl511_premA = "";
	protected String cl511_premR = "";
	protected String cm512_premA = "";
	protected String cm512_premR = "";
	protected String sp513_premA = "";
	protected String sp513_premR = "";
	protected String sp514_premA = "";
	protected String sp514_premR = "";
	protected String sp64_premA = "";
	protected String sp64_premR = "";
	protected String oef74_ded = "";
	protected String oef75_ded = "";
	protected String oef76_insured_name = "";
	protected String oef76_insured_relationship = "";
	protected String oef77_limit = "N/A";
	protected String oef77_ded = "N/A";
	protected String oef77_oap4_sp_prema = "";
	protected String oef77_oap4_sp_premb = "";
	protected String oef77_oap4_sp_premc = "";
	protected String oef77_oap4_sp_premd = "";
	protected String oef78_pers = "";
	protected String oef78_liab_limit = "";
	protected String oef78_insuredind1 = "";
	protected String oef78_insuredind2 = "";
	protected String oef78_insuredind3 = "";
	protected String cl5_ded = "";
	protected String cl6_lim = "";
	protected String cl6_ded = "";
	protected String oef78A_exclDriver = "";
	protected String oef78A_driverLicNo = "";
	protected String oef81_lim = "N/A";
	protected String oef81_premA = "$N/A";
	protected String oef81_premR = "$";
	protected String oef82_511_ded = "";
	protected String oef82_511_prem = "";
	protected String oef82_512_ded = "";
	protected String oef82_512_prem = "";
	protected String oef82_513_ded = "";
	protected String oef82_513_prem = "";
	protected String oef82_lim = "";
	protected LinkedHashSet<HashMap<String, String>> oef82PersonsTable = new LinkedHashSet<HashMap<String, String>>();
	protected String oef83_limit = "N/A";
	protected String oef83_ded = "N/A";
	protected String oef83_numOfVeh = "";
	protected String oef83_coll5 = "";
	protected String oef83_coll6 = "";
	protected String oef86_limit = "N/A";
	protected String maxnum_veha = "";
	protected String oap4_sp_limita = "";
	protected String oap4_sp_deda = "";
	protected String oap4_sp_prema = "";
	protected String maxnum_vehb = "";
	protected String oap4_sp_limitb = "";
	protected String oap4_sp_dedb = "";
	protected String oap4_sp_premb = "";
	protected String maxnum_vehc = "";
	protected String oap4_sp_limitc = "";
	protected String oap4_sp_dedc = "";
	protected String oap4_sp_premc = "";
	protected String maxnum_vehd = "";
	protected String oap4_sp_limitd = "";
	protected String oap4_sp_dedd = "";
	protected String oap4_sp_premd = "";
	protected String gdpc_servPlateNo = "";
	protected String gdpc_expDate = "";
	protected String gspc_servPlateNo = "";
	protected String gspc_expDate = "";
	protected LinkedHashSet<HashMap<String, String>> aloccgl_locations = new LinkedHashSet<HashMap<String, String>>();
	protected LinkedHashSet<HashMap<String, String>> dloccgl_locations = new LinkedHashSet<HashMap<String, String>>();
	protected LinkedHashSet<HashMap<String, String>> alocc_locations = new LinkedHashSet<HashMap<String, String>>();
	protected LinkedHashSet<HashMap<String, String>> dlocc_locations = new LinkedHashSet<HashMap<String, String>>();
	protected String ttc_vehdesc = "";
	protected String ttc_period1 = "";
	protected String ttc_period2 = "";
	protected String ttc_serno = "";
	protected String ttc_limit = "";
	protected String ttc_ded = "";
	protected String ttc_prem = "";
	protected String ctc_limit = "N/A";
	protected String cargoLimit1Dec = "N/A";
	protected String cargoLimit3Dec = "N/A";
	protected String cargoLimit4Dec = "N/A";
	protected String cargoLimit5Dec = "N/A";
	protected String cargoLimit6Dec = "N/A";
	protected LinkedHashSet<Endorsement> numEnds = new LinkedHashSet<Endorsement>();
	protected int maxEndNo = 0;
	protected String opcf25AA_unitYear = "";
	protected String opcf25AA_unitMake = "";
	protected String opcf25AA_unitSerialNo = "";
	protected String opcf25AA_prem = "";
	protected String opcf25AA_prem_a = "";
	protected String opcf25AA_prem_r = "";
	protected boolean opcf25AA_addAuto = false;
	protected String opcf25AA_addAutoNo = "";
	protected boolean opcf25AA_delAuto = false;
	protected String opcf25AA_delAutoNo = "";
	protected String opcf25AA_biprem_a = "";
	protected String opcf25AA_biprem_r = "";
	protected String opcf25AA_liabLimit = "";
	protected boolean opcf25AA_delCov = false;
	protected String opcf25AA_delCovVeh = "";
	protected boolean opcf25AA_addCov = false;
	protected String opcf25AA_addCovVeh = "";
	protected boolean opcf25AA_chgAuto = false;
	protected String opcf25AA_chgAutoVeh = "";
	protected boolean opcf25AA_chgOB = false;
	protected String opcf25AA_chgOBVeh = "";
	protected boolean opcf25AA_oth = false;
	protected String opcf25AA_othDesc = "";
	protected String opcf25AA_pd_a = "";
	protected String opcf25AA_pd_r = "";
	protected String opcf25AA_sp_a = "";
	protected String opcf25AA_sp_r = "";
	protected String opcf25AA_cm_a = "";
	protected String opcf25AA_cm_r = "";
	protected String opcf25AA_cl_a = "";
	protected String opcf25AA_cl_r = "";
	protected String opcf25AA_ap_a = "";
	protected String opcf25AA_ap_r = "";
	protected String opcf25AA_a = "";
	protected String opcf25AA_r = "";
	protected String opcf25AA_spDed = "";
	protected String opcf25AA_cmDed = "";
	protected String opcf25AA_clDed = "";
	protected String opcf25AA_apDed = "";
	protected String opcf25AA_abPrem_a = "";
	protected String opcf25AA_abPrem_r = "";
	protected String opcf25AA_irPrem_a = "";
	protected String opcf25AA_irPrem_r = "";
	protected String opcf25AA_irLim = "";
	protected String opcf25AA_mrcPrem_a = "";
	protected String opcf25AA_mrcPrem_r = "";
	protected String opcf25AA_ociPrem_a = "";
	protected String opcf25AA_ociPrem_r = "";
	protected String opcf25AA_chmPrem_a = "";
	protected String opcf25AA_chmPrem_r = "";
	protected String opcf25AA_dfPrem_a = "";
	protected String opcf25AA_dfPrem_r = "";
	protected String opcf25AA_dcPrem_a = "";
	protected String opcf25AA_dcPrem_r = "";
	protected String opcf25AA_ibPrem_a = "";
	protected String opcf25AA_ibPrem_r = "";
	protected String opcf25AA_uaPrem_a = "";
	protected String opcf25AA_uaPrem_r = "";
	protected String opcf25AA_dcpdPrem_a = "";
	protected String opcf25AA_dcpdPrem_r = "";
	protected String opcf25AA_dcpdDed = "";
	protected String opcf25AA_endList_a = "";
	protected String opcf25AA_endList_r = "";
	protected String opcf25AA_endList = "";
	protected boolean opcf25AA_nameAddr = false;
	protected Section opcf25AASec = null;
	protected Section opcf2Sec = null;
	protected Section opcf5Sec = null;
	protected Section opcf5BlSec = null;
	protected Section opcf8Sec = null;
	protected Section opcf9Sec = null;
	protected Section opcf13CSec = null;
	protected Section opcf19Sec = null;
	protected Section opcf20Sec = null;
	protected Section opcf20HSec = null;
	protected Section opcf21ASec = null;
	protected Section opcf21BSec = null;
	protected Section opcf23ASec = null;
	protected Section opcf23ABlSec = null;
	protected Section opcf27Sec = null;
	protected Section opcf27BSec = null;
	protected Section opcf28Sec = null;
	protected Section opcf28ASec = null;
	protected Section opcf30Sec = null;
	protected Section opcf31Sec = null;
	protected Section opcf38Sec = null;
	protected Section opcf40Sec = null;
	protected Section opcf43Sec = null;
	protected Section opcf43ASec = null;
	protected Section opcf44RSec = null;
	protected Section opcf47Sec = null;
	protected Section opcf48Sec = null;
	protected Section oef71Sec = null;
	protected Section oef72Sec = null;
	protected Section oef73Sec = null;
	protected Section oef74Sec = null;
	protected Section oef75Sec = null;
	protected Section oef76Sec = null;
	protected Section oef77Sec = null;
	protected Section oef78Sec = null;
	protected Section oef78ASec = null;
	protected Section oef79Sec = null;
	protected Section oef80Sec = null;
	protected Section oef81Sec = null;
	protected Section oef82Sec = null;
	protected Section oef83Sec = null;
	protected Section oef86Sec = null;
	protected Section oef87Sec = null;
	protected Section gdpcgSec = null;
	protected Section gspcSec = null;
	protected Section eon114Sec = null;
	protected Section eon101Sec = null;
	protected Section eon102Sec = null;
	protected Section gecglmtcSec = null;
	protected Section gecglSec = null;
	protected Section eon20GSec = null;
	protected Section gdpcSec = null;
	protected Section gleeSec = null;
	protected Section aicSec = null;
	protected Section aiccSec = null;
	protected Section eon109Sec = null;
	protected Section eon110Sec = null;
	protected Section eon111Sec = null;
	protected Section eon112Sec = null;
	protected Section eon121Sec = null;
	protected Section doccSec = null;
	protected Section wosSec = null;
	protected Section aoccglSec = null;
	protected Section moccglSec = null;
	protected Section aloccglSec = null;
	protected Section dloccglSec = null;
	protected Section cdelSec = null;
	protected Section aoccSec = null;
	protected Section moccSec = null;
	protected Section aloccSec = null;
	protected Section dloccSec = null;
	protected Section ttcSec = null;
	protected Section cpeeSec = null;
	protected Section cdecSec = null;
	protected Section eon116Sec = null;
	protected Section eon117Sec = null;
	protected Section iaSec = null;
	protected boolean showOPCF2Section = false;
	protected boolean showOPCF25AASection = false;
	protected boolean showOPCF5Section = false;
	protected boolean showOPCF5BlSection = false;
	protected boolean showOPCF8Section = false;
	protected boolean showOPCF9Section = false;
	protected boolean showOPCF13CSection = false;
	protected boolean showOPCF19Section = false;
	protected boolean showOPCF20Section = false;
	protected boolean showOPCF20HSection = false;
	protected boolean showOPCF21ASection = false;
	protected boolean showOPCF21BSection = false;
	protected boolean showOPCF23ASection = false;
	protected boolean showOPCF23ABlSection = false;
	protected boolean showOPCF27Section = false;
	protected boolean showOPCF27BSection = false;
	protected boolean showOPCF28Section = false;
	protected boolean showOPCF28ASection = false;
	protected boolean showOPCF30Section = false;
	protected boolean showOPCF31Section = false;
	protected boolean showOPCF38Section = false;
	protected boolean showOPCF40Section = false;
	protected boolean showOPCF43Section = false;
	protected boolean showOPCF43ASection = false;
	protected boolean showOPCF44RSection = false;
	protected boolean showOPCF47Section = false;
	protected boolean showOPCF48Section = false;
	protected boolean showCGODecSection = false;
	protected boolean showOEF71Section = false;
	protected boolean showOEF72Section = false;
	protected boolean showOEF73Section = false;
	protected boolean showOEF74Section = false;
	protected boolean showOEF75Section = false;
	protected boolean showOEF76Section = false;
	protected boolean showOEF77Section = false;
	protected boolean showOEF78Section = false;
	protected boolean showOEF78ASection = false;
	protected boolean showOEF79Section = false;
	protected boolean showOEF80Section = false;
	protected boolean showOEF81Section = false;
	protected boolean showOEF82Section = false;
	protected boolean showOEF83Section = false;
	protected boolean showOEF86Section = false;
	protected boolean showOEF87Section = false;
	protected boolean showGDPCGSection = false;
	protected boolean showGSPCSection = false;
	protected boolean showEON114Section = false;
	protected boolean showEON101Section = false;
	protected boolean showEON102Section = false;
	protected boolean showGECGLMTCSection = false;
	protected boolean showGECGLSection = false;
	protected boolean showEON20GSection = false;
	protected boolean showGDPCSection = false;
	protected boolean showGLEESection = false;
	protected boolean showAICSection = false;
	protected boolean showAICCSection = false;
	protected boolean showEON109Section = false;
	protected boolean showEON110Section = false;
	protected boolean showEON111Section = false;
	protected boolean showEON112Section = false;
	protected boolean showEON121Section = false;
	protected boolean showDOCCSection = false;
	protected boolean showWOSSection = false;
	protected boolean showAOCCGLSection = false;
	protected boolean showMOCCGLSection = false;
	protected boolean showALOCCGLSection = false;
	protected boolean showDLOCCGLSection = false;
	protected boolean showCDELSection = false;
	protected boolean showAOCCSection = false;
	protected boolean showMOCCSection = false;
	protected boolean showALOCCSection = false;
	protected boolean showDLOCCSection = false;
	protected boolean showTTCSection = false;
	protected boolean showCPEESection = false;
	protected boolean showCDECSection = false;
	protected boolean showEON116Section = false;
	protected boolean showEON117Section = false;
	protected boolean showIASection = false;



	public String getTemplatePath() {
		return "";
	}

	public void create(String storageKey) throws Exception {
	}

	public void initializeFields() {
	}

	protected void setOPCF5LessorTable(Document doc) {
		// LESSOR table
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = opcf5Sec;
		Section refSection = tmpSection;
		for (LienholderLessorSubPolicy lessorSubPolicy : subPolicyAuto.getLienholderLessors()) {
			if (lessorSubPolicy.getLienLessorType().equalsIgnoreCase(SpecialtyAutoConstants.LIENLESSORTYPE_HLESSOR)) {
				LienholderLessor lessor = lessorSubPolicy.getLienholderLessor();
				String transType = policyDocument.getPolicyTransaction().getTransactionType();
				String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
				String businessStatus = lessorSubPolicy.getBusinessStatus();
				String systemStatus = lessorSubPolicy.getSystemStatus();
				boolean hasNewVehicle = hasNewVehicle(lessor.getLienholderLessorPK());

				if ((transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
						&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
						&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)))
						|| hasNewVehicle) {
					Section lessorSection = (Section) tmpSection.deepClone(true);
					Table lessorTable = lessorSection.getBody().getTables().get(1);

					// Set Lessor Name
					Row nameRow = lessorTable.getRows().get(0);
					Cell nameCell = nameRow.getCells().get(1);
					nameCell.getFirstParagraph().removeAllChildren();
					Run nameRun = new Run(doc, lessor.getCompanyName());
					nameRun.getFont().setName("Arial");
					nameRun.getFont().setSize(8);
					nameRun.getFont().setBold(true);
					nameCell.getFirstParagraph().appendChild(nameRun);
					// Set Lessor addr
					Row addrRow = lessorTable.getRows().get(1);
					Cell addrCell = addrRow.getCells().get(1);
					addrCell.getFirstParagraph().removeAllChildren();
					Run addrRun = new Run(doc,
							lessor.getLienholderLessorAddress().getAddressLine1() + ", "
									+ lessor.getLienholderLessorAddress().getCity() + " "
									+ lessor.getLienholderLessorAddress().getProvState() + " "
									+ lessor.getLienholderLessorAddress().getPostalZip());
					addrRun.getFont().setName("Arial");
					addrRun.getFont().setSize(8);
					addrRun.getFont().setBold(true);
					addrCell.getFirstParagraph().appendChild(addrRun);

					Row firstVehicleRow = lessorTable.getRows().get(4);
					List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto
							.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
					vehicleDetails.sort(Comparator.comparingInt(lhl -> {
						if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
							return Integer.parseInt(lhl.getVehicleNum());
						} else {
							return 0;
						}
					}));

					for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
						if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
								.equals(lessor.getLienholderLessorPK())) {
							String vehicleBusinessStatus = vehicleDetail.getBusinessStatus();
							String vehicleSystemStatus = vehicleDetail.getSystemStatus();

							if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
									&& vehicleBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
									&& (vehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
											|| vehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
								Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

								// Veh_Num
								newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
								Run newRun1 = new Run(doc,
										vehicleDetail.getVehicleNum() != null ? vehicleDetail.getVehicleNum().toString()
												: "");
								newRun1.getFont().setName("Arial");
								newRun1.getFont().setSize(8);
								newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

								// Veh_Desc
								newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
								Run newRun2 = new Run(doc,
										vehicleDetail.getVehicleYear() + " " + vehicleDetail.getDescription());
								newRun2.getFont().setName("Arial");
								newRun2.getFont().setSize(8);
								newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

								// Veh_Serial_No
								newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
								Run newRun3 = new Run(doc, vehicleDetail.getSerialNum());
								newRun3.getFont().setName("Arial");
								newRun3.getFont().setSize(8);
								newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

								// LB_Limit
								newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
								Run newRun4 = new Run(doc, "$" + lbLimit);
								newRun4.getFont().setName("Arial");
								newRun4.getFont().setSize(8);
								newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

								// AP_Ded
								newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
								Run newRun5 = new Run(doc, "$" + numberFormat(vehicleDetail.getLessorDeductible()));
								newRun5.getFont().setName("Arial");
								newRun5.getFont().setSize(8);
								newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

								lessorTable.insertAfter(newVehicleRow, firstVehicleRow);
								firstVehicleRow = newVehicleRow;
							}
						}
					}
					lessorTable.removeChild(lessorTable.getRows().get(4));
					doc.insertAfter(lessorSection, refSection);
					refSection = lessorSection;
				}
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF19Section(Document doc) throws Exception {
		Section tmpSection = opcf19Sec;
		Section refSection = tmpSection;

		List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();

		for (SpecialtyVehicleRisk vehiclerisk : vehicleRisks) {
			Set<Endorsement> vehEndorsements = vehiclerisk.getRiskEndorsements();
			for (Endorsement end : vehEndorsements) {
				if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
					String value = numberFormat(end.getLimit1amount());
					String vehicles = "";
					if (fleetBasis.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)) {
						vehicles = numberFormat(vehiclerisk.getVehicleSequence());
					} else {
						if (end.getDataExtension() != null) {
							for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
								for (ExtensionData extData : extType.getExtensionDatas()) {
									if (extData.getColumnId().equalsIgnoreCase("OPCF19_VehDesc")
											&& extData.getColumnValue() != null) {
										vehicles = extData.getColumnValue();
									}
								}
							}
						}
					}

					Section new19Section = (Section) tmpSection.deepClone(true);

					// OPCF19_vehicles
					Table table = new19Section.getBody().getTables().get(1);
					Row row = table.getRows().get(0);
					Cell cell = row.getCells().get(1);
					cell.getFirstParagraph().getRange().replace("{{OPCF19_Vehicles}}", vehicles);

					// OPCF19_value
					new19Section.getBody().getParagraphs().get(3).getRange().replace("{{OPCF19_Value}}", value);

					doc.insertAfter(new19Section, refSection);
					refSection = new19Section;
				}
			}

		}
		doc.removeChild(tmpSection);
	}

	private void setOPCF20Section() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20Section = true;
					this.opcf20_limit = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("MTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("LTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("HTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TT")) {
					this.showOPCF20HSection = true;
					this.opcf20H_limit = numberFormat(end.getLimit1amount());
				}
			}
		}
	}

	protected void setOPCF21BFirstTable(Document doc) {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		Section sec = opcf21BSec;
		Table tmpTable = sec.getBody().getTables().get(2);
		Row firstVehicleRow = tmpTable.getRows().get(3);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			if (vehicle.getVehicleDescription().equals("OP27")) {
				continue;
			}
			String veh_desc = vehicle.getDisplayString();
			String dcdpd_ded = "N/A";
			String sp_ded = "N/A";
			String comp_ded = "N/A";
			String coll_ded = "N/A";
			String ap_ded = "N/A";

			for (Coverage cove : vehicle.getRiskCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("DCPD") && cove.getDeductible1() != null) {
					dcdpd_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP") && cove.getDeductible1() != null) {
					sp_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM") && cove.getDeductible1() != null) {
					comp_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL") && cove.getDeductible1() != null) {
					coll_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP") && cove.getDeductible1() != null) {
					ap_ded = numberFormat(cove.getDeductible1());
				}
			}

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// Veh_Desc
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, veh_desc);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

			// DCDPD_Ded
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, dcdpd_ded);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			if (!dcdpd_ded.equalsIgnoreCase("N/A")) {
				newRun2.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

			// SP_Ded
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, sp_ded);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			if (!sp_ded.equalsIgnoreCase("N/A")) {
				newRun3.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

			// COMP_Ded
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, comp_ded);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			if (!comp_ded.equalsIgnoreCase("N/A")) {
				newRun4.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

			// COLL_Ded
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, coll_ded);
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			if (!coll_ded.equalsIgnoreCase("N/A")) {
				newRun5.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

			// AP_Ded
			newVehicleRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun6 = new Run(doc, ap_ded);
			newRun6.getFont().setName("Arial");
			newRun6.getFont().setSize(8);
			if (!ap_ded.equalsIgnoreCase("N/A")) {
				newRun6.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(5).getFirstParagraph().appendChild(newRun6);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		tmpTable.removeChild(tmpTable.getRows().get(3));
	}

	protected void setOPCF21BSecondTable(Document doc) {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		String veh_desc = "";
		String unitRate = "";
		String numOfUnits = "";
		String vehPrem = "";

		Section sec = opcf21BSec;
		Table tmpTable = sec.getBody().getTables().get(4);
		Row firstVehicleRow = tmpTable.getRows().get(1);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			veh_desc = vehicle.getDisplayString();
			unitRate = "$" + numberFormat(vehicle.getUnitRate());
			numOfUnits = Integer.toString(vehicle.getNumberOfUnits());
			if (vehicle.getUnitRate() != null && vehicle.getNumberOfUnits() != null) {
				vehPrem = numberFormat(vehicle.getUnitRate() * vehicle.getNumberOfUnits());
			}

			if (vehicle.getVehicleDescription().equals("OP27")) {
				veh_desc = "OPCF 27B";
				unitRate = "";
				numOfUnits = "";
				vehPrem = numberFormat(vehicle.getRiskPremium().getWrittenPremium());
			}

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, numOfUnits);
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, veh_desc);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, unitRate);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$" + vehPrem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		if (this.showCGODecSection) {
			// Cargo Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.cargo);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, this.cargo_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		if (this.ebeExists) {
			// Employee Benefits Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.emp_benefits);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, this.ebe_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		tmpTable.removeChild(tmpTable.getRows().get(1));
	}

	protected void setOPCF23ASecondTable(Document doc) {
		// LIENHOLDER table
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = opcf23ASec;
		Section refSection = tmpSection;
		for (LienholderLessorSubPolicy lienholderSubPolicy : subPolicyAuto.getLienholderLessors()) {
			if (lienholderSubPolicy.getLienLessorType().equalsIgnoreCase(SpecialtyAutoConstants.LIENLESSORTYPE_HOLDER)) {
				LienholderLessor lienHolder = lienholderSubPolicy.getLienholderLessor();
				String transType = policyDocument.getPolicyTransaction().getTransactionType();
				String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
				String businessStatus = lienholderSubPolicy.getBusinessStatus();
				String systemStatus = lienholderSubPolicy.getSystemStatus();
				boolean hasNewVehicle = hasNewVehicle(lienHolder.getLienholderLessorPK());

				if ((transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
						&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
						&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)))
						|| hasNewVehicle) {
					Section lienHolderSection = (Section) tmpSection.deepClone(true);
					Table lienHolderTable = lienHolderSection.getBody().getTables().get(1);

					// Set Lienholder Name and Address
					Row addrRow = lienHolderTable.getFirstRow();
					Cell addrCell = addrRow.getFirstCell();

					// Name
					Paragraph paraName = addrCell.getParagraphs().get(2);
					Run runName = new Run(doc, lienHolder.getCompanyName());
					runName.getFont().setName("Arial");
					runName.getFont().setSize(8);
					runName.getFont().setBold(true);
					paraName.removeAllChildren();
					paraName.appendChild(runName);

					// Street
					Paragraph paraStreet = addrCell.getParagraphs().get(3);
					Run runStreet = new Run(doc, lienHolder.getLienholderLessorAddress().getAddressLine1());
					runStreet.getFont().setName("Arial");
					runStreet.getFont().setSize(8);
					runStreet.getFont().setBold(true);
					paraStreet.removeAllChildren();
					paraStreet.appendChild(runStreet);

					// City & Zip Code
					Paragraph paraCity = addrCell.getParagraphs().get(4);
					Run runCity = new Run(doc,
							lienHolder.getLienholderLessorAddress().getCity() + " "
									+ lienHolder.getLienholderLessorAddress().getProvState() + " "
									+ lienHolder.getLienholderLessorAddress().getPostalZip());
					runCity.getFont().setName("Arial");
					runCity.getFont().setSize(8);
					runCity.getFont().setBold(true);
					paraCity.removeAllChildren();
					paraCity.appendChild(runCity);

					Table vehicleTable = lienHolderSection.getBody().getTables().get(2);
					Row firstVehicleRow = vehicleTable.getRows().get(1);
					List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto
							.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
					vehicleDetails.sort(Comparator.comparingInt(lhl -> {
						if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
							return Integer.parseInt(lhl.getVehicleNum());
						} else {
							return 0;
						}
					}));

					for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
						if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
								.equals(lienHolder.getLienholderLessorPK())) {
							String vehicleBusinessStatus = vehicleDetail.getBusinessStatus();
							String vehicleSystemStatus = vehicleDetail.getSystemStatus();

							if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
									&& vehicleBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
									&& (vehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
											|| vehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
								Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

								// Automobile #
								newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
								Run newRun1 = new Run(doc,
										vehicleDetail.getVehicleNum() != null ? vehicleDetail.getVehicleNum().toString()
												: "");
								newRun1.getFont().setName("Arial");
								newRun1.getFont().setSize(8);
								newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

								// Model Year
								newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
								Run newRun2 = new Run(doc,
										vehicleDetail.getVehicleYear() != null
												? Integer.toString(vehicleDetail.getVehicleYear())
												: "");
								newRun2.getFont().setName("Arial");
								newRun2.getFont().setSize(8);
								newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

								// Trade NAme (Make)
								newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
								Run newRun3 = new Run(doc, vehicleDetail.getDescription());
								newRun3.getFont().setName("Arial");
								newRun3.getFont().setSize(8);
								newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

								// Serial #
								newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
								Run newRun4 = new Run(doc, vehicleDetail.getSerialNum());
								newRun4.getFont().setName("Arial");
								newRun4.getFont().setSize(8);
								newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

								vehicleTable.insertAfter(newVehicleRow, firstVehicleRow);
								firstVehicleRow = newVehicleRow;
							}
						}
					}
					vehicleTable.removeChild(vehicleTable.getRows().get(1));
					doc.insertAfter(lienHolderSection, refSection);
					refSection = lienHolderSection;
				}
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF27Table(Document doc) {
		Section sec = opcf27Sec;
		Table table = sec.getBody().getTables().get(2);
		Row firstRow = table.getRows().get(1);
		for (HashMap<String, String> person : opcf27PersonsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Named Persons
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, person.get("pers"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Relationship to Insured/Lessee
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, person.get("relation"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setOPCF27BSection(Document doc) {
		Section tmpSection = opcf27BSec;
		Section refSection = tmpSection;

		Iterator<Endorsement> it = opcf27BEnds.iterator();
		while (it.hasNext()) {
			Endorsement end = it.next();
			String typeOfVehicle = "";
			if (end.getDataExtension() != null) {
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")) {
							typeOfVehicle = extData.getColumnValue();
						}
					}
				}
			}
			String limit = numberFormat(end.getLimit1amount());
			String deductible = numberFormat(end.getDeductible1amount());

			Section new27BSection = (Section) tmpSection.deepClone(true);

			// OPCF27B_APDed
			Table dedTable = new27BSection.getBody().getTables().get(2);
			Row dedRow = dedTable.getRows().get(4);
			Cell dedCell = dedRow.getCells().get(2);
			dedCell.getFirstParagraph().removeAllChildren();
			Run dedRun = new Run(doc, "$" + deductible);
			dedRun.getFont().setName("Arial");
			dedRun.getFont().setSize(8);
			dedRun.getFont().setBold(true);
			dedCell.getFirstParagraph().appendChild(dedRun);

			// OPCF27B_Tractor_Trailer
			Table vehTypeTable = new27BSection.getBody().getTables().get(3);
			Row vehTypeRow = vehTypeTable.getFirstRow();
			Cell vehTypeCell = vehTypeRow.getFirstCell();
			vehTypeCell.getFirstParagraph().removeAllChildren();
			Run vehTypeRun = new Run(doc, typeOfVehicle);
			vehTypeRun.getFont().setName("Arial");
			vehTypeRun.getFont().setSize(8);
			vehTypeRun.getFont().setBold(true);
			vehTypeCell.getFirstParagraph().appendChild(vehTypeRun);

			// OPCF27B_Limit
			Run limit1Run = new Run(doc, "We will not pay more than ");
			limit1Run.getFont().setName("Arial");
			limit1Run.getFont().setSize(8);

			Run limit2Run = new Run(doc, "$" + limit);
			limit2Run.getFont().setName("Arial");
			limit2Run.getFont().setSize(8);
			limit2Run.getFont().setBold(true);

			Run limit3Run = new Run(doc,
					" under this change for any one occurrence plus the costs provided for in 3.4 above.");
			limit3Run.getFont().setName("Arial");
			limit3Run.getFont().setSize(8);

			new27BSection.getBody().getParagraphs().get(13).removeAllChildren();
			new27BSection.getBody().getParagraphs().get(13).appendChild(limit1Run);
			new27BSection.getBody().getParagraphs().get(13).appendChild(limit2Run);
			new27BSection.getBody().getParagraphs().get(13).appendChild(limit3Run);

			doc.insertAfter(new27BSection, refSection);
			refSection = new27BSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF28ASection(Document doc) {
		Section tmpSection = opcf28ASec;
		Section refSection = tmpSection;
		opcf28AEnds.sort(Comparator.comparing(Endorsement::getEndorsementsk));

		for (Endorsement end : opcf28AEnds) {
			StringBuilder autoNumSB = new StringBuilder();
			StringBuilder modYearSB = new StringBuilder();
			StringBuilder makeSB = new StringBuilder();
			StringBuilder serNumSB = new StringBuilder();
			String driver = "";
			String licNum = "";
			if (end.getDataExtension() != null) {
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF28A_AutoNum")
								&& extData.getColumnValue() != null) {
							if (autoNumSB.length() > 0) {
								autoNumSB.append(ControlChar.LINE_BREAK);
							}
							autoNumSB.append(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_ModYear")
								&& extData.getColumnValue() != null) {
							if (modYearSB.length() > 0) {
								modYearSB.append(ControlChar.LINE_BREAK);
							}
							modYearSB.append(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_Make")
								&& extData.getColumnValue() != null) {
							if (makeSB.length() > 0) {
								makeSB.append(ControlChar.LINE_BREAK);
							}
							makeSB.append(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_SerNum")
								&& extData.getColumnValue() != null) {
							if (serNumSB.length() > 0) {
								serNumSB.append(ControlChar.LINE_BREAK);
							}
							serNumSB.append(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_Driver")
								&& extData.getColumnValue() != null) {
							driver = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_LicNum")
								&& extData.getColumnValue() != null) {
							licNum = extData.getColumnValue();
						}
					}
				}
			}

			Section new28ASection = (Section) tmpSection.deepClone(true);
			Table table = new28ASection.getBody().getTables().get(1);
			Row row = table.getRows().get(1);

			// Automobile #
			row.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run cellRun = new Run(doc, autoNumSB.toString());
			cellRun.getFont().setName("Arial");
			cellRun.getFont().setSize(8);
			row.getCells().get(0).getFirstParagraph().appendChild(cellRun);

			// Model Year
			row.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run cellRun1 = new Run(doc, modYearSB.toString());
			cellRun1.getFont().setName("Arial");
			cellRun1.getFont().setSize(8);
			row.getCells().get(1).getFirstParagraph().appendChild(cellRun1);

			// Make
			row.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run cellRun2 = new Run(doc, makeSB.toString());
			cellRun2.getFont().setName("Arial");
			cellRun2.getFont().setSize(8);
			row.getCells().get(2).getFirstParagraph().appendChild(cellRun2);

			// Serial # / VIN
			row.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run cellRun3 = new Run(doc, serNumSB.toString());
			cellRun3.getFont().setName("Arial");
			cellRun3.getFont().setSize(8);
			row.getCells().get(3).getFirstParagraph().appendChild(cellRun3);

			// Paragraph
			Paragraph para = new28ASection.getBody().getParagraphs().get(21);
			para.removeAllChildren();

			Run paraRun = new Run(doc, "Name of Excluded Driver: ");
			paraRun.getFont().setName("Arial");
			paraRun.getFont().setSize(8);

			Run paraRun1 = new Run(doc, driver);
			paraRun1.getFont().setName("Arial");
			paraRun1.getFont().setSize(8);
			paraRun1.getFont().setBold(true);

			Run paraRun2 = new Run(doc, "      Driver's Licence # ");
			paraRun2.getFont().setName("Arial");
			paraRun2.getFont().setSize(8);

			Run paraRun3 = new Run(doc, licNum);
			paraRun3.getFont().setName("Arial");
			paraRun3.getFont().setSize(8);
			paraRun3.getFont().setBold(true);

			para.appendChild(paraRun);
			para.appendChild(paraRun1);
			para.appendChild(paraRun2);
			para.appendChild(paraRun3);

			doc.insertAfter(new28ASection, refSection);
			refSection = new28ASection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF31Section(Document doc) throws Exception {
		Section tmpSection = opcf31Sec;
		Section refSection = tmpSection;

		List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();

		for (SpecialtyVehicleRisk vechicleRisk : vehicleRisks) {
			for (Endorsement end : vechicleRisk.getRiskEndorsements()) {
				if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
					StringBuilder opcf31VehDescSB = new StringBuilder();
					String limit = numberFormat(end.getLimit1amount());
					String otparty = "";
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OPCF31_OtParty")
										&& extData.getColumnValue() != null) {
									otparty = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OPCF31_VehDesc")
										&& extData.getColumnValue() != null && !fleetBasis.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)) {
									if (opcf31VehDescSB.length() > 0) {
										opcf31VehDescSB.append(", ");
									}
									opcf31VehDescSB.append(extData.getColumnValue());
								}
							}
						}
					}

					if (fleetBasis.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)) {
						opcf31VehDescSB.append(numberFormat(vechicleRisk.getVehicleSequence()));
					}

					Section new31Section = (Section) tmpSection.deepClone(true);

					// OPCF31_Limit
					new31Section.getBody().getParagraphs().get(5).getRange().replace("{{OPCF31_Limit}}", limit);

					// OPCF31_OTParty
					new31Section.getBody().getParagraphs().get(5).getRange().replace("{{OPCF31_OTParty}}", otparty);

					// OPCF31_VehDesc
					new31Section.getBody().getParagraphs().get(7).getRange().replace("{{OPCF31_VehDesc}}",
							opcf31VehDescSB.toString());

					doc.insertAfter(new31Section, refSection);
					refSection = new31Section;
				}
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF38Section(Document doc) throws Exception {
		Section tmpSection = opcf38Sec;
		Section refSection = tmpSection;

		Iterator<Endorsement> it = opcf38Ends.iterator();
		while (it.hasNext()) {
			Endorsement end = it.next();
			String limit = numberFormat(end.getLimit1amount());
			String prem = premFormat(end.getEndorsementPremium().getNetPremiumChange());
			String totLimit = numberFormat(end.getLimit1amount());
			String totPrem = premFormat(end.getEndorsementPremium().getNetPremiumChange());
			String desc = "";
			if (end.getDataExtension() != null) {
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF38_EqDesc")
								&& extData.getColumnValue() != null) {
							desc = extData.getColumnValue();
						}
					}
				}
			}

			Section new38Section = (Section) tmpSection.deepClone(true);
			Table table = new38Section.getBody().getTables().get(2);
			Row row1 = table.getRows().get(1);
			Cell cell1_0 = row1.getCells().get(0);
			Cell cell1_1 = row1.getCells().get(1);
			Cell cell1_2 = row1.getCells().get(2);
			Row row4 = table.getRows().get(4);
			Cell cell4_1 = row4.getCells().get(1);
			Cell cell4_2 = row4.getCells().get(2);

			// OPCF38_Desc
			cell1_0.getFirstParagraph().getRange().replace("{{OPCF38_Desc}}", desc);

			// OPCF38_Limit
			cell1_1.getFirstParagraph().getRange().replace("{{OPCF38_Limit}}", limit);

			// OPCF38_Prem
			cell1_2.getFirstParagraph().getRange().replace("{{OPCF38_Prem}}", prem);

			// OPCF38_TotLimit
			cell4_1.getFirstParagraph().getRange().replace("{{OPCF38_TotLimit}}", totLimit);

			// OPCF38_TotPrem
			cell4_2.getFirstParagraph().getRange().replace("{{OPCF38_TotPrem}}", totPrem);

			doc.insertAfter(new38Section, refSection);
			refSection = new38Section;
		}
		doc.removeChild(tmpSection);
	}

	protected void setOEF72Table(Document doc) {
		Section sec = oef72Sec;
		Table table = sec.getBody().getTables().get(1);
		Row firstRow = table.getRows().get(1);
		for (HashMap<String, String> alteration : oef72AlterationsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Alteration Number
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, alteration.get("altNum"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Change Description
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, alteration.get("changeDesc"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setOEF82FirstTable(Document doc) {
		Section sec = oef82Sec;
		Table table = sec.getBody().getTables().get(1);
		Row firstRow = table.getRows().get(1);
		for (HashMap<String, String> person : oef82PersonsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Number
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, person.get("number"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Name
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, person.get("name"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Relationship
			newRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, person.get("relation"));
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setEON102Section(Document doc) {
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = eon102Sec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyAuto.getAdditionalInsureds()) {
			String transType = policyDocument.getPolicyTransaction().getTransactionType();
			String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
			String businessStatus = addIns.getBusinessStatus();
			String systemStatus = addIns.getSystemStatus();

			if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
					&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
					&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
				Section addInsSection = (Section) tmpSection.deepClone(true);

				// Set Additional Insured
				Run nameRun = new Run(doc, addIns.getAdditionalInsuredName());
				nameRun.getFont().setName("Arial");
				nameRun.getFont().setSize(8);
				nameRun.getFont().setBold(true);
				addInsSection.getBody().getParagraphs().get(2).removeAllChildren();
				addInsSection.getBody().getParagraphs().get(2).appendChild(nameRun);

				Run streetRun = new Run(doc, addIns.getAdditionalInsuredAddress().getAddressLine1() + ", "
						+ addIns.getAdditionalInsuredAddress().getCity());
				streetRun.getFont().setName("Arial");
				streetRun.getFont().setSize(8);
				streetRun.getFont().setBold(true);
				addInsSection.getBody().getParagraphs().get(3).removeAllChildren();
				addInsSection.getBody().getParagraphs().get(3).appendChild(streetRun);

				Run pcRun = new Run(doc, addIns.getAdditionalInsuredAddress().getProvState() + " "
						+ addIns.getAdditionalInsuredAddress().getPostalZip());
				pcRun.getFont().setName("Arial");
				pcRun.getFont().setSize(8);
				pcRun.getFont().setBold(true);
				addInsSection.getBody().getParagraphs().get(4).removeAllChildren();
				addInsSection.getBody().getParagraphs().get(4).appendChild(pcRun);

				Table tmpTable = addInsSection.getBody().getTables().get(1);
				Row firstVehicleRow = tmpTable.getRows().get(1);

				for (AdditionalInsuredVehicles addInsVeh : subPolicyAuto.getAdditionalInsuredVehicles()) {
					if (addInsVeh.getAdditionalInsured().getAdditionalInsuredPK()
							.equals(addIns.getAdditionalInsuredPK())) {
						Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

						// Veh_Desc
						newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
						Run newRun1 = new Run(doc, addInsVeh.getDescription());
						newRun1.getFont().setName("Arial");
						newRun1.getFont().setSize(8);
						newRun1.getFont().setBold(true);
						newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

						// Veh_VIN
						newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
						Run newRun2 = new Run(doc, addInsVeh.getSerialNum());
						newRun2.getFont().setName("Arial");
						newRun2.getFont().setSize(8);
						newRun2.getFont().setBold(true);
						newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

						tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
						firstVehicleRow = newVehicleRow;
					}
				}

				tmpTable.removeChild(tmpTable.getRows().get(1));
				doc.insertAfter(addInsSection, refSection);
				refSection = addInsSection;
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setDOCCSection(Document doc) throws Exception {
		Section tmpSection = doccSec;
		Section refSection = tmpSection;

		for(HashMap<String, String> docc : doccTypes) {
			Section doccSection = (Section) tmpSection.deepClone(true);
			Paragraph premPara = doccSection.getBody().getTables().get(0).getRows().get(2).getCells().get(4).getFirstParagraph();
			premPara.getRange().replace("{{DOCC_Prem}}", docc.get("prem"));
			doccSection.getBody().getParagraphs().get(1).getRange().replace("{{DOCC_Type}}", docc.get("type"));

			doc.insertAfter(doccSection, refSection);
			refSection = doccSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setGECGLMTCVehicles(Document doc) {
		Paragraph para = gecglmtcSec.getBody().getParagraphs().get(3);
		StringBuilder vehiclesSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> vehicle : gecglmtc_vehicles) {
			if (vehiclesSB.length() > 0) {
				vehiclesSB.append(", ");
			} else {
				vehiclesSB.append("Vehicle Description: ");
			}
			vehiclesSB.append(vehicle.get("veh_desc") + " s/n " + vehicle.get("ser_num"));
		}

		Run newRun = new Run(doc, vehiclesSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setGECGLVehicles(Document doc) {
		Paragraph para = gecglSec.getBody().getParagraphs().get(6);
		StringBuilder vehiclesSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> vehicle : gecgl_vehicles) {
			if (vehiclesSB.length() > 0) {
				vehiclesSB.append(", ");
			} else {
				vehiclesSB.append("Vehicle Description: ");
			}
			vehiclesSB.append(vehicle.get("veh_desc") + " s/n " + vehicle.get("ser_num"));
		}

		Run newRun = new Run(doc, vehiclesSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setALOCCGLSection(Document doc) {
		Paragraph para = aloccglSec.getBody().getParagraphs().get(2);
		StringBuilder locationsSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> location : aloccgl_locations) {
			if (locationsSB.length() > 0) {
				locationsSB.append(", ");
			}

			locationsSB.append(location.get("street") + ", " + location.get("city") + " " + location.get("prov_state") + " " + location.get("pc"));
		}

		Run newRun = new Run(doc, locationsSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setDLOCCGLSection(Document doc) {
		Paragraph para = dloccglSec.getBody().getParagraphs().get(2);
		StringBuilder locationsSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> location : dloccgl_locations) {
			if (locationsSB.length() > 0) {
				locationsSB.append(", ");
			}

			locationsSB.append(location.get("street") + ", " + location.get("city") + " " + location.get("prov_state") + " " + location.get("pc"));
		}

		Run newRun = new Run(doc, locationsSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setALOCCSection(Document doc) {
		Paragraph para = aloccSec.getBody().getParagraphs().get(2);
		StringBuilder locationsSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> location : alocc_locations) {
			if (locationsSB.length() > 0) {
				locationsSB.append(", ");
			}

			locationsSB.append(location.get("street") + ", " + location.get("city") + " " + location.get("prov_state") + " " + location.get("pc"));
		}

		Run newRun = new Run(doc, locationsSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setDLOCCSection(Document doc) {
		Paragraph para = dloccSec.getBody().getParagraphs().get(3);
		StringBuilder locationsSB = new StringBuilder();

		para.removeAllChildren();
		for (HashMap<String, String> location : dlocc_locations) {
			if (locationsSB.length() > 0) {
				locationsSB.append(", ");
			}

			locationsSB.append(location.get("street") + ", " + location.get("city") + " " + location.get("prov_state") + " " + location.get("pc"));
		}

		Run newRun = new Run(doc, locationsSB.toString());
		newRun.getFont().setName("Arial");
		newRun.getFont().setSize(8);
		newRun.getFont().setBold(true);
		para.appendChild(newRun);
	}

	protected void setAICSection(Document doc) throws Exception {
		CGLSubPolicy<Risk> subPolicyCGL = (CGLSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		Section tmpSection = aicSec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyCGL.getAdditionalInsureds()) {
			String transType = policyDocument.getPolicyTransaction().getTransactionType();
			String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
			String businessStatus = addIns.getBusinessStatus();
			String systemStatus = addIns.getSystemStatus();

			if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
					&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
					&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
				Section addInsSection = (Section) tmpSection.deepClone(true);

				// Set Additional Insured
				// Paragraph 2
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				// Paragraph 3
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				// Paragraph 5
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				doc.insertAfter(addInsSection, refSection);
				refSection = addInsSection;
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setAICCSection(Document doc) throws Exception {
		CargoSubPolicy<Risk> subPolicyCGO = (CargoSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
		Section tmpSection = aiccSec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyCGO.getAdditionalInsureds()) {
			String transType = policyDocument.getPolicyTransaction().getTransactionType();
			String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
			String businessStatus = addIns.getBusinessStatus();
			String systemStatus = addIns.getSystemStatus();

			if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
					&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
					&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
				Section addInsSection = (Section) tmpSection.deepClone(true);

				// Set Additional Insured
				// Paragraph 2
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				// Paragraph 3
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				// Paragraph 5
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
				addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

				doc.insertAfter(addInsSection, refSection);
				refSection = addInsSection;
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setEON109Section(Document doc) throws Exception {
		Section tmpSection = eon109Sec;
		Section refSection = tmpSection;
		for (Endorsement end : eon109Ends) {
			Section eon109Section = (Section) tmpSection.deepClone(true);
			String lessorName = "";
			String lessorAddr = "";
			List<String> descs = new ArrayList<String>();
			List<String> vins = new ArrayList<String>();

			if (end.getDataExtension() != null) {
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON109_Lessor_Name")
								&& extData.getColumnValue() != null) {
							lessorName = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_Lessor_Addr")
								&& extData.getColumnValue() != null) {
							lessorAddr = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_Veh_Desc")
								&& extData.getColumnValue() != null) {
							descs.add(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_VIN")
								&& extData.getColumnValue() != null) {
							vins.add(extData.getColumnValue());
						}
					}
				}
			}

			// Set name and address
			eon109Section.getBody().getParagraphs().get(1).getRange().replace("{{EON109_Lessor_Name}}", lessorName);
			eon109Section.getBody().getParagraphs().get(1).getRange().replace("{{EON109_Lessor_Addr}}", lessorAddr);

			Table table = eon109Section.getBody().getTables().get(1);
			Row firstRow = table.getRows().get(1);
			for (int i = 0; i < descs.size(); i++) {
				Row newRow = (Row) firstRow.deepClone(true);

				// Vehicle Desc
				newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
				Run newRun0 = new Run(doc, descs.get(i));
				newRun0.getFont().setName("Arial");
				newRun0.getFont().setSize(8);
				newRun0.getFont().setBold(true);
				newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

				// VIN
				newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
				Run newRun1 = new Run(doc, vins.get(i));
				newRun1.getFont().setName("Arial");
				newRun1.getFont().setSize(8);
				newRun1.getFont().setBold(true);
				newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

				table.insertAfter(newRow, firstRow);
				firstRow = newRow;
			}
			table.removeChild(table.getRows().get(1));
			doc.insertAfter(eon109Section, refSection);
			refSection = eon109Section;
		}
		doc.removeChild(tmpSection);
	}

	private void initializeSections(Document doc) {
	}

	private void hideSections(Document doc) {
	}

	protected boolean isValidEndorsement(Endorsement end) {
		boolean isValid = false;
		String transType = this.policyDocument.getPolicyTransaction().getTransactionType();
		String txnType = this.policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
		String businessStatus = end.getBusinessStatus();
		String systemStatus = end.getSystemStatus();

		// New Endorsement scenario
		if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
				&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
				&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
			return true;
		}

		// OPCF5 scenario
		if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
			SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument
					.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

			for (LienholderLessorSubPolicy lessorSubPolicy : subPolicyAuto.getLienholderLessors()) {
				if (lessorSubPolicy.getLienLessorType().equalsIgnoreCase(SpecialtyAutoConstants.LIENLESSORTYPE_HLESSOR)) {
					LienholderLessor lessor = lessorSubPolicy.getLienholderLessor();

					String opcf5LessorBusinessStatus = lessorSubPolicy.getBusinessStatus();
					String opcf5LessorSystemStatus = lessorSubPolicy.getSystemStatus();

					if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& opcf5LessorBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
							&& (opcf5LessorSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
									|| opcf5LessorSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
						return true;
					} else {
						List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto
								.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
						vehicleDetails.sort(Comparator.comparingInt(lhl -> {
							if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
								return Integer.parseInt(lhl.getVehicleNum());
							} else {
								return 0;
							}
						}));

						for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
							if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
									.equals(lessor.getLienholderLessorPK())) {
								String opcf5VehicleBusinessStatus = vehicleDetail.getBusinessStatus();
								String opcf5VehicleSystemStatus = vehicleDetail.getSystemStatus();

								if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
										&& opcf5VehicleBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
										&& (opcf5VehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
												|| opcf5VehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
									return true;
								}
							}
						}

					}
				}
			}
		}

		// OPCF23A scenario
		if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
			SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument
					.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

			for (LienholderLessorSubPolicy lienholderSubPolicy : subPolicyAuto.getLienholderLessors()) {
				if (lienholderSubPolicy.getLienLessorType().equalsIgnoreCase(SpecialtyAutoConstants.LIENLESSORTYPE_HOLDER)) {
					LienholderLessor lienHolder = lienholderSubPolicy.getLienholderLessor();

					String opcf23ALienholderBusinessStatus = lienholderSubPolicy.getBusinessStatus();
					String opcf23ALienholderSystemStatus = lienholderSubPolicy.getSystemStatus();

					if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& opcf23ALienholderBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
							&& (opcf23ALienholderSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
									|| opcf23ALienholderSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
						return true;
					} else {
						List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto
								.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
						vehicleDetails.sort(Comparator.comparingInt(lhl -> {
							if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
								return Integer.parseInt(lhl.getVehicleNum());
							} else {
								return 0;
							}
						}));

						for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
							if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
									.equals(lienHolder.getLienholderLessorPK())) {
								String opcf23AVehicleBusinessStatus = vehicleDetail.getBusinessStatus();
								String opcf23AVehicleSystemStatus = vehicleDetail.getSystemStatus();

								if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
										&& opcf23AVehicleBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
										&& (opcf23AVehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
												|| opcf23AVehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
									return true;
								}
							}
						}

					}
				}
			}
		}

		return isValid;
	}

	protected boolean hasNewVehicle(Long lienholderLessorPK) {
		boolean hasNewVehicle = false;
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto.getLienholderLessorVehicleDetails().stream()
				.collect(Collectors.toList());
		vehicleDetails.sort(Comparator.comparingInt(lhl -> {
			if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
				return Integer.parseInt(lhl.getVehicleNum());
			} else {
				return 0;
			}
		}));
		String transType = this.policyDocument.getPolicyTransaction().getTransactionType();
		String txnType = this.policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

		for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
			if (vehicleDetail.getLienholderLessor().getLienholderLessorPK().equals(lienholderLessorPK)) {
				String opcf23AVehicleBusinessStatus = vehicleDetail.getBusinessStatus();
				String opcf23AVehicleSystemStatus = vehicleDetail.getSystemStatus();

				if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET) && txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
						&& opcf23AVehicleBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
						&& (opcf23AVehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
								|| opcf23AVehicleSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING))) {
					return true;
				}
			}
		}
		return hasNewVehicle;
	}
	
	protected boolean hasVehicleClassChanged(PolicyTransaction<?> offsetTxn, SpecialtyVehicleRisk vehiclerisk) {
		boolean haschanged = false;
		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) offsetTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (subPolicy != null && subPolicy.getSubPolicyRisks() != null) {
			Set<Risk> risks = subPolicy.getSubPolicyRisks();
			for (Risk risk : risks) {
				if (risk.getRiskType().equalsIgnoreCase("AU") && risk.getOriginatingPK().equals(vehiclerisk.getOriginatingPK())) {
					SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
					haschanged = !vehicle.getVehicleClass().equals(vehiclerisk.getVehicleClass());
					break;
				}
			}
		}

		return haschanged;
	}

	protected void endorsementNumeration(Document doc) {
		Integer sectionNo = maxEndNo > 0 ? maxEndNo : getMaxEndorsementNumber();
		for (Section sec : doc.getSections()) {
			boolean isEndSection = false;
			if (sec.getBody().getTables() != null && sec.getBody().getTables().get(0) != null
					&& sec.getBody().getTables().get(0).getRows() != null
					&& sec.getBody().getTables().get(0).getRows().get(1) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText() != null
					&& !sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText().replaceAll("[^a-zA-Z0-9]", "").isEmpty()) {
				isEndSection = true;
			}

			if (isEndSection) {
				sectionNo++;
				Table table = sec.getBody().getTables().get(0);
				Row row = table.getRows().get(1);
				Cell cell = row.getCells().get(4);
				String endCode = cell.getFirstParagraph().getText();
				endCode = endCode.replaceAll("[^a-zA-Z0-9]", "");

				for (Endorsement end : numEnds) {
					if (endCode.equalsIgnoreCase(end.getEndorsementCd())) {
						end.setEndorsementNumber(sectionNo);
					}
				}

				cell.getFirstParagraph().removeAllChildren();
				Run run = new Run(doc, sectionNo.toString());
				run.getFont().setName("Arial");
				run.getFont().setSize(8);
				run.getFont().setBold(true);
				cell.getFirstParagraph().appendChild(run);
			}
		}

		/*for (Endorsement end : numEnds) {
			System.out.println(end.getEndorsementCd() + " = " + end.getEndorsementNumber());
		}*/
	}

	protected Integer getMaxEndorsementNumber() {

		Integer rv = null;

		Set<Integer> endNumbers = new HashSet<Integer>();
		SubPolicy<?> autoSP = policyDocument.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null) {
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementNumber() != null) {
					if(!isValidEndorsement(end)) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null) {
						if(!isValidEndorsement(end)) {
							endNumbers.add(end.getEndorsementNumber());
						}
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null) {
						if(!isValidEndorsement(end)) {
							endNumbers.add(end.getEndorsementNumber());
						}
					}
				}
			}
		}
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementNumber() != null) {
				if(!isValidEndorsement(end)) {
					endNumbers.add(end.getEndorsementNumber());
				}
			}
		}
		rv = (!endNumbers.isEmpty() ? Collections.max(endNumbers) : 0);
		return rv;
	}

}
