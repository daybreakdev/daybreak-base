package com.echelon.services.document;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Document;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.entities.Location;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyPackageTowing extends PolicyPackage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4614949501322170971L;

	public PolicyPackageTowing(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage)insurancePolicy;
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		
		this.policyDocument = policyDocument;
		this.docDate = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "MMMM dd, yyyy");
		this.date = formatDate(LocalDate.now(), "MMMM dd, yyyy");
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressProvState = insurancePolicy.getInsuredAddress().getProvState();
		this.insuredAddressPC = insurancePolicy.getInsuredAddress().getPostalZip();
		this.contractNo = autoPackage.getAutoAssocMemberNum(); 
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.brokerContact = insurancePolicy.getProducerContactName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		
		for (Location loc : insurancePolicy.getPolicyProducer().getLocations()) {
			if (loc.getCompany().getCompanyPK().equals(insurancePolicy.getPolicyProducer().getCompanyPK())) {
				this.broker_addr = loc.getAddressLine1();
				this.broker_city = loc.getCity();
				this.broker_provState = loc.getProvState();
				this.broker_pc = loc.getPostalZip();
			}
		}
		
		this.effectiveDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"M/dd/yyyy");
		this.expiryDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"M/dd/yyyy");
		this.isLaterThanJan2021 = policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().compareTo(JAN2021) >= 0;
		this.issue_date = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "M/dd/yyyy");
		this.lbLimit = numberFormat(autoPackage.getLiabilityLimit());
		this.effYear = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"yyyy");
		this.effMonth = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"MM");
		this.effDay = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"dd");
		this.expYear = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"yyyy");
		this.expMonth = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"MM");
		this.expDay = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"dd");
		this.prepDate = formatDate(policyDocument.getCreatedDate().toLocalDate(), "MM/dd/yyyy");
		
		// Sub Policies
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		for (SubPolicy<Risk> subPolicy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
				subPolicyCGL = (CGLSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
				subPolicyCGO = (CargoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPolicy;
			}
		}
		
		LinkedHashSet<Endorsement> endDescs = new LinkedHashSet<Endorsement>();
		
		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			endDescs.add(end);
			
			if (end.getEndorsementCd().equalsIgnoreCase("EON110")) {
				this.showEON110Section = true;
				this.numEnds.add(end);
				this.eon110_no = numberFormat(end.getEndorsementNumber());
				this.per_occurrence_ded = end.getDeductible1amount() != null ? numberFormat(end.getDeductible1amount()) : "N/A";
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON111")) {
				this.showEON111Section = true;
				this.numEnds.add(end);
				this.eon111_effective_date = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
						"MMMM dd, yyyy");
				this.eon111_expiry_date = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
						"MMMM dd, yyyy");
			} else if (end.getEndorsementCd().equalsIgnoreCase("IA")) {
				this.showIASection = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON112")) {
				this.showEON112Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON116")) {
				this.showEON116Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON117")) {
				this.showEON117Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				this.showEON121Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("DOC")) {
				this.showDOCSection = true;
				this.numEnds.add(end);
			}
		}
		
		//Sub Policy Auto
		this.vehPrem = numberFormat(subPolicyAuto.getSubPolicyPremium().getWrittenPremium());
		LinkedHashSet<Endorsement> autoEndorsements = new LinkedHashSet<Endorsement>();
		for(Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			autoEndorsements.add(end);
		}
		
		fleetBasis = subPolicyAuto.getFleetBasis();
		if(fleetBasis.startsWith("21B")) {
			this.sec7Ded = "As per OPCF 21B";
			this.vehDesc = "All vehicles owned, leased to, registered, licensed and operated by and on behalf of the named insured";
			this.dcpd_ded = "As per OPCF 21B";
		} else if (fleetBasis.startsWith("21A")) {
			this.sec7Ded = "As per OPCF 21A";
			this.vehDesc = "All vehicles owned, leased to, registered, licensed and operated by and on behalf of the named insured";
			this.dcpd_ded = "As per OPCF 21A";
		} else if (fleetBasis.equalsIgnoreCase("SCHD")) {
			this.sec7Ded = "See Schedule of vehicles attached";
			this.vehDesc = "See Schedule of vehicles";
			this.dcpd_ded = "See Schedule of vehicles";
		}
		
		//Vehicle Risks
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		Double biSum = 0.0; 
		Double pdSum = 0.0;
		Double abSum = 0.0;
		Double uaSum = 0.0;
		Double dcpdSum = 0.0;
		Double spSum = 0.0;
		Double cmSum = 0.0;
		Double clSum = 0.0;
		Double apSum = 0.0;
		
		for(SpecialtyVehicleRisk vehicle : vehicles) {
			
			for (Coverage cove : vehicle.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("BI")) {
					biSum = cove.getCoveragePremium().getWrittenPremium() != null ? biSum + cove.getCoveragePremium().getWrittenPremium() : biSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("PD")) {
					pdSum = cove.getCoveragePremium().getWrittenPremium() != null ? pdSum + cove.getCoveragePremium().getWrittenPremium() : pdSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("AB")) {
					abSum = cove.getCoveragePremium().getWrittenPremium() != null ? abSum + cove.getCoveragePremium().getWrittenPremium() : abSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("UA")) {
					uaSum = cove.getCoveragePremium().getWrittenPremium() != null ? uaSum + cove.getCoveragePremium().getWrittenPremium() : uaSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					dcpdSum = cove.getCoveragePremium().getWrittenPremium() != null ? dcpdSum + cove.getCoveragePremium().getWrittenPremium() : dcpdSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP")) {
					spSum = cove.getCoveragePremium().getWrittenPremium() != null ? spSum + cove.getCoveragePremium().getWrittenPremium() : spSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM")) {
					cmSum = cove.getCoveragePremium().getWrittenPremium() != null ? cmSum + cove.getCoveragePremium().getWrittenPremium() : cmSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL")) {
					clSum = cove.getCoveragePremium().getWrittenPremium() != null ? clSum + cove.getCoveragePremium().getWrittenPremium() : clSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP")) {
					apSum = cove.getCoveragePremium().getWrittenPremium() != null ? apSum + cove.getCoveragePremium().getWrittenPremium() : apSum;
				}
			}
			
			Set<Endorsement> vehEndorsements = vehicle.getRiskEndorsements();
			for(Endorsement end : vehEndorsements) {
				autoEndorsements.add(end);
			}
		}
		this.biTotalPrem = numberFormat(biSum);
		this.pdTotalPrem = numberFormat(pdSum);
		this.abTotalPrem = numberFormat(abSum);
		this.uaTotalPrem = numberFormat(uaSum);
		this.dcpdTotalPrem = numberFormat(dcpdSum);
		this.spTotalPrem = numberFormat(spSum);
		this.compTotalPrem = numberFormat(cmSum);
		this.collTotalPrem = numberFormat(clSum);
		this.apTotalPrem = numberFormat(apSum);
		
		//Endorsements
		LinkedHashSet<String> opcf27BVehTypes = new LinkedHashSet<String>();
		List<Endorsement> autoEndList = new ArrayList<Endorsement>();
		Iterator<Endorsement> itAutoEnd = autoEndorsements.iterator();
		StringBuilder opcf2NameSB = new StringBuilder();
		StringBuilder opcf2RelationSB = new StringBuilder();
		StringBuilder eon114NameInsSB = new StringBuilder();
		StringBuilder gdpcDealerPlateNoSB = new StringBuilder();
		StringBuilder gdpcExpDateSB = new StringBuilder();
		
		while(itAutoEnd.hasNext()) {
			Endorsement end = itAutoEnd.next();
			endDescs.add(end);
			
			if (end.getEndorsementCd().equalsIgnoreCase("OPCF2")) {
				this.showOPCF2Section = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF2_Pers") && extData.getColumnValue() != null) {
							if (opcf2NameSB.length() > 0) {
								opcf2NameSB.append("\n");
							}
							opcf2NameSB.append("   " + extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF2_Relation")
								&& extData.getColumnValue() != null) {
							if (opcf2RelationSB.length() > 0) {
								opcf2RelationSB.append("\n");
							}
							opcf2RelationSB.append("   " + extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
				this.showOPCF5Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5Bl")) {
				this.showOPCF5BlSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF8")) {
				this.showOPCF8Section = true;
				if (this.opcf8_ded.equals("N/A")) {
					this.opcf8_ded = numberFormat(end.getDeductible1amount());
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF9")) {
				this.showOPCF9Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF13C")) {
				this.showOPCF13CSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
				this.showOPCF19Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF20")) {
				this.opcf20_exists = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21A")) {
				this.showOPCF21ASection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21B")) {
				this.showOPCF21BSection = true;
				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BQ")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BS")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BA")) {
					this.opcf21B_prorata = true;
				}

				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21B5")) {
					this.opcf21B_5050 = true;
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
				this.showOPCF23ASection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_DCPD") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_dcpd_ck = true;
							this.opcf23A_dcpd_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_SP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_sp_ck = true;
							this.opcf23A_sp_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CL") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_cl_ck = true;
							this.opcf23A_cl_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CM") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_cm_ck = true;
							this.opcf23A_cm_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_AP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_ap_ck = true;
							this.opcf23A_ap_ded = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23ABl")) {
				this.showOPCF23ABlSection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_DCPD") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_dcpd_ck = true;
							this.opcf23ABL_dcpd_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_SP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_sp_ck = true;
							this.opcf23ABL_sp_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CL") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_cl_ck = true;
							this.opcf23ABL_cl_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CM") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_cm_ck = true;
							this.opcf23ABL_cm_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_AP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_ap_ck = true;
							this.opcf23ABL_ap_ded = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25A")) {
				this.showOPCF25ASection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF25A_TermDay")
								&& extData.getColumnValue() != null) {
							this.termination_day = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27")) {
				this.showOPCF27Section = true;
				this.opcf27_apded = numberFormat(end.getDeductible1amount());
				this.opcf27_limit = numberFormat(end.getLimit1amount());
				List<String> pers = new ArrayList<String>();
				List<String> relation = new ArrayList<String>();
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27_Pers") && extData.getColumnValue() != null) {
							pers.add(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF27_Relation")
								&& extData.getColumnValue() != null) {
							relation.add(extData.getColumnValue());
						}
					}
				}

				for (int i = 0; i < pers.size(); i++) {
					HashMap<String, String> person = new HashMap<String, String>();
					person.put("pers", pers.get(i));
					person.put("relation", relation.get(i));
					opcf27PersonsTable.add(person);
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) {
				this.showOPCF27BSection = true;

				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")
								&& extData.getColumnValue() != null) {
							if (!opcf27BVehTypes.contains(extData.getColumnValue())) {
								opcf27BVehTypes.add(extData.getColumnValue());
								this.opcf27BEnds.add(end);
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28")) {
				this.showOPCF28Section = true;
				this.opcf28_limit = numberFormat(end.getLimit1amount());
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF28_Pers")
								&& extData.getColumnValue() != null) {
							this.opcf28_pers = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_Coll")
								&& extData.getColumnValue() != null) {
							if(extData.getColumnValue().equalsIgnoreCase("Insured")) {
								this.opcf28_coll = true;
								this.opcf28CL_ded = numberFormat(end.getDeductible1amount());
							} else {
								this.opcf28_collNot = true;
							}
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_AllPeril")
								&& extData.getColumnValue() != null) {
							if(extData.getColumnValue().equalsIgnoreCase("Insured")) {
								this.opcf28_allperil = true;
								this.opcf28AP_ded = numberFormat(end.getDeductible1amount());
							} else {
								this.opcf28_allperilNot = true;
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28A")) {
				this.showOPCF28ASection = true;
				this.opcf28AEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF30")) {
				this.showOPCF30Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
				this.showOPCF31Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF38")) {
				this.showOPCF38Section = true;
				this.opcf38Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF40")) {
				this.showOPCF40Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF43")) {
				this.showOPCF43Section = true;
				this.showOPCF43ASection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) {
				this.showOPCF44RSection = true;
				Integer limit = end.getLimit1amount() != null ? end.getLimit1amount() : 0;
				if (autoPackage.getLiabilityLimit() > 2000000) {
					this.opcf44RPrintLimit = "(Maximum limit: $2,000,000)";
				} else {
					this.opcf44RPrintLimit = "(limit: $" + numberFormat(limit) + ")";
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF47")) {
				this.showOPCF47Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF48")) {
				this.showOPCF48Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON114")) {
				this.showEON114Section = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON114_NamedIns")
								&& extData.getColumnValue() != null) {
							if (eon114NameInsSB.length() > 0) {
								eon114NameInsSB.append(" and/or\n");
							}
							eon114NameInsSB.append(extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON101")) {
				this.showEON101Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON102")) {
				this.showEON102Section = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON102_Sec7_Ded")
								&& extData.getColumnValue() != null) {
							if (extData.getColumnValue().startsWith("See")) {
								this.eon102_sec7_ded = extData.getColumnValue();
							} else {
								this.eon102_sec7_ded = "$" + extData.getColumnValue();
							}
						}
						
						if (extData.getColumnId().equalsIgnoreCase("EON102_DCPD_Ded")
								&& extData.getColumnValue() != null) {
							if (extData.getColumnValue().startsWith("See")) {
								this.eon102_ded = extData.getColumnValue();
							} else {
								this.eon102_ded = "$" + extData.getColumnValue();
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
				this.showEON20GSection = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON109")) {
				this.showEON109Section = true;
				this.numEnds.add(end);
				this.eon109Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("GDPC")) {
				this.showGDPCSection = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
								&& extData.getColumnValue() != null) {
							if(gdpcDealerPlateNoSB.length() > 0) {
								gdpcDealerPlateNoSB.append("\n");
							}
							gdpcDealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
								&& extData.getColumnValue() != null) {
							if(gdpcExpDateSB.length() > 0) {
								gdpcExpDateSB.append("\n");
							}
							gdpcExpDateSB.append("Expiry Date: " + extData.getColumnValue());
						}
					}
				}

			}
			
			if (end.getEndorsementCd().startsWith("OPCF")) {
				autoEndList.add(end);
			}
			
		}
		
		this.opcf2_name = opcf2NameSB.toString();
		this.opcf2_relationship = opcf2RelationSB.toString();
		this.eon114_name_ins = eon114NameInsSB.toString();
		this.gdpc_dealer_plate_no = gdpcDealerPlateNoSB.toString();
		this.gdpc_exp_date = gdpcExpDateSB.toString();
		this.endList1 = orderedEndList(autoEndList, lookupConfig);
		
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			LookupTableItem lookupBusinessDescItem = lookupConfig
					.getConfigLookup(ConfigConstants.LOOKUPTABLE_BusinessDescriptions).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getBusinessDescription()))
					.findFirst().orElse(null);
			this.businessType = lookupBusinessDescItem.getItemValue();
		}
		
		// Sub Policy CGL
		if (subPolicyCGL != null) {
			
			this.showCGLDecSection = true;
			this.cgl_lb_limit = numberFormat(subPolicyCGL.getCglLimit());
			this.cgl_ded = numberFormat(subPolicyCGL.getCglDeductible());
			
			// Coverages
			for (Coverage cove : subPolicyCGL.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("SPF6TPL")) {
					this.showSPF6Section = true;
					this.spf6_lb_limit = numberFormat(cove.getLimit1());
					this.sef96_lb_limit = "Incl.";
					this.sef99_lb_limit = "Incl.";
				} else if (cove.getCoverageCode().equalsIgnoreCase("TLL")) {
					this.tll_limit = numberFormat(cove.getLimit1());
					if (cove.getDeductible1() != null) {
						this.tll_ded = numberFormat(cove.getDeductible1());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("EBE")) {
					this.ebeExists = true;
					this.emp_benefits = "Employee Benefits Extension";
					this.ebe_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.ebe_limit = numberFormat(cove.getLimit1());
					if (cove.getDeductible1() != null) {
						this.ebe_ded = numberFormat(cove.getDeductible1());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("MEX")) {
					this.mex_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("BIPD")) {
					this.cgl_ge_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				}
			}
			
			//Endorsements
			for (Endorsement end : subPolicyCGL.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);

				if (end.getEndorsementCd().equalsIgnoreCase("SEF96")) {
					//this.sef96_lb_limit = numberFormat(end.getLimit1amount());
					this.showSEF96Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("SEF99")) {
					//this.sef99_lb_limit = numberFormat(end.getLimit1amount());
					this.showSEF99Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("GECGLMTC")) {
					this.showGECGLMTCSection = true;
					this.numEnds.add(end);
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GELC_ModType")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_mod_type = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GELC_VehDesc")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_veh_desc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GELC_SerNum")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_veh_serial_no = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GECGL")) {
					this.showGECGLSection = true;
					this.numEnds.add(end);
					this.gecgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GEL_ModType")
									&& extData.getColumnValue() != null) {
								this.gecgl_mod_type = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GEL_VehDesc")
									&& extData.getColumnValue() != null) {
								this.gecgl_veh_desc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GEL_SerNum")
									&& extData.getColumnValue() != null) {
								this.gecgl_veh_serial_no = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GLEE")) {
					this.showGLEESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("WOS")) {
					this.showWOSSection = true;
					this.numEnds.add(end);
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("WOS_CoName")
									&& extData.getColumnValue() != null) {
								this.wos_co_name = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("WOS_CoAddr")
									&& extData.getColumnValue() != null) {
								this.wos_co_addr = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AOCCGL")) {
					this.showAOCCGLSection = true;
					this.numEnds.add(end);
					this.aoccgl_prem = numberFormat(subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCCGL")) {
					this.showMOCCGLSection = true;
					this.numEnds.add(end);
					this.moccgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("AIC")) {
					this.showAICSection = true;
					this.numEnds.add(end);
					this.aic_limit = numberFormat(end.getLimit1amount());
					this.aic_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEL")) {
					this.showCDELSection = true;
					this.numEnds.add(end);
				}
				
			}
			
			StringBuilder locationSB = new StringBuilder();
			Set<PolicyLocation> policyLocations = subPolicyCGL.getCglLocations().stream().map(CGLLocation::getPolicyLocation).collect(Collectors.toSet());
			//for (PolicyLocation pl : subPolicyCGL.getCglLocations()) {
			for (PolicyLocation pl : policyLocations) {
				if (locationSB.length() > 0) {
					locationSB.append("\n");
				}

				locationSB.append(pl.getLocationAddress().getAddressLine1());
				locationSB.append(", ");
				locationSB.append(pl.getLocationAddress().getCity());
				locationSB.append(", Ontario ");
				locationSB.append(pl.getLocationAddress().getPostalZip());
			}
			this.cgl_locations = locationSB.toString();
			if(subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
				this.totalCGLPrem = numberFormat(subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
			} else {
				this.totalCGLPrem = "Incl.";
			}
			
		}
		
		// Sub Policy CGO
		if (subPolicyCGO != null) {
			
			this.showCGODecSection = true;
			this.cpe_limit = "10,000";
			this.cpe_ded = "1,000";
			this.cgo_limit = numberFormat(subPolicyCGO.getCargoLimit());
			this.cgo_ded = numberFormat(subPolicyCGO.getCargoDeductible());
			this.cargo = "Motor Truck Cargo";
			this.cargo_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());	
			
			StringBuilder locationSB = new StringBuilder();
			for (Coverage cove : subPolicyCGO.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("MTCC")) {
					this.cargoLimit = numberFormat(cove.getLimit1());
					this.cargo_ded = numberFormat(cove.getDeductible1());
					this.cargo_ge_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.cargoLimit1 = "Incl.";
					this.cargoLimit2 = "Incl.";
					this.cargoLimit3 = "10,000";
					this.cargoLimit4 = numberFormat(cove.getLimit1());
					this.cargoLimit5 = numberFormat(cove.getLimit1());

					this.cargoLimit1Dec = "Incl.";
					this.cargoLimit3Dec = "Incl.";
					this.cargoLimit4Dec = "10,000";
					this.cargoLimit5Dec = numberFormat(cove.getLimit1());
					this.cargoLimit6Dec = numberFormat(cove.getLimit1());
					this.showCDECDecRow = true;

					for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
						if (risk.getRiskType().equalsIgnoreCase("LO")) {
							for (Coverage coverage : risk.getCoverageDetail().getCoverages()) {
								if (coverage.getCoverageCode().equalsIgnoreCase("MTCC")) {
									PolicyLocation pl = (PolicyLocation) risk;

									if (locationSB.length() > 0) {
										locationSB.append("\n");
									}

									locationSB.append(pl.getLocationAddress().getAddressLine1());
									locationSB.append(", ");
									locationSB.append(pl.getLocationAddress().getCity());
									locationSB.append(" ON ");
									locationSB.append(pl.getLocationAddress().getPostalZip());
								}
							}
						}
					}

				}
			}
			
			for (Endorsement end : subPolicyCGO.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);
				
				if (end.getEndorsementCd().equalsIgnoreCase("CPEE")) {
					this.showCPEESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("AOCC")) {
					this.showAOCCSection = true;
					this.numEnds.add(end);
					this.aocc_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCC")) {
					this.showMOCCSection = true;
					this.numEnds.add(end);
					this.mocc_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEC")) {
					this.showCDECSection = true;
					this.numEnds.add(end);
				}  else if (end.getEndorsementCd().equalsIgnoreCase("TTC")) {
					this.showTTCSection = true;
					this.numEnds.add(end);
					this.ttc_limit = numberFormat(end.getLimit1amount());
					this.ttc_ded = numberFormat(end.getDeductible1amount());
					this.ttc_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("TTC_VehDesc")
									&& extData.getColumnValue() != null) {
								this.ttc_vehdesc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period1")
									&& extData.getColumnValue() != null) {
								this.ttc_period1 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period2")
									&& extData.getColumnValue() != null) {
								this.ttc_period2 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_SerNo")
									&& extData.getColumnValue() != null) {
								this.ttc_serno = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AICC")) {
					this.showAICCSection = true;
					this.numEnds.add(end);
					this.aicc_limit = numberFormat(end.getLimit1amount());
					this.aicc_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CTC")) {
					this.cargoLimit2Dec = numberFormat(end.getLimit1amount());
					this.ctc_limit = numberFormat(end.getLimit1amount());
				}
			}
			
			this.cargo_locations = locationSB.toString();
			if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
				this.totalCargoPrem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
			} else {
				this.totalCargoPrem = "Incl.";
			}
		}
		
		// Sub Policy GAR
		if (subPolicyGAR != null) {
			
			this.showGARDecSection = true;
			if(subPolicyGAR.getSubPolicyNamedInsured() != null && !subPolicyGAR.getSubPolicyNamedInsured().isEmpty()) {
				subPolicyNamedInsured = subPolicyGAR.getSubPolicyNamedInsured();
			} else {
				subPolicyNamedInsured = insuredName;
			}
			
			// Coverages
			Double bipd = 0.0;
			for (Coverage cove : subPolicyGAR.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("TPL")) {
					this.oap4_lb_limit = numberFormat(autoPackage.getLiabilityLimit());
				} else if (cove.getCoverageCode().equalsIgnoreCase("BI")) {
					this.oap4_bi_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					bipd = bipd + cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("PD")) {
					this.oap4_pd_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					bipd = bipd + cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("AB")) {
					this.oap4_ab_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					this.oap4_dcpd_ded = numberFormat(cove.getDeductible1());
					this.oap4_dcpd_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL511")) {
					this.cl5_ded = numberFormat(cove.getDeductible1());
					this.cl511_ded = numberFormat(cove.getDeductible1());
					this.gar511_ded = "$" + numberFormat(cove.getDeductible1());
					this.gar511_Prem = "$" + numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL61")) {
					this.oap4_cl_limit = numberFormat(cove.getLimit1());
					this.oap4_cl_ded = numberFormat(cove.getDeductible1());
					this.oap4_cl_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.cl6_lim = numberFormat(cove.getLimit1());
					this.cl6_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			this.oap4_bipd_prem = numberFormat(bipd);

			//Endorsements
			StringBuilder endCodeSB = new StringBuilder();
			StringBuilder oef76NameSB = new StringBuilder();
			StringBuilder oef76RelationshipSB = new StringBuilder();
			StringBuilder dealerPlateNoSB = new StringBuilder();
			StringBuilder dealerExpDateSB = new StringBuilder();
			StringBuilder servPlateNoSB = new StringBuilder();
			StringBuilder expDateSB = new StringBuilder();
			StringBuilder oef72EndCodeSB = new StringBuilder();
			
			for (Endorsement end : subPolicyGAR.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);
				
				if (end.getEndorsementCd().equalsIgnoreCase("OEF71")) {
					this.showOEF71Section = true;
					this.oef71_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
					this.showOEF72Section = true;
					this.oef72_prem = "Incl.";
					List<String> altNums = new ArrayList<String>();
					List<String> changeDescs = new ArrayList<String>();
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF72_AltNum")
									&& extData.getColumnValue() != null) {
								altNums.add(extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("OEF72_ChangeDesc")
									&& extData.getColumnValue() != null) {
								changeDescs.add(extData.getColumnValue());
							}
						}
					}
					
					for(int i = 0; i < altNums.size(); i++) {
						HashMap<String, String> alteration = new HashMap<String, String>();
						alteration.put("altNum", altNums.get(i));
						alteration.put("changeDesc", changeDescs.get(i));
						this.oef72AlterationsTable.add(alteration);
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF73")) {
					this.showOEF73Section = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF73_OtParty")
									&& extData.getColumnValue() != null) {
								this.oef73_other_party = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OEF73_Address")
									&& extData.getColumnValue() != null) {
								this.oef73_other_party_address = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF74")) {
					this.showOEF74Section = true;
					this.oef74_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF75")) {
					this.showOEF75Section = true;
					this.oef75_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF76")) {
					this.showOEF76Section = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF76_Name")
									&& extData.getColumnValue() != null) {
								if (oef76NameSB.length() > 0) {
									oef76NameSB.append("\n");
								}
								oef76NameSB.append(extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("OEF76_Relation")
									&& extData.getColumnValue() != null) {
								if (oef76RelationshipSB.length() > 0) {
									oef76RelationshipSB.append("\n");
								}
								oef76RelationshipSB.append(extData.getColumnValue());
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF77")) {
					this.showOEF77Section = true;
					this.oef77_limit = "Incl.";
					this.oef77_ded = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF78")) {
					this.showOEF78Section = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF78_Pers")
									&& extData.getColumnValue() != null) {
								this.oef78_pers = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OEF78_TPL")
									&& extData.getColumnValue() != null) {
								this.oef78_insuredind1 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OEF78_Coll5")
									&& extData.getColumnValue() != null) {
								this.oef78_insuredind2 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OEF78_Coll6")
									&& extData.getColumnValue() != null) {
								this.oef78_insuredind3 = extData.getColumnValue();
							}
						}
					}
					this.oef78_liab_limit = numberFormat(end.getLimit1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF78A")) {
					this.showOEF78ASection = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF78A_ExclDriv")
									&& extData.getColumnValue() != null) {
								this.oef78A_exclDriver = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OEF78A_LicNum")
									&& extData.getColumnValue() != null) {
								this.oef78A_driverLicNo = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF79")) {
					this.showOEF79Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF80")) {
					this.showOEF80Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF81")) {
					this.showOEF81Section = true;
					this.oef81_lim = numberFormat(end.getLimit1amount());
					this.oef81_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF82")) {
					this.showOEF82Section = true;
					this.oef82_lim = numberFormat(end.getLimit1amount());
					List<String> names = new ArrayList<String>();
					List<String> relations = new ArrayList<String>();
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF82_Name")
										&& extData.getColumnValue() != null) {
									names.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("OEF82_Relation")
										&& extData.getColumnValue() != null) {
									relations.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("511_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_511_ded = extData.getColumnValue();
									this.oef82_511_prem = "Incl.";
								} else if (extData.getColumnId().equalsIgnoreCase("512_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_512_ded = extData.getColumnValue();
									this.oef82_512_prem = "Incl.";
								} else if (extData.getColumnId().equalsIgnoreCase("513_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_513_ded = extData.getColumnValue();
									this.oef82_513_prem = "Incl.";
								}
							}
						}
					}

					for (int i = 0; i < names.size(); i++) {
						HashMap<String, String> person = new HashMap<String, String>();
						person.put("number", Integer.toString(i + 1) + ".");
						person.put("name", names.get(i));
						person.put("relation", relations.get(i));
						oef82PersonsTable.add(person);
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF83")) {
					this.showOEF83Section = true;
					this.oef83_limit = "Incl.";
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF83_NumOfVeh")
										&& extData.getColumnValue() != null) {
									this.oef83_numOfVeh = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF83_Coll5")
										&& extData.getColumnValue() != null) {
									this.oef83_coll5 = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF83_Coll6")
										&& extData.getColumnValue() != null) {
									this.oef83_coll6 = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF86")) {
					this.showOEF86Section = true;
					this.oef86_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF87")) {
					this.showOEF87Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("GSPC")) {
					this.showGSPCSection = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GSPC_PlateNo")
									&& extData.getColumnValue() != null) {
								if (servPlateNoSB.length() > 0) {
									servPlateNoSB.append("\n");
								}
								servPlateNoSB.append("Service Plate No.: " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("GSPC_ExpDate")
									&& extData.getColumnValue() != null) {
								if (expDateSB.length() > 0) {
									expDateSB.append("\n");
								}
								expDateSB.append("Expiry Date:  " + extData.getColumnValue());
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GDPCG")) {
					this.showGDPCGSection = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
									&& extData.getColumnValue() != null) {
								if (dealerPlateNoSB.length() > 0) {
									dealerPlateNoSB.append("\n");
								}
								dealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
									&& extData.getColumnValue() != null) {
								if (dealerExpDateSB.length() > 0) {
									dealerExpDateSB.append("\n");
								}
								dealerExpDateSB.append("Expiry Date:  " + extData.getColumnValue());
							}
						}
					}
				}
				
				
				if (end.getEndorsementCd().startsWith("OEF") && !end.getEndorsementCd().equalsIgnoreCase("OEF81")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					if (endCodeSB.length() > 0) {
						endCodeSB.append(", ");
					}
					if (lookupEndorsementCodeItem != null) {
						endCodeSB.append(lookupEndorsementCodeItem.getItemValue());
					} else {
						endCodeSB.append(end.getEndorsementCd());
					}

					if (!end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
						if (oef72EndCodeSB.length() > 0) {
							oef72EndCodeSB.append(", ");
						}
						if (lookupEndorsementCodeItem != null) {
							oef72EndCodeSB.append(lookupEndorsementCodeItem.getItemValue());
						} else {
							oef72EndCodeSB.append(end.getEndorsementCd());
						}
					}
				}
			}
			this.oef76_insured_name = oef76NameSB.toString();
			this.oef76_insured_relationship = oef76RelationshipSB.toString();
			this.gdpc_servPlateNo = dealerPlateNoSB.toString();
			this.gdpc_expDate = dealerExpDateSB.toString();
			this.gspc_servPlateNo = servPlateNoSB.toString();
			this.gspc_expDate = expDateSB.toString();
			int cm512Locations = 0;
			int sp513Locations = 0;
			int sp514Locations = 0;
			Double cm512_prem_total = 0.0;
			Double sp513_prem_total = 0.0;
			Double sp514_prem_total = 0.0;
			Double sp64_prem_total = 0.0;
			
			
			for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
				if (risk.getRiskType().equalsIgnoreCase("LO")) {
					PolicyLocation pl = (PolicyLocation) risk;
					Coverage coveCM512 = pl.getCoverageDetail().getCoverageByCode("CM512");
					Coverage coveSP513 = pl.getCoverageDetail().getCoverageByCode("SP513");
					Coverage coveSP514 = pl.getCoverageDetail().getCoverageByCode("SP514");
					Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
					
					if (coveCM512 != null) {
						this.cm512_limit = numberFormat(coveCM512.getLimit1());
						this.cm512_ded = numberFormat(coveCM512.getDeductible1());
						cm512Locations++;
					}

					if (coveSP513 != null) {
						this.sp513_limit = numberFormat(coveSP513.getLimit1());
						this.sp513_ded = numberFormat(coveSP513.getDeductible1());
						sp513Locations++;
					}

					if (coveSP514 != null) {
						this.sp514_limit = numberFormat(coveSP514.getLimit1());
						this.sp514_ded = numberFormat(coveSP514.getDeductible1());
						sp514Locations++;
					}
					
					if (coveSP64 != null) {
						this.oap4_sp_limit = "As per Garage Cert. of Ins.";
						this.oap4_sp_ded = "As per Garage Cert. of Ins.";
					}
					
					if (pl.getLocationId().equalsIgnoreCase("A")) {
						if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationa = locationSB.toString();
							this.insBldga = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLota = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_veha = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limita = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_deda = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_prema = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_prema = "$Incl.";
							}
						}
						
						int limitA = 0;
						int deductibleA = 0;
						double premiumA = 0;

						if (coveCM512 != null) {
							this.subSecA = coveCM512.getDescription().substring(0, 5);
							limitA = limitA + coveCM512.getLimit1();
							deductibleA = deductibleA + coveCM512.getDeductible1();
							premiumA = premiumA + coveCM512.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP513 != null) {
							this.subSecA = coveSP513.getDescription().substring(0, 5);
							limitA = limitA + coveSP513.getLimit1();
							deductibleA = deductibleA + coveSP513.getDeductible1();
							premiumA = premiumA + coveSP513.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP514 != null) {
							this.subSecA = coveSP514.getDescription().substring(0, 5);
							limitA = limitA + coveSP514.getLimit1();
							deductibleA = deductibleA + coveSP514.getDeductible1();
							premiumA = premiumA + coveSP514.getCoveragePremium().getWrittenPremium();
						}

						this.sbs_limA = "$" + numberFormat(limitA);
						this.sbs_dedA = "$" + numberFormat(deductibleA);
						this.sbs_premA = "$" + numberFormat(premiumA);

						if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
							this.sbs_limA = "";
							this.sbs_dedA = "";
							this.sbs_premA = "";
						}
					} else if (pl.getLocationId().equalsIgnoreCase("B")) {
						if (pl.getCustomerMainAddressPK() == null && (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null)) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationb = locationSB.toString();
							this.insBldgb = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotb = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_vehb = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitb = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedb = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premb = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premb = "$Incl.";
							}
						}

						int limitB = 0;
						int deductibleB = 0;
						double premiumB = 0;

						if (coveCM512 != null) {
							this.subSecB = coveCM512.getDescription().substring(0, 5);
							limitB = limitB + coveCM512.getLimit1();
							deductibleB = deductibleB + coveCM512.getDeductible1();
							premiumB = premiumB + coveCM512.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP513 != null) {
							this.subSecB = coveSP513.getDescription().substring(0, 5);
							limitB = limitB + coveSP513.getLimit1();
							deductibleB = deductibleB + coveSP513.getDeductible1();
							premiumB = premiumB + coveSP513.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP514 != null) {
							this.subSecB = coveSP514.getDescription().substring(0, 5);
							limitB = limitB + coveSP514.getLimit1();
							deductibleB = deductibleB + coveSP514.getDeductible1();
							premiumB = premiumB + coveSP514.getCoveragePremium().getWrittenPremium();
						}

						this.sbs_limB = "$" + numberFormat(limitB);
						this.sbs_dedB = "$" + numberFormat(deductibleB);
						this.sbs_premB = "$" + numberFormat(premiumB);

						if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
							this.sbs_limB = "";
							this.sbs_dedB = "";
							this.sbs_premB = "";
						}
					} else if (pl.getLocationId().equalsIgnoreCase("C")) {
						if (pl.getCustomerMainAddressPK() == null && (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null)) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationc = locationSB.toString();
							this.insBldgc = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotc = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;

							if (coveSP64 != null) {
								this.maxnum_vehc = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitc = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedc = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premc = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premc = "$Incl.";
							}
						}

						int limitC = 0;
						int deductibleC = 0;
						double premiumC = 0;

						if (coveCM512 != null) {
							this.subSecC = coveCM512.getDescription().substring(0, 5);
							limitC = limitC + coveCM512.getLimit1();
							deductibleC = deductibleC + coveCM512.getDeductible1();
							premiumC = premiumC + coveCM512.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP513 != null) {
							this.subSecC = coveSP513.getDescription().substring(0, 5);
							limitC = limitC + coveSP513.getLimit1();
							deductibleC = deductibleC + coveSP513.getDeductible1();
							premiumC = premiumC + coveSP513.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP514 != null) {
							this.subSecC = coveSP514.getDescription().substring(0, 5);
							limitC = limitC + coveSP514.getLimit1();
							deductibleC = deductibleC + coveSP514.getDeductible1();
							premiumC = premiumC + coveSP514.getCoveragePremium().getWrittenPremium();
						}

						this.sbs_limC = "$" + numberFormat(limitC);
						this.sbs_dedC = "$" + numberFormat(deductibleC);
						this.sbs_premC = "$" + numberFormat(premiumC);

						if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
							this.sbs_limC = "";
							this.sbs_dedC = "";
							this.sbs_premC = "";
						}
					} else if (pl.getLocationId().equalsIgnoreCase("D")) {
						if (pl.getCustomerMainAddressPK() == null && (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null)) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationd = locationSB.toString();
							this.insBldgd = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotd = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_vehd = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitd = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedd = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premd = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premd = "$Incl.";
							}
						}

						int limitD = 0;
						int deductibleD = 0;
						double premiumD = 0;

						if (coveCM512 != null) {
							this.subSecD = coveCM512.getDescription().substring(0, 5);
							limitD = limitD + coveCM512.getLimit1();
							deductibleD = deductibleD + coveCM512.getDeductible1();
							premiumD = premiumD + coveCM512.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP513 != null) {
							this.subSecD = coveSP513.getDescription().substring(0, 5);
							limitD = limitD + coveSP513.getLimit1();
							deductibleD = deductibleD + coveSP513.getDeductible1();
							premiumD = premiumD + coveSP513.getCoveragePremium().getWrittenPremium();
						}

						if (coveSP514 != null) {
							this.subSecD = coveSP514.getDescription().substring(0, 5);
							limitD = limitD + coveSP514.getLimit1();
							deductibleD = deductibleD + coveSP514.getDeductible1();
							premiumD = premiumD + coveSP514.getCoveragePremium().getWrittenPremium();
						}

						this.sbs_limD = "$" + numberFormat(limitD);
						this.sbs_dedD = "$" + numberFormat(deductibleD);
						this.sbs_premD = "$" + numberFormat(premiumD);

						if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
							this.sbs_limD = "";
							this.sbs_dedD = "";
							this.sbs_premD = "";
						}
					} else {

						if (coveCM512 != null) {
							cm512_prem_total = cm512_prem_total + coveCM512.getCoveragePremium().getWrittenPremium();
						}
						if (coveSP513 != null) {
							sp513_prem_total = sp513_prem_total + coveSP513.getCoveragePremium().getWrittenPremium();
						}
						if (coveSP514 != null) {
							sp514_prem_total = sp514_prem_total + coveSP514.getCoveragePremium().getWrittenPremium();
						}
						if (coveSP64 != null) {
							sp64_prem_total = sp64_prem_total + coveSP64.getCoveragePremium().getWrittenPremium();
						}
					}
				}
			}
			
			if (cm512Locations > 1) {
				this.cm512_limit = "As per Garage Cert. of Ins.";
				this.cm512_ded = "As per Garage Cert. of Ins.";
			}
			if (sp513Locations > 1) {
				this.sp513_limit = "As per Garage Cert. of Ins.";
				this.sp513_ded = "As per Garage Cert. of Ins.";
			}
			if (sp514Locations > 1) {
				this.sp514_limit = "As per Garage Cert. of Ins.";
				this.sp514_ded = "As per Garage Cert. of Ins.";
			}
			
			this.garBusinessType = subPolicyGAR.getBusinessDescription();
			this.ft_emp = numberFormat(subPolicyGAR.getNumFullTimeEmployees());
			this.pt_emp = numberFormat(subPolicyGAR.getNumPartTimeEmployees());
			this.oap4_prem = numberFormat(subPolicyGAR.getSubPolicyPremium().getWrittenPremium());
			this.gar_end_code = endCodeSB.toString();
			this.oap4_ab_limit = "Standard Limits";
			this.oap4_ua_limit = "Incl.";
			this.cm512_prem = numberFormat(cm512_prem_total);
			this.sp513_prem = numberFormat(sp513_prem_total);
			this.sp514_prem = numberFormat(sp514_prem_total);
			this.sp64_prem = numberFormat(sp64_prem_total);
			if (!this.showOEF81Section) {
				this.oef81_prem = "N/A";
			}
			this.oef72_end_codes = oef72EndCodeSB.toString();
		}

		StringBuilder endDescSB = new StringBuilder();
		HashMap<Integer, String> endDescMap = new HashMap<Integer, String>();
		Iterator<Endorsement> itEndDescs = endDescs.iterator();
		while (itEndDescs.hasNext()) {
			Endorsement end = itEndDescs.next();

			switch (end.getEndorsementCd()) {
			case "OPCF8":
				endDescMap.put(0, end.getDescription());
				break;
			case "OPCF9":
				endDescMap.put(1, end.getDescription());
				break;
			case "OPCF13C":
				endDescMap.put(2, end.getDescription());
				break;
			case "OPCF19":
				endDescMap.put(3, end.getDescription());
				break;
			case "OPCF21B":
				endDescMap.put(4, end.getDescription());
				break;
			case "OPCF28":
				endDescMap.put(5, end.getDescription());
				break;
			case "OPCF28A":
				endDescMap.put(6, end.getDescription());
				break;
			case "OPCF30":
				endDescMap.put(7, end.getDescription());
				break;
			case "OPCF31":
				endDescMap.put(8, end.getDescription());
				break;
			case "OPCF38":
				endDescMap.put(9, end.getDescription());
				break;
			case "OPCF40":
				endDescMap.put(10, end.getDescription());
				break;
			case "OEF71":
				endDescMap.put(11, end.getDescription());
				break;
			case "OEF72":
				endDescMap.put(12, end.getDescription());
				break;
			case "OEF73":
				endDescMap.put(13, end.getDescription());
				break;
			case "OEF74":
				endDescMap.put(14, end.getDescription());
				break;
			case "OEF75":
				endDescMap.put(15, end.getDescription());
				break;
			case "OEF78":
				endDescMap.put(16, end.getDescription());
				break;
			case "OEF78A":
				endDescMap.put(17, end.getDescription());
				break;
			case "OEF79":
				endDescMap.put(18, end.getDescription());
				break;
			case "OEF83":
				endDescMap.put(19, end.getDescription());
				break;
			case "OEF86":
				endDescMap.put(20, end.getDescription());
				break;
			case "SEF99":
				endDescMap.put(21, end.getDescription());
				break;
			case "EON110":
				endDescMap.put(22, end.getDescription());
				break;
			case "EON111":
				endDescMap.put(23, end.getDescription());
				break;
			case "IA":
				endDescMap.put(24, end.getDescription());
				break;
			case "GLEE":
				endDescMap.put(25, end.getDescription());
				break;
			case "CPEE":
				endDescMap.put(26, end.getDescription());
				break;
			case "GDPC":
				endDescMap.put(27, end.getDescription());
				break;
			case "GDPCG":
				endDescMap.put(27, end.getDescription());
				break;
			case "GSPC":
				endDescMap.put(28, end.getDescription());
				break;
			}
		}
		
		for (int i = 0; i <= 28; i++) {
			String desc = endDescMap.get(i);
			if (desc != null) {
				if (endDescSB.length() > 0) {
					endDescSB.append("\n");
				}
				endDescSB.append(desc);
			}
		}
		
		this.end_desc = endDescSB.toString();
		this.total_premium = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		
		if(this.showOPCF21BSection) {
			this.opcf21b_prem = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		}
	}
	
	@Override
	public String getTemplatePath() {
		return "templates/PolicyPackage_tmp.docx";
	}
	
	@Override
	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if(attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);

		setGeneralDeclarationsTable(doc);
		
		if (this.showOPCF5Section) {
			setOPCF5LessorTable(doc);
		}
		
		if (this.showOPCF19Section) {
			setOPCF19Section(doc);
		}
		
		if (this.opcf20_exists) {
			setOPCF20Section();
		}

		if (this.showOPCF21BSection) {
			setOPCF21BFirstTable(doc);
			setOPCF21BSecondTable(doc);
		}

		if (this.showOPCF23ASection) {
			setOPCF23ASecondTable(doc);
		}
		
		if (this.showOPCF27Section) {
			setOPCF27Table(doc);
		}
		
		if (this.showOPCF27BSection) {
			setOPCF27BSection(doc);
		}
		
		if (this.showOPCF28ASection) {
			setOPCF28ASection(doc);
		}
		
		if (this.showOPCF31Section) {
			setOPCF31Section(doc);
		}
		
		if (this.showOPCF38Section) {
			setOPCF38Section(doc);
		}
		
		setMotorTruckCargoDecTable(doc);
		
		setCommercialGeneralLiabilityDecTable(doc);
		
		if (this.showOEF72Section) {
			setOEF72Table(doc);
		}
		
		if (this.showOEF82Section) {
			setOEF82FirstTable(doc);
		}

		if (this.showEON102Section) {
			setEON102Section(doc);
		}
		
		if (this.showAICSection) {
			setAICSection(doc);
		}
		
		if (this.showAICCSection) {
			setAICCSection(doc);
		}
		
		if (this.showEON109Section) {
			setEON109Section(doc);
		}

		hideSections(doc);
		endorsementNumeration(doc);
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}
	
	@Override
	public void initializeFields() {
		fieldNames = new String[] { "Doc_Date", "Insured_Name", "Insured_Address_Street", "Insured_Address_City",
				"Insured_Address_ProvState", "Insured_Address_PC", "Contract_No", "Pol_No", "End_Desc",
				"Broker_Contact", "BrokerName", "Effective_Date", "Expiry_Date", "LB_Limit", "OPCF8_Ded", "DCPD_Ded",
				"CPE_Limit", "CPE_Ded", "Cargo_Ded", "Cargo_Limit", "Cargo_Limit1", "Cargo_Limit2", "Cargo_Limit3",
				"Cargo_Limit4", "Cargo_Limit5", "CGL_LB_Limit", "CGL_Ded", "MEX_Limit", "TLL_Limit", "TLL_Ded",
				"EBE_Limit", "EBE_Ded", "SPF6_LB_Limit", "SEF96_LB_Limit", "SEF99_LB_Limit", "OAP4_Limit",
				"OAP4_DCPD_Ded", "CL511_Ded", "CM512_Limit", "CM512_Ded", "SP513_Limit", "SP513_Ded", "SP514_Limit",
				"SP514_Ded", "GAR511_Ded", "GAR511_Prem", "SubSecA", "SubSecB", "SubSecC", "SubSecD", "SBS_LimA",
				"SBS_LimB", "SBS_LimC", "SBS_LimD", "SBS_DedA", "SBS_DedB", "SBS_DedC", "SBS_DedD", "SBS_PremA",
				"SBS_PremB", "SBS_PremC", "SBS_PremD", "OEF81_Lim", "OEF81_Prem", "OAP4_CL_Limit", "OAP4_CL_Ded",
				"OAP4_SP_Limit", "OAP4_SP_Ded", "OEF71_Limit", "OEF71_Ded", "OEF77_Limit", "OEF77_Ded",
				"OEF77_OAP4_SP_PremA", "OEF77_OAP4_SP_PremB", "OEF77_OAP4_SP_PremC", "OEF77_OAP4_SP_PremD",
				"OEF82511_Ded", "OEF82511_Prem", "OEF82512_Ded", "OEF82512_Prem", "OEF82513_Ded", "OEF82513_Prem",
				"OEF82_Lim", "OEF83_Limit", "OEF83_Ded", "OEF86_Limit", "Total_Premium", "OPCF21B_Prem", "EON110_No",
				"MaxEndNo", "MasterBrokerNumber", "BrokerAddr", "BrokerCity", "BrokerProvState", "BrokerPC",
				"Issue_Date", "EffYear", "EffMonth", "EffDay", "ExpYear", "ExpMonth", "ExpDay", "Prep_Date",
				"BITotalPrem", "PDTotalPrem", "ABTotalPrem", "UATotalPrem", "DCPDTotalPrem", "SPTotalPrem",
				"COMPTotalPrem", "COLLTotalPrem", "APTotalPrem", "EndList1", "VehPrem", "OPCF2_Name",
				"OPCF2_Relationship", "OPCF20_Limit", "OPCF20H_Limit", "Termination_Day", "OPCF27_APDed", "OPCF27_Limit",
				"OPCF27B_APDed", "BusinessType", "OPCF27B_Limit", "OPCF28_Pers", "OPCF28_Limit", "OPCF28CL_Ded",
				"OPCF28AP_Ded", "OPCF28_Coll", "OPCF28_CollNot", "OPCF28_AllPeril", "OPCF28_AllPerilNot",
				"Cargo_Limit1_Dec", "Cargo_Limit2_Dec", "Cargo_Limit3_Dec", "Cargo_Limit4_Dec", "Cargo_Limit5_Dec",
				"Cargo_Limit6_Dec", "TotalCargoPrem", "Cargo_Locations", "TotalCGLPrem", "CGL_Locations",
				"GAR_LocationA", "InsBldgA", "InsLotA", "GAR_LocationB", "InsBldgB", "InsLotB", "GAR_LocationC",
				"InsBldgC", "InsLotC", "GAR_LocationD", "InsBldgD", "InsLotD", "GAR_BusinessType", "FT_Emp", "PT_Emp",
				"OAP4_BI_Prem", "OAP4_PD_Prem", "OAP4_BIPD_Prem", "OAP4_AB_Prem", "OAP4_DCPD_Prem", "OAP4_CL_Prem",
				"MaxNum_VehA", "OAP4_SP_LimitA", "OAP4_SP_DedA", "OAP4_SP_PremA", "MaxNum_VehB", "OAP4_SP_LimitB",
				"OAP4_SP_DedB", "OAP4_SP_PremB", "MaxNum_VehC", "OAP4_SP_LimitC", "OAP4_SP_DedC", "OAP4_SP_PremC",
				"MaxNum_VehD", "OAP4_SP_LimitD", "OAP4_SP_DedD", "OAP4_SP_PremD", "OAP4_Prem", "GAR_EndCode",
				"OAP4_AB_Limit", "OAP4_UA_Limit", "CM512_Prem", "SP513_Prem", "SP514_Prem", "SP64_Prem",
				"OEF72_End_Codes", "OEF72_End_Prem", "OEF72_Net_Prem", "OEF72_Prem", "OEF73_Other_Party",
				"OEF73_Other_Party_Address", "OEF74_Ded", "OEF75_Ded", "OEF76_Insured_Name",
				"OEF76_Insured_Relationship", "OEF78_Person", "OEF78_Liab_Limit", "OEF78_InsuredInd1", "CL5_Ded",
				"OEF78_InsuredInd2", "CL6_Lim", "CL6_Ded", "OEF78_InsuredInd3", "OEF78A_Excl_Driver",
				"OEF78A_Driver_LicNo", "OEF83_NumOfVeh", "OEF83_Coll5", "OEF83_Coll6", "GDPC_ServPlateNo",
				"GDPC_ExpDate", "GSPC_ServPlateNo", "GSPC_ExpDate", "EON114_Name_Ins", "Add_Ins_Name",
				"Add_Ins_Addr_Street", "Add_Ins_Addr_City", "Add_Ins_Addr_PC", "EON102_Ded", "EON102_Sec7_Ded",
				"GECGLMTC_Mod_Type", "GECGLMTC_Veh_Desc", "GECGLMTC_Veh_SerialNo", "CGL_GE_Prem", "CGO_Limit",
				"CGO_Ded", "Cargo_GE_Prem", "Per_Occurrence_Ded", "GECGL_Mod_Type", "GECGL_Veh_Desc",
				"GECGL_Veh_SerialNo", "GECGL_Prem", "AIC_Limit", "AIC_Ded", "EON111_Effective_Date",
				"EON111_Expiry_Date", "Date", "GDPC_Dealer_PlateNo", "GDPC_Exp_Date", "WOS_Co_Name", "WOS_Co_Addr",
				"AOCCGL_Prem", "MOCCGL_Prem", "AOCC_Prem", "MOCC_Prem", "OPCF21B_Prorata", "OPCF21B_5050", "Sec7_Ded",
				"Veh_Desc", "OPCF23A_DCPD_CK", "OPCF23A_DCPD_Ded", "OPCF23A_SP_CK", "OPCF23A_SP_Ded", "OPCF23A_CL_CK",
				"OPCF23A_CL_Ded", "OPCF23A_CM_CK", "OPCF23A_CM_Ded", "OPCF23A_AP_CK", "OPCF23A_AP_Ded",
				"OPCF23ABL_DCPD_CK", "OPCF23ABL_DCPD_Ded", "OPCF23ABL_SP_CK", "OPCF23ABL_SP_Ded", "OPCF23ABL_CL_CK",
				"OPCF23ABL_CL_Ded", "OPCF23ABL_CM_CK", "OPCF23ABL_CM_Ded", "OPCF23ABL_AP_CK", "OPCF23ABL_AP_Ded",
				"TTC_VehDesc", "TTC_Period1", "TTC_Period2", "TTC_SerNo", "TTC_Limit", "TTC_Ded", "TTC_Prem",
				"AICC_Limit", "AICC_Ded", "CTC_Limit", "SubPolicy_Named_Insured", "MaxEndNo_Item6" };
		fieldValues = new Object[] { docDate, insuredName, insuredAddressStreet, insuredAddressCity,
				insuredAddressProvState, insuredAddressPC, contractNo, polNo, end_desc, brokerContact, brokerName,
				effectiveDate, expiryDate, lbLimit, opcf8_ded, dcpd_ded, cpe_limit, cpe_ded, cargo_ded, cargoLimit,
				cargoLimit1, cargoLimit2, cargoLimit3, cargoLimit4, cargoLimit5, cgl_lb_limit, cgl_ded, mex_limit,
				tll_limit, tll_ded, ebe_limit, ebe_ded, spf6_lb_limit, sef96_lb_limit, sef99_lb_limit, oap4_lb_limit,
				oap4_dcpd_ded, cl511_ded, cm512_limit, cm512_ded, sp513_limit, sp513_ded, sp514_limit, sp514_ded,
				gar511_ded, gar511_Prem, subSecA, subSecB, subSecC, subSecD, sbs_limA, sbs_limB, sbs_limC, sbs_limD,
				sbs_dedA, sbs_dedB, sbs_dedC, sbs_dedD, sbs_premA, sbs_premB, sbs_premC, sbs_premD, oef81_lim,
				oef81_prem, oap4_cl_limit, oap4_cl_ded, oap4_sp_limit, oap4_sp_ded, oef71_limit, oef71_ded, oef77_limit,
				oef77_ded, oef77_oap4_sp_prema, oef77_oap4_sp_premb, oef77_oap4_sp_premc, oef77_oap4_sp_premd,
				oef82_511_ded, oef82_511_prem, oef82_512_ded, oef82_512_prem, oef82_513_ded, oef82_513_prem, oef82_lim,
				oef83_limit, oef83_ded, oef86_limit, total_premium, opcf21b_prem, eon110_no, maxendno, master_broker_no,
				broker_addr, broker_city, broker_provState, broker_pc, issue_date, effYear, effMonth, effDay, expYear,
				expMonth, expDay, prepDate, biTotalPrem, pdTotalPrem, abTotalPrem, uaTotalPrem, dcpdTotalPrem,
				spTotalPrem, compTotalPrem, collTotalPrem, apTotalPrem, endList1, vehPrem, opcf2_name,
				opcf2_relationship, opcf20_limit, opcf20H_limit, termination_day, opcf27_apded, opcf27_limit,
				opcf27B_apded, businessType, opcf27B_limit, opcf28_pers, opcf28_limit, opcf28CL_ded, opcf28AP_ded,
				opcf28_coll, opcf28_collNot, opcf28_allperil, opcf28_allperilNot, cargoLimit1Dec, cargoLimit2Dec,
				cargoLimit3Dec, cargoLimit4Dec, cargoLimit5Dec, cargoLimit6Dec, totalCargoPrem, cargo_locations,
				totalCGLPrem, cgl_locations, gar_locationa, insBldga, insLota, gar_locationb, insBldgb, insLotb,
				gar_locationc, insBldgc, insLotc, gar_locationd, insBldgd, insLotd, garBusinessType, ft_emp, pt_emp,
				oap4_bi_prem, oap4_pd_prem, oap4_bipd_prem, oap4_ab_prem, oap4_dcpd_prem, oap4_cl_prem, maxnum_veha,
				oap4_sp_limita, oap4_sp_deda, oap4_sp_prema, maxnum_vehb, oap4_sp_limitb, oap4_sp_dedb, oap4_sp_premb,
				maxnum_vehc, oap4_sp_limitc, oap4_sp_dedc, oap4_sp_premc, maxnum_vehd, oap4_sp_limitd, oap4_sp_dedd,
				oap4_sp_premd, oap4_prem, gar_end_code, oap4_ab_limit, oap4_ua_limit, cm512_prem, sp513_prem,
				sp514_prem, sp64_prem, oef72_end_codes, oef72_end_prem, oef72_net_prem, oef72_prem, oef73_other_party,
				oef73_other_party_address, oef74_ded, oef75_ded, oef76_insured_name, oef76_insured_relationship,
				oef78_pers, oef78_liab_limit, oef78_insuredind1, cl5_ded, oef78_insuredind2, cl6_lim, cl6_ded,
				oef78_insuredind3, oef78A_exclDriver, oef78A_driverLicNo, oef83_numOfVeh, oef83_coll5, oef83_coll6,
				gdpc_servPlateNo, gdpc_expDate, gspc_servPlateNo, gspc_expDate, eon114_name_ins, add_ins_name,
				add_ins_addr_street, add_ins_addr_city, add_ins_addr_pc, eon102_ded, eon102_sec7_ded, gecglmtc_mod_type,
				gecglmtc_veh_desc, gecglmtc_veh_serial_no, cgl_ge_prem, cgo_limit, cgo_ded, cargo_ge_prem,
				per_occurrence_ded, gecgl_mod_type, gecgl_veh_desc, gecgl_veh_serial_no, gecgl_prem, aic_limit, aic_ded,
				eon111_effective_date, eon111_expiry_date, date, gdpc_dealer_plate_no, gdpc_exp_date, wos_co_name,
				wos_co_addr, aoccgl_prem, moccgl_prem, aocc_prem, mocc_prem, opcf21B_prorata, opcf21B_5050, sec7Ded,
				vehDesc, opcf23A_dcpd_ck, opcf23A_dcpd_ded, opcf23A_sp_ck, opcf23A_sp_ded, opcf23A_cl_ck,
				opcf23A_cl_ded, opcf23A_cm_ck, opcf23A_cm_ded, opcf23A_ap_ck, opcf23A_ap_ded, opcf23ABL_dcpd_ck,
				opcf23ABL_dcpd_ded, opcf23ABL_sp_ck, opcf23ABL_sp_ded, opcf23ABL_cl_ck, opcf23ABL_cl_ded,
				opcf23ABL_cm_ck, opcf23ABL_cm_ded, opcf23ABL_ap_ck, opcf23ABL_ap_ded, ttc_vehdesc, ttc_period1,
				ttc_period2, ttc_serno, ttc_limit, ttc_ded, ttc_prem, aicc_limit, aicc_ded, ctc_limit,
				subPolicyNamedInsured, maxendno_item6 };
	}
	
	protected void setOPCF20Section() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20Section = true;
					this.opcf20_limit = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("MTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("LTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("HTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TT")) {
					this.showOPCF20HSection = true;
					this.opcf20H_limit = numberFormat(end.getLimit1amount());
				}
			}
		}
	}

	@Override
	protected void initializeSections(Document doc) {
		genDecSec = doc.getSections().get(1);
		opcf2Sec = doc.getSections().get(3);
		opcf5Sec = doc.getSections().get(4);
		opcf5BlSec = doc.getSections().get(5);
		opcf8Sec = doc.getSections().get(6);
		opcf9Sec = doc.getSections().get(7);
		opcf13CSec = doc.getSections().get(8);
		opcf19Sec = doc.getSections().get(9);
		opcf20Sec = doc.getSections().get(10);
		opcf20HSec = doc.getSections().get(11);
		opcf21ASec = doc.getSections().get(12);
		opcf21BSec = doc.getSections().get(13);
		opcf23ASec = doc.getSections().get(14);
		opcf23ABlSec = doc.getSections().get(15);
		opcf25ASec = doc.getSections().get(16);
		opcf27Sec = doc.getSections().get(17);
		opcf27BSec = doc.getSections().get(18);
		opcf28Sec = doc.getSections().get(19);
		opcf28ASec = doc.getSections().get(20);
		opcf30Sec = doc.getSections().get(21);
		opcf31Sec = doc.getSections().get(22);
		opcf38Sec = doc.getSections().get(23);
		opcf40Sec = doc.getSections().get(24);
		opcf43Sec = doc.getSections().get(25);
		opcf43ASec = doc.getSections().get(26);
		opcf44RSec = doc.getSections().get(27);
		opcf47Sec = doc.getSections().get(28);
		opcf48Sec = doc.getSections().get(29);
		cgoDecSec = doc.getSections().get(30);
		cglDecSec = doc.getSections().get(31);
		garDecSec = doc.getSections().get(32);
		oef71Sec = doc.getSections().get(33);
		oef72Sec = doc.getSections().get(34);
		oef73Sec = doc.getSections().get(35);
		oef74Sec = doc.getSections().get(36);
		oef75Sec = doc.getSections().get(37);
		oef76Sec = doc.getSections().get(38);
		oef77Sec = doc.getSections().get(39);
		oef78Sec = doc.getSections().get(40);
		oef78ASec = doc.getSections().get(41);
		oef79Sec = doc.getSections().get(42);
		oef80Sec = doc.getSections().get(43);
		oef81Sec = doc.getSections().get(44);
		oef82Sec = doc.getSections().get(45);
		oef83Sec = doc.getSections().get(46);
		oef86Sec = doc.getSections().get(47);
		oef87Sec = doc.getSections().get(48);
		gdpcgSec = doc.getSections().get(49);
		gspcSec = doc.getSections().get(50);
		spf6_1Sec = doc.getSections().get(51);
		spf6_2Sec = doc.getSections().get(52);
		spf6_3Sec = doc.getSections().get(53);
		spf6_4Sec = doc.getSections().get(54);
		spf6_5Sec = doc.getSections().get(55);
		spf6_6Sec = doc.getSections().get(56);
		spf6_7Sec = doc.getSections().get(57);
		spf6_8Sec = doc.getSections().get(58);
		spf6_9Sec = doc.getSections().get(59);
		sef96Sec = doc.getSections().get(60);
		sef99Sec = doc.getSections().get(61);
		endorsementSection = doc.getSections().get(62);
		eon114Sec = doc.getSections().get(63);
		eon101Sec = doc.getSections().get(64);
		eon102Sec = doc.getSections().get(65);
		gecglmtcSec = doc.getSections().get(66);
		gecglSec = doc.getSections().get(67);
		eon20GSec = doc.getSections().get(68);
		gdpcSec = doc.getSections().get(69);
		gleeSec = doc.getSections().get(70);
		aicSec = doc.getSections().get(71);
		aiccSec = doc.getSections().get(72);
		eon109Sec = doc.getSections().get(73);
		eon110Sec = doc.getSections().get(74);
		eon111Sec = doc.getSections().get(75);
		eon112Sec = doc.getSections().get(76);
		eon121Sec = doc.getSections().get(77);
		docSec = doc.getSections().get(78);
		wosSec = doc.getSections().get(79);
		aoccglSec= doc.getSections().get(80);
		moccglSec= doc.getSections().get(81);
		cdelSec = doc.getSections().get(82);
		aoccSec= doc.getSections().get(83);
		moccSec= doc.getSections().get(84);
		cpeeSec = doc.getSections().get(85);
		cdecSec = doc.getSections().get(86);
		ttcSec = doc.getSections().get(87);
		eon116Sec = doc.getSections().get(88);
		eon117Sec = doc.getSections().get(89);
		iaSec = doc.getSections().get(90);
	}
	
	@Override
	protected void hideSections(Document doc) {
		
		if (numEnds.isEmpty()) {
			doc.removeChild(endorsementSection);
		}

		if (!showOPCF2Section) {
			doc.removeChild(opcf2Sec);
		}

		if (!showOPCF5Section) {
			doc.removeChild(opcf5Sec);
		}

		if (!showOPCF5BlSection) {
			doc.removeChild(opcf5BlSec);
		}

		if (!showOPCF8Section) {
			doc.removeChild(opcf8Sec);
		}

		if (!showOPCF9Section) {
			doc.removeChild(opcf9Sec);
		}

		if (!showOPCF13CSection) {
			doc.removeChild(opcf13CSec);
		}

		if (!showOPCF19Section) {
			doc.removeChild(opcf19Sec);
		}

		if (!showOPCF20Section) {
			doc.removeChild(opcf20Sec);
		}
		
		if (!showOPCF20HSection) {
			doc.removeChild(opcf20HSec);
		}

		if (!showOPCF21ASection) {
			doc.removeChild(opcf21ASec);
		}

		if (!showOPCF21BSection) {
			doc.removeChild(opcf21BSec);
		}

		if (!showOPCF23ASection) {
			doc.removeChild(opcf23ASec);
		}

		if (!showOPCF23ABlSection) {
			doc.removeChild(opcf23ABlSec);
		}

		if (!showOPCF25ASection) {
			doc.removeChild(opcf25ASec);
		}

		if (!showOPCF27Section) {
			doc.removeChild(opcf27Sec);
		}

		if (!showOPCF27BSection) {
			doc.removeChild(opcf27BSec);
		}

		if (!showOPCF28Section) {
			doc.removeChild(opcf28Sec);
		}

		if (!showOPCF28ASection) {
			doc.removeChild(opcf28ASec);
		}

		if (!showOPCF30Section) {
			doc.removeChild(opcf30Sec);
		}

		if (!showOPCF31Section) {
			doc.removeChild(opcf31Sec);
		}

		if (!showOPCF38Section) {
			doc.removeChild(opcf38Sec);
		}

		if (!showOPCF40Section) {
			doc.removeChild(opcf40Sec);
		}

		if (!showOPCF43Section) {
			doc.removeChild(opcf43Sec);
		}

		if (!showOPCF43ASection) {
			doc.removeChild(opcf43ASec);
		}

		if (!showOPCF44RSection) {
			doc.removeChild(opcf44RSec);
		}

		if (!showOPCF47Section) {
			doc.removeChild(opcf47Sec);
		}

		if (!showOPCF48Section) {
			doc.removeChild(opcf48Sec);
		}
		
		if (!showCGODecSection) {
			doc.removeChild(cgoDecSec);
		}
		
		if (!showCGLDecSection) {
			doc.removeChild(cglDecSec);
		}
		
		if (!showGARDecSection) {
			doc.removeChild(garDecSec);
		}
		
		if (!showOEF71Section) {
			doc.removeChild(oef71Sec);
		}
		
		if (!showOEF72Section) {
			doc.removeChild(oef72Sec);
		}
		
		if (!showOEF73Section) {
			doc.removeChild(oef73Sec);
		}
		
		if (!showOEF74Section) {
			doc.removeChild(oef74Sec);
		}
		
		if (!showOEF75Section) {
			doc.removeChild(oef75Sec);
		}
		
		if (!showOEF76Section) {
			doc.removeChild(oef76Sec);
		}
		
		if (!showOEF77Section) {
			doc.removeChild(oef77Sec);
		}
		
		if (!showOEF78Section) {
			doc.removeChild(oef78Sec);
		}
		
		if (!showOEF78ASection) {
			doc.removeChild(oef78ASec);
		}
		
		if (!showOEF79Section) {
			doc.removeChild(oef79Sec);
		}
		
		if (!showOEF80Section) {
			doc.removeChild(oef80Sec);
		}
		
		if (!showOEF81Section) {
			doc.removeChild(oef81Sec);
		}
		
		if (!showOEF82Section) {
			doc.removeChild(oef82Sec);
		}
		
		if (!showOEF83Section) {
			doc.removeChild(oef83Sec);
		}
		
		if (!showOEF86Section) {
			doc.removeChild(oef86Sec);
		}
		
		if (!showOEF87Section) {
			doc.removeChild(oef87Sec);
		}

		if (!showGDPCGSection) {
			doc.removeChild(gdpcgSec);
		}
		
		if (!showGSPCSection) {
			doc.removeChild(gspcSec);
		}
		
		if (!showSPF6Section) {
			doc.removeChild(spf6_1Sec);
			doc.removeChild(spf6_2Sec);
			doc.removeChild(spf6_3Sec);
			doc.removeChild(spf6_4Sec);
			doc.removeChild(spf6_5Sec);
			doc.removeChild(spf6_6Sec);
			doc.removeChild(spf6_7Sec);
			doc.removeChild(spf6_8Sec);
			doc.removeChild(spf6_9Sec);
		}
		
		if (!showSEF96Section) {
			doc.removeChild(sef96Sec);
		}
		
		if (!showSEF99Section) {
			doc.removeChild(sef99Sec);
		}
		
		if (!showEON114Section) {
			doc.removeChild(eon114Sec);
		}
		
		if (!showEON101Section) {
			doc.removeChild(eon101Sec);
		}
		
		if (!showEON102Section) {
			doc.removeChild(eon102Sec);
		}
		
		if (!showGECGLMTCSection) {
			doc.removeChild(gecglmtcSec);
		}
		
		if (!showGECGLSection) {
			doc.removeChild(gecglSec);
		}
		
		if (!showEON20GSection) {
			doc.removeChild(eon20GSec);
		}
		
		if (!showAICSection) {
			doc.removeChild(aicSec);
		}
		
		if (!showAICCSection) {
			doc.removeChild(aiccSec);
		}
		
		if (!showEON109Section) {
			doc.removeChild(eon109Sec);
		}
		
		if (!showEON110Section) {
			doc.removeChild(eon110Sec);
		}
		
		if (!showEON111Section) {
			doc.removeChild(eon111Sec);
		}
		
		if (!showEON112Section) {
			doc.removeChild(eon112Sec);
		}
		
		if (!showEON121Section) {
			doc.removeChild(eon121Sec);
		}
		
		if (!showGLEESection) {
			doc.removeChild(gleeSec);
		}
		
		if (!showIASection) {
			doc.removeChild(iaSec);
		}
		
		if (!showGDPCSection) {
			doc.removeChild(gdpcSec);
		}
		
		if (!showDOCSection) {
			doc.removeChild(docSec);
		}
		
		if (!showCPEESection) {
			doc.removeChild(cpeeSec);
		}
		
		if (!showEON116Section) {
			doc.removeChild(eon116Sec);
		}
		
		if (!showEON117Section) {
			doc.removeChild(eon117Sec);
		}
		
		if (!showWOSSection) {
			doc.removeChild(wosSec);
		}
		
		if (!showAOCCGLSection) {
			doc.removeChild(aoccglSec);
		}
		
		if (!showMOCCGLSection) {
			doc.removeChild(moccglSec);
		}
		
		if (!showAOCCSection) {
			doc.removeChild(aoccSec);
		}
		
		if (!showMOCCSection) {
			doc.removeChild(moccSec);
		}
		
		if (!showCDELSection) {
			doc.removeChild(cdelSec);
		}
		
		if (!showCDECSection) {
			doc.removeChild(cdecSec);
		}
		
		if (!showTTCSection) {
			doc.removeChild(ttcSec);
		}
	}

	
}
