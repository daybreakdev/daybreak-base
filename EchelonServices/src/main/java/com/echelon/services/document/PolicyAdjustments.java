package com.echelon.services.document;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.utils.Constants;

public class PolicyAdjustments extends EchelonDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 96795077846393260L;
	protected String[] fieldNames;
	protected Object[] fieldValues;
	protected String brokerName = "";
	protected String master_broker_no = "";
	protected String insuredName = "";
	protected String polNo = "";
	protected String effectiveDate = "";
	protected String expiryDate = "";
	protected String adj50_Prem = "";
	protected String adjProrata_Prem = "";
	protected String polEffectiveDate = "";
	protected String adjEffectiveDate = "";
	protected LinkedHashSet<Endorsement> numEnds = new LinkedHashSet<Endorsement>();
	protected Section eon116Sec = null;
	protected Section eon117Sec = null;
	protected SpecialtyPolicyDTO policyDTO;
	protected boolean showEON116Section = false;
	protected boolean showEON117Section = false;

	@Override
	public String getTemplatePath() {
		return "";
	}

	@Override
	public void create(String storageKey) throws Exception {
	}

	@Override
	public void initializeFields() {
	}

	protected void setEON116Table(Document doc) {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		Section sec = eon116Sec;
		Table tmpTable = sec.getBody().getTables().get(1);
		Row firstVehicleRow = tmpTable.getRows().get(1);
		Double adj50Prem = 0.0;
		
		CGLSubPolicy<Risk> subPolicyCGL = (CGLSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		
		CargoSubPolicy<Risk> subPolicyCGO = (CargoSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			List<SpecialtyVehicleRiskAdjustment> adjustments = vehicle.getSvrAdjustments().stream()
					.filter(a -> a.getSpecialtyVehicleRisk().getPolicyRiskPK().equals(vehicle.getPolicyRiskPK()))
					.collect(Collectors.toList());

			for (SpecialtyVehicleRiskAdjustment adjustment : adjustments) {
				if (!vehicle.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)) {
					continue;
				}
				PolicyTransaction<?> offsetTxn = (new PolicyTransactionDAO())
						.findById(policyDocument.getPolicyTransaction().getOriginatingPK());
				Integer prevAdjustmentNumOfUnits = policyDTO != null ? getPrevAdjustmentNumOfUnits(adjustment)
						: getPrevAdjustmentNumOfUnits(offsetTxn,
								adjustment.getSpecialtyVehicleRisk().getOriginatingPK());
				String veh_desc = adjustment.getVehicleDescription();
				String numOfUnitsEff = numberFormat(prevAdjustmentNumOfUnits);
				String numOfUnitsExp = numberFormat(adjustment.getNumOfUnits());
				String diff = numberFormat(adjustment.getNumOfUnits() - prevAdjustmentNumOfUnits);
				String unitRate = "$" + numberFormat(vehicle.getUnitRate());

				Double prem = adjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();

				if (subPolicyCGL != null) {
					SpecialtyVehicleRiskAdjustment cglAdjustment = subPolicyCGL
							.getSvrAdjustmentsByAdjNum(adjustment.getGroupID(), adjustment.getAdjustmentNum());
					if (cglAdjustment != null) {
						prem = prem + cglAdjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();
					}
				}

				if (subPolicyCGO != null) {
					SpecialtyVehicleRiskAdjustment cargoAdjustment = subPolicyCGO
							.getSvrAdjustmentsByAdjNum(adjustment.getGroupID(), adjustment.getAdjustmentNum());
					if (cargoAdjustment != null) {
						prem = prem + cargoAdjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();
					}
				}

				String adjPrem = premFormat(prem);
				adj50Prem = adj50Prem + prem;

				Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

				// VehDesc
				newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
				Run newRun0 = new Run(doc, veh_desc);
				newRun0.getFont().setName("Arial");
				newRun0.getFont().setSize(8);
				newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

				// NumOfUnits_Eff
				newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
				Run newRun1 = new Run(doc, numOfUnitsEff);
				newRun1.getFont().setName("Arial");
				newRun1.getFont().setSize(8);
				newRun1.getFont().setBold(true);
				newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

				// NumOfUnits_Exp
				newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
				Run newRun2 = new Run(doc, numOfUnitsExp);
				newRun2.getFont().setName("Arial");
				newRun2.getFont().setSize(8);
				newRun2.getFont().setBold(true);
				newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

				// Diff
				newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
				Run newRun3 = new Run(doc, diff);
				newRun3.getFont().setName("Arial");
				newRun3.getFont().setSize(8);
				newRun3.getFont().setBold(true);
				newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

				// unitrate
				newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
				Run newRun4 = new Run(doc, unitRate);
				newRun4.getFont().setName("Arial");
				newRun4.getFont().setSize(8);
				newRun4.getFont().setBold(true);
				newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

				// Adj_Prem
				newVehicleRow.getCells().get(6).getFirstParagraph().removeAllChildren();
				Run newRun6 = new Run(doc, adjPrem);
				newRun6.getFont().setName("Arial");
				newRun6.getFont().setSize(8);
				newRun6.getFont().setBold(true);
				newVehicleRow.getCells().get(6).getFirstParagraph().appendChild(newRun6);

				tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
				firstVehicleRow = newVehicleRow;
			}
		}
		tmpTable.removeChild(tmpTable.getRows().get(1));
		adj50_Prem = premFormat(adj50Prem);
	}

	protected void setEON117Table(Document doc) {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		Section sec = eon117Sec;
		Table tmpTable = sec.getBody().getTables().get(1);
		Row firstVehicleRow = tmpTable.getRows().get(1);
		Double adjProrataPrem = 0.0;
		List<HashMap<String, String>> vehAdjustments = new ArrayList<HashMap<String, String>>();
		
		CGLSubPolicy<Risk> subPolicyCGL = (CGLSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		
		CargoSubPolicy<Risk> subPolicyCGO = (CargoSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			List<SpecialtyVehicleRiskAdjustment> adjustments = vehicle.getSvrAdjustments().stream()
					.filter(a -> a.getSpecialtyVehicleRisk().getPolicyRiskPK().equals(vehicle.getPolicyRiskPK()))
					.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentDate))
					.collect(Collectors.toList());

			for (SpecialtyVehicleRiskAdjustment adjustment : adjustments) {
				if (!vehicle.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)) {
					continue;
				}

				String veh_desc = adjustment.getVehicleDescription();
				String adjDate = formatDate(adjustment.getAdjustmentDate(), "MM/dd/yyyy");
				String addedDate = adjustment.getPremiumChange() >= 0
						? formatDate(adjustment.getAdjustmentDate(), "MM/dd/yyyy")
						: "";
				String deletedDate = adjustment.getPremiumChange() < 0
						? formatDate(adjustment.getAdjustmentDate(), "MM/dd/yyyy")
						: "";
				String unitRate = "$" + numberFormat(vehicle.getUnitRate());
				String prorata = numberFormat(adjustment.getProRataFactor());
				
				Double prem = adjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();
				
				if (subPolicyCGL != null) {
					SpecialtyVehicleRiskAdjustment cglAdjustment = subPolicyCGL.getSvrAdjustmentsByAdjNum(adjustment.getGroupID(),
							adjustment.getAdjustmentNum());
					if (cglAdjustment != null) {
						prem = prem + cglAdjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();
					}
				}

				if (subPolicyCGO != null) {
					SpecialtyVehicleRiskAdjustment cargoAdjustment = subPolicyCGO.getSvrAdjustmentsByAdjNum(adjustment.getGroupID(),
							adjustment.getAdjustmentNum());
					if (cargoAdjustment != null) {
						prem = prem + cargoAdjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange();
					}
				}
				
				String netChange = premFormat(prem);
				adjProrataPrem = adjProrataPrem + prem;

				HashMap<String, String> vehAdjustment = new HashMap<String, String>();
				vehAdjustment.put("veh_desc", veh_desc);
				vehAdjustment.put("adjDate", adjDate);
				vehAdjustment.put("addedDate", addedDate);
				vehAdjustment.put("deletedDate", deletedDate);
				vehAdjustment.put("unitRate", unitRate);
				vehAdjustment.put("prorata", prorata);
				vehAdjustment.put("netChange", netChange);
				vehAdjustments.add(vehAdjustment);
			}
		}

		Collections.sort(vehAdjustments, new Comparator<HashMap<String, String>>() {
			public int compare(HashMap<String, String> one, HashMap<String, String> two) {
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
				LocalDate date1 = LocalDate.parse(one.get("adjDate"), formatter);
				LocalDate date2 = LocalDate.parse(two.get("adjDate"), formatter);
				return date1.compareTo(date2);
			}
		});
		
		for (HashMap<String, String> vehAdjustment : vehAdjustments) {

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// VehDesc
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, vehAdjustment.get("veh_desc"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// added_date
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, vehAdjustment.get("addedDate"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// deleted_date
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, vehAdjustment.get("deletedDate"));
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// unitrate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, vehAdjustment.get("unitRate"));
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// prorata
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, vehAdjustment.get("prorata"));
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			// netchange
			newVehicleRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, vehAdjustment.get("netChange"));
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			newVehicleRow.getCells().get(5).getFirstParagraph().appendChild(newRun5);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		tmpTable.removeChild(tmpTable.getRows().get(1));
		adjProrata_Prem = premFormat(adjProrataPrem);
	}

	protected void initializeSections(Document doc) {
	}

	protected void hideSections(Document doc) {
	}
	
	protected Integer getPrevAdjustmentNumOfUnits(PolicyTransaction<?> offsetTxn, Long policyRiskPK) {
		Integer numOfUnits = null;
		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) offsetTxn
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (subPolicy != null && subPolicy.getSubPolicyRisks() != null) {
			Set<Risk> risks = subPolicy.getSubPolicyRisks();
			for (Risk risk : risks) {
				if (risk.getRiskType().equalsIgnoreCase("AU") && risk.getPolicyRiskPK().equals(policyRiskPK)) {
					SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
					numOfUnits = vehicle.getNumberOfUnits();
					break;
				}
			}
		}

		return numOfUnits;
	}
	
	protected Integer getPrevAdjustmentNumOfUnits(SpecialtyVehicleRiskAdjustment adjustment) {
		Integer result = null;
		SpecialtyVehicleRiskAdjustment prevAdj = adjustment.findPrevAdjustment();
		if (prevAdj != null) {
			result = prevAdj.getNumOfUnits();
		} else {
			SpecialtyPolicyItemDTO vehicleItemDTO = policyDTO
					.getVehicleItemByPK(adjustment.getSpecialtyVehicleRisk().getPolicyRiskPK());
			if (vehicleItemDTO != null) {
				result = vehicleItemDTO.getOriginatingUnits();
			}
		}
		return result;
	}

	protected boolean isValidEndorsement(Endorsement end) {
		boolean isValid = false;
		String transType = this.policyDocument.getPolicyTransaction().getTransactionType();
		String txnType = this.policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

		// New Endorsement scenario
		if (transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
				&& txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_ADJUSTMENT)) {
			return true;
		}

		return isValid;
	}

	protected void endorsementNumeration(Document doc) {
		Integer sectionNo = getMaxEndorsementNumber();
		for (Section sec : doc.getSections()) {
			boolean isEndSection = false;
			if (sec.getBody().getTables() != null && sec.getBody().getTables().get(0) != null
					&& sec.getBody().getTables().get(0).getRows() != null
					&& sec.getBody().getTables().get(0).getRows().get(1) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph()
							.getText() != null
					&& !sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph()
							.getText().replaceAll("[^a-zA-Z0-9]", "").isEmpty()) {
				isEndSection = true;
			}

			if (isEndSection) {
				sectionNo++;
				Table table = sec.getBody().getTables().get(0);
				Row row = table.getRows().get(1);
				Cell cell = row.getCells().get(4);
				String endCode = cell.getFirstParagraph().getText();
				endCode = endCode.replaceAll("[^a-zA-Z0-9]", "");

				for (Endorsement end : numEnds) {
					if (endCode.equalsIgnoreCase(end.getEndorsementCd())) {
						end.setEndorsementNumber(sectionNo);
					}
				}

				cell.getFirstParagraph().removeAllChildren();
				Run run = new Run(doc, sectionNo.toString());
				run.getFont().setName("Arial");
				run.getFont().setSize(8);
				run.getFont().setBold(true);
				cell.getFirstParagraph().appendChild(run);
			}
		}

		/*
		 * for (Endorsement end : numEnds) { System.out.println(end.getEndorsementCd() +
		 * " = " + end.getEndorsementNumber()); }
		 */
	}

	private Integer getMaxEndorsementNumber() {

		Integer rv = null;

		Set<Integer> endNumbers = new HashSet<Integer>();
		SubPolicy<?> autoSP = policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null) {
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
					endNumbers.add(end.getEndorsementNumber());
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				endNumbers.add(end.getEndorsementNumber());
			}
		}
		rv = (!endNumbers.isEmpty() ? Collections.max(endNumbers) : 0);
		return rv;
	}

}
