package com.echelon.services.document;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.services.document.DSPolicyDocumentsService;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.utils.SpecialtyAutoConstants;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class PolicyDocumentsService extends DSPolicyDocumentsService {

	public PolicyDocumentsService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * @param policyTransaction
	 * @param documentIds
	 * 	valid values QL, PP, AF
	 * @return
	 * @throws Exception
	 */
	public List<PolicyDocuments> createDocuments(PolicyTransaction policyTransaction, List<String> documentIds) throws Exception {
		return createDocuments(policyTransaction, convertArrayToJSON(documentIds));
	}
	
	/**
	 * @param policyTransaction
	 * @param jsonDocuments     
	 * { 
	 * 	"documents":[ 
	 * 		{ 
	 * 			"documentId":"PP",
	 *       	"storageType":"DOC" 
	 *     	}, 
	 *     	{ 
	 *     		"documentId":"AF",
	 *          "storageType":"PDF" 
	 *      }
	 *   ] 
	 * }
	 * @return
	 * @throws Exception
	 */
	public List<PolicyDocuments> createDocuments(PolicyTransaction policyTransaction, String jsonDocuments)
			throws Exception {
		List<PolicyDocuments> documents = new ArrayList<PolicyDocuments>();
		InsurancePolicy insurancePolicy = policyTransaction.getPolicyVersion().getInsurancePolicy();

		ObjectMapper mapper = new ObjectMapper();
		JsonNode jsonObject = mapper.readTree(jsonDocuments);
		ArrayNode documentArray = (ArrayNode) jsonObject.get("documents");

		for (JsonNode jsonNode : documentArray) {
			String documentId = jsonNode.get("documentId").asText();
			String storageType = jsonNode.get("storageType").asText();
			String ext;
			switch (storageType) {
			case "PDF":
				ext = ".pdf";
				break;
			case "DOC":
				ext = ".docx";
				break;
			default:
				storageType = "DOC";
				ext = ".docx";
			}

			SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyTransaction
					.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
			if (documentId.equalsIgnoreCase("AF") && !subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
				continue;
			}

			PolicyDocuments document = new PolicyDocuments();
			document.setPolicyTransaction(policyTransaction);
			document.setCreatedDate(LocalDateTime.now());
			document.setUpdatedDate(LocalDateTime.now());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String time = sdf.format(new Date());

			if (documentId.equalsIgnoreCase("QL")) {
				document.setBusinessStatus("G");
				document.setDocumentId("QL");
				String quoteNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getQuoteNum();
				String storageKey = "QL_" + quoteNo + "_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					if (document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductCd()
							.equalsIgnoreCase("TOWING")) {
						QuoteLetter quote = new QuoteLetter(document);
						quote.create(document.getStorageKey());
					} else if (document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductCd()
							.equalsIgnoreCase("NONFLEET")) {
						QuoteLetterNonFleet quote = new QuoteLetterNonFleet(document);
						quote.create(document.getStorageKey());
					}
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					if (subPolicyAuto.getFleetBasis()
							.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)) {
						QuoteLetterSchLHT quote = new QuoteLetterSchLHT(document);
						quote.create(document.getStorageKey());
					} else {
						QuoteLetterLHT quote = new QuoteLetterLHT(document);
						quote.create(document.getStorageKey());
					}
				}
			} else if (documentId.equalsIgnoreCase("PP")) {
				document.setBusinessStatus("G");
				document.setDocumentId("PP");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "PP_" + policyNo + "_" + time + ext : "PP_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					if (insurancePolicy.getProductCd().equalsIgnoreCase("TOWING")) {
						PolicyPackageTowing pp = new PolicyPackageTowing(document);
						pp.create(document.getStorageKey());
					} else if (insurancePolicy.getProductCd().equalsIgnoreCase("NONFLEET")) {
						PolicyPackageNonFleet pp = new PolicyPackageNonFleet(document);
						pp.create(document.getStorageKey());
					}
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					PolicyPackageLHT pp = new PolicyPackageLHT(document);
					pp.create(document.getStorageKey());
				}
			} else if (documentId.equalsIgnoreCase("AF")) {
				document.setBusinessStatus("G");
				document.setDocumentId("AF");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "AF_" + policyNo + "_" + time + ext : "AF_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				AutoFleetSchedule af = new AutoFleetSchedule(document);
				af.create(document.getStorageKey());
			} else if (documentId.equalsIgnoreCase("PC")) {
				document.setBusinessStatus("G");
				document.setDocumentId("PC");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "PC_" + policyNo + "_" + time + ext : "PC_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					PolicyChangeTowing pc = new PolicyChangeTowing(document);
					pc.create(document.getStorageKey());
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					PolicyChangeLHT pc = new PolicyChangeLHT(document);
					pc.create(document.getStorageKey());
				}
			} else if (documentId.equalsIgnoreCase("CA")) {
				document.setBusinessStatus("G");
				document.setDocumentId("CA");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "CA_" + policyNo + "_" + time + ext : "CA_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					PolicyCancelTowing ca = new PolicyCancelTowing(document);
					ca.create(document.getStorageKey());
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					PolicyCancelLHT ca = new PolicyCancelLHT(document);
					ca.create(document.getStorageKey());
				}
			} else if (documentId.equalsIgnoreCase("EP")) {
				document.setBusinessStatus("G");
				document.setDocumentId("EP");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "EP_" + policyNo + "_" + time + ext : "EP_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					PolicyExtension ep = new PolicyExtension(document);
					ep.create(document.getStorageKey());
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					PolicyExtensionLHT ep = new PolicyExtensionLHT(document);
					ep.create(document.getStorageKey());
				}
			} else if (documentId.equalsIgnoreCase("ADJ")) {
				document.setBusinessStatus("G");
				document.setDocumentId("ADJ");
				String policyNo = policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
				String storageKey = !policyNo.equals("") ? "ADJ_" + policyNo + "_" + time + ext : "ADJ_" + time + ext;
				document.setStorageKey(storageKey);
				document.setDistributionTarget("NA");
				document.setStorageType(storageType);
				document.setUpdatedDate(LocalDateTime.now());
				document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());
				if (insurancePolicy.getProductProgram().equalsIgnoreCase("TO")) {
					PolicyAdjustmentsTowing pa = new PolicyAdjustmentsTowing(document);
					pa.create(document.getStorageKey());
				} else if (insurancePolicy.getProductProgram().equalsIgnoreCase("LO")) {
					PolicyAdjustmentsLHT pa = new PolicyAdjustmentsLHT(document);
					pa.create(document.getStorageKey());
				}
			} else {
				throw new Exception("Document ID not valid");
			}

			documents.add(document);
		}

		return documents;
	}
	
	private String convertArrayToJSON(List<String> documentIds) throws JsonProcessingException {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();
		ArrayNode documentArray = mapper.createArrayNode();

		for (String documentId : documentIds) {
			ObjectNode document = mapper.createObjectNode();
			document.put("documentId", documentId);
			document.put("storageType", "DOC");
			documentArray.add(document);
		}
		rootNode.set("documents", documentArray);
		return mapper.writeValueAsString(rootNode);
	}

}
