package com.echelon.services.document;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Table;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyNotes;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class QuoteLetterLHT extends EchelonDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8734847407834214579L;
	private static final LocalDateTime JAN2021 = LocalDateTime.of(2021, 1, 1, 0, 0);
	private String[] fieldNames;
	private Object[] fieldValues;
	private String currDate = "";
	private String referenceNo = "";
	private String brokerName = "";
	private String brokerContact = "";
	private String insuredName = "";
	private String insuredAddressStreet = "";
	private String insuredAddressCity = "";
	private String insuredAddressPostalCode = "";
	private String effectiveDate = "";
	private String expiryDate = "";
	private String lbLimit = "";
	private String ded_ref = "";
	private String totAutoPrem = "";
	private boolean proRata = false;
	private boolean fifthyFifthy = false;
	private boolean quarterly = false;
	private boolean every6m = false;
	private boolean annual = false;
	private String opcf2_pers = "";
	private String opcf8_ded = "N/A";
	private String opcf20_lim = "";
	private String opcf20H_lim = "";
	private String opcf27_pers = "";
	private String opcf27_lim = "";
	private String opcf27_ded = "";
	private String businessType = "";
	private String tll_limit = "";
	private String cgl_lb_limit = "";
	private String cgl_ded = "";
	private String cgl_location = "";
	private String ebe_lim = "";
	private String ebe_ded = "";
	private String lpoll_lim = "";
	private String lpoll_ded = "";
	private String wll_lim = "";
	private String wll_ded = "";
	private String totCGLPrem = "";
	private String spf6_lb_limit = "";
	private String cargo_limit = "";
	private String cargo_ded = ""; 
	private String contcargo_lim = "N/A";
	private String contcargo_prem = "N/A";
	private String totMTCPrem = "";
	private String end_excl = "";
	private String gar_location = "";
	private String gar_lb_limit = "";
	private String gar_dcpd_ded = "";
	private String gar_6_1_limit = "N/A";
	private String gar_6_1_ded = "N/A";
	private String loc_6_4_id = "";
	private String loc_6_4_limit = "";
	private String loc_6_4_ded = "";
	private String totGARPrem = "";
	private String gar_end_desc = "";
	private String mtco_ded = "";
	private String mtco_limit = "";
	private String perocc_ded = "N/A";
	private String totPolPrem = "";
	private String comm = "";
	private String uw_notes = "";
	private String date_prep = "";
	private String prep_by = "";
	private String cp_prem = "";
	private boolean opcf20_exists = false;
	private boolean showCGL = false;
	private boolean showGLEE = false;
	private boolean showEBE = false;
	private boolean showLPOLL = false;
	private boolean showPOLL = false;
	private boolean showWLL = false;
	private boolean showSPF6TPL = false;
	private boolean showSEF96 = false;
	private boolean showSEF99 = false;
	private boolean showCGO = false;
	private boolean showCGOExtOfCov = false;
	private boolean showCDEC = false;
	private boolean showCIDC = false;
	private boolean showEEOL = false;
	private boolean showGAR = false;
	private boolean showGARSection61 = false;
	private boolean showGARSection64 = false;
	private boolean showCDEL = false;
	private boolean showEON111 = false;
	private boolean showIA = false;
	private boolean showCRB = false;
	private boolean showMTCO = false;
	private boolean showEON110 = false;
	private boolean showOPCF2 = false;
	private boolean showOPCF5 = false;
	private boolean showOPCF8 = false;
	private boolean showOPCF9 = false;
	private boolean showOPCF13C = false;
	private boolean showOPCF19 = false;
	private boolean showOPCF20 = false;
	private boolean showOPCF20H = false;
	private boolean showEON20A = false;
	private boolean showOPCF21A = false;
	private boolean showOPCF21B = false;
	private boolean showOPCF23A = false;
	private boolean showOPCF27 = false;
	private boolean showOPCF27B = false;
	private boolean showOPCF30 = false;
	private boolean showOPCF38 = false;
	private boolean showOPCF40 = false;
	private boolean showOPCF43_43A = false;
	private boolean showOPCF44R = false;
	private boolean showEON141 = false;
	private boolean showOPCF25A_EON145 = false;
	private boolean showOPCF25A_GrRec = false;
	private boolean showOPCF47 = false;
	private boolean showOPCF48 = false;
	private boolean isLaterThanJan2021 = false;
	private boolean showCP = false;
	private LinkedHashSet<Endorsement> opcf27BEnds = new LinkedHashSet<Endorsement>();
	
	public QuoteLetterLHT(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage) insurancePolicy;
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());

		// Sub Policies
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		for (SubPolicy<Risk> subPolicy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
				subPolicyCGL = (CGLSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
				subPolicyCGO = (CargoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
				subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPolicy;
			}
		}

		this.policyDocument = policyDocument;
		this.currDate = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "MMMM dd, yyyy");
		this.referenceNo = insurancePolicy.getExternalQuoteNum() != null
				? insurancePolicy.getExternalQuoteNum()
				: "";
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.brokerContact = insurancePolicy.getProducerContactName();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressPostalCode = insurancePolicy.getInsuredAddress().getPostalZip();
		this.effectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermEffDate().toLocalDate(), "M/dd/yyyy");
		this.expiryDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "M/dd/yyyy");
		this.isLaterThanJan2021 = policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermEffDate().compareTo(JAN2021) >= 0;
		this.lbLimit = numberFormat(autoPackage.getLiabilityLimit());
		this.totAutoPrem = numberFormat(subPolicyAuto.getSubPolicyPremium().getWrittenPremium());
		
		if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21B5")) {
			this.fifthyFifthy = true;
			this.annual = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BQ")) {
			this.proRata = true;
			this.quarterly = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BA")) {
			this.proRata = true;
			this.annual = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BS")) {
			this.proRata = true;
			this.every6m = true;
		}
		
		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase("EON110")) {
				this.showEON110 = true;
				this.perocc_ded = "5,000 or $10,000 in case of theft if endorsement 'Change in Deductible Clause' Cargo is applicable";
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON111")) {
				this.showEON111 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("IA")) {
				this.showIA = true;
			}
		}
		
		// AUTO Sub Policy
		List<Endorsement> autoEndorsements = new ArrayList<Endorsement>();
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			autoEndorsements.add(end);
		}

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Set<Endorsement> vehEndorsements = vehicle.getRiskEndorsements();
			Iterator<Endorsement> itVehEnd = vehEndorsements.iterator();
			while (itVehEnd.hasNext()) {
				Endorsement end = itVehEnd.next();
				autoEndorsements.add(end);
			}
		}

		autoEndorsements.sort(Comparator.comparing(Endorsement::getEndorsementsk));
		StringBuilder opcf2PersSB = new StringBuilder();
		StringBuilder opcf27PersSB = new StringBuilder();
		LinkedHashSet<String> opcf27BVehTypes = new LinkedHashSet<String>();

		for (Endorsement end : autoEndorsements) {

			if (end.getEndorsementCd().equalsIgnoreCase("OPCF2")) {
				this.showOPCF2 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF2_Pers")) {
							if (opcf2PersSB.length() > 0) {
								opcf2PersSB.append(", ");
							}
							opcf2PersSB.append(extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
				this.showOPCF5 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF8")) {
				this.showOPCF8 = true;
				if (this.opcf8_ded.equals("N/A")) {
					this.opcf8_ded = numberFormat(end.getDeductible1amount());
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF9")) {
				this.showOPCF9 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF13C")) {
				this.showOPCF13C = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
				this.showOPCF19 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF20")) {
				this.opcf20_exists = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON20A")) {
				this.showEON20A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21A")) {
				this.showOPCF21A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21B")) {
				this.showOPCF21B = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
				this.showOPCF20 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27")) {
				this.showOPCF27 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27_Pers") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equalsIgnoreCase("")) {
							if (opcf27PersSB.length() > 0) {
								opcf27PersSB.append(", ");
							}
							opcf27PersSB.append(extData.getColumnValue());
						}
					}
				}
				this.opcf27_lim = numberFormat(end.getLimit1amount());
				this.opcf27_ded = numberFormat(end.getDeductible1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) {
				this.showOPCF27B = true;
				
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")
								&& extData.getColumnValue() != null) {
							if (!opcf27BVehTypes.contains(extData.getColumnValue())) {
								opcf27BVehTypes.add(extData.getColumnValue());
								this.opcf27BEnds.add(end);
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF30")) {
				this.showOPCF30 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF38")) {
				this.showOPCF38 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF40")) {
				this.showOPCF40 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF43")
					|| end.getEndorsementCd().equalsIgnoreCase("OPCF43A")) {
				this.showOPCF43_43A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) {
				this.showOPCF44R = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON141")) {
				this.showEON141 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25AW")) {
				this.showOPCF25A_EON145 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25AR")) {
				this.showOPCF25A_GrRec = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF47")) {
				this.showOPCF47 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF48")) {
				this.showOPCF48 = true;
			}
		}

		this.opcf2_pers = opcf2PersSB.toString();
		this.opcf27_pers = opcf27PersSB.toString();

		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			LookupTableItem lookupBusinessDescItem = lookupConfig
					.getConfigLookup(ConfigConstants.LOOKUPTABLE_BusinessDescriptions).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getBusinessDescription()))
					.findFirst().orElse(null);
			this.businessType = lookupBusinessDescItem.getItemValue();
		}
		
		if (subPolicyCGL != null) {
			this.showCGL = true;
			this.showCDEL = true;
			for (Coverage cove : subPolicyCGL.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("TLL")) {
					this.tll_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("SPF6TPL")) {
					this.showSPF6TPL = true;
					this.spf6_lb_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("EBE")) {
					this.showEBE = true;
					this.ebe_lim = numberFormat(cove.getLimit1());
					this.ebe_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("LPOLL")) {
					this.showLPOLL = true;
					this.lpoll_lim = numberFormat(cove.getLimit1());
					this.lpoll_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("WLL")) {
					this.showWLL = true;
					this.wll_lim = numberFormat(cove.getLimit1());
					this.wll_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			for (Endorsement end : subPolicyCGL.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().equalsIgnoreCase("POLL")) {
					this.showPOLL = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("CRB")) {
					this.showCRB = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("SEF96")){
					this.showSEF96 = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("SEF99")){
					this.showSEF99 = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("GLEE")){
					this.showGLEE = true;
				}
			}
			
			this.cgl_lb_limit = numberFormat(subPolicyCGL.getCglLimit());
			this.cgl_ded = numberFormat(subPolicyCGL.getCglDeductible());
			this.totCGLPrem = numberFormat(subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
			
			StringBuilder locationSB = new StringBuilder();
			Set<PolicyLocation> policyLocations = subPolicyCGL.getCglLocations().stream().map(CGLLocation::getPolicyLocation).collect(Collectors.toSet());
			for (PolicyLocation pl : policyLocations.stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toSet())) {
				if (locationSB.length() > 0) {
					locationSB.append("\n");
				}

				locationSB.append(pl.getLocationAddress().getAddressLine1());
				locationSB.append(", ");
				locationSB.append(pl.getLocationAddress().getCity());
				locationSB.append(", Ontario ");
				locationSB.append(pl.getLocationAddress().getPostalZip());
			}
			this.cgl_location = locationSB.length() == 0 ? "No other locations" : locationSB.toString();
		}
		
		if (subPolicyCGO != null) {
			this.showCGO = true;
			for (Coverage cove : subPolicyCGO.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("MTCC")) {
					this.showCGOExtOfCov = true;
					this.cargo_limit = numberFormat(cove.getLimit1());
					this.cargo_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("MTCO")) {
					this.showMTCO = true;
					this.mtco_ded = numberFormat(cove.getDeductible1());
					this.mtco_limit = numberFormat(cove.getLimit1());
				}
			}
			this.totMTCPrem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());

			StringBuilder endExclSB = new StringBuilder();
			for (Endorsement end : subPolicyCGO.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().startsWith("CDEC")) {
					this.showCDEC = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("CTC")) {
					this.contcargo_lim = numberFormat(end.getLimit1amount());
					this.contcargo_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CIDC")) {
					this.showCIDC = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("TDGC")
						|| end.getEndorsementCd().equalsIgnoreCase("TDGPL")
						|| end.getEndorsementCd().equalsIgnoreCase("FOE")
						|| end.getEndorsementCd().equalsIgnoreCase("CEMTC")
						|| end.getEndorsementCd().equalsIgnoreCase("CEMTC")
						|| end.getEndorsementCd().equalsIgnoreCase("TORV")
						|| end.getEndorsementCd().equalsIgnoreCase("TOCA")) {
					this.showEEOL = true;
					if (endExclSB.length() > 0) {
						endExclSB.append("\n");
					}
					endExclSB.append(end.getDescription());
				}
			}
			this.end_excl = endExclSB.toString();
		}
		
		if (subPolicyGAR != null) {
			this.showGAR = true;
			LinkedHashSet<String> garEndDesc = new LinkedHashSet<String>();
			
			for (Coverage cove : subPolicyGAR.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					this.gar_dcpd_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL61")) {
					this.showGARSection61 = true;
					this.gar_6_1_limit = numberFormat(cove.getLimit1());
					this.gar_6_1_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			for (Endorsement end : subPolicyGAR.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().startsWith("OEF")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					LookupTableItem lookupEndorsementDescItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementDescriptions).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					String description = null;

					if (lookupEndorsementCodeItem != null && lookupEndorsementDescItem != null) {
						String endCode = lookupEndorsementCodeItem.getItemValue();
						String endDesc = lookupEndorsementDescItem.getItemValue();
						description = endCode + " " + endDesc;
					} else {
						description = end.getDescription();
					}

					garEndDesc.add(description);
				}
			}
			
			StringBuilder locationSB = new StringBuilder();
			StringBuilder locatinSP64Id = new StringBuilder();
			StringBuilder locatinSP64Limit = new StringBuilder();
			StringBuilder locatinSP64Ded = new StringBuilder();
			List<PolicyLocation> policyLocations = getPolicyLocations();
			
			if(subPolicyGAR.getSubPolicyNamedInsured() != null && !subPolicyGAR.getSubPolicyNamedInsured().isEmpty()) {
				locationSB.append(subPolicyGAR.getSubPolicyNamedInsured());
			}
			
			for (PolicyLocation pl : policyLocations) {
				Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
				
				if (coveSP64 != null) {
					if (locationSB.length() > 0) {
						locationSB.append("\n");
					}
					locationSB.append(pl.getLocationId());
					locationSB.append(" ");
					locationSB.append(pl.getLocationAddress().getAddressLine1());
					locationSB.append(", ");
					locationSB.append(pl.getLocationAddress().getCity());
					locationSB.append(", Ontario ");
					locationSB.append(pl.getLocationAddress().getPostalZip());
					locationSB.append("      Maximum # of vehicles: ");
					locationSB.append(numberFormat(pl.getNumVehicles()));
				
					this.showGARSection64 = true;
					if (locatinSP64Id.length() > 0 || locatinSP64Limit.length() > 0 || locatinSP64Ded.length() > 0) {
						locatinSP64Id.append("\n");
						locatinSP64Limit.append("\n");
						locatinSP64Ded.append("\n");
					}

					locatinSP64Id.append("Location: " + pl.getLocationId());
					locatinSP64Limit.append("Limit: $" + numberFormat(coveSP64.getLimit1()));
					locatinSP64Ded.append("Deductible: $" + numberFormat(coveSP64.getDeductible1()));
				}
			}
			
			StringBuilder garEndDescSB = new StringBuilder();
			Iterator<String> itGAREndDesc = garEndDesc.iterator();
			while (itGAREndDesc.hasNext()) {
				if (garEndDescSB.length() > 0) {
					garEndDescSB.append("\n");
				}
				garEndDescSB.append(itGAREndDesc.next());
			}
			
			this.gar_location = locationSB.toString();
			this.gar_lb_limit = numberFormat(autoPackage.getLiabilityLimit());
			this.loc_6_4_id = locatinSP64Id.toString();
			this.loc_6_4_limit = locatinSP64Limit.toString();
			this.loc_6_4_ded = locatinSP64Ded.toString();
			this.gar_end_desc = garEndDescSB.toString();
			this.totGARPrem = numberFormat(subPolicyGAR.getSubPolicyPremium().getWrittenPremium());
		}
		
		if (subPolicyCP != null) {
			this.showCP = true;
			this.cp_prem = numberFormat(subPolicyCP.getSubPolicyPremium().getWrittenPremium());
		}
		
		this.totPolPrem = numberFormat(
				policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		this.comm = numberFormat(policyDocument.getPolicyTransaction().getPolicyVersion().getProducerCommissionRate());

		Set<PolicyNotes> notes = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyNotes();
		Iterator<PolicyNotes> itNotes = notes.iterator();
		StringBuilder notesSB = new StringBuilder();
		while (itNotes.hasNext()) {
			PolicyNotes pn = itNotes.next();
			if (!pn.getIsInternal()) {
				if (notesSB.length() > 0) {
					notesSB.append("\n");
				}
				notesSB.append(pn.getNoteDetails());
			}
		}
		this.uw_notes = notesSB.toString();
		if (insurancePolicy.getQuotePreparedDate() != null) {
			this.date_prep = formatDate(insurancePolicy.getQuotePreparedDate().toLocalDate(), "MMMM dd, yyyy");
		}
		this.prep_by = insurancePolicy.getQuotePreparedBy();
	}
	
	public String getTemplatePath() {
		return "templates/QuoteLetterLHT_tmp.docx";
	}

	public void create(String storageKey) throws Exception {
		if (this.opcf20_exists) {
			setOPCF20Row();
		}
		initializeFields();
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if (attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		hideSections(doc);
		setFirstTable(doc);
		setSecondTable(doc);
		doc.save(attachPath + File.separator + storageKey);
	}

	public void initializeFields() {
		fieldNames = new String[] { "Date", "Reference_No", "Broker_Name", "Broker_Contact", "Insured_Name",
				"Insured_Address_Street", "Insured_Address_City", "Insured_Address_Postal_Code", "Effective_Date",
				"Expiry_Date", "LB_Limit", "Ded_Ref", "TotAutoPrem", "ProRata", "FifthyFifthy", "Quarterly", "Every6m",
				"Annual", "OPCF2_Pers", "OPCF8_Ded", "OPCF20_Lim", "OPCF20H_Lim", "OPCF27_Pers", "OPCF27_Lim",
				"OPCF27_Ded", "BusinessType", "TLL_Limit", "CGL_LB_Limit", "CGL_Ded", "CGL_Location", "EBE_Lim",
				"EBE_Ded", "LPOLL_Lim", "LPOLL_Ded", "WLL_Lim", "WLL_Ded", "TotCGLPrem", "SPF6_LB_Limit", "Cargo_Limit",
				"Cargo_Ded", "ContCargo_Limit", "ContCargo_Prem", "End_Excl", "TotMTCPrem", "GAR_Location",
				"GAR_LB_Limit", "GAR_DCPD_Ded", "GAR_6_1_Limit", "GAR_6_1_Ded", "Loc_6_4_Id", "Loc_6_4_Limit",
				"Loc_6_4_Ded", "GAR_End_Desc", "TotGARPrem", "MTCO_Ded", "MTCO_Lim", "PerOcc_Ded", "TotPolPrem", "Comm",
				"UW_Notes", "Date_Prep", "Prep_By", "CPPrem" };
		fieldValues = new Object[] { currDate, referenceNo, brokerName, brokerContact, insuredName,
				insuredAddressStreet, insuredAddressCity, insuredAddressPostalCode, effectiveDate, expiryDate, lbLimit,
				ded_ref, totAutoPrem, proRata, fifthyFifthy, quarterly, every6m, annual, opcf2_pers, opcf8_ded,
				opcf20_lim, opcf20H_lim, opcf27_pers, opcf27_lim, opcf27_ded, businessType, tll_limit, cgl_lb_limit,
				cgl_ded, cgl_location, ebe_lim, ebe_ded, lpoll_lim, lpoll_ded, wll_lim, wll_ded, totCGLPrem,
				spf6_lb_limit, cargo_limit, cargo_ded, contcargo_lim, contcargo_prem, end_excl, totMTCPrem,
				gar_location, gar_lb_limit, gar_dcpd_ded, gar_6_1_limit, gar_6_1_ded, loc_6_4_id, loc_6_4_limit,
				loc_6_4_ded, gar_end_desc, totGARPrem, mtco_ded, mtco_limit, perocc_ded, totPolPrem, comm, uw_notes,
				date_prep, prep_by, cp_prem };
	}
	
	private void setOPCF20Row() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20 = true;
					this.opcf20_lim = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")) {
					this.showOPCF20H = true;
					this.opcf20H_lim = numberFormat(end.getLimit1amount());
				}
			}
		}
	}
	
	private void setOPCF27BRows(Document doc, Table opcfTable, Row typeOfVehicleRow, Row amtOfInsuranceRow,
			Row deductibleRow) throws Exception {

		Row refRow = deductibleRow;

		for (Endorsement end : opcf27BEnds) {

			String typeOfVehicle = "";
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")) {
						typeOfVehicle = extData.getColumnValue();
					}
				}
			}
			String limit = numberFormat(end.getLimit1amount());
			String deductible = numberFormat(end.getDeductible1amount());

			Row newTypeOfVehicleRow = (Row) typeOfVehicleRow.deepClone(true);
			Cell typeOfVehicleCell = newTypeOfVehicleRow.getCells().get(2);
			typeOfVehicleCell.getFirstParagraph().removeAllChildren();
			Run typeOfVehicleRun = new Run(doc, typeOfVehicle);
			typeOfVehicleRun.getFont().setName("Arial");
			typeOfVehicleRun.getFont().setSize(8);
			typeOfVehicleRun.getFont().setBold(true);
			newTypeOfVehicleRow.getCells().get(2).getFirstParagraph().appendChild(typeOfVehicleRun);
			opcfTable.insertAfter(newTypeOfVehicleRow, refRow);
			refRow = newTypeOfVehicleRow;

			Row newAmtOfInsuranceRow = (Row) amtOfInsuranceRow.deepClone(true);
			Cell amtOfInsuranceCell = newAmtOfInsuranceRow.getCells().get(2);
			amtOfInsuranceCell.getFirstParagraph().removeAllChildren();
			Run amtOfInsuranceRun = new Run(doc, "$" + limit);
			amtOfInsuranceRun.getFont().setName("Arial");
			amtOfInsuranceRun.getFont().setSize(8);
			amtOfInsuranceRun.getFont().setBold(true);
			newAmtOfInsuranceRow.getCells().get(2).getFirstParagraph().appendChild(amtOfInsuranceRun);
			opcfTable.insertAfter(newAmtOfInsuranceRow, refRow);
			refRow = newAmtOfInsuranceRow;

			Row newDeductibleRow = (Row) deductibleRow.deepClone(true);
			Cell dedCell = newDeductibleRow.getCells().get(2);
			dedCell.getFirstParagraph().removeAllChildren();
			Run dedRun = new Run(doc, "$" + deductible);
			dedRun.getFont().setName("Arial");
			dedRun.getFont().setSize(8);
			dedRun.getFont().setBold(true);
			newDeductibleRow.getCells().get(2).getFirstParagraph().appendChild(dedRun);
			opcfTable.insertAfter(newDeductibleRow, refRow);
			refRow = newDeductibleRow;

		}

		opcfTable.removeChild(typeOfVehicleRow);
		opcfTable.removeChild(amtOfInsuranceRow);
		opcfTable.removeChild(deductibleRow);
	}
	
	private void setFirstTable(Document doc) {
		// Main table
		Table mainTable = doc.getFirstSection().getBody().getTables().get(0);

		// Vehicle table
		Row mainRow = mainTable.getRows().get(21);
		Cell mainCell = mainRow.getFirstCell();
		Table firstTable = mainCell.getTables().get(0);
		Row firstTableRow = firstTable.getRows().get(3);
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			if (vehicle.getVehicleDescription().equals("OP27")) {
				continue;
			}
			Set<Coverage> coverages = vehicle.getRiskCoverages();
			String dcpd_ded = "N/A";
			String sp_ded = "N/A";
			String comp_ded = "N/A";
			String coll_ded = "N/A";
			String ap_ded = "N/A";
			for (Coverage coverage : coverages) {
				if (coverage.getCoverageCode().equalsIgnoreCase("DCPD")) {
					if (coverage.getDeductible1() != null) {
						dcpd_ded = numberFormat(coverage.getDeductible1());
					}
				} else if (coverage.getCoverageCode().equalsIgnoreCase("SP")) {
					if (coverage.getDeductible1() != null) {
						sp_ded = numberFormat(coverage.getDeductible1());
					}
				} else if (coverage.getCoverageCode().equalsIgnoreCase("CM")) {
					if (coverage.getDeductible1() != null) {
						comp_ded = numberFormat(coverage.getDeductible1());
					}
				} else if (coverage.getCoverageCode().equalsIgnoreCase("CL")) {
					if (coverage.getDeductible1() != null) {
						coll_ded = numberFormat(coverage.getDeductible1());
					}
				} else if (coverage.getCoverageCode().equalsIgnoreCase("AP")) {
					if (coverage.getDeductible1() != null) {
						ap_ded = numberFormat(coverage.getDeductible1());
					}
				}
			}

			Row newVehicleRow = (Row) firstTableRow.deepClone(true);

			// Veh_Desc
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, vehicle.getDisplayString());
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

			// DCPD_Ded
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, dcpd_ded);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			if (!dcpd_ded.equalsIgnoreCase("N/A")) {
				newRun2.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

			// SP_Ded
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, sp_ded);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			if(!sp_ded.equalsIgnoreCase("N/A")) {
				newRun3.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

			// COMP_Ded
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, comp_ded);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			if(!comp_ded.equalsIgnoreCase("N/A")) {
				newRun4.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

			// COLL_Ded
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, coll_ded);
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			if(!coll_ded.equalsIgnoreCase("N/A")) {
				newRun5.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

			// AP_Ded
			newVehicleRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun6 = new Run(doc, ap_ded);
			newRun6.getFont().setName("Arial");
			newRun6.getFont().setSize(8);
			if(!ap_ded.equalsIgnoreCase("N/A")) {
				newRun6.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(5).getFirstParagraph().appendChild(newRun6);

			firstTable.insertAfter(newVehicleRow, firstTableRow);
			firstTableRow = newVehicleRow;
		}

		firstTable.removeChild(firstTable.getRows().get(3));
	}

	private void setSecondTable(Document doc) throws Exception {
		// Main table
		Table mainTable = doc.getFirstSection().getBody().getTables().get(0);
		
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());

		// Vehicle table
		Row mainRow = mainTable.getRows().get(21);
		Cell mainCell = mainRow.getFirstCell();
		Table secondTable = mainCell.getTables().get(1);
		Row secondTableRow = secondTable.getRows().get(1);
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Row newVehicleRow = (Row) secondTableRow.deepClone(true);
			
			String numOfUnits = vehicle.getNumberOfUnits() != null ? numberFormat(vehicle.getNumberOfUnits()) : "";
			String vehDesc = vehicle.getDisplayString() != null ? vehicle.getDisplayString() : "";
			String unitRate = "$" + numberFormat(vehicle.getCoverageDetail().getCoveragePremium().getWrittenPremium() / vehicle.getNumberOfUnits());
			if (vehicle.getVehicleDescription().equals("OP27")) {
				numOfUnits = "";
				vehDesc = "OPCF 27B";
				unitRate = "";
			}
			String location = "";
			if (vehicle.getUnitExposure() != null) {
				LookupTableItem lookupBusinessDescItem = lookupConfig
						.getConfigLookup(ConfigConstants.LOOKUPTABLE_UnitExposure).stream()
						.filter(o -> o.getItemKey().equalsIgnoreCase(vehicle.getUnitExposure())).findFirst()
						.orElse(null);
				location = lookupBusinessDescItem.getItemValue();
			}

			// NumofUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, numOfUnits);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, vehDesc);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

			// location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, location);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, unitRate);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			String vehPrem = vehicle.getRiskPremium().getWrittenPremium() != null
					? "$" + numberFormat(vehicle.getRiskPremium().getWrittenPremium())
					: "";
			Run newRun5 = new Run(doc, vehPrem);
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

			secondTable.insertAfter(newVehicleRow, secondTableRow);
			secondTableRow = newVehicleRow;
		}

		secondTable.removeChild(secondTable.getRows().get(1));
	}
	
	private List<PolicyLocation> getPolicyLocations() {
		List<PolicyLocation> locations = new ArrayList<PolicyLocation>();
		for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
			if (risk.getRiskType().equalsIgnoreCase(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)) {
				PolicyLocation pl = (PolicyLocation) risk;
				locations.add(pl);
			}
		}
		locations.sort(Comparator.comparing(PolicyLocation::getLocationId));

		return locations;
	}
	
	private void hideSections(Document doc) throws Exception {
		// table for AUTO
		Table opcfTable = doc.getSections().get(0).getBody().getTables().get(1);
		// table for CGL, SPF6TPL and CGO
		Table cglTable = doc.getSections().get(0).getBody().getTables().get(2);
		//table for GAR
		Table garTable = doc.getFirstSection().getBody().getTables().get(3);
		//table for other endorsements
		Table otherTable = doc.getFirstSection().getBody().getTables().get(4);
		//table for Commercial Property
		Table cpTable = doc.getFirstSection().getBody().getTables().get(5);
		
		//OPCF Rows
		Row opcfHeaderRow = opcfTable.getRows().get(0);
		Row opcf2Row = opcfTable.getRows().get(1);
		Row opcf5Row = opcfTable.getRows().get(2);
		Row opcf8Row = opcfTable.getRows().get(3);
		Row opcf9Row = opcfTable.getRows().get(4);
		Row opcf13CRow = opcfTable.getRows().get(5);
		Row opcf19Row = opcfTable.getRows().get(6);
		Row opcf20Row = opcfTable.getRows().get(7);
		Row opcf20HRow = opcfTable.getRows().get(8);
		Row eon20ARow = opcfTable.getRows().get(9);
		Row opcf21ARow = opcfTable.getRows().get(10);
		Row opcf21BRow = opcfTable.getRows().get(11);
		Row opcf23ARow = opcfTable.getRows().get(12);
		Row opcf27Row = opcfTable.getRows().get(13);
		Row opcf27B_1Row = opcfTable.getRows().get(14);
		Row opcf27B_2Row = opcfTable.getRows().get(15);
		Row opcf27B_3Row = opcfTable.getRows().get(16);
		Row opcf27B_4Row = opcfTable.getRows().get(17);
		Row opcf30Row = opcfTable.getRows().get(18);
		Row opcf38Row = opcfTable.getRows().get(19);
		Row opcf40Row = opcfTable.getRows().get(20);
		Row opcf43_43ARow = opcfTable.getRows().get(21);
		Row opcf44RRow = opcfTable.getRows().get(22);
		Row eon141Row = opcfTable.getRows().get(23);
		Row opcf25Aeon145Row = opcfTable.getRows().get(24);
		Row opcf25AGrRecRow = opcfTable.getRows().get(25);
		Row opcf47Row = opcfTable.getRows().get(26);
		Row opcf48Row = opcfTable.getRows().get(27);
		Row opcfFooterRow = opcfTable.getRows().get(28);
		
		// CGL, SPF6 and Cargo Rows
		List<Row> cglRows = new ArrayList<Row>();
		List<Row> spf6tplRows = new ArrayList<Row>();
		List<Row> cgoRows = new ArrayList<Row>();
		List<Row> cgoExtOfCovRows = new ArrayList<Row>();
		List<Row> cidcRows = new ArrayList<Row>();
		List<Row> restRows = new ArrayList<Row>();
		Row gleeRow = cglTable.getRows().get(12);
		Row ebeRow = cglTable.getRows().get(13);
		Row lpollRow = cglTable.getRows().get(14);
		Row pollRow = cglTable.getRows().get(15);
		Row wllRow = cglTable.getRows().get(16);
		Row sef96Row = cglTable.getRows().get(22);
		Row sef99Row = cglTable.getRows().get(23);
		Row eeolRow = cglTable.getRows().get(40);
		RowCollection coll = cglTable.getRows();
		Iterator<Row> it = coll.iterator();
		int i = 0;
		while (it.hasNext()) {

			/*
			 * Rows from 0 to 19 belongs to CGL section 
			 * Rows from 20, 21 and 24 belongs to SPF6TPL
			 * Row 22 belongs to SEF96
			 * Row 23 belongs to SEF99
			 * Rows from 25 to 28 belongs to CGO
			 * Rows from 29 to 35 belongs to extension of coverage
			 * Rows from 36 to 39 belongs to Change in Deductible clause
			 * Row 40 belongs to EEOL
			 * Rows from 41 to 42 rest of table
			 */
			Row row = it.next();
			if (i <= 19) {
				cglRows.add(row);
			} else if ((i >= 20 && i <= 21) || i == 24) {
				spf6tplRows.add(row);
			} else if (i >= 25 && i <= 28) {
				cgoRows.add(row);
			} else if (i >= 29 && i <= 35) {
				cgoExtOfCovRows.add(row);
			} else if (i >= 36 && i <= 39) {
				cidcRows.add(row);
			} else if (i >= 41){
				restRows.add(row);
			}

			i++;
		}
		
		// Garage Rows
		Row garSection6Row = garTable.getRows().get(4);
		Row garSection61Row = garTable.getRows().get(5);
		Row garSection64_1Row = garTable.getRows().get(6);
		Row garSection64_2Row = garTable.getRows().get(7);
		
		//Other Endorsements Rows
		Row cdecRow = otherTable.getRows().get(1);
		Row cdelRow = otherTable.getRows().get(2);
		Row eon111Row = otherTable.getRows().get(3);
		Row iaRow = otherTable.getRows().get(4);
		Row crbRow = otherTable.getRows().get(5);
		Row mtcoRow = otherTable.getRows().get(6);
		Row eon110Row = otherTable.getRows().get(7);
		
		if(!this.showOPCF2) {
			opcfTable.removeChild(opcf2Row);
		}
		
		if(!this.showOPCF5) {
			opcfTable.removeChild(opcf5Row);
		}
		
		if(!this.showOPCF8) {
			opcfTable.removeChild(opcf8Row);
		}
		
		if(!this.showOPCF9) {
			opcfTable.removeChild(opcf9Row);
		}
		
		if(!this.showOPCF13C) {
			opcfTable.removeChild(opcf13CRow);
		}
		
		if(!this.showOPCF19) {
			opcfTable.removeChild(opcf19Row);
		}
		
		if(!this.showOPCF20) {
			opcfTable.removeChild(opcf20Row);
		}
		
		if(!this.showOPCF20H) {
			opcfTable.removeChild(opcf20HRow);
		}
		
		if(!this.showEON20A) {
			opcfTable.removeChild(eon20ARow);
		}
		
		if(!this.showOPCF21A) {
			opcfTable.removeChild(opcf21ARow);
		}
		
		if(!this.showOPCF21B) {
			opcfTable.removeChild(opcf21BRow);
		}
		
		if(!this.showOPCF23A) {
			opcfTable.removeChild(opcf23ARow);
		}
		
		if(!this.showOPCF27) {
			opcfTable.removeChild(opcf27Row);
		}
		
		if (!this.showOPCF27B) {
			opcfTable.removeChild(opcf27B_1Row);
			opcfTable.removeChild(opcf27B_2Row);
			opcfTable.removeChild(opcf27B_3Row);
			opcfTable.removeChild(opcf27B_4Row);
		} else {
			setOPCF27BRows(doc, opcfTable, opcf27B_2Row, opcf27B_3Row, opcf27B_4Row);
		}
		
		if(!this.showOPCF30) {
			opcfTable.removeChild(opcf30Row);
		}
		
		if(!this.showOPCF38) {
			opcfTable.removeChild(opcf38Row);
		}
		
		if(!this.showOPCF40) {
			opcfTable.removeChild(opcf40Row);
		}
		
		if(!this.showOPCF43_43A) {
			opcfTable.removeChild(opcf43_43ARow);
		}
		
		if(!this.showOPCF44R) {
			opcfTable.removeChild(opcf44RRow);
		}
		
		if(!this.showEON141) {
			opcfTable.removeChild(eon141Row);
		}
		
		if(!this.showOPCF25A_EON145) {
			opcfTable.removeChild(opcf25Aeon145Row);
		}
		
		if(!this.showOPCF25A_GrRec) {
			opcfTable.removeChild(opcf25AGrRecRow);
		}
		
		if(!this.showOPCF47) {
			opcfTable.removeChild(opcf47Row);
		}
		
		if(!this.showOPCF48) {
			opcfTable.removeChild(opcf48Row);
		}
		
		if (!this.showOPCF2 && !this.showOPCF5 && !this.showOPCF8 && !this.showOPCF9 && !this.showOPCF13C
				&& !this.showOPCF19 && !this.showOPCF20 && !this.showEON20A && !this.showOPCF21A && !this.showOPCF21B
				&& !this.showOPCF23A && !this.showOPCF27 && !this.showOPCF27B && !this.showOPCF30 && !this.showOPCF38
				&& !this.showOPCF40 && !this.showOPCF43_43A && !this.showOPCF44R && !this.showEON141
				&& !this.showOPCF25A_EON145 && !this.showOPCF25A_GrRec && !this.showOPCF47 && !this.showOPCF48) {
			opcfTable.removeChild(opcfHeaderRow);
			opcfTable.removeChild(opcfFooterRow);
		}
		
		if(!this.showGLEE && this.showCGL) {
			cglTable.removeChild(gleeRow);
		}
		
		if(!this.showEBE && this.showCGL) {
			cglTable.removeChild(ebeRow);
		}
		
		if(!this.showLPOLL && this.showCGL) {
			cglTable.removeChild(lpollRow);
		}
		
		if(!this.showPOLL && this.showCGL) {
			cglTable.removeChild(pollRow);
		}
		
		if(!this.showWLL && this.showCGL) {
			cglTable.removeChild(wllRow);
		}
		
		if(!this.showCGL) {
			removeRowsFromTable(doc, cglTable, cglRows);
		}
		
		if(!this.showSPF6TPL) {
			removeRowsFromTable(doc, cglTable, spf6tplRows);
		}
		
		if (!this.showSEF96) {
			cglTable.removeChild(sef96Row);
		}

		if (!this.showSEF99) {
			cglTable.removeChild(sef99Row);
		}
		
		if(!this.showCGOExtOfCov) {
			removeRowsFromTable(doc, cglTable, cgoExtOfCovRows);
		}
		
		if(!this.showCIDC) {
			removeRowsFromTable(doc, cglTable, cidcRows);
		}
		
		if(!this.showEEOL) {
			cglTable.removeChild(eeolRow);
		}
		
		if(!this.showCGO) {
			removeRowsFromTable(doc, cglTable, cgoRows);
			removeRowsFromTable(doc, cglTable, restRows);
		}
		
		if (!this.showGARSection61) {
			garTable.removeChild(garSection61Row);
		}

		if (!this.showGARSection64) {
			garTable.removeChild(garSection64_1Row);
			garTable.removeChild(garSection64_2Row);
		}

		if (!this.showGARSection61 && !this.showGARSection64) {
			garTable.removeChild(garSection6Row);
		}
		
		if(!this.showGAR) {
			garTable.removeAllChildren();
		}
		
		if(!this.showCDEC || !this.isLaterThanJan2021) {
			otherTable.removeChild(cdecRow);
		}
		
		if(!this.showCDEL) {
			otherTable.removeChild(cdelRow);
		}
		
		if(!this.showEON111) {
			otherTable.removeChild(eon111Row);
		}
		
		if(!this.showIA) {
			otherTable.removeChild(iaRow);
		}
		
		if(!this.showCRB) {
			otherTable.removeChild(crbRow);
		}
		
		if(!this.showMTCO) {
			otherTable.removeChild(mtcoRow);
		}
		
		if(!this.showEON110) {
			otherTable.removeChild(eon110Row);
		}
		
		if (!this.showCP) {
			cpTable.removeAllChildren();
		}
	}
	
	private void removeRowsFromTable(Document doc, Table table, List<Row> rows) {
		for (Row row : rows) {
			table.removeChild(row);
		}
	}

}
