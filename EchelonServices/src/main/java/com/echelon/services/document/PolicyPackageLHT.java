package com.echelon.services.document;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Paragraph;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.entities.Location;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertyLocation;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyPackageLHT extends PolicyPackage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 483959676067307278L;
	private String lpoll_limit = "N/A";
	private String lpoll_ded = "N/A";
	private String poll_limit = "N/A";
	private String wll_limit = "N/A";
	private String wll_ded = "N/A";
	private String mtco_limit = "N/A";
	private String mtco_ded = "N/A";
	private String mtco_prem = "";
	private String cont_limit = "N/A";
	private String cont_ded = "N/A";
	private String petp_limit = "N/A";
	private String opcf21A_prem = "N/A";
	private String opcf21A_rcpt = "N/A";
	private String opcf21A_rate = "N/A";
	private String min_prem_pct = "N/A";
	private String min_prem = "N/A";
	private String end_cicd_text = "";
	private String end_cicd_no = "";
	private String eon110_ded = "N/A";
	private String opcf4A_ext_type = "";
	private String opcf4A_limit = "";
	private String opcf4A_cover = "";
	private String opcf4B_mat_type = "";
	private String opcf4B_limit = "";
	private String opcf4B_cover = "";
	private String opcf4B_prem = "";
	private String opcf5C_limit = "";
	private String opcf5C_prem = "";
	private String security_system_req = "";
	private boolean opcf25AW_noDed = false;
	private boolean opcf25AW_withDed = false;
	private String opcf25AW_ded = "";
	private boolean ctcExists = false;
	private String cont_cargo = "";
	private String ctc_prem = "";
	private String cp_locations = "";
	private String commProp_prem = "";
	private String sef_ends = "";
	private boolean mtco_exists = false;
	private boolean mtcc_exists = false;
	private String eon20A_limit = "";
	private String eon119_ownerName = "";
	private String eon119_vehDescription = "";
	private String lisc_company_name = "";
	private String lisc_company_address = "";
	private String mtgt_prem = "";
	private String torv_limita = "";
	private String torv_limitb = "";
	private String torv_limitc = "";
	private String torv_ded1 = "";
	private String torv_ded2 = "";
	private String torv_prem = ""; 
	private String toca_limita = "";
	private String toca_limitb = "";
	private String toca_limitc = "";
	private String toca_ded1 = "";
	private String toca_ded2 = "";
	private String toca_prem = ""; 
	private String tdgpl_class = "";
	private String cemtc_exclusion = "";
	private String docc_prem = "";
	private String docc_type = "";
	private String aloccgl_prem = "";
	private String dloccgl_prem = "";
	private String end110_no = "";
	private List<Endorsement> liscEnds = new ArrayList<Endorsement>();
	private List<Endorsement> eon119Ends = new ArrayList<Endorsement>();
	private boolean checked = true;
	private boolean unchecked = false;
	private Section opcf4ASec = null;
	private Section opcf4BSec = null;
	private Section opcf5CSec = null;
	private Section opcf25AWSec = null;
	private Section opcf25ARSec = null;
	private Section cgoDecOFSec = null;
	private Section cpDecSec = null;
	private Section peSec = null;
	private Section eon20ASec = null;
	private Section cidcSec = null;
	private Section cidcofSec = null;
	private Section eon119Sec = null;
	private Section eon141Sec = null;
	private Section crbSec = null;
	private Section apxASec = null;
	private Section tlleSec = null;
	private Section wlleSec = null;
	private Section liscSec = null;
	private Section mtgtSec = null;
	private Section mtgtofSec = null;
	private Section mobiSec = null;
	private Section torvSec = null;
	private Section tocaSec = null;
	private Section tdgSec = null;
	private Section tdgplSec = null;
	private Section tdgcSec = null;
	private Section cemtcSec = null;
	private Section cemtcofSec = null;
	private Section foeSec = null;
	private Section doccSec = null;
	private Section aoccofSec = null;
	private Section aloccglSec = null;
	private Section dloccglSec = null;
	private Section aloccpSec = null;
	private Section dloccpSec = null;
	private Section mocmcSec = null;
	private Section aloccSec = null;
	private Section dloccSec = null;
	private Section eon115Sec = null;
	private Section eon115_2Sec = null;
	private Section eon145Sec = null;
	private boolean showOPCF4ASection = false;
	private boolean showOPCF4BSection = false;
	private boolean showOPCF5CSection = false;
	private boolean showOPCF25AWSection = false;
	private boolean showOPCF25ARSection = false;
	private boolean showCGODecOFSection = false;
	private boolean showCPDecSection = false;
	private boolean showPESection = false;
	private boolean showEON20ASection = false;
	private boolean showCIDCSection = false;
	private boolean showCIDCOFSection = false;
	private boolean showEON119Section = false;
	private boolean showEON141Section = false;
	private boolean showCRBSection = false;
	private boolean showApxASection = false;
	private boolean showTLLESection = false;
	private boolean showWLLESection = false;
	private boolean showLISCSection = false;
	private boolean showMTGTSection = false;
	private boolean showMTGTOFSection = false;
	private boolean showMOBISection = false;
	private boolean showTORVSection = false;
	private boolean showTOCASection = false;
	private boolean showTDGSection = false;
	private boolean showTDGPLSection = false;
	private boolean showTDGCSection = false;
	private boolean showCEMTCSection = false;
	private boolean showCEMTCOFSection = false;
	private boolean showFOESection = false;
	private boolean showDOCCSection = false;
	private boolean showAOCCOFSection = false;
	private boolean showALOCCGLSection = false;
	private boolean showDLOCCGLSection = false;
	private boolean showALOCCPSection = false;
	private boolean showDLOCCPSection = false;
	private boolean showMOCMCSection = false;
	private boolean showALOCCSection = false;
	private boolean showDLOCCSection = false;
	private boolean showEON115Section = false;
	private boolean showEON145Section = false;
	
	
	public PolicyPackageLHT(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage)insurancePolicy;
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		
		this.policyDocument = policyDocument;
		this.docDate = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "MMMM dd, yyyy");
		this.date = formatDate(LocalDate.now(), "MMMM dd, yyyy");
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressProvState = insurancePolicy.getInsuredAddress().getProvState();
		this.insuredAddressPC = insurancePolicy.getInsuredAddress().getPostalZip();
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.brokerContact = insurancePolicy.getProducerContactName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		
		for (Location loc : insurancePolicy.getPolicyProducer().getLocations()) {
			if (loc.getCompany().getCompanyPK().equals(insurancePolicy.getPolicyProducer().getCompanyPK())) {
				this.broker_addr = loc.getAddressLine1();
				this.broker_city = loc.getCity();
				this.broker_provState = loc.getProvState();
				this.broker_pc = loc.getPostalZip();
			}
		}
		
		this.effectiveDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"M/dd/yyyy");
		this.expiryDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"M/dd/yyyy");
		this.isLaterThanJan2021 = policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().compareTo(JAN2021) >= 0;
		this.issue_date = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "M/dd/yyyy");
		this.lbLimit = numberFormat(autoPackage.getLiabilityLimit());
		this.effYear = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"yyyy");
		this.effMonth = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"MM");
		this.effDay = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"dd");
		this.expYear = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"yyyy");
		this.expMonth = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"MM");
		this.expDay = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"dd");
		this.prepDate = formatDate(policyDocument.getCreatedDate().toLocalDate(), "MM/dd/yyyy");
		
		// Sub Policies
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		for (SubPolicy<Risk> subPolicy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
				subPolicyCGL = (CGLSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
				subPolicyCGO = (CargoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
				subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPolicy;
			}
		}
		
		LinkedHashSet<Endorsement> endDescs = new LinkedHashSet<Endorsement>();
		
		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			endDescs.add(end);
			
			if (end.getEndorsementCd().equalsIgnoreCase("EON110")) {
				this.showEON110Section = true;
				this.numEnds.add(end);
				this.eon110_no = numberFormat(end.getEndorsementNumber());
				this.per_occurrence_ded = end.getDeductible1amount() != null ? numberFormat(end.getDeductible1amount()) : "N/A";
				this.eon110_ded = numberFormat(end.getDeductible1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON111")) {
				this.showEON111Section = true;
				this.numEnds.add(end);
				this.eon111_effective_date = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
						"MMMM dd, yyyy");
				this.eon111_expiry_date = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
						"MMMM dd, yyyy");
			} else if (end.getEndorsementCd().equalsIgnoreCase("IA")) {
				this.showIASection = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON112")) {
				this.showEON112Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON115")) {
				this.showEON115Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON116")) {
				this.showEON116Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON117")) {
				this.showEON117Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				this.showEON121Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("DOCC")) {
				this.showDOCCSection = true;
				this.docc_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("DOCC_Type")
								&& extData.getColumnValue() != null) {
							this.docc_type = extData.getColumnValue();
						}
					}
				}
				this.numEnds.add(end);
			}
		}
		
		//Sub Policy Auto
		this.vehPrem = numberFormat(subPolicyAuto.getSubPolicyPremium().getWrittenPremium());
		LinkedHashSet<Endorsement> autoEndorsements = new LinkedHashSet<Endorsement>();
		for(Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			autoEndorsements.add(end);
		}
		
		fleetBasis = subPolicyAuto.getFleetBasis();
		if(fleetBasis.startsWith("21B")) {
			this.sec7Ded = "As per OPCF 21B";
			this.vehDesc = "All vehicles owned, leased to, registered, licensed and operated by and on behalf of the named insured";
			this.dcpd_ded = "As per OPCF 21B";
		} else if (fleetBasis.startsWith("21A")) {
			this.sec7Ded = "As per OPCF 21A";
			this.vehDesc = "All vehicles owned, leased to, registered, licensed and operated by and on behalf of the named insured";
			this.dcpd_ded = "As per OPCF 21A";
		} else if (fleetBasis.equalsIgnoreCase("SCHD")) {
			this.sec7Ded = "See Schedule of vehicles attached";
			this.vehDesc = "See Schedule of vehicles";
			this.dcpd_ded = "See Schedule of vehicles";
		}
		
		//Vehicle Risks
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		Double biSum = 0.0; 
		Double pdSum = 0.0;
		Double abSum = 0.0;
		Double uaSum = 0.0;
		Double dcpdSum = 0.0;
		Double spSum = 0.0;
		Double cmSum = 0.0;
		Double clSum = 0.0;
		Double apSum = 0.0;
		
		for(SpecialtyVehicleRisk vehicle : vehicles) {
			
			for (Coverage cove : vehicle.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("BI")) {
					biSum = cove.getCoveragePremium().getWrittenPremium() != null ? biSum + cove.getCoveragePremium().getWrittenPremium() : biSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("PD")) {
					pdSum = cove.getCoveragePremium().getWrittenPremium() != null ? pdSum + cove.getCoveragePremium().getWrittenPremium() : pdSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("AB")) {
					abSum = cove.getCoveragePremium().getWrittenPremium() != null ? abSum + cove.getCoveragePremium().getWrittenPremium() : abSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("UA")) {
					uaSum = cove.getCoveragePremium().getWrittenPremium() != null ? uaSum + cove.getCoveragePremium().getWrittenPremium() : uaSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					dcpdSum = cove.getCoveragePremium().getWrittenPremium() != null ? dcpdSum + cove.getCoveragePremium().getWrittenPremium() : dcpdSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP")) {
					spSum = cove.getCoveragePremium().getWrittenPremium() != null ? spSum + cove.getCoveragePremium().getWrittenPremium() : spSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM")) {
					cmSum = cove.getCoveragePremium().getWrittenPremium() != null ? cmSum + cove.getCoveragePremium().getWrittenPremium() : cmSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL")) {
					clSum = cove.getCoveragePremium().getWrittenPremium() != null ? clSum + cove.getCoveragePremium().getWrittenPremium() : clSum;
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP")) {
					apSum = cove.getCoveragePremium().getWrittenPremium() != null ? apSum + cove.getCoveragePremium().getWrittenPremium() : apSum;
				}
			}
			
			Set<Endorsement> vehEndorsements = vehicle.getRiskEndorsements();
			for(Endorsement end : vehEndorsements) {
				autoEndorsements.add(end);
			}
		}
		this.biTotalPrem = numberFormat(biSum);
		this.pdTotalPrem = numberFormat(pdSum);
		this.abTotalPrem = numberFormat(abSum);
		this.uaTotalPrem = numberFormat(uaSum);
		this.dcpdTotalPrem = numberFormat(dcpdSum);
		this.spTotalPrem = numberFormat(spSum);
		this.compTotalPrem = numberFormat(cmSum);
		this.collTotalPrem = numberFormat(clSum);
		this.apTotalPrem = numberFormat(apSum);
		
		//Endorsements
		LinkedHashSet<String> opcf27BVehTypes = new LinkedHashSet<String>();
		List<Endorsement> autoEndList = new ArrayList<Endorsement>();
		Iterator<Endorsement> itAutoEnd = autoEndorsements.iterator();
		StringBuilder opcf2NameSB = new StringBuilder();
		StringBuilder opcf2RelationSB = new StringBuilder();
		StringBuilder eon114NameInsSB = new StringBuilder();
		StringBuilder gdpcDealerPlateNoSB = new StringBuilder();
		StringBuilder gdpcExpDateSB = new StringBuilder();
		
		while(itAutoEnd.hasNext()) {
			Endorsement end = itAutoEnd.next();
			endDescs.add(end);
			
			if (end.getEndorsementCd().equalsIgnoreCase("OPCF2")) {
				this.showOPCF2Section = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF2_Pers") && extData.getColumnValue() != null) {
							if (opcf2NameSB.length() > 0) {
								opcf2NameSB.append("\n");
							}
							opcf2NameSB.append("   " + extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF2_Relation")
								&& extData.getColumnValue() != null) {
							if (opcf2RelationSB.length() > 0) {
								opcf2RelationSB.append("\n");
							}
							opcf2RelationSB.append("   " + extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF4A")) {
				this.showOPCF4ASection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF4A_ExpType")
								&& extData.getColumnValue() != null) {
							this.opcf4A_ext_type = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF4A_Sec7")
								&& extData.getColumnValue() != null) {
							this.opcf4A_cover = extData.getColumnValue();
						}
					}
				}
				this.opcf4A_limit = numberFormat(end.getLimit1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF4B")) {
				this.showOPCF4BSection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF4B_MatType")
								&& extData.getColumnValue() != null) {
							this.opcf4B_mat_type = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF4B_Sec7")
								&& extData.getColumnValue() != null) {
							this.opcf4B_cover = extData.getColumnValue();
						}
					}
				}
				this.opcf4B_limit = numberFormat(end.getLimit1amount());
				this.opcf4B_prem = premFormat(end.getEndorsementPremium().getWrittenPremium());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
				this.showOPCF5Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5Bl")) {
				this.showOPCF5BlSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5C")) {
				this.showOPCF5CSection = true;
				this.opcf5C_limit = end.getLimit1amount() == 0 ? "Not Applicable" : "$" + numberFormat(end.getLimit1amount());
				this.opcf5C_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF8")) {
				this.showOPCF8Section = true;
				if (this.opcf8_ded.equals("N/A")) {
					this.opcf8_ded = numberFormat(end.getDeductible1amount());
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF9")) {
				this.showOPCF9Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF13C")) {
				this.showOPCF13CSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
				this.showOPCF19Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF20")) {
				this.opcf20_exists = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21A")) {
				this.showOPCF21ASection = true;
				this.opcf21A_prem = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
				this.opcf21A_rate = "";
				this.opcf21A_rcpt = "";
				this.min_prem_pct = "";
				this.min_prem = "";
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21B")) {
				this.showOPCF21BSection = true;
				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BQ")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BS")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BA")) {
					this.opcf21B_prorata = true;
				}

				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21B5")) {
					this.opcf21B_5050 = true;
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
				this.showOPCF23ASection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_DCPD") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_dcpd_ck = true;
							this.opcf23A_dcpd_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_SP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_sp_ck = true;
							this.opcf23A_sp_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CL") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_cl_ck = true;
							this.opcf23A_cl_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CM") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_cm_ck = true;
							this.opcf23A_cm_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23A_AP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23A_ap_ck = true;
							this.opcf23A_ap_ded = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23ABl")) {
				this.showOPCF23ABlSection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_DCPD") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_dcpd_ck = true;
							this.opcf23ABL_dcpd_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_SP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_sp_ck = true;
							this.opcf23ABL_sp_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CL") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_cl_ck = true;
							this.opcf23ABL_cl_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CM") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_cm_ck = true;
							this.opcf23ABL_cm_ded = extData.getColumnValue();
						}

						if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_AP") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equals("")) {
							this.opcf23ABL_ap_ck = true;
							this.opcf23ABL_ap_ded = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25A")) {
				this.showOPCF25ASection = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF25A_TermDay")
								&& extData.getColumnValue() != null) {
							this.termination_day = extData.getColumnValue();
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25AW")) {
				this.showOPCF25AWSection = true;
				this.showEON145Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25AR")) {
				this.showOPCF25ARSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27")) {
				this.showOPCF27Section = true;
				this.opcf27_apded = numberFormat(end.getDeductible1amount());
				this.opcf27_limit = numberFormat(end.getLimit1amount());
				List<String> pers = new ArrayList<String>();
				List<String> relation = new ArrayList<String>();
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27_Pers") && extData.getColumnValue() != null) {
							pers.add(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF27_Relation")
								&& extData.getColumnValue() != null) {
							relation.add(extData.getColumnValue());
						}
					}
				}

				for (int i = 0; i < pers.size(); i++) {
					HashMap<String, String> person = new HashMap<String, String>();
					person.put("pers", pers.get(i));
					person.put("relation", relation.get(i));
					opcf27PersonsTable.add(person);
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) {
				this.showOPCF27BSection = true;

				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")
								&& extData.getColumnValue() != null) {
							if (!opcf27BVehTypes.contains(extData.getColumnValue())) {
								opcf27BVehTypes.add(extData.getColumnValue());
								this.opcf27BEnds.add(end);
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28")) {
				this.showOPCF28Section = true;
				this.opcf28_limit = numberFormat(end.getLimit1amount());
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF28_Pers")
								&& extData.getColumnValue() != null) {
							this.opcf28_pers = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_Coll")
								&& extData.getColumnValue() != null) {
							if(extData.getColumnValue().equalsIgnoreCase("Insured")) {
								this.opcf28_coll = true;
								this.opcf28CL_ded = numberFormat(end.getDeductible1amount());
							} else {
								this.opcf28_collNot = true;
							}
						} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_AllPeril")
								&& extData.getColumnValue() != null) {
							if(extData.getColumnValue().equalsIgnoreCase("Insured")) {
								this.opcf28_allperil = true;
								this.opcf28AP_ded = numberFormat(end.getDeductible1amount());
							} else {
								this.opcf28_allperilNot = true;
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28A")) {
				this.showOPCF28ASection = true;
				this.opcf28AEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF30")) {
				this.showOPCF30Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
				this.showOPCF31Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF38")) {
				this.showOPCF38Section = true;
				this.opcf38Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF40")) {
				this.showOPCF40Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF43")) {
				this.showOPCF43Section = true;
				this.showOPCF43ASection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) {
				this.showOPCF44RSection = true;
				Integer limit = end.getLimit1amount() != null ? end.getLimit1amount() : 0;
				if (autoPackage.getLiabilityLimit() > 2000000) {
					this.opcf44RPrintLimit = "(Maximum limit: $2,000,000)";
				} else {
					this.opcf44RPrintLimit = "(limit: $" + numberFormat(limit) + ")";
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF47")) {
				this.showOPCF47Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF48")) {
				this.showOPCF48Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON114")) {
				this.showEON114Section = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON114_NamedIns")
								&& extData.getColumnValue() != null) {
							if (eon114NameInsSB.length() > 0) {
								eon114NameInsSB.append(" and/or\n");
							}
							eon114NameInsSB.append(extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON101")) {
				this.showEON101Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON102")) {
				this.showEON102Section = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON102_Sec7_Ded")
								&& extData.getColumnValue() != null) {
							if (extData.getColumnValue().startsWith("See")) {
								this.eon102_sec7_ded = extData.getColumnValue();
							} else {
								this.eon102_sec7_ded = "$" + extData.getColumnValue();
							}
						}
						
						if (extData.getColumnId().equalsIgnoreCase("EON102_DCPD_Ded")
								&& extData.getColumnValue() != null) {
							if (extData.getColumnValue().startsWith("See")) {
								this.eon102_ded = extData.getColumnValue();
							} else {
								this.eon102_ded = "$" + extData.getColumnValue();
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON20A")) {
				this.showEON20ASection = true;
				this.eon20A_limit = numberFormat(end.getLimit1amount());
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON109")) {
				this.showEON109Section = true;
				this.numEnds.add(end);
				this.eon109Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("GDPC")) {
				this.showGDPCSection = true;
				this.numEnds.add(end);
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
								&& extData.getColumnValue() != null) {
							if(gdpcDealerPlateNoSB.length() > 0) {
								gdpcDealerPlateNoSB.append("\n");
							}
							gdpcDealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
								&& extData.getColumnValue() != null) {
							if(gdpcExpDateSB.length() > 0) {
								gdpcExpDateSB.append("\n");
							}
							gdpcExpDateSB.append("Expiry Date: " + extData.getColumnValue());
						}
					}
				}

			} else if(end.getEndorsementCd().equalsIgnoreCase("EON119")) {
				this.showEON119Section = true;
				this.eon119Ends.add(end);
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON141")) {
				this.showEON141Section = true;
				this.numEnds.add(end);
			} 
			
			if (end.getEndorsementCd().startsWith("OPCF")) {
				autoEndList.add(end);
			}
			
		}
		
		this.opcf2_name = opcf2NameSB.toString();
		this.opcf2_relationship = opcf2RelationSB.toString();
		this.eon114_name_ins = eon114NameInsSB.toString();
		this.gdpc_dealer_plate_no = gdpcDealerPlateNoSB.toString();
		this.gdpc_exp_date = gdpcExpDateSB.toString();
		this.endList1 = orderedEndList(autoEndList, lookupConfig);
		
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			LookupTableItem lookupBusinessDescItem = lookupConfig
					.getConfigLookup(ConfigConstants.LOOKUPTABLE_BusinessDescriptions).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getBusinessDescription()))
					.findFirst().orElse(null);
			this.businessType = lookupBusinessDescItem.getItemValue();
		}
		
		// Sub Policy CGL
		if (subPolicyCGL != null) {
			
			this.showCGLDecSection = true;
			this.cgl_lb_limit = numberFormat(subPolicyCGL.getCglLimit());
			this.cgl_ded = numberFormat(subPolicyCGL.getCglDeductible());
			
			// Coverages
			for (Coverage cove : subPolicyCGL.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("SPF6TPL")) {
					this.showSPF6Section = true;
					this.spf6_lb_limit = numberFormat(cove.getLimit1());
					this.sef99_lb_limit = "Incl.";
				} else if (cove.getCoverageCode().equalsIgnoreCase("TLL")) {
					this.tll_limit = numberFormat(cove.getLimit1());
					if (cove.getDeductible1() != null) {
						this.tll_ded = numberFormat(cove.getDeductible1());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("EBE")) {
					this.ebeExists = true;
					this.emp_benefits = "Employee Benefits Extension";
					this.ebe_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.ebe_limit = numberFormat(cove.getLimit1());
					if (cove.getDeductible1() != null) {
						this.ebe_ded = numberFormat(cove.getDeductible1());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("MEX")) {
					this.mex_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("BIPD")) {
					this.cgl_ge_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("LPOLL")) {
					this.lpoll_limit = numberFormat(cove.getLimit1());
					this.lpoll_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("WLL")) {
					this.wll_limit = numberFormat(cove.getLimit1());
					this.wll_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			//Endorsements
			for (Endorsement end : subPolicyCGL.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);

				if (end.getEndorsementCd().equalsIgnoreCase("SEF96")) {
					this.showSEF96Section = true;
					if (this.showSPF6Section) {
						this.sef96_lb_limit = "Incl.";
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("SEF99")) {
					this.showSEF99Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("GECGLMTC")) {
					this.showGECGLMTCSection = true;
					this.numEnds.add(end);
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GELC_ModType")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_mod_type = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GELC_VehDesc")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_veh_desc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GELC_SerNum")
									&& extData.getColumnValue() != null) {
								this.gecglmtc_veh_serial_no = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GECGL")) {
					this.showGECGLSection = true;
					this.numEnds.add(end);
					this.gecgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GEL_ModType")
									&& extData.getColumnValue() != null) {
								this.gecgl_mod_type = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GEL_VehDesc")
									&& extData.getColumnValue() != null) {
								this.gecgl_veh_desc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("GEL_SerNum")
									&& extData.getColumnValue() != null) {
								this.gecgl_veh_serial_no = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GLEE")) {
					this.showGLEESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("WOS")) {
					this.showWOSSection = true;
					this.numEnds.add(end);
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("WOS_CoName")
									&& extData.getColumnValue() != null) {
								this.wos_co_name = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("WOS_CoAddr")
									&& extData.getColumnValue() != null) {
								this.wos_co_addr = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AOCCGL")) {
					this.showAOCCGLSection = true;
					this.numEnds.add(end);
					this.aoccgl_prem = numberFormat(subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCCGL")) {
					this.showMOCCGLSection = true;
					this.numEnds.add(end);
					this.moccgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("AIC")) {
					this.showAICSection = true;
					this.numEnds.add(end);
					this.aic_limit = numberFormat(end.getLimit1amount());
					this.aic_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEL")) {
					this.showCDELSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("POLL")) {
					this.poll_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("CRB")) {
					this.showCRBSection = true;
					this.showApxASection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("TLLE")) {
					this.showTLLESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("WLLE")) {
					this.showWLLESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("ALOCCGL")) {
					this.showALOCCGLSection = true;
					this.aloccgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("DLOCCGL")) {
					this.showDLOCCGLSection = true;
					this.dloccgl_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					this.numEnds.add(end);
				}
				
			}
			
			StringBuilder locationSB = new StringBuilder();
			Set<PolicyLocation> policyLocations = subPolicyCGL.getCglLocations().stream().map(CGLLocation::getPolicyLocation).collect(Collectors.toSet());
			for (PolicyLocation pl : policyLocations) {
				if (locationSB.length() > 0) {
					locationSB.append("\n");
				}

				locationSB.append(pl.getLocationAddress().getAddressLine1());
				locationSB.append(", ");
				locationSB.append(pl.getLocationAddress().getCity());
				locationSB.append(", Ontario ");
				locationSB.append(pl.getLocationAddress().getPostalZip());
			}
			this.cgl_locations = locationSB.toString();
			if(subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
				this.totalCGLPrem = numberFormat(subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
			} else {
				this.totalCGLPrem = "Incl.";
			}
			
			if(this.showSEF96Section && this.showSEF99Section) {
				this.sef_ends = "96 and 99";
			} else if (this.showSEF96Section) {
				this.sef_ends = "96";
			} else if (this.showSEF99Section) {
				this.sef_ends = "99";
			}
			
		}
		
		// Sub Policy CGO
		if (subPolicyCGO != null) {
			
			this.showCGODecSection = true;
			this.cgo_limit = numberFormat(subPolicyCGO.getCargoLimit());
			this.cgo_ded = numberFormat(subPolicyCGO.getCargoDeductible());
			this.cargo = "Motor Truck Cargo";
			this.cargo_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());	
			
			StringBuilder locationSB = new StringBuilder();
			for (Coverage cove : subPolicyCGO.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("MTCC")) {
					this.cargoLimit = numberFormat(cove.getLimit1());
					this.cargo_ded = numberFormat(cove.getDeductible1());
					this.cargo_ge_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.cargoLimit1 = "Incl.";
					this.cargoLimit2 = "Incl.";
					this.cargoLimit3 = "10,000";
					this.cargoLimit4 = numberFormat(cove.getLimit1());
					this.cargoLimit5 = numberFormat(cove.getLimit1());

					this.cargoLimit1Dec = "Incl.";
					this.cargoLimit3Dec = "Incl.";
					this.cargoLimit4Dec = "10,000";
					this.cargoLimit5Dec = numberFormat(cove.getLimit1());
					this.cargoLimit6Dec = numberFormat(cove.getLimit1());
					this.showCDECDecRow = true;
					this.mtcc_exists = true;

					for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
						if (risk.getRiskType().equalsIgnoreCase("LO")) {
							for (Coverage coverage : risk.getCoverageDetail().getCoverages()) {
								if (coverage.getCoverageCode().equalsIgnoreCase("MTCC")) {
									PolicyLocation pl = (PolicyLocation) risk;

									if (locationSB.length() > 0) {
										locationSB.append("\n");
									}

									locationSB.append(pl.getLocationAddress().getAddressLine1());
									locationSB.append(", ");
									locationSB.append(pl.getLocationAddress().getCity());
									locationSB.append(" ON ");
									locationSB.append(pl.getLocationAddress().getPostalZip());
								}
							}
						}
					}

				} else if (cove.getCoverageCode().equalsIgnoreCase("MTCO")) {
					this.showCGODecOFSection = true;
					this.mtco_exists = true;
					this.mtco_limit = numberFormat(cove.getLimit1());
					this.mtco_ded = numberFormat(cove.getDeductible1());
					this.mtco_prem = fleetBasis.equalsIgnoreCase("SCHD") ? numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium()) : "Incl.";
				} 
			}
			
			for (Endorsement end : subPolicyCGO.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);
				
				if (end.getEndorsementCd().equalsIgnoreCase("AOCC")) {
					this.showAOCCSection = true;
					this.showAOCCOFSection = true;
					this.numEnds.add(end);
					this.aocc_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCC")) {
					this.showMOCCSection = true;
					this.numEnds.add(end);
					this.mocc_prem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEC")) {
					this.showCDECSection = true;
					this.numEnds.add(end);
				}  else if (end.getEndorsementCd().equalsIgnoreCase("TTC")) {
					this.showTTCSection = true;
					this.numEnds.add(end);
					this.ttc_limit = numberFormat(end.getLimit1amount());
					this.ttc_ded = numberFormat(end.getDeductible1amount());
					this.ttc_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("TTC_VehDesc")
									&& extData.getColumnValue() != null) {
								this.ttc_vehdesc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period1")
									&& extData.getColumnValue() != null) {
								this.ttc_period1 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period2")
									&& extData.getColumnValue() != null) {
								this.ttc_period2 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TTC_SerNo")
									&& extData.getColumnValue() != null) {
								this.ttc_serno = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AICC")) {
					this.showAICCSection = true;
					this.numEnds.add(end);
					this.aicc_limit = numberFormat(end.getLimit1amount());
					this.aicc_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CTC")) {
					this.cargoLimit2Dec = numberFormat(end.getLimit1amount());
					this.ctc_limit = numberFormat(end.getLimit1amount());
					this.ctcExists = true;
					this.ctc_prem = premFormat(end.getEndorsementPremium().getWrittenPremium());
					this.cont_cargo = "Contingent Truck Cargo";
				} else if (end.getEndorsementCd().equalsIgnoreCase("CIDC")) {
					this.showCIDCSection = mtcc_exists;
					this.showCIDCOFSection = mtco_exists;
				} else if (end.getEndorsementCd().equalsIgnoreCase("LISC")) {
					this.showLISCSection = true;
					this.numEnds.add(end);
					this.liscEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("MTGT")) {
					this.showMTGTSection = true;
					this.showMTGTOFSection = true;
					this.mtgt_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOBI")) {
					this.showMOBISection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("TORV")) {
					this.showTORVSection = true;
					this.numEnds.add(end);
					this.torv_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("TORV_LimitA")
									&& extData.getColumnValue() != null) {
								this.torv_limita = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TORV_LimitB")
									&& extData.getColumnValue() != null) {
								this.torv_limitb = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TORV_LimitC")
									&& extData.getColumnValue() != null) {
								this.torv_limitc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TORV_Ded1")
									&& extData.getColumnValue() != null) {
								this.torv_ded1 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TORV_Ded2")
									&& extData.getColumnValue() != null) {
								this.torv_ded2 = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("TOCA")) {
					this.showTOCASection = true;
					this.numEnds.add(end);
					this.toca_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("TOCA_LimitA")
									&& extData.getColumnValue() != null) {
								this.toca_limita = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TOCA_LimitB")
									&& extData.getColumnValue() != null) {
								this.toca_limitb = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TOCA_LimitC")
									&& extData.getColumnValue() != null) {
								this.toca_limitc = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TOCA_Ded1")
									&& extData.getColumnValue() != null) {
								this.toca_ded1 = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("TOCA_Ded2")
									&& extData.getColumnValue() != null) {
								this.toca_ded2 = extData.getColumnValue();
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("TDG")) {
					this.showTDGSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("TDGPL")) {
					this.showTDGPLSection = true;
					this.numEnds.add(end);
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("TDGPL_Class")
									&& extData.getColumnValue() != null) {
								this.tdgpl_class = extData.getColumnValue();
							} 
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("TDGC")) {
					this.showTDGCSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("CEMTC")) {
					this.showCEMTCSection = true;
					this.numEnds.add(end);
					StringBuilder exclisionSB = new StringBuilder();
					int count = 0;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("CEMTC_Excl")
									&& extData.getColumnValue() != null) {
								if (exclisionSB.length() > 0) {
									exclisionSB.append("\n");
								}
								count++;
								exclisionSB.append("(" + count + ") " + extData.getColumnValue());
							}
						}
					}
					this.cemtc_exclusion = exclisionSB.toString();
				} else if (end.getEndorsementCd().equalsIgnoreCase("FOE")) {
					this.showFOESection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCMC")) {
					this.showMOCMCSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("ALOCC")) {
					this.showALOCCSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("DLOCC")) {
					this.showDLOCCSection = true;
					this.numEnds.add(end);
				}
			}
			
			this.cargo_locations = locationSB.toString();
			if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
				this.totalCargoPrem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
			} else {
				this.totalCargoPrem = "Incl.";
			}
			
		} else {
			this.cgo_limit = "N/A";
			this.cgo_ded = "N/A";
		}
		
		// Sub Policy GAR
		if (subPolicyGAR != null) {
			
			this.showGARDecSection = true;
			if(subPolicyGAR.getSubPolicyNamedInsured() != null && !subPolicyGAR.getSubPolicyNamedInsured().isEmpty()) {
				subPolicyNamedInsured = subPolicyGAR.getSubPolicyNamedInsured();
			} else {
				subPolicyNamedInsured = insuredName;
			}
			
			// Coverages
			Double bipd = 0.0;
			for (Coverage cove : subPolicyGAR.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("TPL")) {
					this.oap4_lb_limit = numberFormat(autoPackage.getLiabilityLimit());
				} else if (cove.getCoverageCode().equalsIgnoreCase("BI")) {
					this.oap4_bi_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					bipd = bipd + cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("PD")) {
					this.oap4_pd_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					bipd = bipd + cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("AB")) {
					this.oap4_ab_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					this.oap4_dcpd_ded = numberFormat(cove.getDeductible1());
					this.oap4_dcpd_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL61")) {
					this.oap4_cl_limit = numberFormat(cove.getLimit1());
					this.oap4_cl_ded = numberFormat(cove.getDeductible1());
					this.oap4_cl_prem = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					this.cl6_lim = numberFormat(cove.getLimit1());
					this.cl6_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			this.oap4_bipd_prem = numberFormat(bipd);

			//Endorsements
			StringBuilder endCodeSB = new StringBuilder();
			StringBuilder dealerPlateNoSB = new StringBuilder();
			StringBuilder dealerExpDateSB = new StringBuilder();
			StringBuilder servPlateNoSB = new StringBuilder();
			StringBuilder expDateSB = new StringBuilder();
			StringBuilder oef72EndCodeSB = new StringBuilder();
			
			for (Endorsement end : subPolicyGAR.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);
				
				if (end.getEndorsementCd().equalsIgnoreCase("OEF71")) {
					this.showOEF71Section = true;
					this.oef71_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
					this.showOEF72Section = true;
					this.oef72_prem = "Incl.";
					List<String> altNums = new ArrayList<String>();
					List<String> changeDescs = new ArrayList<String>();
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OEF72_AltNum")
									&& extData.getColumnValue() != null) {
								altNums.add(extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("OEF72_ChangeDesc")
									&& extData.getColumnValue() != null) {
								changeDescs.add(extData.getColumnValue());
							}
						}
					}
					
					for(int i = 0; i < altNums.size(); i++) {
						HashMap<String, String> alteration = new HashMap<String, String>();
						alteration.put("altNum", altNums.get(i));
						alteration.put("changeDesc", changeDescs.get(i));
						this.oef72AlterationsTable.add(alteration);
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF75")) {
					this.showOEF75Section = true;
					this.oef75_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF77")) {
					this.showOEF77Section = true;
					this.oef77_limit = "Incl.";
					this.oef77_ded = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("GSPC")) {
					this.showGSPCSection = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GSPC_PlateNo")
									&& extData.getColumnValue() != null) {
								if (servPlateNoSB.length() > 0) {
									servPlateNoSB.append("\n");
								}
								servPlateNoSB.append("Service Plate No.: " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("GSPC_ExpDate")
									&& extData.getColumnValue() != null) {
								if (expDateSB.length() > 0) {
									expDateSB.append("\n");
								}
								expDateSB.append("Expiry Date:  " + extData.getColumnValue());
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GDPCG")) {
					this.showGDPCGSection = true;
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
									&& extData.getColumnValue() != null) {
								if (dealerPlateNoSB.length() > 0) {
									dealerPlateNoSB.append("\n");
								}
								dealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
									&& extData.getColumnValue() != null) {
								if (dealerExpDateSB.length() > 0) {
									dealerExpDateSB.append("\n");
								}
								dealerExpDateSB.append("Expiry Date:  " + extData.getColumnValue());
							}
						}
					}
				}
				
				
				if (end.getEndorsementCd().startsWith("OEF") && !end.getEndorsementCd().equalsIgnoreCase("OEF81")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					if (endCodeSB.length() > 0) {
						endCodeSB.append(", ");
					}
					if (lookupEndorsementCodeItem != null) {
						endCodeSB.append(lookupEndorsementCodeItem.getItemValue());
					} else {
						endCodeSB.append(end.getEndorsementCd());
					}

					if (!end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
						if (oef72EndCodeSB.length() > 0) {
							oef72EndCodeSB.append(", ");
						}
						if (lookupEndorsementCodeItem != null) {
							oef72EndCodeSB.append(lookupEndorsementCodeItem.getItemValue());
						} else {
							oef72EndCodeSB.append(end.getEndorsementCd());
						}
					}
				}
			}
			this.gdpc_servPlateNo = dealerPlateNoSB.toString();
			this.gdpc_expDate = dealerExpDateSB.toString();
			this.gspc_servPlateNo = servPlateNoSB.toString();
			this.gspc_expDate = expDateSB.toString();
			Double sp64_prem_total = 0.0;
			
			
			for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
				if (risk.getRiskType().equalsIgnoreCase("LO")) {
					PolicyLocation pl = (PolicyLocation) risk;
					Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
					
					
					if (coveSP64 != null) {
						this.oap4_sp_limit = "As per Garage Cert. of Ins.";
						this.oap4_sp_ded = "As per Garage Cert. of Ins.";
					}
					
					if (pl.getLocationId().equalsIgnoreCase("A")) {
						if (coveSP64 != null) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationa = locationSB.toString();
							this.insBldga = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLota = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_veha = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limita = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_deda = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_prema = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_prema = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("B")) {
						if (pl.getCustomerMainAddressPK() == null && coveSP64 != null) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationb = locationSB.toString();
							this.insBldgb = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotb = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_vehb = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitb = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedb = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premb = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premb = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("C")) {
						if (pl.getCustomerMainAddressPK() == null && coveSP64 != null) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationc = locationSB.toString();
							this.insBldgc = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotc = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;

							if (coveSP64 != null) {
								this.maxnum_vehc = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitc = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedc = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premc = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premc = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("D")) {
						if (pl.getCustomerMainAddressPK() == null && coveSP64 != null) {
							StringBuilder locationSB = new StringBuilder();
							locationSB.append(pl.getLocationAddress().getAddressLine1());
							locationSB.append(", ");
							locationSB.append(pl.getLocationAddress().getCity());
							locationSB.append(" ON ");
							locationSB.append(pl.getLocationAddress().getPostalZip());
							this.gar_locationd = locationSB.toString();
							this.insBldgd = pl.getIsVehiclesInBuilding() != null ? pl.getIsVehiclesInBuilding() : false;
							this.insLotd = pl.getIsVehiclesOnLot() != null ? pl.getIsVehiclesOnLot() : false;
							if (coveSP64 != null) {
								this.maxnum_vehd = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitd = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedd = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premd = "$"
										+ numberFormat(coveSP64.getCoveragePremium().getWrittenPremium());
								this.oef77_oap4_sp_premd = "$Incl.";
							}
						}
					} else {
						if (coveSP64 != null) {
							sp64_prem_total = sp64_prem_total + coveSP64.getCoveragePremium().getWrittenPremium();
						}
					}
				}
			}
			
			this.garBusinessType = subPolicyGAR.getBusinessDescription();
			this.ft_emp = numberFormat(subPolicyGAR.getNumFullTimeEmployees());
			this.pt_emp = numberFormat(subPolicyGAR.getNumPartTimeEmployees());
			this.oap4_prem = numberFormat(subPolicyGAR.getSubPolicyPremium().getWrittenPremium());
			this.gar_end_code = endCodeSB.toString();
			this.oap4_ab_limit = "Standard Limits";
			this.oap4_ua_limit = "Incl.";
			this.sp64_prem = numberFormat(sp64_prem_total);
			this.oef72_end_codes = oef72EndCodeSB.toString();
		}
		
		//Sub Policy CP
		if (subPolicyCP != null) {
			this.showCPDecSection = true;
			this.showPESection = true;
			this.commProp_prem = premFormat(subPolicyCP.getSubPolicyPremium().getWrittenPremium());

			// Coverages
			for (Coverage cove : subPolicyCP.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("CONT")) {
					this.cont_limit = "10,000";
					this.cont_ded = "1,000";
				}
			}
			
			//Endorsements
			for (Endorsement end : subPolicyCP.getCoverageDetail().getEndorsements()) {
				endDescs.add(end);
				
				if (end.getEndorsementCd().equalsIgnoreCase("ALOCCP")) {
					this.showALOCCPSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("DLOCCP")) {
					this.showDLOCCPSection = true;
					this.numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("PETP")) {
					this.petp_limit = "Incl.";
				}
			}
			
			StringBuilder locationSB = new StringBuilder();
			Set<PolicyLocation> policyLocations = subPolicyCP.getCommercialPropertyLocations().stream().map(CommercialPropertyLocation::getPolicyLocation).collect(Collectors.toSet());
			for (PolicyLocation pl : policyLocations) {
				if (locationSB.length() > 0) {
					locationSB.append("\n");
				}

				locationSB.append(pl.getLocationAddress().getAddressLine1());
				locationSB.append(", ");
				locationSB.append(pl.getLocationAddress().getCity());
				locationSB.append(", Ontario ");
				locationSB.append(pl.getLocationAddress().getPostalZip());
			}
			this.cp_locations = locationSB.toString();
		}

		StringBuilder endDescSB = new StringBuilder();
		HashMap<Integer, String> endDescMap = new HashMap<Integer, String>();
		Iterator<Endorsement> itEndDescs = endDescs.iterator();
		while (itEndDescs.hasNext()) {
			Endorsement end = itEndDescs.next();

			switch (end.getEndorsementCd()) {
			case "OPCF4A":
				endDescMap.put(0, end.getDescription());
				break;
			case "OPCF4B":
				endDescMap.put(1, end.getDescription());
				break;
			case "OPCF8":
				endDescMap.put(2, end.getDescription());
				break;
			case "OPCF9":
				endDescMap.put(3, end.getDescription());
				break;
			case "OPCF13C":
				endDescMap.put(4, end.getDescription());
				break;
			case "OPCF19":
				endDescMap.put(5, end.getDescription());
				break;
			case "OPCF21B":
				endDescMap.put(6, end.getDescription());
				break;
			case "OPCF25AW":
				endDescMap.put(7, end.getDescription());
				break;
			case "OPCF28":
				endDescMap.put(8, end.getDescription());
				break;
			case "OPCF28A":
				endDescMap.put(9, end.getDescription());
				break;
			case "OPCF30":
				endDescMap.put(10, end.getDescription());
				break;
			case "OPCF31":
				endDescMap.put(11, end.getDescription());
				break;
			case "OPCF38":
				endDescMap.put(12, end.getDescription());
				break;
			case "OPCF40":
				endDescMap.put(13, end.getDescription());
				break;
			case "OEF71":
				endDescMap.put(14, end.getDescription());
				break;
			case "OEF72":
				endDescMap.put(15, end.getDescription());
				break;
			case "OEF83":
				endDescMap.put(16, end.getDescription());
				break;
			case "OEF86":
				endDescMap.put(17, end.getDescription());
				break;
			case "SEF99":
				endDescMap.put(18, end.getDescription());
				break;
			case "EON110":
				endDescMap.put(19, end.getDescription());
				break;
			case "EON111":
				endDescMap.put(20, end.getDescription());
				break;
			case "IA":
				endDescMap.put(21, end.getDescription());
				break;
			case "CIDC":
				endDescMap.put(22, end.getDescription());
				break;
			case "TDG":
				endDescMap.put(23, end.getDescription());
				break;
			case "TDGPL":
				endDescMap.put(24, end.getDescription());
				break;
			case "FOE":
				endDescMap.put(25, end.getDescription());
				break;
			case "CEMTC":
				endDescMap.put(26, end.getDescription());
				break;
			case "TORV":
				endDescMap.put(27, end.getDescription());
				break;
			case "TOCA":
				endDescMap.put(28, end.getDescription());
				break;
			case "MTCO":
				endDescMap.put(29, end.getDescription());
				break;
			case "GLEE":
				endDescMap.put(30, end.getDescription());
				break;
			case "GDPC":
				endDescMap.put(31, end.getDescription());
				break;
			case "GDPCG":
				endDescMap.put(32, end.getDescription());
				break;
			case "GSPC":
				endDescMap.put(32, end.getDescription());
				break;
			}
		}
		
		for (int i = 0; i <= 32; i++) {
			String desc = endDescMap.get(i);
			if (desc != null) {
				if (endDescSB.length() > 0) {
					endDescSB.append("\n");
				}
				endDescSB.append(desc);
			}
		}
		
		this.end_desc = endDescSB.toString();
		this.total_premium = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		
		if(this.showOPCF21BSection) {
			this.opcf21b_prem = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		}
	}
	
	@Override
	public String getTemplatePath() {
		return "templates/PolicyPackageLHT_tmp.docx";
	}
	
	@Override
	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if(attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);

		setGeneralDeclarationsTable(doc);
		
		if (this.showOPCF5Section) {
			setOPCF5LessorTable(doc);
		}
		
		if (this.showOPCF19Section) {
			setOPCF19Section(doc);
		}
		
		if (this.opcf20_exists) {
			setOPCF20Section();
		}

		if (this.showOPCF21BSection) {
			setOPCF21BFirstTable(doc);
			setOPCF21BSecondTable(doc);
		}

		if (this.showOPCF23ASection) {
			setOPCF23ASecondTable(doc);
		}
		
		if (this.showOPCF25AWSection) {
			setOPCF25AWSection(doc);
		}
		
		if (this.showOPCF27Section) {
			setOPCF27Table(doc);
		}
		
		if (this.showOPCF27BSection) {
			setOPCF27BSection(doc);
		}
		
		if (this.showOPCF28ASection) {
			setOPCF28ASection(doc);
		}
		
		if (this.showOPCF31Section) {
			setOPCF31Section(doc);
		}
		
		if (this.showOPCF38Section) {
			setOPCF38Section(doc);
		}
		
		setMotorTruckCargoDecTable(doc);
		
		setCommercialGeneralLiabilityDecTable(doc);
		
		if (this.showOEF72Section) {
			setOEF72Table(doc);
		}
		
		if (this.showAICSection) {
			setAICSection(doc);
		}
		
		if (this.showAICCSection) {
			setAICCSection(doc);
		}
		
		if (this.showEON102Section) {
			setEON102Section(doc);
		}
		
		if (this.showEON109Section) {
			setEON109Section(doc);
		}
		
		if (this.showEON119Section) {
			setEON119Section(doc);
		}

		hideSections(doc);
		endorsementNumeration(doc);
		
		if (this.showCIDCSection) {
			setCIDCEnd110No(doc);
		}
		
		if (this.showCIDCSection) {
			setCIDCEndNo(doc);
		}

		if (this.showLISCSection) {
			setLISCSection(doc);
		}
		
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}
	
	@Override
	public void initializeFields() {
		fieldNames = new String[] { "Doc_Date", "Insured_Name", "Insured_Address_Street", "Insured_Address_City",
				"Insured_Address_ProvState", "Insured_Address_PC", "Pol_No", "End_Desc", "Broker_Contact", "BrokerName",
				"Effective_Date", "Expiry_Date", "LB_Limit", "OPCF8_Ded", "DCPD_Ded", "Cargo_Ded", "Cargo_Limit",
				"Cargo_Limit1", "Cargo_Limit2", "Cargo_Limit3", "Cargo_Limit4", "Cargo_Limit5", "CGL_LB_Limit",
				"CGL_Ded", "MEX_Limit", "TLL_Limit", "TLL_Ded", "EBE_Limit", "EBE_Ded", "SPF6_LB_Limit",
				"SEF96_LB_Limit", "SEF99_LB_Limit", "OAP4_Limit", "OAP4_DCPD_Ded", "OAP4_CL_Limit", "OAP4_CL_Ded",
				"OAP4_SP_Limit", "OAP4_SP_Ded", "OEF71_Limit", "OEF71_Ded", "OEF77_Limit", "OEF77_Ded",
				"OEF77_OAP4_SP_PremA", "OEF77_OAP4_SP_PremB", "OEF77_OAP4_SP_PremC", "OEF77_OAP4_SP_PremD",
				"OEF86_Limit", "Total_Premium", "OPCF21B_Prem", "EON110_No", "MaxEndNo", "MasterBrokerNumber",
				"BrokerAddr", "BrokerCity", "BrokerProvState", "BrokerPC", "Issue_Date", "EffYear", "EffMonth",
				"EffDay", "ExpYear", "ExpMonth", "ExpDay", "Prep_Date", "BITotalPrem", "PDTotalPrem", "ABTotalPrem",
				"UATotalPrem", "DCPDTotalPrem", "SPTotalPrem", "COMPTotalPrem", "COLLTotalPrem", "APTotalPrem",
				"EndList1", "VehPrem", "OPCF2_Name", "OPCF2_Relationship", "OPCF20_Limit", "Termination_Day",
				"OPCF27_APDed", "OPCF27_Limit", "OPCF27B_APDed", "BusinessType", "OPCF27B_Limit", "OPCF28_Pers",
				"OPCF28_Limit", "OPCF28CL_Ded", "OPCF28AP_Ded", "OPCF28_Coll", "OPCF28_CollNot", "OPCF28_AllPeril",
				"OPCF28_AllPerilNot", "Cargo_Limit1_Dec", "Cargo_Limit2_Dec", "Cargo_Limit3_Dec", "Cargo_Limit4_Dec",
				"Cargo_Limit5_Dec", "Cargo_Limit6_Dec", "TotalCargoPrem", "Cargo_Locations", "TotalCGLPrem",
				"CGL_Locations", "GAR_LocationA", "InsBldgA", "InsLotA", "GAR_LocationB", "InsBldgB", "InsLotB",
				"GAR_LocationC", "InsBldgC", "InsLotC", "GAR_LocationD", "InsBldgD", "InsLotD", "GAR_BusinessType",
				"FT_Emp", "PT_Emp", "OAP4_BI_Prem", "OAP4_PD_Prem", "OAP4_BIPD_Prem", "OAP4_AB_Prem", "OAP4_DCPD_Prem",
				"OAP4_CL_Prem", "MaxNum_VehA", "OAP4_SP_LimitA", "OAP4_SP_DedA", "OAP4_SP_PremA", "MaxNum_VehB",
				"OAP4_SP_LimitB", "OAP4_SP_DedB", "OAP4_SP_PremB", "MaxNum_VehC", "OAP4_SP_LimitC", "OAP4_SP_DedC",
				"OAP4_SP_PremC", "MaxNum_VehD", "OAP4_SP_LimitD", "OAP4_SP_DedD", "OAP4_SP_PremD", "OAP4_Prem",
				"GAR_EndCode", "OAP4_AB_Limit", "OAP4_UA_Limit", "SP64_Prem", "OEF72_End_Codes", "OEF72_End_Prem",
				"OEF72_Net_Prem", "OEF72_Prem", "OEF75_Ded", "GDPC_ServPlateNo", "GDPC_ExpDate", "GSPC_ServPlateNo",
				"GSPC_ExpDate", "EON114_Name_Ins", "Add_Ins_Name", "Add_Ins_Addr_Street", "Add_Ins_Addr_City",
				"Add_Ins_Addr_PC", "EON102_Ded", "EON102_Sec7_Ded", "GECGLMTC_Mod_Type", "GECGLMTC_Veh_Desc",
				"GECGLMTC_Veh_SerialNo", "CGL_GE_Prem", "CGO_Limit", "CGO_Ded", "Cargo_GE_Prem", "Per_Occurrence_Ded",
				"GECGL_Mod_Type", "GECGL_Veh_Desc", "GECGL_Veh_SerialNo", "GECGL_Prem", "AIC_Limit", "AIC_Ded",
				"EON111_Effective_Date", "EON111_Expiry_Date", "Date", "GDPC_Dealer_PlateNo", "GDPC_Exp_Date",
				"WOS_Co_Name", "WOS_Co_Addr", "AOCCGL_Prem", "MOCCGL_Prem", "AOCC_Prem", "MOCC_Prem", "OPCF21B_Prorata",
				"OPCF21B_5050", "Sec7_Ded", "Veh_Desc", "OPCF23A_DCPD_CK", "OPCF23A_DCPD_Ded", "OPCF23A_SP_CK",
				"OPCF23A_SP_Ded", "OPCF23A_CL_CK", "OPCF23A_CL_Ded", "OPCF23A_CM_CK", "OPCF23A_CM_Ded", "OPCF23A_AP_CK",
				"OPCF23A_AP_Ded", "OPCF23ABL_DCPD_CK", "OPCF23ABL_DCPD_Ded", "OPCF23ABL_SP_CK", "OPCF23ABL_SP_Ded",
				"OPCF23ABL_CL_CK", "OPCF23ABL_CL_Ded", "OPCF23ABL_CM_CK", "OPCF23ABL_CM_Ded", "OPCF23ABL_AP_CK",
				"OPCF23ABL_AP_Ded", "TTC_VehDesc", "TTC_Period1", "TTC_Period2", "TTC_SerNo", "TTC_Limit", "TTC_Ded",
				"TTC_Prem", "AICC_Limit", "AICC_Ded", "CTC_Limit", "SubPolicy_Named_Insured", "LPOLL_Limit",
				"LPOLL_Ded", "POLL_Limit", "WLL_Limit", "WLL_Ded", "MTCO_Limit", "MTCO_Ded", "MTCO_Prem", "CONT_Limit",
				"CONT_Ded", "PETP_Limit", "OPCF21A_Prem", "OPCF21A_Rcpt", "OPCF21A_Rate", "Min_Prem_Pct", "Min_Prem",
				"EON110_Ded", "EndCIDC_Text", "EndCIDC_No", "OPCF4A_Exp_Type", "OPCF4A_Limit", "OPCF4A_Cover",
				"OPCF4B_Mat_Type", "OPCF4B_Limit", "OPCF4B_Cover", "OPCF4B_Prem", "OPCF5C_Prem", "OPCF5C_Limit",
				"OPCF20H_Limit", "Security_System_Req", "OPCF25AW_NoDed", "OPCF25AW_WithDed", "OPCF25AW_Ded",
				"CP_Locations", "CommProp_Prem", "SEF_Ends", "EON20A_Limit", "EON119_OwnerName",
				"EON119_VehDescription", "LISC_Company_Name", "LISC_Company_Address", "MTGT_Prem", "TORV_Prem",
				"TORV_LimitA", "TORV_LimitB", "TORV_LimitC", "TORV_Ded1", "TORV_Ded2", "TOCA_Prem", "TOCA_LimitA",
				"TOCA_LimitB", "TOCA_LimitC", "TOCA_Ded1", "TOCA_Ded2", "TDGPL_Class", "CEMTC_Exclusion", "DOC_Prem",
				"DOCC_Type", "ALOCCGL_Prem", "DLOCCGL_Prem", "End110_No", "Checked", "Unchecked", "MaxEndNo_Item6" };
		fieldValues = new Object[] { docDate, insuredName, insuredAddressStreet, insuredAddressCity,
				insuredAddressProvState, insuredAddressPC, polNo, end_desc, brokerContact, brokerName, effectiveDate,
				expiryDate, lbLimit, opcf8_ded, dcpd_ded, cargo_ded, cargoLimit, cargoLimit1, cargoLimit2, cargoLimit3,
				cargoLimit4, cargoLimit5, cgl_lb_limit, cgl_ded, mex_limit, tll_limit, tll_ded, ebe_limit, ebe_ded,
				spf6_lb_limit, sef96_lb_limit, sef99_lb_limit, oap4_lb_limit, oap4_dcpd_ded, oap4_cl_limit, oap4_cl_ded,
				oap4_sp_limit, oap4_sp_ded, oef71_limit, oef71_ded, oef77_limit, oef77_ded, oef77_oap4_sp_prema,
				oef77_oap4_sp_premb, oef77_oap4_sp_premc, oef77_oap4_sp_premd, oef86_limit, total_premium, opcf21b_prem,
				eon110_no, maxendno, master_broker_no, broker_addr, broker_city, broker_provState, broker_pc,
				issue_date, effYear, effMonth, effDay, expYear, expMonth, expDay, prepDate, biTotalPrem, pdTotalPrem,
				abTotalPrem, uaTotalPrem, dcpdTotalPrem, spTotalPrem, compTotalPrem, collTotalPrem, apTotalPrem,
				endList1, vehPrem, opcf2_name, opcf2_relationship, opcf20_limit, termination_day, opcf27_apded,
				opcf27_limit, opcf27B_apded, businessType, opcf27B_limit, opcf28_pers, opcf28_limit, opcf28CL_ded,
				opcf28AP_ded, opcf28_coll, opcf28_collNot, opcf28_allperil, opcf28_allperilNot, cargoLimit1Dec,
				cargoLimit2Dec, cargoLimit3Dec, cargoLimit4Dec, cargoLimit5Dec, cargoLimit6Dec, totalCargoPrem,
				cargo_locations, totalCGLPrem, cgl_locations, gar_locationa, insBldga, insLota, gar_locationb, insBldgb,
				insLotb, gar_locationc, insBldgc, insLotc, gar_locationd, insBldgd, insLotd, garBusinessType, ft_emp,
				pt_emp, oap4_bi_prem, oap4_pd_prem, oap4_bipd_prem, oap4_ab_prem, oap4_dcpd_prem, oap4_cl_prem,
				maxnum_veha, oap4_sp_limita, oap4_sp_deda, oap4_sp_prema, maxnum_vehb, oap4_sp_limitb, oap4_sp_dedb,
				oap4_sp_premb, maxnum_vehc, oap4_sp_limitc, oap4_sp_dedc, oap4_sp_premc, maxnum_vehd, oap4_sp_limitd,
				oap4_sp_dedd, oap4_sp_premd, oap4_prem, gar_end_code, oap4_ab_limit, oap4_ua_limit, sp64_prem,
				oef72_end_codes, oef72_end_prem, oef72_net_prem, oef72_prem, oef75_ded, gdpc_servPlateNo, gdpc_expDate,
				gspc_servPlateNo, gspc_expDate, eon114_name_ins, add_ins_name, add_ins_addr_street, add_ins_addr_city,
				add_ins_addr_pc, eon102_ded, eon102_sec7_ded, gecglmtc_mod_type, gecglmtc_veh_desc,
				gecglmtc_veh_serial_no, cgl_ge_prem, cgo_limit, cgo_ded, cargo_ge_prem, per_occurrence_ded,
				gecgl_mod_type, gecgl_veh_desc, gecgl_veh_serial_no, gecgl_prem, aic_limit, aic_ded,
				eon111_effective_date, eon111_expiry_date, date, gdpc_dealer_plate_no, gdpc_exp_date, wos_co_name,
				wos_co_addr, aoccgl_prem, moccgl_prem, aocc_prem, mocc_prem, opcf21B_prorata, opcf21B_5050, sec7Ded,
				vehDesc, opcf23A_dcpd_ck, opcf23A_dcpd_ded, opcf23A_sp_ck, opcf23A_sp_ded, opcf23A_cl_ck,
				opcf23A_cl_ded, opcf23A_cm_ck, opcf23A_cm_ded, opcf23A_ap_ck, opcf23A_ap_ded, opcf23ABL_dcpd_ck,
				opcf23ABL_dcpd_ded, opcf23ABL_sp_ck, opcf23ABL_sp_ded, opcf23ABL_cl_ck, opcf23ABL_cl_ded,
				opcf23ABL_cm_ck, opcf23ABL_cm_ded, opcf23ABL_ap_ck, opcf23ABL_ap_ded, ttc_vehdesc, ttc_period1,
				ttc_period2, ttc_serno, ttc_limit, ttc_ded, ttc_prem, aicc_limit, aicc_ded, ctc_limit,
				subPolicyNamedInsured, lpoll_limit, lpoll_ded, poll_limit, wll_limit, wll_ded, mtco_limit, mtco_ded,
				mtco_prem, cont_limit, cont_ded, petp_limit, opcf21A_prem, opcf21A_rcpt, opcf21A_rate, min_prem_pct,
				min_prem, eon110_ded, end_cicd_text, end_cicd_no, opcf4A_ext_type, opcf4A_limit, opcf4A_cover,
				opcf4B_mat_type, opcf4B_limit, opcf4B_cover, opcf4B_prem, opcf5C_prem, opcf5C_limit, opcf20H_limit,
				security_system_req, opcf25AW_noDed, opcf25AW_withDed, opcf25AW_ded, cp_locations, commProp_prem,
				sef_ends, eon20A_limit, eon119_ownerName, eon119_vehDescription, lisc_company_name,
				lisc_company_address, mtgt_prem, torv_prem, torv_limita, torv_limitb, torv_limitc, torv_ded1, torv_ded2,
				toca_prem, toca_limita, toca_limitb, toca_limitc, toca_ded1, toca_ded2, tdgpl_class, cemtc_exclusion,
				docc_prem, docc_type, aloccgl_prem, dloccgl_prem, end110_no, checked, unchecked, maxendno_item6 };
	}
	
	@Override
	protected void setGeneralDeclarationsTable(Document doc) {
		Table table = genDecSec.getBody().getTables().get(1);
		Row cdecRow = table.getRows().get(20);
		Row cdelRow = table.getRows().get(26);
		
		if (!isLaterThanJan2021) {
			table.removeChild(cdecRow);
			table.removeChild(cdelRow);
		}
	}
	
	protected void setOPCF20Section() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20Section = true;
					this.opcf20_limit = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")) {
					this.showOPCF20HSection = true;
					this.opcf20H_limit = numberFormat(end.getLimit1amount());
				}
			}
		}
	}
	
	@Override
	protected void setOPCF21BSecondTable(Document doc) throws Exception {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		String veh_desc = "";
		String location = "";
		String unitRate = "";
		String numOfUnits = "";
		String vehPrem = "";
		
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());

		Section sec = opcf21BSec;
		Table tmpTable = sec.getBody().getTables().get(4);
		Row firstVehicleRow = tmpTable.getRows().get(1);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			veh_desc = vehicle.getDisplayString();
			unitRate = "$" + numberFormat(vehicle.getUnitRate());
			numOfUnits = Integer.toString(vehicle.getNumberOfUnits());
			if(vehicle.getUnitRate() != null && vehicle.getNumberOfUnits() != null) {
				vehPrem = numberFormat(vehicle.getUnitRate() * vehicle.getNumberOfUnits());
			}
			
			if(vehicle.getVehicleDescription().equals("OP27")) {
				veh_desc = "OPCF 27B";
				unitRate = "";
				numOfUnits = "";
				vehPrem = numberFormat(vehicle.getRiskPremium().getWrittenPremium());	
			}
			
			if (vehicle.getUnitExposure() != null) {
				LookupTableItem lookupBusinessDescItem = lookupConfig
						.getConfigLookup(ConfigConstants.LOOKUPTABLE_UnitExposure).stream()
						.filter(o -> o.getItemKey().equalsIgnoreCase(vehicle.getUnitExposure())).findFirst()
						.orElse(null);
				location = lookupBusinessDescItem.getItemValue();
			}

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);
			
			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, numOfUnits);
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, veh_desc);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);
			
			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, location);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);
						
			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, unitRate);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$" + vehPrem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		
		if (this.ctcExists) {
			// Contingent Truck Cargo Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);
			
			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.cont_cargo);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);
			
			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, this.ctc_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		
		if (this.ebeExists) {
			// Employee Benefits Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);
			
			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.emp_benefits);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);
			
			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$ " + this.ebe_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		
		tmpTable.removeChild(tmpTable.getRows().get(1));
	}
	
	protected void setOPCF25AWSection(Document doc) throws Exception {
		Section tmpOPCF25AWSec = opcf25AWSec;
		Section tmpEON145Sec = eon145Sec;
		Section refEON145Sec = tmpEON145Sec;
		
		List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();
		
		for (SpecialtyVehicleRisk vehiclerisk : vehicleRisks) {

			Endorsement end = vehiclerisk.getCoverageDetail().getEndorsementByCode("OPCF25AW");

			if (end != null) {
				// Unit Year
				String unitYear = vehiclerisk.getUnitYear() != null ? Integer.toString(vehiclerisk.getUnitYear()) : "";

				// Unit Make
				String unitMake = vehiclerisk.getUnitMake() != null ? vehiclerisk.getUnitMake() : "";

				// Unit Serial Number or VIN
				String unitSerialNo = vehiclerisk.getUnitSerialNumberorVin() != null ? vehiclerisk.getUnitSerialNumberorVin() : "";
				
				
				String securitySystemReq = "";
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF25A_SecuritySystemReq")
								&& extData.getColumnValue() != null) {
							securitySystemReq = extData.getColumnValue();
						}
					}
				}
				
				String ded = numberFormat(end.getDeductible1amount());
				String noDed = "{{Unchecked}}";
				String withDed = "{{Unchecked}}";
				if(end.getDeductible1amount() == null) {
					noDed = "{{Checked}}";
				} else {
					withDed = "{{Checked}}";
				}
				
				Section newOPCF25AWSec = (Section) tmpOPCF25AWSec.deepClone(true);
				Section newEON145Sec = (Section) tmpEON145Sec.deepClone(true);
				
				Table table1 = newOPCF25AWSec.getBody().getTables().get(1);
				Row row14 = table1.getRows().get(14);
				
				Cell unitYearCell = row14.getCells().get(3);
				unitYearCell.getRange().replace("{{OPCF25AW_UnitYear}}", unitYear);
				
				Cell unitMakeCell = row14.getCells().get(4);
				unitMakeCell.getRange().replace("{{OPCF25AW_UnitMake}}", unitMake);
				
				Cell unitSerialNoCell = row14.getCells().get(5);
				unitSerialNoCell.getRange().replace("{{OPCF25AW_UnitSerialNo}}", unitSerialNo);
				
				Table table3 = newEON145Sec.getBody().getTables().get(0);
				Row row0 = table3.getRows().get(0);
				row0.getFirstCell().getRange().replace("{{Security_System_Req}}", securitySystemReq);
				
				Paragraph para7 = newEON145Sec.getBody().getParagraphs().get(7);
				para7.getRange().replace("{{OPCF25AW_NoDed}}", noDed);
				
				Paragraph para8 = newEON145Sec.getBody().getParagraphs().get(8);
				para8.getRange().replace("{{OPCF25AW_WithDed}}", withDed);
				para8.getRange().replace("{{OPCF25AW_Ded}}", ded);
				
				
				doc.insertAfter(newOPCF25AWSec, refEON145Sec);
				doc.insertAfter(newEON145Sec, newOPCF25AWSec);
				refEON145Sec = newEON145Sec;
			}
		}
		
		doc.removeChild(tmpOPCF25AWSec);
		doc.removeChild(tmpEON145Sec);
	}
	
	private void setCIDCEnd110No(Document doc) {
		if (this.showEON110Section) {
			Table table = eon110Sec.getBody().getTables().get(0);
			Row row = table.getRows().get(1);
			Cell cell = row.getCells().get(4);
			this.end110_no = cell.getFirstParagraph().getText();
		} else {
			this.end110_no = "";
		}
	}
	
	private void setCIDCEndNo(Document doc) {
		String carriersFormNo = "";
		String ownersFormNo = "";

		if (this.showCIDCSection) {
			Table table = cidcSec.getBody().getTables().get(0);
			Row row = table.getRows().get(1);
			Cell cell = row.getCells().get(4);
			carriersFormNo = cell.getFirstParagraph().getText();
		}

		if (this.showCIDCOFSection) {
			Table table = cidcofSec.getBody().getTables().get(0);
			Row row = table.getRows().get(1);
			Cell cell = row.getCells().get(4);
			ownersFormNo = cell.getFirstParagraph().getText();
		}

		if (!carriersFormNo.equals("") && !ownersFormNo.equals("")) {
			this.end_cicd_text = "and see End't Nos";
			this.end_cicd_no = carriersFormNo + ", " + ownersFormNo;
		} else if (!carriersFormNo.equals("")) {
			this.end_cicd_text = "and see End't No";
			this.end_cicd_no = carriersFormNo;
		} else if (!ownersFormNo.equals("")) {
			this.end_cicd_text = "and see End't No";
			this.end_cicd_no = ownersFormNo;
		} else {
			this.end_cicd_text = "";
			this.end_cicd_no = "";
		}
	}
	
	protected void setLISCSection(Document doc) throws Exception {
		Section tmpSection = liscSec;
		Section refSection = tmpSection;
		
		for (Endorsement end : liscEnds) {
			
			String limit = numberFormat(end.getLimit1amount());
			String companyName = "";
			String companyAddress = "";
			
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("LISC_CompanyName")
							&& extData.getColumnValue() != null) {
						companyName = extData.getColumnValue();
					} else if (extData.getColumnId().equalsIgnoreCase("LISC_CompanyAddress")
							&& extData.getColumnValue() != null) {
						companyAddress = extData.getColumnValue();
					}
				}
				
				Section newLISCSection = (Section) tmpSection.deepClone(true);

				newLISCSection.getBody().getParagraphs().get(1).getRange().replace("{{LISC_Limit}}", limit);
				newLISCSection.getBody().getParagraphs().get(2).getRange().replace("{{LISC_Company_Name}}", companyName);
				newLISCSection.getBody().getParagraphs().get(2).getRange().replace("{{LISC_Company_Address}}", companyAddress);

				doc.insertAfter(newLISCSection, refSection);
				refSection = newLISCSection;
			}
		}
		doc.removeChild(tmpSection);
	}
	
	protected void setEON119Section(Document doc) throws Exception {

		Section tmpSection = eon119Sec;
		Section refSection = tmpSection;

		for (Endorsement end : eon119Ends) {
			
			String ownerName = "";
			String vehDescription = "";
			

			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("EON119_OwnerName")
							&& extData.getColumnValue() != null) {
						ownerName = extData.getColumnValue();
					} else if (extData.getColumnId().equalsIgnoreCase("EON119_VehDesc")
							&& extData.getColumnValue() != null) {
						vehDescription = extData.getColumnValue();
					}
				}
				
				Section newEON119Section = (Section) tmpSection.deepClone(true);
				
				Table table = newEON119Section.getBody().getTables().get(1);
				Row row1 = table.getRows().get(0);
				Row row2 = table.getRows().get(1);
				Cell cell1 = row1.getCells().get(1);
				Cell cell2 = row2.getCells().get(1);

				cell1.getFirstParagraph().getRange().replace("{{EON119_OwnerName}}", ownerName);
				cell2.getFirstParagraph().getRange().replace("{{EON119_VehDescription}}", vehDescription);

				doc.insertAfter(newEON119Section, refSection);
				refSection = newEON119Section;
				
			}

		}
		doc.removeChild(tmpSection);
	}
	
	@Override
	protected void initializeSections(Document doc) {
		genDecSec = doc.getSections().get(1);
		opcf2Sec = doc.getSections().get(3);
		opcf4ASec = doc.getSections().get(4);
		opcf4BSec =doc.getSections().get(5);
		opcf5Sec = doc.getSections().get(6);
		opcf5BlSec = doc.getSections().get(7);
		opcf5CSec = doc.getSections().get(8);
		opcf8Sec = doc.getSections().get(9);
		opcf9Sec = doc.getSections().get(10);
		opcf13CSec = doc.getSections().get(11);
		opcf19Sec = doc.getSections().get(12);
		opcf20Sec = doc.getSections().get(13);
		opcf20HSec = doc.getSections().get(14);
		opcf21ASec = doc.getSections().get(15);
		opcf21BSec = doc.getSections().get(16);
		opcf23ASec = doc.getSections().get(17);
		opcf23ABlSec = doc.getSections().get(18);
		opcf25ASec = doc.getSections().get(19);
		opcf25AWSec = doc.getSections().get(20);
		eon145Sec = doc.getSections().get(21);
		opcf25ARSec = doc.getSections().get(22);
		opcf27Sec = doc.getSections().get(23);
		opcf27BSec = doc.getSections().get(24);
		opcf28Sec = doc.getSections().get(25);
		opcf28ASec = doc.getSections().get(26);
		opcf30Sec = doc.getSections().get(27);
		opcf31Sec = doc.getSections().get(28);
		opcf38Sec = doc.getSections().get(29);
		opcf40Sec = doc.getSections().get(30);
		opcf43Sec = doc.getSections().get(31);
		opcf43ASec = doc.getSections().get(32);
		opcf44RSec = doc.getSections().get(33);
		opcf47Sec = doc.getSections().get(34);
		opcf48Sec = doc.getSections().get(35);
		cgoDecSec = doc.getSections().get(36);
		cgoDecOFSec = doc.getSections().get(37);
		cglDecSec = doc.getSections().get(38);
		cpDecSec = doc.getSections().get(39);
		peSec = doc.getSections().get(40);
		garDecSec = doc.getSections().get(41);
		oef71Sec = doc.getSections().get(42);
		oef72Sec = doc.getSections().get(43);
		oef75Sec = doc.getSections().get(44);
		oef77Sec = doc.getSections().get(45);
		gdpcgSec = doc.getSections().get(46);
		gspcSec = doc.getSections().get(47);
		spf6_1Sec = doc.getSections().get(48);
		spf6_2Sec = doc.getSections().get(49);
		spf6_3Sec = doc.getSections().get(50);
		spf6_4Sec = doc.getSections().get(51);
		spf6_5Sec = doc.getSections().get(52);
		spf6_6Sec = doc.getSections().get(53);
		spf6_7Sec = doc.getSections().get(54);
		spf6_8Sec = doc.getSections().get(55);
		spf6_9Sec = doc.getSections().get(56);
		sef96Sec = doc.getSections().get(57);
		sef99Sec = doc.getSections().get(58);
		endorsementSection = doc.getSections().get(59);
		eon114Sec = doc.getSections().get(60);
		eon20ASec = doc.getSections().get(61);
		aiccSec = doc.getSections().get(62);
		aicSec = doc.getSections().get(63);
		eon101Sec = doc.getSections().get(64);
		eon102Sec = doc.getSections().get(65);
		eon109Sec = doc.getSections().get(66);
		eon110Sec = doc.getSections().get(67);
		cidcSec = doc.getSections().get(68);
		cidcofSec = doc.getSections().get(69);
		cdelSec = doc.getSections().get(70);
		cdecSec = doc.getSections().get(71);
		eon111Sec = doc.getSections().get(72);
		eon112Sec = doc.getSections().get(73);
		eon119Sec = doc.getSections().get(74);
		eon121Sec = doc.getSections().get(75);
		eon141Sec = doc.getSections().get(76);
		gdpcSec = doc.getSections().get(77);
		gleeSec = doc.getSections().get(78);
		crbSec = doc.getSections().get(79);
		apxASec = doc.getSections().get(80);
		tlleSec = doc.getSections().get(81);
		wlleSec = doc.getSections().get(82);
		liscSec = doc.getSections().get(83);
		mtgtSec = doc.getSections().get(84);
		mtgtofSec = doc.getSections().get(85);
		mobiSec = doc.getSections().get(86);
		torvSec = doc.getSections().get(87);
		tocaSec = doc.getSections().get(88);
		tdgSec = doc.getSections().get(89);
		tdgplSec = doc.getSections().get(90);
		tdgcSec = doc.getSections().get(91);
		cemtcSec = doc.getSections().get(92);
		cemtcofSec = doc.getSections().get(93);
		foeSec = doc.getSections().get(94);
		gecglmtcSec = doc.getSections().get(95);
		gecglSec = doc.getSections().get(96);
		doccSec = doc.getSections().get(97);
		aoccglSec = doc.getSections().get(98);
		moccglSec= doc.getSections().get(99);
		aoccSec= doc.getSections().get(100);
		aoccofSec = doc.getSections().get(101);
		aloccglSec = doc.getSections().get(102);
		dloccglSec = doc.getSections().get(103);
		aloccpSec = doc.getSections().get(104);
		dloccpSec = doc.getSections().get(105);
		wosSec = doc.getSections().get(106);
		moccSec = doc.getSections().get(107);
		mocmcSec = doc.getSections().get(108);
		aloccSec = doc.getSections().get(109);
		dloccSec = doc.getSections().get(110);
		ttcSec = doc.getSections().get(111);
		eon115Sec = doc.getSections().get(112);
		eon115_2Sec = doc.getSections().get(113);
		eon116Sec = doc.getSections().get(114);
		eon117Sec = doc.getSections().get(115);
		iaSec = doc.getSections().get(116);
	}
	
	@Override
	protected void hideSections(Document doc) {

		if (numEnds.isEmpty()) {
			doc.removeChild(endorsementSection);
		}
		
		if (!showOPCF2Section) {
			doc.removeChild(opcf2Sec);
		}
		
		if (!showOPCF4ASection) {
			doc.removeChild(opcf4ASec);
		}
		
		if (!showOPCF4BSection) {
			doc.removeChild(opcf4BSec);
		}

		if (!showOPCF5Section) {
			doc.removeChild(opcf5Sec);
		}

		if (!showOPCF5BlSection) {
			doc.removeChild(opcf5BlSec);
		}
		
		if (!showOPCF5CSection) {
			doc.removeChild(opcf5CSec);
		}

		if (!showOPCF8Section) {
			doc.removeChild(opcf8Sec);
		}

		if (!showOPCF9Section) {
			doc.removeChild(opcf9Sec);
		}

		if (!showOPCF13CSection) {
			doc.removeChild(opcf13CSec);
		}

		if (!showOPCF19Section) {
			doc.removeChild(opcf19Sec);
		}

		if (!showOPCF20Section) {
			doc.removeChild(opcf20Sec);
		}
		
		if (!showOPCF20HSection) {
			doc.removeChild(opcf20HSec);
		}

		if (!showOPCF21ASection) {
			doc.removeChild(opcf21ASec);
		}

		if (!showOPCF21BSection) {
			doc.removeChild(opcf21BSec);
		}

		if (!showOPCF23ASection) {
			doc.removeChild(opcf23ASec);
		}

		if (!showOPCF23ABlSection) {
			doc.removeChild(opcf23ABlSec);
		}

		if (!showOPCF25ASection) {
			doc.removeChild(opcf25ASec);
		}
		
		if (!showOPCF25AWSection) {
			doc.removeChild(opcf25AWSec);
		}
		
		if (!showEON145Section) {
			doc.removeChild(eon145Sec);
		}
		
		if (!showOPCF25ARSection) {
			doc.removeChild(opcf25ARSec);
		}

		if (!showOPCF27Section) {
			doc.removeChild(opcf27Sec);
		}

		if (!showOPCF27BSection) {
			doc.removeChild(opcf27BSec);
		}

		if (!showOPCF28Section) {
			doc.removeChild(opcf28Sec);
		}

		if (!showOPCF28ASection) {
			doc.removeChild(opcf28ASec);
		}

		if (!showOPCF30Section) {
			doc.removeChild(opcf30Sec);
		}

		if (!showOPCF31Section) {
			doc.removeChild(opcf31Sec);
		}

		if (!showOPCF38Section) {
			doc.removeChild(opcf38Sec);
		}

		if (!showOPCF40Section) {
			doc.removeChild(opcf40Sec);
		}

		if (!showOPCF43Section) {
			doc.removeChild(opcf43Sec);
		}

		if (!showOPCF43ASection) {
			doc.removeChild(opcf43ASec);
		}

		if (!showOPCF44RSection) {
			doc.removeChild(opcf44RSec);
		}

		if (!showOPCF47Section) {
			doc.removeChild(opcf47Sec);
		}

		if (!showOPCF48Section) {
			doc.removeChild(opcf48Sec);
		}
		
		if (!showCGODecSection) {
			doc.removeChild(cgoDecSec);
		}
		
		if (!showCGODecOFSection) {
			doc.removeChild(cgoDecOFSec);
		}
		
		if (!showCGLDecSection) {
			doc.removeChild(cglDecSec);
		}
		
		if (!showCPDecSection) {
			doc.removeChild(cpDecSec);
		}
		
		if (!showPESection) {
			doc.removeChild(peSec);
		}
		
		if (!showGARDecSection) {
			doc.removeChild(garDecSec);
		}
		
		if (!showOEF71Section) {
			doc.removeChild(oef71Sec);
		}
		
		if (!showOEF72Section) {
			doc.removeChild(oef72Sec);
		}
		
		if (!showOEF75Section) {
			doc.removeChild(oef75Sec);
		}
		
		if (!showOEF77Section) {
			doc.removeChild(oef77Sec);
		}

		if (!showGDPCGSection) {
			doc.removeChild(gdpcgSec);
		}
		
		if (!showGSPCSection) {
			doc.removeChild(gspcSec);
		}
		
		if (!showSPF6Section) {
			doc.removeChild(spf6_1Sec);
			doc.removeChild(spf6_2Sec);
			doc.removeChild(spf6_3Sec);
			doc.removeChild(spf6_4Sec);
			doc.removeChild(spf6_5Sec);
			doc.removeChild(spf6_6Sec);
			doc.removeChild(spf6_7Sec);
			doc.removeChild(spf6_8Sec);
			doc.removeChild(spf6_9Sec);
		}
		
		if (!showSEF96Section) {
			doc.removeChild(sef96Sec);
		}
		
		if (!showSEF99Section) {
			doc.removeChild(sef99Sec);
		}
		
		if (!showEON114Section) {
			doc.removeChild(eon114Sec);
		}
		
		if (!showEON20ASection) {
			doc.removeChild(eon20ASec);
		}
		
		if (!showAICCSection) {
			doc.removeChild(aiccSec);
		}
		
		if (!showAICSection) {
			doc.removeChild(aicSec);
		}
		
		if (!showEON101Section) {
			doc.removeChild(eon101Sec);
		}
		
		if (!showEON102Section) {
			doc.removeChild(eon102Sec);
		}
		
		if (!showEON109Section) {
			doc.removeChild(eon109Sec);
		}
		
		if (!showEON110Section) {
			doc.removeChild(eon110Sec);
		}
		
		if (!showCIDCSection) {
			doc.removeChild(cidcSec);
		}
		
		if (!showCIDCOFSection) {
			doc.removeChild(cidcofSec);
		}
		
		if (!showCDELSection) {
			doc.removeChild(cdelSec);
		}
		
		if (!showCDECSection) {
			doc.removeChild(cdecSec);
		}
		
		if (!showEON111Section) {
			doc.removeChild(eon111Sec);
		}
		
		if (!showEON112Section) {
			doc.removeChild(eon112Sec);
		}
		
		if (!showEON119Section) {
			doc.removeChild(eon119Sec);
		}
		
		if (!showEON121Section) {
			doc.removeChild(eon121Sec);
		}
		
		if (!showEON141Section) {
			doc.removeChild(eon141Sec);
		}
		
		if (!showGDPCSection) {
			doc.removeChild(gdpcSec);
		}
		
		if (!showGLEESection) {
			doc.removeChild(gleeSec);
		}
		
		if (!showCRBSection) {
			doc.removeChild(crbSec);
		}
		
		if (!showApxASection) {
			doc.removeChild(apxASec);
		}
		
		if (!showTLLESection) {
			doc.removeChild(tlleSec);
		}
		
		if (!showWLLESection) {
			doc.removeChild(wlleSec);
		}
		
		if (!showLISCSection) {
			doc.removeChild(liscSec);
		}
		
		if (!showMTGTSection) {
			doc.removeChild(mtgtSec);
		}
		
		if (!showMTGTOFSection) {
			doc.removeChild(mtgtofSec);
		}
		
		if (!showMOBISection) {
			doc.removeChild(mobiSec);
		}
		
		if (!showTORVSection) {
			doc.removeChild(torvSec);
		}
		
		if (!showTOCASection) {
			doc.removeChild(tocaSec);
		}
		
		if (!showTDGSection) {
			doc.removeChild(tdgSec);
		}
		
		if (!showTDGPLSection) {
			doc.removeChild(tdgplSec);
		}
		
		if (!showTDGCSection) {
			doc.removeChild(tdgcSec);
		}
		
		if (!showCEMTCSection) {
			doc.removeChild(cemtcSec);
		}
		
		if (!showCEMTCOFSection) {
			doc.removeChild(cemtcofSec);
		}
		
		if (!showFOESection) {
			doc.removeChild(foeSec);
		}
		
		if (!showGECGLMTCSection) {
			doc.removeChild(gecglmtcSec);
		}
		
		if (!showGECGLSection) {
			doc.removeChild(gecglSec);
		}
		
		if (!showDOCCSection) {
			doc.removeChild(doccSec);
		}
		
		if (!showAOCCGLSection) {
			doc.removeChild(aoccglSec);
		}
		
		if (!showMOCCGLSection) {
			doc.removeChild(moccglSec);
		}
		
		if (!showAOCCSection) {
			doc.removeChild(aoccSec);
		}
		
		if (!showAOCCOFSection) {
			doc.removeChild(aoccofSec);
		}
		
		if (!showALOCCGLSection) {
			doc.removeChild(aloccglSec);
		}

		if (!showDLOCCGLSection) {
			doc.removeChild(dloccglSec);
		}

		if (!showALOCCPSection) {
			doc.removeChild(aloccpSec);
		}

		if (!showDLOCCPSection) {
			doc.removeChild(dloccpSec);
		}

		if (!showWOSSection) {
			doc.removeChild(wosSec);
		}

		if (!showMOCCSection) {
			doc.removeChild(moccSec);
		}
		
		if (!showMOCMCSection) {
			doc.removeChild(mocmcSec);
		}
		
		if (!showALOCCSection) {
			doc.removeChild(aloccSec);
		}
		
		if (!showDLOCCSection) {
			doc.removeChild(dloccSec);
		}
		
		if (!showTTCSection) {
			doc.removeChild(ttcSec);
		}
		
		if (!showEON115Section) {
			doc.removeChild(eon115Sec);
			doc.removeChild(eon115_2Sec);
		}
		
		if (!showEON116Section) {
			doc.removeChild(eon116Sec);
		}
		
		if (!showEON117Section) {
			doc.removeChild(eon117Sec);
		}
		
		if (!showIASection) {
			doc.removeChild(iaSec);
		}
	}
}
