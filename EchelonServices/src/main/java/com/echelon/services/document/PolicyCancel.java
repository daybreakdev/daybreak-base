package com.echelon.services.document;

import java.io.File;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Paragraph;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyCancel extends EchelonDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6387374743202557389L;
	protected String[] fieldNames;
	protected Object[] fieldValues;
	protected String brokerName = "";
	protected String master_broker_no = "";
	protected String insuredName = "";
	protected String polNo = "";
	protected String effectiveDate = "";
	protected String expiryDate = "";
	protected String eon121_prem = "";
	protected String eon121_returnPrem = "";
	protected String eon121_origPrem = "";
	protected String eon121_earnedPrem = "";
	protected String cancellation_type = "";
	protected String canc_date = "";
	protected LinkedHashSet<Endorsement> numEnds = new LinkedHashSet<Endorsement>();
	protected Section eon121Sec = null;
	protected Section eon122Sec = null;
	protected boolean showEON121Section = false;
	protected boolean showEON122Section = false;

	
	public String getTemplatePath() {
		return "";
	}

	public void create(String storageKey) throws Exception {
	}

	public void initializeFields() {
	}
	
	protected void setEON121Paragraphs(Document doc) {
		Paragraph para = eon121Sec.getBody().getParagraphs().get(5);
		String label1 = "";
		String label2 = "";
		if (policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType().equalsIgnoreCase("CANCELLATION")
				&& policyDocument.getPolicyTransaction().getPolicyVersion().getCancellationMethodCd()
						.equalsIgnoreCase("Prorata")) {
			
			LocalDate termExpiry = policyDocument.getPolicyTransaction().getPolicyTerm().getTermExpDate().toLocalDate();
			LocalDate termEffective = policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate();
			LocalDate cancDate = policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate();

			double days = Double.valueOf(DAYS.between(cancDate, termExpiry)) / Double.valueOf(DAYS.between(termEffective, termExpiry));
			DecimalFormat df = new DecimalFormat("#,###,##0.000");
			label1 = "Prorata Unearned: ";
			label2 = df.format(days);
		} else if (policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType().equalsIgnoreCase("CANCELLATION")
				&& policyDocument.getPolicyTransaction().getPolicyVersion().getCancellationMethodCd()
				.equalsIgnoreCase("Short")) {
			label1 = "Earned Premium: ";
			label2 = "$" + this.eon121_earnedPrem;
		}
		
		para.removeAllChildren();
		
		Run run1 = new Run(doc, label1);
		run1.getFont().setName("Arial");
		run1.getFont().setSize(8);
		run1.getFont().setBold(false);
		
		Run run2 = new Run(doc, label2);
		run2.getFont().setName("Arial");
		run2.getFont().setSize(8);
		run2.getFont().setBold(true);
		
		para.appendChild(run1);
		para.appendChild(run2);
		
	}
	
	protected void initializeSections(Document doc) {
	}
	
	protected void hideSections(Document doc) {
	}
	
	protected boolean isValidEndorsement(Endorsement end) {
		boolean isValid = false;
		String transType = this.policyDocument.getPolicyTransaction().getTransactionType();
		String txnType = this.policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

		// New Endorsement scenario
		if (transType.equalsIgnoreCase("OFFSET") && txnType.equalsIgnoreCase("CANCELLATION")) {
			return true;
		}
		
		return isValid;
	}
	
	protected void endorsementNumeration(Document doc) {
		Integer sectionNo = getMaxEndorsementNumber();
		for (Section sec : doc.getSections()) {
			boolean isEndSection = false;
			if (sec.getBody().getTables() != null && sec.getBody().getTables().get(0) != null
					&& sec.getBody().getTables().get(0).getRows() != null
					&& sec.getBody().getTables().get(0).getRows().get(1) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText() != null
					&& !sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText().replaceAll("[^a-zA-Z0-9]", "").isEmpty()) {
				isEndSection = true;
			}

			if (isEndSection) {
				sectionNo++;
				Table table = sec.getBody().getTables().get(0);
				Row row = table.getRows().get(1);
				Cell cell = row.getCells().get(4);
				String endCode = cell.getFirstParagraph().getText();
				endCode = endCode.replaceAll("[^a-zA-Z0-9]", ""); 

				for (Endorsement end : numEnds) {
					if (endCode.equalsIgnoreCase(end.getEndorsementCd())) {
						end.setEndorsementNumber(sectionNo);
					}
				}

				cell.getFirstParagraph().removeAllChildren();
				Run run = new Run(doc, sectionNo.toString());
				run.getFont().setName("Arial");
				run.getFont().setSize(8);
				run.getFont().setBold(true);
				cell.getFirstParagraph().appendChild(run);
			}
		}
		
		/*for (Endorsement end : numEnds) {
			System.out.println(end.getEndorsementCd() + " = " + end.getEndorsementNumber());
		}*/
	}
	
	private Integer getMaxEndorsementNumber() {
		
		Integer rv = null;

		Set<Integer> endNumbers = new HashSet<Integer>();
		SubPolicy<?> autoSP = policyDocument.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null) {
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
					endNumbers.add(end.getEndorsementNumber());
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				endNumbers.add(end.getEndorsementNumber());
			}
		}
		rv = (!endNumbers.isEmpty() ? Collections.max(endNumbers) : 0);
		return rv;
	}

}
