package com.echelon.services.document;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.aspose.words.Document;
import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyChangeTowing extends PolicyChangeAuto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3807547112006814752L;
	
	public PolicyChangeTowing(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage) insurancePolicy;
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		PolicyTransaction<?> offsetTxn = (new PolicyTransactionDAO()).findById(policyDocument.getPolicyTransaction().getOriginatingPK());

		// Sub Policies
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		for (SubPolicy<Risk> subPolicy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
				subPolicyCGL = (CGLSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
				subPolicyCGO = (CargoSubPolicy<Risk>) subPolicy;
			} else if (subPolicy.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPolicy;
			}
		}

		this.policyDocument = policyDocument;
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.effectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(),
				"M/dd/yyyy");
		this.expiryDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "M/dd/yyyy");
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressProvState = insurancePolicy.getInsuredAddress().getProvState();
		this.insuredAddressPC = insurancePolicy.getInsuredAddress().getPostalZip();
		this.expYear = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "yyyy");
		this.expMonth = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "MM");
		this.expDay = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "dd");
		this.lbLimit = numberFormat(autoPackage.getLiabilityLimit());

		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if(end.getEndorsementNumber() != null && end.getEndorsementNumber() > maxEndNo) {
				maxEndNo = end.getEndorsementNumber();
			}
			
			if (!isValidEndorsement(end)) {
				continue;
			}

			if (end.getEndorsementCd().equalsIgnoreCase("EON110")) {
				this.showEON110Section = true;
				numEnds.add(end);
				this.per_occurrence_ded = numberFormat(end.getDeductible1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON111")) {
				this.showEON111Section = true;
				numEnds.add(end);
				this.eon111_effective_date = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion()
						.getVersionTerm().getTermEffDate().toLocalDate(), "MMMM dd, yyyy");
				this.eon111_expiry_date = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion()
						.getVersionTerm().getTermExpDate().toLocalDate(), "MMMM dd, yyyy");
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON112")) {
				this.showEON112Section = true;
				numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				this.showEON121Section = true;
				numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("DOCC")) {
				this.showDOCCSection = true;
				numEnds.add(end);
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("DOCC_Type")
									&& extData.getColumnValue() != null) {
								String doccType = extData.getColumnValue();
								HashMap<String, String> doccMap = new HashMap<String, String>();
								if(doccType.equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
									doccMap.put("type", "Commercial General Liability Insurance (QE2100R - 04/2014)");
									doccMap.put("prem", premFormat(subPolicyCGL.getSubPolicyPremium().getNetPremiumChange()));
								} else if (doccType.equalsIgnoreCase("Cargo")) {
									doccMap.put("type", "Motor Truck Cargo - Carriers Form (QE4200R - 07/2016)");
									doccMap.put("prem", premFormat(subPolicyCGO.getSubPolicyPremium().getNetPremiumChange()));
								} else if (doccType.equalsIgnoreCase("Garage")) {
									doccMap.put("type", "Ontario Automobile Policy (Garage) O.A.P. #4");
									doccMap.put("prem", premFormat(subPolicyGAR.getSubPolicyPremium().getNetPremiumChange()));
								}
								this.doccTypes.add(doccMap);
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("IA")) {
				this.showIASection = true;
				numEnds.add(end);
			}
		}

		// Sub Policy Auto
		LinkedHashSet<Endorsement> autoEndorsements = new LinkedHashSet<Endorsement>();
		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			autoEndorsements.add(end);
		}

		// Vehicle Risks
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Set<Endorsement> vehEndorsements = vehicle.getRiskEndorsements();
			for (Endorsement end : vehEndorsements) {
				autoEndorsements.add(end);
			}
		}

		fleetBasis = subPolicyAuto.getFleetBasis();

		// Endorsements
		Iterator<Endorsement> itAutoEnd = autoEndorsements.iterator();
		StringBuilder opcf2NameSB = new StringBuilder();
		StringBuilder opcf2RelationSB = new StringBuilder();
		LinkedHashSet<String> opcf27BVehTypes = new LinkedHashSet<String>();
		StringBuilder eon114NameInsSB = new StringBuilder();
		StringBuilder gdpcDealerPlateNoSB = new StringBuilder();
		StringBuilder gdpcExpDateSB = new StringBuilder();

		while (itAutoEnd.hasNext()) {
			Endorsement end = itAutoEnd.next();
			
			if(end.getEndorsementNumber() != null && end.getEndorsementNumber() > maxEndNo) {
				maxEndNo = end.getEndorsementNumber();
			}
			
			if (!isValidEndorsement(end)) {
				continue;
			}

			if (end.getEndorsementCd().equalsIgnoreCase("OPCF2")) {
				this.showOPCF2Section = true;
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF2_Pers")
									&& extData.getColumnValue() != null) {
								if (opcf2NameSB.length() > 0) {
									opcf2NameSB.append("\n");
								}
								opcf2NameSB.append("   " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("OPCF2_Relation")
									&& extData.getColumnValue() != null) {
								if (opcf2RelationSB.length() > 0) {
									opcf2RelationSB.append("\n");
								}
								opcf2RelationSB.append("   " + extData.getColumnValue());
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
				this.showOPCF5Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5Bl")) {
				this.showOPCF5BlSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF8")) {
				this.showOPCF8Section = true;
				if (this.opcf8_ded.equals("")) {
					this.opcf8_ded = numberFormat(end.getDeductible1amount());
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF9")) {
				this.showOPCF9Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF13C")) {
				this.showOPCF13CSection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
				this.showOPCF19Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF20")) {
				this.opcf20_exists = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21A")) {
				this.showOPCF21ASection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21B")) {
				this.showOPCF21BSection = true;
				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BQ")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BS")
						|| subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BA")) {
					this.opcf21B_prorata = true;
				}

				if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21B5")) {
					this.opcf21B_5050 = true;
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
				this.showOPCF23ASection = true;
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF23A_DCPD")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23A_dcpd_ck = true;
								this.opcf23A_dcpd_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23A_SP") && extData.getColumnValue() != null
									&& !extData.getColumnValue().equals("")) {
								this.opcf23A_sp_ck = true;
								this.opcf23A_sp_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CL") && extData.getColumnValue() != null
									&& !extData.getColumnValue().equals("")) {
								this.opcf23A_cl_ck = true;
								this.opcf23A_cl_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23A_CM") && extData.getColumnValue() != null
									&& !extData.getColumnValue().equals("")) {
								this.opcf23A_cm_ck = true;
								this.opcf23A_cm_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23A_AP") && extData.getColumnValue() != null
									&& !extData.getColumnValue().equals("")) {
								this.opcf23A_ap_ck = true;
								this.opcf23A_ap_ded = extData.getColumnValue();
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23ABl")) {
				this.showOPCF23ABlSection = true;
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_DCPD")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23ABL_dcpd_ck = true;
								this.opcf23ABL_dcpd_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_SP")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23ABL_sp_ck = true;
								this.opcf23ABL_sp_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CL")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23ABL_cl_ck = true;
								this.opcf23ABL_cl_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_CM")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23ABL_cm_ck = true;
								this.opcf23ABL_cm_ded = extData.getColumnValue();
							}

							if (extData.getColumnId().equalsIgnoreCase("OPCF23ABL_AP")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf23ABL_ap_ck = true;
								this.opcf23ABL_ap_ded = extData.getColumnValue();
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF25AA")) {
				this.showOPCF25AASection = true;
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF25AA_Oth")
									&& extData.getColumnValue() != null && !extData.getColumnValue().equals("")) {
								this.opcf25AA_oth = true;
								this.opcf25AA_othDesc = extData.getColumnValue();
							}
						}
					}
				}
				this.opcf25AA_prem = premFormat(subPolicyAuto.getSubPolicyPremium().getNetPremiumChange());
				this.opcf25AA_prem_a = subPolicyAuto.getSubPolicyPremium().getNetPremiumChange() >= 0 ? premFormat(subPolicyAuto.getSubPolicyPremium().getNetPremiumChange()) : "";
				this.opcf25AA_prem_r = subPolicyAuto.getSubPolicyPremium().getNetPremiumChange() < 0 ? premFormat(subPolicyAuto.getSubPolicyPremium().getNetPremiumChange()) : "";
				this.opcf25AA_liabLimit = autoPackage.getLiabilityLimit() != null ? "$" + numberFormat(autoPackage.getLiabilityLimit()) : "";
				String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
				String transactionType = policyDocument.getPolicyTransaction().getTransactionType();
				String subPolicyAutoSystemStatus = subPolicyAuto.getSystemStatus();
				String subPolicyAutoBusinessStatus = subPolicyAuto.getBusinessStatus();
				List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();
				LinkedHashSet<String> vehEndList = new LinkedHashSet<String>();
				LinkedHashSet<String> addCoveVehList = new LinkedHashSet<String>();
				LinkedHashSet<String> delCoveVehList = new LinkedHashSet<String>();
				LinkedHashSet<String> chgOBList = new LinkedHashSet<String>();
				int autoCont = 1;
				StringBuilder unitYearSB = new StringBuilder();
				StringBuilder unitMakeSB = new StringBuilder();
				StringBuilder unitSerialNoSB = new StringBuilder();
				StringBuilder addAutoNoSB = new StringBuilder();
				StringBuilder delAutoNoSB = new StringBuilder();
				StringBuilder addCoveVehSB = new StringBuilder();
				StringBuilder delCoveVehSB = new StringBuilder();
				StringBuilder chgAutoVehSB = new StringBuilder();
				StringBuilder chgOBVehSB = new StringBuilder();
				StringBuilder endListSB = new StringBuilder();
				Double biPremA = null;
				Double biPremR = null;
				Double pdPremA = null;
				Double pdPremR = null;
				Double spPremA = null;
				Double spPremR = null;
				Double cmPremA = null;
				Double cmPremR = null;
				Double clPremA = null;
				Double clPremR = null;
				Double apPremA = null;
				Double apPremR = null;
				Double spDed = null;
				Double cmDed = null;
				Double clDed = null;
				Double apDed = null;
				Double abPremA = null;
				Double abPremR = null;
				Double irPremA = null;
				Double irPremR = null;
				Double irLimit = null;
				Double mrcPremA = null;
				Double mrcPremR = null;
				Double ociPremA = null;
				Double ociPremR = null;
				Double chmPremA = null;
				Double chmPremR = null;
				Double dfPremA = null;
				Double dfPremR = null;
				Double dcPremA = null;
				Double dcPremR = null;
				Double ibPremA = null;
				Double ibPremR = null;
				Double uaPremA = null;
				Double uaPremR = null;
				Double endListA = null;
				Double endListR = null;
				Double dcpdPremA = null;
				Double dcpdPremR = null;
				Double dcpdDed = null;
				
				if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
					if (!this.insuredName.equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getDbaName())) {
						this.opcf25AA_nameAddr = true;
					} else {
						for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
							if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
									&& transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
									&& risk.getRiskType()
											.equalsIgnoreCase(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)) {
								PolicyLocation pl = (PolicyLocation) risk;
								if (pl.getLocationId().equalsIgnoreCase(SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT)
										&& pl.getLocationAddress().getSystemStatus()
												.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
										&& pl.getLocationAddress().getBusinessStatus()
												.equalsIgnoreCase(Constants.BUSINESS_STATUS_MODIFIED)) {
									this.opcf25AA_nameAddr = true;
								}
							}
						}
					}
				}

				for (SpecialtyVehicleRisk vehiclerisk : vehicleRisks) {
					String systemStatus = vehiclerisk.getSystemStatus();
					String businessStatus = vehiclerisk.getBusinessStatus();
					String autoNo = vehiclerisk.getVehicleSequence() != null ? Integer.toString(vehiclerisk.getVehicleSequence()) : "";
					String unitYear = vehiclerisk.getUnitYear() != null ? Integer.toString(vehiclerisk.getUnitYear()) : "";
					boolean isVehicleAdded = false;
					boolean isVehicleDeleted = false;
					boolean isVehicleIncluded = false;
					
					if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
							&& (((systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
									&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW))
									|| (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE)
											&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED))
									|| (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
											&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_MODIFIED)))
							&& autoCont <= 3) {
						
						// Unit Year
						if (unitYearSB.length() > 0) {
							unitYearSB.append("\n");
						}
						unitYearSB.append(autoNo + ". " + unitYear);

						// Unit Make
						if (unitMakeSB.length() > 0) {
							unitMakeSB.append("\n");
						}
						unitMakeSB.append(vehiclerisk.getUnitMake());

						// Unit Serial Number or VIN
						if (unitSerialNoSB.length() > 0) {
							unitSerialNoSB.append("\n");
						}
						unitSerialNoSB.append(vehiclerisk.getUnitSerialNumberorVin());
						isVehicleIncluded = true;

						// Add Auto
						if ((systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
								|| systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
								&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW)
								&& (vehiclerisk.getVehicleDescription() == null
										|| (vehiclerisk.getVehicleDescription() != null
												&& !vehiclerisk.getVehicleDescription().equalsIgnoreCase("OP27")))) {
							this.opcf25AA_addAuto = true;
							if (addAutoNoSB.length() > 0) {
								addAutoNoSB.append(", ");
							}
							addAutoNoSB.append(autoNo);
							isVehicleAdded = true;
						}

						// Delete Auto
						if (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE)
								&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED)
								&& (vehiclerisk.getVehicleDescription() == null
										|| (vehiclerisk.getVehicleDescription() != null
												&& !vehiclerisk.getVehicleDescription().equalsIgnoreCase("OP27")))) {
							this.opcf25AA_delAuto = true;
							if (delAutoNoSB.length() > 0) {
								delAutoNoSB.append(", ");
							}
							delAutoNoSB.append(autoNo);
							isVehicleDeleted = true;
						}
						
						// Change in Raiting
						if (!systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE)
								&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_MODIFIED)
								&& hasVehicleClassChanged(offsetTxn, vehiclerisk)) {
							this.opcf25AA_chgAuto = true;
							if (chgAutoVehSB.length() > 0) {
								chgAutoVehSB.append(" ,");
							}
							chgAutoVehSB.append(autoNo);
						}

						autoCont++;
					}
					
					// Additional Premium
					boolean isAddPrem = txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
							&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
							&& (businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NOCHANGE) || businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW))
							&& subPolicyAutoSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
							&& subPolicyAutoBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NOCHANGE);

					// Return Premium
					boolean isReturnPrem = txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
							&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE))
							&& (businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NOCHANGE)
									|| businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED))
							&& subPolicyAutoSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
							&& subPolicyAutoBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NOCHANGE);

					// Change in Raiting
					boolean isChangePrem = txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
							&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
									|| systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE))
							&& businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_MODIFIED)
							&& subPolicyAutoSystemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
							&& subPolicyAutoBusinessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NOCHANGE);

					if (isAddPrem || isReturnPrem || isChangePrem) {
						for (Coverage cove : vehiclerisk.getCoverageDetail().getCoverages()) {
							
							// Add Coverage
							boolean isAddCove = cove.getCoverageType().equalsIgnoreCase("COVERAGE")
									&& (vehiclerisk.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
											|| vehiclerisk.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
									&& (cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
											|| cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
									&& (cove.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW));
							
							// Delete Coverage
							boolean isDeleteCove = cove.getCoverageType().equalsIgnoreCase("COVERAGE")
									&& (vehiclerisk.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
											|| vehiclerisk.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
											|| vehiclerisk.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE))
									&& cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE)
									&& (cove.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED));

							// Change Coverage
							boolean isChangeCove = cove.getCoverageType().equalsIgnoreCase("COVERAGE")
									&& (cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
											|| cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE)
											|| cove.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE))
									&& cove.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_MODIFIED);

							if ((isAddCove || isDeleteCove || isChangeCove)
									&& cove.getCoveragePremium().getNetPremiumChange() != null) {

								// Additional Premium & Return Premium
								if (cove.getCoverageCode().equalsIgnoreCase("BI")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										biPremA = biPremA == null ? 0.0 : biPremA;
										biPremA = biPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										biPremR = biPremR == null ? 0.0 : biPremR;
										biPremR = biPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("PD")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										pdPremA = pdPremA == null ? 0.0 : pdPremA;
										pdPremA = pdPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										pdPremR = pdPremR == null ? 0.0 : pdPremR;
										pdPremR = pdPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("SP")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										spPremA = spPremA == null ? 0.0 : spPremA;
										spPremA = spPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										spPremR = spPremR == null ? 0.0 : spPremR;
										spPremR = spPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									spDed = cove.getDeductible1() != null ? cove.getDeductible1() : spDed;
								} else if (cove.getCoverageCode().equalsIgnoreCase("CM")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										cmPremA = cmPremA == null ? 0.0 : cmPremA;
										cmPremA = cmPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										cmPremR = cmPremR == null ? 0.0 : cmPremR;
										cmPremR = cmPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									cmDed = cove.getDeductible1() != null ? cove.getDeductible1() : cmDed;
								} else if (cove.getCoverageCode().equalsIgnoreCase("CL")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										clPremA = clPremA == null ? 0.0 : clPremA;
										clPremA = clPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										clPremR = clPremR == null ? 0.0 : clPremR;
										clPremR = clPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									clDed = cove.getDeductible1() != null ? cove.getDeductible1() : clDed;
								} else if (cove.getCoverageCode().equalsIgnoreCase("AP")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										apPremA = apPremA == null ? 0.0 : apPremA;
										apPremA = apPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										apPremR = apPremR == null ? 0.0 : apPremR;
										apPremR = apPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									apDed = cove.getDeductible1() != null ? cove.getDeductible1() : apDed;
								} else if (cove.getCoverageCode().equalsIgnoreCase("AB")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										abPremA = abPremA == null ? 0.0 : abPremA;
										abPremA = abPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										abPremR = abPremR == null ? 0.0 : abPremR;
										abPremR = abPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("IR")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										irPremA = irPremA == null ? 0.0 : irPremA;
										irPremA = irPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										irPremR = irPremR == null ? 0.0 : irPremR;
										irPremR = irPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									irLimit = cove.getLimit1() != null ? cove.getLimit1() : irLimit;
								} else if (cove.getCoverageCode().equalsIgnoreCase("MRC")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										mrcPremA = mrcPremA == null ? 0.0 : mrcPremA;
										mrcPremA = mrcPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										mrcPremR = mrcPremR == null ? 0.0 : mrcPremR;
										mrcPremR = mrcPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("OCI")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										ociPremA = ociPremA == null ? 0.0 : ociPremA;
										ociPremA = ociPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										ociPremR = ociPremR == null ? 0.0 : ociPremR;
										ociPremR = ociPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("CHM")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										chmPremA = chmPremA == null ? 0.0 : chmPremA;
										chmPremA = chmPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										chmPremR = chmPremR == null ? 0.0 : chmPremR;
										chmPremR = chmPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("DF")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										dfPremA = dfPremA == null ? 0.0 : dfPremA;
										dfPremA = dfPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										dfPremR = dfPremR == null ? 0.0 : dfPremR;
										dfPremR = dfPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("DC")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										dcPremA = dcPremA == null ? 0.0 : dcPremA;
										dcPremA = dcPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										dcPremR = dcPremR == null ? 0.0 : dcPremR;
										dcPremR = dcPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("IB")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										ibPremA = ibPremA == null ? 0.0 : ibPremA;
										ibPremA = ibPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										ibPremR = ibPremR == null ? 0.0 : ibPremR;
										ibPremR = ibPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("UA")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										uaPremA = uaPremA == null ? 0.0 : uaPremA;
										uaPremA = uaPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										uaPremR = uaPremR == null ? 0.0 : uaPremR;
										uaPremR = uaPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
								} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
									if (cove.getCoveragePremium().getNetPremiumChange() >= 0) {
										dcpdPremA = dcpdPremA == null ? 0.0 : dcpdPremA;
										dcpdPremA = dcpdPremA + cove.getCoveragePremium().getNetPremiumChange();
									} else {
										dcpdPremR = dcpdPremR == null ? 0.0 : dcpdPremR;
										dcpdPremR = dcpdPremR + cove.getCoveragePremium().getNetPremiumChange();
									}
									dcpdDed = cove.getDeductible1() != null ? cove.getDeductible1() : dcpdDed;
								}

								// Add Coverage
								if (!isVehicleAdded && isAddCove) {
									this.opcf25AA_addCov = true;
									addCoveVehList.add(autoNo);
									if (subPolicyAuto.getFleetBasis()
											.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)
											&& !isVehicleIncluded && autoCont <= 3) {
										// Unit Year
										if (unitYearSB.length() > 0) {
											unitYearSB.append("\n");
										}
										unitYearSB.append(autoNo + ". " + unitYear);

										// Unit Make
										if (unitMakeSB.length() > 0) {
											unitMakeSB.append("\n");
										}
										unitMakeSB.append(vehiclerisk.getUnitMake());

										// Unit Serial Number or VIN
										if (unitSerialNoSB.length() > 0) {
											unitSerialNoSB.append("\n");
										}
										unitSerialNoSB.append(vehiclerisk.getUnitSerialNumberorVin());
										isVehicleIncluded = true;
										autoCont++;
									}
								}

								// Delete Coverage
								if (!isVehicleDeleted && isDeleteCove) {
									this.opcf25AA_delCov = true;
									delCoveVehList.add(autoNo);
									if (subPolicyAuto.getFleetBasis()
											.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)
											&& !isVehicleIncluded && autoCont <= 3) {
										// Unit Year
										if (unitYearSB.length() > 0) {
											unitYearSB.append("\n");
										}
										unitYearSB.append(autoNo + ". " + unitYear);

										// Unit Make
										if (unitMakeSB.length() > 0) {
											unitMakeSB.append("\n");
										}
										unitMakeSB.append(vehiclerisk.getUnitMake());

										// Unit Serial Number or VIN
										if (unitSerialNoSB.length() > 0) {
											unitSerialNoSB.append("\n");
										}
										unitSerialNoSB.append(vehiclerisk.getUnitSerialNumberorVin());
										isVehicleIncluded = true;
										autoCont++;
									}
								}
							}
						}
					}

					// Optional Benefits
					if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
							&& (systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE) || systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING)
									|| systemStatus.equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
							&& (businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED) || businessStatus.equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW))) {
						for (Coverage cove : vehiclerisk.getCoverageDetail().getCoverages()) {
							if (cove.getCoverageCode().equalsIgnoreCase("IR")
									|| cove.getCoverageCode().equalsIgnoreCase("MRC")
									|| cove.getCoverageCode().equalsIgnoreCase("OCI")
									|| cove.getCoverageCode().equalsIgnoreCase("CHM")
									|| cove.getCoverageCode().equalsIgnoreCase("DF")
									|| cove.getCoverageCode().equalsIgnoreCase("DC")) {
								this.opcf25AA_chgOB = true;
								chgOBList.add(autoNo);
								break;
							}
						}
					}
					
					// Vehicle & Sub Policy Auto Endorsements
					LinkedHashSet<Endorsement> vehEnds = new LinkedHashSet<Endorsement>();
					for(Endorsement autoEnd : subPolicyAuto.getCoverageDetail().getEndorsements()) {
						vehEnds.add(autoEnd);
					}
					
					for (Endorsement vehEnd : vehiclerisk.getRiskEndorsements()) {
						vehEnds.add(vehEnd);
					}
					
					
					for (Endorsement vehEnd : vehEnds) {
						if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transactionType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
								&& (vehEnd.getEndorsementCd().startsWith("OPCF")
										|| vehEnd.getEndorsementCd().equalsIgnoreCase("EON20G"))
								&& !vehEnd.getEndorsementCd().equalsIgnoreCase("OPCF25AA")
								&& !vehEnd.getEndorsementCd().equalsIgnoreCase("OPCF25A")) {

							// Endorsement List & Endorsement List Additional Premium & Endorsement List Return Premium
							if (((vehEnd.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_PENDING) || vehEnd.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_ACTIVE))
									&& vehEnd.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_NEW))
									|| (vehEnd.getSystemStatus().equalsIgnoreCase(Constants.SYSTEN_STATUS_INACTIVE)
											&& vehEnd.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED))) {
								vehEndList.add(vehEnd.getEndorsementCd());
								if (vehEnd.getEndorsementPremium().getNetPremiumChange() != null) {
									if (vehEnd.getEndorsementPremium().getNetPremiumChange() >= 0) {
										endListA = endListA == null ? 0.0 : endListA;
										endListA = endListA + vehEnd.getEndorsementPremium().getNetPremiumChange();
									} else {
										endListR = endListR == null ? 0.0 : endListR;
										endListR = endListR + vehEnd.getEndorsementPremium().getNetPremiumChange();
									}
								}
							}
						}
					}
				}
				
				//Endorsement List
				for (String vehEndCode : vehEndList) {

					String endCode = vehEndCode.replace("OPCF", "");
					if(endCode.equalsIgnoreCase("EON20G")) {
						endCode = "EON 20G";
					}
					
					if(endListSB.length() > 0) {
						endListSB.append(", ");
					}
					endListSB.append(endCode);
				}
				
				//Add Coverage Vehicle List
				for (String vehicleNo : addCoveVehList) {
					if (addCoveVehSB.length() > 0) {
						addCoveVehSB.append(", ");
					}
					addCoveVehSB.append(vehicleNo);
				}
				
				//Delete Coverage Vehicle List
				for (String vehicleNo : delCoveVehList) {
					if (delCoveVehSB.length() > 0) {
						delCoveVehSB.append(", ");
					}
					delCoveVehSB.append(vehicleNo);
				}
				
				//Optional Benefits List
				for(String vehicleNo : chgOBList) {
					if(chgOBVehSB.length()>0) {
						chgOBVehSB.append(" ,");
					}
					chgOBVehSB.append(vehicleNo);
				}
				
				//Total Additional Premium
				if (biPremA == null && pdPremA == null && spPremA == null && cmPremA == null && clPremA == null
						&& apPremA == null && abPremA == null && irPremA == null && mrcPremA == null && ociPremA == null
						&& chmPremA == null && dfPremA == null && dcPremA == null && ibPremA == null && uaPremA == null
						&& dcpdPremA == null && endListA == null) {
					this.opcf25AA_a = "";
				} else {
					this.opcf25AA_a = premFormat((biPremA == null ? 0.0 : biPremA) + (pdPremA == null ? 0.0 : pdPremA)
							+ (spPremA == null ? 0.0 : spPremA) + (cmPremA == null ? 0.0 : cmPremA)
							+ (clPremA == null ? 0.0 : clPremA) + (apPremA == null ? 0.0 : apPremA)
							+ (abPremA == null ? 0.0 : abPremA) + (irPremA == null ? 0.0 : irPremA)
							+ (mrcPremA == null ? 0.0 : mrcPremA) + (ociPremA == null ? 0.0 : ociPremA)
							+ (chmPremA == null ? 0.0 : chmPremA) + (dfPremA == null ? 0.0 : dfPremA)
							+ (dcPremA == null ? 0.0 : dcPremA) + (ibPremA == null ? 0.0 : ibPremA)
							+ (uaPremA == null ? 0.0 : uaPremA) + (dcpdPremA == null ? 0.0 : dcpdPremA)
							+ (endListA == null ? 0.0 : endListA));
				}
				
				//Total Return Premium
				if (biPremR == null && pdPremR == null && spPremR == null && cmPremR == null && clPremR == null
						&& apPremR == null && abPremR == null && irPremR == null && mrcPremR == null && ociPremR == null
						&& chmPremR == null && dfPremR == null && dcPremR == null && ibPremR == null && uaPremR == null
						&& dcpdPremR == null && endListR == null) {
					this.opcf25AA_r = "";
				} else {
					this.opcf25AA_r = premFormat((biPremR == null ? 0.0 : biPremR) + (pdPremR == null ? 0.0 : pdPremR)
							+ (spPremR == null ? 0.0 : spPremR) + (cmPremR == null ? 0.0 : cmPremR)
							+ (clPremR == null ? 0.0 : clPremR) + (apPremR == null ? 0.0 : apPremR)
							+ (abPremR == null ? 0.0 : abPremR) + (irPremR == null ? 0.0 : irPremR)
							+ (mrcPremR == null ? 0.0 : mrcPremR) + (ociPremR == null ? 0.0 : ociPremR)
							+ (chmPremR == null ? 0.0 : chmPremR) + (dfPremR == null ? 0.0 : dfPremR)
							+ (dcPremR == null ? 0.0 : dcPremR) + (ibPremR == null ? 0.0 : ibPremR)
							+ (uaPremR == null ? 0.0 : uaPremR) + (dcpdPremR == null ? 0.0 : dcpdPremR)
							+ (endListR == null ? 0.0 : endListR));
				}
				
				this.opcf25AA_unitYear = unitYearSB.toString();
				this.opcf25AA_unitMake = unitMakeSB.toString();
				this.opcf25AA_unitSerialNo = unitSerialNoSB.toString();
				this.opcf25AA_addAutoNo = addAutoNoSB.toString();
				this.opcf25AA_delAutoNo = delAutoNoSB.toString();
				this.opcf25AA_biprem_a = biPremA != null ? premFormat(biPremA) : "";
				this.opcf25AA_biprem_r = biPremR != null ? premFormat(biPremR) : "";
				this.opcf25AA_addCovVeh = addCoveVehSB.toString();
				this.opcf25AA_delCovVeh = delCoveVehSB.toString();
				this.opcf25AA_chgAutoVeh = chgAutoVehSB.toString();
				this.opcf25AA_chgOBVeh = chgOBVehSB.toString();
				this.opcf25AA_pd_a = pdPremA != null ? premFormat(pdPremA) : "";
				this.opcf25AA_sp_a = spPremA != null ? premFormat(spPremA) : "";
				this.opcf25AA_cm_a = cmPremA != null ? premFormat(cmPremA) : "";
				this.opcf25AA_cl_a = clPremA != null ? premFormat(clPremA) : "";
				this.opcf25AA_ap_a = apPremA != null ? premFormat(apPremA) : "";
				this.opcf25AA_pd_r = pdPremR != null ? premFormat(pdPremR) : "";
				this.opcf25AA_sp_r = spPremR != null ? premFormat(spPremR) : "";
				this.opcf25AA_cm_r = cmPremR != null ? premFormat(cmPremR) : "";
				this.opcf25AA_cl_r = clPremR != null ? premFormat(clPremR) : "";
				this.opcf25AA_ap_r = apPremR != null ? premFormat(apPremR) : "";		
				this.opcf25AA_spDed = spDed != null ? premFormat(spDed) : "";
				this.opcf25AA_cmDed = cmDed != null ? premFormat(cmDed) : "";
				this.opcf25AA_clDed = clDed != null ? premFormat(clDed) : "";
				this.opcf25AA_apDed = apDed != null ? premFormat(apDed) : "";
				this.opcf25AA_abPrem_a = abPremA != null ? premFormat(abPremA) : "";
				this.opcf25AA_abPrem_r = abPremR != null ? premFormat(abPremR) : "";
				this.opcf25AA_irPrem_a = irPremA != null ? premFormat(irPremA) : "";
				this.opcf25AA_irPrem_r = irPremR != null ? premFormat(irPremR) : "";
				this.opcf25AA_irLim = irLimit != null ? premFormat(irLimit) : "";
				this.opcf25AA_mrcPrem_a = mrcPremA != null ? premFormat(mrcPremA) : "";
				this.opcf25AA_mrcPrem_r = mrcPremR != null ? premFormat(mrcPremR) : "";
				this.opcf25AA_ociPrem_a = ociPremA != null ? premFormat(ociPremA) : "";
				this.opcf25AA_ociPrem_r = ociPremR != null ? premFormat(ociPremR) : "";
				this.opcf25AA_chmPrem_a = chmPremA != null ? premFormat(chmPremA) : "";
				this.opcf25AA_chmPrem_r = chmPremR != null ? premFormat(chmPremR) : "";
				this.opcf25AA_dfPrem_a = dfPremA != null ? premFormat(dfPremA) : "";
				this.opcf25AA_dfPrem_r = dfPremR != null ? premFormat(dfPremR) : "";
				this.opcf25AA_dcPrem_a = dcPremA != null ? premFormat(dcPremA) : "";
				this.opcf25AA_dcPrem_r = dcPremR != null ? premFormat(dcPremR) : "";
				this.opcf25AA_ibPrem_a = ibPremA != null ? premFormat(ibPremA) : "";
				this.opcf25AA_ibPrem_r = ibPremR != null ? premFormat(ibPremR) : "";
				this.opcf25AA_uaPrem_a = uaPremA != null ? premFormat(uaPremA) : "";
				this.opcf25AA_uaPrem_r = uaPremR != null ? premFormat(uaPremR) : "";
				this.opcf25AA_dcpdPrem_a = dcpdPremA != null ? premFormat(dcpdPremA) : "";
				this.opcf25AA_dcpdPrem_r = dcpdPremR != null ? premFormat(dcpdPremR) : "";
				this.opcf25AA_dcpdDed = dcpdDed != null ? premFormat(dcpdDed) : "";
				this.opcf25AA_endList_a = endListA != null ? premFormat(endListA) : "";
				this.opcf25AA_endList_r = endListR != null ? premFormat(endListR) : "";
				this.opcf25AA_endList = endListSB.toString();

			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27")) {
				this.showOPCF27Section = true;
				this.opcf27_apded = numberFormat(end.getDeductible1amount());
				this.opcf27_limit = numberFormat(end.getLimit1amount());
				List<String> pers = new ArrayList<String>();
				List<String> relation = new ArrayList<String>();
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF27_Pers")
									&& extData.getColumnValue() != null) {
								pers.add(extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("OPCF27_Relation")
									&& extData.getColumnValue() != null) {
								relation.add(extData.getColumnValue());
							}
						}
					}
				}

				for (int i = 0; i < pers.size(); i++) {
					HashMap<String, String> person = new HashMap<String, String>();
					person.put("pers", pers.get(i));
					person.put("relation", relation.get(i));
					opcf27PersonsTable.add(person);
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) {
				this.showOPCF27BSection = true;
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")
									&& extData.getColumnValue() != null) {
								if (!opcf27BVehTypes.contains(extData.getColumnValue())) {
									opcf27BVehTypes.add(extData.getColumnValue());
									this.opcf27BEnds.add(end);
								}
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28")) {
				this.showOPCF28Section = true;
				this.opcf28_limit = numberFormat(end.getLimit1amount());
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF28_Pers")
									&& extData.getColumnValue() != null) {
								this.opcf28_pers = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_Coll")
									&& extData.getColumnValue() != null) {
								if (extData.getColumnValue().equalsIgnoreCase("Insured")) {
									this.opcf28_coll = true;
									this.opcf28CL_ded = numberFormat(end.getDeductible1amount());
								} else {
									this.opcf28_collNot = true;
								}
							} else if (extData.getColumnId().equalsIgnoreCase("OPCF28_AllPeril")
									&& extData.getColumnValue() != null) {
								if (extData.getColumnValue().equalsIgnoreCase("Insured")) {
									this.opcf28_allperil = true;
									this.opcf28AP_ded = numberFormat(end.getDeductible1amount());
								} else {
									this.opcf28_allperilNot = true;
								}
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28A")) {
				this.showOPCF28ASection = true;
				this.opcf28AEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF30")) {
				this.showOPCF30Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
				this.showOPCF31Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF38")) {
				this.showOPCF38Section = true;
				this.opcf38Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF40")) {
				this.showOPCF40Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF43")) {
				this.showOPCF43Section = true;
				this.showOPCF43ASection = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) {
				this.showOPCF44RSection = true;
				Integer limit = end.getLimit1amount() != null ? end.getLimit1amount() : 0;
				if (limit < autoPackage.getLiabilityLimit()) {
					this.opcf44RPrintLimit = "(limit: $" + numberFormat(limit) + ")";
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF47")) {
				this.showOPCF47Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF48")) {
				this.showOPCF48Section = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON114")) {
				this.showEON114Section = true;
				numEnds.add(end);
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("EON114_NamedIns")
									&& extData.getColumnValue() != null) {
								if (eon114NameInsSB.length() > 0) {
									eon114NameInsSB.append(" and/or\n");
								}
								eon114NameInsSB.append(extData.getColumnValue());
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON101")) {
				this.showEON101Section = true;
				numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON102")) {
				this.showEON102Section = true;
				numEnds.add(end);
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("EON102_Sec7_Ded")
									&& extData.getColumnValue() != null) {
								if (extData.getColumnValue().startsWith("See")) {
									this.eon102_sec7_ded = extData.getColumnValue();
								} else {
									this.eon102_sec7_ded = "$" + extData.getColumnValue();
								}
							}

							if (extData.getColumnId().equalsIgnoreCase("EON102_DCPD_Ded")
									&& extData.getColumnValue() != null) {
								if (extData.getColumnValue().startsWith("See")) {
									this.eon102_ded = extData.getColumnValue();
								} else {
									this.eon102_ded = "$" + extData.getColumnValue();
								}
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
				this.showEON20GSection = true;
				numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("GDPC")) {
				this.showGDPCSection = true;
				numEnds.add(end);
				if (end.getDataExtension() != null) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
									&& extData.getColumnValue() != null) {
								if (gdpcDealerPlateNoSB.length() > 0) {
									gdpcDealerPlateNoSB.append("\n");
								}
								gdpcDealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
							} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
									&& extData.getColumnValue() != null) {
								if (gdpcExpDateSB.length() > 0) {
									gdpcExpDateSB.append("\n");
								}
								gdpcExpDateSB.append("Expiry Date: " + extData.getColumnValue());
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON109")) {
				this.showEON109Section = true;
				this.numEnds.add(end);
				this.eon109Ends.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON116")) {
				this.showEON116Section = true;
				numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON117")) {
				this.showEON117Section = true;
				numEnds.add(end);
			}
		}

		this.opcf2_name = opcf2NameSB.toString();
		this.opcf2_relationship = opcf2RelationSB.toString();
		this.eon114_name_ins = eon114NameInsSB.toString();
		this.gdpc_dealer_plate_no = gdpcDealerPlateNoSB.toString();
		this.gdpc_exp_date = gdpcExpDateSB.toString();

		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			LookupTableItem lookupBusinessDescItem = lookupConfig
					.getConfigLookup(ConfigConstants.LOOKUPTABLE_BusinessDescriptions).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getBusinessDescription()))
					.findFirst().orElse(null);
			this.businessType = lookupBusinessDescItem.getItemValue();
		}

		// Sub Policy CGL
		if (subPolicyCGL != null) {

			this.cgl_lb_limit = numberFormat(subPolicyCGL.getCglLimit());
			this.cgl_ded = numberFormat(subPolicyCGL.getCglDeductible());

			// Coverages
			for (Coverage cove : subPolicyCGL.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("EBE")) {
					this.ebeExists = true;
					this.emp_benefits = "Employee Benefits Extension";
					this.ebe_prem = premFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("TLL")) {
					this.tll_limit = numberFormat(cove.getLimit1());
					if (cove.getDeductible1() != null) {
						this.tll_ded = numberFormat(cove.getDeductible1());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("BIPD")) {
					this.cgl_ge_prem = premFormat(cove.getCoveragePremium().getNetPremiumChange());
					this.cglcgo_ge_prem_total = cove.getCoveragePremium().getNetPremiumChange();
				}
			}

			// Endorsements
			for (Endorsement end : subPolicyCGL.getCoverageDetail().getEndorsements()) {
				
				if(end.getEndorsementNumber() != null && end.getEndorsementNumber() > maxEndNo) {
					maxEndNo = end.getEndorsementNumber();
				}
				
				if (!isValidEndorsement(end)) {
					continue;
				}

				if (end.getEndorsementCd().equalsIgnoreCase("GECGL")) {
					this.showGECGLSection = true;
					numEnds.add(end);
					this.gecgl_prem = premFormat(end.getEndorsementPremium().getNetPremiumChange());
					List<String> vehDescs = new ArrayList<String>();
					List<String> serNumbers = new ArrayList<String>();
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("GEL_ModType")
										&& extData.getColumnValue() != null) {
									this.gecgl_mod_type = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("GEL_VehDesc")
										&& extData.getColumnValue() != null) {
									vehDescs.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("GEL_SerNum")
										&& extData.getColumnValue() != null) {
									serNumbers.add(extData.getColumnValue());
								}
							}
						}
					}
					for (int i = 0; i < vehDescs.size(); i++) {
						HashMap<String, String> vehicle = new HashMap<String, String>();
						vehicle.put("veh_desc", vehDescs.get(i));
						vehicle.put("ser_num", serNumbers.get(i));
						gecgl_vehicles.add(vehicle);
					}
					Endorsement eon110End = policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsementByCode("EON110");
					this.per_occurrence_ded = numberFormat(eon110End.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("GLEE")) {
					this.showGLEESection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("AIC")) {
					this.showAICSection = true;
					numEnds.add(end);
					this.aic_limit = numberFormat(end.getLimit1amount());
					this.aic_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("WOS")) {
					this.showWOSSection = true;
					numEnds.add(end);
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("WOS_CoName")
										&& extData.getColumnValue() != null) {
									this.wos_co_name = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("WOS_CoAddr")
										&& extData.getColumnValue() != null) {
									this.wos_co_addr = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AOCCGL")) {
					this.showAOCCGLSection = true;
					numEnds.add(end);
					this.aoccgl_prem = premFormat(subPolicyCGL.getSubPolicyPremium().getNetPremiumChange());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCCGL")) {
					this.showMOCCGLSection = true;
					numEnds.add(end);
					this.moccgl_prem = premFormat(end.getEndorsementPremium().getNetPremiumChange());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEL")) {
					this.showCDELSection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("ALOCCGL")) {
					this.showALOCCGLSection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("DLOCCGL")) {
					this.showDLOCCGLSection = true;
					numEnds.add(end);
				}
			}
			
			for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
				String transType = risk.getPolicyTransaction().getTransactionType();
				String txnType = risk.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

				if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
						&& risk.getRiskType().equalsIgnoreCase(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)) {
					PolicyLocation pl = (PolicyLocation) risk;
					Endorsement endALOCCGL = pl.getCoverageDetail().getEndorsementByCode("ALOCCGL");

					if (endALOCCGL != null) {
						HashMap<String, String> location = new HashMap<String, String>();
						location.put("street", pl.getLocationAddress().getAddressLine1());
						location.put("city", pl.getLocationAddress().getCity());
						location.put("prov_state", pl.getLocationAddress().getProvState());
						location.put("pc", pl.getLocationAddress().getPostalZip());
						aloccgl_locations.add(location);
					}
				}
			}
			
			for (CGLLocation cglLoc : subPolicyCGL.getCglLocations()) {
				if (cglLoc.getPolicyLocation() != null && cglLoc.getBusinessStatus() != null) {
					PolicyLocation pl = cglLoc.getPolicyLocation();
					String transType = pl.getPolicyTransaction().getTransactionType();
					String txnType = pl.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

					if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)) {
						if (cglLoc.getBusinessStatus().equalsIgnoreCase(Constants.BUSINESS_STATUS_DELETED)) {
							HashMap<String, String> location = new HashMap<String, String>();
							location.put("street", pl.getLocationAddress().getAddressLine1());
							location.put("city", pl.getLocationAddress().getCity());
							location.put("prov_state", pl.getLocationAddress().getProvState());
							location.put("pc", pl.getLocationAddress().getPostalZip());
							dloccgl_locations.add(location);
						}
					}
				}
			}
		}

		// Sub Policy CGO
		if (subPolicyCGO != null) {
			this.showCGODecSection = true;
			this.cargo_prem = premFormat(subPolicyCGO.getSubPolicyPremium().getNetPremiumChange());
			this.cargo = "Motor Truck Cargo";

			for (Coverage cove : subPolicyCGO.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("MTCC")) {
					this.cargo_ge_prem = premFormat(cove.getCoveragePremium().getNetPremiumChange());
					this.cglcgo_ge_prem_total = this.cglcgo_ge_prem_total + cove.getCoveragePremium().getNetPremiumChange();
					this.cargoLimit1Dec = "Incl.";
					this.cargoLimit3Dec = "Incl.";
					this.cargoLimit4Dec = "10,000";
					this.cargoLimit5Dec = numberFormat(cove.getLimit1());
					this.cargoLimit6Dec = numberFormat(cove.getLimit1());
					this.mtcc_limit = numberFormat(cove.getLimit1());
					this.mtcc_ded = numberFormat(cove.getDeductible1());
				} 
			}

			for (Endorsement end : subPolicyCGO.getCoverageDetail().getEndorsements()) {
				
				if(end.getEndorsementNumber() != null && end.getEndorsementNumber() > maxEndNo) {
					maxEndNo = end.getEndorsementNumber();
				}
				
				if (!isValidEndorsement(end)) {
					continue;
				}

				if (end.getEndorsementCd().equalsIgnoreCase("CPEE")) {
					this.showCPEESection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("AOCC")) {
					this.showAOCCSection = true;
					numEnds.add(end);
					this.aocc_prem = premFormat(subPolicyCGO.getSubPolicyPremium().getNetPremiumChange());
				} else if (end.getEndorsementCd().equalsIgnoreCase("MOCC")) {
					this.showMOCCSection = true;
					numEnds.add(end);
					this.mocc_prem = premFormat(subPolicyCGO.getSubPolicyPremium().getNetPremiumChange());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CDEC")) {
					this.showCDECSection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("GECGLMTC")) {
					this.showGECGLMTCSection = true;
					numEnds.add(end);
					List<String> vehDescs = new ArrayList<String>();
					List<String> serNumbers = new ArrayList<String>();
					this.cglcgo_ge_prem = premFormat(this.cglcgo_ge_prem_total);
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("GELC_ModType")
										&& extData.getColumnValue() != null) {
									this.gecglmtc_mod_type = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("GELC_VehDesc")
										&& extData.getColumnValue() != null) {
									vehDescs.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("GELC_SerNum")
										&& extData.getColumnValue() != null) {
									serNumbers.add(extData.getColumnValue());
								}
							}
						}
					}

					for (int i = 0; i < vehDescs.size(); i++) {
						HashMap<String, String> vehicle = new HashMap<String, String>();
						vehicle.put("veh_desc", vehDescs.get(i));
						vehicle.put("ser_num", serNumbers.get(i));
						gecglmtc_vehicles.add(vehicle);
					}
					Endorsement eon110End = policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsementByCode("EON110");
					this.per_occurrence_ded = numberFormat(eon110End.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("ALOCC")) {
					this.showALOCCSection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("DLOCC")) {
					this.showDLOCCSection = true;
					numEnds.add(end);
				} else if (end.getEndorsementCd().equalsIgnoreCase("TTC")) {
					this.showTTCSection = true;
					numEnds.add(end);
					this.ttc_limit = numberFormat(end.getLimit1amount());
					this.ttc_ded = numberFormat(end.getDeductible1amount());
					this.ttc_prem = premFormat(end.getEndorsementPremium().getNetPremiumChange());
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("TTC_VehDesc")
										&& extData.getColumnValue() != null) {
									this.ttc_vehdesc = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("TTC_SerNo")
										&& extData.getColumnValue() != null) {
									this.ttc_serno = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period1")
										&& extData.getColumnValue() != null) {
									this.ttc_period1 = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("TTC_Period2")
										&& extData.getColumnValue() != null) {
									this.ttc_period2 = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("AICC")) {
					this.showAICCSection = true;
					numEnds.add(end);
					this.aicc_limit = numberFormat(end.getLimit1amount());
					this.aicc_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("CTC")) {
					this.ctc_limit = numberFormat(end.getLimit1amount());
				}
			}
			
			for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
				String transType = risk.getPolicyTransaction().getTransactionType();
				String txnType = risk.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();

				if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE) && transType.equalsIgnoreCase(Constants.TRANS_TYPE_ONSET)
						&& risk.getRiskType().equalsIgnoreCase(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)) {
					PolicyLocation pl = (PolicyLocation) risk;
					Endorsement endALOCC = pl.getCoverageDetail().getEndorsementByCode("ALOCC");
					Endorsement endDLOCC = pl.getCoverageDetail().getEndorsementByCode("DLOCC");

					if (endALOCC != null) {
						HashMap<String, String> location = new HashMap<String, String>();
						location.put("street", pl.getLocationAddress().getAddressLine1());
						location.put("city", pl.getLocationAddress().getCity());
						location.put("prov_state", pl.getLocationAddress().getProvState());
						location.put("pc", pl.getLocationAddress().getPostalZip());
						alocc_locations.add(location);
					}

					if (endDLOCC != null) {
						HashMap<String, String> location = new HashMap<String, String>();
						location.put("street", pl.getLocationAddress().getAddressLine1());
						location.put("city", pl.getLocationAddress().getCity());
						location.put("prov_state", pl.getLocationAddress().getProvState());
						location.put("pc", pl.getLocationAddress().getPostalZip());
						dlocc_locations.add(location);
					}
				}
			}
		}

		// Sub Policy GAR
		if (subPolicyGAR != null) {

			// Coverages
			for (Coverage cove : subPolicyGAR.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("CL511")) {
					this.cl5_ded = numberFormat(cove.getDeductible1());
					this.cl511_premA = cove.getCoveragePremium().getNetPremiumChange() < 0 ? "$" : premFormat(cove.getCoveragePremium().getNetPremiumChange());
					this.cl511_premR = cove.getCoveragePremium().getNetPremiumChange() < 0 ? premFormat(cove.getCoveragePremium().getNetPremiumChange(), false) : "$";
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL61")) {
					this.cl6_lim = numberFormat(cove.getLimit1());
					this.cl6_ded = numberFormat(cove.getDeductible1());
				}
			}

			// Endorsements
			StringBuilder oef72EndCodeSB = new StringBuilder();
			StringBuilder oef76NameSB = new StringBuilder();
			StringBuilder oef76RelationshipSB = new StringBuilder();
			StringBuilder dealerPlateNoSB = new StringBuilder();
			StringBuilder dealerExpDateSB = new StringBuilder();
			StringBuilder servPlateNoSB = new StringBuilder();
			StringBuilder expDateSB = new StringBuilder();

			for (Endorsement end : subPolicyGAR.getCoverageDetail().getEndorsements()) {
				
				if(end.getEndorsementNumber() != null && end.getEndorsementNumber() > maxEndNo) {
					maxEndNo = end.getEndorsementNumber();
				}
				
				if (!isValidEndorsement(end)) {
					continue;
				}

				if (end.getEndorsementCd().equalsIgnoreCase("OEF71")) {
					this.showOEF71Section = true;
					this.oef71_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
					this.showOEF72Section = true;
					List<String> altNums = new ArrayList<String>();
					List<String> changeDescs = new ArrayList<String>();
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF72_AltNum")
										&& extData.getColumnValue() != null) {
									altNums.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("OEF72_ChangeDesc")
										&& extData.getColumnValue() != null) {
									changeDescs.add(extData.getColumnValue());
								}
							}
						}
					}
					this.oef72_prem = premFormat(subPolicyGAR.getSubPolicyPremium().getNetPremiumChange());
					this.oef72_net_premA = subPolicyGAR.getSubPolicyPremium().getNetPremiumChange() < 0 ? "$" : premFormat(subPolicyGAR.getSubPolicyPremium().getNetPremiumChange());
					this.oef72_net_premR = subPolicyGAR.getSubPolicyPremium().getNetPremiumChange() < 0 ? premFormat(subPolicyGAR.getSubPolicyPremium().getNetPremiumChange(), false) : "$";
					for(int i = 0; i < altNums.size(); i++) {
						HashMap<String, String> alteration = new HashMap<String, String>();
						alteration.put("altNum", altNums.get(i));
						alteration.put("changeDesc", changeDescs.get(i));
						this.oef72AlterationsTable.add(alteration);
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF73")) {
					this.showOEF73Section = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF73_OtParty")
										&& extData.getColumnValue() != null) {
									this.oef73_other_party = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF73_Address")
										&& extData.getColumnValue() != null) {
									this.oef73_other_party_address = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF74")) {
					this.showOEF74Section = true;
					this.oef74_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF75")) {
					this.showOEF75Section = true;
					this.oef75_ded = numberFormat(end.getDeductible1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF76")) {
					this.showOEF76Section = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF76_Name")
										&& extData.getColumnValue() != null) {
									if (oef76NameSB.length() > 0) {
										oef76NameSB.append("\n");
									}
									oef76NameSB.append(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("OEF76_Relation")
										&& extData.getColumnValue() != null) {
									if (oef76RelationshipSB.length() > 0) {
										oef76RelationshipSB.append("\n");
									}
									oef76RelationshipSB.append(extData.getColumnValue());
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF77")) {
					this.showOEF77Section = true;
					this.oef77_limit = "Incl.";
					this.oef77_ded = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF78")) {
					this.showOEF78Section = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF78_Pers")
										&& extData.getColumnValue() != null) {
									this.oef78_pers = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF78_TPL")
										&& extData.getColumnValue() != null) {
									this.oef78_insuredind1 = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF78_Coll5")
										&& extData.getColumnValue() != null) {
									this.oef78_insuredind2 = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF78_Coll6")
										&& extData.getColumnValue() != null) {
									this.oef78_insuredind3 = extData.getColumnValue();
								}
							}
						}
					}
					this.oef78_liab_limit = numberFormat(end.getLimit1amount());
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF78A")) {
					this.showOEF78ASection = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF78A_ExclDriv")
										&& extData.getColumnValue() != null) {
									this.oef78A_exclDriver = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF78A_LicNum")
										&& extData.getColumnValue() != null) {
									this.oef78A_driverLicNo = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF79")) {
					this.showOEF79Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF80")) {
					this.showOEF80Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF81")) {
					this.showOEF81Section = true;
					this.oef81_lim = numberFormat(end.getLimit1amount());
					this.oef81_premA = end.getEndorsementPremium().getNetPremiumChange() < 0 ? "$" : premFormat(end.getEndorsementPremium().getNetPremiumChange());
					this.oef81_premR = end.getEndorsementPremium().getNetPremiumChange() < 0 ? premFormat(end.getEndorsementPremium().getNetPremiumChange(), false) : "$";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF82")) {
					this.showOEF82Section = true;
					this.oef82_lim = numberFormat(end.getLimit1amount());
					List<String> names = new ArrayList<String>();
					List<String> relations = new ArrayList<String>();
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF82_Name")
										&& extData.getColumnValue() != null) {
									names.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("OEF82_Relation")
										&& extData.getColumnValue() != null) {
									relations.add(extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("511_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_511_ded = extData.getColumnValue();
									this.oef82_511_prem = "$Incl.";
								} else if (extData.getColumnId().equalsIgnoreCase("512_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_512_ded = extData.getColumnValue();
									this.oef82_512_prem = "$Incl.";
								} else if (extData.getColumnId().equalsIgnoreCase("513_Ded")
										&& extData.getColumnValue() != null) {
									this.oef82_513_ded = extData.getColumnValue();
									this.oef82_513_prem = "$Incl.";
								}
							}
						}
					}

					for (int i = 0; i < names.size(); i++) {
						HashMap<String, String> person = new HashMap<String, String>();
						person.put("number", Integer.toString(i + 1) + ".");
						person.put("name", names.get(i));
						person.put("relation", relations.get(i));
						oef82PersonsTable.add(person);
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF83")) {
					this.showOEF83Section = true;
					this.oef83_limit = "Incl.";
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OEF83_NumOfVeh")
										&& extData.getColumnValue() != null) {
									this.oef83_numOfVeh = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF83_Coll5")
										&& extData.getColumnValue() != null) {
									this.oef83_coll5 = extData.getColumnValue();
								} else if (extData.getColumnId().equalsIgnoreCase("OEF83_Coll6")
										&& extData.getColumnValue() != null) {
									this.oef83_coll6 = extData.getColumnValue();
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF86")) {
					this.showOEF86Section = true;
					this.oef86_limit = "Incl.";
				} else if (end.getEndorsementCd().equalsIgnoreCase("OEF87")) {
					this.showOEF87Section = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("GSPC")) {
					this.showGSPCSection = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("GSPC_PlateNo")
										&& extData.getColumnValue() != null) {
									if (servPlateNoSB.length() > 0) {
										servPlateNoSB.append("\n");
									}
									servPlateNoSB.append("Service Plate No.: " + extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("GSPC_ExpDate")
										&& extData.getColumnValue() != null) {
									if (expDateSB.length() > 0) {
										expDateSB.append("\n");
									}
									expDateSB.append("Expiry Date:  " + extData.getColumnValue());
								}
							}
						}
					}
				} else if (end.getEndorsementCd().equalsIgnoreCase("GDPCG")) {
					this.showGDPCGSection = true;
					if (end.getDataExtension() != null) {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("GDPC_PlateNo")
										&& extData.getColumnValue() != null) {
									if (dealerPlateNoSB.length() > 0) {
										dealerPlateNoSB.append("\n");
									}
									dealerPlateNoSB.append("Dealer Plate No.: " + extData.getColumnValue());
								} else if (extData.getColumnId().equalsIgnoreCase("GDPC_ExpDate")
										&& extData.getColumnValue() != null) {
									if (dealerExpDateSB.length() > 0) {
										dealerExpDateSB.append("\n");
									}
									dealerExpDateSB.append("Expiry Date:  " + extData.getColumnValue());
								}
							}
						}
					}
				}

				if (end.getEndorsementCd().startsWith("OEF") && !end.getEndorsementCd().equalsIgnoreCase("OEF81")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					if (!end.getEndorsementCd().equalsIgnoreCase("OEF72")) {
						if (oef72EndCodeSB.length() > 0) {
							oef72EndCodeSB.append(", ");
						}
						if (lookupEndorsementCodeItem != null) {
							oef72EndCodeSB.append(lookupEndorsementCodeItem.getItemValue());
						} else {
							oef72EndCodeSB.append(end.getEndorsementCd());
						}
					}
				}
			}

			Double cm512_prem_total = 0.0;
			Double sp513_prem_total = 0.0;
			Double sp514_prem_total = 0.0;
			Double sp64_prem_total = 0.0;

			for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
				if (risk.getRiskType().equalsIgnoreCase(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)) {
					PolicyLocation pl = (PolicyLocation) risk;
					Coverage coveCM512 = pl.getCoverageDetail().getCoverageByCode("CM512");
					Coverage coveSP513 = pl.getCoverageDetail().getCoverageByCode("SP513");
					Coverage coveSP514 = pl.getCoverageDetail().getCoverageByCode("SP514");
					Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
					
					if (coveCM512 != null) {
						cm512_prem_total = cm512_prem_total + coveCM512.getCoveragePremium().getNetPremiumChange();
					}
					if (coveSP513 != null) {
						sp513_prem_total = sp513_prem_total + coveSP513.getCoveragePremium().getNetPremiumChange();
					}
					if (coveSP514 != null) {
						sp514_prem_total = sp514_prem_total + coveSP514.getCoveragePremium().getNetPremiumChange();
					}
					if (coveSP64 != null) {
						sp64_prem_total = sp64_prem_total + coveSP64.getCoveragePremium().getNetPremiumChange();
					}

					if (pl.getLocationId().equalsIgnoreCase("A")) {
						if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
							if (coveSP64 != null) {
								this.maxnum_veha = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limita = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_deda = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_prema = premFormat(coveSP64.getCoveragePremium().getNetPremiumChange());
								this.oef77_oap4_sp_prema = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("B")) {
						if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
							if (coveSP64 != null) {
								this.maxnum_vehb = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitb = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedb = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premb = premFormat(coveSP64.getCoveragePremium().getNetPremiumChange());
								this.oef77_oap4_sp_premb = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("C")) {
						if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
							if (coveSP64 != null) {
								this.maxnum_vehc = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitc = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedc = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premc = premFormat(coveSP64.getCoveragePremium().getNetPremiumChange());
								this.oef77_oap4_sp_premc = "$Incl.";
							}
						}
					} else if (pl.getLocationId().equalsIgnoreCase("D")) {
						if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
							if (coveSP64 != null) {
								this.maxnum_vehd = numberFormat(pl.getNumVehicles());
								this.oap4_sp_limitd = "$" + numberFormat(coveSP64.getLimit1());
								this.oap4_sp_dedd = "$" + numberFormat(coveSP64.getDeductible1());
								this.oap4_sp_premd = premFormat(coveSP64.getCoveragePremium().getNetPremiumChange());
								this.oef77_oap4_sp_premd = "$Incl.";
							}
						}
					}
				}
			}

			this.cm512_premA = cm512_prem_total < 0 ? "$" : premFormat(cm512_prem_total);
			this.cm512_premR = cm512_prem_total < 0 ? premFormat(cm512_prem_total, false) : "$";
			
			this.sp513_premA = sp513_prem_total < 0 ? "$" : premFormat(sp513_prem_total);
			this.sp513_premR = sp513_prem_total < 0 ? premFormat(sp513_prem_total, false) : "$";
			this.sp514_premA = sp514_prem_total < 0 ? "$" : premFormat(sp514_prem_total);
			this.sp514_premR = sp514_prem_total < 0 ? premFormat(sp514_prem_total, false) : "$";
			this.sp64_premA = sp64_prem_total < 0 ? "$" : premFormat(sp64_prem_total);
			this.sp64_premR = sp64_prem_total < 0 ? premFormat(sp64_prem_total, false) : "$";
			this.oef72_end_codes = oef72EndCodeSB.toString();
			this.oef76_insured_name = oef76NameSB.toString();
			this.oef76_insured_relationship = oef76RelationshipSB.toString();
			this.gdpc_servPlateNo = dealerPlateNoSB.toString();
			this.gdpc_expDate = dealerExpDateSB.toString();
			this.gspc_servPlateNo = servPlateNoSB.toString();
			this.gspc_expDate = expDateSB.toString();
		}
	}
	
	@Override
	public String getTemplatePath() {
		return "templates/PolicyChange_tmp.docx";
	}
	
	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if (attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);
		
		if (this.showOPCF5Section) {
			setOPCF5LessorTable(doc);
		}
		
		if (this.opcf20_exists) {
			setOPCF20Section();
		}

		if (this.showOPCF19Section) {
			setOPCF19Section(doc);
		}

		if (this.showOPCF21BSection) {
			setOPCF21BFirstTable(doc);
			setOPCF21BSecondTable(doc);
		}

		if (this.showOPCF23ASection) {
			setOPCF23ASecondTable(doc);
		}

		if (this.showOPCF27Section) {
			setOPCF27Table(doc);
		}

		if (this.showOPCF27BSection) {
			setOPCF27BSection(doc);
		}

		if (this.showOPCF28ASection) {
			setOPCF28ASection(doc);
		}

		if (this.showOPCF31Section) {
			setOPCF31Section(doc);
		}

		if (this.showOPCF38Section) {
			setOPCF38Section(doc);
		}
		
		if (this.showOEF72Section) {
			setOEF72Table(doc);
		}

		if (this.showOEF82Section) {
			setOEF82FirstTable(doc);
		}

		if (this.showEON102Section) {
			setEON102Section(doc);
		}
		
		if (this.showDOCCSection) {
			setDOCCSection(doc);
		}
		
		if (this.showGECGLMTCSection) {
			setGECGLMTCVehicles(doc);
		}
		
		if (this.showGECGLSection) {
			setGECGLVehicles(doc);
		}
		
		if (this.showALOCCGLSection) {
			setALOCCGLSection(doc);
		}
		
		if (this.showDLOCCGLSection) {
			setDLOCCGLSection(doc);
		}
		
		if (this.showALOCCSection) {
			setALOCCSection(doc);
		}
		
		if (this.showDLOCCSection) {
			setDLOCCSection(doc);
		}

		if (this.showAICSection) {
			setAICSection(doc);
		}

		if (this.showAICCSection) {
			setAICCSection(doc);
		}
		
		if (this.showEON109Section) {
			setEON109Section(doc);
		}

		hideSections(doc);
		endorsementNumeration(doc);
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}

	public void initializeFields() {
		fieldNames = new String[] { "BrokerName", "MasterBrokerNumber", "Insured_Name", "Pol_No", "Effective_Date",
				"Expiry_Date", "Insured_Address_Street", "Insured_Address_City", "Insured_Address_ProvState",
				"Insured_Address_PC", "ExpYear", "ExpMonth", "ExpDay", "OPCF2_Name", "OPCF2_Relationship", "LB_Limit",
				"OPCF8_Ded", "OPCF20_Limit", "OPCF20H_Limit", "OPCF21B_Prorata", "OPCF21B_5050", "OPCF27_APDed",
				"OPCF27_Limit", "OPCF27B_APDed", "BusinessType", "OPCF28_Pers", "OPCF28_Limit", "OPCF28CL_Ded",
				"OPCF28AP_Ded", "OPCF28_Coll", "OPCF28_CollNot", "OPCF28_AllPeril", "OPCF28_AllPerilNot",
				"EON114_Name_Ins", "EON102_Ded", "EON102_Sec7_Ded", "CGLCGO_GE_Prem", "GECGLMTC_Mod_Type",
				"GECGL_Mod_Type", "GECGL_Prem", "GDPC_Dealer_PlateNo", "GDPC_Exp_Date", "AIC_Limit", "AIC_Ded",
				"EON111_Effective_Date", "EON111_Expiry_Date", "Per_Occurrence_Ded", "WOS_Co_Name", "WOS_Co_Addr",
				"AOCCGL_Prem", "MOCCGL_Prem", "AOCC_Prem", "MOCC_Prem", "MTCC_Limit", "MTCC_Ded", "TLL_Limit",
				"TLL_Ded", "CGL_LB_Limit", "CGL_Ded", "CGL_GE_Prem", "Cargo_GE_Prem", "OEF71_Limit", "OEF71_Ded",
				"OEF72_Prem", "CL511_PremA", "CL511_PremR", "CM512_PremA", "CM512_PremR", "SP513_PremA", "SP513_PremR",
				"SP514_PremA", "SP514_PremR", "SP64_PremA", "SP64_PremR", "OEF72_End_Codes", "OEF72_End_PremA",
				"OEF72_End_PremR", "OEF72_Net_PremA", "OEF72_Net_PremR", "OEF73_Other_Party",
				"OEF73_Other_Party_Address", "OEF81_Lim", "OEF81_PremA", "OEF81_PremR", "OEF74_Ded", "OEF75_Ded",
				"OEF76_Insured_Name", "OEF76_Insured_Relationship", "OEF77_Limit", "OEF77_Ded", "OEF77_OAP4_SP_PremA",
				"OEF77_OAP4_SP_PremB", "OEF77_OAP4_SP_PremC", "OEF77_OAP4_SP_PremD", "OEF78_Person", "OEF78_Liab_Limit",
				"OEF78_InsuredInd1", "OEF78_InsuredInd2", "OEF78_InsuredInd3", "OEF78A_Excl_Driver",
				"OEF78A_Driver_LicNo", "CL5_Ded", "CL6_Lim", "CL6_Ded", "MaxNum_VehA", "OAP4_SP_LimitA", "OAP4_SP_DedA",
				"OAP4_SP_PremA", "MaxNum_VehB", "OAP4_SP_LimitB", "OAP4_SP_DedB", "OAP4_SP_PremB", "MaxNum_VehC",
				"OAP4_SP_LimitC", "OAP4_SP_DedC", "OAP4_SP_PremC", "MaxNum_VehD", "OAP4_SP_LimitD", "OAP4_SP_DedD",
				"OAP4_SP_PremD", "OEF82511_Ded", "OEF82511_Prem", "OEF82512_Ded", "OEF82512_Prem", "OEF82513_Ded",
				"OEF82513_Prem", "OEF82_Lim", "OEF83_Limit", "OEF83_Ded", "OEF83_NumOfVeh", "OEF83_Coll5",
				"OEF83_Coll6", "OEF86_Limit", "GDPC_ServPlateNo", "GDPC_ExpDate", "GSPC_ServPlateNo", "GSPC_ExpDate",
				"TTC_VehDesc", "TTC_SerNo", "TTC_Period1", "TTC_Period2", "TTC_Limit", "TTC_Ded", "TTC_Prem",
				"AICC_Limit", "AICC_Ded", "OPCF23A_DCPD_CK", "OPCF23A_DCPD_Ded", "OPCF23A_SP_CK", "OPCF23A_SP_Ded",
				"OPCF23A_CL_CK", "OPCF23A_CL_Ded", "OPCF23A_CM_CK", "OPCF23A_CM_Ded", "OPCF23A_AP_CK", "OPCF23A_AP_Ded",
				"OPCF23ABL_DCPD_CK", "OPCF23ABL_DCPD_Ded", "OPCF23ABL_SP_CK", "OPCF23ABL_SP_Ded", "OPCF23ABL_CL_CK",
				"OPCF23ABL_CL_Ded", "OPCF23ABL_CM_CK", "OPCF23ABL_CM_Ded", "OPCF23ABL_AP_CK", "OPCF23ABL_AP_Ded",
				"CTC_Limit", "Cargo_Limit1_Dec", "Cargo_Limit3_Dec", "Cargo_Limit4_Dec", "Cargo_Limit5_Dec",
				"Cargo_Limit6_Dec", "OPCF25AA_UnitYear", "OPCF25AA_UnitMake", "OPCF25AA_UnitSerialNo",
				"OPCF25AA_AddAuto", "OPCF25AA_AddAutoNo", "OPCF25AA_DelAuto", "OPCF25AA_DelAutoNo", "OPCF25AA_Prem",
				"OPCF25AA_PremA", "OPCF25AA_PremR", "OPCF25AA_BIPrem_A", "OPCF25AA_BIPrem_R", "OPCF25AA_LiabLimit",
				"OPCF25AA_AddCov", "OPCF25AA_AddCovVeh", "OPCF25AA_DelCov", "OPCF25AA_DelCovVeh", "OPCF25AA_PDPrem_A",
				"OPCF25AA_PDPrem_R", "OPCF25AA_SP_A", "OPCF25AA_SP_R", "OPCF25AA_CM_A", "OPCF25AA_CM_R",
				"OPCF25AA_CL_A", "OPCF25AA_CL_R", "OPCF25AA_AP_A", "OPCF25AA_AP_R", "OPCF25AA_A", "OPCF25AA_R",
				"25AA_SPDed", "25AA_CMDed", "25AA_CLDed", "25AA_APDed", "OPCF25AA_ABPrem_A", "OPCF25AA_ABPrem_R",
				"OPCF25AA_IRPrem_A", "OPCF25AA_IRPrem_R", "OPCF25AA_IRLim", "OPCF25AA_MRCPrem_A", "OPCF25AA_MRCPrem_R",
				"OPCF25AA_OCIPrem_A", "OPCF25AA_OCIPrem_R", "OPCF25AA_CHMPrem_A", "OPCF25AA_CHMPrem_R",
				"OPCF25AA_DFPrem_A", "OPCF25AA_DFPrem_R", "OPCF25AA_DCPrem_A", "OPCF25AA_DCPrem_R", "OPCF25AA_IBPrem_A",
				"OPCF25AA_IBPrem_R", "OPCF25AA_UAPrem_A", "OPCF25AA_UAPrem_R", "OPCF25AA_DCPD_A", "OPCF25AA_DCPD_R",
				"OPCF25AA_DCPDDed", "OPCF25AA_EndList", "OPCF25AA_EndList_A", "OPCF25AA_EndList_R", "OPCF25AA_ChgAuto",
				"OPCF25AA_ChgAutoVeh", "OPCF25AA_ChgOB", "OPCF25AA_ChgOBVeh", "OPCF25AA_Oth", "OPCF25AA_Oth_desc",
				"OPCF25AA_NameAddr" };
		fieldValues = new Object[] { brokerName, master_broker_no, insuredName, polNo, effectiveDate, expiryDate,
				insuredAddressStreet, insuredAddressCity, insuredAddressProvState, insuredAddressPC, expYear, expMonth,
				expDay, opcf2_name, opcf2_relationship, lbLimit, opcf8_ded, opcf20_limit, opcf20H_limit,
				opcf21B_prorata, opcf21B_5050, opcf27_apded, opcf27_limit, opcf27B_apded, businessType, opcf28_pers,
				opcf28_limit, opcf28CL_ded, opcf28AP_ded, opcf28_coll, opcf28_collNot, opcf28_allperil,
				opcf28_allperilNot, eon114_name_ins, eon102_ded, eon102_sec7_ded, cglcgo_ge_prem, gecglmtc_mod_type,
				gecgl_mod_type, gecgl_prem, gdpc_dealer_plate_no, gdpc_exp_date, aic_limit, aic_ded,
				eon111_effective_date, eon111_expiry_date, per_occurrence_ded, wos_co_name, wos_co_addr, aoccgl_prem,
				moccgl_prem, aocc_prem, mocc_prem, mtcc_limit, mtcc_ded, tll_limit, tll_ded, cgl_lb_limit, cgl_ded,
				cgl_ge_prem, cargo_ge_prem, oef71_limit, oef71_ded, oef72_prem, cl511_premA, cl511_premR, cm512_premA,
				cm512_premR, sp513_premA, sp513_premR, sp514_premA, sp514_premR, sp64_premA, sp64_premR,
				oef72_end_codes, oef72_end_premA, oef72_end_premR, oef72_net_premA, oef72_net_premR, oef73_other_party,
				oef73_other_party_address, oef81_lim, oef81_premA, oef81_premR, oef74_ded, oef75_ded,
				oef76_insured_name, oef76_insured_relationship, oef77_limit, oef77_ded, oef77_oap4_sp_prema,
				oef77_oap4_sp_premb, oef77_oap4_sp_premc, oef77_oap4_sp_premd, oef78_pers, oef78_liab_limit,
				oef78_insuredind1, oef78_insuredind2, oef78_insuredind3, oef78A_exclDriver, oef78A_driverLicNo, cl5_ded,
				cl6_lim, cl6_ded, maxnum_veha, oap4_sp_limita, oap4_sp_deda, oap4_sp_prema, maxnum_vehb, oap4_sp_limitb,
				oap4_sp_dedb, oap4_sp_premb, maxnum_vehc, oap4_sp_limitc, oap4_sp_dedc, oap4_sp_premc, maxnum_vehd,
				oap4_sp_limitd, oap4_sp_dedd, oap4_sp_premd, oef82_511_ded, oef82_511_prem, oef82_512_ded,
				oef82_512_prem, oef82_513_ded, oef82_513_prem, oef82_lim, oef83_limit, oef83_ded, oef83_numOfVeh,
				oef83_coll5, oef83_coll6, oef86_limit, gdpc_servPlateNo, gdpc_expDate, gspc_servPlateNo, gspc_expDate,
				ttc_vehdesc, ttc_serno, ttc_period1, ttc_period2, ttc_limit, ttc_ded, ttc_prem, aicc_limit, aicc_ded,
				opcf23A_dcpd_ck, opcf23A_dcpd_ded, opcf23A_sp_ck, opcf23A_sp_ded, opcf23A_cl_ck, opcf23A_cl_ded,
				opcf23A_cm_ck, opcf23A_cm_ded, opcf23A_ap_ck, opcf23A_ap_ded, opcf23ABL_dcpd_ck, opcf23ABL_dcpd_ded,
				opcf23ABL_sp_ck, opcf23ABL_sp_ded, opcf23ABL_cl_ck, opcf23ABL_cl_ded, opcf23ABL_cm_ck, opcf23ABL_cm_ded,
				opcf23ABL_ap_ck, opcf23ABL_ap_ded, ctc_limit, cargoLimit1Dec, cargoLimit3Dec, cargoLimit4Dec,
				cargoLimit5Dec, cargoLimit6Dec, opcf25AA_unitYear, opcf25AA_unitMake, opcf25AA_unitSerialNo,
				opcf25AA_addAuto, opcf25AA_addAutoNo, opcf25AA_delAuto, opcf25AA_delAutoNo, opcf25AA_prem,
				opcf25AA_prem_a, opcf25AA_prem_r, opcf25AA_biprem_a, opcf25AA_biprem_r, opcf25AA_liabLimit,
				opcf25AA_addCov, opcf25AA_addCovVeh, opcf25AA_delCov, opcf25AA_delCovVeh, opcf25AA_pd_a, opcf25AA_pd_r,
				opcf25AA_sp_a, opcf25AA_sp_r, opcf25AA_cm_a, opcf25AA_cm_r, opcf25AA_cl_a, opcf25AA_cl_r, opcf25AA_ap_a,
				opcf25AA_ap_r, opcf25AA_a, opcf25AA_r, opcf25AA_spDed, opcf25AA_cmDed, opcf25AA_clDed, opcf25AA_apDed,
				opcf25AA_abPrem_a, opcf25AA_abPrem_r, opcf25AA_irPrem_a, opcf25AA_irPrem_r, opcf25AA_irLim,
				opcf25AA_mrcPrem_a, opcf25AA_mrcPrem_r, opcf25AA_ociPrem_a, opcf25AA_ociPrem_r, opcf25AA_chmPrem_a,
				opcf25AA_chmPrem_r, opcf25AA_dfPrem_a, opcf25AA_dfPrem_r, opcf25AA_dcPrem_a, opcf25AA_dcPrem_r,
				opcf25AA_ibPrem_a, opcf25AA_ibPrem_r, opcf25AA_uaPrem_a, opcf25AA_uaPrem_r, opcf25AA_dcpdPrem_a,
				opcf25AA_dcpdPrem_r, opcf25AA_dcpdDed, opcf25AA_endList, opcf25AA_endList_a, opcf25AA_endList_r,
				opcf25AA_chgAuto, opcf25AA_chgAutoVeh, opcf25AA_chgOB, opcf25AA_chgOBVeh, opcf25AA_oth,
				opcf25AA_othDesc, opcf25AA_nameAddr };
	}
	
	protected void setOPCF20Section() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20Section = true;
					this.opcf20_limit = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("MTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("LTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("HTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TT")) {
					this.showOPCF20HSection = true;
					this.opcf20H_limit = numberFormat(end.getLimit1amount());
				}
			}
		}
	}
	
	private void initializeSections(Document doc) {
		opcf25AASec = doc.getSections().get(0);
		opcf2Sec = doc.getSections().get(1);
		opcf5Sec = doc.getSections().get(2);
		opcf5BlSec = doc.getSections().get(3);
		opcf8Sec = doc.getSections().get(4);
		opcf9Sec = doc.getSections().get(5);
		opcf13CSec = doc.getSections().get(6);
		opcf19Sec = doc.getSections().get(7);
		opcf20Sec = doc.getSections().get(8);
		opcf20HSec = doc.getSections().get(9);
		opcf21ASec = doc.getSections().get(10);
		opcf21BSec = doc.getSections().get(11);
		opcf23ASec = doc.getSections().get(12);
		opcf23ABlSec = doc.getSections().get(13);
		opcf27Sec = doc.getSections().get(14);
		opcf27BSec = doc.getSections().get(15);
		opcf28Sec = doc.getSections().get(16);
		opcf28ASec = doc.getSections().get(17);
		opcf30Sec = doc.getSections().get(18);
		opcf31Sec = doc.getSections().get(19);
		opcf38Sec = doc.getSections().get(20);
		opcf40Sec = doc.getSections().get(21);
		opcf43Sec = doc.getSections().get(22);
		opcf43ASec = doc.getSections().get(23);
		opcf44RSec = doc.getSections().get(24);
		opcf47Sec = doc.getSections().get(25);
		opcf48Sec = doc.getSections().get(26);
		oef71Sec = doc.getSections().get(27);
		oef72Sec = doc.getSections().get(28);
		oef73Sec = doc.getSections().get(29);
		oef74Sec = doc.getSections().get(30);
		oef75Sec = doc.getSections().get(31);
		oef76Sec = doc.getSections().get(32);
		oef77Sec = doc.getSections().get(33);
		oef78Sec = doc.getSections().get(34);
		oef78ASec = doc.getSections().get(35);
		oef79Sec = doc.getSections().get(36);
		oef80Sec = doc.getSections().get(37);
		oef81Sec = doc.getSections().get(38);
		oef82Sec = doc.getSections().get(39);
		oef83Sec = doc.getSections().get(40);
		oef86Sec = doc.getSections().get(41);
		oef87Sec = doc.getSections().get(42);
		gdpcgSec = doc.getSections().get(43);
		gspcSec = doc.getSections().get(44);
		eon114Sec = doc.getSections().get(45);
		eon101Sec = doc.getSections().get(46);
		eon102Sec = doc.getSections().get(47);
		gecglmtcSec = doc.getSections().get(48);
		gecglSec = doc.getSections().get(49);
		eon20GSec = doc.getSections().get(50);
		gdpcSec = doc.getSections().get(51);
		gleeSec = doc.getSections().get(52);
		aicSec = doc.getSections().get(53);
		aiccSec = doc.getSections().get(54);
		eon109Sec = doc.getSections().get(55);
		eon110Sec = doc.getSections().get(56);
		eon111Sec = doc.getSections().get(57);
		eon112Sec = doc.getSections().get(58);
		eon121Sec = doc.getSections().get(59);
		doccSec = doc.getSections().get(60);
		wosSec = doc.getSections().get(61);
		aoccglSec = doc.getSections().get(62);
		moccglSec = doc.getSections().get(63);
		aloccglSec = doc.getSections().get(64);
		dloccglSec = doc.getSections().get(65);
		cdelSec = doc.getSections().get(66);
		aoccSec = doc.getSections().get(67);
		moccSec = doc.getSections().get(68);
		aloccSec = doc.getSections().get(69);
		dloccSec = doc.getSections().get(70);
		cpeeSec = doc.getSections().get(71);
		cdecSec = doc.getSections().get(72);
		ttcSec = doc.getSections().get(73);
		eon116Sec = doc.getSections().get(74);
		eon117Sec = doc.getSections().get(75);
		iaSec = doc.getSections().get(76);
	}

	private void hideSections(Document doc) {
		if (!showOPCF25AASection) {
			doc.removeChild(opcf25AASec);
		}

		if (!showOPCF2Section) {
			doc.removeChild(opcf2Sec);
		}

		if (!showOPCF5Section) {
			doc.removeChild(opcf5Sec);
		}

		if (!showOPCF5BlSection) {
			doc.removeChild(opcf5BlSec);
		}

		if (!showOPCF8Section) {
			doc.removeChild(opcf8Sec);
		}

		if (!showOPCF9Section) {
			doc.removeChild(opcf9Sec);
		}

		if (!showOPCF13CSection) {
			doc.removeChild(opcf13CSec);
		}

		if (!showOPCF19Section) {
			doc.removeChild(opcf19Sec);
		}

		if (!showOPCF20Section) {
			doc.removeChild(opcf20Sec);
		}
		
		if (!showOPCF20HSection) {
			doc.removeChild(opcf20HSec);
		}

		if (!showOPCF21ASection) {
			doc.removeChild(opcf21ASec);
		}

		if (!showOPCF21BSection) {
			doc.removeChild(opcf21BSec);
		}

		if (!showOPCF23ASection) {
			doc.removeChild(opcf23ASec);
		}

		if (!showOPCF23ABlSection) {
			doc.removeChild(opcf23ABlSec);
		}

		if (!showOPCF27Section) {
			doc.removeChild(opcf27Sec);
		}

		if (!showOPCF27BSection) {
			doc.removeChild(opcf27BSec);
		}

		if (!showOPCF28Section) {
			doc.removeChild(opcf28Sec);
		}

		if (!showOPCF28ASection) {
			doc.removeChild(opcf28ASec);
		}

		if (!showOPCF30Section) {
			doc.removeChild(opcf30Sec);
		}

		if (!showOPCF31Section) {
			doc.removeChild(opcf31Sec);
		}

		if (!showOPCF38Section) {
			doc.removeChild(opcf38Sec);
		}

		if (!showOPCF40Section) {
			doc.removeChild(opcf40Sec);
		}

		if (!showOPCF43Section) {
			doc.removeChild(opcf43Sec);
		}

		if (!showOPCF43ASection) {
			doc.removeChild(opcf43ASec);
		}

		if (!showOPCF44RSection) {
			doc.removeChild(opcf44RSec);
		}

		if (!showOPCF47Section) {
			doc.removeChild(opcf47Sec);
		}

		if (!showOPCF48Section) {
			doc.removeChild(opcf48Sec);
		}

		if (!showOEF71Section) {
			doc.removeChild(oef71Sec);
		}

		if (!showOEF72Section) {
			doc.removeChild(oef72Sec);
		}

		if (!showOEF73Section) {
			doc.removeChild(oef73Sec);
		}

		if (!showOEF74Section) {
			doc.removeChild(oef74Sec);
		}

		if (!showOEF75Section) {
			doc.removeChild(oef75Sec);
		}

		if (!showOEF76Section) {
			doc.removeChild(oef76Sec);
		}

		if (!showOEF77Section) {
			doc.removeChild(oef77Sec);
		}

		if (!showOEF78Section) {
			doc.removeChild(oef78Sec);
		}

		if (!showOEF78ASection) {
			doc.removeChild(oef78ASec);
		}

		if (!showOEF79Section) {
			doc.removeChild(oef79Sec);
		}

		if (!showOEF80Section) {
			doc.removeChild(oef80Sec);
		}

		if (!showOEF81Section) {
			doc.removeChild(oef81Sec);
		}

		if (!showOEF82Section) {
			doc.removeChild(oef82Sec);
		}

		if (!showOEF83Section) {
			doc.removeChild(oef83Sec);
		}

		if (!showOEF86Section) {
			doc.removeChild(oef86Sec);
		}

		if (!showOEF87Section) {
			doc.removeChild(oef87Sec);
		}

		if (!showGDPCGSection) {
			doc.removeChild(gdpcgSec);
		}

		if (!showGSPCSection) {
			doc.removeChild(gspcSec);
		}

		if (!showEON114Section) {
			doc.removeChild(eon114Sec);
		}

		if (!showEON101Section) {
			doc.removeChild(eon101Sec);
		}

		if (!showEON102Section) {
			doc.removeChild(eon102Sec);
		}

		if (!showGECGLMTCSection) {
			doc.removeChild(gecglmtcSec);
		}

		if (!showGECGLSection) {
			doc.removeChild(gecglSec);
		}

		if (!showEON20GSection) {
			doc.removeChild(eon20GSec);
		}

		if (!showGDPCSection) {
			doc.removeChild(gdpcSec);
		}

		if (!showGLEESection) {
			doc.removeChild(gleeSec);
		}

		if (!showAICSection) {
			doc.removeChild(aicSec);
		}

		if (!showAICCSection) {
			doc.removeChild(aiccSec);
		}

		if (!showEON109Section) {
			doc.removeChild(eon109Sec);
		}

		if (!showEON110Section) {
			doc.removeChild(eon110Sec);
		}

		if (!showEON111Section) {
			doc.removeChild(eon111Sec);
		}

		if (!showEON112Section) {
			doc.removeChild(eon112Sec);
		}

		if (!showEON121Section) {
			doc.removeChild(eon121Sec);
		}

		if (!showDOCCSection) {
			doc.removeChild(doccSec);
		}

		if (!showWOSSection) {
			doc.removeChild(wosSec);
		}

		if (!showAOCCGLSection) {
			doc.removeChild(aoccglSec);
		}

		if (!showMOCCGLSection) {
			doc.removeChild(moccglSec);
		}

		if (!showALOCCGLSection) {
			doc.removeChild(aloccglSec);
		}

		if (!showDLOCCGLSection) {
			doc.removeChild(dloccglSec);
		}

		if (!showCDELSection) {
			doc.removeChild(cdelSec);
		}

		if (!showAOCCSection) {
			doc.removeChild(aoccSec);
		}

		if (!showMOCCSection) {
			doc.removeChild(moccSec);
		}

		if (!showALOCCSection) {
			doc.removeChild(aloccSec);
		}

		if (!showDLOCCSection) {
			doc.removeChild(dloccSec);
		}

		if (!showTTCSection) {
			doc.removeChild(ttcSec);
		}

		if (!showCPEESection) {
			doc.removeChild(cpeeSec);
		}

		if (!showCDECSection) {
			doc.removeChild(cdecSec);
		}

		if (!showEON116Section) {
			doc.removeChild(eon116Sec);
		}

		if (!showEON117Section) {
			doc.removeChild(eon117Sec);
		}

		if (!showIASection) {
			doc.removeChild(iaSec);
		}
	}

}
