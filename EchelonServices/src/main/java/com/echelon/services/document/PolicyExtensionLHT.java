package com.echelon.services.document;

import com.ds.ins.domain.policy.PolicyDocuments;

public class PolicyExtensionLHT extends PolicyExtension {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8643366081509968823L;

	public PolicyExtensionLHT(PolicyDocuments policyDocument) throws Exception {
		super(policyDocument);
	}
	
	@Override
	public String getTemplatePath() {
		return "templates/PolicyExtensionLHT_tmp.docx";
	} 

}
