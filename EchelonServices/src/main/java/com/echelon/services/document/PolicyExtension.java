package com.echelon.services.document;

import java.io.File;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyExtension extends EchelonDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7986450124932823556L;
	private String[] fieldNames;
	private Object[] fieldValues;
	private String brokerName = "";
	private String master_broker_no = "";
	private String insuredName = "";
	private String polNo = "";
	private String effectiveDate = "";
	private String extEnd_prem = "";
	private String extEnd_days = "";
	private String extEnd_date = "";
	private Section extSec = null;
	private boolean showExtSection = false;

	public PolicyExtension(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		String txnType = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyTxnType();
		String businessStatus = policyDocument.getPolicyTransaction().getPolicyVersion().getBusinessStatus();
		PolicyTerm policyTerm = policyDocument.getPolicyTransaction().getPolicyTerm();

		this.policyDocument = policyDocument;
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.effectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(),
				"MM/dd/yyyy");
		this.extEnd_prem = premFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getNetPremiumChange());
		this.extEnd_days = numberFormat(policyTerm.getTermDays());
		this.extEnd_date = formatDate(policyTerm.getTermExpDate().toLocalDate(), "MM/dd/yyyy");
		if (txnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_EXTENSION)
				&& (businessStatus.equalsIgnoreCase(Constants.VERSION_STATUS_PENDINGEXTENSTION)
						|| businessStatus.equalsIgnoreCase(Constants.VERSION_STATUS_ISSUEDEXTENSTION))) {
			showExtSection = true;
		}
	}
	
	public String getTemplatePath() {
		return "templates/PolicyExtension_tmp.docx";
	}

	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if (attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);

		hideSections(doc);
		endorsementNumeration(doc);
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}

	public void initializeFields() {
		fieldNames = new String[] { "BrokerName", "MasterBrokerNumber", "Insured_Name", "Pol_No", "Effective_Date",
				"ExtEnd_Prem", "ExtEnd_days", "ExtEnd_date" };
		fieldValues = new Object[] { brokerName, master_broker_no, insuredName, polNo, effectiveDate, extEnd_prem,
				extEnd_days, extEnd_date };
	}

	private void initializeSections(Document doc) {
		extSec = doc.getSections().get(0);
	}

	private void hideSections(Document doc) {
		if (!showExtSection) {
			doc.removeChild(extSec);
		}
	}
	
	private void endorsementNumeration(Document doc) {
		Integer sectionNo = getMaxEndorsementNumber();
		for (Section sec : doc.getSections()) {
			boolean isEndSection = false;
			if (sec.getBody().getTables() != null && sec.getBody().getTables().get(0) != null
					&& sec.getBody().getTables().get(0).getRows() != null
					&& sec.getBody().getTables().get(0).getRows().get(1) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4) != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph() != null
					&& sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText() != null
					&& !sec.getBody().getTables().get(0).getRows().get(1).getCells().get(4).getFirstParagraph().getText().replaceAll("[^a-zA-Z0-9]", "").isEmpty()) {
				isEndSection = true;
			}

			if (isEndSection) {
				sectionNo++;
				Table table = sec.getBody().getTables().get(0);
				Row row = table.getRows().get(1);
				Cell cell = row.getCells().get(4);
				String endCode = cell.getFirstParagraph().getText();
				endCode = endCode.replaceAll("[^a-zA-Z0-9]", ""); 

				cell.getFirstParagraph().removeAllChildren();
				Run run = new Run(doc, sectionNo.toString());
				run.getFont().setName("Arial");
				run.getFont().setSize(8);
				run.getFont().setBold(true);
				cell.getFirstParagraph().appendChild(run);
			}
		}
		
		/*for (Endorsement end : numEnds) {
			System.out.println(end.getEndorsementCd() + " = " + end.getEndorsementNumber());
		}*/
	}
	
	private Integer getMaxEndorsementNumber() {
		
		Integer rv = null;

		Set<Integer> endNumbers = new HashSet<Integer>();
		SubPolicy<?> autoSP = policyDocument.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null) {
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
					endNumbers.add(end.getEndorsementNumber());
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementNumber() != null && !end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				endNumbers.add(end.getEndorsementNumber());
			}
		}
		rv = (!endNumbers.isEmpty() ? Collections.max(endNumbers) : 0);
		return rv;
	}

}
