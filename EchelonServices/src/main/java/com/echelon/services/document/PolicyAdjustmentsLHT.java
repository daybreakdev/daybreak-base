package com.echelon.services.document;

import java.io.File;

import com.aspose.words.Document;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;

public class PolicyAdjustmentsLHT extends PolicyAdjustments {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2602132646693916794L;
	
	public PolicyAdjustmentsLHT(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		this.policyDocument = policyDocument;
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.effectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(),
				"MM/dd/yyyy");
		this.expiryDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "MM/dd/yyyy");
		this.polEffectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermEffDate().toLocalDate(), "MM/dd/yyyy");
		this.adjEffectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(), "MM/dd/yyyy");

		// Sub Policy Auto
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {

			if (!isValidEndorsement(end)) {
				continue;
			}

			if (end.getEndorsementCd().equalsIgnoreCase("EON116")) {
				this.showEON116Section = true;
				this.numEnds.add(end);
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON117")) {
				this.showEON117Section = true;
				this.numEnds.add(end);
			}
		}
	}
	
	public PolicyAdjustmentsLHT(PolicyDocuments policyDocument, SpecialtyPolicyDTO policyDTO) throws Exception {
		this(policyDocument);
		this.policyDTO = policyDTO;
	}

	@Override
	public String getTemplatePath() {
		return "templates/PolicyAdjustmentsLHT_tmp.docx";
	}

	@Override
	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if (attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);

		if (this.showEON116Section) {
			setEON116Table(doc);
		}

		if (this.showEON117Section) {
			setEON117Table(doc);
		}

		hideSections(doc);
		endorsementNumeration(doc);
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}

	@Override
	public void initializeFields() {
		fieldNames = new String[] { "BrokerName", "MasterBrokerNumber", "Insured_Name", "Pol_No", "Effective_Date",
				"Expiry_Date", "ADJ50_Prem", "ADJProrata_Prem", "PolEffective_Date", "AdjEffective_Date" };
		fieldValues = new Object[] { brokerName, master_broker_no, insuredName, polNo, effectiveDate, expiryDate,
				adj50_Prem, adjProrata_Prem, polEffectiveDate, adjEffectiveDate };
	}

	@Override
	protected void initializeSections(Document doc) {
		eon116Sec = doc.getSections().get(0);
		eon117Sec = doc.getSections().get(1);
	}

	@Override
	protected void hideSections(Document doc) {
		if (!showEON116Section) {
			doc.removeChild(eon116Sec);
		}

		if (!showEON117Section) {
			doc.removeChild(eon117Sec);
		}
	}

}
