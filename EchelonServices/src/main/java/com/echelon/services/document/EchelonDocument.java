package com.echelon.services.document;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.utils.SpecialtyAutoConstants;

public abstract class EchelonDocument {
	public PolicyDocuments policyDocument;
	public String templatePath = getTemplatePath();
	
	public abstract String getTemplatePath();

	public abstract void create(String storageKey) throws Exception;

	public abstract void initializeFields();

	public List<SpecialtyVehicleRisk> getVehicleRisks() {
		List<SpecialtyVehicleRisk> vehicles = new ArrayList<SpecialtyVehicleRisk>();

		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (subPolicy != null && subPolicy.getSubPolicyRisks() != null) {
			Set<Risk> risks = subPolicy.getSubPolicyRisks();
			for (Risk risk : risks) {
				if (risk.getRiskType().equalsIgnoreCase("AU")) {
					vehicles.add((SpecialtyVehicleRisk) risk);
				}
			}
		}
		vehicles.sort(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence));

		return vehicles;
	}

	public Date convertLocalDateToDate(LocalDate date) {
		return java.sql.Date.valueOf(date);
	}

	public Date convertLocalDateTimeToDate(LocalDateTime date) {
		return java.util.Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
	}

	public String formatDate(Date date, String pattern) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("en", "US"));
		String dateWithFormat = simpleDateFormat.format(date);
		return dateWithFormat;
	}

	public String formatDate(LocalDate date, String pattern) {
		return formatDate(convertLocalDateToDate(date), pattern);
	}

	public String numberFormat(Object n) {
		if (n == null) {
			return "";
		}
		NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
		String val = nf.format(n);
		return val;
	}

	public String premFormat(Double n) {
		return premFormat(n, true);
	}

	public String premFormat(Double n, boolean showParenthesis) {
		String val = null;
		if (n == null) {
			return "";
		}

		if (n < 0) {
			n = n * -1;
			val = showParenthesis ? "($" + numberFormat(n) + ")" : "$" + numberFormat(n);
		} else {
			val = "$" + numberFormat(n);
		}
		return val;
	}

	public double stringToDouble(String strNumber) throws NumberFormatException {
		if (strNumber == null || strNumber.equals("")) {
			return 0.0;
		}
		double number = Double.parseDouble(strNumber.replace(",", ""));
		return number;
	}

}
