package com.echelon.services.document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.ControlChar;
import com.aspose.words.Document;
import com.aspose.words.Paragraph;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyPackage extends EchelonDocument implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1504327312394811511L;
	protected static final LocalDateTime JAN2021 = LocalDateTime.of(2021, 1, 1, 0, 0);
	protected String[] fieldNames;
	protected Object[] fieldValues;
	protected String docDate = "";
	protected String insuredName = "";
	protected String insuredAddressStreet = "";
	protected String insuredAddressCity = "";
	protected String insuredAddressProvState = "";
	protected String insuredAddressPC = "";
	protected String contractNo = "";
	protected String polNo = "";
	protected String end_desc = "";
	protected String brokerContact = "";
	protected String brokerName = "";
	protected String effectiveDate = "";
	protected String expiryDate = "";
	protected String lbLimit = "";
	protected String opcf8_ded = "N/A";
	protected String dcpd_ded = "N/A";
	protected String cargo_ded = "N/A";
	protected String cpe_limit = "N/A";
	protected String cpe_ded = "N/A";
	protected String cargoLimit = "N/A";
	protected String cargoLimit1 = "N/A";
	protected String cargoLimit2 = "N/A";
	protected String cargoLimit3 = "N/A";
	protected String cargoLimit4 = "N/A";
	protected String cargoLimit5 = "N/A";
	protected String cgl_lb_limit = "N/A";
	protected String cgl_ded = "N/A";
	protected String mex_limit = "N/A";
	protected String tll_limit = "N/A";
	protected String tll_ded = "N/A";
	protected String ebe_limit = "N/A";
	protected String ebe_ded = "N/A";
	protected String spf6_lb_limit = "N/A";
	protected String sef96_lb_limit = "N/A";
	protected String sef99_lb_limit = "N/A";
	protected String oap4_lb_limit = "N/A";
	protected String oap4_dcpd_ded = "N/A";
	protected String cl511_ded = "N/A";
	protected String cm512_limit = "N/A";
	protected String cm512_ded = "N/A";
	protected String sp513_limit = "N/A";
	protected String sp513_ded = "N/A";
	protected String sp514_limit = "N/A";
	protected String sp514_ded = "N/A";
	protected String gar511_ded = "";
	protected String gar511_Prem = "";
	protected String subSecA = "";
	protected String subSecB = "";
	protected String subSecC = "";
	protected String subSecD = "";
	protected String sbs_limA = "";
	protected String sbs_limB = "";
	protected String sbs_limC = "";
	protected String sbs_limD = "";
	protected String sbs_dedA = "";
	protected String sbs_dedB = "";
	protected String sbs_dedC = "";
	protected String sbs_dedD = "";
	protected String sbs_premA = "";
	protected String sbs_premB = "";
	protected String sbs_premC = "";
	protected String sbs_premD = "";
	protected String oef81_lim = "N/A";
	protected String oef81_prem = "N/A";
	protected String oap4_cl_limit = "N/A";
	protected String oap4_cl_ded = "N/A";
	protected String oap4_sp_limit = "N/A";
	protected String oap4_sp_ded = "N/A";
	protected String oap4_ab_limit = "$N/A";
	protected String oap4_ua_limit = "N/A";
	protected String oef71_limit = "N/A";
	protected String oef71_ded = "N/A";
	protected String oef77_limit = "N/A";
	protected String oef77_ded = "N/A";
	protected String oef77_oap4_sp_prema = "";
	protected String oef77_oap4_sp_premb = "";
	protected String oef77_oap4_sp_premc = "";
	protected String oef77_oap4_sp_premd = "";
	protected String oef82_511_ded = "";
	protected String oef82_511_prem = "";
	protected String oef82_512_ded = "";
	protected String oef82_512_prem = "";
	protected String oef82_513_ded = "";
	protected String oef82_513_prem = "";
	protected String oef82_lim = "";
	protected LinkedHashSet<HashMap<String, String>> oef82PersonsTable = new LinkedHashSet<HashMap<String, String>>();
	protected String oef83_limit = "N/A";
	protected String oef83_ded = "N/A";
	protected String oef86_limit = "N/A";
	protected String total_premium = "";
	protected String opcf21b_prem = "N/A";
	protected String eon110_no = "N/A";
	protected LinkedHashSet<Endorsement> numEnds = new LinkedHashSet<Endorsement>();
	protected String maxendno = "";
	protected String maxendno_item6 = "";
	protected String master_broker_no = "";
	protected String broker_addr = "";
	protected String broker_city = "";
	protected String broker_provState = "";
	protected String broker_pc = "";
	protected String issue_date = "";
	protected String effYear = "";
	protected String effMonth = "";
	protected String effDay = "";
	protected String expYear = "";
	protected String expMonth = "";
	protected String expDay = "";
	protected String prepDate = "";
	protected String biTotalPrem = "";
	protected String pdTotalPrem = "";
	protected String abTotalPrem = "";
	protected String uaTotalPrem = "";
	protected String dcpdTotalPrem = "";
	protected String spTotalPrem = "";
	protected String compTotalPrem = "";
	protected String collTotalPrem = "";
	protected String apTotalPrem = "";
	protected String endList1 = "";
	protected String vehPrem = "";
	protected String opcf2_name = "";
	protected String opcf2_relationship = "";
	protected String opcf20_limit = "";
	protected String opcf20H_limit = "";
	protected boolean opcf20_exists = false;
	protected String termination_day = "";
	protected boolean opcf21B_prorata = false;
	protected boolean opcf21B_5050 = false;
	protected String opcf27_apded = "";
	protected String opcf27_limit = "";
	protected String opcf27B_apded = "";
	protected String businessType = "";
	protected LinkedHashSet<HashMap<String, String>> opcf27PersonsTable = new LinkedHashSet<HashMap<String, String>>();
	protected LinkedHashSet<Endorsement> opcf27BEnds = new LinkedHashSet<Endorsement>();
	protected List<Endorsement> opcf28AEnds = new ArrayList<Endorsement>();
	protected String opcf27B_limit = "";
	protected String opcf28_pers = "";
	protected String opcf28_limit = "";
	protected String opcf28CL_ded = "";
	protected String opcf28AP_ded = "";
	protected boolean opcf28_coll = false;
	protected boolean opcf28_allperil = false;
	protected boolean opcf28_collNot = false;
	protected boolean opcf28_allperilNot = false;
	protected LinkedHashSet<Endorsement> opcf38Ends = new LinkedHashSet<Endorsement>();
	protected String cargoLimit1Dec = "N/A";
	protected String cargoLimit2Dec = "N/A";
	protected String cargoLimit3Dec = "N/A";
	protected String cargoLimit4Dec = "N/A";
	protected String cargoLimit5Dec = "N/A";
	protected String cargoLimit6Dec = "N/A";
	protected String totalCargoPrem = "";
	protected String cargo_locations = "";
	protected String totalCGLPrem = "";
	protected String cgl_locations = "";
	protected String gar_locationa = "";
	protected boolean insBldga = false;
	protected boolean insLota = false;
	protected String gar_locationb = "";
	protected boolean insBldgb = false;
	protected boolean insLotb = false;
	protected String gar_locationc = "";
	protected boolean insBldgc = false;
	protected boolean insLotc = false;
	protected String gar_locationd = "";
	protected boolean insBldgd = false;
	protected boolean insLotd = false;
	protected String garBusinessType = "";
	protected String ft_emp = "";
	protected String pt_emp = "";
	protected String oap4_bi_prem = "";
	protected String oap4_pd_prem = "";
	protected String oap4_bipd_prem = "";
	protected String oap4_ab_prem = "";
	protected String oap4_dcpd_prem = "";
	protected String oap4_cl_prem = "";
	protected String maxnum_veha = "";
	protected String oap4_sp_limita = "";
	protected String oap4_sp_deda = "";
	protected String oap4_sp_prema = "";
	protected String maxnum_vehb = "";
	protected String oap4_sp_limitb = "";
	protected String oap4_sp_dedb = "";
	protected String oap4_sp_premb = "";
	protected String maxnum_vehc = "";
	protected String oap4_sp_limitc = "";
	protected String oap4_sp_dedc = "";
	protected String oap4_sp_premc = "";
	protected String maxnum_vehd = "";
	protected String oap4_sp_limitd = "";
	protected String oap4_sp_dedd = "";
	protected String oap4_sp_premd = "";
	protected String oap4_prem = "";
	protected String gar_end_code = "";
	protected LinkedHashSet<HashMap<String, String>> oef72AlterationsTable = new LinkedHashSet<HashMap<String, String>>();
	protected String cm512_prem = "";
	protected String sp513_prem = "";
	protected String sp514_prem = "";
	protected String sp64_prem = "";
	protected String oef72_end_codes = "";
	protected String oef72_end_prem = "Incl.";
	protected String oef72_net_prem = "Incl.";
	protected String oef72_prem = "Incl.";
	protected String oef73_other_party = "";
	protected String oef73_other_party_address = "";
	protected String oef74_ded = "";
	protected String oef75_ded = "";
	protected String oef76_insured_name = "";
	protected String oef76_insured_relationship = "";
	protected String oef78_pers = "";
	protected String oef78_liab_limit = "";
	protected String oef78_insuredind1 = "";
	protected String cl5_ded = "";
	protected String oef78_insuredind2 = "";
	protected String cl6_lim = "";
	protected String cl6_ded = "";
	protected String oef78_insuredind3 = "";
	protected String oef78A_exclDriver = "";
	protected String oef78A_driverLicNo = "";
	protected String oef83_numOfVeh = "";
	protected String oef83_coll5 = "";
	protected String oef83_coll6 = "";
	protected String gdpc_servPlateNo = "";
	protected String gdpc_expDate = "";
	protected String gspc_servPlateNo = "";
	protected String gspc_expDate = "";
	protected String eon114_name_ins = "";
	protected String eon102_ded = "";
	protected String eon102_sec7_ded = "";
	protected String add_ins_name = "";
	protected String add_ins_addr_street = "";
	protected String add_ins_addr_city = "";
	protected String add_ins_addr_pc = "";
	protected String gecglmtc_mod_type = "";
	protected String gecglmtc_veh_desc = "";
	protected String gecglmtc_veh_serial_no = "";
	protected String cgl_ge_prem = "";
	protected String cgo_limit = "";
	protected String cgo_ded = "";
	protected String cargo_ge_prem = "";
	protected String per_occurrence_ded = "N/A";
	protected String gecgl_mod_type = "";
	protected String gecgl_veh_desc = "";
	protected String gecgl_veh_serial_no = "";
	protected String gecgl_prem = "";
	protected String aic_limit = "";
	protected String aic_ded = "";
	protected String aicc_limit = "";
	protected String aicc_ded = "";
	protected LinkedHashSet<Endorsement> eon109Ends = new LinkedHashSet<Endorsement>();
	protected String eon111_effective_date = "";
	protected String eon111_expiry_date = "";
	protected String date = "";
	protected String gdpc_dealer_plate_no = "";
	protected String gdpc_exp_date = "";
	protected String wos_co_name = "";
	protected String wos_co_addr = "";
	protected String aoccgl_prem = "";
	protected String moccgl_prem = "";
	protected String aocc_prem = "";
	protected String mocc_prem = "";
	protected String cargo = "";
	protected String cargo_prem = "";
	protected String emp_benefits = "";
	protected String ebe_prem = "";
	protected String opcf44RPrintLimit = "";
	protected String sec7Ded = "";
	protected String vehDesc = "";
	protected String fleetBasis = "";
	protected String ttc_vehdesc = "";
	protected String ttc_period1 = "";
	protected String ttc_period2 = "";
	protected String ttc_serno = "";
	protected String ttc_limit = "";
	protected String ttc_ded = "";
	protected String ttc_prem = "";
	protected String ctc_limit = "N/A";
	protected String subPolicyNamedInsured = "";
	protected boolean ebeExists = false;
	protected boolean opcf23A_dcpd_ck = false;
	protected String opcf23A_dcpd_ded = "";
	protected boolean opcf23A_sp_ck = false;
	protected String opcf23A_sp_ded = "";
	protected boolean opcf23A_cl_ck = false;
	protected String opcf23A_cl_ded = "";
	protected boolean opcf23A_cm_ck = false;
	protected String opcf23A_cm_ded = "";
	protected boolean opcf23A_ap_ck = false;
	protected String opcf23A_ap_ded = "";
	protected boolean opcf23ABL_dcpd_ck = false;
	protected String opcf23ABL_dcpd_ded = "";
	protected boolean opcf23ABL_sp_ck = false;
	protected String opcf23ABL_sp_ded = "";
	protected boolean opcf23ABL_cl_ck = false;
	protected String opcf23ABL_cl_ded = "";
	protected boolean opcf23ABL_cm_ck = false;
	protected String opcf23ABL_cm_ded = "";
	protected boolean opcf23ABL_ap_ck = false;
	protected String opcf23ABL_ap_ded = "";
	protected Section genDecSec = null;
	protected Section opcf2Sec = null;
	protected Section opcf5Sec = null;
	protected Section opcf5BlSec = null;
	protected Section opcf8Sec = null;
	protected Section opcf9Sec = null;
	protected Section opcf13CSec = null;
	protected Section opcf19Sec = null;
	protected Section opcf20Sec = null;
	protected Section opcf20HSec = null;
	protected Section opcf21ASec = null;
	protected Section opcf21BSec = null;
	protected Section opcf23ASec = null;
	protected Section opcf23ABlSec = null;
	protected Section opcf25ASec = null;
	protected Section opcf27Sec = null;
	protected Section opcf27BSec = null;
	protected Section opcf28Sec = null;
	protected Section opcf28ASec = null;
	protected Section opcf30Sec = null;
	protected Section opcf31Sec = null;
	protected Section opcf38Sec = null;
	protected Section opcf40Sec = null;
	protected Section opcf43Sec = null;
	protected Section opcf43ASec = null;
	protected Section opcf44RSec = null;
	protected Section opcf47Sec = null;
	protected Section opcf48Sec = null;
	protected Section cgoDecSec = null;
	protected Section cglDecSec = null;
	protected Section garDecSec = null;
	protected Section oef71Sec = null;
	protected Section oef72Sec = null;
	protected Section oef73Sec = null;
	protected Section oef74Sec = null;
	protected Section oef75Sec = null;
	protected Section oef76Sec = null;
	protected Section oef77Sec = null;
	protected Section oef78Sec = null;
	protected Section oef78ASec = null;
	protected Section oef79Sec = null;
	protected Section oef80Sec = null;
	protected Section oef81Sec = null;
	protected Section oef82Sec = null;
	protected Section oef83Sec = null;
	protected Section oef86Sec = null;
	protected Section oef87Sec = null;
	protected Section gdpcgSec = null;
	protected Section gspcSec = null;
	protected Section spf6_1Sec = null;
	protected Section spf6_2Sec = null;
	protected Section spf6_3Sec = null;
	protected Section spf6_4Sec = null;
	protected Section spf6_5Sec = null;
	protected Section spf6_6Sec = null;
	protected Section spf6_7Sec = null;
	protected Section spf6_8Sec = null;
	protected Section spf6_9Sec = null;
	protected Section sef96Sec = null;
	protected Section sef99Sec = null;
	protected Section endorsementSection = null;
	protected Section eon114Sec = null;
	protected Section eon101Sec = null;
	protected Section eon102Sec = null;
	protected Section gecglmtcSec = null;
	protected Section gecglSec = null;
	protected Section eon20GSec = null;
	protected Section aicSec = null;
	protected Section aiccSec = null;
	protected Section eon109Sec = null;
	protected Section eon110Sec = null;
	protected Section eon111Sec = null;
	protected Section eon112Sec = null;
	protected Section eon121Sec = null;
	protected Section gleeSec = null;
	protected Section iaSec = null;
	protected Section gdpcSec = null;
	protected Section docSec = null;
	protected Section cpeeSec = null;
	protected Section eon116Sec = null;
	protected Section eon117Sec = null;
	protected Section wosSec = null;
	protected Section aoccglSec = null;
	protected Section moccglSec = null;
	protected Section aoccSec = null;
	protected Section moccSec = null;
	protected Section cdelSec = null;
	protected Section cdecSec = null;
	protected Section ttcSec = null;
	protected boolean showOPCF2Section = false;
	protected boolean showOPCF5Section = false;
	protected boolean showOPCF5BlSection = false;
	protected boolean showOPCF8Section = false;
	protected boolean showOPCF9Section = false;
	protected boolean showOPCF13CSection = false;
	protected boolean showOPCF19Section = false;
	protected boolean showOPCF20Section = false;
	protected boolean showOPCF20HSection = false;
	protected boolean showOPCF21ASection = false;
	protected boolean showOPCF21BSection = false;
	protected boolean showOPCF23ASection = false;
	protected boolean showOPCF23ABlSection = false;
	protected boolean showOPCF25ASection = false;
	protected boolean showOPCF27Section = false;
	protected boolean showOPCF27BSection = false;
	protected boolean showOPCF28Section = false;
	protected boolean showOPCF28ASection = false;
	protected boolean showOPCF30Section = false;
	protected boolean showOPCF31Section = false;
	protected boolean showOPCF38Section = false;
	protected boolean showOPCF40Section = false;
	protected boolean showOPCF43Section = false;
	protected boolean showOPCF43ASection = false;
	protected boolean showOPCF44RSection = false;
	protected boolean showOPCF47Section = false;
	protected boolean showOPCF48Section = false;
	protected boolean showCGODecSection = false;
	protected boolean showCGLDecSection = false;
	protected boolean showGARDecSection = false;
	protected boolean showOEF71Section = false;
	protected boolean showOEF72Section = false;
	protected boolean showOEF73Section = false;
	protected boolean showOEF74Section = false;
	protected boolean showOEF75Section = false;
	protected boolean showOEF76Section = false;
	protected boolean showOEF77Section = false;
	protected boolean showOEF78Section = false;
	protected boolean showOEF78ASection = false;
	protected boolean showOEF79Section = false;
	protected boolean showOEF80Section = false;
	protected boolean showOEF81Section = false;
	protected boolean showOEF82Section = false;
	protected boolean showOEF83Section = false;
	protected boolean showOEF86Section = false;
	protected boolean showOEF87Section = false;
	protected boolean showGDPCGSection = false;
	protected boolean showGSPCSection = false;
	protected boolean showSPF6Section = false;
	protected boolean showSEF96Section = false;
	protected boolean showSEF99Section = false;
	protected boolean showEON114Section = false;
	protected boolean showEON101Section = false;
	protected boolean showEON102Section = false;
	protected boolean showGECGLMTCSection = false;
	protected boolean showGECGLSection = false;
	protected boolean showEON20GSection = false;
	protected boolean showAICSection = false;
	protected boolean showAICCSection = false;
	protected boolean showEON109Section = false;
	protected boolean showEON110Section = false;
	protected boolean showEON111Section = false;
	protected boolean showEON112Section = false;
	protected boolean showEON121Section = false;
	protected boolean showGLEESection = false;
	protected boolean showIASection = false;
	protected boolean showGDPCSection = false;
	protected boolean showDOCSection = false;
	protected boolean showCPEESection = false;
	protected boolean showEON116Section = false;
	protected boolean showEON117Section = false;
	protected boolean showWOSSection = false;
	protected boolean showAOCCGLSection = false;
	protected boolean showMOCCGLSection = false;
	protected boolean showAOCCSection = false;
	protected boolean showMOCCSection = false;
	protected boolean showCDELSection = false;
	protected boolean showCDECSection = false;
	protected boolean showTTCSection = false;
	protected boolean showCDECDecRow = false;
	protected boolean isLaterThanJan2021 = false;


	public String getTemplatePath() {
		return "";
	}

	public void create(String storageKey) throws Exception {
	}

	public void initializeFields() {
	}

	protected void setGeneralDeclarationsTable(Document doc) {
		Table table = genDecSec.getBody().getTables().get(1);
		Row cdecRow = table.getRows().get(21);
		Row cdelRow = table.getRows().get(28);
		if (!isLaterThanJan2021) {
			table.removeChild(cdecRow);
			table.removeChild(cdelRow);
		}
	}

	protected void setOPCF5LessorTable(Document doc) {
		// LESSOR table
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = opcf5Sec;
		Section refSection = tmpSection;
		for (LienholderLessorSubPolicy lessorSubPolicy : subPolicyAuto.getLienholderLessors()) {
			if (lessorSubPolicy.getLienLessorType().equalsIgnoreCase("LESSOR")) {
				LienholderLessor lessor = lessorSubPolicy.getLienholderLessor();
				Section lessorSection = (Section) tmpSection.deepClone(true);
				Table lessorTable = lessorSection.getBody().getTables().get(1);

				// Set Lessor Name
				Row nameRow = lessorTable.getRows().get(0);
				Cell nameCell = nameRow.getCells().get(1);
				nameCell.getFirstParagraph().removeAllChildren();
				Run nameRun = new Run(doc, lessor.getCompanyName());
				nameRun.getFont().setName("Arial");
				nameRun.getFont().setSize(8);
				nameRun.getFont().setBold(true);
				nameCell.getFirstParagraph().appendChild(nameRun);
				// Set Lessor addr
				Row addrRow = lessorTable.getRows().get(1);
				Cell addrCell = addrRow.getCells().get(1);
				addrCell.getFirstParagraph().removeAllChildren();
				Run addrRun = new Run(doc,
						lessor.getLienholderLessorAddress().getAddressLine1() + ", "
								+ lessor.getLienholderLessorAddress().getCity() + " " + lessor.getLienholderLessorAddress().getProvState() + " "
								+ lessor.getLienholderLessorAddress().getPostalZip());
				addrRun.getFont().setName("Arial");
				addrRun.getFont().setSize(8);
				addrRun.getFont().setBold(true);
				addrCell.getFirstParagraph().appendChild(addrRun);

				Row firstVehicleRow = lessorTable.getRows().get(4);
				List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
				vehicleDetails.sort(Comparator.comparingInt(lhl -> {
					if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
						return Integer.parseInt(lhl.getVehicleNum());
					} else {
						return 0;
					}
				}));
				for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
					if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
							.equals(lessor.getLienholderLessorPK())) {
						Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

						// Veh_Num
						newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
						Run newRun1 = new Run(doc, vehicleDetail.getVehicleNum() != null ? vehicleDetail.getVehicleNum().toString() : "");
						newRun1.getFont().setName("Arial");
						newRun1.getFont().setSize(8);
						newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

						// Veh_Desc
						newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
						Run newRun2 = new Run(doc, vehicleDetail.getVehicleYear() + " " + vehicleDetail.getDescription());
						newRun2.getFont().setName("Arial");
						newRun2.getFont().setSize(8);
						newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

						// Veh_Serial_No
						newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
						Run newRun3 = new Run(doc, vehicleDetail.getSerialNum());
						newRun3.getFont().setName("Arial");
						newRun3.getFont().setSize(8);
						newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

						// LB_Limit
						newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
						Run newRun4 = new Run(doc, "$" + lbLimit);
						newRun4.getFont().setName("Arial");
						newRun4.getFont().setSize(8);
						newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

						// AP_Ded
						newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
						Run newRun5 = new Run(doc, "$" + numberFormat(vehicleDetail.getLessorDeductible()));
						newRun5.getFont().setName("Arial");
						newRun5.getFont().setSize(8);
						newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

						lessorTable.insertAfter(newVehicleRow, firstVehicleRow);
						firstVehicleRow = newVehicleRow;
					}
				}
				lessorTable.removeChild(lessorTable.getRows().get(4));
				doc.insertAfter(lessorSection, refSection);
				refSection = lessorSection;
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF19Section(Document doc) throws Exception {
		Section tmpSection = opcf19Sec;
		Section refSection = tmpSection;

		List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();

		for (SpecialtyVehicleRisk vehiclerisk : vehicleRisks) {
			Set<Endorsement> vehEndorsements = vehiclerisk.getRiskEndorsements();
			for (Endorsement end : vehEndorsements) {
				if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
					String value = numberFormat(end.getLimit1amount());
					String vehicles = "";
					if (fleetBasis.equalsIgnoreCase("SCHD")) {
						vehicles = numberFormat(vehiclerisk.getVehicleSequence());
					} else {
						for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
							for (ExtensionData extData : extType.getExtensionDatas()) {
								if (extData.getColumnId().equalsIgnoreCase("OPCF19_VehDesc")
										&& extData.getColumnValue() != null) {
									vehicles = extData.getColumnValue();
								}
							}
						}
					}

					Section new19Section = (Section) tmpSection.deepClone(true);

					// OPCF19_vehicles
					Table table = new19Section.getBody().getTables().get(1);
					Row row = table.getRows().get(0);
					Cell cell = row.getCells().get(1);
					cell.getFirstParagraph().getRange().replace("{{OPCF19_Vehicles}}", vehicles);

					// OPCF19_value
					new19Section.getBody().getParagraphs().get(3).getRange().replace("{{OPCF19_Value}}", value);

					doc.insertAfter(new19Section, refSection);
					refSection = new19Section;
				}
			}

		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF21BFirstTable(Document doc) {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		Section sec = opcf21BSec;
		Table tmpTable = sec.getBody().getTables().get(2);
		Row firstVehicleRow = tmpTable.getRows().get(3);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			if (vehicle.getVehicleDescription().equals("OP27")) {
				continue;
			}
			String veh_desc = vehicle.getDisplayString();
			String dcdpd_ded = "N/A";
			String sp_ded = "N/A";
			String comp_ded = "N/A";
			String coll_ded = "N/A";
			String ap_ded = "N/A";

			for (Coverage cove : vehicle.getRiskCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("DCPD") && cove.getDeductible1() != null) {
					dcdpd_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP") && cove.getDeductible1() != null) {
					sp_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM") && cove.getDeductible1() != null) {
					comp_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL") && cove.getDeductible1() != null) {
					coll_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP") && cove.getDeductible1() != null) {
					ap_ded = numberFormat(cove.getDeductible1());
				}
			}

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// Veh_Desc
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, veh_desc);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

			// DCDPD_Ded
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, dcdpd_ded);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			if (!dcdpd_ded.equalsIgnoreCase("N/A")) {
				newRun2.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

			// SP_Ded
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, sp_ded);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			if (!sp_ded.equalsIgnoreCase("N/A")) {
				newRun3.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

			// COMP_Ded
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, comp_ded);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			if (!comp_ded.equalsIgnoreCase("N/A")) {
				newRun4.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

			// COLL_Ded
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, coll_ded);
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			if (!coll_ded.equalsIgnoreCase("N/A")) {
				newRun5.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun5);

			// AP_Ded
			newVehicleRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun6 = new Run(doc, ap_ded);
			newRun6.getFont().setName("Arial");
			newRun6.getFont().setSize(8);
			if (!ap_ded.equalsIgnoreCase("N/A")) {
				newRun6.getFont().setBold(true);
			}
			newVehicleRow.getCells().get(5).getFirstParagraph().appendChild(newRun6);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}
		tmpTable.removeChild(tmpTable.getRows().get(3));
	}

	protected void setOPCF21BSecondTable(Document doc) throws Exception {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		String veh_desc = "";
		String unitRate = "";
		String numOfUnits = "";
		String vehPrem = "";

		Section sec = opcf21BSec;
		Table tmpTable = sec.getBody().getTables().get(4);
		Row firstVehicleRow = tmpTable.getRows().get(1);

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			veh_desc = vehicle.getDisplayString();
			unitRate = "$" + numberFormat(vehicle.getUnitRate());
			numOfUnits = Integer.toString(vehicle.getNumberOfUnits());
			if(vehicle.getUnitRate() != null && vehicle.getNumberOfUnits() != null) {
				vehPrem = numberFormat(vehicle.getUnitRate() * vehicle.getNumberOfUnits());
			}

			if(vehicle.getVehicleDescription().equals("OP27")) {
				veh_desc = "OPCF 27B";
				unitRate = "";
				numOfUnits = "";
				vehPrem = numberFormat(vehicle.getRiskPremium().getWrittenPremium());
			}

			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, numOfUnits);
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, veh_desc);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, unitRate);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$" + vehPrem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		if (this.showCGODecSection) {
			// Cargo Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.cargo);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$ " + this.cargo_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		if (this.ebeExists) {
			// Employee Benefits Row
			Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

			// NumOfUnits
			newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, "");
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(8);
			newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Veh_Desc
			newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, this.emp_benefits);
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Location
			newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, "");
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// UnitRate
			newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, "");
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// VehPrem
			newVehicleRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, "$ " + this.ebe_prem);
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(8);
			newVehicleRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
			firstVehicleRow = newVehicleRow;
		}

		tmpTable.removeChild(tmpTable.getRows().get(1));
	}

	protected void setOPCF23ASecondTable(Document doc) {
		// LIENHOLDER table
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction().findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = opcf23ASec;
		Section refSection = tmpSection;
		for (LienholderLessorSubPolicy lienholderSubPolicy : subPolicyAuto.getLienholderLessors()) {
			if (lienholderSubPolicy.getLienLessorType().equalsIgnoreCase("LIENHOLDER")) {
				LienholderLessor lienHolder = lienholderSubPolicy.getLienholderLessor();
				Section lienHolderSection = (Section) tmpSection.deepClone(true);
				Table lienHolderTable = lienHolderSection.getBody().getTables().get(1);

				// Set Lienholder Name and Address
				Row addrRow = lienHolderTable.getFirstRow();
				Cell addrCell = addrRow.getFirstCell();

				//Name
				Paragraph paraName = addrCell.getParagraphs().get(2);
				Run runName = new Run(doc, lienHolder.getCompanyName());
				runName.getFont().setName("Arial");
				runName.getFont().setSize(8);
				runName.getFont().setBold(true);
				paraName.removeAllChildren();
				paraName.appendChild(runName);

				//Street
				Paragraph paraStreet = addrCell.getParagraphs().get(3);
				Run runStreet = new Run(doc, lienHolder.getLienholderLessorAddress().getAddressLine1());
				runStreet.getFont().setName("Arial");
				runStreet.getFont().setSize(8);
				runStreet.getFont().setBold(true);
				paraStreet.removeAllChildren();
				paraStreet.appendChild(runStreet);

				//City & Zip Code
				Paragraph paraCity = addrCell.getParagraphs().get(4);
				Run runCity = new Run(doc, lienHolder.getLienholderLessorAddress().getCity() + " " + lienHolder.getLienholderLessorAddress().getProvState() + " "
						+ lienHolder.getLienholderLessorAddress().getPostalZip());
				runCity.getFont().setName("Arial");
				runCity.getFont().setSize(8);
				runCity.getFont().setBold(true);
				paraCity.removeAllChildren();
				paraCity.appendChild(runCity);

				Table vehicleTable = lienHolderSection.getBody().getTables().get(2);
				Row firstVehicleRow = vehicleTable.getRows().get(1);
				List<LienholderLessorVehicleDetails> vehicleDetails = subPolicyAuto.getLienholderLessorVehicleDetails().stream().collect(Collectors.toList());
				vehicleDetails.sort(Comparator.comparingInt(lhl -> {
					if (lhl.getVehicleNum() != null && !lhl.getVehicleNum().equals("")) {
						return Integer.parseInt(lhl.getVehicleNum());
					} else {
						return 0;
					}
				}));
				for (LienholderLessorVehicleDetails vehicleDetail : vehicleDetails) {
					if (vehicleDetail.getLienholderLessor().getLienholderLessorPK()
							.equals(lienHolder.getLienholderLessorPK())) {
						Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

						// Automobile #
						newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
						Run newRun1 = new Run(doc, vehicleDetail.getVehicleNum() != null ? vehicleDetail.getVehicleNum().toString() : "");
						newRun1.getFont().setName("Arial");
						newRun1.getFont().setSize(8);
						newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

						// Model Year
						newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
						Run newRun2 = new Run(doc, vehicleDetail.getVehicleYear() != null ? Integer.toString(vehicleDetail.getVehicleYear()) : "");
						newRun2.getFont().setName("Arial");
						newRun2.getFont().setSize(8);
						newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

						// Trade NAme (Make)
						newVehicleRow.getCells().get(2).getFirstParagraph().removeAllChildren();
						Run newRun3 = new Run(doc, vehicleDetail.getDescription());
						newRun3.getFont().setName("Arial");
						newRun3.getFont().setSize(8);
						newVehicleRow.getCells().get(2).getFirstParagraph().appendChild(newRun3);

						// Serial #
						newVehicleRow.getCells().get(3).getFirstParagraph().removeAllChildren();
						Run newRun4 = new Run(doc, vehicleDetail.getSerialNum());
						newRun4.getFont().setName("Arial");
						newRun4.getFont().setSize(8);
						newVehicleRow.getCells().get(3).getFirstParagraph().appendChild(newRun4);

						vehicleTable.insertAfter(newVehicleRow, firstVehicleRow);
						firstVehicleRow = newVehicleRow;
					}
				}
				vehicleTable.removeChild(vehicleTable.getRows().get(1));
				doc.insertAfter(lienHolderSection, refSection);
				refSection = lienHolderSection;
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF27Table(Document doc) {
		Section sec = opcf27Sec;
		Table table = sec.getBody().getTables().get(2);
		Row firstRow = table.getRows().get(1);
		for(HashMap<String,String> person : opcf27PersonsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Named Persons
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, person.get("pers"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Relationship to Insured/Lessee
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, person.get("relation"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setOPCF27BSection(Document doc) throws Exception {
		Section tmpSection = opcf27BSec;
		Section refSection = tmpSection;

		Iterator<Endorsement> it = opcf27BEnds.iterator();
		while (it.hasNext()) {
			Endorsement end = it.next();
			String typeOfVehicle = "";
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")) {
						typeOfVehicle = extData.getColumnValue();
					}
				}
			}
			String limit = numberFormat(end.getLimit1amount());
			String deductible = numberFormat(end.getDeductible1amount());

			Section new27BSection = (Section) tmpSection.deepClone(true);

			//OPCF27B_APDed
			Table dedTable = new27BSection.getBody().getTables().get(2);
			dedTable.getRows().get(4).getRange().replace("{{OPCF27B_APDed}}", deductible);

			//OPCF27B_Tractor_Trailer
			Table vehTypeTable = new27BSection.getBody().getTables().get(3);
			vehTypeTable.getFirstRow().getRange().replace("{{OPCF27B_Tractor_Trailer}}", typeOfVehicle);

			// OPCF27B_Limit
			new27BSection.getBody().getParagraphs().get(13).getRange().replace("{{OPCF27B_Limit}}", limit);

			doc.insertAfter(new27BSection, refSection);
			refSection = new27BSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF28ASection(Document doc) {
		Section tmpSection = opcf28ASec;
		Section refSection = tmpSection;
		opcf28AEnds.sort(Comparator.comparing(Endorsement::getEndorsementsk));

		for (Endorsement end : opcf28AEnds) {
			StringBuilder autoNumSB = new StringBuilder();
			StringBuilder modYearSB = new StringBuilder();
			StringBuilder makeSB = new StringBuilder();
			StringBuilder serNumSB = new StringBuilder();
			String driver = "";
			String licNum = "";
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("OPCF28A_AutoNum") && extData.getColumnValue() != null) {
						if (autoNumSB.length() > 0) {
							autoNumSB.append(ControlChar.LINE_BREAK);
						}
						autoNumSB.append(extData.getColumnValue());
					} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_ModYear")
							&& extData.getColumnValue() != null) {
						if (modYearSB.length() > 0) {
							modYearSB.append(ControlChar.LINE_BREAK);
						}
						modYearSB.append(extData.getColumnValue());
					} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_Make")
							&& extData.getColumnValue() != null) {
						if (makeSB.length() > 0) {
							makeSB.append(ControlChar.LINE_BREAK);
						}
						makeSB.append(extData.getColumnValue());
					} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_SerNum")
							&& extData.getColumnValue() != null) {
						if (serNumSB.length() > 0) {
							serNumSB.append(ControlChar.LINE_BREAK);
						}
						serNumSB.append(extData.getColumnValue());
					} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_Driver")
							&& extData.getColumnValue() != null) {
						driver = extData.getColumnValue();
					} else if (extData.getColumnId().equalsIgnoreCase("OPCF28A_LicNum")
							&& extData.getColumnValue() != null) {
						licNum = extData.getColumnValue();
					}
				}
			}

			Section new28ASection = (Section) tmpSection.deepClone(true);
			Table table = new28ASection.getBody().getTables().get(1);
			Row row = table.getRows().get(1);

			//Automobile #
			row.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run cellRun = new Run(doc, autoNumSB.toString());
			cellRun.getFont().setName("Arial");
			cellRun.getFont().setSize(8);
			row.getCells().get(0).getFirstParagraph().appendChild(cellRun);

			//Model Year
			row.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run cellRun1 = new Run(doc, modYearSB.toString());
			cellRun1.getFont().setName("Arial");
			cellRun1.getFont().setSize(8);
			row.getCells().get(1).getFirstParagraph().appendChild(cellRun1);

			//Make
			row.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run cellRun2 = new Run(doc, makeSB.toString());
			cellRun2.getFont().setName("Arial");
			cellRun2.getFont().setSize(8);
			row.getCells().get(2).getFirstParagraph().appendChild(cellRun2);

			//Serial # / VIN
			row.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run cellRun3 = new Run(doc, serNumSB.toString());
			cellRun3.getFont().setName("Arial");
			cellRun3.getFont().setSize(8);
			row.getCells().get(3).getFirstParagraph().appendChild(cellRun3);

			//Paragraph
			Paragraph para = new28ASection.getBody().getParagraphs().get(21);
			para.removeAllChildren();

			Run paraRun = new Run(doc, "Name of Excluded Driver: ");
			paraRun.getFont().setName("Arial");
			paraRun.getFont().setSize(8);

			Run paraRun1 = new Run(doc, driver);
			paraRun1.getFont().setName("Arial");
			paraRun1.getFont().setSize(8);
			paraRun1.getFont().setBold(true);

			Run paraRun2 = new Run(doc, "      Driver's Licence # ");
			paraRun2.getFont().setName("Arial");
			paraRun2.getFont().setSize(8);

			Run paraRun3 = new Run(doc, licNum);
			paraRun3.getFont().setName("Arial");
			paraRun3.getFont().setSize(8);
			paraRun3.getFont().setBold(true);

			para.appendChild(paraRun);
			para.appendChild(paraRun1);
			para.appendChild(paraRun2);
			para.appendChild(paraRun3);

			doc.insertAfter(new28ASection, refSection);
			refSection = new28ASection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF31Section(Document doc) throws Exception {
		Section tmpSection = opcf31Sec;
		Section refSection = tmpSection;

		List<SpecialtyVehicleRisk> vehicleRisks = getVehicleRisks();

		for (SpecialtyVehicleRisk vechicleRisk : vehicleRisks) {
			for (Endorsement end : vechicleRisk.getRiskEndorsements()) {
				if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
					StringBuilder opcf31VehDescSB = new StringBuilder();
					String limit = numberFormat(end.getLimit1amount());
					String otparty = "";
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("OPCF31_OtParty")
									&& extData.getColumnValue() != null) {
								otparty = extData.getColumnValue();
							} else if (extData.getColumnId().equalsIgnoreCase("OPCF31_VehDesc")
									&& extData.getColumnValue() != null && !fleetBasis.equalsIgnoreCase("SCHD")) {
								if (opcf31VehDescSB.length() > 0) {
									opcf31VehDescSB.append(", ");
								}
								opcf31VehDescSB.append(extData.getColumnValue());
							}
						}
					}

					if (fleetBasis.equalsIgnoreCase("SCHD")) {
						opcf31VehDescSB.append(numberFormat(vechicleRisk.getVehicleSequence()));
					}

					Section new31Section = (Section) tmpSection.deepClone(true);

					// OPCF31_Limit
					new31Section.getBody().getParagraphs().get(5).getRange().replace("{{OPCF31_Limit}}", limit);

					// OPCF31_OTParty
					new31Section.getBody().getParagraphs().get(5).getRange().replace("{{OPCF31_OTParty}}", otparty);

					// OPCF31_VehDesc
					new31Section.getBody().getParagraphs().get(7).getRange().replace("{{OPCF31_VehDesc}}",
							opcf31VehDescSB.toString());

					doc.insertAfter(new31Section, refSection);
					refSection = new31Section;
				}
			}
		}
		doc.removeChild(tmpSection);
	}

	protected void setOPCF38Section(Document doc) throws Exception {
		Section tmpSection = opcf38Sec;
		Section refSection = tmpSection;

		Iterator<Endorsement> it = opcf38Ends.iterator();
		while (it.hasNext()) {
			Endorsement end = it.next();
			String limit = numberFormat(end.getLimit1amount());
			String prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
			String totLimit = numberFormat(end.getLimit1amount());
			String totPrem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
			String desc = "";
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("OPCF38_EqDesc")
							&& extData.getColumnValue() != null) {
						desc = extData.getColumnValue();
					}
				}
			}

			Section new38Section = (Section) tmpSection.deepClone(true);
			Table table = new38Section.getBody().getTables().get(2);
			Row row1 = table.getRows().get(1);
			Cell cell1_0 = row1.getCells().get(0);
			Cell cell1_1 = row1.getCells().get(1);
			Cell cell1_2 = row1.getCells().get(2);
			Row row4 = table.getRows().get(4);
			Cell cell4_1 = row4.getCells().get(1);
			Cell cell4_2 = row4.getCells().get(2);

			//OPCF38_Desc
			cell1_0.getFirstParagraph().getRange().replace("{{OPCF38_Desc}}", desc);

			//OPCF38_Limit
			cell1_1.getFirstParagraph().getRange().replace("{{OPCF38_Limit}}", limit);

			//OPCF38_Prem
			cell1_2.getFirstParagraph().getRange().replace("{{OPCF38_Prem}}", prem);

			//OPCF38_TotLimit
			cell4_1.getFirstParagraph().getRange().replace("{{OPCF38_TotLimit}}", totLimit);

			//OPCF38_TotPrem
			cell4_2.getFirstParagraph().getRange().replace("{{OPCF38_TotPrem}}", totPrem);

			doc.insertAfter(new38Section, refSection);
			refSection = new38Section;
		}
		doc.removeChild(tmpSection);
	}

	protected void setMotorTruckCargoDecTable(Document doc) {
		Table table = cgoDecSec.getBody().getTables().get(0);
		Row cdecRow = table.getRows().get(7);
		if (!showCDECDecRow || !isLaterThanJan2021) {
			table.removeChild(cdecRow);
		}
	}

	protected void setCommercialGeneralLiabilityDecTable(Document doc) {
		Table table = cglDecSec.getBody().getTables().get(0);
		Row cdelRow = table.getRows().get(14);
		if (!isLaterThanJan2021) {
			table.removeChild(cdelRow);
		}
	}

	protected void setOEF72Table(Document doc) {
		Section sec = oef72Sec;
		Table table = sec.getBody().getTables().get(1);
		Row firstRow = table.getRows().get(1);
		for (HashMap<String, String> alteration : oef72AlterationsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Alteration Number
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, alteration.get("altNum"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Change Description
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, alteration.get("changeDesc"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setOEF82FirstTable(Document doc) {
		Section sec = oef82Sec;
		Table table = sec.getBody().getTables().get(1);
		Row firstRow = table.getRows().get(1);
		for (HashMap<String, String> person : oef82PersonsTable) {
			Row newRow = (Row) firstRow.deepClone(true);

			// Number
			newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun0 = new Run(doc, person.get("number"));
			newRun0.getFont().setName("Arial");
			newRun0.getFont().setSize(8);
			newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

			// Name
			newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, person.get("name"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(8);
			newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Relationship
			newRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, person.get("relation"));
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			table.insertAfter(newRow, firstRow);
			firstRow = newRow;
		}
		table.removeChild(table.getRows().get(1));
	}

	protected void setEON102Section(Document doc) {
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Section tmpSection = eon102Sec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyAuto.getAdditionalInsureds()) {
			Section addInsSection = (Section) tmpSection.deepClone(true);

			// Set Additional Insured
			Run nameRun = new Run(doc, addIns.getAdditionalInsuredName());
			nameRun.getFont().setName("Arial");
			nameRun.getFont().setSize(8);
			nameRun.getFont().setBold(true);
			addInsSection.getBody().getParagraphs().get(2).removeAllChildren();
			addInsSection.getBody().getParagraphs().get(2).appendChild(nameRun);

			Run streetRun = new Run(doc, addIns.getAdditionalInsuredAddress().getAddressLine1() + ", "
					+ addIns.getAdditionalInsuredAddress().getCity());
			streetRun.getFont().setName("Arial");
			streetRun.getFont().setSize(8);
			streetRun.getFont().setBold(true);
			addInsSection.getBody().getParagraphs().get(3).removeAllChildren();
			addInsSection.getBody().getParagraphs().get(3).appendChild(streetRun);

			Run pcRun = new Run(doc, addIns.getAdditionalInsuredAddress().getProvState() + " " + addIns.getAdditionalInsuredAddress().getPostalZip());
			pcRun.getFont().setName("Arial");
			pcRun.getFont().setSize(8);
			pcRun.getFont().setBold(true);
			addInsSection.getBody().getParagraphs().get(4).removeAllChildren();
			addInsSection.getBody().getParagraphs().get(4).appendChild(pcRun);

			Table tmpTable = addInsSection.getBody().getTables().get(1);
			Row firstVehicleRow = tmpTable.getRows().get(1);

			for (AdditionalInsuredVehicles addInsVeh : subPolicyAuto.getAdditionalInsuredVehicles()) {
				if (addInsVeh.getAdditionalInsured().getAdditionalInsuredPK().equals(addIns.getAdditionalInsuredPK())) {
					Row newVehicleRow = (Row) firstVehicleRow.deepClone(true);

					// Veh_Desc
					newVehicleRow.getCells().get(0).getFirstParagraph().removeAllChildren();
					Run newRun1 = new Run(doc, addInsVeh.getDescription());
					newRun1.getFont().setName("Arial");
					newRun1.getFont().setSize(8);
					newRun1.getFont().setBold(true);
					newVehicleRow.getCells().get(0).getFirstParagraph().appendChild(newRun1);

					// Veh_VIN
					newVehicleRow.getCells().get(1).getFirstParagraph().removeAllChildren();
					Run newRun2 = new Run(doc, addInsVeh.getSerialNum());
					newRun2.getFont().setName("Arial");
					newRun2.getFont().setSize(8);
					newRun2.getFont().setBold(true);
					newVehicleRow.getCells().get(1).getFirstParagraph().appendChild(newRun2);

					tmpTable.insertAfter(newVehicleRow, firstVehicleRow);
					firstVehicleRow = newVehicleRow;
				}
			}

			tmpTable.removeChild(tmpTable.getRows().get(1));
			doc.insertAfter(addInsSection, refSection);
			refSection = addInsSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setAICSection(Document doc) throws Exception {
		CGLSubPolicy<Risk> subPolicyCGL = (CGLSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		Section tmpSection = aicSec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyCGL.getAdditionalInsureds()) {
			Section addInsSection = (Section) tmpSection.deepClone(true);

			// Set Additional Insured
			//Paragraph 2
			addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(2).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			//Paragraph 3
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			//Paragraph 5
			addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(5).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			doc.insertAfter(addInsSection, refSection);
			refSection = addInsSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setAICCSection(Document doc) throws Exception {
		CargoSubPolicy<Risk> subPolicyCGO = (CargoSubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
		Section tmpSection = aiccSec;
		Section refSection = tmpSection;
		for (AdditionalInsured addIns : subPolicyCGO.getAdditionalInsureds()) {
			Section addInsSection = (Section) tmpSection.deepClone(true);

			// Set Additional Insured
			//Paragraph 3
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(3).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			//Paragraph 4
			addInsSection.getBody().getParagraphs().get(4).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(4).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(4).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(4).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(4).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			//Paragraph 6
			addInsSection.getBody().getParagraphs().get(6).getRange().replace("{{Add_Ins_Name}}", addIns.getAdditionalInsuredName());
			addInsSection.getBody().getParagraphs().get(6).getRange().replace("{{Add_Ins_Addr_Street}}", addIns.getAdditionalInsuredAddress().getAddressLine1());
			addInsSection.getBody().getParagraphs().get(6).getRange().replace("{{Add_Ins_Addr_City}}", addIns.getAdditionalInsuredAddress().getCity());
			addInsSection.getBody().getParagraphs().get(6).getRange().replace("{{Add_Ins_Addr_ProvState}}", addIns.getAdditionalInsuredAddress().getProvState());
			addInsSection.getBody().getParagraphs().get(6).getRange().replace("{{Add_Ins_Addr_PC}}", addIns.getAdditionalInsuredAddress().getPostalZip());

			doc.insertAfter(addInsSection, refSection);
			refSection = addInsSection;
		}
		doc.removeChild(tmpSection);
	}

	protected void setEON109Section(Document doc) throws Exception {
		Section tmpSection = eon109Sec;
		Section refSection = tmpSection;
		for (Endorsement end : eon109Ends) {
			Section eon109Section = (Section) tmpSection.deepClone(true);
			String lessorName = "";
			String lessorAddr = "";
			List<String> descs = new ArrayList<String>();
			List<String> vins = new ArrayList<String>();

			if (end.getDataExtension() != null) {
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("EON109_Lessor_Name")
								&& extData.getColumnValue() != null) {
							lessorName = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_Lessor_Addr")
								&& extData.getColumnValue() != null) {
							lessorAddr = extData.getColumnValue();
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_Veh_Desc")
								&& extData.getColumnValue() != null) {
							descs.add(extData.getColumnValue());
						} else if (extData.getColumnId().equalsIgnoreCase("EON109_VIN")
								&& extData.getColumnValue() != null) {
							vins.add(extData.getColumnValue());
						}
					}
				}
			}

			// Set name and address
			eon109Section.getBody().getParagraphs().get(1).getRange().replace("{{EON109_Lessor_Name}}", lessorName);
			eon109Section.getBody().getParagraphs().get(1).getRange().replace("{{EON109_Lessor_Addr}}", lessorAddr);

			Table table = eon109Section.getBody().getTables().get(1);
			Row firstRow = table.getRows().get(1);
			for (int i = 0; i < descs.size(); i++) {
				Row newRow = (Row) firstRow.deepClone(true);

				// Vehicle Desc
				newRow.getCells().get(0).getFirstParagraph().removeAllChildren();
				Run newRun0 = new Run(doc, descs.get(i));
				newRun0.getFont().setName("Arial");
				newRun0.getFont().setSize(8);
				newRun0.getFont().setBold(true);
				newRow.getCells().get(0).getFirstParagraph().appendChild(newRun0);

				// VIN
				newRow.getCells().get(1).getFirstParagraph().removeAllChildren();
				Run newRun1 = new Run(doc, vins.get(i));
				newRun1.getFont().setName("Arial");
				newRun1.getFont().setSize(8);
				newRun1.getFont().setBold(true);
				newRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

				table.insertAfter(newRow, firstRow);
				firstRow = newRow;
			}
			table.removeChild(table.getRows().get(1));
			doc.insertAfter(eon109Section, refSection);
			refSection = eon109Section;
		}
		doc.removeChild(tmpSection);
	}

	protected String orderedEndList(List<Endorsement> endList, IGenericLookupConfig lookupConfig) {
		StringBuilder endListSB = new StringBuilder();
		HashMap<Integer, String> endMap = new HashMap<Integer, String>();
		for (Endorsement end : endList) {
			LookupTableItem lookupEndorsementCodeItem = lookupConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes)
					.stream().filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
					.orElse(null);

			String endCode;
			if (lookupEndorsementCodeItem != null) {
				endCode = lookupEndorsementCodeItem.getItemValue();
			} else {
				endCode = end.getEndorsementCd();
			}

			switch (end.getEndorsementCd()) {
			case "OPCF2":
				endMap.put(0, endCode);
				break;
			case "OPCF5":
				endMap.put(1, endCode);
				break;
			case "OPCF8":
				endMap.put(2, endCode);
				break;
			case "OPCF9":
				endMap.put(3, endCode);
				break;
			case "OPCF13C":
				endMap.put(4, endCode);
				break;
			case "OPCF19":
				endMap.put(5, endCode);
				break;
			case "OPCF20":
				endMap.put(6, endCode);
				break;
			case "OPCF21A":
				endMap.put(7, endCode);
				break;
			case "OPCF21B":
				endMap.put(8, endCode);
				break;
			case "OPCF23A":
				endMap.put(9, endCode);
				break;
			case "OPCF25A":
				endMap.put(10, endCode);
				break;
			case "OPCF27":
				endMap.put(11, endCode);
				break;
			case "OPCF27B":
				endMap.put(12, endCode);
				break;
			case "OPCF28":
				endMap.put(13, endCode);
				break;
			case "OPCF28A":
				endMap.put(14, endCode);
				break;
			case "OPCF30":
				endMap.put(15, endCode);
				break;
			case "OPCF31":
				endMap.put(16, endCode);
				break;
			case "OPCF38":
				endMap.put(17, endCode);
				break;
			case "OPCF40":
				endMap.put(18, endCode);
				break;
			case "OPCF43":
				endMap.put(19, endCode);
				break;
			case "OPCF44R":
				if (!opcf44RPrintLimit.equals("")) {
					endCode = endCode + " " + opcf44RPrintLimit;
				}
				endMap.put(20, endCode);
				break;
			case "OPCF47":
				endMap.put(21, endCode);
				break;
			case "OPCF48":
				endMap.put(22, endCode);
				break;
			}

		}

		for (int i = 0; i <= 22; i++) {
			if (endMap.get(i) != null) {
				if (endListSB.length() > 0) {
					endListSB.append(", ");
				}
				endListSB.append(endMap.get(i));
			}
		}

		return endListSB.toString();
	}

	protected void initializeSections(Document doc) {
	}

	protected void hideSections(Document doc) {
	}

	protected void endorsementNumeration(Document doc) {
		Integer sectionNo = 0;
		boolean isEndSection = false;
		for (Section sec : doc.getSections()) {

			if (sec.equals(endorsementSection)) {
				isEndSection = true;
				continue;
			}

			if (isEndSection) {
				sectionNo++;
				Table table = sec.getBody().getTables().get(0);
				Row row = table.getRows().get(1);
				Cell cell = row.getCells().get(4);
				String endCode = cell.getFirstParagraph().getText();
				endCode = endCode.replaceAll("[^a-zA-Z0-9]", "");

				for (Endorsement end : numEnds) {
					if (endCode.equalsIgnoreCase(end.getEndorsementCd())) {
						end.setEndorsementNumber(sectionNo);
					}
				}

				cell.getFirstParagraph().removeAllChildren();
				Run run = new Run(doc, sectionNo.toString());
				run.getFont().setName("Arial");
				run.getFont().setSize(8);
				run.getFont().setBold(true);
				cell.getFirstParagraph().appendChild(run);
			}
		}

		/*for (Endorsement end : numEnds) {
			System.out.println(end.getEndorsementCd() + " = " + end.getEndorsementNumber());
		}*/

		this.maxendno = numberFormat(sectionNo);
		this.maxendno_item6 = sectionNo > 0 ? "1 to " + numberFormat(sectionNo) : "N/A";
	}

}
