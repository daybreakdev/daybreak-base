package com.echelon.services.document;

import java.io.File;

import com.aspose.words.Document;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;

public class PolicyCancelTowing extends PolicyCancel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9023133614913048796L;
	
	public PolicyCancelTowing(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();

		this.policyDocument = policyDocument;
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.master_broker_no = insurancePolicy.getPolicyProducer().getProducerId();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.polNo = insurancePolicy.getBasePolicyNum();
		this.effectiveDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(),
				"MM/dd/yyyy");
		this.expiryDate = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm()
				.getTermExpDate().toLocalDate(), "MM/dd/yyyy");
		
		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			
			if (!isValidEndorsement(end)) {
				continue;
			}
			
			if (end.getEndorsementCd().equalsIgnoreCase("EON121")) {
				this.showEON121Section = true;
				this.numEnds.add(end);
				Double prem = policyDocument.getPolicyTransaction().getTransactionPremium().getNetPremiumChange();
				Double orgPrem = policyDocument.getPolicyTransaction().getTransactionPremium().getOriginalPremium();
				this.eon121_prem = premFormat(prem);
				this.eon121_returnPrem = prem < 0 ? numberFormat(prem * -1) : numberFormat(prem);
				this.eon121_origPrem = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getTermPremium());
				this.cancellation_type = policyDocument.getPolicyTransaction().getPolicyVersion().getCancellationMethodCd();
				this.canc_date = formatDate(policyDocument.getPolicyTransaction().getPolicyVersion().getVersionDate(), "MM/dd/yyyy");
				if (policyDocument.getPolicyTransaction().getPolicyVersion().getCancellationMethodCd()
						.equalsIgnoreCase("Short")) {
					this.eon121_earnedPrem = numberFormat(
							policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
				} else {
					this.eon121_earnedPrem = numberFormat(orgPrem + prem);
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON122")) {
				this.showEON122Section = true;
				this.numEnds.add(end);
			}
		}
	}
	
	@Override
	public String getTemplatePath() {
		return "templates/PolicyCancel_tmp.docx";
	}
	
	@Override
	public void create(String storageKey) throws Exception {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if (attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		initializeSections(doc);
		
		if (this.showEON121Section) {
			setEON121Paragraphs(doc);
		}
		
		hideSections(doc);
		endorsementNumeration(doc);
		initializeFields();
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		doc.save(attachPath + File.separator + storageKey);
	}

	@Override
	public void initializeFields() {
		fieldNames = new String[] { "BrokerName", "MasterBrokerNumber", "Insured_Name", "Pol_No", "Effective_Date",
				"Expiry_Date", "EON121_Prem", "EON121_ReturnPrem", "EON121_OrigPrem", "Cancellation_Type", "Canc_Date" };
		fieldValues = new Object[] { brokerName, master_broker_no, insuredName, polNo, effectiveDate, expiryDate,
				eon121_prem, eon121_returnPrem, eon121_origPrem, cancellation_type, canc_date };
	}
	
	@Override
	protected void initializeSections(Document doc) {
		eon121Sec = doc.getSections().get(0);
		eon122Sec = doc.getSections().get(1);
	}
	
	@Override
	protected void hideSections(Document doc) {
		if (!showEON121Section) {
			doc.removeChild(eon121Sec);
		}

		if (!showEON122Section) {
			doc.removeChild(eon122Sec);
		}
	}

}
