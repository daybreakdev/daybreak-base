package com.echelon.services.document;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.aspose.words.Cell;
import com.aspose.words.Document;
import com.aspose.words.Row;
import com.aspose.words.RowCollection;
import com.aspose.words.Run;
import com.aspose.words.Table;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyNotes;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;

public class QuoteLetterNonFleet extends EchelonDocument implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2719765784248525952L;
	private static final int TABLE_INDEX = 22;
	private static final LocalDateTime JAN2021 = LocalDateTime.of(2021, 1, 1, 0, 0);
	private String[] fieldNames;
	private Object[] fieldValues;
	private String currDate = "";
	private String referenceNo = "";
	private String brokerName = "";
	private String brokerContact = "";
	private String insuredName = "";
	private String insuredAddressStreet = "";
	private String insuredAddressCity = "";
	private String insuredAddressPostalCode = "";
	private String effectiveDate = "";
	private String expiryDate = "";
	private String lbLimit = "";
	private String totAutoPrem = "";
	private String end_code = "";
	private String end_desc = "";
	private String opcf2_pers = "";
	private String opcf8_ded = "N/A";
	private String opcf19_lim = "";
	private String opcf20_lim = "";
	private String opcf20H_lim = "";
	private String opcf27_pers = "";
	private String opcf27_lim = "";
	private String opcf27_ded = "";
	private String opcf27b_vehtype = "";
	private String opcf27b_lim = "";
	private String opcf27b_ded = "";
	private String opcf28_pers = "";
	private String opcf28_lim = "";
	private String opcf28_ded = "";
	private String opcf28a_driver = "";
	private String opcf31_desc = "";
	private String opcf31_lim = "";
	private String opcf38_desc = "";
	private String opcf38_lim = "";
	private String businessType = "";
	private String tll_limit = "";
	private String cgl_lb_limit = "";
	private String cgl_ded, loc_street = "";
	private String loc_city = "";
	private String loc_postal_code = "";
	private String ebe_lim = "";
	private String ebe_ded = "";
	private String totCGLPrem = "";
	private String spf6_lb_limit = "";
	private String cargo_limit = "";
	private String cargo_ded = "";
	private String cargo_prem = ""; 
	private String contcargo_lim = "N/A";
	private String contcargo_prem = "N/A";
	private String loc_id;
	private String maxno_veh = "";
	private String gar_lb_limit = "";
	private String gar_dcpd_ded = "";
	private String gar_511_Ded = "N/A";
	private String subSectionA = "";
	private String subSectionB = "";
	private String subSectionC = "";
	private String subSectionD = "";
	private String sbLimA = "";
	private String sbLimB = "";
	private String sbLimC = "";
	private String sbLimD = "";
	private String sbDedA = "";
	private String sbDedB = "";
	private String sbDedC = "";
	private String sbDedD = "";
	private String gar_6_1_limit = "N/A";
	private String gar_6_1_ded = "N/A";
	private String loc_6_4_limit = "";
	private String loc_6_4_ded = "";
	private String loc_6_4_id = "";
	private String totGARPrem = "";
	private String perocc_ded = "N/A";
	private String totPolPrem = "";
	private String comm = "";
	private String uw_notes = "";
	private String date_prep = "";
	private String prep_by = "";
	private String totMTCPrem = "";
	private String ded_ref = "";
	private boolean proRata = false;
	private boolean fifthyFifthy = false;
	private boolean quarterly = false;
	private boolean every6m = false;
	private boolean annual = false;
	private boolean opcf20_exists = false;
	private boolean showCGL = false;
	private boolean showEBE = false;
	private boolean showCGOExtOfCov = false;
	private boolean showCDEC = false;
	private boolean showSPF6TPL = false;
	private boolean showCGO = false;
	private boolean showGAR = false;
	private boolean showOPCF2 = false;
	private boolean showOPCF5 = true;
	private boolean showOPCF8 = false;
	private boolean showOPCF9 = false;
	private boolean showOPCF13C = false;
	private boolean showOPCF19 = false;
	private boolean showOPCF20 = false;
	private boolean showOPCF20H = false;
	private boolean showEON20G = false;
	private boolean showOPCF21A = false;
	private boolean showOPCF21B = false;
	private boolean showOPCF23A = true;
	private boolean showOPCF27 = false;
	private boolean showOPCF27B = false;
	private boolean showOPCF28 = false;
	private boolean showOPCF28A = false;
	private boolean showOPCF30 = false;
	private boolean showOPCF31 = false;
	private boolean showOPCF38 = false;
	private boolean showOPCF40 = false;
	private boolean showOPCF43_43A = false;
	private boolean showOPCF44R = false;
	private boolean showOPCF47 = false;
	private boolean showOPCF48 = false;
	private boolean showOPCF = false;
	private boolean showGARSection5 = false;
	private boolean showGARSection61 = false;
	private boolean showGARSection64 = false;
	private boolean isLaterThanJan2021 = false;
	private String cgl_location = "";
	private String cgl_end_desc = "";
	private String gar_location = "";
	private String gar_end_desc = "";
	private String other_end_desc = "";
	private String eon111_code = "";
	private String eon111_desc = "";
	private LinkedHashSet<Endorsement> opcf27BEnds = new LinkedHashSet<Endorsement>();
	private LinkedHashSet<HashMap<String, String>> section5ExtraLocations = new LinkedHashSet<HashMap<String, String>>();
	

	public QuoteLetterNonFleet(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage)insurancePolicy;
		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		
		//Sub Policies
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		for(SubPolicy<Risk> subPolicy : policyDocument.getPolicyTransaction().getSubPolicies()) {
			if(subPolicy.getSubPolicyTypeCd().equalsIgnoreCase("AUTO")) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			} else if(subPolicy.getSubPolicyTypeCd().equalsIgnoreCase("CGL")) {
				subPolicyCGL = (CGLSubPolicy<Risk>) subPolicy;
			} else if(subPolicy.getSubPolicyTypeCd().equalsIgnoreCase("CGO")) {
				subPolicyCGO = (CargoSubPolicy<Risk>) subPolicy;
			} else if(subPolicy.getSubPolicyTypeCd().equalsIgnoreCase("GAR")) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPolicy;
			}
		}
		
		
		this.policyDocument = policyDocument;
		this.currDate = formatDate(policyDocument.getUpdatedDate().toLocalDate(), "MMMM dd, yyyy");
		this.referenceNo = insurancePolicy.getBasePolicyNum() != null && !insurancePolicy.getBasePolicyNum().isEmpty() ? insurancePolicy.getBasePolicyNum() : insurancePolicy.getQuoteNum();
		this.brokerName = insurancePolicy.getPolicyProducer().getLegalName();
		this.brokerContact = insurancePolicy.getProducerContactName();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressPostalCode = insurancePolicy.getInsuredAddress().getPostalZip();
		this.effectiveDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate(),
				"M/dd/yyyy");
		this.expiryDate = formatDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate().toLocalDate(),
				"M/dd/yyyy");
		this.isLaterThanJan2021 = policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate().compareTo(JAN2021) >= 0;
		this.lbLimit = numberFormat(autoPackage.getLiabilityLimit());
		this.totAutoPrem = "$" + numberFormat(subPolicyAuto.getSubPolicyPremium().getWrittenPremium());
		
		if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21B5")) {
			this.fifthyFifthy = true;
			this.annual = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BQ")) {
			this.proRata = true;
			this.quarterly = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BA")) {
			this.proRata = true;
			this.annual = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("21BS")) {
			this.proRata = true;
			this.every6m = true;
		} else if (subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
			this.ded_ref = "See Auto's below for the applicable deductible(s)";
		}
		
		// Policy
		for (Endorsement end : policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase("EON110")) {
				this.perocc_ded = numberFormat(end.getDeductible1amount());
			}
		}
		
		LinkedHashSet<String> endCodes = new LinkedHashSet<String>();
		LinkedHashSet<String> endDescs = new LinkedHashSet<String>();
		LinkedHashSet<String> otherEndDesc = new LinkedHashSet<String>();
		
		// AUTO Sub Policy
		List<Endorsement> autoEndorsements = new ArrayList<Endorsement>();
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		
		for(Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			autoEndorsements.add(end);
		}
		
		for(SpecialtyVehicleRisk vehicle : vehicles) {
			Set<Endorsement> vehEndorsements = vehicle.getRiskEndorsements();
			Iterator<Endorsement> itVehEnd = vehEndorsements.iterator();
			while(itVehEnd.hasNext()) {
				Endorsement end = itVehEnd.next();
				autoEndorsements.add(end);
			}
		}
		
		StringBuilder opcf2PersSB = new StringBuilder();
		StringBuilder opcf27PersSB = new StringBuilder();
		StringBuilder opcf28ADriverSB = new StringBuilder();
		StringBuilder opcf31DescSB = new StringBuilder();
		LinkedHashSet<String> opcf27BVehTypes = new LinkedHashSet<String>();
		autoEndorsements.sort(Comparator.comparing(Endorsement::getEndorsementsk));
		
		for(Endorsement end : autoEndorsements) {
			
			if (end.getEndorsementCd().equalsIgnoreCase("OPCF2")) {
				this.showOPCF2 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF2_Pers")) {
							if(opcf2PersSB.length() > 0) {
								opcf2PersSB.append(", ");
							}
							opcf2PersSB.append(extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF5")) {
				this.showOPCF5 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF8")) {
				this.showOPCF8 = true;
				if (this.opcf8_ded.equals("N/A")) {
					this.opcf8_ded = numberFormat(end.getDeductible1amount());
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF9")) {
				this.showOPCF9 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF13C")) {
				this.showOPCF13C = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF19")) {
				this.showOPCF19 = true;
				this.opcf19_lim = numberFormat(end.getLimit1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF20")) {
				this.opcf20_exists = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21A")) {
				this.showOPCF21A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF21B")) {
				this.showOPCF21B = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF23A")) {
				this.showOPCF23A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27")) {
				this.showOPCF27 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27_Pers") && extData.getColumnValue() != null
								&& !extData.getColumnValue().equalsIgnoreCase("")) {
							if (opcf27PersSB.length() > 0) {
								opcf27PersSB.append(", ");
							}
							opcf27PersSB.append(extData.getColumnValue());
						}
					}
				}
				this.opcf27_lim = numberFormat(end.getLimit1amount());
				this.opcf27_ded = numberFormat(end.getDeductible1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) {
				this.showOPCF27B = true;

				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")
								&& extData.getColumnValue() != null) {
							if (!opcf27BVehTypes.contains(extData.getColumnValue())) {
								opcf27BVehTypes.add(extData.getColumnValue());
								this.opcf27BEnds.add(end);
							}
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28")) {
				this.showOPCF28 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF28_Pers")) {
							this.opcf28_pers = extData.getColumnValue();
						}
					}
				}
				this.opcf28_lim = numberFormat(end.getLimit1amount());
				this.opcf28_ded = numberFormat(end.getDeductible1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF28A")) {
				this.showOPCF28A = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF28A_Driver")) {
							if(opcf28ADriverSB.length() > 0) {
								opcf28ADriverSB.append(", ");
							}
							opcf28ADriverSB.append(extData.getColumnValue());
						}
					}
				}
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF30")) {
				this.showOPCF30 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF31")) {
				this.showOPCF31 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF31_VehDesc")) {
							if(opcf31DescSB.length() > 0) {
								opcf31DescSB.append(", ");
							}
							opcf31DescSB.append(extData.getColumnValue());
						}
					}
				}
				this.opcf31_lim = numberFormat(end.getLimit1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF38")) {
				this.showOPCF38 = true;
				for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
					for (ExtensionData extData : extType.getExtensionDatas()) {
						if (extData.getColumnId().equalsIgnoreCase("OPCF38_EqDesc")) {
							this.opcf38_desc = extData.getColumnValue();
						}
					}
				}
				this.opcf38_lim = numberFormat(end.getLimit1amount());
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF40")) {
				this.showOPCF40 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF43") || end.getEndorsementCd().equalsIgnoreCase("OPCF43A")) {
				this.showOPCF43_43A = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) {
				this.showOPCF44R = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF47")) {
				this.showOPCF47 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("OPCF48")) {
				this.showOPCF48 = true;
			} else if (end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
				this.showEON20G = true;
			}
		}
		
		this.opcf2_pers = opcf2PersSB.toString();
		this.opcf27_pers = opcf27PersSB.toString();
		this.opcf28a_driver = opcf28ADriverSB.toString();
		this.opcf31_desc = opcf31DescSB.toString();
		StringBuilder endCodeSB = new StringBuilder();
		StringBuilder endDescSB = new StringBuilder();

		Iterator<String> endCodeIt = endCodes.iterator();
		while (endCodeIt.hasNext()) {
			if (endCodeSB.length() > 0) {
				endCodeSB.append("\n");
			}

			endCodeSB.append(endCodeIt.next());
		}

		Iterator<String> endDescIt = endDescs.iterator();
		while (endDescIt.hasNext()) {
			if (endDescSB.length() > 0) {
				endDescSB.append("\n");
			}

			endDescSB.append(endDescIt.next());
		}

		this.end_code = endCodeSB.toString();
		this.end_desc = endDescSB.toString();
		
		Set<Endorsement> polEnds = policyDocument.getPolicyTransaction().getCoverageDetail().getEndorsements();
		Iterator<Endorsement> polEndsIt = polEnds.iterator();
		while(polEndsIt.hasNext()) {
			Endorsement end = polEndsIt.next();
			
			if (end.getEndorsementCd().equalsIgnoreCase("EON111")) {
				this.eon111_code = "Profit Sharing Endorsement:";
				this.eon111_desc = "Return premium of 5% if the Loss Ratio is equal to or less than 25% (conditional on the renewal with Echelon)";
			}
		}
		
		StringBuilder otherEndDescSB = new StringBuilder();
		
		Iterator<String> itOtherEndDesc = otherEndDesc.iterator();
		while(itOtherEndDesc.hasNext()) {
			if(otherEndDescSB.length() > 0) {
				otherEndDescSB.append("\n");
			}
			
			otherEndDescSB.append(itOtherEndDesc.next());
		}
		
		this.other_end_desc = otherEndDescSB.toString();
		

		
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			LookupTableItem lookupBusinessDescItem = lookupConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_BusinessDescriptions).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(
							insurancePolicy.getPolicyCustomer().getCommercialCustomer().getBusinessDescription()))
					.findFirst().orElse(null);
			this.businessType = lookupBusinessDescItem.getItemValue();
		}		
		
		if (subPolicyCGL != null) {
			this.showCGL = true;
			LinkedHashSet<String> cglEndDesc = new LinkedHashSet<String>();
			for (Coverage cove : subPolicyCGL.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("TLL")) {
					this.tll_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("SPF6TPL")) {
					this.showSPF6TPL = true;
					this.spf6_lb_limit = numberFormat(cove.getLimit1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("EBE")) {
					this.showEBE = true;
					this.ebe_lim = numberFormat(cove.getLimit1());
					this.ebe_ded = numberFormat(cove.getDeductible1());
				}
			}
			
			for (Endorsement end : subPolicyCGL.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().equalsIgnoreCase("GLEE")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					LookupTableItem lookupEndorsementDescItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementDescriptions).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					String description = null;

					if (lookupEndorsementCodeItem != null && lookupEndorsementDescItem != null) {
						String endCode = lookupEndorsementCodeItem.getItemValue();
						String endDesc = lookupEndorsementDescItem.getItemValue();
						description = endCode + " " + endDesc;
					} else {
						description = end.getDescription();
					}

					cglEndDesc.add(description);
				}
			}
			
			this.cgl_lb_limit = numberFormat(subPolicyCGL.getCglLimit());
			this.cgl_ded = numberFormat(subPolicyCGL.getCglDeductible());
			this.totCGLPrem = numberFormat(
					subPolicyCGL.getSubPolicyPremium().getWrittenPremium());
			
			StringBuilder locationSB = new StringBuilder();
			Set<PolicyLocation> policyLocations = subPolicyCGL.getCglLocations().stream().map(CGLLocation::getPolicyLocation).collect(Collectors.toSet());
			//for (PolicyLocation pl : subPolicyCGL.getCglLocations().stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toSet())) {
			for (PolicyLocation pl : policyLocations.stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toSet())) {
				if (locationSB.length() > 0) {
					locationSB.append("\n");
				}

				locationSB.append(pl.getLocationAddress().getAddressLine1());
				locationSB.append(", ");
				locationSB.append(pl.getLocationAddress().getCity());
				locationSB.append(", Ontario ");
				locationSB.append(pl.getLocationAddress().getPostalZip());
			}
			this.cgl_location = locationSB.length() == 0 ? "No other locations" : locationSB.toString();
			
			StringBuilder cglEndDescSB = new StringBuilder();
			Iterator<String> itCGLEndDesc = cglEndDesc.iterator();
			while (itCGLEndDesc.hasNext()) {
				if (cglEndDescSB.length() > 0) {
					cglEndDescSB.append("\n");
				}
				cglEndDescSB.append(itCGLEndDesc.next());
			}
			
			this.cgl_end_desc = cglEndDescSB.toString();
		}
		
		if (subPolicyCGO != null) {
			this.showCGO = true;
			for (Coverage cove : subPolicyCGO.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("MTCC")) {
					this.showCGOExtOfCov = true;
					this.cargo_limit = numberFormat(cove.getLimit1());
					this.cargo_ded = numberFormat(cove.getDeductible1());
				} 
			}
			this.totMTCPrem = numberFormat(subPolicyCGO.getSubPolicyPremium().getWrittenPremium());
			
			for (Endorsement end : subPolicyCGO.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().startsWith("CDEC")) {
					this.showCDEC = true;
				} else if (end.getEndorsementCd().equalsIgnoreCase("CTC")) {
					this.cargo_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
					this.contcargo_lim = numberFormat(end.getLimit1amount());
					this.contcargo_prem = numberFormat(end.getEndorsementPremium().getWrittenPremium());
				}
			}
		}
		
		if (subPolicyGAR != null) {
			this.showGAR = true;
			LinkedHashSet<String> garEndDesc = new LinkedHashSet<String>();

			for (Coverage cove : subPolicyGAR.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("TPL")) {
					this.gar_lb_limit = numberFormat(autoPackage.getLiabilityLimit());
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					this.gar_dcpd_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL61")) {
					this.showGARSection61 = true;
					this.gar_6_1_limit = numberFormat(cove.getLimit1());
					this.gar_6_1_ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL511")) {
					this.showGARSection5 = true;
					this.gar_511_Ded = numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM512")) {
					this.showGARSection5 = true;
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP513")) {
					this.showGARSection5 = true;
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP514")) {
					this.showGARSection5 = true;
				}
			}
			
			for (Endorsement end : subPolicyGAR.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementCd().startsWith("OEF")) {

					LookupTableItem lookupEndorsementCodeItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementCodes).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					LookupTableItem lookupEndorsementDescItem = lookupConfig
							.getConfigLookup(ConfigConstants.LOOKUPTABLE_EndorsementDescriptions).stream()
							.filter(o -> o.getItemKey().equalsIgnoreCase(end.getEndorsementCd())).findFirst()
							.orElse(null);

					String description = null;

					if (lookupEndorsementCodeItem != null && lookupEndorsementDescItem != null) {
						String endCode = lookupEndorsementCodeItem.getItemValue();
						String endDesc = lookupEndorsementDescItem.getItemValue();
						description = endCode + " " + endDesc;
					} else {
						description = end.getDescription();
					}

					garEndDesc.add(description);
				}
			}

			StringBuilder locationSB = new StringBuilder();
			StringBuilder locatinSP64Id = new StringBuilder();
			StringBuilder locatinSP64Limit = new StringBuilder();
			StringBuilder locatinSP64Ded = new StringBuilder();
			List<PolicyLocation> policyLocations = getPolicyLocations();
			
			if(subPolicyGAR.getSubPolicyNamedInsured() != null && !subPolicyGAR.getSubPolicyNamedInsured().isEmpty()) {
				locationSB.append(subPolicyGAR.getSubPolicyNamedInsured());
			}
			
			for (PolicyLocation pl : policyLocations) {
				Coverage coveCL511 = pl.getCoverageDetail().getCoverageByCode("CL511");
				Coverage coveCM512 = pl.getCoverageDetail().getCoverageByCode("CM512");
				Coverage coveSP513 = pl.getCoverageDetail().getCoverageByCode("SP513");
				Coverage coveSP514 = pl.getCoverageDetail().getCoverageByCode("SP514");
				Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
				
				if (coveCM512 != null || coveSP513 != null || coveSP514 != null || coveSP64 != null) {
					if (locationSB.length() > 0) {
						locationSB.append("\n");
					}
					locationSB.append(pl.getLocationId());
					locationSB.append(" ");
					locationSB.append(pl.getLocationAddress().getAddressLine1());
					locationSB.append(", ");
					locationSB.append(pl.getLocationAddress().getCity());
					locationSB.append(", Ontario ");
					locationSB.append(pl.getLocationAddress().getPostalZip());
					locationSB.append("      Maximum # of vehicles: ");
					locationSB.append(numberFormat(pl.getNumVehicles()));
				}

				if (coveSP64 != null) {
					this.showGARSection64 = true;
					if (locatinSP64Id.length() > 0 || locatinSP64Limit.length() > 0 || locatinSP64Ded.length() > 0) {
						locatinSP64Id.append("\n");
						locatinSP64Limit.append("\n");
						locatinSP64Ded.append("\n");
					}

					locatinSP64Id.append("Location: " + pl.getLocationId());
					locatinSP64Limit.append("$" + numberFormat(coveSP64.getLimit1()));
					locatinSP64Ded.append("$" + numberFormat(coveSP64.getDeductible1()));
				}
				
				if (pl.getLocationId().equalsIgnoreCase("A")) {
					if (coveCL511 != null) {
						this.subSectionA = coveCL511.getDescription().substring(0, 5);
					} else if (coveCM512 != null) {
						this.subSectionA = coveCM512.getDescription().substring(0, 5);
						this.sbLimA = "$" + numberFormat(coveCM512.getLimit1());
						this.sbDedA = "$" + numberFormat(coveCM512.getDeductible1());
					} else if (coveSP513 != null) {
						this.subSectionA = coveSP513.getDescription().substring(0, 5);
						this.sbLimA = "$" + numberFormat(coveSP513.getLimit1());
						this.sbDedA = "$" + numberFormat(coveSP513.getDeductible1());
					} else if (coveSP514 != null) {
						this.subSectionA = coveSP514.getDescription().substring(0, 5);
						this.sbLimA = "$" + numberFormat(coveSP514.getLimit1());
						this.sbDedA = "$" + numberFormat(coveSP514.getDeductible1());
					}
					if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
						this.sbLimA = "$N/A";
						this.sbDedA = "$N/A";
					}
				} else if (pl.getLocationId().equalsIgnoreCase("B")) {
					if (coveCL511 != null) {
						this.subSectionB = coveCL511.getDescription().substring(0, 5);
					} else if (coveCM512 != null) {
						this.subSectionB = coveCM512.getDescription().substring(0, 5);
						this.sbLimB = "$" + numberFormat(coveCM512.getLimit1());
						this.sbDedB = "$" + numberFormat(coveCM512.getDeductible1());
					} else if (coveSP513 != null) {
						this.subSectionB = coveSP513.getDescription().substring(0, 5);
						this.sbLimB = "$" + numberFormat(coveSP513.getLimit1());
						this.sbDedB = "$" + numberFormat(coveSP513.getDeductible1());
					} else if (coveSP514 != null) {
						this.subSectionB = coveSP514.getDescription().substring(0, 5);
						this.sbLimB = "$" + numberFormat(coveSP514.getLimit1());
						this.sbDedB = "$" + numberFormat(coveSP514.getDeductible1());
					}
					if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
						this.sbLimB = "$N/A";
						this.sbDedB = "$N/A";
					}
				} else if (pl.getLocationId().equalsIgnoreCase("C")) {
					if (coveCL511 != null) {
						this.subSectionC = coveCL511.getDescription().substring(0, 5);
					} else if (coveCM512 != null) {
						this.subSectionC = coveCM512.getDescription().substring(0, 5);
						this.sbLimC = "$" + numberFormat(coveCM512.getLimit1());
						this.sbDedC = "$" + numberFormat(coveCM512.getDeductible1());
					} else if (coveSP513 != null) {
						this.subSectionC = coveSP513.getDescription().substring(0, 5);
						this.sbLimC = "$" + numberFormat(coveSP513.getLimit1());
						this.sbDedC = "$" + numberFormat(coveSP513.getDeductible1());
					} else if (coveSP514 != null) {
						this.subSectionC = coveSP514.getDescription().substring(0, 5);
						this.sbLimC = "$" + numberFormat(coveSP514.getLimit1());
						this.sbDedC = "$" + numberFormat(coveSP514.getDeductible1());
					}
					if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
						this.sbLimC = "$N/A";
						this.sbDedC = "$N/A";
					}
				} else if (pl.getLocationId().equalsIgnoreCase("D")) {
					if (coveCL511 != null) {
						this.subSectionD = coveCL511.getDescription().substring(0, 5);
					} else if (coveCM512 != null) {
						this.subSectionD = coveCM512.getDescription().substring(0, 5);
						this.sbLimD = "$" + numberFormat(coveCM512.getLimit1());
						this.sbDedD = "$" + numberFormat(coveCM512.getDeductible1());
					} else if (coveSP513 != null) {
						this.subSectionD = coveSP513.getDescription().substring(0, 5);
						this.sbLimD = "$" + numberFormat(coveSP513.getLimit1());
						this.sbDedD = "$" + numberFormat(coveSP513.getDeductible1());
					} else if (coveSP514 != null) {
						this.subSectionD = coveSP514.getDescription().substring(0, 5);
						this.sbLimD = "$" + numberFormat(coveSP514.getLimit1());
						this.sbDedD = "$" + numberFormat(coveSP514.getDeductible1());
					}
					if (coveCM512 == null && coveSP513 == null && coveSP514 == null) {
						this.sbLimD = "$N/A";
						this.sbDedD = "$N/A";
					}
				} else if (coveCL511 != null || coveCM512 != null || coveSP513 != null || coveSP514 != null) {
					HashMap<String, String> extLoc = new HashMap<String, String>();
					extLoc.put("id", pl.getLocationId());
					if (coveCL511 != null) {
						extLoc.put("subSection", coveCL511.getDescription().substring(0, 5));
						extLoc.put("sbLimit", "$N/A");
						extLoc.put("sbDed", "$N/A");
					} else if (coveCM512 != null) {
						extLoc.put("subSection", coveCM512.getDescription().substring(0, 5));
						extLoc.put("sbLimit", "$" + numberFormat(coveCM512.getLimit1()));
						extLoc.put("sbDed", "$" + numberFormat(coveCM512.getDeductible1()));
					} else if (coveSP513 != null) {
						extLoc.put("subSection", coveSP513.getDescription().substring(0, 5));
						extLoc.put("sbLimit", "$" + numberFormat(coveSP513.getLimit1()));
						extLoc.put("sbDed", "$" + numberFormat(coveSP513.getDeductible1()));
					} else if (coveSP514 != null) {
						extLoc.put("subSection", coveSP514.getDescription().substring(0, 5));
						extLoc.put("sbLimit", "$" + numberFormat(coveSP514.getLimit1()));
						extLoc.put("sbDed", "$" + numberFormat(coveSP514.getDeductible1()));
					}
					section5ExtraLocations.add(extLoc);
				}
			}
			
			StringBuilder garEndDescSB = new StringBuilder();
			Iterator<String> itGAREndDesc = garEndDesc.iterator();
			while (itGAREndDesc.hasNext()) {
				if (garEndDescSB.length() > 0) {
					garEndDescSB.append("\n");
				}
				garEndDescSB.append(itGAREndDesc.next());
			}
			
			this.gar_location = locationSB.toString();
			this.gar_end_desc = garEndDescSB.toString();
			this.loc_6_4_id = locatinSP64Id.toString();
			this.loc_6_4_limit = locatinSP64Limit.toString();
			this.loc_6_4_ded = locatinSP64Ded.toString();
			this.totGARPrem = numberFormat(subPolicyGAR.getSubPolicyPremium().getWrittenPremium());
		}
		
		this.totPolPrem = numberFormat(policyDocument.getPolicyTransaction().getTransactionPremium().getWrittenPremium());
		this.comm = numberFormat(policyDocument.getPolicyTransaction().getPolicyVersion().getProducerCommissionRate());
		
		Set<PolicyNotes> notes = policyDocument.getPolicyTransaction().getPolicyVersion().getPolicyNotes();
		Iterator<PolicyNotes> itNotes = notes.iterator();
		StringBuilder notesSB = new StringBuilder();
		while (itNotes.hasNext()) {
			PolicyNotes pn = itNotes.next();
			if (!pn.getIsInternal()) {
				if (notesSB.length() > 0) {
					notesSB.append("\n");
				}
				notesSB.append(pn.getNoteDetails());
			}
		}
		this.uw_notes = notesSB.toString();
		if(insurancePolicy.getQuotePreparedDate() != null) {
			this.date_prep = formatDate(insurancePolicy.getQuotePreparedDate().toLocalDate(), "MMMM dd, yyyy"); 
		}
		this.prep_by = insurancePolicy.getQuotePreparedBy();
	}
	
	public String getTemplatePath() {
		return "templates/QuoteLetterNonFleet_tmp.docx";
	}

	public void create(String storageKey) throws Exception {
		if (this.opcf20_exists) {
			setOPCF20Row();
		}
		initializeFields();
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if(attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		setCAIRow(doc);
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		hideSections(doc);
		if (showGARSection5) {
			setSection5ExtraLocations(doc);
		}
		doc.save(attachPath + File.separator + storageKey);
	}
	
	private void setOPCF20Row() {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			Endorsement end = vehicle.getCoverageDetail().getEndorsementByCode("OPCF20");
			if (end != null) {
				if (vehicle.getVehicleDescription().equalsIgnoreCase("LC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("PP")) {
					this.showOPCF20 = true;
					this.opcf20_lim = numberFormat(end.getLimit1amount());
				} else if (vehicle.getVehicleDescription().equalsIgnoreCase("HC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TRC")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("MTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("LTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("HTT")
						|| vehicle.getVehicleDescription().equalsIgnoreCase("TT")) {
					this.showOPCF20H = true;
					this.opcf20H_lim = numberFormat(end.getLimit1amount());
				}
			}
		}
	}

	public void initializeFields() {
		fieldNames = new String[] { "Date", "Reference_No", "Broker_Name", "Broker_Contact", "Insured_Name",
				"Insured_Address_Street", "Insured_Address_City", "Insured_Address_Postal_Code", "Effective_Date",
				"Expiry_Date", "LB_Limit", "TotAutoPrem", "End_Code", "End_Desc", "OPCF2_Pers", "OPCF8_Ded",
				"OPCF19_Lim", "OPCF20_Lim", "OPCF20H_Lim", "OPCF27_Pers", "OPCF27_Lim", "OPCF27_Ded", "OPCF27B_VehType",
				"OPCF27B_Lim", "OPCF27B_Ded", "OPCF28_Pers", "OPCF28_Lim", "OPCF28_Ded", "OPCF28A_Driver",
				"OPCF31_Desc", "OPCF31_Lim", "OPCF38_Desc", "OPCF38_Lim", "BusinessType", "TLL_Limit", "CGL_LB_Limit",
				"CGL_Ded", "Loc_Street", "Loc_City", "Loc_Postal_Code", "EBE_Lim", "EBE_Ded", "TotCGLPrem",
				"SPF6_LB_Limit", "Cargo_Limit", "Cargo_Ded", "Cargo_Prem", "Loc_ID", "MaxNo_Veh", "GAR_LB_Limit",
				"GAR_DCPD_Ded", "511_Ded", "SubsectionA", "SubsectionB", "SubsectionC", "SubsectionD", "SbLimA",
				"SbLimB", "SbLimC", "SbLimD", "SbDedA", "SbDedB", "SbDedC", "SbDedD", "GAR_6_1_Limit", "GAR_6_1_Ded",
				"Loc_6_4_Limit", "Loc_6_4_Ded", "Loc_6_4_Id", "TotGARPrem", "PerOcc_Ded", "TotPolPrem", "Comm",
				"UW_Notes", "Date_Prep", "Prep_By", "TotMTCPrem", "ProRata", "FifthyFifthy", "Quarterly", "Every6m",
				"Annual", "CGL_Location", "CGL_End_Desc", "GAR_Location", "GAR_End_Desc", "Other_End_Desc",
				"Eon111_Code", "Eon111_Desc", "ContCargo_Limit", "ContCargo_Prem", "Ded_Ref" };
		fieldValues = new Object[] { currDate, referenceNo, brokerName, brokerContact, insuredName,
				insuredAddressStreet, insuredAddressCity, insuredAddressPostalCode, effectiveDate, expiryDate, lbLimit,
				totAutoPrem, end_code, end_desc, opcf2_pers, opcf8_ded, opcf19_lim, opcf20_lim, opcf20H_lim,
				opcf27_pers, opcf27_lim, opcf27_ded, opcf27b_vehtype, opcf27b_lim, opcf27b_ded, opcf28_pers, opcf28_lim,
				opcf28_ded, opcf28a_driver, opcf31_desc, opcf31_lim, opcf38_desc, opcf38_lim, businessType, tll_limit,
				cgl_lb_limit, cgl_ded, loc_street, loc_city, loc_postal_code, ebe_lim, ebe_ded, totCGLPrem,
				spf6_lb_limit, cargo_limit, cargo_ded, cargo_prem, loc_id, maxno_veh, gar_lb_limit, gar_dcpd_ded,
				gar_511_Ded, subSectionA, subSectionB, subSectionC, subSectionD, sbLimA, sbLimB, sbLimC, sbLimD, sbDedA,
				sbDedB, sbDedC, sbDedD, gar_6_1_limit, gar_6_1_ded, loc_6_4_limit, loc_6_4_ded, loc_6_4_id, totGARPrem,
				perocc_ded, totPolPrem, comm, uw_notes, date_prep, prep_by, totMTCPrem, proRata, fifthyFifthy,
				quarterly, every6m, annual, cgl_location, cgl_end_desc, gar_location, gar_end_desc, other_end_desc,
				eon111_code, eon111_desc, contcargo_lim, contcargo_prem, ded_ref };
	}

	private void setCAIRow(Document doc) throws Exception {
		// Main table
		Table mainTable = doc.getFirstSection().getBody().getTables().get(0);

		// Vehicle Row
		Row mainRow = mainTable.getRows().get(TABLE_INDEX);
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();

		//Get sections data
		LinkedHashSet<HashMap> vehickeSet = new LinkedHashSet<HashMap>();
		int autoNo = 0;
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			autoNo++;
			VehicleDriver vehicleDriver = vehicle.getVehicleDrivers().stream()
					.filter(vd -> vd.getDriverType().equalsIgnoreCase("PRINCIPAL")).findFirst().orElse(null);
			String princDriver = vehicleDriver != null ? vehicleDriver.getDriver().getFullName() : "";
			String unitYear = vehicle.getUnitYear() != null ? Integer.toString(vehicle.getUnitYear()) : "";
			String unitMake = vehicle.getUnitMake() != null ? vehicle.getUnitMake() : "";
			String vin = vehicle.getUnitSerialNumberorVin() != null ? vehicle.getUnitSerialNumberorVin() : "";
			String dcpdDed = "N/A";
			String spDed = "N/A";
			String cmDed = "N/A";
			String clDed = "N/A";
			String apDed = "N/A";
			String vehPrem = vehicle.getRiskPremium().getWrittenPremium() != null
					? premFormat(vehicle.getRiskPremium().getWrittenPremium())
					: "";
			
			for (Coverage cove : vehicle.getCoverageDetail().getCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					dcpdDed = "$" + numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP")) {
					spDed = "$" + numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM")) {
					cmDed = "$" + numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL")) {
					clDed = "$" + numberFormat(cove.getDeductible1());
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP")) {
					apDed = "$" + numberFormat(cove.getDeductible1());
				}
			}

			StringBuilder endListSB = new StringBuilder();

			for (Endorsement end : vehicle.getRiskEndorsements()) {
				if (endListSB.length() > 0) {
					endListSB.append(", ");
				}
				endListSB.append(end.getEndorsementCd());
			}

			String endList = endListSB.toString();
			HashMap<String, String> vehicleMap = new HashMap<String, String>();
			vehicleMap.put("Prin_Driver", princDriver);
			vehicleMap.put("AutoNo", numberFormat(autoNo));
			vehicleMap.put("UnitYear", unitYear);
			vehicleMap.put("UnitMake", unitMake);
			vehicleMap.put("VIN", vin);
			vehicleMap.put("DCPDDed", dcpdDed);
			vehicleMap.put("SPDed", spDed);
			vehicleMap.put("CMDed", cmDed);
			vehicleMap.put("CLDed", clDed);
			vehicleMap.put("APDed", apDed);
			vehicleMap.put("EndList", endList);
			vehicleMap.put("VehPrem", vehPrem);
			vehickeSet.add(vehicleMap);
		}
		
		// Set Rows
		if (vehickeSet.size() > 0) {
			Row tmpRow = mainRow;
			Row refRow = tmpRow;
			boolean cleanAuto2 = true;
			for (HashMap<String, String> vehicleMap : vehickeSet) {
				boolean isPair = Integer.parseInt(vehicleMap.get("AutoNo")) % 2 == 0 ? true : false;
				if (!isPair) {
					cleanAuto2 = true;
					Row caiRow = (Row) tmpRow.deepClone(true);
					Table table = caiRow.getFirstCell().getTables().get(0);
					table.getRows().get(0).getCells().get(1).getFirstParagraph().getRange().replace("{{Pinc_Driver1}}", vehicleMap.get("Prin_Driver"));
					table.getRows().get(1).getCells().get(1).getFirstParagraph().getRange().replace("{{Automobile1}}", "Automobile " + vehicleMap.get("AutoNo"));
					table.getRows().get(2).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_YearMake1}}", vehicleMap.get("UnitYear") + " " + vehicleMap.get("UnitMake"));
					table.getRows().get(3).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_BodyType1}}", "");
					table.getRows().get(4).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_VIN1}}", vehicleMap.get("VIN"));
					table.getRows().get(7).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_DCPD1}}", vehicleMap.get("DCPDDed"));
					table.getRows().get(8).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_SPD1}}", vehicleMap.get("SPDed"));
					table.getRows().get(9).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_CM1}}", vehicleMap.get("CMDed"));
					table.getRows().get(10).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_CL1}}", vehicleMap.get("CLDed"));
					table.getRows().get(11).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_AP1}}", vehicleMap.get("APDed"));
					table.getRows().get(12).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_EndList1}}", vehicleMap.get("EndList"));
					table.getRows().get(13).getCells().get(1).getFirstParagraph().getRange().replace("{{Auto_VehPrem1}}", vehicleMap.get("VehPrem"));
					mainTable.insertAfter(caiRow, refRow);
					refRow = caiRow;
				} else {
					cleanAuto2 = false;
					Table table = refRow.getFirstCell().getTables().get(0);
					table.getRows().get(0).getCells().get(2).getFirstParagraph().getRange().replace("{{Pinc_Driver2}}", vehicleMap.get("Prin_Driver"));
					table.getRows().get(1).getCells().get(2).getFirstParagraph().getRange().replace("{{Automobile2}}", "Automobile " + vehicleMap.get("AutoNo"));
					table.getRows().get(2).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_YearMake2}}", vehicleMap.get("UnitYear") + " " + vehicleMap.get("UnitMake"));
					table.getRows().get(3).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_BodyType2}}", "");
					table.getRows().get(4).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_VIN2}}", vehicleMap.get("VIN"));
					table.getRows().get(7).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_DCPD2}}", vehicleMap.get("DCPDDed"));
					table.getRows().get(8).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_SPD2}}", vehicleMap.get("SPDed"));
					table.getRows().get(9).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_CM2}}", vehicleMap.get("CMDed"));
					table.getRows().get(10).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_CL2}}", vehicleMap.get("CLDed"));
					table.getRows().get(11).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_AP2}}", vehicleMap.get("APDed"));
					table.getRows().get(12).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_EndList2}}", vehicleMap.get("EndList"));
					table.getRows().get(13).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_VehPrem2}}", vehicleMap.get("VehPrem"));
				}
			}
			
			if (cleanAuto2) {
				Table table = refRow.getFirstCell().getTables().get(0);
				table.getRows().get(0).getCells().get(2).getFirstParagraph().getRange().replace("{{Pinc_Driver2}}", "");
				table.getRows().get(1).getCells().get(2).getFirstParagraph().getRange().replace("{{Automobile2}}", "");
				table.getRows().get(2).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_YearMake2}}", "");
				table.getRows().get(3).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_BodyType2}}", "");
				table.getRows().get(4).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_VIN2}}", "");
				table.getRows().get(7).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_DCPD2}}", "");
				table.getRows().get(8).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_SPD2}}", "");
				table.getRows().get(9).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_CM2}}", "");
				table.getRows().get(10).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_CL2}}", "");
				table.getRows().get(11).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_AP2}}", "");
				table.getRows().get(12).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_EndList2}}", "");
				table.getRows().get(13).getCells().get(2).getFirstParagraph().getRange().replace("{{Auto_VehPrem2}}", "");
			}
		}
		
		mainTable.removeChild(mainRow);
	}
	
	private void setOPCF27BRows(Document doc, Table opcfTable, Row typeOfVehicleRow, Row amtOfInsuranceRow,
			Row deductibleRow) {
		Iterator<Endorsement> it = opcf27BEnds.iterator();
		Row refRow = deductibleRow;
		boolean isFirst = true;
		while (it.hasNext()) {
			Endorsement end = it.next();
			String typeOfVehicle = "";
			for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
				for (ExtensionData extData : extType.getExtensionDatas()) {
					if (extData.getColumnId().equalsIgnoreCase("OPCF27B_VehType")) {
						typeOfVehicle = extData.getColumnValue();
					}
				}
			}
			String limit = numberFormat(end.getLimit1amount());
			String deductible = numberFormat(end.getDeductible1amount());

			if (isFirst) {
				Cell typeOfVehicleCell = typeOfVehicleRow.getCells().get(3);
				typeOfVehicleCell.getFirstParagraph().removeAllChildren();
				Run typeOfVehicleRun = new Run(doc, typeOfVehicle);
				typeOfVehicleRun.getFont().setName("Arial");
				typeOfVehicleRun.getFont().setSize(8);
				typeOfVehicleRun.getFont().setBold(true);
				typeOfVehicleRow.getCells().get(3).getFirstParagraph().appendChild(typeOfVehicleRun);

				Cell amtOfInsuranceCell = amtOfInsuranceRow.getCells().get(3);
				amtOfInsuranceCell.getFirstParagraph().removeAllChildren();
				Run amtOfInsuranceRun = new Run(doc, limit);
				amtOfInsuranceRun.getFont().setName("Arial");
				amtOfInsuranceRun.getFont().setSize(8);
				amtOfInsuranceRun.getFont().setBold(true);
				amtOfInsuranceRow.getCells().get(3).getFirstParagraph().appendChild(amtOfInsuranceRun);

				Cell dedCell = deductibleRow.getCells().get(3);
				dedCell.getFirstParagraph().removeAllChildren();
				Run dedRun = new Run(doc, deductible);
				dedRun.getFont().setName("Arial");
				dedRun.getFont().setSize(8);
				dedRun.getFont().setBold(true);
				deductibleRow.getCells().get(3).getFirstParagraph().appendChild(dedRun);

				isFirst = false;
			} else {

				Row newTypeOfVehicleRow = (Row) typeOfVehicleRow.deepClone(true);
				Cell typeOfVehicleCell = newTypeOfVehicleRow.getCells().get(3);
				typeOfVehicleCell.getFirstParagraph().removeAllChildren();
				Run typeOfVehicleRun = new Run(doc, typeOfVehicle);
				typeOfVehicleRun.getFont().setName("Arial");
				typeOfVehicleRun.getFont().setSize(8);
				typeOfVehicleRun.getFont().setBold(true);
				newTypeOfVehicleRow.getCells().get(3).getFirstParagraph().appendChild(typeOfVehicleRun);
				opcfTable.insertAfter(newTypeOfVehicleRow, refRow);
				refRow = newTypeOfVehicleRow;

				Row newAmtOfInsuranceRow = (Row) amtOfInsuranceRow.deepClone(true);
				Cell amtOfInsuranceCell = newAmtOfInsuranceRow.getCells().get(3);
				amtOfInsuranceCell.getFirstParagraph().removeAllChildren();
				Run amtOfInsuranceRun = new Run(doc, limit);
				amtOfInsuranceRun.getFont().setName("Arial");
				amtOfInsuranceRun.getFont().setSize(8);
				amtOfInsuranceRun.getFont().setBold(true);
				newAmtOfInsuranceRow.getCells().get(3).getFirstParagraph().appendChild(amtOfInsuranceRun);
				opcfTable.insertAfter(newAmtOfInsuranceRow, refRow);
				refRow = newAmtOfInsuranceRow;

				Row newDeductibleRow = (Row) deductibleRow.deepClone(true);
				Cell dedCell = newDeductibleRow.getCells().get(3);
				dedCell.getFirstParagraph().removeAllChildren();
				Run dedRun = new Run(doc, deductible);
				dedRun.getFont().setName("Arial");
				dedRun.getFont().setSize(8);
				dedRun.getFont().setBold(true);
				newDeductibleRow.getCells().get(3).getFirstParagraph().appendChild(dedRun);
				opcfTable.insertAfter(newDeductibleRow, refRow);
				refRow = newDeductibleRow;

			}
		}
	}
	
	private void setSection5ExtraLocations(Document doc) {
		Table garTable = doc.getFirstSection().getBody().getTables().get(3);
		Row refRow = garTable.getRows().get(9);
		for (HashMap<String, String> loc : section5ExtraLocations) {
			Row newLocRow = (Row) refRow.deepClone(true);

			// Location Id
			String locId = (String) loc.get("id");
			newLocRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, locId);
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(8);
			newRun2.getFont().setBold(true);
			newLocRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);
						
			// Sub Section
			String subSection = (String) loc.get("subSection");
			newLocRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, subSection);
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(8);
			newRun3.getFont().setBold(true);
			newLocRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// sbLimit
			String subLimit = (String) loc.get("sbLimit");
			newLocRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, subLimit);
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(8);
			newRun5.getFont().setBold(true);
			newLocRow.getCells().get(5).getFirstParagraph().appendChild(newRun5);

			// sbDed
			String subDed = (String) loc.get("sbDed");
			newLocRow.getCells().get(7).getFirstParagraph().removeAllChildren();
			Run newRun7 = new Run(doc, subDed);
			newRun7.getFont().setName("Arial");
			newRun7.getFont().setSize(8);
			newRun7.getFont().setBold(true);
			newLocRow.getCells().get(7).getFirstParagraph().appendChild(newRun7);

			garTable.insertAfter(newLocRow, refRow);
			refRow = newLocRow;
		}
	}
	
	private List<PolicyLocation> getPolicyLocations() {
		List<PolicyLocation> locations = new ArrayList<PolicyLocation>();
		for (Risk risk : policyDocument.getPolicyTransaction().getPolicyRisks()) {
			if (risk.getRiskType().equalsIgnoreCase("LO")) {
				PolicyLocation pl = (PolicyLocation) risk;
				locations.add(pl);
			}
		}
		locations.sort(Comparator.comparing(PolicyLocation::getLocationId));

		return locations;
	}
	
	private void hideSections(Document doc) {
		List<Row> cglRows = new ArrayList<Row>();
		List<Row> spf6tplRows = new ArrayList<Row>();
		List<Row> cgoRows = new ArrayList<Row>();
		List<Row> cgoExtOfCovRows = new ArrayList<Row>();
		
		//table for AUTO
		Table opcfTable = doc.getSections().get(0).getBody().getTables().get(1);
		//table for CGL, SPF6TPL and CGO
		Table cglTable = doc.getSections().get(0).getBody().getTables().get(2);
		//table for GAR
		Table garTable = doc.getFirstSection().getBody().getTables().get(3);
		Table garTable2 = doc.getFirstSection().getBody().getTables().get(4);
		Table garTable3 = doc.getFirstSection().getBody().getTables().get(5);
		//Table for Other Endorsements
		Table otherEndTable = doc.getFirstSection().getBody().getTables().get(6);
		
		Row opcfHeaderRow = opcfTable.getRows().get(0);
		Row opcf2Row = opcfTable.getRows().get(1);
		Row opcf5Row = opcfTable.getRows().get(2);
		Row opcf8Row = opcfTable.getRows().get(3);
		Row opcf9Row = opcfTable.getRows().get(4);
		Row opcf13CRow = opcfTable.getRows().get(5);
		Row opcf19Row = opcfTable.getRows().get(6);
		Row opcf20Row = opcfTable.getRows().get(7);
		Row opcf20HRow = opcfTable.getRows().get(8);
		Row eon20GRow = opcfTable.getRows().get(9);
		Row opcf21ARow = opcfTable.getRows().get(10);
		Row opcf21BRow = opcfTable.getRows().get(11);
		Row opcf23ARow = opcfTable.getRows().get(12);
		Row opcf27Row = opcfTable.getRows().get(13);
		Row opcf27B_1Row = opcfTable.getRows().get(14);
		Row opcf27B_2Row = opcfTable.getRows().get(15);
		Row opcf27B_3Row = opcfTable.getRows().get(16);
		Row opcf27B_4Row = opcfTable.getRows().get(17);
		Row opcf28Row = opcfTable.getRows().get(18);
		Row opcf28ARow = opcfTable.getRows().get(19);
		Row opcf30Row = opcfTable.getRows().get(20);
		Row opcf31Row = opcfTable.getRows().get(21);
		Row opcf38Row = opcfTable.getRows().get(22);
		Row opcf40Row = opcfTable.getRows().get(23);
		Row opcf43_43ARow = opcfTable.getRows().get(24);
		Row opcf44RRow = opcfTable.getRows().get(25);
		Row opcf47Row = opcfTable.getRows().get(26);
		Row opcf48Row = opcfTable.getRows().get(27);
		Row opcfRow = opcfTable.getRows().get(28);
		Row opcfFooterRow = opcfTable.getRows().get(29);
		Row cdecRow = otherEndTable.getRows().get(1);
		Row cdelRow = otherEndTable.getRows().get(2);
		
		//Row cglLocationRow = cglTable.getRows().get(10);
		
		Row garSection5Row = garTable.getRows().get(4);
		Row garSection5_1Row = garTable.getRows().get(5);
		Row garSection5_2Row = garTable.getRows().get(6);
		Row garSection5_3Row = garTable.getRows().get(7);
		Row garSection5_4Row = garTable.getRows().get(8);
		Row garSection5_5Row = garTable.getRows().get(9);
		Row garSection6Row = garTable.getRows().get(10);
		Row garSection6_1Row = garTable.getRows().get(11);
		Row garSection6_4_1Row = garTable.getRows().get(12);
		Row garSection6_4_2Row = garTable2.getRows().get(0);
		
		if(!this.showOPCF2) {
			opcfTable.removeChild(opcf2Row);
		}
		
		if(!this.showOPCF5) {
			opcfTable.removeChild(opcf5Row);
		}
		
		if(!this.showOPCF8) {
			opcfTable.removeChild(opcf8Row);
		}
		
		if(!this.showOPCF9) {
			opcfTable.removeChild(opcf9Row);
		}
		
		if(!this.showOPCF13C) {
			opcfTable.removeChild(opcf13CRow);
		}
		
		if(!this.showOPCF19) {
			opcfTable.removeChild(opcf19Row);
		}
		
		if(!this.showOPCF20) {
			opcfTable.removeChild(opcf20Row);
		}
		
		if(!this.showOPCF20H) {
			opcfTable.removeChild(opcf20HRow);
		}
		
		if(!this.showEON20G) {
			opcfTable.removeChild(eon20GRow);
		}
		
		if(!this.showOPCF21A) {
			opcfTable.removeChild(opcf21ARow);
		}
		
		if(!this.showOPCF21B) {
			opcfTable.removeChild(opcf21BRow);
		}
		
		if(!this.showOPCF23A) {
			opcfTable.removeChild(opcf23ARow);
		}
		
		if(!this.showOPCF27) {
			opcfTable.removeChild(opcf27Row);
		}
		
		if (!this.showOPCF27B) {
			opcfTable.removeChild(opcf27B_1Row);
			opcfTable.removeChild(opcf27B_2Row);
			opcfTable.removeChild(opcf27B_3Row);
			opcfTable.removeChild(opcf27B_4Row);
		} else {
			setOPCF27BRows(doc, opcfTable, opcf27B_2Row, opcf27B_3Row, opcf27B_4Row);
		}
		
		if(!this.showOPCF28) {
			opcfTable.removeChild(opcf28Row);
		}
		
		if(!this.showOPCF28A) {
			opcfTable.removeChild(opcf28ARow);
		}
		
		if(!this.showOPCF30) {
			opcfTable.removeChild(opcf30Row);
		}
		
		if(!this.showOPCF31) {
			opcfTable.removeChild(opcf31Row);
		}
		
		if(!this.showOPCF38) {
			opcfTable.removeChild(opcf38Row);
		}
		
		if(!this.showOPCF40) {
			opcfTable.removeChild(opcf40Row);
		}
		
		if(!this.showOPCF43_43A) {
			opcfTable.removeChild(opcf43_43ARow);
		}
		
		if(!this.showOPCF44R) {
			opcfTable.removeChild(opcf44RRow);
		}
		
		if(!this.showOPCF47) {
			opcfTable.removeChild(opcf47Row);
		}
		
		if(!this.showOPCF48) {
			opcfTable.removeChild(opcf48Row);
		}
		
		if(!this.showOPCF) {
			opcfTable.removeChild(opcfRow);
		}
		
		if (!this.showOPCF2 && !this.showOPCF5 && !this.showOPCF8 && !this.showOPCF9 && !this.showOPCF13C
				&& !this.showOPCF19 && !this.showOPCF20 && !this.showEON20G && !this.showOPCF21A && !this.showOPCF21B
				&& !this.showOPCF23A && !this.showOPCF27 && !this.showOPCF27B && !this.showOPCF28 && !this.showOPCF28A
				&& !this.showOPCF30 && !this.showOPCF31 && !this.showOPCF38 && !this.showOPCF40
				&& !this.showOPCF43_43A && !this.showOPCF44R && !this.showOPCF47 && !this.showOPCF48 && !this.showOPCF) {
			opcfTable.removeChild(opcfHeaderRow);
			opcfTable.removeChild(opcfFooterRow);
		}
		
		if (!this.showGARSection5) {
			garTable.removeChild(garSection5Row);
			garTable.removeChild(garSection5_1Row);
			garTable.removeChild(garSection5_2Row);
			garTable.removeChild(garSection5_3Row);
			garTable.removeChild(garSection5_4Row);
			garTable.removeChild(garSection5_5Row);
		}

		if (!this.showGARSection61) {
			garTable.removeChild(garSection6_1Row);
		}

		if (!this.showGARSection64) {
			garTable.removeChild(garSection6_4_1Row);
			garTable2.removeChild(garSection6_4_2Row);
		}

		if (!this.showGARSection61 && !this.showGARSection64) {
			garTable.removeChild(garSection6Row);
		}
		
		RowCollection coll = cglTable.getRows();
		Iterator<Row> it = coll.iterator();
		int i = 0;
		while (it.hasNext()) {

			/*
			 * Rows from 0 to 16 belongs to CGL section 
			 * Rows from 17 to 20 belongs to SPF6TPL
			 * Rows from 21 to 24 belongs to CGO
			 * Rows from 25 to 31 belongs to extension of coverage
			 */
			if (i < 16) {
				cglRows.add(it.next());
			} else if (i >= 16 && i < 20) {
				spf6tplRows.add(it.next());
			} else if(i >= 20 && i < 25){
				cgoRows.add(it.next());
			} else if(i >= 25){
				cgoExtOfCovRows.add(it.next());
			}

			i++;
		}
		
		if(!showEBE && showCGL) {
			cglTable.removeChild(cglTable.getRows().get(12));
		}
		
		if(!showCGL) {
			removeRowsFromTable(doc, cglTable, cglRows);
		}
		
		if(!showSPF6TPL) {
			removeRowsFromTable(doc, cglTable, spf6tplRows);
		}
		
		if(!showCGO) {
			removeRowsFromTable(doc, cglTable, cgoRows);
			//remove last line outside the table
			doc.getFirstSection().getBody().removeChild(doc.getFirstSection().getBody().getChildNodes().get(6));
		}
		
		if(!showCGOExtOfCov) {
			removeRowsFromTable(doc, cglTable, cgoExtOfCovRows);
		}
		
		if(!showGAR) {
			garTable.removeAllChildren();
			garTable2.removeAllChildren();
			garTable3.removeAllChildren();
		}
		
		if(!showCDEC || !isLaterThanJan2021) {
			otherEndTable.removeChild(cdecRow);
		}
		
		if(!showCGL || !isLaterThanJan2021) {
			otherEndTable.removeChild(cdelRow);
		}

	}
	
	private void removeRowsFromTable(Document doc, Table table, List<Row> rows) {
		for (Row row : rows) {
			table.removeChild(row);
		}
	}
}
