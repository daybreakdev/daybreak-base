package com.echelon.services.document;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.aspose.words.Document;
import com.aspose.words.Row;
import com.aspose.words.Run;
import com.aspose.words.Section;
import com.aspose.words.Table;
import com.ds.ins.dal.dao.StatCodesDAO;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.StatCodes;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.services.document.AsposeLicense;
import com.ds.ins.services.document.HandleMergeField;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class AutoFleetSchedule extends EchelonDocument implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -883566303900631089L;
	private IGenericLookupConfig lookupConfig;
	private String[] fieldNames;
	private Object[] fieldValues;
	private String polNo = "";
	private String insuredName = "";
	private String insuredAddressStreet = "";
	private String insuredAddressCity = "";
	private String insuredAddressPC = "";
	private String effectiveDate = "";
	private String expiryDate = "";
	private String limit = "";
	private String totPrem = "";
	List<HashMap<String, String>> vehFirstTable = new ArrayList<HashMap<String, String>>();
	List<HashMap<String, String>> vehOtherTables = new ArrayList<HashMap<String, String>>();

	public AutoFleetSchedule(PolicyDocuments policyDocument) throws Exception {
		AsposeLicense.getInstance().getLicense();
		InsurancePolicy insurancePolicy = policyDocument.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();
		SpecialtyAutoPackage autoPackage = (SpecialtyAutoPackage) insurancePolicy;
		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

		this.policyDocument = policyDocument;
		this.lookupConfig = (new ProductConfigFactory()).getLookupConfig(insurancePolicy.getProductCd(),
				policyDocument.getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		this.polNo = insurancePolicy.getBasePolicyNum();
		if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getCommercialCustomer().getLegalName();
		} else if (insurancePolicy.getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
			this.insuredName = insurancePolicy.getPolicyCustomer().getPersonalCustomer().getFullName();
		}
		this.insuredAddressStreet = insurancePolicy.getInsuredAddress().getAddressLine1();
		this.insuredAddressCity = insurancePolicy.getInsuredAddress().getCity();
		this.insuredAddressPC = insurancePolicy.getInsuredAddress().getPostalZip();
		this.effectiveDate = formatDate(
				convertLocalDateTimeToDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermEffDate()),
				"M/dd/yyyy");
		this.expiryDate = formatDate(
				convertLocalDateTimeToDate(
						policyDocument.getPolicyTransaction().getPolicyVersion().getVersionTerm().getTermExpDate()),
				"M/dd/yyyy");
		this.limit = numberFormat(autoPackage.getLiabilityLimit());
		this.totPrem = numberFormat(subPolicy.getSubPolicyPremium().getWrittenPremium());
	}
	
	public String getTemplatePath() {
		return "templates/AutoFleetSchedule_tmp.docx";
	}

	public void create(String storageKey) throws Exception {
		initializeFields();
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		if(attachPath == null || attachPath.equalsIgnoreCase("null")) {
			throw new Exception("Attachment directory was not found");
		}
		Document doc = new Document(Thread.currentThread().getContextClassLoader().getResourceAsStream(templatePath));
		doc.getMailMerge().setFieldMergingCallback(new HandleMergeField());
		doc.getMailMerge().setUseNonMergeFields(true);
		doc.getMailMerge().execute(fieldNames, fieldValues);
		getVehicleRecords();
		setFirstPageTable(doc);
		setOtherPageTable(doc);
		doc.save(attachPath + File.separator + storageKey);
	}

	public void initializeFields() {
		fieldNames = new String[] { "Pol_No", "Insured_Name", "Insured_Address_Street", "Insured_Address_City",
				"Insured_Address_PC", "Effective_Date", "Expiry_Date", "Limit", "TotPrem" };
		fieldValues = new Object[] { polNo, insuredName, insuredAddressStreet, insuredAddressCity, insuredAddressPC,
				effectiveDate, expiryDate, limit, totPrem };
	}
	
	private void getVehicleRecords() throws InsurancePolicyException {
		List<SpecialtyVehicleRisk> vehicles = getVehicleRisks();
		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) policyDocument.getPolicyTransaction()
				.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		int vehNo = 1;
		String territoryCode = "";
		
		//PolicyService policyService = new PolicyService();
		StatCodes sc = (new StatCodesDAO()).findStatCodesByPostalCode(insuredAddressPC);
		territoryCode = sc.getTerritoryCode();

		for (SpecialtyVehicleRisk vehicle : vehicles) {
			HashMap<String, String> vehRecord = new HashMap<String, String>();
			List<Endorsement> endList = new ArrayList<Endorsement>();
			LookupTableItem lookupSchVehBodyItem = lookupConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_SchVehBody).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(vehicle.getVehicleDescription())).findFirst()
					.orElse(null);
			String bt = lookupSchVehBodyItem != null && lookupSchVehBodyItem.getItemValue() != null ? lookupSchVehBodyItem.getItemValue() : "";

			Double bi_premium = 0.0;
			Double pd_premium = 0.0;
			String liabpr = "";
			String dcd = "N/A";
			String dcpr = "0";
			String abpr = "0";
			String uapr = "0";
			String apd = "N/A";
			String appr = "0";
			String cld = "N/A";
			String clpr = "0";
			String cmd = "N/A";
			String cmpr = "0";
			String spd = "N/A";
			String sppr = "0";
			String endCodeList = "";
			Double endPr = 0.0;
			String vehPrem = "";

			for (Coverage cove : vehicle.getRiskCoverages()) {
				if (cove.getCoverageCode().equalsIgnoreCase("BI")
						&& cove.getCoveragePremium().getWrittenPremium() != null) {
					bi_premium = cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("PD")
						&& cove.getCoveragePremium().getWrittenPremium() != null) {
					pd_premium = cove.getCoveragePremium().getWrittenPremium();
				} else if (cove.getCoverageCode().equalsIgnoreCase("DCPD")) {
					if (cove.getDeductible1() != null) {
						dcd = numberFormat(cove.getDeductible1());
					}
					if (cove.getCoveragePremium().getWrittenPremium() != null) {
						dcpr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("AB")
						&& cove.getCoveragePremium().getWrittenPremium() != null) {
					abpr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("UA")
						&& cove.getCoveragePremium().getWrittenPremium() != null) {
					uapr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
				} else if (cove.getCoverageCode().equalsIgnoreCase("AP")) {
					if (cove.getDeductible1() != null) {
						apd = numberFormat(cove.getDeductible1());
					}
					if (cove.getCoveragePremium().getWrittenPremium() != null) {
						appr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("CL")) {
					if (cove.getDeductible1() != null) {
						cld = numberFormat(cove.getDeductible1());
					}
					if (cove.getCoveragePremium().getWrittenPremium() != null) {
						clpr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("CM")) {
					if (cove.getDeductible1() != null) {
						cmd = numberFormat(cove.getDeductible1());
					}
					if (cove.getCoveragePremium().getWrittenPremium() != null) {
						cmpr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					}
				} else if (cove.getCoverageCode().equalsIgnoreCase("SP")) {
					if (cove.getDeductible1() != null) {
						spd = numberFormat(cove.getDeductible1());
					}
					if (cove.getCoveragePremium().getWrittenPremium() != null) {
						sppr = numberFormat(cove.getCoveragePremium().getWrittenPremium());
					}
				}
			}

			//Endorsements from SubPolicyAuto
			for(Endorsement end : subPolicy.getCoverageDetail().getEndorsements()) {
				if(!end.getEndorsementCd().equalsIgnoreCase("OPCF25A")) {
					endList.add(end);	
				}
			}
			//Endorsements from PolicyRisk
			for (Endorsement end : vehicle.getCoverageDetail().getEndorsements()) {
				endList.add(end);
				if (end.getEndorsementPremium() != null) {
					endPr = endPr + end.getEndorsementPremium().getWrittenPremium();
				}
			}

			liabpr = numberFormat(bi_premium + pd_premium);
			endCodeList = orderedEndList(endList);

			if (vehicle.getCoverageDetail().getCoveragePremium() != null) {
				vehPrem = numberFormat(vehicle.getRiskPremium().getWrittenPremium());
			}

			// Auto Fleet Schedule table
			vehRecord.put("vehNo", vehNo + ".");
			vehRecord.put("Yr", vehicle.getUnitYear() != null ? Integer.toString(vehicle.getUnitYear()) : "");
			vehRecord.put("Make", vehicle.getUnitMake() != null ? vehicle.getUnitMake() : "");
			vehRecord.put("BT", bt);
			vehRecord.put("LPN", numberFormat(vehicle.getUnitLPN()));
			vehRecord.put("SerialNo", vehicle.getUnitSerialNumberorVin());
			vehRecord.put("Ter", territoryCode);
			vehRecord.put("LIABPr", liabpr);
			vehRecord.put("DCD", dcd);
			vehRecord.put("DCDPr", dcpr);
			vehRecord.put("ABPr", abpr);
			vehRecord.put("UAPr", uapr);
			// Section 7
			vehRecord.put("APD", apd);
			vehRecord.put("APPr", appr);
			vehRecord.put("CLD", cld);
			vehRecord.put("CLPr", clpr);
			vehRecord.put("CMD", cmd);
			vehRecord.put("CMPr", cmpr);
			vehRecord.put("SPD", spd);
			vehRecord.put("SPPr", sppr);
			vehRecord.put("EndList", endCodeList);
			vehRecord.put("EndPr", numberFormat(endPr));
			vehRecord.put("VehPrem", vehPrem);

			if (vehNo <= 13) {
				this.vehFirstTable.add(vehRecord);
			} else {
				this.vehOtherTables.add(vehRecord);
			}
			vehNo++;
		}

		//Fix to 13 records
		if (this.vehFirstTable.size() < 13) {
			for (int i = this.vehFirstTable.size() + 1; i <= 13; i++) {
				HashMap<String, String> vehRecord = new HashMap<String, String>();
				// Auto Fleet Schedule table
				vehRecord.put("vehNo", i + ".");
				vehRecord.put("Yr", "");
				vehRecord.put("Make", "");
				vehRecord.put("BT", "");
				vehRecord.put("LPN", "");
				vehRecord.put("SerialNo", "");
				vehRecord.put("Ter", "");
				vehRecord.put("LIABPr", "");
				vehRecord.put("DCD", "");
				vehRecord.put("DCDPr", "");
				vehRecord.put("ABPr", "");
				vehRecord.put("UAPr", "");
				// Section 7
				vehRecord.put("APD", "");
				vehRecord.put("APPr", "");
				vehRecord.put("CLD", "");
				vehRecord.put("CLPr", "");
				vehRecord.put("CMD", "");
				vehRecord.put("CMPr", "");
				vehRecord.put("SPD", "");
				vehRecord.put("SPPr", "");
				vehRecord.put("EndList", "");
				vehRecord.put("EndPr", "");
				vehRecord.put("VehPrem", "");
				this.vehFirstTable.add(vehRecord);
			}
		} 
		
		if(this.vehOtherTables.size() != 0 && this.vehOtherTables.size() % 13 != 0) {
			while(this.vehOtherTables.size() % 13 != 0) {
				HashMap<String, String> vehRecord = new HashMap<String, String>();
				// Auto Fleet Schedule table
				vehRecord.put("vehNo", vehNo + ".");
				vehRecord.put("Yr", "");
				vehRecord.put("Make", "");
				vehRecord.put("BT", "");
				vehRecord.put("LPN", "");
				vehRecord.put("SerialNo", "");
				vehRecord.put("Ter", "");
				vehRecord.put("LIABPr", "");
				vehRecord.put("DCD", "");
				vehRecord.put("DCDPr", "");
				vehRecord.put("ABPr", "");
				vehRecord.put("UAPr", "");
				// Section 7
				vehRecord.put("APD", "");
				vehRecord.put("APPr", "");
				vehRecord.put("CLD", "");
				vehRecord.put("CLPr", "");
				vehRecord.put("CMD", "");
				vehRecord.put("CMPr", "");
				vehRecord.put("SPD", "");
				vehRecord.put("SPPr", "");
				vehRecord.put("EndList", "");
				vehRecord.put("EndPr", "");
				vehRecord.put("VehPrem", "");
				this.vehOtherTables.add(vehRecord);
				vehNo++;
			}
		}
	}

	private void setFirstPageTable(Document doc) {
		int rowIdx = 7;
		int rowS7Idx = 26;
		double totalLiabPr = 0.0;
		double totPrem = 0.0;
		Section sec = doc.getFirstSection();
		Table table = sec.getBody().getTables().get(1);

		for (HashMap<String, String> vehRecord : this.vehFirstTable) {
			Row vehRow = table.getRows().get(rowIdx);
			totalLiabPr = totalLiabPr + stringToDouble(vehRecord.get("LIABPr"));
			totPrem = totPrem + stringToDouble(vehRecord.get("VehPrem"));

			// Veh_No
			vehRow.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newRun = new Run(doc, vehRecord.get("vehNo"));
			newRun.getFont().setName("Arial");
			newRun.getFont().setSize(7);
			newRun.getFont().setBold(true);
			vehRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

			// Year
			vehRow.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newRun1 = new Run(doc, vehRecord.get("Yr"));
			newRun1.getFont().setName("Arial");
			newRun1.getFont().setSize(7);
			vehRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

			// Make
			vehRow.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newRun2 = new Run(doc, vehRecord.get("Make"));
			newRun2.getFont().setName("Arial");
			newRun2.getFont().setSize(7);
			vehRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

			// BT
			vehRow.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newRun3 = new Run(doc, vehRecord.get("BT"));
			newRun3.getFont().setName("Arial");
			newRun3.getFont().setSize(7);
			vehRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

			// LPN
			vehRow.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newRun4 = new Run(doc, vehRecord.get("LPN"));
			newRun4.getFont().setName("Arial");
			newRun4.getFont().setSize(7);
			vehRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

			// SerialNo
			vehRow.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newRun5 = new Run(doc, vehRecord.get("SerialNo"));
			newRun5.getFont().setName("Arial");
			newRun5.getFont().setSize(7);
			vehRow.getCells().get(5).getFirstParagraph().appendChild(newRun5);

			// Ter
			vehRow.getCells().get(6).getFirstParagraph().removeAllChildren();
			Run newRun6 = new Run(doc, vehRecord.get("Ter"));
			newRun6.getFont().setName("Arial");
			newRun6.getFont().setSize(7);
			vehRow.getCells().get(6).getFirstParagraph().appendChild(newRun6);

			// LIABPr
			vehRow.getCells().get(7).getFirstParagraph().removeAllChildren();
			Run newRun7 = new Run(doc, vehRecord.get("LIABPr"));
			newRun7.getFont().setName("Arial");
			newRun7.getFont().setSize(7);
			vehRow.getCells().get(7).getFirstParagraph().appendChild(newRun7);

			// DCD
			vehRow.getCells().get(8).getFirstParagraph().removeAllChildren();
			Run newRun8 = new Run(doc, vehRecord.get("DCD"));
			newRun8.getFont().setName("Arial");
			newRun8.getFont().setSize(7);
			vehRow.getCells().get(8).getFirstParagraph().appendChild(newRun8);

			// DCPr
			vehRow.getCells().get(9).getFirstParagraph().removeAllChildren();
			Run newRun9 = new Run(doc, vehRecord.get("DCDPr"));
			newRun9.getFont().setName("Arial");
			newRun9.getFont().setSize(7);
			vehRow.getCells().get(9).getFirstParagraph().appendChild(newRun9);

			// ABPr
			vehRow.getCells().get(10).getFirstParagraph().removeAllChildren();
			Run newRun10 = new Run(doc, vehRecord.get("ABPr"));
			newRun10.getFont().setName("Arial");
			newRun10.getFont().setSize(7);
			vehRow.getCells().get(10).getFirstParagraph().appendChild(newRun10);

			// UAPr
			vehRow.getCells().get(11).getFirstParagraph().removeAllChildren();
			Run newRun11 = new Run(doc, vehRecord.get("UAPr"));
			newRun11.getFont().setName("Arial");
			newRun11.getFont().setSize(7);
			vehRow.getCells().get(11).getFirstParagraph().appendChild(newRun11);

			rowIdx++;

			// Section 7
			Row vehSec7Row = table.getRows().get(rowS7Idx);

			// Veh_No
			vehSec7Row.getCells().get(0).getFirstParagraph().removeAllChildren();
			Run newSec7Run = new Run(doc, vehRecord.get("vehNo"));
			newSec7Run.getFont().setName("Arial");
			newSec7Run.getFont().setSize(7);
			newSec7Run.getFont().setBold(true);
			vehSec7Row.getCells().get(0).getFirstParagraph().appendChild(newSec7Run);

			// APD
			vehSec7Row.getCells().get(1).getFirstParagraph().removeAllChildren();
			Run newSec7Run1 = new Run(doc, vehRecord.get("APD"));
			newSec7Run1.getFont().setName("Arial");
			newSec7Run1.getFont().setSize(7);
			vehSec7Row.getCells().get(1).getFirstParagraph().appendChild(newSec7Run1);

			// APPr
			vehSec7Row.getCells().get(2).getFirstParagraph().removeAllChildren();
			Run newSec7Run2 = new Run(doc, vehRecord.get("APPr"));
			newSec7Run2.getFont().setName("Arial");
			newSec7Run2.getFont().setSize(7);
			vehSec7Row.getCells().get(2).getFirstParagraph().appendChild(newSec7Run2);

			// CLD
			vehSec7Row.getCells().get(3).getFirstParagraph().removeAllChildren();
			Run newSec7Run3 = new Run(doc, vehRecord.get("CLD"));
			newSec7Run3.getFont().setName("Arial");
			newSec7Run3.getFont().setSize(7);
			vehSec7Row.getCells().get(3).getFirstParagraph().appendChild(newSec7Run3);

			// CLPr
			vehSec7Row.getCells().get(4).getFirstParagraph().removeAllChildren();
			Run newSec7Run4 = new Run(doc, vehRecord.get("CLPr"));
			newSec7Run4.getFont().setName("Arial");
			newSec7Run4.getFont().setSize(7);
			vehSec7Row.getCells().get(4).getFirstParagraph().appendChild(newSec7Run4);

			// CMD
			vehSec7Row.getCells().get(5).getFirstParagraph().removeAllChildren();
			Run newSec7Run5 = new Run(doc, vehRecord.get("CMD"));
			newSec7Run5.getFont().setName("Arial");
			newSec7Run5.getFont().setSize(7);
			vehSec7Row.getCells().get(5).getFirstParagraph().appendChild(newSec7Run5);

			// CMPr
			vehSec7Row.getCells().get(6).getFirstParagraph().removeAllChildren();
			Run newSec7Run6 = new Run(doc, vehRecord.get("CMPr"));
			newSec7Run6.getFont().setName("Arial");
			newSec7Run6.getFont().setSize(7);
			vehSec7Row.getCells().get(6).getFirstParagraph().appendChild(newSec7Run6);

			// SPD
			vehSec7Row.getCells().get(7).getFirstParagraph().removeAllChildren();
			Run newSec7Run7 = new Run(doc, vehRecord.get("SPD"));
			newSec7Run7.getFont().setName("Arial");
			newSec7Run7.getFont().setSize(7);
			vehSec7Row.getCells().get(7).getFirstParagraph().appendChild(newSec7Run7);

			// SPPr
			vehSec7Row.getCells().get(8).getFirstParagraph().removeAllChildren();
			Run newSec7Run8 = new Run(doc, vehRecord.get("SPPr"));
			newSec7Run8.getFont().setName("Arial");
			newSec7Run8.getFont().setSize(7);
			vehSec7Row.getCells().get(8).getFirstParagraph().appendChild(newSec7Run8);

			// EndList
			vehSec7Row.getCells().get(9).getFirstParagraph().removeAllChildren();
			Run newSec7Run9 = new Run(doc, vehRecord.get("EndList"));
			newSec7Run9.getFont().setName("Arial");
			newSec7Run9.getFont().setSize(7);
			vehSec7Row.getCells().get(9).getFirstParagraph().appendChild(newSec7Run9);

			// EndPr
			vehSec7Row.getCells().get(10).getFirstParagraph().removeAllChildren();
			Run newSec7Run10 = new Run(doc, vehRecord.get("EndPr"));
			newSec7Run10.getFont().setName("Arial");
			newSec7Run10.getFont().setSize(7);
			vehSec7Row.getCells().get(10).getFirstParagraph().appendChild(newSec7Run10);

			// VehPrem
			vehSec7Row.getCells().get(11).getFirstParagraph().removeAllChildren();
			Run newSec7Run11 = new Run(doc, vehRecord.get("VehPrem"));
			newSec7Run11.getFont().setName("Arial");
			newSec7Run11.getFont().setSize(7);
			vehSec7Row.getCells().get(11).getFirstParagraph().appendChild(newSec7Run11);

			rowS7Idx++;
		}
		
		//Set Liability Limit header
		String limitStr;
		if(totalLiabPr == 0) {
			limitStr = "$0";
		} else {
			limitStr = "$" + limit;
		}
		
		table.getRows().get(5).getCells().get(7).getFirstParagraph().removeAllChildren();
		Run limitRun = new Run(doc, limitStr);
		limitRun.getFont().setName("Arial");
		limitRun.getFont().setSize(7);
		limitRun.getFont().setBold(true);
		table.getRows().get(5).getCells().get(7).getFirstParagraph().appendChild(limitRun);
		
		//Set Total Premium
		table.getRows().get(40).getCells().get(2).getFirstParagraph().removeAllChildren();
		Run totPremRun = new Run(doc, "$" + numberFormat(totPrem));
		totPremRun.getFont().setName("Arial");
		totPremRun.getFont().setSize(8);
		totPremRun.getFont().setBold(true);
		table.getRows().get(40).getCells().get(2).getFirstParagraph().appendChild(totPremRun);
	}
	
	private void setOtherPageTable(Document doc) {
		int rowIdx = 7;
		int rowS7Idx = 26;
		int vehIdx = 1;
		Section templateSection = doc.getSections().get(1);
		Section refSection = templateSection;
		
		if(this.vehOtherTables.size() > 0) {
			Section sec = (Section) templateSection.deepClone(true);
			Table table = sec.getBody().getTables().get(0);
			double totalLiabPr = 0.0;
			double totPrem = 0.0;
			
			for (HashMap<String, String> vehRecord : this.vehOtherTables) {
				Row vehRow = table.getRows().get(rowIdx);
				totalLiabPr = totalLiabPr + stringToDouble(vehRecord.get("LIABPr"));
				totPrem = totPrem + stringToDouble(vehRecord.get("VehPrem"));

				// Veh_No
				vehRow.getCells().get(0).getFirstParagraph().removeAllChildren();
				Run newRun = new Run(doc, vehRecord.get("vehNo"));
				newRun.getFont().setName("Arial");
				newRun.getFont().setSize(7);
				newRun.getFont().setBold(true);
				vehRow.getCells().get(0).getFirstParagraph().appendChild(newRun);

				// Year
				vehRow.getCells().get(1).getFirstParagraph().removeAllChildren();
				Run newRun1 = new Run(doc, vehRecord.get("Yr"));
				newRun1.getFont().setName("Arial");
				newRun1.getFont().setSize(7);
				vehRow.getCells().get(1).getFirstParagraph().appendChild(newRun1);

				// Make
				vehRow.getCells().get(2).getFirstParagraph().removeAllChildren();
				Run newRun2 = new Run(doc, vehRecord.get("Make"));
				newRun2.getFont().setName("Arial");
				newRun2.getFont().setSize(7);
				vehRow.getCells().get(2).getFirstParagraph().appendChild(newRun2);

				// BT
				vehRow.getCells().get(3).getFirstParagraph().removeAllChildren();
				Run newRun3 = new Run(doc, vehRecord.get("BT"));
				newRun3.getFont().setName("Arial");
				newRun3.getFont().setSize(7);
				vehRow.getCells().get(3).getFirstParagraph().appendChild(newRun3);

				// LPN
				vehRow.getCells().get(4).getFirstParagraph().removeAllChildren();
				Run newRun4 = new Run(doc, vehRecord.get("LPN"));
				newRun4.getFont().setName("Arial");
				newRun4.getFont().setSize(7);
				vehRow.getCells().get(4).getFirstParagraph().appendChild(newRun4);

				// SerialNo
				vehRow.getCells().get(5).getFirstParagraph().removeAllChildren();
				Run newRun5 = new Run(doc, vehRecord.get("SerialNo"));
				newRun5.getFont().setName("Arial");
				newRun5.getFont().setSize(7);
				vehRow.getCells().get(5).getFirstParagraph().appendChild(newRun5);

				// Ter
				vehRow.getCells().get(6).getFirstParagraph().removeAllChildren();
				Run newRun6 = new Run(doc, vehRecord.get("Ter"));
				newRun6.getFont().setName("Arial");
				newRun6.getFont().setSize(7);
				vehRow.getCells().get(6).getFirstParagraph().appendChild(newRun6);

				// LIABPr
				vehRow.getCells().get(7).getFirstParagraph().removeAllChildren();
				Run newRun7 = new Run(doc, vehRecord.get("LIABPr"));
				newRun7.getFont().setName("Arial");
				newRun7.getFont().setSize(7);
				vehRow.getCells().get(7).getFirstParagraph().appendChild(newRun7);

				// DCD
				vehRow.getCells().get(8).getFirstParagraph().removeAllChildren();
				Run newRun8 = new Run(doc, vehRecord.get("DCD"));
				newRun8.getFont().setName("Arial");
				newRun8.getFont().setSize(7);
				vehRow.getCells().get(8).getFirstParagraph().appendChild(newRun8);

				// DCPr
				vehRow.getCells().get(9).getFirstParagraph().removeAllChildren();
				Run newRun9 = new Run(doc, vehRecord.get("DCDPr"));
				newRun9.getFont().setName("Arial");
				newRun9.getFont().setSize(7);
				vehRow.getCells().get(9).getFirstParagraph().appendChild(newRun9);

				// ABPr
				vehRow.getCells().get(10).getFirstParagraph().removeAllChildren();
				Run newRun10 = new Run(doc, vehRecord.get("ABPr"));
				newRun10.getFont().setName("Arial");
				newRun10.getFont().setSize(7);
				vehRow.getCells().get(10).getFirstParagraph().appendChild(newRun10);

				// UAPr
				vehRow.getCells().get(11).getFirstParagraph().removeAllChildren();
				Run newRun11 = new Run(doc, vehRecord.get("UAPr"));
				newRun11.getFont().setName("Arial");
				newRun11.getFont().setSize(7);
				vehRow.getCells().get(11).getFirstParagraph().appendChild(newRun11);

				rowIdx++;

				// Section 7
				Row vehSec7Row = table.getRows().get(rowS7Idx);

				// Veh_No
				vehSec7Row.getCells().get(0).getFirstParagraph().removeAllChildren();
				Run newSec7Run = new Run(doc, vehRecord.get("vehNo"));
				newSec7Run.getFont().setName("Arial");
				newSec7Run.getFont().setSize(7);
				newSec7Run.getFont().setBold(true);
				vehSec7Row.getCells().get(0).getFirstParagraph().appendChild(newSec7Run);

				// APD
				vehSec7Row.getCells().get(1).getFirstParagraph().removeAllChildren();
				Run newSec7Run1 = new Run(doc, vehRecord.get("APD"));
				newSec7Run1.getFont().setName("Arial");
				newSec7Run1.getFont().setSize(7);
				vehSec7Row.getCells().get(1).getFirstParagraph().appendChild(newSec7Run1);

				// APPr
				vehSec7Row.getCells().get(2).getFirstParagraph().removeAllChildren();
				Run newSec7Run2 = new Run(doc, vehRecord.get("APPr"));
				newSec7Run2.getFont().setName("Arial");
				newSec7Run2.getFont().setSize(7);
				vehSec7Row.getCells().get(2).getFirstParagraph().appendChild(newSec7Run2);

				// CLD
				vehSec7Row.getCells().get(3).getFirstParagraph().removeAllChildren();
				Run newSec7Run3 = new Run(doc, vehRecord.get("CLD"));
				newSec7Run3.getFont().setName("Arial");
				newSec7Run3.getFont().setSize(7);
				vehSec7Row.getCells().get(3).getFirstParagraph().appendChild(newSec7Run3);

				// CLPr
				vehSec7Row.getCells().get(4).getFirstParagraph().removeAllChildren();
				Run newSec7Run4 = new Run(doc, vehRecord.get("CLPr"));
				newSec7Run4.getFont().setName("Arial");
				newSec7Run4.getFont().setSize(7);
				vehSec7Row.getCells().get(4).getFirstParagraph().appendChild(newSec7Run4);

				// CMD
				vehSec7Row.getCells().get(5).getFirstParagraph().removeAllChildren();
				Run newSec7Run5 = new Run(doc, vehRecord.get("CMD"));
				newSec7Run5.getFont().setName("Arial");
				newSec7Run5.getFont().setSize(7);
				vehSec7Row.getCells().get(5).getFirstParagraph().appendChild(newSec7Run5);

				// CMPr
				vehSec7Row.getCells().get(6).getFirstParagraph().removeAllChildren();
				Run newSec7Run6 = new Run(doc, vehRecord.get("CMPr"));
				newSec7Run6.getFont().setName("Arial");
				newSec7Run6.getFont().setSize(7);
				vehSec7Row.getCells().get(6).getFirstParagraph().appendChild(newSec7Run6);

				// SPD
				vehSec7Row.getCells().get(7).getFirstParagraph().removeAllChildren();
				Run newSec7Run7 = new Run(doc, vehRecord.get("SPD"));
				newSec7Run7.getFont().setName("Arial");
				newSec7Run7.getFont().setSize(7);
				vehSec7Row.getCells().get(7).getFirstParagraph().appendChild(newSec7Run7);

				// SPPr
				vehSec7Row.getCells().get(8).getFirstParagraph().removeAllChildren();
				Run newSec7Run8 = new Run(doc, vehRecord.get("SPPr"));
				newSec7Run8.getFont().setName("Arial");
				newSec7Run8.getFont().setSize(7);
				vehSec7Row.getCells().get(8).getFirstParagraph().appendChild(newSec7Run8);

				// EndList
				vehSec7Row.getCells().get(9).getFirstParagraph().removeAllChildren();
				Run newSec7Run9 = new Run(doc, vehRecord.get("EndList"));
				newSec7Run9.getFont().setName("Arial");
				newSec7Run9.getFont().setSize(7);
				vehSec7Row.getCells().get(9).getFirstParagraph().appendChild(newSec7Run9);

				// EndPr
				vehSec7Row.getCells().get(10).getFirstParagraph().removeAllChildren();
				Run newSec7Run10 = new Run(doc, vehRecord.get("EndPr"));
				newSec7Run10.getFont().setName("Arial");
				newSec7Run10.getFont().setSize(7);
				vehSec7Row.getCells().get(10).getFirstParagraph().appendChild(newSec7Run10);

				// VehPrem
				vehSec7Row.getCells().get(11).getFirstParagraph().removeAllChildren();
				Run newSec7Run11 = new Run(doc, vehRecord.get("VehPrem"));
				newSec7Run11.getFont().setName("Arial");
				newSec7Run11.getFont().setSize(7);
				vehSec7Row.getCells().get(11).getFirstParagraph().appendChild(newSec7Run11);

				rowS7Idx++;
				
				if (vehIdx % 13 == 0) {
					// Set Liability Limit header
					String limitStr;
					if (totalLiabPr == 0) {
						limitStr = "$0";
					} else {
						limitStr = "$" + limit;
					}

					table.getRows().get(5).getCells().get(7).getFirstParagraph().removeAllChildren();
					Run limitRun = new Run(doc, limitStr);
					limitRun.getFont().setName("Arial");
					limitRun.getFont().setSize(7);
					limitRun.getFont().setBold(true);
					table.getRows().get(5).getCells().get(7).getFirstParagraph().appendChild(limitRun);

					// Set Total Premium
					table.getRows().get(40).getCells().get(2).getFirstParagraph().removeAllChildren();
					Run totPremRun = new Run(doc, "$" + numberFormat(totPrem));
					totPremRun.getFont().setName("Arial");
					totPremRun.getFont().setSize(8);
					totPremRun.getFont().setBold(true);
					table.getRows().get(40).getCells().get(2).getFirstParagraph().appendChild(totPremRun);

					// Insert new section
					doc.insertAfter(sec, refSection);
					refSection = sec;
					sec = (Section) templateSection.deepClone(true);
					table = sec.getBody().getTables().get(0);
					rowIdx = 7;
					rowS7Idx = 26;
					totalLiabPr = 0.0;
					totPrem = 0.0;
				}
				vehIdx++;
			}
			
		}
		doc.removeChild(templateSection);
	}

	private String orderedEndList(List<Endorsement> endList) {
		StringBuilder endListSB = new StringBuilder();
		HashMap<Integer, String> endMap = new HashMap<Integer, String>();
		for (Endorsement end : endList) {
			String endCode = end.getEndorsementCd().replace("OPCF", "");

			switch (end.getEndorsementCd()) {
			case "OPCF2":
				endMap.put(0, endCode);
				break;
			case "OPCF5":
				endMap.put(1, endCode);
				break;
			case "OPCF8":
				endMap.put(2, endCode);
				break;
			case "OPCF9":
				endMap.put(3, endCode);
				break;
			case "OPCF13C":
				endMap.put(4, endCode);
				break;
			case "OPCF19":
				endMap.put(5, endCode);
				break;
			case "OPCF20":
				endMap.put(6, endCode);
				break;
			case "OPCF21A":
				endMap.put(7, endCode);
				break;
			case "OPCF21B":
				endMap.put(8, endCode);
				break;
			case "OPCF23A":
				endMap.put(9, endCode);
				break;
			case "OPCF25A":
				endMap.put(10, endCode);
				break;
			case "OPCF27":
				endMap.put(11, endCode);
				break;
			case "OPCF27B":
				endMap.put(12, endCode);
				break;
			case "OPCF28":
				endMap.put(13, endCode);
				break;
			case "OPCF28A":
				endMap.put(14, endCode);
				break;
			case "OPCF30":
				endMap.put(15, endCode);
				break;
			case "OPCF31":
				endMap.put(16, endCode);
				break;
			case "OPCF38":
				endMap.put(17, endCode);
				break;
			case "OPCF40":
				endMap.put(18, endCode);
				break;
			case "OPCF43":
				endMap.put(19, "43/43A");
				break;
			case "OPCF44R":
				endMap.put(20, endCode);
				break;
			case "OPCF47":
				endMap.put(21, endCode);
				break;
			case "OPCF48":
				endMap.put(22, endCode);
				break;
			}

		}

		for (int i = 0; i <= 22; i++) {
			if (endMap.get(i) != null) {
				if (endListSB.length() > 0) {
					endListSB.append(", ");
				}
				endListSB.append(endMap.get(i));
			}
		}

		return endListSB.toString();
	}
	
}
