package com.echelon.services.bordereau;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.services.exporter.Exporter;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.prodconf.ConfigConstants;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.server.StreamResource;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class GridViewCreator {
	final static BordereauxService service = new BordereauxService();

	public static StreamResource createGridWithListDataProviderDemo(List<PolicyTransaction> pt, String delimiter, boolean summary, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) throws Exception {
		return createGridDemo(pt, delimiter, summary, ptAllList, policyProcessing);
	}

	private static StreamResource createGridDemo(List<PolicyTransaction> pt, String delimiter, boolean summary, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) throws Exception {


		
		//Getting product code
		String productCode = null;
		if (pt != null && pt.size() > 0 && pt.get(0) != null && pt.get(0).getPolicyVersion() != null) {
			productCode = pt.get(0).getPolicyVersion().getInsurancePolicy().getProductCd();
		}
		
		
		Grid<Bordereaux> grid = new Grid<>();
		Map<Column<Bordereaux>, String> gridHeaderMap = new HashMap<>();;
		
		
		if (productCode != null && productCode.equals(ConfigConstants.PRODUCTCODE_TOWING)) {//For towing print columns from GridViewTowing View

			GridViewTowing gridTowing = new GridViewTowing();
			
			//Create columns for the file
			grid = gridTowing.createColumns();

			//Getting data for the Bordereau
			setupListDataProviderForGrid(grid, pt, ptAllList, policyProcessing);
			
			//Mapping all data to those columns 
			gridHeaderMap = gridTowing.mappingGridColumns(grid);
			
		} else if (productCode != null && productCode.equals(ConfigConstants.PRODUCTCODE_LHT)) {//For LHT print just the columns from GridViewLhtOn Definition
			GridViewLhtOn gridLhtOn = new GridViewLhtOn();
			
			//Create columns for the file
			grid = gridLhtOn.createColumns();

			//Getting data for the Bordereau
			setupListDataProviderForGrid(grid, pt, ptAllList,policyProcessing);
			
			//Mapping all data to those columns 
			gridHeaderMap = gridLhtOn.mappingGridColumns(grid);
			
		} else if (productCode != null && productCode.equals(ConfigConstants.PRODUCTCODE_TOWING_NONFLEET)) {//For Non-Fleet print just the columns from GridViewNonFleet Definition
			GridViewNonFleet gridNonFleet = new GridViewNonFleet();
			
			//Create columns for the file
			grid = gridNonFleet.createColumns();

			//Getting data for the Bordereau
			setupListDataProviderForGrid(grid, pt, ptAllList, policyProcessing);
			
			//Mapping all data to those columns 
			gridHeaderMap = gridNonFleet.mappingGridColumns(grid);


		} else {//For the rest of the cases print all columns defined in the GridViewDefault java file

			
			GridViewDefault gridDefault = new GridViewDefault();
			
			//Create columns for the file
			grid = gridDefault.createColumns();

			//Getting data for the Bordereau
			setupListDataProviderForGrid(grid, pt, ptAllList, policyProcessing);
			
			//Mapping all data to those columns 
			gridHeaderMap = gridDefault.mappingGridColumns(grid);

		}
		

		String fileName = getFileName(summary, delimiter, pt, productCode);

		StreamResource downloadAsCSV = new StreamResource(fileName, Exporter.exportAsCSV(grid,gridHeaderMap, delimiter));
		
		
		return downloadAsCSV;
	}

	private static void setupListDataProviderForGrid(Grid<Bordereaux> grid, List<PolicyTransaction> pt, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) throws Exception {
		ListDataProvider<Bordereaux> listDataProvider = DataProvider
				.fromStream(service.getBordereauxInformation(pt, ptAllList, policyProcessing));
		//listDataProvider.setSortOrder(Bordereaux::getRISKID, SortDirection.ASCENDING);
		grid.setDataProvider(listDataProvider);

	}
	
	
	public static String getFileName(boolean summary, String delimiter, List<PolicyTransaction> pt, String productCode) {

		String name = "";
		
		//Making the csv file to be downloaded in the customer side
		String pattern = "MMddyyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String dateLabel = simpleDateFormat.format(new Date());
		
		if (productCode != null && productCode.equals(ConfigConstants.PRODUCTCODE_LHT)) {
			if (summary) {

				if (delimiter.equals("tab")) {
					name = "LHT002_Summary_"+dateLabel+"_LO"+".txt";
				}else {
					name = "LHT002_Summary_"+dateLabel+"_LO"+".csv";
				}
			}else {
				if (pt != null && pt.size() > 0 ) {
					if (delimiter.equals("tab")) {
						name = "LHT002_"+pt.get(0).getPolicyTerm().getInsurancePolicy().getBasePolicyNum()+"_"+dateLabel+"_LO"+".txt";
					}else {
						name = "LHT002_"+pt.get(0).getPolicyTerm().getInsurancePolicy().getBasePolicyNum()+"_"+dateLabel+"_LO"+".csv";
					}
				} else {
					name = "LHT002_"+dateLabel+"_LO"+".csv";
				}
					
			}
		}else {
			if (summary) {

				if (delimiter.equals("tab")) {
					name = "LHB001_Summary_"+dateLabel+"_TO"+".txt";
				}else {
					name = "LHB001_Summary_"+dateLabel+"_TO"+".csv";
				}
			}else {
				if (pt != null && pt.size() > 0 ) {
					if (delimiter.equals("tab")) {
						name = "LHB001_"+pt.get(0).getPolicyTerm().getInsurancePolicy().getBasePolicyNum()+"_"+dateLabel+"_TO"+".txt";
					}else {
						name = "LHB001_"+pt.get(0).getPolicyTerm().getInsurancePolicy().getBasePolicyNum()+"_"+dateLabel+"_TO"+".csv";
					}
				} else {
					name = "LHB001_"+dateLabel+"_TO"+".csv";
				}
					
			}
		}

		return name;
	}
	
}
