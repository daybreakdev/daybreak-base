package com.echelon.services.bordereau;

import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamResource;

public class DSPolicyBordereau extends VerticalLayout {

	public DSPolicyBordereau() {

	}

	public StreamResource downloadFile(List<PolicyTransaction> pt, String delimiter, boolean summary, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) {
		StreamResource normalGrid = null;
		try {
			normalGrid = GridViewCreator.createGridWithListDataProviderDemo(pt, delimiter, summary, ptAllList, policyProcessing);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return normalGrid;
		
	}
}
