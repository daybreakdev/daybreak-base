package com.echelon.services.bordereau;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class Bordereaux {
	private Integer termID;
	private Integer transactionID;
	private String policyNumber;
	private LocalDate policyEffectiveDate;
	private LocalDate policyExpiryDate;
	private LocalDate transactionEffectiveDate;
	private String transactionType;
	private String policyFleetIndicator;
	private String policyRiskProvince;
	private String policyRSPIndicator;
	private String policyNewBusinessIndicator;
	private Integer policySection3Limit;
	private String insuredRIN;
	private String insuredName;
	private String insuredStreet;
	private String insuredCity;
	private String insuredProvince;
	private String insuredPostalCode;
	private String masterBrokerName;
	private String masterBrokerStreet;
	private String masterBrokerCity;
	private String masterBrokerPostalCode;
	private String masterBrokerNumber;
	
	private String subBrokerName;
	private String subBrokerStreet;
	private String subBrokerCity;
	private String subBrokerPostalCode;
	private String subBrokerNumber;
	
	private Integer unitNumber;
	private Integer unitCount;
	private String unitStatCode;
	private String unitRatingTerritory;
	private String unitVehicleType;
	private String unitTrailerType;
	private String trailerIndicator;
	private String unitDescription;
	private String unitVehicleClass;
	private String unitDriverRecord;
	private String unitMake;
	private Integer unitYear;
	private String unitVIN;
	private Double unitListPriceNew;
	private String unitLocationExposure;
	private Double oPCF2Prem;
	private String oPCF4AType;
	private String oPCF4APrem;
	private String oPCF4BType;
	private String oPCF4BPrem;
	private String oPCF5Indicator;
	private String oPCF5CPrem;
	private String oPCF5DPrem;
	private String oPCF5GIndicator;
	private String oPCF8Deductible;
	private String oPCF9Indicator;
	private String oPCF13CIndicator;
	private Integer oPCF19ValueVehicle;
	private String oPCF20Prem;
	private String oPCF21AReceipts;
	private String oPCF21APrem;
	private String oPCF21BIndicator;
	private String oPCF23AIndicator;
	private String oPCF23BPrem;
	private String oPCF25AIndicator;
	private String oPCF25APrem;
	private String oPCF25A145Indicator;
	private Integer oPCF27Limit;
	private Integer oPCF27Deductible;
	private String oPCF27Prem;
	private String oPCF27BDeductible;
	private String oPCF27BLimit;
	private String oPCF27BPrem;
	private String oPCF28Indicator;
	private String oPCF28AIndicator;
	private String oPCF30Indicator;
	private String oPCF31Indicator;
	private String oPCF38Limit;
	private String oPCF38Prem;
	private String oPCF40Deductible;
	private String oPCF43Prem;
	private String oPCF43APrem;
	private String oPCF44Prem;
	private String oPCF47Indicator;
	private String oPCF48Prem;
	private String dealerPlateCoverageIndicator;
	private String eON20ATravelExpensesDeductible;
	private String eON20ATravelExpensesPrem;
	private String eON20GLossIncomeDeductible;
	private String eON20GLossIncomePrem;
	private String eON104BuydownDeductible;
	private String eON104BuydownDeductiblePrem;
	private String conditionalReleaseBondPrem;
	private String autoEndtotalPrem;
	private String autoSection3BITotalPrem;
	private String autoSection3PDTotalPrem;
	private String autoSection4ABTotalPrem;
	private String autoSection5UATotalPrem;
	private String autoSection6DCPDDeductible;
	private String autoSection6DCPDTotalPrem;
	private String autoSection7CCLDeductible;
	private String autoSection7CCLTotalPrem;
	private String autoSection7BCMDeductible;
	private String autoSection7BCMTotalPrem;
	private String autoSection7DAPDeductible;
	private String autoSection7DAPTotalPrem;
	private String autoSection7ASPDeductible;
	private String autoSection7ASPTotalPrem;
	private String autoABIncomeReplacementTotalPrem;
	private String autoABIncomeReplacementIndicator;
	private String autoABIncomeReplacementLimit;
	private String autoABDeathAndFuneralTotalPrem;
	private String autoABDeathAndFuneralIndicator;
	private String autoABMedicalRehabAndATCTotalPrem;
	private String autoABMedicalRehabAndATCIndicator;
	private String autoABMedicalRehabAndATCLimit;
	private String autoABCATImpairementTotalPrem;
	private String autoABCATImpairementIndicator;
	private String autoABCATImpairementLimit;
	private String autoABCaregiverHousekeeperTotalPrem;
	private String autoABCaregiverHousekeeperIndicator;
	private String autoABDependentCareTotalPrem;
	private String autoABDependentCareIndicator;
	private String autoABIndexationTotalPrem;
	private String autoABIndexationIndicator;
	private String autoWOEndtTotalPrem;
	private String contentsPersonalEffectsLimit;
	private String contentsPersonalEffectsDeductible;
	private String contentsPersonalEffectsPremium;
	private String cargoLimit;
	private String cargoDeductible;
	private String cargoAdditionalLimitPrem;
	private String accruedFreightChargesLimit;
	private String accruedFreightChargesPremium;
	private String contingentCargoLimit;
	private String contingentCargoDeductible;
	private String contingentCargoPrem;
	private String debrisRemovalLimit;
	private String debrisRemovalPremium;
	private String equipmentLimit;
	private String equipmentPremium;
	private String identityMisrepresentationLimit;
	private String identityMisrepresentationPremium;
	private String temporaryStorageLimit;
	private String temporaryStoragePremium;
	private String tripTransitCargoAdditionalLimit;
	private String tripTransitCargoPrem;
	private String qE4201ROwnersFormLimit;
	private String qE4201ROwnersFormDeductible;
	private String qE4201ROwnersFormPrem;
	private String eON141ChangetoDeductibleIndicator;
	private String dangerousGoodsExclusionIndicator;
	private String dangerousGoodsPartialLoadIndicator;
	private String dangerousGoodsProductClassesIndicator;
	private String forestryExclusionIndicator;
	private String cargoExclusionIndicator;
	private String cargoGeneralEndorsementPrem;
	private String cargoTotalPrem;
	private String cGLDeductible;
	private String cGLLimit;
	private String tenantsLegalLiabilityStreet;
	private String tenantsLegalLiabilityCity;
	private String tenantsLegalLiabilityPostalCode;
	private String tenantsLegalLiabilityLimit;
	private String tenantsLegalLiabilityDeductible;
	private String tenantsLegalLiabilityPrem;
	private String qE4023NWarehousemanLegalLiabilityLimit;
	private String qE4023NWarehousemanLegalLiabilityDeductible;
	private String qE4023NWarehousemanLegalLiabilityPrem;
	private String qE2033NEmployeeBenefitsExtensionLimit;
	private String qE2033NEmployeeBenefitsExtensionDeductible;
	private String qE2033NEmployeeBenefitsExtensionPrem;
	private String qE2313NLimitedPollutionLiabilityLimit;
	private String qE2313NLimitedPollutionLiabilityDeductible;
	private String qE2313NLimitedPollutionLiabilityPrem;
	private String sPF6NonOwnedAutomobileIndicator;
	private String sEF94LegalLiabilityIndicator;
	private String sEF96ContractualLiabilityIndicator;
	private String sEF99LeasedVehicleIndicator;
	private String garageLiabilityExtensionIndicator;
	private String qE2345NTotalPollutionExclusionIndicator;
	private String qE2345NTotalPollutionExclusionPremium;
	private String mobileEquipmentLimit;
	private String mobileEquipmentDeductible;
	private String mobileEquipmentPrem;
	private String cGLGeneralEndorsementPrem;
	private String cGLTotalPrem;
	private String businessOperations;
	private String locationAddress;
	private String locationCity;
	private String locationProvince;
	private String locationPostalCode;
	private String locationIncludeBuilding;
	private String locationIncludeLot;
	private Integer locationTotalFullTimeEmployees;
	private Integer locationTotalPartTimeEmployees;
	private Integer qE4037RContentsLimit;
	private Integer qE4037RContentsDeductible;
	private Double qE4037RContentsPrem;
	private String qE4037RPropertyExtensionIndicator;
	private String oAP4Section1Limit;
	private String oAP4Section1BIPrem;
	private String oAP4Section1PDPrem;
	private String oAP4Section2ABPrem;
	private String oAP4Section3UAPrem;
	private String oAP4Section4DCPDDeductible;
	private String oAP4Section4DCPDPrem;
	private String oAP4Section61CLLimit;
	private Integer oAP4Section511CLDeductible;
	private Double oAP4Section511CLPremium;
	private Integer oAP4Section512CMLimit;
	private Integer oAP4Section512CMDeductible;
	private Double oAP4Section512CMPremium;
	private Integer oAP4Section513SPLimit;
	private Integer oAP4Section513SPDeductible;
	private Double oAP4Section513SPPremium;
	private Integer oAP4Section514SPLimit;
	private Integer oAP4Section514SPDeductible;
	private Double oAP4Section514SPPremium;
	private String oAP4Section61CLDeductible;
	private String oAP4Section61CLPrem;
	private String oAP4Section64SPLimit;
	private String oAP4Section64SPDeductible;
	private Integer oAP4Section64SPMaximumVehicles;
	private String oAP4Section64SPPrem;
	private String oEF71ExcludingOwnedAutomobilesIndicator;
	
	private String oEF72MultipleAlterationPremium;
	private String oEF73ExcludingFinancedIndicator;
	private String oEF74OpenLotTheftPremium;
	
	private String oEF75OpenLotTheftPrem;
	
	private String oEF76AdditionalInsuredPremium;
	
	private String oEF77CustomerAutoPrem;
	
	private String oEF78AExcludedDriverEndorsementIndicator;
	private String oEF78ReductionCoverageNamedPersonsIndicator;
	private String oEF79FireDeductibleOwnedAutomobilesIndicator;
	private String oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium;
	private String oEF81GarageFamilyProtectionLimit;
	
	private String oEF81GarageFamilyProtectionPrem;
	
	private String oEF82LiabilityDamagedNonOwnedLimit;
	private String oEF82LiabilityDamagedNonOwnedDeductible;
	private String oEF82LiabilityDamagedNonOwnedPremium;
	private String oEF83AutomobileTransportationPremium;
	private String oEF85FinalPremiumComputationIndicator;
	private String oEF86CustomersAutomobilesFireDeductibleIndicator;
	
	private String oAP4TotalPrem;
	private String eON101BlanketLessorsIndicator;
	private String eON102AdditionalInsuredIndividualIndicator;
	private String eON103AdditionalInsuredOOIndicator;
	private String eON109InsurableInterestDeletedIndicator;
	private String eON110PerOccuranceDeductibleIndicator;
	private String eON110PerOccuranceDeductible;
	private String eON111ProfitSharingIndicator;
	private String eON112ProfitSharingAdjustmentPrem;
	private String eON114NamedInsuredIndicator;
	private String eON115OPCF21AReceiptsBasisAdjustmentPrem;
	private String eON116OPCF21B5050BasisAdjustmentPrem;
	private String eON117OPCF21BProRataBasisAdjustmentPrem;
	private String eON119RegisteredOwnerIndicator;
	private String eON121PolicyCancellationPrem;
	private String eON122PolicyCancellationAdjustementPrem;
	private String indemnityAgreementIndicator;
	private String garageServicePlateCoverageIndicator;
	private Integer garageServicePlateCount;
	
	private String netReinsurancePremium;
	private String reinsurerCode;
	private String rISKID;
	private String programCode;
	private String product;
	private String blankEndorsement;
	private String totalPrem;
	private Double brokerCommissionAmount;

	//I commented this out because the constructor has so many parameters and it sent the following error: Too many parameters, parameters are exceeding the limit of 255 words eligible for method parameters
	/*
	public Bordereaux(Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet, String insuredCity,
			String insuredProvince, String insuredPostalCode, String masterBrokerName, String masterBrokerStreet,
			String masterBrokerCity, String masterBrokerPostalCode, String masterBrokerNumber, String subBrokerName, String subBrokerStreet,  String subBrokerCity, String subBrokerPostalCode, String subBrokerNumber, Integer unitNumber,
			Integer unitCount, String unitStatCode, String unitRatingTerritory, String unitVehicleType,
			String unitTrailerType, String trailerIndicator, String unitDescription, String unitVehicleClass, String unitDriverRecord,
			String unitMake, Integer unitYear, String unitVIN, Double unitListPriceNew, String unitLocationExposure,
			Double oPCF2Prem, String oPCF4AType, String oPCF4APrem, String oPCF4BType, String oPCF4BPrem,
			String oPCF5Indicator, String oPCF5CPrem, String oPCF5DPrem, String oPCF5GIndicator, String oPCF8Deductible,
			String oPCF9Indicator, String oPCF13CIndicator, Integer oPCF19ValueVehicle, String oPCF20Prem,
			String oPCF21AReceipts, String oPCF21APrem, String oPCF21BIndicator, String oPCF23AIndicator,
			String oPCF23BPrem, String oPCF25AIndicator, String oPCF25APrem, String oPCF25A145Indicator, Integer oPCF27Limit, Integer oPCF27Deductible,
			String oPCF27Prem, String oPCF27BDeductible, String oPCF27BLimit, String oPCF27BPrem,
			String oPCF28Indicator, String oPCF28AIndicator, String oPCF30Indicator, String oPCF31Indicator,
			String oPCF38Limit, String oPCF38Prem, String oPCF40Deductible, String oPCF43Prem, String oPCF43APrem,
			String oPCF44Prem, String oPCF47Indicator, String oPCF48Prem, String dealerPlateCoverageIndicator, String eON20ATravelExpensesDeductible,
			String eON20ATravelExpensesPrem, String eON20GLossIncomeDeductible, String eON20GLossIncomePrem,
			String eON104BuydownDeductible, String eON104BuydownDeductiblePrem, String conditionalReleaseBondPrem,
			String autoEndtotalPrem, String autoSection3BITotalPrem, String autoSection3PDTotalPrem,
			String autoSection4ABTotalPrem, String autoSection5UATotalPrem, String autoSection6DCPDDeductible,
			String autoSection6DCPDTotalPrem, String autoSection7CCLDeductible, String autoSection7CCLTotalPrem,
			String autoSection7BCMDeductible, String autoSection7BCMTotalPrem, String autoSection7DAPDeductible,
			String autoSection7DAPTotalPrem, String autoSection7ASPDeductible, String autoSection7ASPTotalPrem,
			String autoABIncomeReplacementTotalPrem, String autoABIncomeReplacementIndicator, String autoABIncomeReplacementLimit,
			String autoABDeathAndFuneralTotalPrem, String autoABDeathAndFuneralIndicator, String autoABMedicalRehabAndATCTotalPrem,
			String autoABMedicalRehabAndATCIndicator, String autoABMedicalRehabAndATCLimit, String autoABCATImpairementTotalPrem,
			String autoABCATImpairementIndicator, String autoABCATImpairementLimit, String autoABCaregiverHousekeeperTotalPrem,
			String autoABCaregiverHousekeeperIndicator, String autoABDependentCareTotalPrem, String autoABDependentCareIndicator,
			String autoABIndexationTotalPrem, String autoABIndexationIndicator,
			String autoWOEndtTotalPrem, String contentsPersonalEffectsLimit, String contentsPersonalEffectsDeductible, String contentsPersonalEffectsPremium,
			String cargoLimit, String cargoDeductible, String cargoAdditionalLimitPrem, String accruedFreightChargesLimit, String accruedFreightChargesPremium,
			String contingentCargoLimit, String contingentCargoDeductible, String contingentCargoPrem,
			String debrisRemovalLimit, String debrisRemovalPremium,
			String equipmentLimit, String equipmentPremium,
			String identityMisrepresentationLimit, String identityMisrepresentationPremium,
			String temporaryStorageLimit, String temporaryStoragePremium,
			String tripTransitCargoAdditionalLimit, String tripTransitCargoPrem, String qE4201ROwnersFormLimit, String qE4201ROwnersFormDeductible, String qE4201ROwnersFormPrem,
			String eON141ChangetoDeductibleIndicator, String dangerousGoodsExclusionIndicator,
			String dangerousGoodsPartialLoadIndicator, String dangerousGoodsProductClassesIndicator,
			String forestryExclusionIndicator, String cargoExclusionIndicator, String cargoGeneralEndorsementPrem,
			String cargoTotalPrem, String cGLDeductible, String cGLLimit,String tenantsLegalLiabilityStreet,
			String tenantsLegalLiabilityCity, String tenantsLegalLiabilityPostalCode, String tenantsLegalLiabilityLimit,
			String tenantsLegalLiabilityDeductible, String tenantsLegalLiabilityPrem,
			String qE4023NWarehousemanLegalLiabilityLimit, String qE4023NWarehousemanLegalLiabilityDeductible,
			String qE4023NWarehousemanLegalLiabilityPrem, String qE2033NEmployeeBenefitsExtensionLimit,
			String qE2033NEmployeeBenefitsExtensionDeductible, String qE2033NEmployeeBenefitsExtensionPrem,
			String qE2313NLimitedPollutionLiabilityLimit, String qE2313NLimitedPollutionLiabilityDeductible,
			String qE2313NLimitedPollutionLiabilityPrem, String sPF6NonOwnedAutomobileIndicator,
			String sEF94LegalLiabilityIndicator, String sEF96ContractualLiabilityIndicator,
			String sEF99LeasedVehicleIndicator, String garageLiabilityExtensionIndicator, String qE2345NTotalPollutionExclusionIndicator, String qE2345NTotalPollutionExclusionPremium,
			String mobileEquipmentLimit, String mobileEquipmentDeductible, String mobileEquipmentPrem,
			String cGLGeneralEndorsementPrem, String cGLTotalPrem, 
			String businessOperations, String locationAddress, String locationCity, String locationProvince, String locationPostalCode, 
			String locationIncludeBuilding, String locationIncludeLot, Integer locationTotalFullTimeEmployees, 
			Integer locationTotalPartTimeEmployees, Integer qE4037RContentsLimit,
			Integer qE4037RContentsDeductible, Double qE4037RContentsPrem, String qE4037RPropertyExtensionIndicator,
			String oAP4Section1Limit, String oAP4Section1BIPrem, String oAP4Section1PDPrem, String oAP4Section2ABPrem,
			String oAP4Section3UAPrem, String oAP4Section4DCPDDeductible, String oAP4Section4DCPDPrem, String oAP4Section61CLLimit, 
			Integer oAP4Section511CLDeductible, Double oAP4Section511CLPremium, Integer oAP4Section512CMLimit, Integer oAP4Section512CMDeductible, 
			Double oAP4Section512CMPremium, Integer oAP4Section513SPLimit, Integer oAP4Section513SPDeductible, Double oAP4Section513SPPremium, 
			Integer oAP4Section514SPLimit, Integer oAP4Section514SPDeductible, Double oAP4Section514SPPremium, 
			String oAP4Section61CLDeductible, String oAP4Section61CLPrem,
			String oAP4Section64SPLimit, String oAP4Section64SPDeductible, Integer oAP4Section64SPMaximumVehicles, String oAP4Section64SPPrem,
			String oEF71ExcludingOwnedAutomobilesIndicator, String oEF72MultipleAlterationPremium, String oEF73ExcludingFinancedIndicator, String oEF74OpenLotTheftPremium, 
			String oEF75OpenLotTheftPrem, String oEF76AdditionalInsuredPremium, String oEF77CustomerAutoPrem, String oEF78AExcludedDriverEndorsementIndicator,
			String oEF78ReductionCoverageNamedPersonsIndicator, String oEF79FireDeductibleOwnedAutomobilesIndicator, String oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium,
			String oEF81GarageFamilyProtectionLimit, String oEF81GarageFamilyProtectionPrem, 
			String oEF82LiabilityDamagedNonOwnedLimit, String oEF82LiabilityDamagedNonOwnedDeductible, String oEF82LiabilityDamagedNonOwnedPremium, 
			String oEF83AutomobileTransportationPremium, String oEF85FinalPremiumComputationIndicator, String oEF86CustomersAutomobilesFireDeductibleIndicator,
			String oAP4TotalPrem, String eON101BlanketLessorsIndicator,
			String eON102AdditionalInsuredIndividualIndicator, String eON103AdditionalInsuredOOIndicator,
			String eON109InsurableInterestDeletedIndicator, String eON110PerOccuranceDeductibleIndicator,
			String eON110PerOccuranceDeductible, String eON111ProfitSharingIndicator,
			String eON112ProfitSharingAdjustmentPrem, String eON114NamedInsuredIndicator,
			String eON115OPCF21AReceiptsBasisAdjustmentPrem, String eON116OPCF21B5050BasisAdjustmentPrem,
			String eON117OPCF21BProRataBasisAdjustmentPrem, String eON119RegisteredOwnerIndicator,
			String eON121PolicyCancellationPrem, String eON122PolicyCancellationAdjustementPrem, 
			String indemnityAgreementIndicator, String garageServicePlateCoverageIndicator, Integer garageServicePlateCount,
			
			String netReinsurancePremium, String reinsurerCode, String rISKID,
			String programCode, String product, String blankEndorsement, String totalPrem,
			Double brokerCommissionAmount) {
		super();
		this.termID = termID;
		this.transactionID = transactionID;
		this.policyNumber = policyNumber;
		this.policyEffectiveDate = policyEffectiveDate;
		this.policyExpiryDate = policyExpiryDate;
		this.transactionEffectiveDate = transactionEffectiveDate;
		this.transactionType = transactionType;
		this.policyFleetIndicator = policyFleetIndicator;
		this.policyRiskProvince = policyRiskProvince;
		this.policyRSPIndicator = policyRSPIndicator;
		this.policyNewBusinessIndicator = policyNewBusinessIndicator;
		this.policySection3Limit = policySection3Limit;
		this.insuredRIN = insuredRIN;
		this.insuredName = insuredName;
		this.insuredStreet = insuredStreet;
		this.insuredCity = insuredCity;
		this.insuredProvince = insuredProvince;
		this.insuredPostalCode = insuredPostalCode;
		this.masterBrokerName = masterBrokerName;
		this.masterBrokerStreet = masterBrokerStreet;
		this.masterBrokerCity = masterBrokerCity;
		this.masterBrokerPostalCode = masterBrokerPostalCode;
		this.masterBrokerNumber = masterBrokerNumber;
		this.subBrokerName = subBrokerName;
		this.subBrokerStreet = subBrokerStreet;
		this.subBrokerCity = subBrokerCity;
		this.subBrokerPostalCode = subBrokerPostalCode;
		this.subBrokerNumber = subBrokerNumber;
		this.unitNumber = unitNumber;
		this.unitCount = unitCount;
		this.unitStatCode = unitStatCode;
		this.unitRatingTerritory = unitRatingTerritory;
		this.unitVehicleType = unitVehicleType;
		this.unitTrailerType = unitTrailerType;
		this.trailerIndicator = trailerIndicator;
		this.unitDescription = unitDescription;
		this.unitVehicleClass = unitVehicleClass;
		this.unitDriverRecord = unitDriverRecord;
		this.unitMake = unitMake;
		this.unitYear = unitYear;
		this.unitVIN = unitVIN;
		this.unitListPriceNew = unitListPriceNew;
		this.unitLocationExposure = unitLocationExposure;
		this.oPCF2Prem = oPCF2Prem;
		this.oPCF4AType = oPCF4AType;
		this.oPCF4APrem = oPCF4APrem;
		this.oPCF4BType = oPCF4BType;
		this.oPCF4BPrem = oPCF4BPrem;
		this.oPCF5Indicator = oPCF5Indicator;
		this.oPCF5CPrem = oPCF5CPrem;
		this.oPCF5DPrem = oPCF5DPrem;
		this.oPCF5GIndicator = oPCF5GIndicator;
		this.oPCF8Deductible = oPCF8Deductible;
		this.oPCF9Indicator = oPCF9Indicator;
		this.oPCF13CIndicator = oPCF13CIndicator;
		this.oPCF19ValueVehicle = oPCF19ValueVehicle;
		this.oPCF20Prem = oPCF20Prem;
		this.oPCF21AReceipts = oPCF21AReceipts;
		this.oPCF21APrem = oPCF21APrem;
		this.oPCF21BIndicator = oPCF21BIndicator;
		this.oPCF23AIndicator = oPCF23AIndicator;
		this.oPCF23BPrem = oPCF23BPrem;
		this.oPCF25AIndicator = oPCF25AIndicator;
		this.oPCF25APrem = oPCF25APrem;
		this.oPCF25A145Indicator = oPCF25A145Indicator;
		this.oPCF27Limit = oPCF27Limit;
		this.oPCF27Deductible = oPCF27Deductible;
		this.oPCF27Prem = oPCF27Prem;
		this.oPCF27BDeductible = oPCF27BDeductible;
		this.oPCF27BLimit = oPCF27BLimit;
		this.oPCF27BPrem = oPCF27BPrem;
		this.oPCF28Indicator = oPCF28Indicator;
		this.oPCF28AIndicator = oPCF28AIndicator;
		this.oPCF30Indicator = oPCF30Indicator;
		this.oPCF31Indicator = oPCF31Indicator;
		this.oPCF38Limit = oPCF38Limit;
		this.oPCF38Prem = oPCF38Prem;
		this.oPCF40Deductible = oPCF40Deductible;
		this.oPCF43Prem = oPCF43Prem;
		this.oPCF43APrem = oPCF43APrem;
		this.oPCF44Prem = oPCF44Prem;
		this.oPCF47Indicator = oPCF47Indicator;
		this.oPCF48Prem = oPCF48Prem;
		this.dealerPlateCoverageIndicator=dealerPlateCoverageIndicator;
		this.eON20ATravelExpensesDeductible = eON20ATravelExpensesDeductible;
		this.eON20ATravelExpensesPrem = eON20ATravelExpensesPrem;
		this.eON20GLossIncomeDeductible = eON20GLossIncomeDeductible;
		this.eON20GLossIncomePrem = eON20GLossIncomePrem;
		this.eON104BuydownDeductible = eON104BuydownDeductible;
		this.eON104BuydownDeductiblePrem = eON104BuydownDeductiblePrem;
		this.conditionalReleaseBondPrem = conditionalReleaseBondPrem;
		this.autoEndtotalPrem = autoEndtotalPrem;
		this.autoSection3BITotalPrem = autoSection3BITotalPrem;
		this.autoSection3PDTotalPrem = autoSection3PDTotalPrem;
		this.autoSection4ABTotalPrem = autoSection4ABTotalPrem;
		this.autoSection5UATotalPrem = autoSection5UATotalPrem;
		this.autoSection6DCPDDeductible = autoSection6DCPDDeductible;
		this.autoSection6DCPDTotalPrem = autoSection6DCPDTotalPrem;
		this.autoSection7CCLDeductible = autoSection7CCLDeductible;
		this.autoSection7CCLTotalPrem = autoSection7CCLTotalPrem;
		this.autoSection7BCMDeductible = autoSection7BCMDeductible;
		this.autoSection7BCMTotalPrem = autoSection7BCMTotalPrem;
		this.autoSection7DAPDeductible = autoSection7DAPDeductible;
		this.autoSection7DAPTotalPrem = autoSection7DAPTotalPrem;
		this.autoSection7ASPDeductible = autoSection7ASPDeductible;
		this.autoSection7ASPTotalPrem = autoSection7ASPTotalPrem;
		this.autoABIncomeReplacementTotalPrem = autoABIncomeReplacementTotalPrem;
		this.autoABIncomeReplacementIndicator = autoABIncomeReplacementIndicator;
		this.autoABIncomeReplacementLimit = autoABIncomeReplacementLimit;
		this.autoABDeathAndFuneralTotalPrem = autoABDeathAndFuneralTotalPrem;
		this.autoABDeathAndFuneralIndicator = autoABDeathAndFuneralIndicator;
		this.autoABMedicalRehabAndATCTotalPrem = autoABMedicalRehabAndATCTotalPrem;
		this.autoABMedicalRehabAndATCIndicator = autoABMedicalRehabAndATCIndicator;
		this.autoABMedicalRehabAndATCLimit = autoABMedicalRehabAndATCLimit;
		this.autoABCATImpairementTotalPrem = autoABCATImpairementTotalPrem;
		this.autoABCATImpairementIndicator = autoABCATImpairementIndicator;
		this.autoABCATImpairementLimit = autoABCATImpairementLimit;
		this.autoABCaregiverHousekeeperTotalPrem = autoABCaregiverHousekeeperTotalPrem;
		this.autoABCaregiverHousekeeperIndicator = autoABCaregiverHousekeeperIndicator;
		this.autoABDependentCareTotalPrem = autoABDependentCareTotalPrem;
		this.autoABDependentCareIndicator = autoABDependentCareIndicator;
		this.autoABIndexationTotalPrem = autoABIndexationTotalPrem;
		this.autoABIndexationIndicator = autoABIndexationIndicator;
		this.autoWOEndtTotalPrem = autoWOEndtTotalPrem;
		this.contentsPersonalEffectsLimit = contentsPersonalEffectsLimit;
		this.contentsPersonalEffectsDeductible = contentsPersonalEffectsDeductible;
		this.contentsPersonalEffectsPremium = contentsPersonalEffectsPremium;
		this.cargoLimit = cargoLimit;
		this.cargoDeductible = cargoDeductible;
		this.cargoAdditionalLimitPrem = cargoAdditionalLimitPrem;
		this.accruedFreightChargesLimit=accruedFreightChargesLimit;
		this.accruedFreightChargesPremium=accruedFreightChargesPremium;
		this.contingentCargoLimit = contingentCargoLimit;
		this.contingentCargoDeductible = contingentCargoDeductible;
		this.contingentCargoPrem = contingentCargoPrem;
		this.debrisRemovalLimit = debrisRemovalLimit;
		this.debrisRemovalPremium = debrisRemovalPremium;
		this.equipmentLimit = equipmentLimit;
		this.equipmentPremium = equipmentPremium;
		this.identityMisrepresentationLimit = identityMisrepresentationLimit;
		this.identityMisrepresentationPremium = identityMisrepresentationPremium;
		this.temporaryStorageLimit = temporaryStorageLimit;
		this.temporaryStoragePremium = temporaryStoragePremium;
		this.tripTransitCargoAdditionalLimit = tripTransitCargoAdditionalLimit;
		this.tripTransitCargoPrem = tripTransitCargoPrem;
		this.qE4201ROwnersFormLimit=qE4201ROwnersFormLimit;
		this.qE4201ROwnersFormDeductible=qE4201ROwnersFormDeductible;
		this.qE4201ROwnersFormPrem = qE4201ROwnersFormPrem;
		this.eON141ChangetoDeductibleIndicator = eON141ChangetoDeductibleIndicator;
		this.dangerousGoodsExclusionIndicator = dangerousGoodsExclusionIndicator;
		this.dangerousGoodsPartialLoadIndicator = dangerousGoodsPartialLoadIndicator;
		this.dangerousGoodsProductClassesIndicator = dangerousGoodsProductClassesIndicator;
		this.forestryExclusionIndicator = forestryExclusionIndicator;
		this.cargoExclusionIndicator = cargoExclusionIndicator;
		this.cargoGeneralEndorsementPrem = cargoGeneralEndorsementPrem;
		this.cargoTotalPrem = cargoTotalPrem;
		this.cGLDeductible = cGLDeductible;
		this.cGLLimit = cGLLimit;
		this.tenantsLegalLiabilityStreet = tenantsLegalLiabilityStreet;
		this.tenantsLegalLiabilityCity = tenantsLegalLiabilityCity;
		this.tenantsLegalLiabilityPostalCode = tenantsLegalLiabilityPostalCode;
		this.tenantsLegalLiabilityLimit = tenantsLegalLiabilityLimit;
		this.tenantsLegalLiabilityDeductible = tenantsLegalLiabilityDeductible;
		this.tenantsLegalLiabilityPrem = tenantsLegalLiabilityPrem;
		this.qE4023NWarehousemanLegalLiabilityLimit = qE4023NWarehousemanLegalLiabilityLimit;
		this.qE4023NWarehousemanLegalLiabilityDeductible = qE4023NWarehousemanLegalLiabilityDeductible;
		this.qE4023NWarehousemanLegalLiabilityPrem = qE4023NWarehousemanLegalLiabilityPrem;
		this.qE2033NEmployeeBenefitsExtensionLimit = qE2033NEmployeeBenefitsExtensionLimit;
		this.qE2033NEmployeeBenefitsExtensionDeductible = qE2033NEmployeeBenefitsExtensionDeductible;
		this.qE2033NEmployeeBenefitsExtensionPrem = qE2033NEmployeeBenefitsExtensionPrem;
		this.qE2313NLimitedPollutionLiabilityLimit = qE2313NLimitedPollutionLiabilityLimit;
		this.qE2313NLimitedPollutionLiabilityDeductible = qE2313NLimitedPollutionLiabilityDeductible;
		this.qE2313NLimitedPollutionLiabilityPrem = qE2313NLimitedPollutionLiabilityPrem;
		this.sPF6NonOwnedAutomobileIndicator = sPF6NonOwnedAutomobileIndicator;
		this.sEF94LegalLiabilityIndicator = sEF94LegalLiabilityIndicator;
		this.sEF96ContractualLiabilityIndicator = sEF96ContractualLiabilityIndicator;
		this.sEF99LeasedVehicleIndicator = sEF99LeasedVehicleIndicator;
		this.garageLiabilityExtensionIndicator=garageLiabilityExtensionIndicator;
		this.qE2345NTotalPollutionExclusionIndicator = qE2345NTotalPollutionExclusionIndicator;
		this.qE2345NTotalPollutionExclusionPremium=qE2345NTotalPollutionExclusionPremium;
		this.mobileEquipmentLimit = mobileEquipmentLimit;
		this.mobileEquipmentDeductible = mobileEquipmentDeductible;
		this.mobileEquipmentPrem = mobileEquipmentPrem;
		this.cGLGeneralEndorsementPrem = cGLGeneralEndorsementPrem;
		this.cGLTotalPrem = cGLTotalPrem;
		this.businessOperations = businessOperations;
		this.locationAddress = locationAddress;
		this.locationCity = locationCity;
		this.locationProvince = locationProvince;
		this.locationPostalCode = locationPostalCode;
		this.locationIncludeBuilding = locationIncludeBuilding;
		this.locationIncludeLot = locationIncludeLot;
		this.locationTotalFullTimeEmployees = locationTotalFullTimeEmployees;
		this.locationTotalPartTimeEmployees = locationTotalPartTimeEmployees;
		this.qE4037RContentsLimit = qE4037RContentsLimit;
		this.qE4037RContentsDeductible = qE4037RContentsDeductible;
		this.qE4037RContentsPrem = qE4037RContentsPrem;
		this.qE4037RPropertyExtensionIndicator = qE4037RPropertyExtensionIndicator;
		this.oAP4Section1Limit = oAP4Section1Limit;
		this.oAP4Section1BIPrem = oAP4Section1BIPrem;
		this.oAP4Section1PDPrem = oAP4Section1PDPrem;
		this.oAP4Section2ABPrem = oAP4Section2ABPrem;
		this.oAP4Section3UAPrem = oAP4Section3UAPrem;
		this.oAP4Section4DCPDDeductible = oAP4Section4DCPDDeductible;
		this.oAP4Section4DCPDPrem = oAP4Section4DCPDPrem;
		this.oAP4Section61CLLimit = oAP4Section61CLLimit;
		this.oAP4Section511CLDeductible = oAP4Section511CLDeductible;
		this.oAP4Section511CLPremium = oAP4Section511CLPremium;
		this.oAP4Section512CMLimit = oAP4Section512CMLimit;
		this.oAP4Section512CMDeductible = oAP4Section512CMDeductible;
		this.oAP4Section512CMPremium = oAP4Section512CMPremium;
		this.oAP4Section513SPLimit = oAP4Section513SPLimit;
		this.oAP4Section513SPDeductible = oAP4Section513SPDeductible;
		this.oAP4Section513SPPremium = oAP4Section513SPPremium;
		this.oAP4Section514SPLimit = oAP4Section514SPLimit;
		this.oAP4Section514SPDeductible = oAP4Section514SPDeductible;
		this.oAP4Section514SPPremium = oAP4Section514SPPremium;
		this.oAP4Section61CLDeductible = oAP4Section61CLDeductible;
		this.oAP4Section61CLPrem = oAP4Section61CLPrem;
		this.oAP4Section64SPLimit = oAP4Section64SPLimit;
		this.oAP4Section64SPDeductible = oAP4Section64SPDeductible;
		this.oAP4Section64SPMaximumVehicles=oAP4Section64SPMaximumVehicles;
		this.oAP4Section64SPPrem = oAP4Section64SPPrem;
		this.oEF71ExcludingOwnedAutomobilesIndicator = oEF71ExcludingOwnedAutomobilesIndicator;
		
		this.oEF72MultipleAlterationPremium = oEF72MultipleAlterationPremium;
		this.oEF73ExcludingFinancedIndicator = oEF73ExcludingFinancedIndicator;
		this.oEF74OpenLotTheftPremium = oEF74OpenLotTheftPremium;
		this.oEF75OpenLotTheftPrem = oEF75OpenLotTheftPrem;
		this.oEF76AdditionalInsuredPremium = oEF76AdditionalInsuredPremium;
		this.oEF77CustomerAutoPrem = oEF77CustomerAutoPrem;
		this.oEF78AExcludedDriverEndorsementIndicator = oEF78AExcludedDriverEndorsementIndicator;
		this.oEF78ReductionCoverageNamedPersonsIndicator = oEF78ReductionCoverageNamedPersonsIndicator;
		this.oEF79FireDeductibleOwnedAutomobilesIndicator = oEF79FireDeductibleOwnedAutomobilesIndicator;
		this.oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium = oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium;
		this.oEF81GarageFamilyProtectionLimit = oEF81GarageFamilyProtectionLimit;
		this.oEF81GarageFamilyProtectionPrem = oEF81GarageFamilyProtectionPrem;
		this.oEF82LiabilityDamagedNonOwnedLimit = oEF82LiabilityDamagedNonOwnedLimit;
		this.oEF82LiabilityDamagedNonOwnedDeductible = oEF82LiabilityDamagedNonOwnedDeductible;
		this.oEF82LiabilityDamagedNonOwnedPremium = oEF82LiabilityDamagedNonOwnedPremium;
		this.oEF83AutomobileTransportationPremium = oEF83AutomobileTransportationPremium;
		this.oEF85FinalPremiumComputationIndicator = oEF85FinalPremiumComputationIndicator;
		this.oEF86CustomersAutomobilesFireDeductibleIndicator = oEF86CustomersAutomobilesFireDeductibleIndicator;
		this.oAP4TotalPrem = oAP4TotalPrem;
		this.eON101BlanketLessorsIndicator = eON101BlanketLessorsIndicator;
		this.eON102AdditionalInsuredIndividualIndicator = eON102AdditionalInsuredIndividualIndicator;
		this.eON103AdditionalInsuredOOIndicator = eON103AdditionalInsuredOOIndicator;
		this.eON109InsurableInterestDeletedIndicator = eON109InsurableInterestDeletedIndicator;
		this.eON110PerOccuranceDeductibleIndicator = eON110PerOccuranceDeductibleIndicator;
		this.eON110PerOccuranceDeductible = eON110PerOccuranceDeductible;
		this.eON111ProfitSharingIndicator = eON111ProfitSharingIndicator;
		this.eON112ProfitSharingAdjustmentPrem = eON112ProfitSharingAdjustmentPrem;
		this.eON114NamedInsuredIndicator = eON114NamedInsuredIndicator;
		this.eON115OPCF21AReceiptsBasisAdjustmentPrem = eON115OPCF21AReceiptsBasisAdjustmentPrem;
		this.eON116OPCF21B5050BasisAdjustmentPrem = eON116OPCF21B5050BasisAdjustmentPrem;
		this.eON117OPCF21BProRataBasisAdjustmentPrem = eON117OPCF21BProRataBasisAdjustmentPrem;
		this.eON119RegisteredOwnerIndicator = eON119RegisteredOwnerIndicator;
		this.eON121PolicyCancellationPrem = eON121PolicyCancellationPrem;
		this.eON122PolicyCancellationAdjustementPrem = eON122PolicyCancellationAdjustementPrem;
		this.indemnityAgreementIndicator = indemnityAgreementIndicator;
		this.garageServicePlateCoverageIndicator=garageServicePlateCoverageIndicator;
		this.garageServicePlateCount=garageServicePlateCount;
		
		this.netReinsurancePremium = netReinsurancePremium;
		this.reinsurerCode = reinsurerCode;
		this.rISKID = rISKID;
		this.programCode = programCode;
		this.product = product;
		this.blankEndorsement = blankEndorsement;
		this.totalPrem = totalPrem;
		this.brokerCommissionAmount = brokerCommissionAmount;
	}
	*/

	public Bordereaux() {
	}
	
	public Integer getTermID() {
		return termID;
	}

	public void setTermID(Integer termID) {
		this.termID = termID;
	}
	
	public Integer getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(Integer transactionID) {
		this.transactionID = transactionID;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public LocalDate getPolicyEffectiveDate() {
		return policyEffectiveDate;
	}

	public void setPolicyEffectiveDate(LocalDate policyEffectiveDate) {
		this.policyEffectiveDate = policyEffectiveDate;
	}

	public LocalDate getPolicyExpiryDate() {
		return policyExpiryDate;
	}

	public void setPolicyExpiryDate(LocalDate policyExpiryDate) {
		this.policyExpiryDate = policyExpiryDate;
	}

	public LocalDate getTransactionEffectiveDate() {
		return transactionEffectiveDate;
	}

	public void setTransactionEffectiveDate(LocalDate transactionEffectiveDate) {
		this.transactionEffectiveDate = transactionEffectiveDate;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getPolicyFleetIndicator() {
		return policyFleetIndicator;
	}

	public void setPolicyFleetIndicator(String policyFleetIndicator) {
		this.policyFleetIndicator = policyFleetIndicator;
	}
	
	public String getPolicyRiskProvince() {
		return policyRiskProvince;
	}
	public void setPolicyRiskProvince(String policyRiskProvince) {
		this.policyRiskProvince = policyRiskProvince;
	}
	
	public String getPolicyRSPIndicator() {
		return policyRSPIndicator;
	}
	public void setPolicyRSPIndicator(String policyRSPIndicator) {
		this.policyRSPIndicator = policyRSPIndicator;
	}
	
	public String getPolicyNewBusinessIndicator() {
		return policyNewBusinessIndicator;
	}
	public void setPolicyNewBusinessIndicator(String policyNewBusinessIndicator) {
		this.policyNewBusinessIndicator = policyNewBusinessIndicator;
	}
	
	public Integer getPolicySection3Limit() {
		return policySection3Limit;
	}
	public void setPolicySection3Limit(Integer policySection3Limit) {
		this.policySection3Limit = policySection3Limit;
	}
	
	public String getInsuredRIN() {
		return insuredRIN;
	}
	public void setInsuredRIN(String insuredRIN) {
		this.insuredRIN = insuredRIN;
	}
	
	public String getInsuredName() {
		return insuredName;
	}
	public void setInsuredName(String insuredName) {
		this.insuredName = insuredName;
	}
	
	public String getInsuredStreet() {
		return insuredStreet;
	}
	public void setInsuredStreet(String insuredStreet) {
		this.insuredStreet = insuredStreet;
	}
	
	public String getInsuredCity() {
		return insuredCity;
	}
	public void setInsuredCity(String insuredCity) {
		this.insuredCity = insuredCity;
	}
	
	public String getInsuredProvince() {
		return insuredProvince;
	}
	public void setInsuredProvince(String insuredProvince) {
		this.insuredProvince = insuredProvince;
	}
	
	public String getInsuredPostalCode() {
		return insuredPostalCode;
	}
	public void setInsuredPostalCode(String insuredPostalCode) {
		this.insuredPostalCode = insuredPostalCode;
	}
	
	public String getMasterBrokerName() {
		return masterBrokerName;
	}
	public void setMasterBrokerName(String masterBrokerName) {
		this.masterBrokerName = masterBrokerName;
	}
	
	public String getMasterBrokerStreet() {
		return masterBrokerStreet;
	}
	public void setMasterBrokerStreet(String masterBrokerStreet) {
		this.masterBrokerStreet = masterBrokerStreet;
	}
	
	public String getMasterBrokerCity() {
		return masterBrokerCity;
	}
	public void setMasterBrokerCity(String masterBrokerCity) {
		this.masterBrokerCity = masterBrokerCity;
	}
	
	public String getMasterBrokerPostalCode() {
		return masterBrokerPostalCode;
	}
	public void setMasterBrokerPostalCode(String masterBrokerPostalCode) {
		this.masterBrokerPostalCode = masterBrokerPostalCode;
	}
	
	public String getMasterBrokerNumber() {
		return masterBrokerNumber;
	}
	public void setMasterBrokerNumber(String masterBrokerNumber) {
		this.masterBrokerNumber = masterBrokerNumber;
	}
	
	public Integer getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(Integer unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public Integer getUnitCount() {
		return unitCount;
	}
	public void setUnitCount(Integer unitCount) {
		this.unitCount = unitCount;
	}
	
	public String getUnitStatCode() {
		return unitStatCode;
	}
	public void setUnitStatCode(String unitStatCode) {
		this.unitStatCode = unitStatCode;
	}
	
	public String getUnitRatingTerritory() {
		return unitRatingTerritory;
	}
	public void setUnitRatingTerritory(String unitRatingTerritory) {
		this.unitRatingTerritory = unitRatingTerritory;
	}
	
	public String getUnitVehicleType() {
		return unitVehicleType;
	}
	public void setUnitVehicleType(String unitVehicleType) {
		this.unitVehicleType = unitVehicleType;
	}
	
	public String getUnitTrailerType() {
		return unitTrailerType;
	}
	public void setUnitTrailerType(String unitTrailerType) {
		this.unitTrailerType = unitTrailerType;
	}
	
	public String getTrailerIndicator() {
		return trailerIndicator;
	}
	public void setTrailerIndicator(String trailerIndicator) {
		this.trailerIndicator = trailerIndicator;
	}
	
	
	public String getUnitDescription() {
		return unitDescription;
	}
	public void setUnitDescription(String unitDescription) {
		this.unitDescription = unitDescription;
	}
	
	public String getUnitVehicleClass() {
		return unitVehicleClass;
	}
	public void setUnitVehicleClass(String unitVehicleClass) {
		this.unitVehicleClass = unitVehicleClass;
	}
	
	public String getUnitDriverRecord() {
		return unitDriverRecord;
	}
	public void setUnitDriverRecord(String unitDriverRecord) {
		this.unitDriverRecord = unitDriverRecord;
	}
	
	public String getUnitMake() {
		return unitMake;
	}
	public void setUnitMake(String unitMake) {
		this.unitMake = unitMake;
	}
	
	public Integer getUnitYear() {
		return unitYear;
	}
	public void setUnitYear(Integer unitYear) {
		this.unitYear = unitYear;
	}
	
	public String getUnitVIN() {
		return unitVIN;
	}
	public void setUnitVIN(String unitVIN) {
		this.unitVIN = unitVIN;
	}
	
	public Double getUnitListPriceNew() {
		return unitListPriceNew;
	}
	public void setUnitListPriceNew(Double unitListPriceNew) {
		this.unitListPriceNew = unitListPriceNew;
	}
	
	public String getUnitLocationExposure() {
		return unitLocationExposure;
	}
	public void setUnitLocationExposure(String unitLocationExposure) {
		this.unitLocationExposure = unitLocationExposure;
	}
	
	public Double getOPCF2Prem() {
		return oPCF2Prem;
	}
	public void setOPCF2Prem(Double oPCF2Prem) {
		this.oPCF2Prem = oPCF2Prem;
	}
	
	public String getOPCF4AType() {
		return oPCF4AType;
	}
	public void setOPCF4AType(String oPCF4AType) {
		this.oPCF4AType = oPCF4AType;
	}
	
	public String getOPCF4APrem() {
		return oPCF4APrem;
	}
	public void setOPCF4APrem(String oPCF4APrem) {
		this.oPCF4APrem = oPCF4APrem;
	}
	
	public String getOPCF4BType() {
		return oPCF4BType;
	}
	public void setOPCF4BType(String oPCF4BType) {
		this.oPCF4BType = oPCF4BType;
	}
	
	public String getOPCF4BPrem() {
		return oPCF4BPrem;
	}
	public void setOPCF4BPrem(String oPCF4BPrem) {
		this.oPCF4BPrem = oPCF4BPrem;
	}
	
	public String getOPCF5Indicator() {
		return oPCF5Indicator;
	}
	public void setOPCF5Indicator(String oPCF5Indicator) {
		this.oPCF5Indicator = oPCF5Indicator;
	}
	
	public String getOPCF5CPrem() {
		return oPCF5CPrem;
	}
	public void setOPCF5CPrem(String oPCF5CPrem) {
		this.oPCF5CPrem = oPCF5CPrem;
	}
	
	public String getOPCF5DPrem() {
		return oPCF5DPrem;
	}
	public void setOPCF5DPrem(String oPCF5DPrem) {
		this.oPCF5DPrem = oPCF5DPrem;
	}
	
	public String getOPCF5GIndicator() {
		return oPCF5GIndicator;
	}
	public void setOPCF5GIndicator(String oPCF5GIndicator) {
		this.oPCF5GIndicator = oPCF5GIndicator;
	}
	
	public String getOPCF8Deductible() {
		return oPCF8Deductible;
	}
	public void setOPCF8Deductible(String oPCF8Deductible) {
		this.oPCF8Deductible = oPCF8Deductible;
	}
	
	public String getOPCF9Indicator() {
		return oPCF9Indicator;
	}
	public void setOPCF9Indicator(String oPCF9Indicator) {
		this.oPCF9Indicator = oPCF9Indicator;
	}
	public String getOPCF13CIndicator() {
		return oPCF13CIndicator;
	}
	public void setOPCF13CIndicator(String oPCF13CIndicator) {
		this.oPCF13CIndicator = oPCF13CIndicator;
	}
	
	public Integer getOPCF19ValueVehicle() {
		return oPCF19ValueVehicle;
	}
	public void setOPCF19ValueVehicle(Integer oPCF19ValueVehicle) {
		this.oPCF19ValueVehicle = oPCF19ValueVehicle;
	}
	
	public String getOPCF20Prem() {
		return oPCF20Prem;
	}
	public void setOPCF20Prem(String oPCF20Prem) {
		this.oPCF20Prem = oPCF20Prem;
	}
	
	public String getOPCF21AReceipts() {
		return oPCF21AReceipts;
	}
	public void setOPCF21AReceipts(String oPCF21AReceipts) {
		this.oPCF21AReceipts = oPCF21AReceipts;
	}
	
	public String getOPCF21APrem() {
		return oPCF21APrem;
	}
	public void setOPCF21APrem(String oPCF21APrem) {
		this.oPCF21APrem = oPCF21APrem;
	}
	
	public String getOPCF21BIndicator() {
		return oPCF21BIndicator;
	}
	public void setOPCF21BIndicator(String oPCF21BIndicator) {
		this.oPCF21BIndicator = oPCF21BIndicator;
	}
	
	public String getOPCF23AIndicator() {
		return oPCF23AIndicator;
	}
	public void setOPCF23AIndicator(String oPCF23AIndicator) {
		this.oPCF23AIndicator = oPCF23AIndicator;
	}
	
	public String getOPCF23BPrem() {
		return oPCF23BPrem;
	}
	public void setOPCF23BPrem(String oPCF23BPrem) {
		this.oPCF23BPrem = oPCF23BPrem;
	}
	
	public String getOPCF25AIndicator() {
		return oPCF25AIndicator;
	}
	public void setOPCF25AIndicator(String oPCF25AIndicator) {
		this.oPCF25AIndicator = oPCF25AIndicator;
	}
	
	public String getOPCF25APrem() {
		return oPCF25APrem;
	}
	public void setOPCF25APrem(String oPCF25APrem) {
		this.oPCF25APrem = oPCF25APrem;
	}
	public String getOPCF25A145Indicator() {
		return oPCF25A145Indicator;
	}
	public void setOPCF25A145Indicator(String oPCF25A145Indicator) {
		this.oPCF25A145Indicator = oPCF25A145Indicator;
	}
	
	public Integer getOPCF27Limit() {
		return oPCF27Limit;
	}
	public void setOPCF27Limit(Integer oPCF27Limit) {
		this.oPCF27Limit = oPCF27Limit;
	}
	
	public Integer getOPCF27Deductible() {
		return oPCF27Deductible;
	}
	public void setOPCF27Deductible(Integer oPCF27Deductible) {
		this.oPCF27Deductible = oPCF27Deductible;
	}
	
	public String getOPCF27Prem() {
		return oPCF27Prem;
	}
	public void setOPCF27Prem(String oPCF27Prem) {
		this.oPCF27Prem = oPCF27Prem;
	}
	
	public String getOPCF27BDeductible() {
		return oPCF27BDeductible;
	}
	public void setOPCF27BDeductible(String oPCF27BDeductible) {
		this.oPCF27BDeductible = oPCF27BDeductible;
	}
	
	public String getOPCF27BLimit() {
		return oPCF27BLimit;
	}
	public void setOPCF27BLimit(String oPCF27BLimit) {
		this.oPCF27BLimit = oPCF27BLimit;
	}
	
	public String getOPCF27BPrem() {
		return oPCF27BPrem;
	}
	public void setoPCF27BPrem(String oPCF27BPrem) {
		this.oPCF27BPrem = oPCF27BPrem;
	}
	
	public String getOPCF28Indicator() {
		return oPCF28Indicator;
	}
	public void setOPCF28Indicator(String oPCF28Indicator) {
		this.oPCF28Indicator = oPCF28Indicator;
	}
	
	public String getOPCF28AIndicator() {
		return oPCF28AIndicator;
	}
	public void setOPCF28AIndicator(String oPCF28AIndicator) {
		this.oPCF28AIndicator = oPCF28AIndicator;
	}
	
	public String getOPCF30Indicator() {
		return oPCF30Indicator;
	}
	public void setOPCF30Indicator(String oPCF30Indicator) {
		this.oPCF30Indicator = oPCF30Indicator;
	}
	
	public String getOPCF31Indicator() {
		return oPCF31Indicator;
	}
	public void setOPCF31Indicator(String oPCF31Indicator) {
		this.oPCF31Indicator = oPCF31Indicator;
	}
	
	public String getOPCF38Limit() {
		return oPCF38Limit;
	}
	public void setOPCF38Limit(String oPCF38Limit) {
		this.oPCF38Limit = oPCF38Limit;
	}
	public String getOPCF38Prem() {
		return oPCF38Prem;
	}
	public void setOPCF38Prem(String oPCF38Prem) {
		this.oPCF38Prem = oPCF38Prem;
	}
	
	public String getOPCF40Deductible() {
		return oPCF40Deductible;
	}
	public void setOPCF40Deductible(String oPCF40Deductible) {
		this.oPCF40Deductible = oPCF40Deductible;
	}
	
	public String getOPCF43Prem() {
		return oPCF43Prem;
	}
	public void setOPCF43Prem(String oPCF43Prem) {
		this.oPCF43Prem = oPCF43Prem;
	}
	
	public String getOPCF43APrem() {
		return oPCF43APrem;
	}
	public void setOPCF43APrem(String oPCF43APrem) {
		this.oPCF43APrem = oPCF43APrem;
	}
	
	public String getOPCF44Prem() {
		return oPCF44Prem;
	}
	public void setOPCF44Prem(String oPCF44Prem) {
		this.oPCF44Prem = oPCF44Prem;
	}
	
	public String getOPCF47Indicator() {
		return oPCF47Indicator;
	}
	public void setOPCF47Indicator(String oPCF47Indicator) {
		this.oPCF47Indicator = oPCF47Indicator;
	}
	
	public String getOPCF48Prem() {
		return oPCF48Prem;
	}
	public void setOPCF48Prem(String oPCF48Prem) {
		this.oPCF48Prem = oPCF48Prem;
	}
	
	public String getDealerPlateCoverageIndicator() {
		return dealerPlateCoverageIndicator;
	}
	public void setDealerPlateCoverageIndicator(String dealerPlateCoverageIndicator) {
		this.dealerPlateCoverageIndicator = dealerPlateCoverageIndicator;
	}
	
	public String getEON20ATravelExpensesDeductible() {
		return eON20ATravelExpensesDeductible;
	}
	public void setEON20ATravelExpensesDeductible(String eON20ATravelExpensesDeductible) {
		this.eON20ATravelExpensesDeductible = eON20ATravelExpensesDeductible;
	}
	
	public String getEON20ATravelExpensesPrem() {
		return eON20ATravelExpensesPrem;
	}
	public void seteON20ATravelExpensesPrem(String eON20ATravelExpensesPrem) {
		this.eON20ATravelExpensesPrem = eON20ATravelExpensesPrem;
	}
	
	public String getEON20GLossIncomeDeductible() {
		return eON20GLossIncomeDeductible;
	}
	public void setEON20GLossIncomeDeductible(String eON20GLossIncomeDeductible) {
		this.eON20GLossIncomeDeductible = eON20GLossIncomeDeductible;
	}
	
	public String getEON20GLossIncomePrem() {
		return eON20GLossIncomePrem;
	}
	public void setEON20GLossIncomePrem(String eON20GLossIncomePrem) {
		this.eON20GLossIncomePrem = eON20GLossIncomePrem;
	}
	public String getEON104BuydownDeductible() {
		return eON104BuydownDeductible;
	}
	public void setEON104BuydownDeductible(String eON104BuydownDeductible) {
		this.eON104BuydownDeductible = eON104BuydownDeductible;
	}
	
	public String getEON104BuydownDeductiblePrem() {
		return eON104BuydownDeductiblePrem;
	}
	public void setEON104BuydownDeductiblePrem(String eON104BuydownDeductiblePrem) {
		this.eON104BuydownDeductiblePrem = eON104BuydownDeductiblePrem;
	}
	
	public String getConditionalReleaseBondPrem() {
		return conditionalReleaseBondPrem;
	}
	public void setConditionalReleaseBondPrem(String conditionalReleaseBondPrem) {
		this.conditionalReleaseBondPrem = conditionalReleaseBondPrem;
	}
	
	public String getAutoEndtotalPrem() {
		return autoEndtotalPrem;
	}
	public void setAutoEndtotalPrem(String autoEndtotalPrem) {
		this.autoEndtotalPrem = autoEndtotalPrem;
	}
	
	public String getAutoSection3BITotalPrem() {
		return autoSection3BITotalPrem;
	}
	public void setAutoSection3BITotalPrem(String autoSection3BITotalPrem) {
		this.autoSection3BITotalPrem = autoSection3BITotalPrem;
	}
	
	public String getAutoSection3PDTotalPrem() {
		return autoSection3PDTotalPrem;
	}
	public void setAutoSection3PDTotalPrem(String autoSection3PDTotalPrem) {
		this.autoSection3PDTotalPrem = autoSection3PDTotalPrem;
	}
	
	public String getAutoSection4ABTotalPrem() {
		return autoSection4ABTotalPrem;
	}
	public void setAutoSection4ABTotalPrem(String autoSection4ABTotalPrem) {
		this.autoSection4ABTotalPrem = autoSection4ABTotalPrem;
	}
	
	public String getAutoSection5UATotalPrem() {
		return autoSection5UATotalPrem;
	}
	public void setAutoSection5UATotalPrem(String autoSection5UATotalPrem) {
		this.autoSection5UATotalPrem = autoSection5UATotalPrem;
	}
	
	public String getAutoSection6DCPDDeductible() {
		return autoSection6DCPDDeductible;
	}
	public void setAutoSection6DCPDDeductible(String autoSection6DCPDDeductible) {
		this.autoSection6DCPDDeductible = autoSection6DCPDDeductible;
	}
	
	public String getAutoSection6DCPDTotalPrem() {
		return autoSection6DCPDTotalPrem;
	}
	public void setAutoSection6DCPDTotalPrem(String autoSection6DCPDTotalPrem) {
		this.autoSection6DCPDTotalPrem = autoSection6DCPDTotalPrem;
	}
	public String getAutoSection7CCLDeductible() {
		return autoSection7CCLDeductible;
	}
	public void setAutoSection7CCLDeductible(String autoSection7CCLDeductible) {
		this.autoSection7CCLDeductible = autoSection7CCLDeductible;
	}
	
	public String getAutoSection7CCLTotalPrem() {
		return autoSection7CCLTotalPrem;
	}
	public void setAutoSection7CCLTotalPrem(String autoSection7CCLTotalPrem) {
		this.autoSection7CCLTotalPrem = autoSection7CCLTotalPrem;
	}
	
	public String getAutoSection7BCMDeductible() {
		return autoSection7BCMDeductible;
	}
	public void setAutoSection7BCMDeductible(String autoSection7BCMDeductible) {
		this.autoSection7BCMDeductible = autoSection7BCMDeductible;
	}
	
	public String getAutoSection7BCMTotalPrem() {
		return autoSection7BCMTotalPrem;
	}
	public void setAutoSection7BCMTotalPrem(String autoSection7BCMTotalPrem) {
		this.autoSection7BCMTotalPrem = autoSection7BCMTotalPrem;
	}
	
	public String getAutoSection7DAPDeductible() {
		return autoSection7DAPDeductible;
	}
	public void setAutoSection7DAPDeductible(String autoSection7DAPDeductible) {
		this.autoSection7DAPDeductible = autoSection7DAPDeductible;
	}
	
	public String getAutoSection7DAPTotalPrem() {
		return autoSection7DAPTotalPrem;
	}
	public void setAutoSection7DAPTotalPrem(String autoSection7DAPTotalPrem) {
		this.autoSection7DAPTotalPrem = autoSection7DAPTotalPrem;
	}
	
	public String getAutoSection7ASPDeductible() {
		return autoSection7ASPDeductible;
	}
	public void setAutoSection7ASPDeductible(String autoSection7ASPDeductible) {
		this.autoSection7ASPDeductible = autoSection7ASPDeductible;
	}
	
	public String getAutoSection7ASPTotalPrem() {
		return autoSection7ASPTotalPrem;
	}
	public void setAutoSection7ASPTotalPrem(String autoSection7ASPTotalPrem) {
		this.autoSection7ASPTotalPrem = autoSection7ASPTotalPrem;
	}
	
	public String getAutoABIncomeReplacementTotalPrem() {
		return autoABIncomeReplacementTotalPrem;
	}
	public void setAutoABIncomeReplacementTotalPrem(String autoABIncomeReplacementTotalPrem) {
		this.autoABIncomeReplacementTotalPrem = autoABIncomeReplacementTotalPrem;
	}
	
	public String getAutoABIncomeReplacementIndicator() {
		return autoABIncomeReplacementIndicator;
	}
	public void setAutoABIncomeReplacementIndicator(String autoABIncomeReplacementIndicator) {
		this.autoABIncomeReplacementIndicator = autoABIncomeReplacementIndicator;
	}
	
	public String getAutoABIncomeReplacementLimit() {
		return autoABIncomeReplacementLimit;
	}
	public void setAutoABIncomeReplacementLimit(String autoABIncomeReplacementLimit) {
		this.autoABIncomeReplacementLimit = autoABIncomeReplacementLimit;
	}
	
	public String getAutoABDeathAndFuneralTotalPrem() {
		return autoABDeathAndFuneralTotalPrem;
	}
	public void setAutoABDeathAndFuneralTotalPrem(String autoABDeathAndFuneralTotalPrem) {
		this.autoABDeathAndFuneralTotalPrem = autoABDeathAndFuneralTotalPrem;
	}
	
	public String getAutoABDeathAndFuneralIndicator() {
		return autoABDeathAndFuneralIndicator;
	}
	public void setAutoABDeathAndFuneralIndicator(String autoABDeathAndFuneralIndicator) {
		this.autoABDeathAndFuneralIndicator = autoABDeathAndFuneralIndicator;
	}
	
	public String getAutoABMedicalRehabAndATCTotalPrem() {
		return autoABMedicalRehabAndATCTotalPrem;
	}
	public void setAutoABMedicalRehabAndATCTotalPrem(String autoABMedicalRehabAndATCTotalPrem) {
		this.autoABMedicalRehabAndATCTotalPrem = autoABMedicalRehabAndATCTotalPrem;
	}
	
	public String getAutoABMedicalRehabAndATCIndicator() {
		return autoABMedicalRehabAndATCIndicator;
	}
	public void setAutoABMedicalRehabAndATCIndicator(String autoABMedicalRehabAndATCIndicator) {
		this.autoABMedicalRehabAndATCIndicator = autoABMedicalRehabAndATCIndicator;
	}
	
	public String getAutoABMedicalRehabAndATCLimit() {
		return autoABMedicalRehabAndATCLimit;
	}
	public void setAutoABMedicalRehabAndATCLimit(String autoABMedicalRehabAndATCLimit) {
		this.autoABMedicalRehabAndATCLimit = autoABMedicalRehabAndATCLimit;
	}
	
	public String getAutoABCATImpairementTotalPrem() {
		return autoABCATImpairementTotalPrem;
	}
	public void setAutoABCATImpairementTotalPrem(String autoABCATImpairementTotalPrem) {
		this.autoABCATImpairementTotalPrem = autoABCATImpairementTotalPrem;
	}
	
	public String getAutoABCATImpairementIndicator() {
		return autoABCATImpairementIndicator;
	}
	public void setAutoABCATImpairementIndicator(String autoABCATImpairementIndicator) {
		this.autoABCATImpairementIndicator = autoABCATImpairementIndicator;
	}
	
	public String getAutoABCATImpairementLimit() {
		return autoABCATImpairementLimit;
	}
	public void setAutoABCATImpairementLimit(String autoABCATImpairementLimit) {
		this.autoABCATImpairementLimit = autoABCATImpairementLimit;
	}
	
	public String getAutoABCaregiverHousekeeperTotalPrem() {
		return autoABCaregiverHousekeeperTotalPrem;
	}
	public void setAutoABCaregiverHousekeeperTotalPrem(String autoABCaregiverHousekeeperTotalPrem) {
		this.autoABCaregiverHousekeeperTotalPrem = autoABCaregiverHousekeeperTotalPrem;
	}
	
	public String getAutoABCaregiverHousekeeperIndicator() {
		return autoABCaregiverHousekeeperIndicator;
	}
	public void setAutoABCaregiverHousekeeperIndicator(String autoABCaregiverHousekeeperIndicator) {
		this.autoABCaregiverHousekeeperIndicator = autoABCaregiverHousekeeperIndicator;
	}
	
	public String getAutoABDependentCareTotalPrem() {
		return autoABDependentCareTotalPrem;
	}
	public void setAutoABDependentCareTotalPrem(String autoABDependentCareTotalPrem) {
		this.autoABDependentCareTotalPrem = autoABDependentCareTotalPrem;
	}
	
	public String getAutoABDependentCareIndicator() {
		return autoABDependentCareIndicator;
	}
	public void setAutoABDependentCareIndicator(String autoABDependentCareIndicator) {
		this.autoABDependentCareIndicator = autoABDependentCareIndicator;
	}
	
	public String getAutoABIndexationTotalPrem() {
		return autoABIndexationTotalPrem;
	}
	public void setAutoABIndexationTotalPrem(String autoABIndexationTotalPrem) {
		this.autoABIndexationTotalPrem = autoABIndexationTotalPrem;
	}
	
	public String getAutoABIndexationIndicator() {
		return autoABIndexationIndicator;
	}
	public void setAutoABIndexationIndicator(String autoABIndexationIndicator) {
		this.autoABIndexationIndicator = autoABIndexationIndicator;
	}
	
	public String getAutoWOEndtTotalPrem() {
		return autoWOEndtTotalPrem;
	}
	public void setAutoWOEndtTotalPrem(String autoWOEndtTotalPrem) {
		this.autoWOEndtTotalPrem = autoWOEndtTotalPrem;
	}
	
	public String getContentsPersonalEffectsLimit() {
		return contentsPersonalEffectsLimit;
	}
	public void setContentsPersonalEffectsLimit(String contentsPersonalEffectsLimit) {
		this.contentsPersonalEffectsLimit = contentsPersonalEffectsLimit;
	}
	
	public String getContentsPersonalEffectsDeductible() {
		return contentsPersonalEffectsDeductible;
	}
	public void setContentsPersonalEffectsDeductible(String contentsPersonalEffectsDeductible) {
		this.contentsPersonalEffectsDeductible = contentsPersonalEffectsDeductible;
	}
	
	public String getContentsPersonalEffectsPremium() {
		return contentsPersonalEffectsPremium;
	}
	public void setContentsPersonalEffectsPremium(String contentsPersonalEffectsPremium) {
		this.contentsPersonalEffectsPremium = contentsPersonalEffectsPremium;
	}
	
	public String getCargoLimit() {
		return cargoLimit;
	}
	public void setCargoLimit(String cargoLimit) {
		this.cargoLimit = cargoLimit;
	}
	public String getCargoDeductible() {
		return cargoDeductible;
	}
	public void setCargoDeductible(String cargoDeductible) {
		this.cargoDeductible = cargoDeductible;
	}
	
	public String getCargoAdditionalLimitPrem() {
		return cargoAdditionalLimitPrem;
	}
	public void setCargoAdditionalLimitPrem(String cargoAdditionalLimitPrem) {
		this.cargoAdditionalLimitPrem = cargoAdditionalLimitPrem;
	}
	
	public String getAccruedFreightChargesLimit() {
		return accruedFreightChargesLimit;
	}
	public void setAccruedFreightChargesLimit(String accruedFreightChargesLimit) {
		this.accruedFreightChargesLimit = accruedFreightChargesLimit;
	}
	
	public String getAccruedFreightChargesPremium() {
		return accruedFreightChargesPremium;
	}
	public void setAccruedFreightChargesPremium(String accruedFreightChargesPremium) {
		this.accruedFreightChargesPremium = accruedFreightChargesPremium;
	}
	
	public String getContingentCargoLimit() {
		return contingentCargoLimit;
	}
	public void setContingentCargoLimit(String contingentCargoLimit) {
		this.contingentCargoLimit = contingentCargoLimit;
	}
	
	public String getContingentCargoDeductible() {
		return contingentCargoDeductible;
	}
	public void setContingentCargoDeductible(String contingentCargoDeductible) {
		this.contingentCargoDeductible = contingentCargoDeductible;
	}
	
	public String getContingentCargoPrem() {
		return contingentCargoPrem;
	}
	public void setContingentCargoPrem(String contingentCargoPrem) {
		this.contingentCargoPrem = contingentCargoPrem;
	}
	
	public String getDebrisRemovalLimit() {
		return debrisRemovalLimit;
	}
	public void setDebrisRemovalLimit(String debrisRemovalLimit) {
		this.debrisRemovalLimit = debrisRemovalLimit;
	}
	
	public String getDebrisRemovalPremium() {
		return debrisRemovalPremium;
	}
	public void setDebrisRemovalPremium(String debrisRemovalPremium) {
		this.debrisRemovalPremium = debrisRemovalPremium;
	}
	
	public String getEquipmentLimit() {
		return equipmentLimit;
	}
	public void setEquipmentLimit(String equipmentLimit) {
		this.equipmentLimit = equipmentLimit;
	}
	
	public String getEquipmentPremium() {
		return equipmentPremium;
	}
	public void setEquipmentPremium(String equipmentPremium) {
		this.equipmentPremium = equipmentPremium;
	}
	
	public String getIdentityMisrepresentationLimit() {
		return identityMisrepresentationLimit;
	}
	public void setIdentityMisrepresentationLimit(String identityMisrepresentationLimit) {
		this.identityMisrepresentationLimit = identityMisrepresentationLimit;
	}
	
	public String getIdentityMisrepresentationPremium() {
		return identityMisrepresentationPremium;
	}
	public void setIdentityMisrepresentationPremium(String identityMisrepresentationPremium) {
		this.identityMisrepresentationPremium = identityMisrepresentationPremium;
	}
	
	public String getTemporaryStorageLimit() {
		return temporaryStorageLimit;
	}
	public void setTemporaryStorageLimit(String temporaryStorageLimit) {
		this.temporaryStorageLimit = temporaryStorageLimit;
	}
	
	public String getTemporaryStoragePremium() {
		return temporaryStoragePremium;
	}
	public void setTemporaryStoragePremium(String temporaryStoragePremium) {
		this.temporaryStoragePremium = temporaryStoragePremium;
	}
	
	public String getTripTransitCargoAdditionalLimit() {
		return tripTransitCargoAdditionalLimit;
	}
	public void setTripTransitCargoAdditionalLimit(String tripTransitCargoAdditionalLimit) {
		this.tripTransitCargoAdditionalLimit = tripTransitCargoAdditionalLimit;
	}
	
	public String getTripTransitCargoPrem() {
		return tripTransitCargoPrem;
	}
	public void setTripTransitCargoPrem(String tripTransitCargoPrem) {
		this.tripTransitCargoPrem = tripTransitCargoPrem;
	}
	
	public String getQE4201ROwnersFormLimit() {
		return qE4201ROwnersFormLimit;
	}
	public void setQE4201ROwnersFormLimit(String qE4201ROwnersFormLimit) {
		this.qE4201ROwnersFormLimit = qE4201ROwnersFormLimit;
	}
	
	public String getQE4201ROwnersFormDeductible() {
		return qE4201ROwnersFormDeductible;
	}
	public void setQE4201ROwnersFormDeductible(String qE4201ROwnersFormDeductible) {
		this.qE4201ROwnersFormDeductible = qE4201ROwnersFormDeductible;
	}
	
	public String getQE4201ROwnersFormPrem() {
		return qE4201ROwnersFormPrem;
	}
	public void setQE4201ROwnersFormPrem(String qE4201ROwnersFormPrem) {
		this.qE4201ROwnersFormPrem = qE4201ROwnersFormPrem;
	}
	
	public String getEON141ChangetoDeductibleIndicator() {
		return eON141ChangetoDeductibleIndicator;
	}
	public void setEON141ChangetoDeductibleIndicator(String eON141ChangetoDeductibleIndicator) {
		this.eON141ChangetoDeductibleIndicator = eON141ChangetoDeductibleIndicator;
	}
	
	public String getDangerousGoodsExclusionIndicator() {
		return dangerousGoodsExclusionIndicator;
	}
	public void setDangerousGoodsExclusionIndicator(String dangerousGoodsExclusionIndicator) {
		this.dangerousGoodsExclusionIndicator = dangerousGoodsExclusionIndicator;
	}
	public String getDangerousGoodsPartialLoadIndicator() {
		return dangerousGoodsPartialLoadIndicator;
	}
	public void setDangerousGoodsPartialLoadIndicator(String dangerousGoodsPartialLoadIndicator) {
		this.dangerousGoodsPartialLoadIndicator = dangerousGoodsPartialLoadIndicator;
	}
	
	public String getDangerousGoodsProductClassesIndicator() {
		return dangerousGoodsProductClassesIndicator;
	}
	public void setDangerousGoodsProductClassesIndicator(String dangerousGoodsProductClassesIndicator) {
		this.dangerousGoodsProductClassesIndicator = dangerousGoodsProductClassesIndicator;
	}
	
	public String getForestryExclusionIndicator() {
		return forestryExclusionIndicator;
	}
	public void setForestryExclusionIndicator(String forestryExclusionIndicator) {
		this.forestryExclusionIndicator = forestryExclusionIndicator;
	}
	
	public String getCargoExclusionIndicator() {
		return cargoExclusionIndicator;
	}
	public void setCargoExclusionIndicator(String cargoExclusionIndicator) {
		this.cargoExclusionIndicator = cargoExclusionIndicator;
	}
	
	public String getCargoGeneralEndorsementPrem() {
		return cargoGeneralEndorsementPrem;
	}
	public void setCargoGeneralEndorsementPrem(String cargoGeneralEndorsementPrem) {
		this.cargoGeneralEndorsementPrem = cargoGeneralEndorsementPrem;
	}
	
	public String getCargoTotalPrem() {
		return cargoTotalPrem;
	}
	public void setCargoTotalPrem(String cargoTotalPrem) {
		this.cargoTotalPrem = cargoTotalPrem;
	}
	
	public String getCGLDeductible() {
		return cGLDeductible;
	}
	public void setCGLDeductible(String cGLDeductible) {
		this.cGLDeductible = cGLDeductible;
	}
	
	public String getCGLLimit() {
		return cGLLimit;
	}
	public void setCGLLimit(String cGLLimit) {
		this.cGLLimit = cGLLimit;
	}
	
	public String getTenantsLegalLiabilityStreet() {
		return tenantsLegalLiabilityStreet;
	}
	public void setTenantsLegalLiabilityStreet(String tenantsLegalLiabilityStreet) {
		this.tenantsLegalLiabilityStreet = tenantsLegalLiabilityStreet;
	}
	
	public String getTenantsLegalLiabilityCity() {
		return tenantsLegalLiabilityCity;
	}
	public void setTenantsLegalLiabilityCity(String tenantsLegalLiabilityCity) {
		this.tenantsLegalLiabilityCity = tenantsLegalLiabilityCity;
	}
	
	public String getTenantsLegalLiabilityPostalCode() {
		return tenantsLegalLiabilityPostalCode;
	}
	public void setTenantsLegalLiabilityPostalCode(String tenantsLegalLiabilityPostalCode) {
		this.tenantsLegalLiabilityPostalCode = tenantsLegalLiabilityPostalCode;
	}
	public String getTenantsLegalLiabilityLimit() {
		return tenantsLegalLiabilityLimit;
	}
	public void setTenantsLegalLiabilityLimit(String tenantsLegalLiabilityLimit) {
		this.tenantsLegalLiabilityLimit = tenantsLegalLiabilityLimit;
	}
	
	public String getTenantsLegalLiabilityDeductible() {
		return tenantsLegalLiabilityDeductible;
	}
	public void setTenantsLegalLiabilityDeductible(String tenantsLegalLiabilityDeductible) {
		this.tenantsLegalLiabilityDeductible = tenantsLegalLiabilityDeductible;
	}
	
	public String getTenantsLegalLiabilityPrem() {
		return tenantsLegalLiabilityPrem;
	}
	public void setTenantsLegalLiabilityPrem(String tenantsLegalLiabilityPrem) {
		this.tenantsLegalLiabilityPrem = tenantsLegalLiabilityPrem;
	}
	
	public String getQE4023NWarehousemanLegalLiabilityLimit() {
		return qE4023NWarehousemanLegalLiabilityLimit;
	}
	public void setQE4023NWarehousemanLegalLiabilityLimit(String qE4023NWarehousemanLegalLiabilityLimit) {
		this.qE4023NWarehousemanLegalLiabilityLimit = qE4023NWarehousemanLegalLiabilityLimit;
	}
	
	public String getQE4023NWarehousemanLegalLiabilityDeductible() {
		return qE4023NWarehousemanLegalLiabilityDeductible;
	}
	public void setQE4023NWarehousemanLegalLiabilityDeductible(String qE4023NWarehousemanLegalLiabilityDeductible) {
		this.qE4023NWarehousemanLegalLiabilityDeductible = qE4023NWarehousemanLegalLiabilityDeductible;
	}
	
	public String getQE4023NWarehousemanLegalLiabilityPrem() {
		return qE4023NWarehousemanLegalLiabilityPrem;
	}
	public void setQE4023NWarehousemanLegalLiabilityPrem(String qE4023NWarehousemanLegalLiabilityPrem) {
		this.qE4023NWarehousemanLegalLiabilityPrem = qE4023NWarehousemanLegalLiabilityPrem;
	}
	
	public String getQE2033NEmployeeBenefitsExtensionLimit() {
		return qE2033NEmployeeBenefitsExtensionLimit;
	}
	public void setQE2033NEmployeeBenefitsExtensionLimit(String qE2033NEmployeeBenefitsExtensionLimit) {
		this.qE2033NEmployeeBenefitsExtensionLimit = qE2033NEmployeeBenefitsExtensionLimit;
	}
	
	public String getQE2033NEmployeeBenefitsExtensionDeductible() {
		return qE2033NEmployeeBenefitsExtensionDeductible;
	}
	public void setQE2033NEmployeeBenefitsExtensionDeductible(String qE2033NEmployeeBenefitsExtensionDeductible) {
		this.qE2033NEmployeeBenefitsExtensionDeductible = qE2033NEmployeeBenefitsExtensionDeductible;
	}
	
	public String getQE2033NEmployeeBenefitsExtensionPrem() {
		return qE2033NEmployeeBenefitsExtensionPrem;
	}
	public void setQE2033NEmployeeBenefitsExtensionPrem(String qE2033NEmployeeBenefitsExtensionPrem) {
		this.qE2033NEmployeeBenefitsExtensionPrem = qE2033NEmployeeBenefitsExtensionPrem;
	}
	
	public String getQE2313NLimitedPollutionLiabilityLimit() {
		return qE2313NLimitedPollutionLiabilityLimit;
	}
	public void setQE2313NLimitedPollutionLiabilityLimit(String qE2313NLimitedPollutionLiabilityLimit) {
		this.qE2313NLimitedPollutionLiabilityLimit = qE2313NLimitedPollutionLiabilityLimit;
	}
	public String getQE2313NLimitedPollutionLiabilityDeductible() {
		return qE2313NLimitedPollutionLiabilityDeductible;
	}
	public void setQE2313NLimitedPollutionLiabilityDeductible(String qE2313NLimitedPollutionLiabilityDeductible) {
		this.qE2313NLimitedPollutionLiabilityDeductible = qE2313NLimitedPollutionLiabilityDeductible;
	}
	
	public String getQE2313NLimitedPollutionLiabilityPrem() {
		return qE2313NLimitedPollutionLiabilityPrem;
	}
	public void setQE2313NLimitedPollutionLiabilityPrem(String qE2313NLimitedPollutionLiabilityPrem) {
		this.qE2313NLimitedPollutionLiabilityPrem = qE2313NLimitedPollutionLiabilityPrem;
	}
	
	public String getSPF6NonOwnedAutomobileIndicator() {
		return sPF6NonOwnedAutomobileIndicator;
	}
	public void setSPF6NonOwnedAutomobileIndicator(String sPF6NonOwnedAutomobileIndicator) {
		this.sPF6NonOwnedAutomobileIndicator = sPF6NonOwnedAutomobileIndicator;
	}
	
	public String getSEF94LegalLiabilityIndicator() {
		return sEF94LegalLiabilityIndicator;
	}
	public void setSEF94LegalLiabilityIndicator(String sEF94LegalLiabilityIndicator) {
		this.sEF94LegalLiabilityIndicator = sEF94LegalLiabilityIndicator;
	}
	
	public String getSEF96ContractualLiabilityIndicator() {
		return sEF96ContractualLiabilityIndicator;
	}
	public void setSEF96ContractualLiabilityIndicator(String sEF96ContractualLiabilityIndicator) {
		this.sEF96ContractualLiabilityIndicator = sEF96ContractualLiabilityIndicator;
	}
	
	public String getSEF99LeasedVehicleIndicator() {
		return sEF99LeasedVehicleIndicator;
	}
	public void setSEF99LeasedVehicleIndicator(String sEF99LeasedVehicleIndicator) {
		this.sEF99LeasedVehicleIndicator = sEF99LeasedVehicleIndicator;
	}
	
	public String getGarageLiabilityExtensionIndicator() {
		return garageLiabilityExtensionIndicator;
	}
	public void setGarageLiabilityExtensionIndicator(String garageLiabilityExtensionIndicator) {
		this.garageLiabilityExtensionIndicator = garageLiabilityExtensionIndicator;
	}
	
	public String getQE2345NTotalPollutionExclusionIndicator() {
		return qE2345NTotalPollutionExclusionIndicator;
	}
	public void setQE2345NTotalPollutionExclusionIndicator(String qE2345NTotalPollutionExclusionIndicator) {
		this.qE2345NTotalPollutionExclusionIndicator = qE2345NTotalPollutionExclusionIndicator;
	}
	
	public String getQE2345NTotalPollutionExclusionPremium() {
		return qE2345NTotalPollutionExclusionPremium;
	}
	public void setQE2345NTotalPollutionExclusionPremium(String qE2345NTotalPollutionExclusionPremium) {
		this.qE2345NTotalPollutionExclusionPremium = qE2345NTotalPollutionExclusionPremium;
	}
	
	public String getMobileEquipmentLimit() {
		return mobileEquipmentLimit;
	}
	public void setMobileEquipmentLimit(String mobileEquipmentLimit) {
		this.mobileEquipmentLimit = mobileEquipmentLimit;
	}
	
	public String getMobileEquipmentDeductible() {
		return mobileEquipmentDeductible;
	}
	public void setMobileEquipmentDeductible(String mobileEquipmentDeductible) {
		this.mobileEquipmentDeductible = mobileEquipmentDeductible;
	}
	
	public String getMobileEquipmentPrem() {
		return mobileEquipmentPrem;
	}
	public void setMobileEquipmentPrem(String mobileEquipmentPrem) {
		this.mobileEquipmentPrem = mobileEquipmentPrem;
	}
	public String getCGLGeneralEndorsementPrem() {
		return cGLGeneralEndorsementPrem;
	}
	public void setcGLGeneralEndorsementPrem(String cGLGeneralEndorsementPrem) {
		this.cGLGeneralEndorsementPrem = cGLGeneralEndorsementPrem;
	}
	
	public String getCGLTotalPrem() {
		return cGLTotalPrem;
	}
	public void setCGLTotalPrem(String cGLTotalPrem) {
		this.cGLTotalPrem = cGLTotalPrem;
	}
	
	public String getBusinessOperations() {
		return businessOperations;
	}
	public void setBusinessOperations(String businessOperations) {
		this.businessOperations = businessOperations;
	}
	
	public String getLocationAddress() {
		return locationAddress;
	}
	public void setLocationAddress(String locationAddress) {
		this.locationAddress = locationAddress;
	}
	
	public String getLocationCity() {
		return locationCity;
	}
	public void setLocationCity(String locationCity) {
		this.locationCity = locationCity;
	}
	
	public String getLocationProvince() {
		return locationProvince;
	}
	public void setLocationProvince(String locationProvince) {
		this.locationProvince = locationProvince;
	}
	
	public String getLocationPostalCode() {
		return locationPostalCode;
	}
	public void setLocationPostalCode(String locationPostalCode) {
		this.locationPostalCode = locationPostalCode;
	}
	
	public String getLocationIncludeBuilding() {
		return locationIncludeBuilding;
	}
	public void setLocationIncludeBuilding(String locationIncludeBuilding) {
		this.locationIncludeBuilding = locationIncludeBuilding;
	}
	
	public String getLocationIncludeLot() {
		return locationIncludeLot;
	}
	public void setLocationIncludeLot(String locationIncludeLot) {
		this.locationIncludeLot = locationIncludeLot;
	}
	
	public Integer getLocationTotalFullTimeEmployees() {
		return locationTotalFullTimeEmployees;
	}
	public void setLocationTotalFullTimeEmployees(Integer locationTotalFullTimeEmployees) {
		this.locationTotalFullTimeEmployees = locationTotalFullTimeEmployees;
	}
	
	public Integer getLocationTotalPartTimeEmployees() {
		return locationTotalPartTimeEmployees;
	}
	public void setLocationTotalPartTimeEmployees(Integer locationTotalPartTimeEmployees) {
		this.locationTotalPartTimeEmployees = locationTotalPartTimeEmployees;
	}
	
	public Integer getQE4037RContentsLimit() {
		return qE4037RContentsLimit;
	}
	public void setQE4037RContentsLimit(Integer qE4037RContentsLimit) {
		this.qE4037RContentsLimit = qE4037RContentsLimit;
	}
	
	public Integer getQE4037RContentsDeductible() {
		return qE4037RContentsDeductible;
	}
	public void setQE4037RContentsDeductible(Integer qE4037RContentsDeductible) {
		this.qE4037RContentsDeductible = qE4037RContentsDeductible;
	}
	
	public Double getQE4037RContentsPrem() {
		return qE4037RContentsPrem;
	}
	public void setQE4037RContentsPrem(Double qE4037RContentsPrem) {
		this.qE4037RContentsPrem = qE4037RContentsPrem;
	}
	
	public String getQE4037RPropertyExtensionIndicator() {
		return qE4037RPropertyExtensionIndicator;
	}
	public void setQE4037RPropertyExtensionIndicator(String qE4037RPropertyExtensionIndicator) {
		this.qE4037RPropertyExtensionIndicator = qE4037RPropertyExtensionIndicator;
	}
	
	public String getOAP4Section1Limit() {
		return oAP4Section1Limit;
	}
	public void setOAP4Section1Limit(String oAP4Section1Limit) {
		this.oAP4Section1Limit = oAP4Section1Limit;
	}
	
	public String getOAP4Section1BIPrem() {
		return oAP4Section1BIPrem;
	}
	public void setOAP4Section1BIPrem(String oAP4Section1BIPrem) {
		this.oAP4Section1BIPrem = oAP4Section1BIPrem;
	}
	
	public String getOAP4Section1PDPrem() {
		return oAP4Section1PDPrem;
	}
	public void setOAP4Section1PDPrem(String oAP4Section1PDPrem) {
		this.oAP4Section1PDPrem = oAP4Section1PDPrem;
	}
	
	public String getOAP4Section2ABPrem() {
		return oAP4Section2ABPrem;
	}
	public void setOAP4Section2ABPrem(String oAP4Section2ABPrem) {
		this.oAP4Section2ABPrem = oAP4Section2ABPrem;
	}
	public String getOAP4Section3UAPrem() {
		return oAP4Section3UAPrem;
	}
	public void setOAP4Section3UAPrem(String oAP4Section3UAPrem) {
		this.oAP4Section3UAPrem = oAP4Section3UAPrem;
	}
	
	public String getOAP4Section4DCPDDeductible() {
		return oAP4Section4DCPDDeductible;
	}
	public void setOAP4Section4DCPDDeductible(String oAP4Section4DCPDDeductible) {
		this.oAP4Section4DCPDDeductible = oAP4Section4DCPDDeductible;
	}
	
	public String getOAP4Section4DCPDPrem() {
		return oAP4Section4DCPDPrem;
	}
	public void setOAP4Section4DCPDPrem(String oAP4Section4DCPDPrem) {
		this.oAP4Section4DCPDPrem = oAP4Section4DCPDPrem;
	}
	
	public String getOAP4Section61CLLimit() {
		return oAP4Section61CLLimit;
	}
	public void setOAP4Section61CLLimit(String oAP4Section61CLLimit) {
		this.oAP4Section61CLLimit = oAP4Section61CLLimit;
	}
	
	public Integer getOAP4Section511CLDeductible() {
		return oAP4Section511CLDeductible;
	}
	public void setOAP4Section511CLDeductible(Integer oAP4Section511CLDeductible) {
		this.oAP4Section511CLDeductible = oAP4Section511CLDeductible;
	}
	
	public Double getOAP4Section511CLPremium() {
		return oAP4Section511CLPremium;
	}
	public void setOAP4Section511CLPremium(Double oAP4Section511CLPremium) {
		this.oAP4Section511CLPremium = oAP4Section511CLPremium;
	}

	public Integer getOAP4Section512CMLimit() {
		return oAP4Section512CMLimit;
	}
	public void setOAP4Section512CMLimit(Integer oAP4Section512CMLimit) {
		this.oAP4Section512CMLimit = oAP4Section512CMLimit;
	}
	
	public Integer getOAP4Section512CMDeductible() {
		return oAP4Section512CMDeductible;
	}
	public void setOAP4Section512CMDeductible(Integer oAP4Section512CMDeductible) {
		this.oAP4Section512CMDeductible = oAP4Section512CMDeductible;
	}
	
	public Double getOAP4Section512CMPremium() {
		return oAP4Section512CMPremium;
	}
	public void setOAP4Section512CMPremium(Double oAP4Section512CMPremium) {
		this.oAP4Section512CMPremium = oAP4Section512CMPremium;
	}
	
	public Integer getOAP4Section513SPLimit() {
		return oAP4Section513SPLimit;
	}
	public void setOAP4Section513SPLimit(Integer oAP4Section513SPLimit) {
		this.oAP4Section513SPLimit = oAP4Section513SPLimit;
	}
	
	public Integer getOAP4Section513SPDeductible() {
		return oAP4Section513SPDeductible;
	}
	public void setOAP4Section513SPDeductible(Integer oAP4Section513SPDeductible) {
		this.oAP4Section513SPDeductible = oAP4Section513SPDeductible;
	}
	
	public Double getOAP4Section513SPPremium() {
		return oAP4Section513SPPremium;
	}
	public void setOAP4Section513SPPremium(Double oAP4Section513SPPremium) {
		this.oAP4Section513SPPremium = oAP4Section513SPPremium;
	}
	
	public Integer getOAP4Section514SPLimit() {
		return oAP4Section514SPLimit;
	}
	public void setOAP4Section514SPLimit(Integer oAP4Section514SPLimit) {
		this.oAP4Section514SPLimit = oAP4Section514SPLimit;
	}
	
	public Integer getOAP4Section514SPDeductible() {
		return oAP4Section514SPDeductible;
	}
	public void setOAP4Section514SPDeductible(Integer oAP4Section514SPDeductible) {
		this.oAP4Section514SPDeductible = oAP4Section514SPDeductible;
	}
	
	public Double getOAP4Section514SPPremium() {
		return oAP4Section514SPPremium;
	}
	public void setOAP4Section514SPPremium(Double oAP4Section514SPPremium) {
		this.oAP4Section514SPPremium = oAP4Section514SPPremium;
	}
	
	public String getOAP4Section61CLDeductible() {
		return oAP4Section61CLDeductible;
	}
	public void setOAP4Section61CLDeductible(String oAP4Section61CLDeductible) {
		this.oAP4Section61CLDeductible = oAP4Section61CLDeductible;
	}
	
	public String getOAP4Section61CLPrem() {
		return oAP4Section61CLPrem;
	}
	public void setOAP4Section61CLPrem(String oAP4Section61CLPrem) {
		this.oAP4Section61CLPrem = oAP4Section61CLPrem;
	}
	public String getOAP4Section64SPLimit() {
		return oAP4Section64SPLimit;
	}

	public void setOAP4Section64SPLimit(String oAP4Section64SPLimit) {
		this.oAP4Section64SPLimit = oAP4Section64SPLimit;
	}
	
	public String getOAP4Section64SPDeductible() {
		return oAP4Section64SPDeductible;
	}
	public void setOAP4Section64SPDeductible(String oAP4Section64SPDeductible) {
		this.oAP4Section64SPDeductible = oAP4Section64SPDeductible;
	}
	
	public Integer getOAP4Section64SPMaximumVehicles() {
		return oAP4Section64SPMaximumVehicles;
	}
	public void setOAP4Section64SPMaximumVehicles(Integer oAP4Section64SPMaximumVehicles) {
		this.oAP4Section64SPMaximumVehicles = oAP4Section64SPMaximumVehicles;
	}
	
	public String getOAP4Section64SPPrem() {
		return oAP4Section64SPPrem;
	}
	public void setOAP4Section64SPPrem(String oAP4Section64SPPrem) {
		this.oAP4Section64SPPrem = oAP4Section64SPPrem;
	}
	
	public String getOEF71ExcludingOwnedAutomobilesIndicator() {
		return oEF71ExcludingOwnedAutomobilesIndicator;
	}
	public void setOEF71ExcludingOwnedAutomobilesIndicator(String oEF71ExcludingOwnedAutomobilesIndicator) {
		this.oEF71ExcludingOwnedAutomobilesIndicator = oEF71ExcludingOwnedAutomobilesIndicator;
	}
	
	public String getOEF72MultipleAlterationPremium() {
		return oEF72MultipleAlterationPremium;
	}
	public void setOEF72MultipleAlterationPremium(String oEF72MultipleAlterationPremium) {
		this.oEF72MultipleAlterationPremium = oEF72MultipleAlterationPremium;
	}
	
	public String getOEF73ExcludingFinancedIndicator() {
		return oEF73ExcludingFinancedIndicator;
	}
	public void setOEF73ExcludingFinancedIndicator(String oEF73ExcludingFinancedIndicator) {
		this.oEF73ExcludingFinancedIndicator = oEF73ExcludingFinancedIndicator;
	}
	
	public String getOEF74OpenLotTheftPremium() {
		return oEF74OpenLotTheftPremium;
	}
	public void setOEF74OpenLotTheftPremium(String oEF74OpenLotTheftPremium) {
		this.oEF74OpenLotTheftPremium = oEF74OpenLotTheftPremium;
	}
	
	public String getOEF75OpenLotTheftPrem() {
		return oEF75OpenLotTheftPrem;
	}
	public void setOEF75OpenLotTheftPrem(String oEF75OpenLotTheftPrem) {
		this.oEF75OpenLotTheftPrem = oEF75OpenLotTheftPrem;
	}
	
	public String getOEF76AdditionalInsuredPremium() {
		return oEF76AdditionalInsuredPremium;
	}
	public void setOEF76AdditionalInsuredPremium(String oEF76AdditionalInsuredPremium) {
		this.oEF76AdditionalInsuredPremium = oEF76AdditionalInsuredPremium;
	}
	
	public String getOEF77CustomerAutoPrem() {
		return oEF77CustomerAutoPrem;
	}
	public void setOEF77CustomerAutoPrem(String oEF77CustomerAutoPrem) {
		this.oEF77CustomerAutoPrem = oEF77CustomerAutoPrem;
	}
	
	public String getOEF78AExcludedDriverEndorsementIndicator() {
		return oEF78AExcludedDriverEndorsementIndicator;
	}
	public void setOEF78AExcludedDriverEndorsementIndicator(String oEF78AExcludedDriverEndorsementIndicator) {
		this.oEF78AExcludedDriverEndorsementIndicator = oEF78AExcludedDriverEndorsementIndicator;
	}
	
	public String getOEF78ReductionCoverageNamedPersonsIndicator() {
		return oEF78ReductionCoverageNamedPersonsIndicator;
	}
	public void setOEF78ReductionCoverageNamedPersonsIndicator(String oEF78ReductionCoverageNamedPersonsIndicator) {
		this.oEF78ReductionCoverageNamedPersonsIndicator = oEF78ReductionCoverageNamedPersonsIndicator;
	}
	
	public String getOEF79FireDeductibleOwnedAutomobilesIndicator() {
		return oEF79FireDeductibleOwnedAutomobilesIndicator;
	}
	public void setOEF79FireDeductibleOwnedAutomobilesIndicator(String oEF79FireDeductibleOwnedAutomobilesIndicator) {
		this.oEF79FireDeductibleOwnedAutomobilesIndicator = oEF79FireDeductibleOwnedAutomobilesIndicator;
	}
	
	public String getOEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium() {
		return oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium;
	}
	public void setOEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium(String oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium) {
		this.oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium = oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium;
	}
	
	public String getOEF81GarageFamilyProtectionLimit() {
		return oEF81GarageFamilyProtectionLimit;
	}
	public void setOEF81GarageFamilyProtectionLimit(String oEF81GarageFamilyProtectionLimit) {
		this.oEF81GarageFamilyProtectionLimit = oEF81GarageFamilyProtectionLimit;
	}
	
	public String getOEF81GarageFamilyProtectionPrem() {
		return oEF81GarageFamilyProtectionPrem;
	}
	public void setOEF81GarageFamilyProtectionPrem(String oEF81GarageFamilyProtectionPrem) {
		this.oEF81GarageFamilyProtectionPrem = oEF81GarageFamilyProtectionPrem;
	}
	
	public String getOEF82LiabilityDamagedNonOwnedLimit() {
		return oEF82LiabilityDamagedNonOwnedLimit;
	}
	public void setOEF82LiabilityDamagedNonOwnedLimit(String oEF82LiabilityDamagedNonOwnedLimit) {
		this.oEF82LiabilityDamagedNonOwnedLimit = oEF82LiabilityDamagedNonOwnedLimit;
	}
	
	public String getOEF82LiabilityDamagedNonOwnedDeductible() {
		return oEF82LiabilityDamagedNonOwnedDeductible;
	}
	public void setOEF82LiabilityDamagedNonOwnedDeductible(String oEF82LiabilityDamagedNonOwnedDeductible) {
		this.oEF82LiabilityDamagedNonOwnedDeductible = oEF82LiabilityDamagedNonOwnedDeductible;
	}
	
	public String getOEF82LiabilityDamagedNonOwnedPremium() {
		return oEF82LiabilityDamagedNonOwnedPremium;
	}
	public void setOEF82LiabilityDamagedNonOwnedPremium(String oEF82LiabilityDamagedNonOwnedPremium) {
		this.oEF82LiabilityDamagedNonOwnedPremium = oEF82LiabilityDamagedNonOwnedPremium;
	}
	
	public String getOEF83AutomobileTransportationPremium() {
		return oEF83AutomobileTransportationPremium;
	}
	public void setOEF83AutomobileTransportationPremium(String oEF83AutomobileTransportationPremium) {
		this.oEF83AutomobileTransportationPremium = oEF83AutomobileTransportationPremium;
	}
	
	public String getOEF85FinalPremiumComputationIndicator() {
		return oEF85FinalPremiumComputationIndicator;
	}
	public void setOEF85FinalPremiumComputationIndicator(String oEF85FinalPremiumComputationIndicator) {
		this.oEF85FinalPremiumComputationIndicator = oEF85FinalPremiumComputationIndicator;
	}
	
	public String getOEF86CustomersAutomobilesFireDeductibleIndicator() {
		return oEF86CustomersAutomobilesFireDeductibleIndicator;
	}
	public void setOEF86CustomersAutomobilesFireDeductibleIndicator(String oEF86CustomersAutomobilesFireDeductibleIndicator) {
		this.oEF86CustomersAutomobilesFireDeductibleIndicator = oEF86CustomersAutomobilesFireDeductibleIndicator;
	}
	
	public String getOAP4TotalPrem() {
		return oAP4TotalPrem;
	}
	public void setOAP4TotalPrem(String oAP4TotalPrem) {
		this.oAP4TotalPrem = oAP4TotalPrem;
	}
	
	public String getEON101BlanketLessorsIndicator() {
		return eON101BlanketLessorsIndicator;
	}
	public void setEON101BlanketLessorsIndicator(String eON101BlanketLessorsIndicator) {
		this.eON101BlanketLessorsIndicator = eON101BlanketLessorsIndicator;
	}
	
	public String getEON102AdditionalInsuredIndividualIndicator() {
		return eON102AdditionalInsuredIndividualIndicator;
	}
	public void setEON102AdditionalInsuredIndividualIndicator(String eON102AdditionalInsuredIndividualIndicator) {
		this.eON102AdditionalInsuredIndividualIndicator = eON102AdditionalInsuredIndividualIndicator;
	}
	
	public String getEON103AdditionalInsuredOOIndicator() {
		return eON103AdditionalInsuredOOIndicator;
	}
	public void setEON103AdditionalInsuredOOIndicator(String eON103AdditionalInsuredOOIndicator) {
		this.eON103AdditionalInsuredOOIndicator = eON103AdditionalInsuredOOIndicator;
	}
	
	public String getEON109InsurableInterestDeletedIndicator() {
		return eON109InsurableInterestDeletedIndicator;
	}
	public void setEON109InsurableInterestDeletedIndicator(String eON109InsurableInterestDeletedIndicator) {
		this.eON109InsurableInterestDeletedIndicator = eON109InsurableInterestDeletedIndicator;
	}
	
	public String getEON110PerOccuranceDeductibleIndicator() {
		return eON110PerOccuranceDeductibleIndicator;
	}
	public void setEON110PerOccuranceDeductibleIndicator(String eON110PerOccuranceDeductibleIndicator) {
		this.eON110PerOccuranceDeductibleIndicator = eON110PerOccuranceDeductibleIndicator;
	}
	
	public String getEON110PerOccuranceDeductible() {
		return eON110PerOccuranceDeductible;
	}
	public void setEON110PerOccuranceDeductible(String eON110PerOccuranceDeductible) {
		this.eON110PerOccuranceDeductible = eON110PerOccuranceDeductible;
	}
	
	public String getEON111ProfitSharingIndicator() {
		return eON111ProfitSharingIndicator;
	}
	public void setEON111ProfitSharingIndicator(String eON111ProfitSharingIndicator) {
		this.eON111ProfitSharingIndicator = eON111ProfitSharingIndicator;
	}
	public String getEON112ProfitSharingAdjustmentPrem() {
		return eON112ProfitSharingAdjustmentPrem;
	}
	public void setEON112ProfitSharingAdjustmentPrem(String eON112ProfitSharingAdjustmentPrem) {
		this.eON112ProfitSharingAdjustmentPrem = eON112ProfitSharingAdjustmentPrem;
	}
	
	public String getEON114NamedInsuredIndicator() {
		return eON114NamedInsuredIndicator;
	}
	public void setEON114NamedInsuredIndicator(String eON114NamedInsuredIndicator) {
		this.eON114NamedInsuredIndicator = eON114NamedInsuredIndicator;
	}
	
	public String getEON115OPCF21AReceiptsBasisAdjustmentPrem() {
		return eON115OPCF21AReceiptsBasisAdjustmentPrem;
	}
	public void setEON115OPCF21AReceiptsBasisAdjustmentPrem(String eON115OPCF21AReceiptsBasisAdjustmentPrem) {
		this.eON115OPCF21AReceiptsBasisAdjustmentPrem = eON115OPCF21AReceiptsBasisAdjustmentPrem;
	}
	
	public String getEON116OPCF21B5050BasisAdjustmentPrem() {
		return eON116OPCF21B5050BasisAdjustmentPrem;
	}
	public void setEON116OPCF21B5050BasisAdjustmentPrem(String eON116OPCF21B5050BasisAdjustmentPrem) {
		this.eON116OPCF21B5050BasisAdjustmentPrem = eON116OPCF21B5050BasisAdjustmentPrem;
	}
	
	public String getEON117OPCF21BProRataBasisAdjustmentPrem() {
		return eON117OPCF21BProRataBasisAdjustmentPrem;
	}
	public void setEON117OPCF21BProRataBasisAdjustmentPrem(String eON117OPCF21BProRataBasisAdjustmentPrem) {
		this.eON117OPCF21BProRataBasisAdjustmentPrem = eON117OPCF21BProRataBasisAdjustmentPrem;
	}
	
	public String getEON119RegisteredOwnerIndicator() {
		return eON119RegisteredOwnerIndicator;
	}
	public void setEON119RegisteredOwnerIndicator(String eON119RegisteredOwnerIndicator) {
		this.eON119RegisteredOwnerIndicator = eON119RegisteredOwnerIndicator;
	}
	
	public String getEON121PolicyCancellationPrem() {
		return eON121PolicyCancellationPrem;
	}
	public void setEON121PolicyCancellationPrem(String eON121PolicyCancellationPrem) {
		this.eON121PolicyCancellationPrem = eON121PolicyCancellationPrem;
	}
	
	public String getEON122PolicyCancellationAdjustementPrem() {
		return eON122PolicyCancellationAdjustementPrem;
	}
	public void setEON122PolicyCancellationAdjustementPrem(String eON122PolicyCancellationAdjustementPrem) {
		this.eON122PolicyCancellationAdjustementPrem = eON122PolicyCancellationAdjustementPrem;
	}
	
	public String getIndemnityAgreementIndicator() {
		return indemnityAgreementIndicator;
	}
	public void setIndemnityAgreementIndicator(String indemnityAgreementIndicator) {
		this.indemnityAgreementIndicator = indemnityAgreementIndicator;
	}
	
	public String getGarageServicePlateCoverageIndicator() {
		return garageServicePlateCoverageIndicator;
	}
	public void setGarageServicePlateCoverageIndicator(String garageServicePlateCoverageIndicator) {
		this.garageServicePlateCoverageIndicator = garageServicePlateCoverageIndicator;
	}
	
	public Integer getGarageServicePlateCount() {
		return garageServicePlateCount;
	}
	public void setGarageServicePlateCount(Integer garageServicePlateCount) {
		this.garageServicePlateCount = garageServicePlateCount;
	}
	
	public String getNetReinsurancePremium() {
		return netReinsurancePremium;
	}
	public void setNetReinsurancePremium(String netReinsurancePremium) {
		this.netReinsurancePremium = netReinsurancePremium;
	}
	
	public String getReinsurerCode() {
		return reinsurerCode;
	}
	public void setReinsurerCode(String reinsurerCode) {
		this.reinsurerCode = reinsurerCode;
	}
	
	public String getRISKID() {
		return rISKID;
	}
	public void setRISKID(String rISKID) {
		this.rISKID = rISKID;
	}
	
	public String getProgramCode() {
		return programCode;
	}
	public void setProgramCode(String programCode) {
		this.programCode = programCode;
	}
	public String getProduct() {
		return product;
	}
	public void setProduct(String product) {
		this.product = product;
	}
	
	public String getBlankEndorsement() {
		return blankEndorsement;
	}
	public void setBlankEndorsement(String blankEndorsement) {
		this.blankEndorsement = blankEndorsement;
	}
	
	public String getTotalPrem() {
		return totalPrem;
	}
	public void setTotalPrem(String totalPrem) {
		this.totalPrem = totalPrem;
	}
	
	public Double getBrokerCommissionAmount() {
		return brokerCommissionAmount;
	}
	public void setBrokerCommissionAmount(Double brokerCommissionAmount) {
		this.brokerCommissionAmount = brokerCommissionAmount;
	}

	public String getSubBrokerName() {
		return subBrokerName;
	}

	public void setSubBrokerName(String subBrokerName) {
		this.subBrokerName = subBrokerName;
	}

	public String getSubBrokerStreet() {
		return subBrokerStreet;
	}

	public void setSubBrokerStreet(String subBrokerStreet) {
		this.subBrokerStreet = subBrokerStreet;
	}

	public String getSubBrokerCity() {
		return subBrokerCity;
	}

	public void setSubBrokerCity(String subBrokerCity) {
		this.subBrokerCity = subBrokerCity;
	}

	public String getSubBrokerPostalCode() {
		return subBrokerPostalCode;
	}

	public void setSubBrokerPostalCode(String subBrokerPostalCode) {
		this.subBrokerPostalCode = subBrokerPostalCode;
	}

	public String getSubBrokerNumber() {
		return subBrokerNumber;
	}

	public void setSubBrokerNumber(String subBrokerNumber) {
		this.subBrokerNumber = subBrokerNumber;
	}
	
}

