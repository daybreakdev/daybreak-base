package com.echelon.services.bordereau;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.CoverageReinsurance;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class BordereauxService {

	private static List<Bordereaux> bordereauxList;

	public Stream<Bordereaux> getBordereauxInformation(List<PolicyTransaction> pt, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) throws Exception {

		ensureTestData(pt, ptAllList, policyProcessing);

		Stream<Bordereaux> filtered = bordereauxList.stream();

		return filtered;
	}

	private int getPolicyMaxRetainedLimit(PolicyTransaction<?> pcyTxn, IGenericProductConfig prodConfig) {
		/*
		 * This code is intended to be temporary. The intent is to allow LHT ON policies
		 * issued using the old (v1) ratebook to use the old maxRetainedLimit value of
		 * 2,000,000. Policies issued using v2 will use the current value of 5M instead.
		 * 
		 * See ES-106 for further information.
		 */
		if ("ONLHT".equalsIgnoreCase(pcyTxn.getPolicyTerm().getRatebookId())
				&& Objects.equals(1, pcyTxn.getPolicyTerm().getRatebookVersion())) {
			return 2_000_000;
		}
		// End of ES-106 "maxRetainedLimit override" code

		return prodConfig.getMaxRetainedLimit();
	}

	private void ensureTestData(List<PolicyTransaction> ptlist, List<PolicyTransaction> ptAllList, IGenericPolicyProcessing policyProcessing) throws Exception {

		bordereauxList = new ArrayList<>();

		if (ptlist == null || (ptlist != null && ptlist.size() == 0)) {
			// Empty report
			setBorderauxCargo(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,
					null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
		} else {
			
			//Get the New business to print in case that we are dealing with a reissue transaction.
			boolean isReissueMainTransaction = false;
			PolicyTransaction ptAdjustment = null;
			if (ptAllList != null) {
				for (PolicyTransaction pt : ptlist) {
					if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)) {
						isReissueMainTransaction = true;
					}
				}
				
				if (isReissueMainTransaction) {
					for (PolicyTransaction<?> ptAll : ptAllList) {
						if (ptAll.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && ptAll.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET)) {
							ptlist.add(0, ptAll);
							//ptlist.add(ptlist.get(0));
							break;
						}
					}
				}
				
				
				for (PolicyTransaction<?> ptAll : ptAllList) {
					if (ptAll.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && ptAll.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET)) {
						ptAdjustment = ptAll; //This is going to take always the most recent adjustment
					}
				}
				
			}

				
					


			for (PolicyTransaction pt : ptlist) {

				// Sub Policies
				SpecialityAutoSubPolicy<Risk> subPolicy = null;
				CGLSubPolicy<Risk> subPolicyCGL = null;
				CargoSubPolicy<Risk> subPolicyCGO = null;
				GarageSubPolicy<Risk> subPolicyGAR = null;
				CommercialPropertySubPolicy<Risk> subPolicyCP = null;
				boolean isSubPolicyCP = false;
				String PolicyFleetIndicator = null;

				Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

				for (SubPolicy<Risk> subPol : subPolicies) {
					if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
						subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
					} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
						subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
					} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
						subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
					} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
						subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
						if (subPolicy.getIsFleet()!=null && subPolicy.getIsFleet()) {
							PolicyFleetIndicator = "Fleet";
						} else {
							PolicyFleetIndicator = "Non-Fleet";
						}
					} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
						subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
						isSubPolicyCP = true;
					}
				}
				
				
				
				IGenericProductConfig productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", pt.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						pt.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						pt.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						pt.getPolicyTerm().getTermEffDate().toLocalDate());
				Integer polSec3Limit = null;
				if (productConfig != null) {
					polSec3Limit = Integer.valueOf(getPolicyMaxRetainedLimit(pt, productConfig));
				}

				
				//Identify if this is a LHT policy above 2000000 LiabilityLimit
				boolean isLht2Millions = false;
				boolean isLht = false;
				String productCode = pt.getPolicyVersion().getInsurancePolicy().getProductCd();
				Integer liabilityLimit = ((SpecialtyAutoPackage)pt.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit();
				if (productCode != null & productCode.equals(ConfigConstants.PRODUCTCODE_LHT)) {
					//isLht = true; I had to comment this because Sangeetha told me that I have to show all the Limit and deductible, I added this initialy because a requirement of the tickets ESL-2042, ESL-2027, ESL-2023, and ESL-2021 but seems that it is causing problems.
					if (liabilityLimit != null && liabilityLimit.compareTo(polSec3Limit)>0) {
						isLht2Millions = true;
					}
				}
				
				
				// SpecialtyAutoPackage.liabilityLimit
				SpecialtyAutoPackage sAutoPackage = (SpecialtyAutoPackage) pt.getPolicyTerm().getInsurancePolicy();
				

				
				//Check if Subpolicy is a renewal
				boolean isrenewal = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL)) {
					isrenewal = true;
				}
				
				//Check if Subpolicy is a cancellation
				boolean iscancellation = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION)) {
					iscancellation = true;
				}
				
				
				//Check if Subpolicy is a Reinstatement
				boolean isReinstatement = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT)) {
					isReinstatement = true;
				}
				
				//Check if Subpolicy is a reissue and if the quantity is going to be negative in the new bussiness transaction
				boolean isReissue = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)) {
					isReissue = true;
				}
				boolean isNegativeQty = false;//This is going to be always false because the reissue offset transaction is going to have always negative quantities, so this variable is never going to be used
				//if (isReissueMainTransaction && pt.getPolicyVersion().getPolicyTxnType().equals("NEWBUSINESS")) {
					//isNegativeQty = true;
				//}
				
				//Check if Subpolicy is a extension
				boolean isextension = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION)) {
					isextension = true;
				}
				
				
				//Check if Subpolicy is an adjustment
				boolean isAdjustment = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT)) {
					isAdjustment = true;
				}
				
				
				
				

				// C A R G O

				if (subPolicyCGO != null) {
					
					
					//Cargo Adjustments
					List<SpecialtyVehicleRiskAdjustment> cargoAdjustmentsList = new ArrayList<>();
					Set<SpecialtyVehicleRiskAdjustment> cargoAdjustments = subPolicyCGO.getSvrAdjustments();
					
					if (cargoAdjustments == null) {
						SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
						cargoAdjustmentsList.add(emptyAdjustment);
					} else if (cargoAdjustments.size() == 0) {
						SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
						cargoAdjustmentsList.add(emptyAdjustment);
					} else {
						cargoAdjustmentsList = cargoAdjustments.stream().collect(Collectors.toList());
					}
					
					for (SpecialtyVehicleRiskAdjustment cargoAdjustment : cargoAdjustmentsList) {
						

					
						//Check if Subpolicy was changed
						boolean ischange = false;
						if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)
								&& !subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
							ischange = true;
						}
						
						//Check if the endorsments and coverages from the Policy have been changed
						if (isSubPolicyEndorAndCovChanged(pt, SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
							ischange = true;
						}
						
						//Check if Subpolicy is a reversal
						boolean isreversal = false;
						if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)
								&& !subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
							isreversal = true;
							ischange = true;//Set "ischange" to true because Sangeetha asked me to add the exactly behavior for PolicyChange also in PolicyReversal status.
						}
						
	
						
						
						if (isReissue || isAdjustment || isReinstatement || (iscancellation && !subPolicyCGO.getSystemStatus().equals(Constants.BUSINESS_STATUS_INACTIVE)) || isreversal || ischange || isrenewal || isextension || pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS)) {
							
	
							// Contents of Personal Effects Indicator
							String contentsPersonalEffectsIndicator = coverageExist(pt, "CPE", SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
	
							// Contents of Personal Effects Limit
							Integer contentsPersonalEffectsLimit = getCoverageLimit1(pt, "CPE", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
	
							// Contents of Personal Effects Deductible
							Integer contentsPersonalEffectsDeductible = getCoverageDeductible(pt, "CPE", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
	
							// Contents of Personal Effects Premium
							Double contentsPersonalEffectsPremium = getCoveragePremiumAdjustments(pt, "CPE", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, ischange, isNegativeQty, isAdjustment, cargoAdjustment, policyProcessing);
	
							// Contingent Cargo Limit CTC
							Integer contingentCargoLimit = getEndormentLimit1(pt, "CTC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
							// Integer contingentCargoLimit = null;
	
							// Contingent Cargo Deductible CTC
							Integer contingentCargoDeductible = getEndorsementDeductible(pt, "CTC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
	
							// Contingent Cargo Prem CTC
							Double contingentCargoPrem = null;
							if (isAdjustment) {
								contingentCargoPrem = (double)0;
							}else {
								contingentCargoPrem = getEndorsementPremium(pt, "CTC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, ischange, isNegativeQty);
							}

							// Double contingentCargoPrem = null;
	
							// Identity Misrepresentation Limit
							// Integer identityMisrepresentationLimit=getCoverageLimit1(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
							Integer identityMisrepresentationLimit = null;
	
							// Identity Misrepresentation Premium
							// Double identityMisrepresentationPremium=getCoveragePremium(pt, "MTCC",
							// SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
							Double identityMisrepresentationPremium = null;
	
							// Temporary Storage Limit
							// Integer temporaryStorageLimit=getCoverageLimit1(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
							Integer temporaryStorageLimit = null;
	
							// Temporary Storage Premium
							// Double temporaryStoragePremium=getCoveragePremium(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
							Double temporaryStoragePremium = null;
	
							// Trip Transit Cargo Additional Limit TTC
							Integer tripTransitCargoAdditionalLimit = getEndormentLimit1(pt, "TTC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
	
							// Trip Transit Cargo Prem TTC
							Double tripTransitCargoPrem = null;
							if (isAdjustment) {
								tripTransitCargoPrem = (double)0;
							}else {
								tripTransitCargoPrem = getEndorsementPremium(pt, "TTC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, ischange, isNegativeQty);
							} 
							
							// EON141 Change to Deductible Indicator 
							String eON141ChangetoDeductibleIndicator  = eON141EndorsementExist(pt, "EON141");
	
							// Cargo General Endorsement Prem
							// Double cargoGeneralEndorsementPrem = getEndorsementPremium(pt, "GECGLMTC",
							// SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
							Double cargoGeneralEndorsementPrem = null;
	
							// Core Cargo Total Prem
							/* Double cargoTotalPremCPE=getCoveragePremium(pt, "CPE", SpecialtyAutoConstants.SUBPCY_CODE_CARGO); */
							Double cargoTotalPremMTCC = getCoveragePremiumAdjustments(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, ischange, isNegativeQty, isAdjustment, cargoAdjustment, policyProcessing);
							

	
							Double coreCargoTotalPrem = 0.0;
							if (productCode != null & productCode.equals("LHT")) {
								coreCargoTotalPrem = /* cargoTotalPremCPE+ */cargoTotalPremMTCC + tripTransitCargoPrem +contingentCargoPrem + contentsPersonalEffectsPremium;
							}else {
								coreCargoTotalPrem = /* cargoTotalPremCPE+ */cargoTotalPremMTCC;
							}
							
	
							// Cargo Limit
							Integer cargoLimit = getCoverageLimit1(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
	
							// Cargo Deductible
							Integer cargoDeductible = getCoverageDeductible(pt, "MTCC", SpecialtyAutoConstants.SUBPCY_CODE_CARGO, isLht, isNegativeQty);
							
							//Cargo Total Premium
							Double cargoTotalPrem = (double) 0;
							if (isAdjustment) {
								cargoTotalPrem = cargoTotalPremMTCC;
							}else {
								cargoTotalPrem = cargoTotalPremMTCC + tripTransitCargoPrem +contingentCargoPrem + contentsPersonalEffectsPremium;
							}

	
							setBorderauxCargo(
									// General Data 1
									pt.getPolicyTerm().getTermNumber(), 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? pt.getPolicyVersion().getPolicyVersionNum() : pt.getPolicyVersion().getPolicyVersionNum()+1, 
									pt.getPolicyTerm().getInsurancePolicy().getBasePolicyNum(),
									pt.getPolicyTerm().getTermEffDate().toLocalDate(),
									pt.getPolicyTerm().getTermExpDate().toLocalDate(),
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? pt.getPolicyVersion().getVersionDate() :
											pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? pt.getPolicyVersion().getVersionDate() :
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? pt.getPolicyVersion().getVersionDate() :
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? cargoAdjustment.getAdjustmentDate()
											: pt.getPolicyTerm().getTermEffDate().toLocalDate(),
									(pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyCGO.getBusinessStatus().equals("NEW") ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !subPolicyCGO.getBusinessStatus().equals("NEW") ? "END" 		
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyCGO.getBusinessStatus().equals("NEW") ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && !subPolicyCGO.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !subPolicyCGO.getBusinessStatus().equals("NEW") ? "END" 
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_ONSET) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? "END"
											: pt.getPolicyVersion().getPolicyTxnType()),
									PolicyFleetIndicator,
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(), "N",
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "NEW" :
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW":
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN":
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									subPolicyCGO.getBusinessStatus(),
									isLht2Millions ? polSec3Limit : (sAutoPackage != null ? sAutoPackage.getLiabilityLimit() : null),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
									pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getRin() : 
									null,
									pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
									pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName() : 
									pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer().getFullName(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getAddressLine1(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getCity(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getPostalZip().trim()
											.replace(" ", ""),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLegalName(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getAddressLine1(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getCity(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getPostalZip().trim().replace(" ", ""),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId(),
	
									// Cargo
									contentsPersonalEffectsLimit, contentsPersonalEffectsDeductible,
									contentsPersonalEffectsPremium,
									// (contentsPersonalEffectsIndicator.equals("N")?((subPolicyCGO!=null)?subPolicyCGO.getCargoLimit():null):null),
									// ((subPolicyCGO!=null)?subPolicyCGO.getCargoLimit():null),
									cargoLimit,
									// (contentsPersonalEffectsIndicator.equals("N")?((subPolicyCGO!=null)?subPolicyCGO.getCargoDeductible():null):null),
									// ((subPolicyCGO!=null)?subPolicyCGO.getCargoDeductible():null),
									cargoDeductible, null, /* (subPolicyCGO!=null)?subPolicyCGO.getCargoLimit():null */null,
									null, contingentCargoLimit, contingentCargoDeductible, contingentCargoPrem,
									/* (subPolicyCGO!=null)?subPolicyCGO.getCargoLimit():null */null, null,
									/* (subPolicyCGO!=null)?subPolicyCGO.getCargoLimit():null */null, null,
									identityMisrepresentationLimit, identityMisrepresentationPremium, temporaryStorageLimit,
									temporaryStoragePremium, tripTransitCargoAdditionalLimit, tripTransitCargoPrem, null,
									null, null, eON141ChangetoDeductibleIndicator, null, null, null, null, null, cargoGeneralEndorsementPrem,
									/*
									 * (subPolicyCGO!= null)? subPolicyCGO. getSubPolicyPremium ().
									 * getTransactionPremium ():null
									 */coreCargoTotalPrem,
	
									// General Data 2
									null, null, "CARGO", productCode != null & productCode.equals(ConfigConstants.PRODUCTCODE_LHT) ? "LO" : "TO",
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
									.getProducerId() != null ? (
									((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId())
											.substring(0, 3))
													.concat(((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
															.getProducerId())
																	.substring(
																			pt.getPolicyTerm().getInsurancePolicy()
																					.getPolicyProducer().getProducerId()
																					.length() - 2,
																			pt.getPolicyTerm().getInsurancePolicy()
																					.getPolicyProducer().getProducerId()
																					.length())))):"",
									null,
									isAdjustment ? getCGOPremiumChangeFromAdjustment(subPolicyCGO, cargoTotalPrem, cargoAdjustment) : cargoTotalPrem,
									(isAdjustment ? getCGOPremiumChangeFromAdjustment(subPolicyCGO, cargoTotalPrem, cargoAdjustment) : cargoTotalPrem) * pt.getPolicyVersion().getProducerCommissionRate() / 100);
						}
					}
				}

				// C G L

				if (subPolicyCGL != null) {
					
					
					//CGL Adjustments
					List<SpecialtyVehicleRiskAdjustment> cglAdjustmentsList = new ArrayList<>();
					Set<SpecialtyVehicleRiskAdjustment> cglAdjustments = subPolicyCGL.getSvrAdjustments();
					
					if (cglAdjustments == null) {
						SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
						cglAdjustmentsList.add(emptyAdjustment);
					} else if (cglAdjustments.size() == 0) {
						SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
						cglAdjustmentsList.add(emptyAdjustment);
					} else {
						cglAdjustmentsList = cglAdjustments.stream().collect(Collectors.toList());
					}
					
					for (SpecialtyVehicleRiskAdjustment cglAdjustment : cglAdjustmentsList) {
						

						
					
						//Check if Subpolicy was changed
						boolean ischange = false;
						if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)
								&& !subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
							ischange = true;
						}
						
						//Check if the endorsments and coverages from the Policy have been changed
						if (isSubPolicyEndorAndCovChanged(pt, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
							ischange = true;
						}
						
						//Check if Subpolicy is a reversal
						boolean isreversal = false;
						if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)
								&& !subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
							isreversal = true;
							ischange = true;//Set "ischange" to true because Sangeetha asked me to add the exactly behavior for PolicyChange also in PolicyReversal status.
						}
						
						
						
						if (isReissue || isAdjustment || isReinstatement || (iscancellation && !subPolicyCGL.getSystemStatus().equals(Constants.BUSINESS_STATUS_INACTIVE)) || isreversal || ischange || isrenewal || isextension || pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS)) {
							
	
							// Tenants Legal Liability Limit TLL
							Integer tenantsLegalLiabilityLimit = null;
							if (isAdjustment) {
								tenantsLegalLiabilityLimit = null;
							}else {
								tenantsLegalLiabilityLimit = getCoverageLimit1(pt, "TLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
							}
								
							
	
							// Tenants Legal Liability Deductible TLL
							Integer tenantsLegalLiabilityDeductible = null;
							if (isAdjustment) {
								tenantsLegalLiabilityDeductible = null;
							}else {
								tenantsLegalLiabilityDeductible = getCoverageDeductible(pt, "TLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
							}
	
							// Tenants Legal Liability Prem TLL
							Double TenantsLegalLiabilityPrem = (double) 0;
							if (isAdjustment) {
								TenantsLegalLiabilityPrem = (double) 0;
							}else {
								TenantsLegalLiabilityPrem = getCoveragePremiumAdjustments(pt, "TLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing);
							}
	
							// QE4023N Warehouseman Legal Liability Limit WLL
							Integer qE4023NWarehousemanLegalLiabilityLimit = getCoverageLimit1(pt, "WLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// QE4023N Warehouseman Legal Liability Deductible WLL
							Integer qE4023NWarehousemanLegalLiabilityDeductible = getCoverageDeductible(pt, "WLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// QE4023N Warehouseman Legal Liability Prem WLL
							Double qE4023NWarehousemanLegalLiabilityPrem = getCoveragePremiumAdjustments(pt, "WLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing);
	
							// QE2033N Employee Benefits Extension Limit EBE
							Integer qE2033NEmployeeBenefitsExtensionLimit = getCoverageLimit1(pt, "EBE", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// QE2033N Employee Benefits Extension Deductible EBE
							Integer qE2033NEmployeeBenefitsExtensionDeductible = getCoverageDeductible(pt, "EBE", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// QE2033N Employee Benefits Extension Prem EBE
							Double qE2033NEmployeeBenefitsExtensionPrem = getCoveragePremiumAdjustments(pt, "EBE", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing);
	
							// QE2313N Limited Pollution Liability Limit LPOLL
							// Integer qE2313NLimitedPollutionLiabilityLimit = getCoverageLimit1(pt, "POLL",
							// SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
							Integer qE2313NLimitedPollutionLiabilityLimit = null;
	
							// QE2313N Limited Pollution Liability Deductible LPOLL
							// Integer qE2313NLimitedPollutionLiabilityDeductible =
							// getCoverageDeductible(pt, "POLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
							Integer qE2313NLimitedPollutionLiabilityDeductible = null;
	
							// QE2313N Limited Pollution Liability Prem LPOLL
							// Double qE2313NLimitedPollutionLiabilityPrem = getCoveragePremium(pt, "POLL",
							// SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
							Double qE2313NLimitedPollutionLiabilityPrem = null;
	
							// SPF6 Non Owned Automobile Indicator
							String sPF6NonOwnedAutomobileIndicatorExists = coverageExist(pt, "SPF6TPL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
	
							// SEF94 Legal Liability Indicator
							String sEF94LegalLiabilityIndicator = "N";
	
							// SEF96 Contractual Liability Indicator
							String sEF96ContractualLiabilityIndicator = endorsementExist(pt, "SEF96", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
	
							// SEF99 Leased Vehicle Indicator
							String sEF99LeasedVehicleIndicator = endorsementExist(pt, "SEF99", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
	
							// Garage Liability Extension Indicator
							String garageLiabilityExtensionIndicator = endorsementExist(pt, "GLEE", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
	
							// QE2345N Total Pollution Exclusion Indicator
							// String qE2345NTotalPollutionExclusionIndicatorExists = coverageExist(pt,
							// "POLL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
							String qE2345NTotalPollutionExclusionIndicatorExists = null;
	
							// QE2345N Total Pollution Exclusion Premium
							// Double qE2345NTotalPollutionExclusionPremium = getCoveragePremium(pt, "POLL",
							// SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
							Double qE2345NTotalPollutionExclusionPremium = null;
	
							// Mobile Equipment Limit MOBEQ
							Integer mobileEquipmentLimit = getEndormentLimit1(pt, "MOBEQ", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// Mobile Equipment Deductible MOBEQ
							Integer mobileEquipmentDeductible = getCoverageDeductible(pt, "MOBEQ", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, isLht, isNegativeQty);
	
							// Mobile Equipment Prem MOBEQ
							Double mobileEquipmentPrem = getCoveragePremiumAdjustments(pt, "MOBEQ", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing);
	
							// CGL General Endorsement Prem
							Double cGLGeneralEndorsementPrem = getCoveragePremiumAdjustments(pt, "GECGL", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing);
	
							// Core CGL Total Prem
							Double coreCGLTotalPrem = null;
							Double coreCGLTotalPremAux = null;
							if (productCode != null & productCode.equals("LHT")) {
								if (isLht2Millions) {
									coreCGLTotalPremAux = (getCGLCoveragePremiumLHTReinsurance(pt, "BIPD", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment));
									coreCGLTotalPrem = coreCGLTotalPremAux;
								}else {
									coreCGLTotalPremAux = (getCGLCoveragePremiumLHTAdjustment(pt, "BIPD", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing));
									coreCGLTotalPrem = coreCGLTotalPremAux + cGLGeneralEndorsementPrem +mobileEquipmentPrem + qE2033NEmployeeBenefitsExtensionPrem + qE4023NWarehousemanLegalLiabilityPrem + 
											TenantsLegalLiabilityPrem;
								}
								

							}else {
								coreCGLTotalPremAux = (getCGLCoveragePremiumLHTAdjustment(pt, "BIPD", SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, ischange, isNegativeQty, isAdjustment, cglAdjustment, policyProcessing));
								coreCGLTotalPrem = coreCGLTotalPremAux;
							}
							
							
							
							//CGL Total Premium
							Double cGLTotalPrem = coreCGLTotalPremAux + cGLGeneralEndorsementPrem +mobileEquipmentPrem + qE2033NEmployeeBenefitsExtensionPrem + qE4023NWarehousemanLegalLiabilityPrem + 
									TenantsLegalLiabilityPrem;
	
	
							setBorderauxCGL(
									// General Data 1
									pt.getPolicyTerm().getTermNumber(), 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? pt.getPolicyVersion().getPolicyVersionNum() : pt.getPolicyVersion().getPolicyVersionNum()+1, 
									pt.getPolicyTerm().getInsurancePolicy().getBasePolicyNum(),
									pt.getPolicyTerm().getTermEffDate().toLocalDate(),
									pt.getPolicyTerm().getTermExpDate().toLocalDate(),
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? pt.getPolicyVersion().getVersionDate() :
										pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? pt.getPolicyVersion().getVersionDate() :
										pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? pt.getPolicyVersion().getVersionDate() :
										pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? cglAdjustment.getAdjustmentDate()
										: pt.getPolicyTerm().getTermEffDate().toLocalDate(),
									(pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyCGL.getBusinessStatus().equals("NEW") ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !subPolicyCGL.getBusinessStatus().equals("NEW") ? "END" 
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyCGL.getBusinessStatus().equals("NEW") ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && !subPolicyCGL.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !subPolicyCGL.getBusinessStatus().equals("NEW") ? "END" 
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? "CAN"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_ONSET) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) ? "NEW"
											: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? "END"
											: pt.getPolicyVersion().getPolicyTxnType()),
									PolicyFleetIndicator,
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(), "N",
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "REN" :
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "NEW" :
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW":
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN":
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
									pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
									subPolicyCGL.getBusinessStatus(), 
									isLht2Millions ? polSec3Limit : (sAutoPackage != null ? sAutoPackage.getLiabilityLimit() : null),
			 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
			 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getRin() : 
			 						null,
			 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
			 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName() : 
			 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer().getFullName(),									
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getAddressLine1(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getCity(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
									pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getPostalZip().trim()
											.replace(" ", ""),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLegalName(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getAddressLine1(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getCity(),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
											.getPostalZip().trim().replace(" ", ""),
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId(),
									// CGL
									(subPolicyCGL != null) ? subPolicyCGL.getCglDeductible() : null,
									(subPolicyCGL != null) ? subPolicyCGL.getCglLimit() : null, null, null, null,
									tenantsLegalLiabilityLimit, tenantsLegalLiabilityDeductible, TenantsLegalLiabilityPrem,
									qE4023NWarehousemanLegalLiabilityLimit, qE4023NWarehousemanLegalLiabilityDeductible,
									qE4023NWarehousemanLegalLiabilityPrem, qE2033NEmployeeBenefitsExtensionLimit,
									qE2033NEmployeeBenefitsExtensionDeductible, qE2033NEmployeeBenefitsExtensionPrem,
									qE2313NLimitedPollutionLiabilityLimit, qE2313NLimitedPollutionLiabilityDeductible,
									qE2313NLimitedPollutionLiabilityPrem, sPF6NonOwnedAutomobileIndicatorExists,
									sEF94LegalLiabilityIndicator, sEF96ContractualLiabilityIndicator,
									sEF99LeasedVehicleIndicator, garageLiabilityExtensionIndicator,
									qE2345NTotalPollutionExclusionIndicatorExists, qE2345NTotalPollutionExclusionPremium,
									mobileEquipmentLimit, mobileEquipmentDeductible, mobileEquipmentPrem,
									cGLGeneralEndorsementPrem, /*
																 * (subPolicyCGL!=null)?subPolicyCGL.getSubPolicyPremium().
																 * getTransactionPremium():null
																 */coreCGLTotalPrem,
	
									// General Data 2
									null, null, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, productCode != null & productCode.equals("LHT") ? "LO" : "TO",
									pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
									.getProducerId() != null ? (
									((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId())
											.substring(0, 3))
													.concat(((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
															.getProducerId())
																	.substring(
																			pt.getPolicyTerm().getInsurancePolicy()
																					.getPolicyProducer().getProducerId()
																					.length() - 2,
																			pt.getPolicyTerm().getInsurancePolicy()
																					.getPolicyProducer().getProducerId()
																					.length())))):"",
									null, isAdjustment ? getCGLPremiumChangeFromAdjustment(subPolicyCGL, cGLTotalPrem, cglAdjustment, policyProcessing) : cGLTotalPrem,
									(isAdjustment ? getCGLPremiumChangeFromAdjustment(subPolicyCGL, cGLTotalPrem, cglAdjustment, policyProcessing) : cGLTotalPrem) * pt.getPolicyVersion().getProducerCommissionRate() / 100);
						}
					}
				}

				// G A R A G E

				if (subPolicyGAR != null) {
					
					//Check if Subpolicy was changed
					boolean ischange = false;
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)
							&& !subPolicyGAR.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
						ischange = true;
					}
					
					//Check if the endorsments and coverages from the Policy have been changed
					if (isSubPolicyEndorAndCovChanged(pt, SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
						ischange = true;
					}
					
					//Check if Subpolicy is a reversal
					boolean isreversal = false;
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)
							&& !subPolicyGAR.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
						isreversal = true;
						ischange = true;//Set "ischange" to true because Sangeetha asked me to add the exactly behavior for PolicyChange also in PolicyReversal status.
					}
					

					
					
					if (isReissue || isReinstatement || (iscancellation && !subPolicyGAR.getSystemStatus().equals(Constants.BUSINESS_STATUS_INACTIVE)) || isreversal || ischange || isrenewal || isextension || pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS)) {
						

						Integer countCovLocations = 0;
						Integer countAllLocations = 0;
						Integer countAllChangedLocations = 0;
						Integer garAutoIncrement = 0;
						Integer garLocationAutoIncrement = 0;
						Set<Risk> polRisk = pt.getPolicyRisks();
						for (Risk risk : polRisk) {
							if (risk.getRiskType().equalsIgnoreCase("LO")) {
								PolicyLocation pl = (PolicyLocation) risk;
								if (!pl.getLocationId().equals("MAILING")) {//count all locations just exclude mailing address
									countAllLocations++;
									if ((ischange && (locationChanged(pl)))) {//Condition added to only count locations modified/added/deleted
										countAllChangedLocations++;
									}
								}
								Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
								Coverage cove512CM = pl.getCoverageDetail().getCoverageByCode("CM512");
								Coverage cove513SP = pl.getCoverageDetail().getCoverageByCode("SP513");
								Coverage cove514SP = pl.getCoverageDetail().getCoverageByCode("SP514");
								if (coveSP64 != null || cove512CM != null || cove513SP != null || cove514SP != null) {
									if (!ischange || (ischange && (
												(coveSP64 != null ? !coveSP64.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE) : false) ||
												(cove512CM != null ? !cove512CM.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE) : false) ||
												(cove513SP != null ? !cove513SP.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE) : false) ||
												(cove514SP != null ? !cove514SP.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE) : false)
									))) {//Condition added to only count locations modified/added/deleted
										countCovLocations++;
									}
								}
							}
						}

						for (Risk risk : polRisk) {
							if (risk.getRiskType().equalsIgnoreCase("LO")) {

								DecimalFormat df = new DecimalFormat("#.##");
								df.setRoundingMode(RoundingMode.FLOOR);

								PolicyLocation pl = (PolicyLocation) risk;

								Coverage coveSP64 = pl.getCoverageDetail().getCoverageByCode("SP64");
								Coverage cove512CM = pl.getCoverageDetail().getCoverageByCode("CM512");
								Coverage cove513SP = pl.getCoverageDetail().getCoverageByCode("SP513");
								Coverage cove514SP = pl.getCoverageDetail().getCoverageByCode("SP514");

								
								//if (coveSP64 != null || cove512CM != null || cove513SP != null || cove514SP != null) {
									//if (!ischange || (ischange && (
											//(coveSP64 != null ? !coveSP64.getBusinessStatus().equals("NOCHANGE") : false) ||
											//(cove512CM != null ? !cove512CM.getBusinessStatus().equals("NOCHANGE") : false) ||
											//(cove513SP != null ? !cove513SP.getBusinessStatus().equals("NOCHANGE") : false) ||
											//(cove514SP != null ? !cove514SP.getBusinessStatus().equals("NOCHANGE") : false)
									//))) {//Condition added to only count locations modified/added/deleted
								if (!pl.getLocationId().equals("MAILING")) {//print records for all locations and exclude mailing address
									garAutoIncrement++;
									if (!ischange || (ischange && (locationChanged(pl)))) {//Condition added to only count locations modified/added/deleted
										garLocationAutoIncrement++;
	
										// OAP4 Section 1 Limit
										Integer oAP4Section1Limit = getCoverageLimit1(pt, "TPL", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OAP4 Section 1 BI Prem
										Double oAP4Section1BIPrem = null;
										if (isLht2Millions) {
											oAP4Section1BIPrem = divideGarageAmounts(ischange, getCoveragePremiumLHT(pt, "BI", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}else {
											oAP4Section1BIPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "BI", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}
	
										// OAP4 Section 1 PD Prem
										Double oAP4Section1PDPrem = null;
										if (isLht2Millions) {
											oAP4Section1PDPrem = divideGarageAmounts(ischange, getCoveragePremiumLHT(pt, "PD", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}else {
											oAP4Section1PDPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "PD", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}
										
										// OAP4 Section 2 AB Prem
										Double oAP4Section2ABPrem = null;
										if (isLht2Millions) {
											oAP4Section2ABPrem = divideGarageAmounts(ischange, getCoveragePremiumLHT(pt, "AB", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}else {
											oAP4Section2ABPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "AB", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										}
										
										// OAP4 Section 3 UA Prem
										Double oAP4Section3UAPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "UA", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										
										// OAP4 Section 4 DCPD Deductible
										Integer oAP4Section4DCPDDeductible = getCoverageDeductible(pt, "DCPD", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OAP4 Section 4 DCPD Prem
										Double oAP4Section4DCPDPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "DCPD", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OAP4 Section 511 CL Deductible
										Integer oAP4Section511CLDeductible = getCoverageDeductible(pt, "CL511", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OAP4 Section 511 CL Premium
										Double oAP4Section511CLPremium = divideGarageAmounts(ischange, getCoveragePremium(pt, "CL511", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OAP4 Section 512 CM Limit
										// Integer oAP4Section512CMLimit = getCoverageLimit1(pt, "CM512", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section512CMLimit = null;
										if (!isLht) {
											oAP4Section512CMLimit = (cove512CM != null
													? cove512CM.getLimit1() : null);
										}
	
										// OAP4 Section 512 CM Deductible
										// Integer oAP4Section512CMDeductible = getCoverageDeductible(pt, "CM512",
										// SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section512CMDeductible = null;
										if (!isLht) {
											oAP4Section512CMDeductible = (cove512CM != null
													? cove512CM.getDeductible1() : null);
										}
	
										// OAP4 Section 512 CM Premium
										// Double oAP4Section512CMPremium = getCoveragePremium(pt, "CM512", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Double oAP4Section512CMPremium = null;
										//if ((ischange && cove512CM != null && !cove512CM.getBusinessStatus().contentEquals("NOCHANGE"))) {
										if (ischange) {
											oAP4Section512CMPremium = (cove512CM != null
													? cove512CM.getCoveragePremium().getNetPremiumChange()
													: null);
										}else {
											oAP4Section512CMPremium = (cove512CM != null
													? cove512CM.getCoveragePremium().getTransactionPremium()
													: null);
										}
										if(isNegativeQty) {
											if (oAP4Section512CMPremium != null) {
												oAP4Section512CMPremium = oAP4Section512CMPremium * -1;
											}
										}
	
										// OAP4 Section 513 SP Limit
										// Integer oAP4Section513SPLimit = getCoverageLimit1(pt, "SP513", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section513SPLimit = null;
										if (!isLht) {
											oAP4Section513SPLimit = (cove513SP != null
													? cove513SP.getLimit1() : null);
										}
	
										// OAP4 Section 513 SP Deductible
										// Integer oAP4Section513SPDeductible = getCoverageDeductible(pt, "SP513",
										// SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section513SPDeductible = null;
										if (!isLht) {
											oAP4Section513SPDeductible = (cove513SP != null
													? cove513SP.getDeductible1() : null);
										}
	
										// OAP4 Section 513 SP Premium
										// Double oAP4Section513SPPremium = getCoveragePremium(pt, "SP513", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Double oAP4Section513SPPremium = null;
										//if ((ischange && cove513SP != null && !cove513SP.getBusinessStatus().contentEquals("NOCHANGE"))) {
										if (ischange) {
											oAP4Section513SPPremium = (cove513SP != null
													? cove513SP.getCoveragePremium().getNetPremiumChange()
													: null);
										}else {
											oAP4Section513SPPremium = (cove513SP != null
													? cove513SP.getCoveragePremium().getTransactionPremium()
													: null);
										}
	
										if(isNegativeQty) {
											if (oAP4Section513SPPremium != null) {
												oAP4Section513SPPremium = oAP4Section513SPPremium * -1;
											}
										}
										// OAP4 Section 514 SP Limit
										// Integer oAP4Section514SPLimit = getCoverageLimit1(pt, "SP514", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section514SPLimit = null;
										if (!isLht) {
											oAP4Section514SPLimit = (cove514SP != null
													? cove514SP.getLimit1() : null);
										}
	
										// OAP4 Section 514 SP Deductible
										// Integer oAP4Section514SPDeductible = getCoverageDeductible(pt, "SP514",
										// SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
										Integer oAP4Section514SPDeductible = null;
										if (!isLht) {
											oAP4Section514SPDeductible = (cove514SP != null
													? cove514SP.getDeductible1() : null);
										}
	
										// OAP4 Section 514 SP Premium
										// Double oAP4Section514SPPremium = getCoveragePremium(pt, "SP514", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);	
										Double oAP4Section514SPPremium = null;
										//if ((ischange && cove514SP != null && !cove514SP.getBusinessStatus().contentEquals("NOCHANGE"))) {
										if (ischange) {
											oAP4Section514SPPremium = (cove514SP != null
													? cove514SP.getCoveragePremium().getNetPremiumChange()
													: null);
										}else {
											oAP4Section514SPPremium = (cove514SP != null
													? cove514SP.getCoveragePremium().getTransactionPremium()
													: null);
										}
										
										if(isNegativeQty) {
											if (oAP4Section514SPPremium != null) {
												oAP4Section514SPPremium = oAP4Section514SPPremium * -1;
											}
										}
	
										// OAP4 Section 6 1 CL Limit
										Integer oAP4Section61CLLimit = getCoverageLimit1(pt, "CL61", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OAP4 Section 6 1 CL Deductible
										Integer oAP4Section61CLDeductible = getCoverageDeductible(pt, "CL61", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OAP4 Section 6 1 CL Prem
										Double oAP4Section61CLPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "CL61", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OAP4 Section 6 4 SP Limit
										Integer oAP4Section64SPLimit = null;
										if (!isLht) {
											oAP4Section64SPLimit = (coveSP64 != null
													? coveSP64.getLimit1() : null);
										}
	
										// OAP4 Section 6 4 SP Deductible
										Integer oAP4Section64SPDeductible = null;
										if (!isLht) {
											oAP4Section64SPDeductible = (coveSP64 != null
													? coveSP64.getDeductible1() : null);
										}
	
										// OAP4 Section 6 4 SP Prem
										Double oAP4Section64SPPrem = null;
										//if ((ischange && coveSP64 != null && !coveSP64.getBusinessStatus().contentEquals("NOCHANGE"))) {
										if (ischange) {
											oAP4Section64SPPrem = (coveSP64 != null
													? coveSP64.getCoveragePremium().getNetPremiumChange()
													: null);
										}else {
											oAP4Section64SPPrem = (coveSP64 != null
													? coveSP64.getCoveragePremium().getTransactionPremium()
													: null);
										}
										if(isNegativeQty) {
											if (oAP4Section64SPPrem != null) {
												oAP4Section64SPPrem = oAP4Section64SPPrem * -1;
											}
										}
	
										// OAP4 Section 6 4 SP Limit
										// Integer oAP4Section64SPLimit = getCoverageLimit1(pt, "SP64", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OAP4 Section 6 4 SP Deductible
										// Integer oAP4Section64SPDeductible = getCoverageDeductible(pt, "SP64", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OAP4 Section 6 4 SP Prem
										// Double oAP4Section64SPPrem = getCoveragePremium(pt, "SP64", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										/////////////////////////////////////////////////////////////////
	
										// OAP4 Section 6 4 SP Maximum Vehicles
										//Integer oAP4Section64SPMaximumVehicles = getNumVehicles(pt, "SP64");
										Integer oAP4Section64SPMaximumVehicles = pl.getNumVehicles();
										
	
										// OEF71 Excluding Owned Automobiles Indicator
										String oEF71ExcludingOwnedAutomobilesIndicator = endorsementExist(pt, "OEF71",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF72 Multiple Alteration Premium
										Double oEF72MultipleAlterationPremium = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF72",SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF73 Excluding Financed Indicator
										String oEF73ExcludingFinancedIndicator = endorsementExist(pt, "OEF73", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF74 Open Lot Theft Premium
										Double oEF74OpenLotTheftPremium = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF74", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										
										// OEF75 Open Lot Theft Prem
										Double oEF75OpenLotTheftPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "OEF75", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF76 Additional Insured Premium
										Double oEF76AdditionalInsuredPremium = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF76", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF77 Customer Auto Prem
										Double oEF77CustomerAutoPrem = divideGarageAmounts(ischange, getCoveragePremium(pt, "OEF77", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty, isAdjustment), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF78A Excluded Driver Endorsement Indicator
										String oEF78AExcludedDriverEndorsementIndicator = endorsementExist(pt, "OEF78A",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF78 Reduction of Coverage for Named Persons Indicator
										String oEF78ReductionCoverageNamedPersonsIndicator = endorsementExist(pt, "OEF78",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF79 Fire Deductible Owned Automobiles Indicator
										String oEF79FireDeductibleOwnedAutomobilesIndicator = endorsementExist(pt, "OEF79",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF80 Specified Owned Automobile Physical Damage Coverage Premium
										Double oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium = divideGarageAmounts(ischange, getEndorsementPremium(
												pt, "OEF80", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										
										// OEF81 Garage Family Protection Limit
										Integer oEF81GarageFamilyProtectionLimit = getEndormentLimit1(pt, "OEF81", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OEF81 Garage Family Protection Prem
										Double oEF81GarageFamilyProtectionPrem = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF81", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
										
										// OEF82 Liability for Damaged Non-Owned Limit
										Integer oEF82LiabilityDamagedNonOwnedLimit = getEndormentLimit1(pt, "OEF82", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OEF82 Liability for Damaged Non-Owned Deductible
										Integer oEF82LiabilityDamagedNonOwnedDeductible = getEndorsementDeductible(pt,
												"OEF82", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, isLht, isNegativeQty);
	
										// OEF82 Liability for Damaged Non-Owned Premium
										Double oEF82LiabilityDamagedNonOwnedPremium = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF82",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF83 Automobile Transportation Premium
										Double oEF83AutomobileTransportationPremium = divideGarageAmounts(ischange, getEndorsementPremium(pt, "OEF83",
												SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, ischange, isNegativeQty), countAllLocations, countAllChangedLocations, garLocationAutoIncrement, df);
	
										// OEF85 Final Premium Computation Indicator
										String oEF85FinalPremiumComputationIndicator = endorsementExist(pt, "OEF85", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// OEF86 Customers Automobiles Fire Deductible Indicator
										String oEF86CustomersAutomobilesFireDeductibleIndicator = endorsementExist(pt,
												"OEF86", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// Garage Service Plate Coverage Indicator
										String garageServicePlateCoverageIndicator = endorsementExist(pt, "GSPC", SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	
										// Garage Service Plate Count
										Integer garageServicePlateCount = countGarageServicePlate(pt, "GSPC");
	
										/*
										 * Double garageTotalPrem =oAP4Section1BIPrem + oAP4Section1PDPrem +
										 * oAP4Section2ABPrem + oAP4Section3UAPrem + oAP4Section4DCPDPrem +
										 * oAP4Section61CLPrem + oAP4Section64SPPrem + oEF72MultipleAlterationPremium +
										 * oEF74OpenLotTheftPremium + oEF75OpenLotTheftPrem +
										 * oEF76AdditionalInsuredPremium + oEF77CustomerAutoPrem +
										 * oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium +
										 * oEF81GarageFamilyProtectionPrem + oEF82LiabilityDamagedNonOwnedPremium +
										 * oEF83AutomobileTransportationPremium;
										 */
										Double oAP4TotalPrem = oAP4Section1BIPrem + oAP4Section1PDPrem + oAP4Section2ABPrem
												+ oAP4Section3UAPrem + oAP4Section4DCPDPrem + oAP4Section61CLPrem
												+ (oAP4Section64SPPrem != null ? oAP4Section64SPPrem : 0)
												+ (oAP4Section511CLPremium != null ? oAP4Section511CLPremium : 0)
												+ (oAP4Section512CMPremium != null ? oAP4Section512CMPremium : 0)
												+ (oAP4Section513SPPremium != null ? oAP4Section513SPPremium : 0)
												+ (oAP4Section514SPPremium != null ? oAP4Section514SPPremium : 0);
	
										Double garageTotalPrem = oAP4TotalPrem
												/* +oEF72MultipleAlterationPremium */ + oEF74OpenLotTheftPremium
												+ oEF75OpenLotTheftPrem + oEF76AdditionalInsuredPremium
												+ oEF77CustomerAutoPrem
												+ oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium
												+ oEF81GarageFamilyProtectionPrem + oEF82LiabilityDamagedNonOwnedPremium
												+ oEF83AutomobileTransportationPremium;
	
										setBorderauxGarage(
												// General Data 1
												pt.getPolicyTerm().getTermNumber(), 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? pt.getPolicyVersion().getPolicyVersionNum() : pt.getPolicyVersion().getPolicyVersionNum()+1, 
												pt.getPolicyTerm().getInsurancePolicy().getBasePolicyNum(),
												pt.getPolicyTerm().getTermEffDate().toLocalDate(),
												pt.getPolicyTerm().getTermExpDate().toLocalDate(),
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? pt.getPolicyVersion().getVersionDate() :
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? pt.getPolicyVersion().getVersionDate() :
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? pt.getPolicyVersion().getVersionDate() 
													: pt.getPolicyTerm().getTermEffDate().toLocalDate(),
												(pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW"	
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyGAR.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && subPolicyGAR.getBusinessStatus().equals("NEW") ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && locationChangeStatus(pl).equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && locationChangeStatus(pl).equals("NEW") ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && risk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && risk.getBusinessStatus().equals("NEW") ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !risk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !risk.getBusinessStatus().equals("NEW") ? "END" 
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyGAR.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && subPolicyGAR.getBusinessStatus().equals("NEW") ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && risk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && risk.getBusinessStatus().equals("NEW") ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && !risk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !risk.getBusinessStatus().equals("NEW") ? "END" 
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? "CAN"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_ONSET) ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) ? "NEW"
														: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? "END"
														: pt.getPolicyVersion().getPolicyTxnType()),
												PolicyFleetIndicator,
												pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
												"N", 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" :
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" :  
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "REN" :
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "NEW" :
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW":
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN":
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
												pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
												subPolicyGAR.getBusinessStatus(), 
												isLht2Millions ? polSec3Limit : (sAutoPackage != null ? sAutoPackage.getLiabilityLimit() : null),
						 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
						 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getRin() : 
						 						null,
						 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
						 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName() : 
						 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer().getFullName(),	
												pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress()
														.getAddressLine1(),
												pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getCity(),
												pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
												pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getPostalZip()
														.trim().replace(" ", ""),
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLegalName(),
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
														.stream().findFirst().get().getAddressLine1(),
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
														.stream().findFirst().get().getCity(),
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
														.stream().findFirst().get().getPostalZip().trim().replace(" ", ""),
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId(),

												// Garage
												(subPolicyGAR != null) ? subPolicyGAR.getBusinessDescription() : null,
												(pl != null) ? pl.getLocationAddress().getAddressLine1() : null,
												(pl != null) ? pl.getLocationAddress().getCity() : null,
												(pl != null) ? pl.getLocationAddress().getProvState() : null,
												(pl != null)
														? pl.getLocationAddress().getPostalZip().trim().replace(" ", "")
														: null,
												(pl != null) ? (pl.getIsVehiclesInBuilding() != null
														? (pl.getIsVehiclesInBuilding() ? "Y" : "N")
														: "N") : "N",
												(pl != null) ? (pl.getIsVehiclesOnLot() != null
														? (pl.getIsVehiclesOnLot() ? "Y" : "N")
														: "N") : "N",
												(pl.getCustomerMainAddressPK() != null
														? (subPolicyGAR != null) ? subPolicyGAR.getNumFullTimeEmployees()
																: null
														: null),
												(pl.getCustomerMainAddressPK() != null
														? (subPolicyGAR != null) ? subPolicyGAR.getNumPartTimeEmployees()
																: null
														: null),
												oAP4Section1Limit, oAP4Section1BIPrem, oAP4Section1PDPrem,
												oAP4Section2ABPrem, oAP4Section3UAPrem, oAP4Section4DCPDDeductible,
												oAP4Section4DCPDPrem, oAP4Section511CLDeductible, oAP4Section511CLPremium,
												oAP4Section512CMLimit, oAP4Section512CMDeductible, oAP4Section512CMPremium,
												oAP4Section513SPLimit, oAP4Section513SPDeductible, oAP4Section513SPPremium,
												oAP4Section514SPLimit, oAP4Section514SPDeductible, oAP4Section514SPPremium,
												oAP4Section61CLLimit, oAP4Section61CLDeductible, oAP4Section61CLPrem,
												oAP4Section64SPLimit, oAP4Section64SPDeductible,
												oAP4Section64SPMaximumVehicles, oAP4Section64SPPrem,
												oEF71ExcludingOwnedAutomobilesIndicator, oEF72MultipleAlterationPremium,
												oEF73ExcludingFinancedIndicator, oEF74OpenLotTheftPremium,
												oEF75OpenLotTheftPrem, oEF76AdditionalInsuredPremium, oEF77CustomerAutoPrem,
												oEF78AExcludedDriverEndorsementIndicator,
												oEF78ReductionCoverageNamedPersonsIndicator,
												oEF79FireDeductibleOwnedAutomobilesIndicator,
												oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium,
												oEF81GarageFamilyProtectionLimit, oEF81GarageFamilyProtectionPrem,
												oEF82LiabilityDamagedNonOwnedLimit, oEF82LiabilityDamagedNonOwnedDeductible,
												oEF82LiabilityDamagedNonOwnedPremium, oEF83AutomobileTransportationPremium,
												oEF85FinalPremiumComputationIndicator,
												oEF86CustomersAutomobilesFireDeductibleIndicator, oAP4TotalPrem,
												(pl.getCustomerMainAddressPK() != null ? garageServicePlateCoverageIndicator
														: null),
												(pl.getCustomerMainAddressPK() != null ? garageServicePlateCount : null),
	
												// General Data 2
												null, null, SpecialtyAutoConstants.SUBPCY_CODE_GARAGE.concat(garAutoIncrement.toString()), productCode != null & productCode.equals(ConfigConstants.PRODUCTCODE_LHT) ? "LO" : "TO",
												
												pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
												.getProducerId() != null ? (
												((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
														.getProducerId()).substring(0, 3))
																.concat(((pt.getPolicyTerm().getInsurancePolicy()
																		.getPolicyProducer().getProducerId()).substring(
																				pt.getPolicyTerm().getInsurancePolicy()
																						.getPolicyProducer().getProducerId()
																						.length() - 2,
																				pt.getPolicyTerm().getInsurancePolicy()
																						.getPolicyProducer().getProducerId()
																						.length())))) : ""
																
																
																,
												null, garageTotalPrem,
												(garageTotalPrem != null)
														? garageTotalPrem
																* (pt.getPolicyVersion().getProducerCommissionRate() / 100)
														: null);
									}
								}
							}
						}
					}

				}
				
				
				
				
				// O T H E R
				
				//Check if Subpolicy was changed
				boolean isOtherPolicychange = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)) {
					isOtherPolicychange = true;
				}
				
				
				//Check if Other subpolicy exist
				boolean existOtherSubpolicy=existsSubPolicyOther(pt, isOtherPolicychange);

				
				//check if the other coverages were changed to know if we will going to print thuis subpolicy in policyChange transaction
				boolean isOtherEndorAndCovchanged  = false;
				if (isSubPolicyOtherEndorAndCovChanged(pt)) {
					isOtherEndorAndCovchanged = true;
				}
				
				//Check if Subpolicy is a reversal
				boolean isOtherreversal = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) {
					isOtherreversal = true;
					isOtherPolicychange = true;//Set "ischange" to true because Sangeetha asked me to add the exactly behavior for PolicyChange also in PolicyReversal status.
				}
				
				//Check if Subpolicy is a renewal
				boolean isOtherrenewal = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL)) {
					isOtherrenewal = true;
				}
				
				//Check if Subpolicy is a cancellation
				boolean isOthercancellation = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION)) {
					isOthercancellation = true;
				}
				
				//Check if Subpolicy is a Reinstatement
				boolean isOtherReinstatement = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT)) {
					isOtherReinstatement = true;
				}
				
				//Check if Subpolicy is a reissue and if the quantity is going to be negative in the new bussiness transaction
				boolean isOtherReissue = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)) {
					isOtherReissue = true;
				}
				boolean isOtherNegativeQty = false;//This is going to be always false because the reissue offset transaction is going to have always negative quantities, so this variable is never going to be used
				//if (isReissueMainTransaction && pt.getPolicyVersion().getPolicyTxnType().equals("NEWBUSINESS")) {
					//isOtherNegativeQty = true;
				//}
				
				//Check if Subpolicy is a extension
				boolean isOtherExtension = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION)) {
					isOtherExtension = true;
				}
				
				//Check if Subpolicy is an adjustment
				boolean isOtherAdjustment = false;
				if (pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT)) {
					isOtherAdjustment = true;
				}
				
				
				
				// QE4037R Property Extension Indicator
				String qE4037RPropertyExtensionIndicator = null;
				
				// QE4037R Contents Limit
				Integer qE4037RContentsLimit = null;;
				
				// QE4037R Contents Deductible
				Integer qE4037RContentsDeductible = null;
				
				// QE4037R Contents Premium
				Double qE4037RContentsPrem = null;
				
				if (isSubPolicyCP) {
					// QE4037R Property Extension Indicator
					qE4037RPropertyExtensionIndicator = coverageExist(pt, "CONT", "CP");
					
					// QE4037R Contents Limit
					qE4037RContentsLimit = getCoverageLimit1(pt, "CONT", "CP", isLht, isOtherNegativeQty);
					
					// QE4037R Contents Deductible
					qE4037RContentsDeductible = getCoverageDeductible(pt, "CONT", "CP", isLht, isOtherNegativeQty);
					
					// QE4037R Contents Premium
					qE4037RContentsPrem = getCoveragePremium(pt, "CONT", "CP", isOtherPolicychange, isOtherNegativeQty, isAdjustment);
				}

				

				// EON101 Blanket Lessors Indicator
				String eON101BlanketLessorsIndicator = endorsementExist(pt, "EON101", null);

				// EON102 Additional Insured Individual Indicator
				String eON102AdditionalInsuredIndividualIndicator = endorsementExist(pt, "EON102", null);

				// EON103 Additional Insured OO Indicator
				String eON103AdditionalInsuredOOIndicator = endorsementExist(pt, "EON103", null);

				// EON109 Insurable Interest Deleted Indicator
				String eON109InsurableInterestDeletedIndicator = endorsementExist(pt, "EON109", null);

				// EON110 Per Occurance Deductible Indicator
				String eON110PerOccuranceDeductibleIndicator = endorsementExist(pt, "EON110", null);

				// EON110 Per Occurance Deductible
				Integer eON110PerOccuranceDeductible = getEndorsementDeductible(pt, "EON110", null, isLht, isOtherNegativeQty);

				// EON111 Profit Sharing Indicator
				String eON111ProfitSharingIndicator = endorsementExist(pt, "EON111", null);

				// EON112 Profit Sharing Adjustment Prem
				Double eON112ProfitSharingAdjustmentPrem = getEndorsementPremium(pt, "EON112", null, isOtherPolicychange, isOtherNegativeQty);

				// EON114 Named Insured Indicator
				String eON114NamedInsuredIndicator = endorsementExist(pt, "EON114", null);

				// EON115 OPCF21A Receipts Basis Adjustment Prem
				Double eON115OPCF21AReceiptsBasisAdjustmentPrem = getEndorsementPremium(pt, "EON115", null, isOtherPolicychange, isOtherNegativeQty);

				// EON116 OPCF21B 5050 Basis Adjustment Prem
				Double eON116OPCF21B5050BasisAdjustmentPrem = getEndorsementPremium(pt, "EON116", null, isOtherPolicychange, isOtherNegativeQty);

				// EON117 OPCF21B Pro Rata Basis Adjustment Prem
				Double eON117OPCF21BProRataBasisAdjustmentPrem = getEndorsementPremium(pt, "EON117", null, isOtherPolicychange, isOtherNegativeQty);

				// EON119 Registered Owner Indicator
				String eON119RegisteredOwnerIndicator = endorsementExist(pt, "EON119", null);

				// EON121 Policy Cancellation Prem
				Double eON121PolicyCancellationPrem = getEndorsementPremium(pt, "EON121", null, isOtherPolicychange, isOtherNegativeQty);

				// EON122 Policy Cancellation Adjustement Prem
				Double eON122PolicyCancellationAdjustementPrem = getEndorsementPremium(pt, "EON122", null, isOtherPolicychange, isOtherNegativeQty);

				// Indemnity Agreement Indicator
				String indemnityAgreementIndicator = endorsementExist(pt, "IA", null);

				Double otherTotalPrem = eON115OPCF21AReceiptsBasisAdjustmentPrem
						+ eON116OPCF21B5050BasisAdjustmentPrem + eON121PolicyCancellationPrem
						+ eON122PolicyCancellationAdjustementPrem + eON112ProfitSharingAdjustmentPrem
						+ eON117OPCF21BProRataBasisAdjustmentPrem;
				
				

				
				
				if ((isOtherReissue || !isOtherAdjustment || isOtherReinstatement || isOthercancellation || isOtherreversal || (isOtherPolicychange && isOtherEndorAndCovchanged) || isOtherrenewal || isOtherExtension || pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS)) && existOtherSubpolicy) {



					setBorderauxOther(
						// General Data 1
						pt.getPolicyTerm().getTermNumber(), 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? pt.getPolicyVersion().getPolicyVersionNum() : pt.getPolicyVersion().getPolicyVersionNum()+1, 
						pt.getPolicyTerm().getInsurancePolicy().getBasePolicyNum(),
						pt.getPolicyTerm().getTermEffDate().toLocalDate(),
						pt.getPolicyTerm().getTermExpDate().toLocalDate(),
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? pt.getPolicyVersion().getVersionDate() :
							pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? pt.getPolicyVersion().getVersionDate() :
							pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? pt.getPolicyVersion().getVersionDate() 
							: pt.getPolicyTerm().getTermEffDate().toLocalDate(),
						(pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? "END" 		
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "REV" 
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "NEW"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? "CAN"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? "NEW"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? "CAN"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_ONSET) ? "NEW"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) ? "NEW"
								: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? "END"
								: pt.getPolicyVersion().getPolicyTxnType()),
						PolicyFleetIndicator,
						pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(), "N",
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" :  
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "REN" :
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "NEW" :
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW":
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW":
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN":
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
						pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
						pt.getPolicyVersion().getPolicyTxnType(), 
						isLht2Millions ? polSec3Limit : (sAutoPackage != null ? sAutoPackage.getLiabilityLimit() : null),
 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getRin() : 
						null,
						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName() : 
						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer().getFullName(),								
						pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getAddressLine1(),
						pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getCity(),
						pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
						pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getPostalZip().trim()
								.replace(" ", ""),
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLegalName(),
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
								.getAddressLine1(),
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
								.getCity(),
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations().stream().findFirst().get()
								.getPostalZip().trim().replace(" ", ""),
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId(),

						// Other
						qE4037RContentsLimit, qE4037RContentsDeductible, qE4037RContentsPrem, qE4037RPropertyExtensionIndicator, eON101BlanketLessorsIndicator,
						eON102AdditionalInsuredIndividualIndicator, eON103AdditionalInsuredOOIndicator,
						eON109InsurableInterestDeletedIndicator, eON110PerOccuranceDeductibleIndicator,
						eON110PerOccuranceDeductible, eON111ProfitSharingIndicator,
						eON112ProfitSharingAdjustmentPrem, eON114NamedInsuredIndicator,
						eON115OPCF21AReceiptsBasisAdjustmentPrem, eON116OPCF21B5050BasisAdjustmentPrem,
						eON117OPCF21BProRataBasisAdjustmentPrem, eON119RegisteredOwnerIndicator,
						eON121PolicyCancellationPrem, eON122PolicyCancellationAdjustementPrem,
						indemnityAgreementIndicator,

						// General Data 2
						null, null, "OTHER", productCode != null & productCode.equals(ConfigConstants.PRODUCTCODE_LHT) ? "LO" : "TO",
						pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
						.getProducerId() != null ? (
						((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId())
								.substring(0, 3))
										.concat(((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
												.getProducerId())
														.substring(
																pt.getPolicyTerm().getInsurancePolicy()
																		.getPolicyProducer().getProducerId()
																		.length() - 2,
																pt.getPolicyTerm().getInsurancePolicy()
																		.getPolicyProducer().getProducerId()
																		.length())))):"",
						null, otherTotalPrem,
						(otherTotalPrem != null)
								? otherTotalPrem * (pt.getPolicyVersion().getProducerCommissionRate() / 100)
								: null);
				}
				

				// A U T O

				if (subPolicy != null) {


						SubPolicy<Risk> subPolicyRisk = pt
								.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

						if (subPolicyRisk != null && subPolicyRisk.getSubPolicyRisks() != null) {

							Set<Risk> risks = subPolicyRisk.getSubPolicyRisks();

							// Sorting Vehicule
							List<SpecialtyVehicleRisk> vehRiskList = new ArrayList<>();
							for (Risk risk : risks) {
								SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
								vehRiskList.add(vehRisk);
							}

							List<SpecialtyVehicleRisk> vehRiskListSorted = vehRiskList.stream()
									.sorted(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence))
									.collect(Collectors.toList());

							// if (!risks.isEmpty()) {
							Integer autoincrement = 1;

							for (SpecialtyVehicleRisk vehRisk : vehRiskListSorted) {
								
								
								//Vehicle Adjustments
								List<SpecialtyVehicleRiskAdjustment> vehRiskAdjustmentsList = new ArrayList<>();
								Set<SpecialtyVehicleRiskAdjustment> vehRiskAdjustments = vehRisk.getSvrAdjustments();
								
								if (vehRiskAdjustments == null) {
									SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
									vehRiskAdjustmentsList.add(emptyAdjustment);
								} else if (vehRiskAdjustments.size() == 0) {
									SpecialtyVehicleRiskAdjustment emptyAdjustment = new SpecialtyVehicleRiskAdjustment();
									vehRiskAdjustmentsList.add(emptyAdjustment);
								} else {
									vehRiskAdjustmentsList = vehRiskAdjustments.stream().collect(Collectors.toList());
								}
								
								for (SpecialtyVehicleRiskAdjustment vehRiskAdjustment : vehRiskAdjustmentsList) {
									

								
									boolean ischange = false;
									//Check if Subpolicy was changed
									if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)
											&& !vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
										ischange = true;
									}
									
									//Check if the endorsments and coverages  from the Policy have been changed when is not a reissue transaction
									if (isAutoEndorAndCovChanged(pt, vehRisk, subPolicy)) {
										ischange = true;
									}
									
									//Check if Subpolicy is a reversal
									boolean isreversal = false;
									if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)
											&& !vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE))) {
										isreversal = true;
										ischange = true;//Set "ischange" to true because Sangeetha asked me to add the exactly behavior for PolicyChange also in PolicyReversal status.
									}
									
	
									
									if (isReissue || isAdjustment || isReinstatement || (iscancellation && !vehRisk.getSystemStatus().equals(Constants.BUSINESS_STATUS_INACTIVE)) || isreversal || ischange || isrenewal || isextension || pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS)) {
	
										
										boolean isopcf27b=(getCodeValue(pt, vehRisk.getVehicleDescription(), ConfigConstants.LOOKUPTABLE_VehDesc)).equals("OPCF 27B");
										
										Double opcf2prem = null;
										String opcf5Exists = null;
										Double opcf5cPremium = null;
										Double opcf5dPremium = null;
										String opcf5gExists = null;;
										Integer opcf8Deductible = null;;
										String opcf9Exists = null;;
										String opcf13cExists = null;;
										Integer opcf19Limit1 = null;
										Double opcf20Premium = null;;
										String opcf21bExists = null;;
										String opcf23aExists = null;;
										String opcf25aExists = null;;
										Double opcf25aPremium = null;;
										String eon145ExistsANDopcf25aExists=null;;
										Integer oPCF27Limit= null;;
										Integer oPCF27Deductible= null;;
										Double opcf27Premium = null;;
										Integer opcf27bDeductible = null;;
										Integer opcf27bLimit1 = null;;
										Double opcf27bPremium = null;;
										String opcf28Exists = null;;
										String opcf28aExists = null;;
										String opcf30Exists = null;;
										String opcf31Exists = null;;
										Integer opcf38Limit1 = null;;
										Double opcf38Premium = null;;
										Integer opcf40Deductible = null;
										Double opcf43Premium = null;;
										Double opcf43aPremium = null;
										Double opcf44Premium = null;;
										String opcf47Exists = null;;
										Double opcf48Premium = null;
										String dealerPlateCoverageIndicator = null;;
										Integer eON20ATravelExpensesDeductible = null;;
										Double eON20ATravelExpensesPrem = null;;
										Integer eon20gDeductible = null;
										Double eon20gPremium = null;;
										Integer eON104BuydownDeductible = null;;
										Double eon104Premium = null;;
										Double conditionalReleaseBondPrem = null;;
										Double autoEndtTotalPrem = null; 
										Integer AutoSection7DAPDeductible = null;
										Double AutoSection7DAPTotalPrem = null;
										
										if (isopcf27b) {
											//OPCF2 Prem
											opcf2prem =(double) 0;
						
											// OPCF5 exists
											opcf5Exists = "N";
											
											// OPCF5C Prem
											opcf5cPremium = (double) 0;
						
											// OPCF5D Endorsment Premium
											opcf5dPremium = null;
						
											// OPCF5G exists Endorsment
											opcf5gExists = "N";
											
											// OPCF8 Endorsment Deductible
											opcf8Deductible = 0;
						
											// OPCF9 exists Endorsment
											opcf9Exists = "N";
						
											// OPCF13C exists Endorsment
											opcf13cExists = "N";
						
											// OPCF19 Endorsment Limit1
											opcf19Limit1 = null;
						
											// OPCF20 Prem
											opcf20Premium = (double) 0;
						
											// OPCF21B exists Endorsment
											opcf21bExists = "N";
						
											// OPCF23A exists Endorsment
											opcf23aExists = "N";
						
											// OPCF25A exists Endorsment
											opcf25aExists = "N";
						
											// OPCF25A Prem
											opcf25aPremium = (double) 0;
						
											// OPCF25A and EON145 exists Endorsment
											eon145ExistsANDopcf25aExists="N";
											
											//OPCF27 Limit
											oPCF27Limit= 0;
											
											//OPCF27 Deductible
											oPCF27Deductible= 0;
											
											// OPCF27 Prem
											opcf27Premium = (double) 0;
						
											// OPCF27B Endorsment Deductible
											opcf27bDeductible = getEndorsementDeductibleAuto("OPCF27B", vehRisk, subPolicy, isLht, isNegativeQty);
						
											// OPCF27B Endorsment Limit1
											opcf27bLimit1 = getEndormentLimit1Auto("OPCF27B", vehRisk, subPolicy, isLht, isNegativeQty);
						
											// OPCF27B Prem
											opcf27bPremium = getEndorsementPremiumAuto("OPCF27B", vehRisk, ischange, isNegativeQty);
						
											// OPCF28 exists Endorsment
											opcf28Exists = "N";
						
											// OPCF28A exists Endorsment
											opcf28aExists = "N";
						
											// OPCF30 exists Endorsment
											opcf30Exists = "N";
						
											// OPCF31 exists Endorsment
											opcf31Exists = "N";
						
											// OPCF38 Endorsment Limit1
											opcf38Limit1 = 0;
						
											// OPCF38 Prem
											opcf38Premium = (double) 0;
						
											// OPCF40 Endorsment Deductible
											opcf40Deductible = null;
						
											// OPCF43 Prem
											opcf43Premium = (double) 0;
						
											// OPCF43A Prem
											opcf43aPremium = null;
						
											// OPCF44R Prem
											opcf44Premium = (double) 0;
											
											// OPCF47 exists Endorsment
											opcf47Exists = "N";
						
											// OPCF48 Prem
											opcf48Premium = null;
											
											// Dealer Plate Coverage Indicator
											dealerPlateCoverageIndicator = "N";
											
											// EON20A Travel Expenses Deductible
											eON20ATravelExpensesDeductible = 0;
											
											// EON20A Travel Expenses Prem
											eON20ATravelExpensesPrem = (double)0;
						
											// EON20G Loss of Income Deductible
											eon20gDeductible = null;
						
											// EON20G Prem
											eon20gPremium = (double) 0;
											
											// EON104 Buydown Deductible
											eON104BuydownDeductible = 0;
						
											// EON104 Buydown Deductible Prem
											eon104Premium = (double) 0;
											
											// Conditional Release Bond Prem
											conditionalReleaseBondPrem = (double) 0;
						
											// Total all endorsement premium
											autoEndtTotalPrem = opcf27bPremium ; 
											
											// Auto Section 7D AP Deductible
											AutoSection7DAPDeductible = 0;
			
											// Auto Section 7D AP Total Prem
											AutoSection7DAPTotalPrem = (double) 0;
										}else {
											// SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
											
											// OPCF2 Prem
											opcf2prem = getEndorsementPremiumAuto("OPCF2", vehRisk, ischange, isNegativeQty);
			
											// OPCF5 exists
											opcf5Exists = endorsementExistAuto("OPCF5", vehRisk, subPolicy);
			
											// OPCF5C Prem
											opcf5cPremium = getEndorsementPremiumAuto("OPCF5C", vehRisk, ischange, isNegativeQty);
			
											// OPCF5D Endorsment Premium
											// Double opcf5dPremium = getEndorsementPremium(pt, "OPCF5D", "AUTO");
											opcf5dPremium = null;
			
											// OPCF5G exists Endorsment
											// String opcf5gExists = endorsementExist(pt, "OPCF5G", "AUTO");
											opcf5gExists = "N";
			
											// OPCF8 Endorsment Deductible
											opcf8Deductible = getEndorsementDeductibleAuto("OPCF8", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF9 exists Endorsment
											opcf9Exists = endorsementExistAuto("OPCF9", vehRisk, subPolicy);
			
											// OPCF13C exists Endorsment
											opcf13cExists = endorsementExistAuto("OPCF13C", vehRisk, subPolicy);
			
											// OPCF19 Endorsment Limit1
											// Integer opcf19Limit1 = getEndormentLimit1(pt, "OPCF19", "AUTO");
											opcf19Limit1 = null;
			
											// OPCF20 Prem
											opcf20Premium = getEndorsementPremiumAuto("OPCF20", vehRisk, ischange, isNegativeQty);
			
											// OPCF21B exists Endorsment
											opcf21bExists = endorsementExistAuto("OPCF21B", vehRisk, subPolicy);
			
											// OPCF23A exists Endorsment
											opcf23aExists = endorsementExistAuto("OPCF23A", vehRisk, subPolicy);
			
											// OPCF25A exists Endorsment
											opcf25aExists = endorsementExistAuto("OPCF25A", vehRisk, subPolicy);
			
											// OPCF25A Prem
											opcf25aPremium = getEndorsementPremiumAuto("OPCF25A", vehRisk, ischange, isNegativeQty);
			
											// OPCF25A and EON145 exists Endorsment
											// String eon145Exists = endorsementExist(pt, "OPCF25A", "AUTO");
											// String eon145ExistsANDopcf25aExists = "N";
											// if (eon145Exists == "Y" && opcf25aExists == "Y") {
											// eon145ExistsANDopcf25aExists = "Y";
											// }
											eon145ExistsANDopcf25aExists = "N";
			
											// OPCF27 Limit
											oPCF27Limit = getEndormentLimit1Auto("OPCF27", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF27 Deductible
											oPCF27Deductible = getEndorsementDeductibleAuto("OPCF27", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF27 Prem
											opcf27Premium = getEndorsementPremiumAuto("OPCF27", vehRisk, ischange, isNegativeQty);
			
											// OPCF27B Endorsment Deductible
											opcf27bDeductible = getEndorsementDeductibleAuto("OPCF27B", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF27B Endorsment Limit1
											opcf27bLimit1 = getEndormentLimit1Auto("OPCF27B", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF27B Prem
											opcf27bPremium = getEndorsementPremiumAuto("OPCF27B", vehRisk, ischange, isNegativeQty);
			
											// OPCF28 exists Endorsment
											opcf28Exists = endorsementExistAuto("OPCF28", vehRisk, subPolicy);
			
											// OPCF28A exists Endorsment
											opcf28aExists = endorsementExistAuto("OPCF28A", vehRisk, subPolicy);
			
											// OPCF30 exists Endorsment
											opcf30Exists = endorsementExistAuto("OPCF30", vehRisk, subPolicy);
			
											// OPCF31 exists Endorsment
											opcf31Exists = endorsementExistAuto("OPCF31", vehRisk, subPolicy);
			
											// OPCF38 Endorsment Limit1
											opcf38Limit1 = getEndormentLimit1Auto("OPCF38", vehRisk, subPolicy, isLht, isNegativeQty);
			
											// OPCF38 Prem
											opcf38Premium = getEndorsementPremiumAuto("OPCF38", vehRisk, ischange, isNegativeQty);
			
											// OPCF40 Endorsment Deductible
											// Integer opcf40Deductible = getEndorsementDeductible(pt, "OPCF40", "AUTO");
											opcf40Deductible = null;
			
											// OPCF43 Prem
											opcf43Premium = getEndorsementPremiumAuto("OPCF43", vehRisk, ischange, isNegativeQty);
			
											// OPCF43A Prem
											if (PolicyFleetIndicator.equals("Non-Fleet")) {
												opcf43aPremium = getEndorsementPremiumAuto("OPCF43A", vehRisk, ischange, isNegativeQty);
											} else {
												opcf43aPremium = (double) 0;
											}
	
			
											// OPCF44R Prem
											opcf44Premium = getEndorsementPremiumAuto("OPCF44R", vehRisk, ischange, isNegativeQty);
			
											// OPCF47 exists Endorsment
											opcf47Exists = endorsementExistAuto("OPCF47", vehRisk, subPolicy);
			
											// OPCF48 Prem
											// Double opcf48Premium = getEndorsementPremiumAuto("OPCF48", vehRisk);
											//Double opcf48Premium = null;
											opcf48Premium = getEndorsementPremiumAuto("OPCF48", vehRisk, ischange, isNegativeQty);
			
											// Dealer Plate Coverage Indicator
											dealerPlateCoverageIndicator = endorsementExistAuto("GDPC", vehRisk, subPolicy);
			
											// EON20A Travel Expenses Deductible
											eON20ATravelExpensesDeductible = getEndorsementDeductibleAuto("EON20A", vehRisk,
													subPolicy, isLht, isNegativeQty);
			
											// EON20A Travel Expenses Prem
											eON20ATravelExpensesPrem = getEndorsementPremiumAuto("EON20A", vehRisk, ischange, isNegativeQty);
			
											// EON20G Loss of Income Deductible
											// Integer eon20gDeductible = getEndorsementDeductible(pt, "EON20G", "AUTO");
											eon20gDeductible = null;
			
											// EON20G Prem
											eon20gPremium = getEndorsementPremiumAuto("EON20G", vehRisk, ischange, isNegativeQty);
			
											// EON104 Buydown Deductible
											eON104BuydownDeductible = getEndorsementDeductibleAuto("EON104", vehRisk,
													subPolicy, isLht, isNegativeQty);
			
											// EON104 Buydown Deductible Prem
											eon104Premium = getEndorsementPremiumAuto("EON104", vehRisk, ischange, isNegativeQty);
			
											// Conditional Release Bond Prem
											conditionalReleaseBondPrem = getEndorsementPremiumAuto("CONRB", vehRisk, ischange, isNegativeQty);
			
											// Total all endorsement premium
											autoEndtTotalPrem = opcf2prem + opcf5cPremium + opcf20Premium + opcf25aPremium
													+ conditionalReleaseBondPrem + opcf27Premium + opcf27bPremium + opcf38Premium
													+ opcf43Premium + opcf44Premium  + opcf48Premium  + eon20gPremium
													+ eon104Premium + eON20ATravelExpensesPrem + opcf43aPremium;
											
											// Auto Section 7D AP Deductible
											AutoSection7DAPDeductible = getCoverageDeductibleAuto("AP", vehRisk, isLht, isNegativeQty);
			
											// Auto Section 7D AP Total Prem
											AutoSection7DAPTotalPrem = getCoveragePremiumAuto("AP", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}
										
	
		
										// Auto Section 3 BI Total Prem
										// Double autoSection3BITotalPrem = getCoveragePremium(pt, "BI", "AUTO");
										Double autoSection3BITotalPrem = null;
										if (isLht2Millions) {
											autoSection3BITotalPrem = getCoveragePremiumAutoLHT("BI", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}else {
											autoSection3BITotalPrem = getCoveragePremiumAuto("BI", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}
										
										
		
										// Auto Section 3 PD Total Prem
										Double autoSection3PDTotalPrem = null;
										if (isLht2Millions) {
											autoSection3PDTotalPrem = getCoveragePremiumAutoLHT("PD", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}else {
											autoSection3PDTotalPrem = getCoveragePremiumAuto("PD", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}
		
										// Auto Section 4 AB Total Prem
										Double autoSection4ABTotalPrem = null;
										if (isLht2Millions) {
											autoSection4ABTotalPrem = getCoveragePremiumAutoLHT("AB", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}else {
											autoSection4ABTotalPrem = getCoveragePremiumAuto("AB", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										}
		
										// Auto Section 5 UA Total Prem
										Double autoSection5UATotalPrem = getCoveragePremiumAuto("UA", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
		
										// Auto Section 6 DCPD Deductible
										Integer autoSection6DCPDDeductible = getCoverageDeductibleAuto("DCPD", vehRisk, isLht, isNegativeQty);
		
										// Auto Section 6 DCPD Total Prem
										Double autoSection6DCPDTotalPrem = getCoveragePremiumAuto("DCPD", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
		
										// Auto Section 7C CL Deductible
										Integer AutoSection7CCLDeductible = getCoverageDeductibleAuto("CL", vehRisk, isLht, isNegativeQty);
		
										// Auto Section 7C CL Total Prem
										Double AutoSection7CCLTotalPrem = getCoveragePremiumAuto("CL", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
		
										// Auto Section 7B CM Deductible
										Integer AutoSection7BCMDeductible = getCoverageDeductibleAuto("CM", vehRisk, isLht, isNegativeQty);
		
										// Auto Section 7B CM Total Prem
										Double AutoSection7BCMTotalPrem = getCoveragePremiumAuto("CM", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
		
										// Auto Section 7A SP Deductible
										Integer AutoSection7ASPDeductible = getCoverageDeductibleAuto("SP", vehRisk, isLht, isNegativeQty);
		
										// Auto Section 7A SP Total Prem
										Double AutoSection7ASPTotalPrem = getCoveragePremiumAuto("SP", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
	
										
										// Auto AB Income Replacement Total Prem
										Double autoABIncomeReplacementTotalPrem = getCoveragePremiumAuto("IR", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Income Replacement Indicator
										String autoABIncomeReplacementIndicator = coverageExistAuto("IR", vehRisk);
										
										// Auto AB Income Replacement Limit
										Integer autoABIncomeReplacementLimit = getAutoCoverageLimit1(vehRisk, "IR", SpecialtyAutoConstants.SUBPCY_CODE_AUTO, isLht, isNegativeQty);
										
										// Auto AB Death and Funeral Total Prem
										Double autoABDeathAndFuneralTotalPrem = getCoveragePremiumAuto("DF", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Death and Funeral Indicator
										String autoABDeathAndFuneralIndicator = coverageExistAuto("DF", vehRisk);
										
										// Auto AB Medical Rehab and ATC Total Prem
										Double autoABMedicalRehabAndATCTotalPrem = getCoveragePremiumAuto("MRC", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Medical Rehab and ATC Indicator
										String autoABMedicalRehabAndATCIndicator = coverageExistAuto("MRC", vehRisk);
										
										// Auto AB Medical Rehab and ATC Limit
										Integer autoABMedicalRehabAndATCLimit = getAutoCoverageLimit1(vehRisk, "MRC", SpecialtyAutoConstants.SUBPCY_CODE_AUTO, isLht, isNegativeQty);
										
										// Auto AB CAT Impairement Total Prem
										Double autoABCATImpairementTotalPrem = getCoveragePremiumAuto("OCI", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB CAT Impairement Indicator
										String autoABCATImpairementIndicator = coverageExistAuto("OCI", vehRisk);
										
										// Auto AB CAT Impairement Limit
										Integer autoABCATImpairementLimit = getAutoCoverageLimit1(vehRisk, "OCI", SpecialtyAutoConstants.SUBPCY_CODE_AUTO, isLht, isNegativeQty);
										
										// Auto AB Caregiver Housekeeper Total Prem
										Double autoABCaregiverHousekeeperTotalPrem = getCoveragePremiumAuto("CHM", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Caregiver Housekeeper Indicator
										String autoABCaregiverHousekeeperIndicator = coverageExistAuto("CHM", vehRisk);
										
										// Auto AB Dependent Care Total Prem
										Double autoABDependentCareTotalPrem = getCoveragePremiumAuto("DC", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Dependent Care Indicator
										String autoABDependentCareIndicator = coverageExistAuto("DC", vehRisk);
										
										// Auto AB Indexation Total Prem
										Double autoABIndexationTotalPrem = getCoveragePremiumAuto("IB", vehRisk, ischange, isNegativeQty, isAdjustment, vehRiskAdjustment, policyProcessing);
										
										// Auto AB Indexation Indicator
										String autoABIndexationIndicator = coverageExistAuto("IB", vehRisk);

		
										// Sum of all coverage premiums
										Double AutoWOEndtTotalPrem = AutoSection7ASPTotalPrem + AutoSection7DAPTotalPrem
												+ AutoSection7BCMTotalPrem + AutoSection7CCLTotalPrem
												+ autoSection6DCPDTotalPrem + autoSection5UATotalPrem + autoSection4ABTotalPrem
												+ autoSection3PDTotalPrem + autoSection3BITotalPrem + autoABIncomeReplacementTotalPrem
												+ autoABDeathAndFuneralTotalPrem + autoABMedicalRehabAndATCTotalPrem + autoABCATImpairementTotalPrem
												+ autoABCaregiverHousekeeperTotalPrem + autoABDependentCareTotalPrem + autoABIndexationTotalPrem;
										
										//Validation to exclude Auto rows with Total Prem as zero in an adjustment transaction
										boolean adjZeroValidation = true;
										if (isAdjustment) {
											if ((AutoWOEndtTotalPrem + autoEndtTotalPrem) == 0) {
												adjZeroValidation = false;
											}
										}
										if (adjZeroValidation) {
											setBorderauxAuto(pt.getPolicyTerm().getTermNumber(), 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? pt.getPolicyVersion().getPolicyVersionNum() : pt.getPolicyVersion().getPolicyVersionNum()+1, 
													pt.getPolicyTerm().getInsurancePolicy().getBasePolicyNum(),
													pt.getPolicyTerm().getTermEffDate().toLocalDate(),
													pt.getPolicyTerm().getTermExpDate().toLocalDate(),
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) ? pt.getPolicyVersion().getVersionDate() :
														pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? pt.getPolicyVersion().getVersionDate() :
														pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? pt.getPolicyVersion().getVersionDate() :
														pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? vehRiskAdjustment.getAdjustmentDate()
														: pt.getPolicyTerm().getTermEffDate().toLocalDate(),
													(pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_NEWBUSINESS) ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && vehRisk.getBusinessStatus().equals("NEW") ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !vehRisk.getBusinessStatus().equals("NEW") ? "END" 
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) ? "CAN"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && vehRisk.getBusinessStatus().equals("NEW") ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) && !vehRisk.getBusinessStatus().equals(Constants.BUSINESS_STATUS_DELETED) && !vehRisk.getBusinessStatus().equals("NEW") ? "END" 
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) ? "CAN"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_OFFSET) ? "CAN"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE)  && pt.getTransactionType().equals(Constants.TRANS_TYPE_ONSET) ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) ? "NEW"
															: pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) ? "END"
															: pt.getPolicyVersion().getPolicyTxnType()),
													PolicyFleetIndicator,
													pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(), "N",
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_CANCELLATION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" :  
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REINSTATEMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_RENEWAL) ? "REN" :
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL) ? "NEW" :
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REISSUE) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW":
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN":
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && pt.getPolicyTerm().getTermNumber().equals(1) ? "NEW" : 
													pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT) && !pt.getPolicyTerm().getTermNumber().equals(1) ? "REN" : 
													vehRisk.getBusinessStatus(), 
													isLht2Millions ? polSec3Limit : (sAutoPackage != null ? sAutoPackage.getLiabilityLimit() : null),
							 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
							 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getRin() : 
							 						null,
							 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM) ?  
							 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName() : 
							 						pt.getPolicyTerm().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer().getFullName(),								
													pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getAddressLine1(),
													pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getCity(),
													pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getProvState(),
													pt.getPolicyTerm().getInsurancePolicy().getInsuredAddress().getPostalZip()
															.trim().replace(" ", ""),
													pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLegalName(),
													pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
															.stream().findFirst().get().getAddressLine1(),
													pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
															.stream().findFirst().get().getCity(),
													pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getLocations()
															.stream().findFirst().get().getPostalZip().trim().replace(" ", ""),
													pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId(),
													// AutoIncrement
													vehRisk.getVehicleSequence()/* autoincrement */, isAdjustment ? getOriginalNumUnitsBeforeAdjustment(vehRisk, ptAdjustment): vehRisk.getNumberOfUnits(),
													sAutoPackage.getStatCode(), sAutoPackage.getTerritoryCode(),
													vehRisk.getVehicleType(),
													getCodeValue(pt, vehRisk.getTrailerType(), ConfigConstants.LOOKUPTABLE_TrailerType),
													(vehRisk.getTrailerType().equals("WT") ? "N" : "Y"),
													getCodeValue(pt, vehRisk.getVehicleDescription(), ConfigConstants.LOOKUPTABLE_VehDesc),
													vehRisk.getVehicleClass(), vehRisk.getDrivingRecord(), vehRisk.getUnitMake(),
													vehRisk.getUnitYear(), vehRisk.getUnitSerialNumberorVin(), /*vehRisk.getUnitLPN()*/vehRisk.getActualCashValue(),
													vehRisk.getUnitExposure(), opcf2prem, null, null, null, null, opcf5Exists,
													opcf5cPremium, opcf5dPremium, opcf5gExists, opcf8Deductible, opcf9Exists,
													opcf13cExists, opcf19Limit1, opcf20Premium, null, null, opcf21bExists,
													opcf23aExists, null, opcf25aExists, opcf25aPremium,
													eon145ExistsANDopcf25aExists, oPCF27Limit, oPCF27Deductible, opcf27Premium,
													opcf27bDeductible, opcf27bLimit1, opcf27bPremium, opcf28Exists, opcf28aExists,
													opcf30Exists, opcf31Exists, opcf38Limit1, opcf38Premium, opcf40Deductible,
													opcf43Premium, opcf43aPremium, opcf44Premium, opcf47Exists, opcf48Premium,
													dealerPlateCoverageIndicator, eON20ATravelExpensesDeductible,
													eON20ATravelExpensesPrem, eon20gDeductible, eon20gPremium,
													eON104BuydownDeductible, eon104Premium, conditionalReleaseBondPrem,
													autoEndtTotalPrem, autoSection3BITotalPrem, autoSection3PDTotalPrem,
													autoSection4ABTotalPrem, autoSection5UATotalPrem, autoSection6DCPDDeductible,
													autoSection6DCPDTotalPrem, AutoSection7CCLDeductible, AutoSection7CCLTotalPrem,
													AutoSection7BCMDeductible, AutoSection7BCMTotalPrem, AutoSection7DAPDeductible,
													AutoSection7DAPTotalPrem, AutoSection7ASPDeductible, AutoSection7ASPTotalPrem,
													autoABIncomeReplacementTotalPrem, autoABIncomeReplacementIndicator,autoABIncomeReplacementLimit,
													autoABDeathAndFuneralTotalPrem,	autoABDeathAndFuneralIndicator,	autoABMedicalRehabAndATCTotalPrem,
													autoABMedicalRehabAndATCIndicator,autoABMedicalRehabAndATCLimit,autoABCATImpairementTotalPrem,
													autoABCATImpairementIndicator,autoABCATImpairementLimit,autoABCaregiverHousekeeperTotalPrem,
													autoABCaregiverHousekeeperIndicator,autoABDependentCareTotalPrem,autoABDependentCareIndicator,
													autoABIndexationTotalPrem,	autoABIndexationIndicator, AutoWOEndtTotalPrem,
			
													// General Data 2
													null, null, 
													subPolicy.getFleetBasis().equals("SCHD") ? vehRisk.getUnitSerialNumberorVin() != null ? !vehRisk.getUnitSerialNumberorVin().equals("") ? vehRisk.getUnitSerialNumberorVin() : "AUTO".concat(vehRisk.getVehicleSequence().toString()) : "AUTO".concat(vehRisk.getVehicleSequence().toString()) : "AUTO".concat(vehRisk.getVehicleSequence().toString()),
															productCode != null & productCode.equals("LHT") ? "LO" : "TO", 
															pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer()
															.getProducerId() != null ? (
													((pt.getPolicyTerm().getInsurancePolicy().getPolicyProducer().getProducerId())
															.substring(0, 3))
																	.concat(((pt.getPolicyTerm().getInsurancePolicy()
																			.getPolicyProducer().getProducerId())
																					.substring(
																							pt.getPolicyTerm().getInsurancePolicy()
																									.getPolicyProducer()
																									.getProducerId().length() - 2,
																							pt.getPolicyTerm().getInsurancePolicy()
																									.getPolicyProducer()
																									.getProducerId().length())))):"",
													null, AutoWOEndtTotalPrem + autoEndtTotalPrem,
													(AutoWOEndtTotalPrem + autoEndtTotalPrem)
															* (pt.getPolicyVersion().getProducerCommissionRate() / 100));
			
											autoincrement++;
										}

									}
								}
							}
						}
				}
			}
		}

	}


	
	
	
	public void setBorderauxAuto(
			// General Data 1
			Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet,
			String insuredCity, String insuredProvince, String insuredPostalCode, String masterBrokerName,
			String masterBrokerStreet, String masterBrokerCity, String masterBrokerPostalCode,
			String masterBrokerNumber,
			// AutoIncrement
			Integer unitNumber, Integer unitCount, String unitStatCode, String unitRatingTerritory,
			String unitVehicleType, String unitTrailerType, String trailerIndicator, String unitDescription,
			String unitVehicleClass, String unitDriverRecord, String unitMake, Integer unitYear, String unitVIN,
			Double unitListPriceNew, String unitLocationExposure, Double oPCF2Prem, String oPCF4AType,
			String oPCF4APrem, String oPCF4BType, String oPCF4BPrem, String oPCF5Indicator, Double oPCF5CPrem,
			Double oPCF5DPrem, String oPCF5GIndicator, Integer oPCF8Deductible, String oPCF9Indicator,
			String oPCF13CIndicator, Integer oPCF19ValueVehicle, Double oPCF20Prem, String oPCF21AReceipts,
			String oPCF21APrem, String oPCF21BIndicator, String oPCF23AIndicator, String oPCF23BPrem,
			String oPCF25AIndicator, Double oPCF25APrem, String oPCF25A145Indicator, Integer oPCF27Limit,
			Integer oPCF27Deductible, Double oPCF27Prem, Integer oPCF27BDeductible, Integer oPCF27BLimit,
			Double oPCF27BPrem, String oPCF28Indicator, String oPCF28AIndicator, String oPCF30Indicator,
			String oPCF31Indicator, Integer oPCF38Limit, Double oPCF38Prem, Integer oPCF40Deductible, Double oPCF43Prem,
			Double oPCF43APrem, Double oPCF44Prem, String opcf47Exists, Double oPCF48Prem,
			String dealerPlateCoverageIndicator, Integer eON20ATravelExpensesDeductible,
			Double eON20ATravelExpensesPrem, Integer eON20GLossIncomeDeductible, Double eON20GLossIncomePrem,
			Integer eON104BuydownDeductible, Double eON104BuydownDeductiblePrem, Double conditionalReleaseBondPrem,
			Double autoEndtotalPrem, Double autoSection3BITotalPrem, Double autoSection3PDTotalPrem,
			Double autoSection4ABTotalPrem, Double autoSection5UATotalPrem, Integer autoSection6DCPDDeductible,
			Double autoSection6DCPDTotalPrem, Integer autoSection7CCLDeductible, Double autoSection7CCLTotalPrem,
			Integer autoSection7BCMDeductible, Double autoSection7BCMTotalPrem, Integer autoSection7DAPDeductible,
			Double autoSection7DAPTotalPrem, Integer autoSection7ASPDeductible, Double autoSection7ASPTotalPrem,
			Double autoABIncomeReplacementTotalPrem, String autoABIncomeReplacementIndicator, Integer autoABIncomeReplacementLimit,
			Double autoABDeathAndFuneralTotalPrem, String autoABDeathAndFuneralIndicator, Double autoABMedicalRehabAndATCTotalPrem,
			String autoABMedicalRehabAndATCIndicator, Integer autoABMedicalRehabAndATCLimit, Double autoABCATImpairementTotalPrem,
			String autoABCATImpairementIndicator, Integer autoABCATImpairementLimit, Double autoABCaregiverHousekeeperTotalPrem,
			String autoABCaregiverHousekeeperIndicator, Double autoABDependentCareTotalPrem, String autoABDependentCareIndicator,
			Double autoABIndexationTotalPrem, String autoABIndexationIndicator, Double autoWOEndtTotalPrem,
			// General Data 2
			String netReinsurancePremium, String reinsurerCode, String rISKID, String programCode, String product,
			String blankEndorsement, Double totalPrem, Double brokerCommissionAmount) {

		final Bordereaux bordereau = new Bordereaux();

		bordereau.setTermID(termID);
		bordereau.setTransactionID(transactionID);
		bordereau.setPolicyNumber(policyNumber);
		bordereau.setTransactionEffectiveDate(transactionEffectiveDate);
		bordereau.setPolicyExpiryDate(policyExpiryDate);
		bordereau.setPolicyEffectiveDate(policyEffectiveDate);
		bordereau.setTransactionType(transactionType);
		bordereau.setPolicyFleetIndicator(policyFleetIndicator);
		bordereau.setPolicyRiskProvince(policyRiskProvince);
		bordereau.setPolicyRSPIndicator(policyRSPIndicator);
		bordereau.setPolicyNewBusinessIndicator(policyNewBusinessIndicator);
		bordereau.setPolicySection3Limit(policySection3Limit);
		bordereau.setInsuredRIN(insuredRIN);
		bordereau.setInsuredName(insuredName);
		bordereau.setInsuredStreet(insuredStreet);
		bordereau.setInsuredCity(insuredCity);
		bordereau.setInsuredProvince(insuredProvince);
		bordereau.setInsuredPostalCode(insuredPostalCode);
		bordereau.setMasterBrokerName(masterBrokerName);
		bordereau.setMasterBrokerStreet(masterBrokerStreet);
		bordereau.setMasterBrokerCity(masterBrokerCity);
		bordereau.setMasterBrokerPostalCode(masterBrokerPostalCode);
		bordereau.setMasterBrokerNumber(masterBrokerNumber);
		 
		bordereau.setUnitNumber(unitNumber);
		bordereau.setUnitCount(unitCount);
		bordereau.setUnitStatCode(unitStatCode);
		bordereau.setUnitRatingTerritory(unitRatingTerritory);
		bordereau.setUnitVehicleType(unitVehicleType);
		bordereau.setUnitTrailerType(unitTrailerType);
		bordereau.setTrailerIndicator(trailerIndicator);
		bordereau.setUnitDescription(unitDescription);
		bordereau.setUnitVehicleClass(unitVehicleClass);
		bordereau.setUnitDriverRecord(unitDriverRecord);
		bordereau.setUnitMake(unitMake);
		bordereau.setUnitYear(unitYear);
		bordereau.setUnitVIN(unitVIN);
		bordereau.setUnitListPriceNew(unitListPriceNew);
		bordereau.setUnitLocationExposure(unitLocationExposure);
		bordereau.setOPCF2Prem(oPCF2Prem);
		bordereau.setOPCF4AType(oPCF4AType);
		bordereau.setOPCF4APrem(oPCF4APrem);
		bordereau.setOPCF4BType(oPCF4BType);
		bordereau.setOPCF4BPrem(oPCF4BPrem);
		bordereau.setOPCF5Indicator(oPCF5Indicator);
		bordereau.setOPCF5CPrem(numberFormat(oPCF5CPrem));
		bordereau.setOPCF5DPrem(numberFormat(oPCF5DPrem));
		bordereau.setOPCF5GIndicator(oPCF5GIndicator);
		bordereau.setOPCF8Deductible(numberFormat(oPCF8Deductible));
		bordereau.setOPCF9Indicator(oPCF9Indicator);
		bordereau.setOPCF13CIndicator(oPCF13CIndicator);
		bordereau.setOPCF19ValueVehicle(oPCF19ValueVehicle);
		bordereau.setOPCF20Prem(numberFormat(oPCF20Prem));
		bordereau.setOPCF21AReceipts(oPCF21AReceipts);
		bordereau.setOPCF21APrem(oPCF21APrem);
		bordereau.setOPCF21BIndicator(oPCF21BIndicator);
		bordereau.setOPCF23AIndicator(oPCF23AIndicator);
		bordereau.setOPCF23BPrem(oPCF23BPrem);
		bordereau.setOPCF25AIndicator(oPCF25AIndicator);
		bordereau.setOPCF25APrem(numberFormat(oPCF25APrem));
		bordereau.setOPCF25A145Indicator(oPCF25A145Indicator);
		bordereau.setOPCF27Limit(oPCF27Limit);
		bordereau.setOPCF27Deductible(oPCF27Deductible);
		bordereau.setOPCF27Prem(numberFormat(oPCF27Prem));
		bordereau.setOPCF27BDeductible(numberFormat(oPCF27BDeductible));
		bordereau.setOPCF27BLimit(numberFormat(oPCF27BLimit));
		bordereau.setoPCF27BPrem(numberFormat(oPCF27BPrem));
		bordereau.setOPCF28Indicator(oPCF28Indicator);
		bordereau.setOPCF28AIndicator(oPCF28AIndicator);
		bordereau.setOPCF30Indicator(oPCF30Indicator);
		bordereau.setOPCF31Indicator(oPCF31Indicator);
		bordereau.setOPCF38Limit(numberFormat(oPCF38Limit));
		bordereau.setOPCF38Prem(numberFormat(oPCF38Prem));
		bordereau.setOPCF40Deductible(numberFormat(oPCF40Deductible));
		bordereau.setOPCF43Prem(numberFormat(oPCF43Prem));
		bordereau.setOPCF43APrem(numberFormat(oPCF43APrem));
		bordereau.setOPCF44Prem(numberFormat(oPCF44Prem));
		bordereau.setOPCF47Indicator(opcf47Exists);
		bordereau.setOPCF48Prem(numberFormat(oPCF48Prem));
		bordereau.setDealerPlateCoverageIndicator(dealerPlateCoverageIndicator);
		bordereau.setEON20ATravelExpensesDeductible(numberFormat(eON20ATravelExpensesDeductible));
		bordereau.seteON20ATravelExpensesPrem(numberFormat(eON20ATravelExpensesPrem));
		bordereau.setEON20GLossIncomeDeductible(numberFormat(eON20GLossIncomeDeductible));
		bordereau.setEON20GLossIncomePrem(numberFormat(eON20GLossIncomePrem));
		bordereau.setEON104BuydownDeductible(numberFormat(eON104BuydownDeductible));
		bordereau.setEON104BuydownDeductiblePrem(numberFormat(eON104BuydownDeductiblePrem));
		bordereau.setConditionalReleaseBondPrem(numberFormat(conditionalReleaseBondPrem));
		bordereau.setAutoEndtotalPrem(numberFormat(autoEndtotalPrem));
		bordereau.setAutoSection3BITotalPrem(numberFormat(autoSection3BITotalPrem));
		bordereau.setAutoSection3PDTotalPrem(numberFormat(autoSection3PDTotalPrem));
		bordereau.setAutoSection4ABTotalPrem(numberFormat(autoSection4ABTotalPrem));
		bordereau.setAutoSection5UATotalPrem(numberFormat(autoSection5UATotalPrem));
		bordereau.setAutoSection6DCPDDeductible(numberFormat(autoSection6DCPDDeductible));
		bordereau.setAutoSection6DCPDTotalPrem(numberFormat(autoSection6DCPDTotalPrem));
		bordereau.setAutoSection7CCLDeductible(numberFormat(autoSection7CCLDeductible));
		bordereau.setAutoSection7CCLTotalPrem(numberFormat(autoSection7CCLTotalPrem));
		bordereau.setAutoSection7BCMDeductible(numberFormat(autoSection7BCMDeductible));
		bordereau.setAutoSection7BCMTotalPrem(numberFormat(autoSection7BCMTotalPrem));
		bordereau.setAutoSection7DAPDeductible(numberFormat(autoSection7DAPDeductible));
		bordereau.setAutoSection7DAPTotalPrem(numberFormat(autoSection7DAPTotalPrem));
		bordereau.setAutoSection7ASPDeductible(numberFormat(autoSection7ASPDeductible));
		bordereau.setAutoSection7ASPTotalPrem(numberFormat(autoSection7ASPTotalPrem));
		bordereau.setAutoABIncomeReplacementTotalPrem(numberFormat(autoABIncomeReplacementTotalPrem));
		bordereau.setAutoABIncomeReplacementIndicator(autoABIncomeReplacementIndicator);
		bordereau.setAutoABIncomeReplacementLimit(numberFormat(autoABIncomeReplacementLimit));
		bordereau.setAutoABDeathAndFuneralTotalPrem(numberFormat(autoABDeathAndFuneralTotalPrem));
		bordereau.setAutoABDeathAndFuneralIndicator(autoABDeathAndFuneralIndicator);
		bordereau.setAutoABMedicalRehabAndATCTotalPrem(numberFormat(autoABMedicalRehabAndATCTotalPrem));
		bordereau.setAutoABMedicalRehabAndATCIndicator(autoABMedicalRehabAndATCIndicator);
		bordereau.setAutoABMedicalRehabAndATCLimit(numberFormat(autoABMedicalRehabAndATCLimit));
		bordereau.setAutoABCATImpairementTotalPrem(numberFormat(autoABCATImpairementTotalPrem));
		bordereau.setAutoABCATImpairementIndicator(autoABCATImpairementIndicator);
		bordereau.setAutoABCATImpairementLimit(numberFormat(autoABCATImpairementLimit));
		bordereau.setAutoABCaregiverHousekeeperTotalPrem(numberFormat(autoABCaregiverHousekeeperTotalPrem));
		bordereau.setAutoABCaregiverHousekeeperIndicator(autoABCaregiverHousekeeperIndicator);
		bordereau.setAutoABDependentCareTotalPrem(numberFormat(autoABDependentCareTotalPrem));
		bordereau.setAutoABDependentCareIndicator(autoABDependentCareIndicator);
		bordereau.setAutoABIndexationTotalPrem(numberFormat(autoABIndexationTotalPrem));
		bordereau.setAutoABIndexationIndicator(autoABIndexationIndicator);
		bordereau.setAutoWOEndtTotalPrem(numberFormat(autoWOEndtTotalPrem));

		bordereau.setNetReinsurancePremium(netReinsurancePremium);
		bordereau.setReinsurerCode(reinsurerCode);
		bordereau.setRISKID(rISKID);
		bordereau.setProgramCode(programCode);
		bordereau.setProduct(product);
		bordereau.setBlankEndorsement(blankEndorsement);
		bordereau.setTotalPrem(numberFormat(totalPrem));
		bordereau.setBrokerCommissionAmount(brokerCommissionAmount);

		bordereauxList.add(bordereau);
	}
	
	
	
	public void setBorderauxOther(
			// General Data 1
			Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet,
			String insuredCity, String insuredProvince, String insuredPostalCode, String masterBrokerName,
			String masterBrokerStreet, String masterBrokerCity, String masterBrokerPostalCode,
			String masterBrokerNumber,
			// Other
			Integer qE4037RContentsLimit, Integer qE4037RContentsDeductible, Double qE4037RContentsPrem,
			String qE4037RPropertyExtensionIndicator, String eON101BlanketLessorsIndicator,
			String eON102AdditionalInsuredIndividualIndicator, String eON103AdditionalInsuredOOIndicator,
			String eON109InsurableInterestDeletedIndicator, String eON110PerOccuranceDeductibleIndicator,
			Integer eON110PerOccuranceDeductible, String eON111ProfitSharingIndicator,
			Double eON112ProfitSharingAdjustmentPrem, String eON114NamedInsuredIndicator,
			Double eON115OPCF21AReceiptsBasisAdjustmentPrem, Double eON116OPCF21B5050BasisAdjustmentPrem,
			Double eON117OPCF21BProRataBasisAdjustmentPrem, String eON119RegisteredOwnerIndicator,
			Double eON121PolicyCancellationPrem, Double eON122PolicyCancellationAdjustementPrem,
			String indemnityAgreementIndicator,
			// General Data 2
			String netReinsurancePremium, String reinsurerCode, String rISKID, String programCode, String product,
			String blankEndorsement, Double totalPrem, Double brokerCommissionAmount) {

		final Bordereaux bordereau = new Bordereaux();

		bordereau.setTermID(termID);
		bordereau.setTransactionID(transactionID);
		bordereau.setPolicyNumber(policyNumber);
		bordereau.setTransactionEffectiveDate(transactionEffectiveDate);
		bordereau.setPolicyExpiryDate(policyExpiryDate);
		bordereau.setPolicyEffectiveDate(policyEffectiveDate);
		bordereau.setTransactionType(transactionType);
		bordereau.setPolicyFleetIndicator(policyFleetIndicator);
		bordereau.setPolicyRiskProvince(policyRiskProvince);
		bordereau.setPolicyRSPIndicator(policyRSPIndicator);
		bordereau.setPolicyNewBusinessIndicator(policyNewBusinessIndicator);
		bordereau.setPolicySection3Limit(policySection3Limit);
		bordereau.setInsuredRIN(insuredRIN);
		bordereau.setInsuredName(insuredName);
		bordereau.setInsuredStreet(insuredStreet);
		bordereau.setInsuredCity(insuredCity);
		bordereau.setInsuredProvince(insuredProvince);
		bordereau.setInsuredPostalCode(insuredPostalCode);
		bordereau.setMasterBrokerName(masterBrokerName);
		bordereau.setMasterBrokerStreet(masterBrokerStreet);
		bordereau.setMasterBrokerCity(masterBrokerCity);
		bordereau.setMasterBrokerPostalCode(masterBrokerPostalCode);
		bordereau.setMasterBrokerNumber(masterBrokerNumber);

		bordereau.setQE4037RContentsLimit(qE4037RContentsLimit);
		bordereau.setQE4037RContentsDeductible(qE4037RContentsDeductible);
		bordereau.setQE4037RContentsPrem(qE4037RContentsPrem);
		bordereau.setQE4037RPropertyExtensionIndicator(qE4037RPropertyExtensionIndicator);
		bordereau.setEON101BlanketLessorsIndicator(eON101BlanketLessorsIndicator);
		bordereau.setEON102AdditionalInsuredIndividualIndicator(eON102AdditionalInsuredIndividualIndicator);
		bordereau.setEON103AdditionalInsuredOOIndicator(eON103AdditionalInsuredOOIndicator);
		bordereau.setEON109InsurableInterestDeletedIndicator(eON109InsurableInterestDeletedIndicator);
		bordereau.setEON110PerOccuranceDeductibleIndicator(eON110PerOccuranceDeductibleIndicator);
		bordereau.setEON110PerOccuranceDeductible(numberFormat(eON110PerOccuranceDeductible));
		bordereau.setEON111ProfitSharingIndicator(eON111ProfitSharingIndicator);
		bordereau.setEON112ProfitSharingAdjustmentPrem(numberFormat(eON112ProfitSharingAdjustmentPrem));
		bordereau.setEON114NamedInsuredIndicator(eON114NamedInsuredIndicator);
		bordereau.setEON115OPCF21AReceiptsBasisAdjustmentPrem(numberFormat(eON115OPCF21AReceiptsBasisAdjustmentPrem));
		bordereau.setEON116OPCF21B5050BasisAdjustmentPrem(numberFormat(eON116OPCF21B5050BasisAdjustmentPrem));
		bordereau.setEON117OPCF21BProRataBasisAdjustmentPrem(numberFormat(eON117OPCF21BProRataBasisAdjustmentPrem));
		bordereau.setEON119RegisteredOwnerIndicator(eON119RegisteredOwnerIndicator);
		bordereau.setEON121PolicyCancellationPrem(numberFormat(eON121PolicyCancellationPrem));
		bordereau.setEON122PolicyCancellationAdjustementPrem(numberFormat(eON122PolicyCancellationAdjustementPrem));
		bordereau.setIndemnityAgreementIndicator(indemnityAgreementIndicator);

		bordereau.setNetReinsurancePremium(netReinsurancePremium);
		bordereau.setReinsurerCode(reinsurerCode);
		bordereau.setRISKID(rISKID);
		bordereau.setProgramCode(programCode);
		bordereau.setProduct(product);
		bordereau.setBlankEndorsement(blankEndorsement);
		bordereau.setTotalPrem(numberFormat(totalPrem));
		bordereau.setBrokerCommissionAmount(brokerCommissionAmount);

		bordereauxList.add(bordereau);
	}
	
	
	public void setBorderauxGarage(
			// General Data 1
			Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet,
			String insuredCity, String insuredProvince, String insuredPostalCode, String masterBrokerName,
			String masterBrokerStreet, String masterBrokerCity, String masterBrokerPostalCode,
			String masterBrokerNumber,

			// Garage
			String businessOperations, String locationAddress, String locationCity, String locationProvince,
			String locationPostalCode, String locationIncludeBuilding, String locationIncludeLot,
			Integer locationTotalFullTimeEmployees, Integer locationTotalPartTimeEmployees, Integer oAP4Section1Limit,
			Double oAP4Section1BIPrem, Double oAP4Section1PDPrem, Double oAP4Section2ABPrem, Double oAP4Section3UAPrem,
			Integer oAP4Section4DCPDDeductible, Double oAP4Section4DCPDPrem, Integer oAP4Section511CLDeductible,
			Double oAP4Section511CLPremium, Integer oAP4Section512CMLimit, Integer oAP4Section512CMDeductible,
			Double oAP4Section512CMPremium, Integer oAP4Section513SPLimit, Integer oAP4Section513SPDeductible,
			Double oAP4Section513SPPremium, Integer oAP4Section514SPLimit, Integer oAP4Section514SPDeductible,
			Double oAP4Section514SPPremium, Integer oAP4Section61CLLimit, Integer oAP4Section61CLDeductible,
			Double oAP4Section61CLPrem, Integer oAP4Section64SPLimit, Integer oAP4Section64SPDeductible,
			Integer oAP4Section64SPMaximumVehicles, Double oAP4Section64SPPrem,
			String oEF71ExcludingOwnedAutomobilesIndicator, Double oEF72MultipleAlterationPremium,
			String oEF73ExcludingFinancedIndicator, Double oEF74OpenLotTheftPremium, Double oEF75OpenLotTheftPrem,
			Double oEF76AdditionalInsuredPremium, Double oEF77CustomerAutoPrem,
			String oEF78AExcludedDriverEndorsementIndicator, String oEF78ReductionCoverageNamedPersonsIndicator,
			String oEF79FireDeductibleOwnedAutomobilesIndicator,
			Double oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium, Integer oEF81GarageFamilyProtectionLimit,
			Double oEF81GarageFamilyProtectionPrem, Integer oEF82LiabilityDamagedNonOwnedLimit,
			Integer oEF82LiabilityDamagedNonOwnedDeductible, Double oEF82LiabilityDamagedNonOwnedPremium,
			Double oEF83AutomobileTransportationPremium, String oEF85FinalPremiumComputationIndicator,
			String oEF86CustomersAutomobilesFireDeductibleIndicator, Double oAP4TotalPrem,
			String garageServicePlateCoverageIndicator, Integer garageServicePlateCount,

			// General Data 2
			String netReinsurancePremium, String reinsurerCode, String rISKID, String programCode, String product,
			String blankEndorsement, Double totalPrem, Double brokerCommissionAmount) {

		final Bordereaux bordereau = new Bordereaux();

		bordereau.setTermID(termID);
		bordereau.setTransactionID(transactionID);
		bordereau.setPolicyNumber(policyNumber);
		bordereau.setTransactionEffectiveDate(transactionEffectiveDate);
		bordereau.setPolicyExpiryDate(policyExpiryDate);
		bordereau.setPolicyEffectiveDate(policyEffectiveDate);
		bordereau.setTransactionType(transactionType);
		bordereau.setPolicyFleetIndicator(policyFleetIndicator);
		bordereau.setPolicyRiskProvince(policyRiskProvince);
		bordereau.setPolicyRSPIndicator(policyRSPIndicator);
		bordereau.setPolicyNewBusinessIndicator(policyNewBusinessIndicator);
		bordereau.setPolicySection3Limit(policySection3Limit);
		bordereau.setInsuredRIN(insuredRIN);
		bordereau.setInsuredName(insuredName);
		bordereau.setInsuredStreet(insuredStreet);
		bordereau.setInsuredCity(insuredCity);
		bordereau.setInsuredProvince(insuredProvince);
		bordereau.setInsuredPostalCode(insuredPostalCode);
		bordereau.setMasterBrokerName(masterBrokerName);
		bordereau.setMasterBrokerStreet(masterBrokerStreet);
		bordereau.setMasterBrokerCity(masterBrokerCity);
		bordereau.setMasterBrokerPostalCode(masterBrokerPostalCode);
		bordereau.setMasterBrokerNumber(masterBrokerNumber);

	
		bordereau.setBusinessOperations(businessOperations);
		bordereau.setLocationAddress(locationAddress);
		bordereau.setLocationCity(locationCity);
		bordereau.setLocationProvince(locationProvince);
		bordereau.setLocationPostalCode(locationPostalCode);
		bordereau.setLocationIncludeBuilding(locationIncludeBuilding);
		bordereau.setLocationIncludeLot(locationIncludeLot);
		bordereau.setLocationTotalFullTimeEmployees(locationTotalFullTimeEmployees);
		bordereau.setLocationTotalPartTimeEmployees(locationTotalPartTimeEmployees);
		bordereau.setOAP4Section1Limit(numberFormat(oAP4Section1Limit));
		bordereau.setOAP4Section1BIPrem(numberFormat(oAP4Section1BIPrem));
		bordereau.setOAP4Section1PDPrem(numberFormat(oAP4Section1PDPrem));
		bordereau.setOAP4Section2ABPrem(numberFormat(oAP4Section2ABPrem));
		bordereau.setOAP4Section3UAPrem(numberFormat(oAP4Section3UAPrem));
		bordereau.setOAP4Section4DCPDDeductible(numberFormat(oAP4Section4DCPDDeductible));
		bordereau.setOAP4Section4DCPDPrem(numberFormat(oAP4Section4DCPDPrem));
		bordereau.setOAP4Section61CLLimit(numberFormat(oAP4Section61CLLimit));
		bordereau.setOAP4Section511CLDeductible(oAP4Section511CLDeductible);
		bordereau.setOAP4Section511CLPremium(oAP4Section511CLPremium);
		bordereau.setOAP4Section512CMLimit(oAP4Section512CMLimit);
		bordereau.setOAP4Section512CMDeductible(oAP4Section512CMDeductible);
		bordereau.setOAP4Section512CMPremium(oAP4Section512CMPremium);
		bordereau.setOAP4Section513SPLimit(oAP4Section513SPLimit);
		bordereau.setOAP4Section513SPDeductible(oAP4Section513SPDeductible);
		bordereau.setOAP4Section513SPPremium(oAP4Section513SPPremium);
		bordereau.setOAP4Section514SPLimit(oAP4Section514SPLimit);
		bordereau.setOAP4Section514SPDeductible(oAP4Section514SPDeductible);
		bordereau.setOAP4Section514SPPremium(oAP4Section514SPPremium);
		bordereau.setOAP4Section61CLDeductible(numberFormat(oAP4Section61CLDeductible));
		bordereau.setOAP4Section61CLPrem(numberFormat(oAP4Section61CLPrem));
		bordereau.setOAP4Section64SPLimit(numberFormat(oAP4Section64SPLimit));
		bordereau.setOAP4Section64SPDeductible(numberFormat(oAP4Section64SPDeductible));
		bordereau.setOAP4Section64SPMaximumVehicles(oAP4Section64SPMaximumVehicles);
		bordereau.setOAP4Section64SPPrem(numberFormat(oAP4Section64SPPrem));
		bordereau.setOEF71ExcludingOwnedAutomobilesIndicator(oEF71ExcludingOwnedAutomobilesIndicator);
		bordereau.setOEF72MultipleAlterationPremium(numberFormat(oEF72MultipleAlterationPremium));
		bordereau.setOEF73ExcludingFinancedIndicator(oEF73ExcludingFinancedIndicator);
		bordereau.setOEF74OpenLotTheftPremium(numberFormat(oEF74OpenLotTheftPremium));
		bordereau.setOEF75OpenLotTheftPrem(numberFormat(oEF75OpenLotTheftPrem));
		bordereau.setOEF76AdditionalInsuredPremium(numberFormat(oEF76AdditionalInsuredPremium));
		bordereau.setOEF77CustomerAutoPrem(numberFormat(oEF77CustomerAutoPrem));
		bordereau.setOEF78AExcludedDriverEndorsementIndicator(oEF78AExcludedDriverEndorsementIndicator);
		bordereau.setOEF78ReductionCoverageNamedPersonsIndicator(oEF78ReductionCoverageNamedPersonsIndicator);
		bordereau.setOEF79FireDeductibleOwnedAutomobilesIndicator(oEF79FireDeductibleOwnedAutomobilesIndicator);
		bordereau.setOEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium(
				numberFormat(oEF80SpecifiedOwnedAutomobilePhysicalDamageCoveragePremium));
		bordereau.setOEF81GarageFamilyProtectionLimit(numberFormat(oEF81GarageFamilyProtectionLimit));
		bordereau.setOEF81GarageFamilyProtectionPrem(numberFormat(oEF81GarageFamilyProtectionPrem));
		bordereau.setOEF82LiabilityDamagedNonOwnedLimit(numberFormat(oEF82LiabilityDamagedNonOwnedLimit));
		bordereau.setOEF82LiabilityDamagedNonOwnedDeductible(numberFormat(oEF82LiabilityDamagedNonOwnedDeductible));
		bordereau.setOEF82LiabilityDamagedNonOwnedPremium(numberFormat(oEF82LiabilityDamagedNonOwnedPremium));
		bordereau.setOEF83AutomobileTransportationPremium(numberFormat(oEF83AutomobileTransportationPremium));
		bordereau.setOEF85FinalPremiumComputationIndicator(oEF85FinalPremiumComputationIndicator);
		bordereau.setOEF86CustomersAutomobilesFireDeductibleIndicator(oEF86CustomersAutomobilesFireDeductibleIndicator);
		bordereau.setOAP4TotalPrem(numberFormat(oAP4TotalPrem));
		bordereau.setGarageServicePlateCoverageIndicator(garageServicePlateCoverageIndicator);
		bordereau.setGarageServicePlateCount(garageServicePlateCount);

		bordereau.setNetReinsurancePremium(netReinsurancePremium);
		bordereau.setReinsurerCode(reinsurerCode);
		bordereau.setRISKID(rISKID);
		bordereau.setProgramCode(programCode);
		bordereau.setProduct(product);
		bordereau.setBlankEndorsement(blankEndorsement);
		bordereau.setTotalPrem(numberFormat(totalPrem));
		bordereau.setBrokerCommissionAmount(brokerCommissionAmount);

		bordereauxList.add(bordereau);
	}
	
	
	
	public void setBorderauxCGL(
			// General Data 1
			Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet,
			String insuredCity, String insuredProvince, String insuredPostalCode, String masterBrokerName,
			String masterBrokerStreet, String masterBrokerCity, String masterBrokerPostalCode,
			String masterBrokerNumber,

			// CGL
			Integer cGLDeductible, Integer cGLLimit, String tenantsLegalLiabilityStreet,
			String tenantsLegalLiabilityCity, String tenantsLegalLiabilityPostalCode,
			Integer tenantsLegalLiabilityLimit, Integer tenantsLegalLiabilityDeductible,
			Double tenantsLegalLiabilityPrem, Integer qE4023NWarehousemanLegalLiabilityLimit,
			Integer qE4023NWarehousemanLegalLiabilityDeductible, Double qE4023NWarehousemanLegalLiabilityPrem,
			Integer qE2033NEmployeeBenefitsExtensionLimit, Integer qE2033NEmployeeBenefitsExtensionDeductible,
			Double qE2033NEmployeeBenefitsExtensionPrem, Integer qE2313NLimitedPollutionLiabilityLimit,
			Integer qE2313NLimitedPollutionLiabilityDeductible, Double qE2313NLimitedPollutionLiabilityPrem,
			String sPF6NonOwnedAutomobileIndicator, String sEF94LegalLiabilityIndicator,
			String sEF96ContractualLiabilityIndicator, String sEF99LeasedVehicleIndicator,
			String garageLiabilityExtensionIndicator, String qE2345NTotalPollutionExclusionIndicator,
			Double qE2345NTotalPollutionExclusionPremium, Integer mobileEquipmentLimit,
			Integer mobileEquipmentDeductible, Double mobileEquipmentPrem, Double cGLGeneralEndorsementPrem,
			Double cGLTotalPrem,
	
			// General Data 2
			String netReinsurancePremium, String reinsurerCode, String rISKID, String programCode, String product,
			String blankEndorsement, Double totalPrem, Double brokerCommissionAmount) {

		final Bordereaux bordereau = new Bordereaux();

		bordereau.setTermID(termID);
		bordereau.setTransactionID(transactionID);
		bordereau.setPolicyNumber(policyNumber);
		bordereau.setTransactionEffectiveDate(transactionEffectiveDate);
		bordereau.setPolicyExpiryDate(policyExpiryDate);
		bordereau.setPolicyEffectiveDate(policyEffectiveDate);
		bordereau.setTransactionType(transactionType);
		bordereau.setPolicyFleetIndicator(policyFleetIndicator);
		bordereau.setPolicyRiskProvince(policyRiskProvince);
		bordereau.setPolicyRSPIndicator(policyRSPIndicator);
		bordereau.setPolicyNewBusinessIndicator(policyNewBusinessIndicator);
		bordereau.setPolicySection3Limit(policySection3Limit);
		bordereau.setInsuredRIN(insuredRIN);
		bordereau.setInsuredName(insuredName);
		bordereau.setInsuredStreet(insuredStreet);
		bordereau.setInsuredCity(insuredCity);
		bordereau.setInsuredProvince(insuredProvince);
		bordereau.setInsuredPostalCode(insuredPostalCode);
		bordereau.setMasterBrokerName(masterBrokerName);
		bordereau.setMasterBrokerStreet(masterBrokerStreet);
		bordereau.setMasterBrokerCity(masterBrokerCity);
		bordereau.setMasterBrokerPostalCode(masterBrokerPostalCode);
		bordereau.setMasterBrokerNumber(masterBrokerNumber);


		bordereau.setCGLDeductible(numberFormat(cGLDeductible));
		bordereau.setCGLLimit(numberFormat(cGLLimit));
		bordereau.setTenantsLegalLiabilityStreet(tenantsLegalLiabilityStreet);
		bordereau.setTenantsLegalLiabilityCity(tenantsLegalLiabilityCity);
		bordereau.setTenantsLegalLiabilityPostalCode(tenantsLegalLiabilityPostalCode);
		bordereau.setTenantsLegalLiabilityLimit(numberFormat(tenantsLegalLiabilityLimit));
		bordereau.setTenantsLegalLiabilityDeductible(numberFormat(tenantsLegalLiabilityDeductible));
		bordereau.setTenantsLegalLiabilityPrem(numberFormat(tenantsLegalLiabilityPrem));
		bordereau.setQE4023NWarehousemanLegalLiabilityLimit(numberFormat(qE4023NWarehousemanLegalLiabilityLimit));
		bordereau.setQE4023NWarehousemanLegalLiabilityDeductible(
				numberFormat(qE4023NWarehousemanLegalLiabilityDeductible));
		bordereau.setQE4023NWarehousemanLegalLiabilityPrem(numberFormat(qE4023NWarehousemanLegalLiabilityPrem));
		bordereau.setQE2033NEmployeeBenefitsExtensionLimit(numberFormat(qE2033NEmployeeBenefitsExtensionLimit));
		bordereau.setQE2033NEmployeeBenefitsExtensionDeductible(
				numberFormat(qE2033NEmployeeBenefitsExtensionDeductible));
		bordereau.setQE2033NEmployeeBenefitsExtensionPrem(numberFormat(qE2033NEmployeeBenefitsExtensionPrem));
		bordereau.setQE2313NLimitedPollutionLiabilityLimit(numberFormat(qE2313NLimitedPollutionLiabilityLimit));
		bordereau.setQE2313NLimitedPollutionLiabilityDeductible(
				numberFormat(qE2313NLimitedPollutionLiabilityDeductible));
		bordereau.setQE2313NLimitedPollutionLiabilityPrem(numberFormat(qE2313NLimitedPollutionLiabilityPrem));
		bordereau.setSPF6NonOwnedAutomobileIndicator(sPF6NonOwnedAutomobileIndicator);
		bordereau.setSEF94LegalLiabilityIndicator(sEF94LegalLiabilityIndicator);
		bordereau.setSEF96ContractualLiabilityIndicator(sEF96ContractualLiabilityIndicator);
		bordereau.setSEF99LeasedVehicleIndicator(sEF99LeasedVehicleIndicator);
		bordereau.setGarageLiabilityExtensionIndicator(garageLiabilityExtensionIndicator);
		bordereau.setQE2345NTotalPollutionExclusionIndicator(qE2345NTotalPollutionExclusionIndicator);
		bordereau.setQE2345NTotalPollutionExclusionPremium(numberFormat(qE2345NTotalPollutionExclusionPremium));
		bordereau.setMobileEquipmentLimit(numberFormat(mobileEquipmentLimit));
		bordereau.setMobileEquipmentDeductible(numberFormat(mobileEquipmentDeductible));
		bordereau.setMobileEquipmentPrem(numberFormat(mobileEquipmentPrem));
		bordereau.setcGLGeneralEndorsementPrem(numberFormat(cGLGeneralEndorsementPrem));
		bordereau.setCGLTotalPrem(numberFormat(cGLTotalPrem));

		bordereau.setNetReinsurancePremium(netReinsurancePremium);
		bordereau.setReinsurerCode(reinsurerCode);
		bordereau.setRISKID(rISKID);
		bordereau.setProgramCode(programCode);
		bordereau.setProduct(product);
		bordereau.setBlankEndorsement(blankEndorsement);
		bordereau.setTotalPrem(numberFormat(totalPrem));
		bordereau.setBrokerCommissionAmount(brokerCommissionAmount);

		bordereauxList.add(bordereau);
	}
	
	
	public void setBorderauxCargo(
			// General Data 1
			Integer termID, Integer transactionID, String policyNumber, LocalDate policyEffectiveDate, LocalDate policyExpiryDate,
			LocalDate transactionEffectiveDate, String transactionType, String policyFleetIndicator,
			String policyRiskProvince, String policyRSPIndicator, String policyNewBusinessIndicator,
			Integer policySection3Limit, String insuredRIN, String insuredName, String insuredStreet,
			String insuredCity, String insuredProvince, String insuredPostalCode, String masterBrokerName,
			String masterBrokerStreet, String masterBrokerCity, String masterBrokerPostalCode,
			String masterBrokerNumber,

			// Cargo
			Integer contentsPersonalEffectsLimit, Integer contentsPersonalEffectsDeductible,
			Double contentsPersonalEffectsPremium, Integer cargoLimit, Integer cargoDeductible,
			String cargoAdditionalLimitPrem, Integer accruedFreightChargesLimit, Double accruedFreightChargesPremium,
			Integer contingentCargoLimit, Integer contingentCargoDeductible, Double contingentCargoPrem,
			Integer debrisRemovalLimit, Double debrisRemovalPremium, Integer equipmentLimit, Double equipmentPremium,
			Integer identityMisrepresentationLimit, Double identityMisrepresentationPremium,
			Integer temporaryStorageLimit, Double temporaryStoragePremium, Integer tripTransitCargoAdditionalLimit,
			Double tripTransitCargoPrem, String qE4201ROwnersFormLimit, String qE4201ROwnersFormDeductible,
			String qE4201ROwnersFormPrem, String eON141ChangetoDeductibleIndicator,
			String dangerousGoodsExclusionIndicator, String dangerousGoodsPartialLoadIndicator,
			String dangerousGoodsProductClassesIndicator, String forestryExclusionIndicator,
			String cargoExclusionIndicator, Double cargoGeneralEndorsementPrem, Double cargoTotalPrem,

			// General Data 2
			String netReinsurancePremium, String reinsurerCode, String rISKID, String programCode, String product,
			String blankEndorsement, Double totalPrem, Double brokerCommissionAmount) {

		final Bordereaux bordereau = new Bordereaux();

		bordereau.setTermID(termID);
		bordereau.setTransactionID(transactionID);
		bordereau.setPolicyNumber(policyNumber);
		bordereau.setTransactionEffectiveDate(transactionEffectiveDate);
		bordereau.setPolicyExpiryDate(policyExpiryDate);
		bordereau.setPolicyEffectiveDate(policyEffectiveDate);
		bordereau.setTransactionType(transactionType);
		bordereau.setPolicyFleetIndicator(policyFleetIndicator);
		bordereau.setPolicyRiskProvince(policyRiskProvince);
		bordereau.setPolicyRSPIndicator(policyRSPIndicator);
		bordereau.setPolicyNewBusinessIndicator(policyNewBusinessIndicator);
		bordereau.setPolicySection3Limit(policySection3Limit);
		bordereau.setInsuredRIN(insuredRIN);
		bordereau.setInsuredName(insuredName);
		bordereau.setInsuredStreet(insuredStreet);
		bordereau.setInsuredCity(insuredCity);
		bordereau.setInsuredProvince(insuredProvince);
		bordereau.setInsuredPostalCode(insuredPostalCode);
		bordereau.setMasterBrokerName(masterBrokerName);
		bordereau.setMasterBrokerStreet(masterBrokerStreet);
		bordereau.setMasterBrokerCity(masterBrokerCity);
		bordereau.setMasterBrokerPostalCode(masterBrokerPostalCode);
		bordereau.setMasterBrokerNumber(masterBrokerNumber);

		bordereau.setContentsPersonalEffectsLimit(numberFormat(contentsPersonalEffectsLimit));
		bordereau.setContentsPersonalEffectsDeductible(numberFormat(contentsPersonalEffectsDeductible));
		bordereau.setContentsPersonalEffectsPremium(numberFormat(contentsPersonalEffectsPremium));
		bordereau.setCargoLimit(numberFormat(cargoLimit));
		bordereau.setCargoDeductible(numberFormat(cargoDeductible));
		bordereau.setCargoAdditionalLimitPrem(cargoAdditionalLimitPrem);
		bordereau.setAccruedFreightChargesLimit(numberFormat(accruedFreightChargesLimit));
		bordereau.setAccruedFreightChargesPremium(numberFormat(accruedFreightChargesPremium));
		bordereau.setContingentCargoLimit(numberFormat(contingentCargoLimit));
		bordereau.setContingentCargoDeductible(numberFormat(contingentCargoDeductible));
		bordereau.setContingentCargoPrem(numberFormat(contingentCargoPrem));
		bordereau.setDebrisRemovalLimit(numberFormat(debrisRemovalLimit));
		bordereau.setDebrisRemovalPremium(numberFormat(debrisRemovalPremium));
		bordereau.setEquipmentLimit(numberFormat(equipmentLimit));
		bordereau.setEquipmentPremium(numberFormat(equipmentPremium));
		bordereau.setIdentityMisrepresentationLimit(numberFormat(identityMisrepresentationLimit));
		bordereau.setIdentityMisrepresentationPremium(numberFormat(identityMisrepresentationPremium));
		bordereau.setTemporaryStorageLimit(numberFormat(temporaryStorageLimit));
		bordereau.setTemporaryStoragePremium(numberFormat(temporaryStoragePremium));
		bordereau.setTripTransitCargoAdditionalLimit(numberFormat(tripTransitCargoAdditionalLimit));
		bordereau.setTripTransitCargoPrem(numberFormat(tripTransitCargoPrem));
		bordereau.setQE4201ROwnersFormLimit(qE4201ROwnersFormLimit);
		bordereau.setQE4201ROwnersFormDeductible(qE4201ROwnersFormDeductible);
		bordereau.setQE4201ROwnersFormPrem(qE4201ROwnersFormPrem);
		bordereau.setEON141ChangetoDeductibleIndicator(eON141ChangetoDeductibleIndicator);
		bordereau.setDangerousGoodsExclusionIndicator(dangerousGoodsExclusionIndicator);
		bordereau.setDangerousGoodsPartialLoadIndicator(dangerousGoodsPartialLoadIndicator);
		bordereau.setDangerousGoodsProductClassesIndicator(dangerousGoodsProductClassesIndicator);
		bordereau.setForestryExclusionIndicator(forestryExclusionIndicator);
		bordereau.setCargoExclusionIndicator(cargoExclusionIndicator);
		bordereau.setCargoGeneralEndorsementPrem(numberFormat(cargoGeneralEndorsementPrem));
		bordereau.setCargoTotalPrem(numberFormat(cargoTotalPrem));


		bordereau.setNetReinsurancePremium(netReinsurancePremium);
		bordereau.setReinsurerCode(reinsurerCode);
		bordereau.setRISKID(rISKID);
		bordereau.setProgramCode(programCode);
		bordereau.setProduct(product);
		bordereau.setBlankEndorsement(blankEndorsement);
		bordereau.setTotalPrem(numberFormat(totalPrem));
		bordereau.setBrokerCommissionAmount(brokerCommissionAmount);

		bordereauxList.add(bordereau);
	}

	public Double getEndorsementPremium(PolicyTransaction pt, String endorsementValue, String subPolicyType, boolean ischange, boolean negativeValue) {

		Double endorsmentPremium = (double) 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Endorsement> endorsmentList = subPolicy.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((ischange /*&& !endo.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
						endorsmentPremium = (endo.getEndorsementPremium().getNetPremiumChange() != null
								? endorsmentPremium + endo.getEndorsementPremium().getNetPremiumChange()
								: endorsmentPremium);
					}else {
						endorsmentPremium = (endo.getEndorsementPremium().getTransactionPremium() != null
								? endorsmentPremium + endo.getEndorsementPremium().getTransactionPremium()
								: endorsmentPremium);
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			List<Endorsement> endorsmentList = subPolicyCGL.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((ischange /*&& !endo.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
						endorsmentPremium = (endo.getEndorsementPremium().getNetPremiumChange() != null
								? endorsmentPremium + endo.getEndorsementPremium().getNetPremiumChange()
								: endorsmentPremium);
					}else {
						endorsmentPremium = (endo.getEndorsementPremium().getTransactionPremium() != null
								? endorsmentPremium + endo.getEndorsementPremium().getTransactionPremium()
								: endorsmentPremium);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Endorsement> endorsmentList = subPolicyCGO.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((ischange /*&& !endo.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
						endorsmentPremium = (endo.getEndorsementPremium().getNetPremiumChange() != null
								? endorsmentPremium + endo.getEndorsementPremium().getNetPremiumChange()
								: endorsmentPremium);
					}else {
						endorsmentPremium = (endo.getEndorsementPremium().getTransactionPremium() != null
								? endorsmentPremium + endo.getEndorsementPremium().getTransactionPremium()
								: endorsmentPremium);
					}
				}
			}
		}

		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((ischange /*&& !endo.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
						endorsmentPremium = (endo.getEndorsementPremium().getNetPremiumChange() != null
								? endorsmentPremium + endo.getEndorsementPremium().getNetPremiumChange()
								: endorsmentPremium);
					}else {
						endorsmentPremium = (endo.getEndorsementPremium().getTransactionPremium() != null
								? endorsmentPremium + endo.getEndorsementPremium().getTransactionPremium()
								: endorsmentPremium);
					}
				}
			}
		}
		
		if (subPolicyType == null) {//This blok is sused to get endorsment from the "Other" subpolicy
			List<Endorsement> endorsmentList = pt.getCoverageDetail().getEndorsements().stream().filter(t -> 
			t.getEndorsementCd().equals(endorsementValue) 
			).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((ischange /*&& !endo.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
						endorsmentPremium = (endo.getEndorsementPremium().getNetPremiumChange() != null
								? endorsmentPremium + endo.getEndorsementPremium().getNetPremiumChange()
								: endorsmentPremium);
					}else {
						endorsmentPremium = (endo.getEndorsementPremium().getTransactionPremium() != null
								? endorsmentPremium + endo.getEndorsementPremium().getTransactionPremium()
								: endorsmentPremium);
					}
				}
			}
		}
		
		
		if(negativeValue) {
			return endorsmentPremium * -1;
		}else {
			return endorsmentPremium;
		}
		
	}

	public Integer getEndormentLimit1Auto(String endorsementValue, SpecialtyVehicleRisk vRisk,
			SpecialityAutoSubPolicy<Risk> subPolicyAuto, boolean isLht, boolean negativeValue) {

		Integer endorsmentLimit1 = 0;

		for (Endorsement endor : vRisk.getRiskEndorsements()) {
			if (endor.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				if (!isLht) {
					endorsmentLimit1 = (endor.getLimit1amount() != null ? endorsmentLimit1 + endor.getLimit1amount()
							: endorsmentLimit1);
				}
			}
		}

		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				if (!isLht) {
					endorsmentLimit1 = (end.getDeductible1amount() != null ? endorsmentLimit1 + end.getDeductible1amount()
							: endorsmentLimit1);
				}
			}
		}
		if(negativeValue) {
			return endorsmentLimit1 * -1;
		}else {
			return endorsmentLimit1;
		}
	}

	public Integer getEndorsementDeductibleAuto(String endorsementValue, SpecialtyVehicleRisk vRisk,
			SpecialityAutoSubPolicy<Risk> subPolicyAuto, boolean isLht, boolean negativeValue) {

		Integer endorsmentDeductible = 0;
		
		

		for (Endorsement endor : vRisk.getRiskEndorsements()) {
			if (endor.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				if (!isLht) {
					endorsmentDeductible = (endor.getDeductible1amount() != null
							? endorsmentDeductible + endor.getDeductible1amount()
							: endorsmentDeductible);
				}
			}
		}

		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				if (!isLht) {
					endorsmentDeductible = (end.getDeductible1amount() != null
							? endorsmentDeductible + end.getDeductible1amount()
							: endorsmentDeductible);
				}
			}
		}
		
		if(negativeValue) {
			return endorsmentDeductible * -1;
		}else {
			return endorsmentDeductible;
		}
	}

	public String endorsementExistAuto(String endorsementValue, SpecialtyVehicleRisk vRisk,
			SpecialityAutoSubPolicy<Risk> subPolicyAuto) {

		String endorsmentExists = "N";

		for (Endorsement endor : vRisk.getRiskEndorsements()) {
			if (endor.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				endorsmentExists = "Y";
			}
		}

		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				endorsmentExists = "Y";
			}
		}

		return endorsmentExists;
	}
	
	
	
	public String coverageExistAuto(String coverageValue, SpecialtyVehicleRisk vRisk) {

		String coverageExists = "N";
		
		for (Coverage cove : vRisk.getCoverageDetail().getCoverages()) {
			if (cove.getCoverageCode().equalsIgnoreCase(coverageValue)) {
				coverageExists = "Y";
			}
		}

		return coverageExists;
	}

	public Double getEndorsementPremiumAuto(String endorsementValue, SpecialtyVehicleRisk vRisk, boolean ischange, boolean negativeValue) {

		Double endorsmentPremium = (double) 0;

		for (Endorsement endor : vRisk.getRiskEndorsements()) {
			if (endor.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				// endorsmentPremium =
				// (endor.getEndorsementPremium().getTransactionPremium()!=null?endorsmentPremium+endor.getEndorsementPremium().getTransactionPremium()*
				// vRisk.getNumberOfUnits():endorsmentPremium);
				if ((ischange /*&& !endor.getBusinessStatus().contentEquals("NOCHANGE")*/)) {
					endorsmentPremium = (endor.getEndorsementPremium().getNetPremiumChange() != null
							? endorsmentPremium + endor.getEndorsementPremium().getNetPremiumChange()
							: endorsmentPremium);
				}else {
					endorsmentPremium = (endor.getEndorsementPremium().getTransactionPremium() != null
							? endorsmentPremium + endor.getEndorsementPremium().getTransactionPremium()
							: endorsmentPremium);
				}
			}
		}

		if(negativeValue) {
			return endorsmentPremium * -1;
		}else {
			return endorsmentPremium;
		}
	}

	public Double getCoveragePremiumAuto(String coverageValue, SpecialtyVehicleRisk vRisk, boolean ischange, boolean negativeValue, boolean isAdjustment, SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) {

		Double coveragePremium = (double) 0;
		
		try {
			loadAdjustmentkData(adjustment, policyProcessing);
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Set<Coverage> coverages = null;
		if (isAdjustment) {
			coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
		}else {
			coverages = vRisk.getCoverageDetail().getCoverages();
		}
			
		if (coverages != null) {
			for (Coverage cove : coverages) {
				if (cove.getCoverageCode().equalsIgnoreCase(coverageValue)) {
					// coveragePremium =
					// (cove.getCoveragePremium().getTransactionPremium()!=null?coveragePremium+cove.getCoveragePremium().getTransactionPremium()
					// * vRisk.getNumberOfUnits():coveragePremium);
					
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}


		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}
	}
	
	public Double getCoveragePremiumAutoLHT(String coverageValue, SpecialtyVehicleRisk vRisk, boolean ischange, boolean negativeValue, boolean isAdjustment, SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) {

		Double coveragePremium = (double) 0;
		try {
			loadAdjustmentkData(adjustment, policyProcessing);
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Set<Coverage> coverages = null;
		if (isAdjustment) {
			//coverages = adjustment.getCoverageDetail().getCoverages(); //Comment this out because this is used for LHT Above 2 millions and this is always going to come from vRisk.getCoverageDetail()
			coverages = vRisk.getCoverageDetail() != null ? vRisk.getCoverageDetail().getCoverages() : null;
		}else {
			coverages = vRisk.getCoverageDetail().getCoverages();
		}
			
		if (coverages != null) {
			for (Coverage cove : coverages) {
				if (cove.getCoverageCode().equalsIgnoreCase(coverageValue)) {

					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange || isAdjustment) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
					}

					
				}
			}
		}


		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}
	}

	


	public Integer getCoverageDeductibleAuto(String coverageValue, SpecialtyVehicleRisk vRisk, boolean isLht, boolean negativeValue) {

		Integer coverageDeductible = 0;

		for (Coverage cove : vRisk.getCoverageDetail().getCoverages()) {
			if (cove.getCoverageCode().equalsIgnoreCase(coverageValue)) {
				if (!isLht) {
					coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
							: coverageDeductible);
				}
			}
		}

		if(negativeValue) {
			return coverageDeductible * -1;
		}else {
			return coverageDeductible;
		}
	}

	public String endorsementExist(PolicyTransaction pt, String endorsementValue, String subPolicyType) {

		String endorsmentExists = "N";

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Endorsement> endorsmentList = subPolicy.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				endorsmentExists = "Y";
			}
		}
		if (subPolicyCGL != null) {
			List<Endorsement> endorsmentList = subPolicyCGL.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				endorsmentExists = "Y";
			}
		}
		if (subPolicyCGO != null) {
			List<Endorsement> endorsmentList = subPolicyCGO.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				endorsmentExists = "Y";
			}
		}
		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				endorsmentExists = "Y";
			}
		}
		
		if (subPolicyType == null) {//This block is sused to get endorsment from the "Other" subpolicy
			List<Endorsement> endorsmentList = pt.getCoverageDetail().getEndorsements().stream().filter(t -> 
			t.getEndorsementCd().equals(endorsementValue) 
			).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					endorsmentExists = "Y";
				}
			}
		}

		return endorsmentExists;
	}
	
	
	public String eON141EndorsementExist(PolicyTransaction pt, String endorsementValue) {

		String endorsmentExists = "N";
		
		SpecialityAutoSubPolicy<Risk> subPolicyAuto = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();
		
		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
				subPolicyAuto = (SpecialityAutoSubPolicy<Risk>) subPol;
			} 
		}
		
		if (subPolicyAuto != null) {


			SubPolicy<Risk> subPolicyRisk = pt
					.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

			if (subPolicyRisk != null && subPolicyRisk.getSubPolicyRisks() != null) {

				Set<Risk> risks = subPolicyRisk.getSubPolicyRisks();

				// Sorting Vehicule
				List<SpecialtyVehicleRisk> vehRiskList = new ArrayList<>();
				for (Risk risk : risks) {
					SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
					vehRiskList.add(vehRisk);
				}

				List<SpecialtyVehicleRisk> vehRiskListSorted = vehRiskList.stream()
						.sorted(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence))
						.collect(Collectors.toList());


				for (SpecialtyVehicleRisk vehRisk : vehRiskListSorted) {
					for (Endorsement endor : vehRisk.getRiskEndorsements()) {
						if (endor.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
							endorsmentExists = "Y";
						}
					}

				}
			}
		}

		for (Endorsement end : subPolicyAuto.getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementCd().equalsIgnoreCase(endorsementValue)) {
				endorsmentExists = "Y";
			}
		}

		return endorsmentExists;
	}
	
	


	public Integer getEndorsementDeductible(PolicyTransaction pt, String endorsementValue, String subPolicyType, boolean isLht, boolean negativeValue) {

		Integer endorsmentDeductible = 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Endorsement> endorsmentList = subPolicy.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentDeductible = (endo.getDeductible1amount() != null
								? endorsmentDeductible + endo.getDeductible1amount()
								: endorsmentDeductible);
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			List<Endorsement> endorsmentList = subPolicyCGL.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentDeductible = (endo.getDeductible1amount() != null
								? endorsmentDeductible + endo.getDeductible1amount()
								: endorsmentDeductible);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Endorsement> endorsmentList = subPolicyCGO.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentDeductible = (endo.getDeductible1amount() != null
								? endorsmentDeductible + endo.getDeductible1amount()
								: endorsmentDeductible);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentDeductible = (endo.getDeductible1amount() != null
								? endorsmentDeductible + endo.getDeductible1amount()
								: endorsmentDeductible);
					}
				}
			}
		}
		
		if (subPolicyType == null) {//This blok is sused to get endorsment from the "Other" subpolicy
			List<Endorsement> endorsmentList = pt.getCoverageDetail().getEndorsements().stream().filter(t -> 
			t.getEndorsementCd().equals(endorsementValue) 
			).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentDeductible = (endo.getDeductible1amount() != null
								? endorsmentDeductible + endo.getDeductible1amount()
								: endorsmentDeductible);
					}
				}
			}
		}
		
		if(negativeValue) {
			return endorsmentDeductible * -1;
		}else {
			return endorsmentDeductible;
		}
	}

	public Integer getEndormentLimit1(PolicyTransaction pt, String endorsementValue, String subPolicyType, boolean isLht, boolean negativeValue) {

		Integer endorsmentLimit1 = 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Endorsement> endorsmentList = subPolicy.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentLimit1 = (endo.getLimit1amount() != null ? endorsmentLimit1 + endo.getLimit1amount()
								: endorsmentLimit1);
					}
				}

			}
		}
		if (subPolicyCGL != null) {
			List<Endorsement> endorsmentList = subPolicyCGL.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentLimit1 = (endo.getLimit1amount() != null ? endorsmentLimit1 + endo.getLimit1amount()
								: endorsmentLimit1);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Endorsement> endorsmentList = subPolicyCGO.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentLimit1 = (endo.getLimit1amount() != null ? endorsmentLimit1 + endo.getLimit1amount()
								: endorsmentLimit1);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if (!isLht) {
						endorsmentLimit1 = (endo.getLimit1amount() != null ? endorsmentLimit1 + endo.getLimit1amount()
								: endorsmentLimit1);
					}	
				}
			}
		}

		if(negativeValue) {
			return endorsmentLimit1 * -1;
		}else {
			return endorsmentLimit1;
		}
	}

	public Double getCoveragePremium(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean ischange, boolean negativeValue, boolean isadjusment) {

		Double coveragePremium = (double) 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCP != null) {
			List<Coverage> coverageList = subPolicyCP.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isadjusment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		
		
		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}

	}
	
	
	public Double getCoveragePremiumAdjustments(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean ischange, boolean negativeValue, boolean isAdjustment, SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) {

		Double coveragePremium = (double) 0;
		
		try {
			loadAdjustmentkData(adjustment, policyProcessing);
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicy.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicyCGL.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicyCGO.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicyGAR.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyCP != null) {
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicyCP.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (ischange || isAdjustment) {
						coveragePremium = (cove.getCoveragePremium().getNetPremiumChange() != null
								? coveragePremium + cove.getCoveragePremium().getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (cove.getCoveragePremium().getTransactionPremium() != null
								? coveragePremium + cove.getCoveragePremium().getTransactionPremium()
								: coveragePremium);
					}
				}
			}
		}
		
		
		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}

	}
	
	
	public Double getCoveragePremiumLHT(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean ischange, boolean negativeValue) {

		Double coveragePremium = (double) 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		if (subPolicyCP != null) {
			List<Coverage> coverageList = subPolicyCP.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}
		
		
		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}

	}
	
	
	public Double getCGLCoveragePremiumLHTReinsurance(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean ischange, boolean negativeValue, boolean isAdjustment) {

		Double coveragePremium = (double) 0;
		


		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}


		if (subPolicyCGL != null) {
			
			Set<Coverage> coverages = subPolicyCGL.getCoverageDetail().getCoverages();

			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					for (CoverageReinsurance cr : cove.getCoverageReinsurances()) {
						if (cr.getIsPrimaryInsurer() != null && cr.getIsPrimaryInsurer()) {
							Premium coveragePrem=cr.getCovReinsurancePremium();
							if (ischange || isAdjustment) {
								coveragePremium = (coveragePrem.getNetPremiumChange() != null
										? coveragePremium + coveragePrem.getNetPremiumChange()
										: coveragePremium);
							}else {
								coveragePremium = (coveragePrem.getTransactionPremium() != null
										? coveragePremium + coveragePrem.getTransactionPremium()
										: coveragePremium);
							}
						}
						
					}
				}
			}
		}

		
		
		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}

	}
	
	
	public Double getCGLCoveragePremiumLHTAdjustment(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean ischange, boolean negativeValue, boolean isAdjustment, SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) {

		Double coveragePremium = (double) 0;
		
		try {
			loadAdjustmentkData(adjustment, policyProcessing);
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}


		if (subPolicyCGL != null) {
			
			Set<Coverage> coverages = null;
			if (isAdjustment) {
				coverages = adjustment.getCoverageDetail() != null ? adjustment.getCoverageDetail().getCoverages() : null;
			}else {
				coverages = subPolicyCGL.getCoverageDetail().getCoverages();
			}
			
			List<Coverage> coverageList = coverages.stream().filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					Premium coveragePrem=cove.getCoveragePremium();
					if (ischange || isAdjustment) {
						coveragePremium = (coveragePrem.getNetPremiumChange() != null
								? coveragePremium + coveragePrem.getNetPremiumChange()
								: coveragePremium);
					}else {
						coveragePremium = (coveragePrem.getTransactionPremium() != null
								? coveragePremium + coveragePrem.getTransactionPremium()
								: coveragePremium);
					}
						
				}
			}
		}

		
		
		if(negativeValue) {
			return coveragePremium * -1;
		}else {
			return coveragePremium;
		}

	}

	public Integer getCoverageDeductible(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean isLht, boolean negativeValue) {

		Integer coverageDeductible = 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}
		if (subPolicyCP != null) {
			List<Coverage> coverageList = subPolicyCP.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}		
		
		
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageDeductible = (cove.getDeductible1() != null ? coverageDeductible + cove.getDeductible1()
								: coverageDeductible);
					}
				}
			}
		}
		
		if(negativeValue) {
			return coverageDeductible * -1;
		}else {
			return coverageDeductible;
		}
	}

	public String coverageExist(PolicyTransaction pt, String coverageCode, String subPolicyType) {

		String coverageExists = "N";

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		if (subPolicyCP != null) {
			List<Coverage> coverageList = subPolicyCP.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream().filter(t -> 
			t.getCoverageCode().equals(coverageCode) 
			).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				coverageExists = "Y";
			}
		}
		

		return coverageExists;
	}

	public Integer getCoverageLimit1(PolicyTransaction pt, String coverageCode, String subPolicyType, boolean isLht, boolean negativeValue) {

		Integer coverageLimit1 = 0;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		CommercialPropertySubPolicy<Risk> subPolicyCP = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)) {
					subPolicyCP = (CommercialPropertySubPolicy<Risk>) subPol;
				}
			}

		}

		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}

			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}
			}
		}
		
		if (subPolicyCP != null) {
			List<Coverage> coverageList = subPolicyCP.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}
			}
		}
		
		if (subPolicyType == null) {//This block is used to get Coverage from the "Other" subpolicy
			List<Coverage> coverageList = pt.getCoverageDetail().getCoverages().stream()
					.filter(t -> t.getCoverageCode().equals(coverageCode)).collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if (!isLht) {
						coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
					}
				}
			}
		}


		if(negativeValue) {
			return coverageLimit1 * -1;
		}else {
			return coverageLimit1;
		}
	}
	
	
	public Integer getAutoCoverageLimit1(SpecialtyVehicleRisk vRisk, String coverageCode, String subPolicyType, boolean isLht, boolean negativeValue) {

		Integer coverageLimit1 = 0;
		
		for (Coverage cove : vRisk.getCoverageDetail().getCoverages()) {
			if (cove.getCoverageCode().equalsIgnoreCase(coverageCode)) {
				coverageLimit1 = (cove.getLimit1() != null ? coverageLimit1 + cove.getLimit1() : coverageLimit1);
			}
		}

		if(negativeValue) {
			return coverageLimit1 * -1;
		}else {
			return coverageLimit1;
		}
	}
	
	

	public Integer getNumVehicles(PolicyTransaction pt, String coverageCode) {

		Integer numVehicles = 0;

		Set<Risk> risks = pt.getPolicyRisks();

		for (Risk risk : risks) {
			if (risk.getRiskType().equalsIgnoreCase("LO")) {
				PolicyLocation pl = (PolicyLocation) risk;
				Coverage cove = pl.getCoverageDetail().getCoverageByCode(coverageCode);
				if (cove != null) {
					numVehicles = pl.getNumVehicles();
				}

			}
		}

		return numVehicles;
	}

	public PolicyLocation getPolicyLocation(PolicyTransaction pt) {

		PolicyLocation pl = null;

		Set<Risk> risks = pt.getPolicyRisks();

		for (Risk risk : risks) {
			if (risk.getRiskType().equalsIgnoreCase("LO")) {
				pl = (PolicyLocation) risk;
			}
		}

		return pl;
	}

	public String getCodeValue(PolicyTransaction pt, String Code, String value) throws Exception {

		String name = "";

		IGenericLookupConfig lookupConfig = (new ProductConfigFactory()).getLookupConfig(
				pt.getPolicyVersion().getInsurancePolicy().getProductCd(),
				pt.getPolicyTerm().getTermEffDate().toLocalDate());
		if (lookupConfig != null) {
			LookupTableItem lookupItem = lookupConfig.getConfigLookup(value).stream()
					.filter(o -> o.getItemKey().equalsIgnoreCase(Code)).findFirst().orElse(null);
			name = lookupItem.getItemValue();
		}


		return name;
	}

	private String numberFormat(Object n) {
		if (n == null) {
			return "";
		} else {
			if (n.equals("")) {
				return "";
			} else {
				if (n == "") {
					return "";
				} else {
					// NumberFormat nf = NumberFormat.getInstance(new Locale("en", "US"));
					NumberFormat nf = new DecimalFormat("###0.00");
					String val = nf.format(n);
					return val;
				}
			}
		}

	}

	public Integer countGarageServicePlate(PolicyTransaction pt, String endorsementValue) {

		Integer garageCounter = 0;

		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
				subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
			}

		}

		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream()
					.filter(t -> t.getEndorsementCd().equals(endorsementValue)).collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement end : endorsmentList) {
					for (ExtensionType extType : end.getDataExtension().getExtensionTypes()) {
						for (ExtensionData extData : extType.getExtensionDatas()) {
							if (extData.getColumnId().equalsIgnoreCase("GSPC_PlateNo")
									&& extData.getColumnValue() != null) {
								garageCounter++;
							}

						}

					}

				}
			}
		}

		return garageCounter;
	}

	private Double getResidueDivision(Double amount, Integer divisor) {
		Double response = (double) 0;
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.FLOOR);
		Double totalSumDividedValues = (double) 0;
		// Double cien=(double) 100;
		// Integer divisor=3;
		Double amountDivided = amount / divisor;
		Double amountDividedRounded = new Double(df.format(amountDivided));
		for (int i = 0; i < divisor; i++) {
			totalSumDividedValues = totalSumDividedValues + amountDividedRounded;
		}
		response = amount - totalSumDividedValues;
		return response;

	}
	
	
	private Double divideGarageAmounts(boolean ischange, Double quantity, Integer allLocationsNumber, Integer changedLocationsNumber, Integer locationPosition, DecimalFormat df) {
		Double response=(double) 0;
		if (!ischange) { 
			
			if (locationPosition == 1) {
				Double totalResidue = getResidueDivision(quantity, allLocationsNumber);
				quantity = quantity / allLocationsNumber;
				quantity = new Double(df.format(quantity));
				response = quantity + totalResidue;
			} else {
				quantity = quantity / allLocationsNumber;
				response = new Double(df.format(quantity));
			}
		}else {
			if (locationPosition == 1) {
				Double totalResidue = getResidueDivision(quantity, changedLocationsNumber);
				quantity = quantity / changedLocationsNumber;
				quantity = new Double(df.format(quantity));
				response = quantity + totalResidue;
			} else {
				quantity = quantity / changedLocationsNumber;
				response = new Double(df.format(quantity));
			}
			
		}
		return response;

	}
	
	
	public boolean isAutoEndorAndCovChanged(PolicyTransaction pt, SpecialtyVehicleRisk vRisk, SpecialityAutoSubPolicy<Risk> subPolicyAuto) {

		boolean response = false;

		for (Endorsement endor : vRisk.getRiskEndorsements()) {
			if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endor.getBusinessStatus()!=null && !endor.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
				response = true;
			}
		}
		
		
		for (Coverage cove : vRisk.getCoverageDetail().getCoverages()) {
			if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && cove.getBusinessStatus()!=null && !cove.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
				response = true;
			}
		}
		
		return response;
	}
	
	
	public boolean isSubPolicyEndorAndCovChanged(PolicyTransaction pt, String subPolicyType) {

		boolean response = false;

		SpecialityAutoSubPolicy<Risk> subPolicy = null;
		CGLSubPolicy<Risk> subPolicyCGL = null;
		CargoSubPolicy<Risk> subPolicyCGO = null;
		GarageSubPolicy<Risk> subPolicyGAR = null;
		Set<SubPolicy<Risk>> subPolicies = pt.getSubPolicies();

		for (SubPolicy<Risk> subPol : subPolicies) {
			if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(subPolicyType)) {
				if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					subPolicy = (SpecialityAutoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					subPolicyCGL = (CGLSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					subPolicyCGO = (CargoSubPolicy<Risk>) subPol;
				} else if (subPol.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)) {
					subPolicyGAR = (GarageSubPolicy<Risk>) subPol;
				}
			}

		}

		
		//Looking in coverages
		
		if (subPolicy != null) {
			List<Coverage> coverageList = subPolicy.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && cove.getBusinessStatus()!=null && !cove.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}

			}
		}
		if (subPolicyCGL != null) {
			List<Coverage> coverageList = subPolicyCGL.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && cove.getBusinessStatus()!=null && !cove.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Coverage> coverageList = subPolicyCGO.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && cove.getBusinessStatus()!=null && !cove.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Coverage> coverageList = subPolicyGAR.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
			if (coverageList != null && !coverageList.isEmpty()) {
				for (Coverage cove : coverageList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && cove.getBusinessStatus()!=null && !cove.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}
		
		
		//looking in endorsements
		if (subPolicy != null) {
			List<Endorsement> endorsmentList = subPolicy.getCoverageDetail().getEndorsements().stream().collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endo.getBusinessStatus()!=null && !endo.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}

			}
		}
		if (subPolicyCGL != null) {
			List<Endorsement> endorsmentList = subPolicyCGL.getCoverageDetail().getEndorsements().stream().collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endo.getBusinessStatus()!=null && !endo.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}
		if (subPolicyCGO != null) {
			List<Endorsement> endorsmentList = subPolicyCGO.getCoverageDetail().getEndorsements().stream().collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endo.getBusinessStatus()!=null && !endo.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}
		if (subPolicyGAR != null) {
			List<Endorsement> endorsmentList = subPolicyGAR.getCoverageDetail().getEndorsements().stream().collect(Collectors.toList());
			if (endorsmentList != null && !endorsmentList.isEmpty()) {
				for (Endorsement endo : endorsmentList) {
					if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endo.getBusinessStatus()!=null && !endo.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}
			}
		}

		return response;
	}
	
	
	public boolean isSubPolicyOtherEndorAndCovChanged(PolicyTransaction pt) {

		boolean response = false;

		List<Endorsement> endorsmentList = pt.getCoverageDetail().getEndorsements().stream().filter(t -> 
		t.getEndorsementCd().equals("EON101") ||
		t.getEndorsementCd().equals("EON102") ||
		t.getEndorsementCd().equals("EON103") ||
		t.getEndorsementCd().equals("EON109") ||
		t.getEndorsementCd().equals("EON110") ||
		t.getEndorsementCd().equals("EON111") ||
		t.getEndorsementCd().equals("EON112") ||
		t.getEndorsementCd().equals("EON114") ||
		t.getEndorsementCd().equals("EON115") ||
		t.getEndorsementCd().equals("EON116") ||
		t.getEndorsementCd().equals("EON117") ||
		t.getEndorsementCd().equals("EON119") ||
		t.getEndorsementCd().equals("EON121") ||
		t.getEndorsementCd().equals("EON122") ||
		t.getEndorsementCd().equals("IA")
		).collect(Collectors.toList());
		if (endorsmentList != null && !endorsmentList.isEmpty()) {
			for (Endorsement endo : endorsmentList) {
				if ((pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_POLICYCHANGE)||pt.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_REVERSAL)) && endo.getBusinessStatus()!=null && !endo.getBusinessStatus().contentEquals(Constants.BUSINESS_STATUS_NOCHANGE)) {
					response = true;
				}
			}
		}

		return response;
	}
	
	
	public boolean existsSubPolicyOther(PolicyTransaction pt, boolean ischanged) {

		boolean response = false;

		List<Endorsement> endorsmentList = pt.getCoverageDetail().getEndorsements().stream().filter(t -> 
		t.getEndorsementCd().equals("EON101") ||
		t.getEndorsementCd().equals("EON102") ||
		t.getEndorsementCd().equals("EON103") ||
		t.getEndorsementCd().equals("EON109") ||
		t.getEndorsementCd().equals("EON110") ||
		t.getEndorsementCd().equals("EON111") ||
		t.getEndorsementCd().equals("EON112") ||
		t.getEndorsementCd().equals("EON114") ||
		t.getEndorsementCd().equals("EON115") ||
		t.getEndorsementCd().equals("EON116") ||
		t.getEndorsementCd().equals("EON117") ||
		t.getEndorsementCd().equals("EON119") ||
		t.getEndorsementCd().equals("EON121") ||
		t.getEndorsementCd().equals("EON122") ||
		t.getEndorsementCd().equals("IA")
		).collect(Collectors.toList());
		if (endorsmentList != null && !endorsmentList.isEmpty()) {
			for (Endorsement endo : endorsmentList) {
				if (ischanged) {
					if (!endo.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE)) {
						response = true;
					}
				}else {
					response = true;
				}
				
			}
		}

		return response;
	}
	
	
	
	public boolean locationChanged(PolicyLocation pl) {

		boolean response = false;
		
		List<Coverage> coveragesList = pl.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
		
		for (Coverage cov : coveragesList) {
			if (cov != null && !cov.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE)) {
				response = true;
			}else {
				response = false;
			}
		}

		return response;
	}
	
	
	public String locationChangeStatus(PolicyLocation pl) {

		String response = "";
		
		List<Coverage> coveragesList = pl.getCoverageDetail().getCoverages().stream().collect(Collectors.toList());
		
		for (Coverage cov : coveragesList) {
			if (cov != null && !cov.getBusinessStatus().equals(Constants.BUSINESS_STATUS_NOCHANGE)) {
				response = cov.getBusinessStatus();
				break;
			}
		}

		return response;
	}
	
	
	public Integer getOriginalNumUnitsBeforeAdjustment(SpecialtyVehicleRisk vehAfterAdjustment, PolicyTransaction vehBeforeAdjustment) {

		Integer response = null;
		
		
		
		if (vehBeforeAdjustment != null && vehAfterAdjustment!= null && vehAfterAdjustment.getSvrAdjustments() != null && vehAfterAdjustment.getSvrAdjustments().size() > 0 ) {

			SubPolicy<Risk> subPolicyRiskBeforeAdjustment = vehBeforeAdjustment
					.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);

			if (subPolicyRiskBeforeAdjustment != null && subPolicyRiskBeforeAdjustment.getSubPolicyRisks() != null) {

				Set<Risk> risksBeforeAdjustment = subPolicyRiskBeforeAdjustment.getSubPolicyRisks();

				// Sorting Vehicule
				List<SpecialtyVehicleRisk> vehRiskBeforeAdjustmentList = new ArrayList<>();
				for (Risk riskBeforeAdjustment : risksBeforeAdjustment) {
					SpecialtyVehicleRisk vehRiskBeforeAdjustment = (SpecialtyVehicleRisk) riskBeforeAdjustment;
					vehRiskBeforeAdjustmentList.add(vehRiskBeforeAdjustment);
				}

				List<SpecialtyVehicleRisk> vehRiskBeforeAdjustmentListSorted = vehRiskBeforeAdjustmentList.stream()
						.sorted(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence))
						.collect(Collectors.toList());
				
				for (SpecialtyVehicleRisk vehRiskBeforeAdjustment : vehRiskBeforeAdjustmentListSorted) {
					
					if (vehRiskBeforeAdjustment.getVehicleSequence().equals(vehAfterAdjustment.getVehicleSequence())) {
						response = vehAfterAdjustment.getNumberOfUnits() - vehRiskBeforeAdjustment.getNumberOfUnits();
					}
				}
			}
		}
		
		if (response == null) {
			response = vehAfterAdjustment.getNumberOfUnits();
		}

		return response;
	}
	
	public Double getPremiumChangeFromAdjustment(SpecialtyVehicleRiskAdjustment vehRiskAdjustment, Double Premium) {

		Double response = null;
		
		if (vehRiskAdjustment != null) {
			response = vehRiskAdjustment.getPremiumChange();
		}
						
		if (response == null) {
			response = Premium;
		}

		return response;
	}
	
	
	
	public Double getCGOPremiumChangeFromAdjustment(CargoSubPolicy<Risk> subPolicyCGO, Double Premium, SpecialtyVehicleRiskAdjustment adjustment) {

		Double response = null;
		
		response = adjustment.getPremiumChange();
	
		if (response == null) {
			response = Premium;
		}

		return response;
	}
	
	public Double getCGLPremiumChangeFromAdjustment(CGLSubPolicy<Risk> subPolicyCGL, Double Premium, SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) {

		Double response = null;
		
		response = adjustment.getPremiumChange();
	
		if (response == null) {
			response = Premium;
		}

		return response;
	}
	
	
	protected SpecialtyVehicleRiskAdjustment loadAdjustmentkData(SpecialtyVehicleRiskAdjustment adjustment, IGenericPolicyProcessing policyProcessing) throws SpecialtyAutoPolicyException {
		if (needToLoadData(adjustment.getCoverageDetail())) {
			ISpecialtyAutoPolicyProcessing saPolicyProcessing = (ISpecialtyAutoPolicyProcessing) policyProcessing;
			adjustment = saPolicyProcessing.loadSpecialtyVehicleRiskAdjustment(adjustment);
		}
		return adjustment;
	}
	
	public boolean needToLoadData(CoverageDetails coverageDetails) {
		boolean toLoad = false;
		try {
			if (coverageDetails !=null) {
				coverageDetails.getCoverages().stream().count();
				coverageDetails.getEndorsements().stream().count();
			}

		} catch (Exception e) {
			toLoad = true;
		}
		
		return toLoad;
	}

}

