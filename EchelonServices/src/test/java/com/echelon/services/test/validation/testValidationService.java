package com.echelon.services.test.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.ds.ins.domain.entities.DataExtension;
import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.entities.ExtensionType;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyMessage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.services.validation.DSValidationService;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.utils.SpecialtyAutoConstants;

public class testValidationService {
	
	@Test
	void testVR_TOW_OAP1_01() throws Exception {
		final String RULEID = "VR-TOW-OAP1-01";
		PolicyTransaction policyTransaction = new PolicyTransaction();

		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh);
		
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_OAP1_02() throws Exception {
		final String RULEID = "VR-TOW-OAP1-02";
		PolicyTransaction policyTransaction = new PolicyTransaction();

		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Premium prem = new Premium();
		veh.setRiskPremium(prem);

		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(0d);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(1d);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test 
	void testVR_TOW_CGL_01() throws Exception {
		final String RULEID = "VR-TOW-CGL-01";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
				
		CGLSubPolicy subPolicy = new CGLSubPolicy();
		subPolicy.setSubPolicyTypeCd("CGL");
		policyTransaction.addSubPolicy(subPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCglDeductible(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCglDeductible(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCglDeductible(0);
		subPolicy.setCglLimit(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCglLimit(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCglDeductible(1);
		subPolicy.setCglLimit(1);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_CGL_02() throws Exception {
		final String RULEID = "VR-TOW-CGL-02";
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setSubPolicyTypeCd("CGL");
		policyTransaction.addSubPolicy(subPolicy);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		Premium prem = new Premium();
		subPolicy.setSubPolicyPremium(prem);

		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(0d);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(1d);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}

	@Test 
	void testVR_TOW_CAR_01_and_02() throws Exception {
		final String RULEID = "VR-TOW-CAR-01";
		final String RULEID2 = "VR-TOW-CAR-02";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		CargoSubPolicy subPolicy = new CargoSubPolicy();
		subPolicy.setSubPolicyTypeCd("CGO");
		policyTransaction.addSubPolicy(subPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		Assertions.assertTrue(list.contains(RULEID2), RULEID2+" should exist");
		
		subPolicy.setCargoDeductible(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCargoDeductible(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCargoDeductible(0);
		subPolicy.setCargoLimit(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCargoLimit(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setCargoDeductible(1);
		subPolicy.setCargoLimit(1);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		CargoDetails detl = new CargoDetails();
		subPolicy.addCargoDetails(detl);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		Assertions.assertTrue(list.contains(RULEID2), RULEID2+" should exist");
		
		detl.setCargoDescription("");
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		Assertions.assertTrue(list.contains(RULEID2), RULEID2+" should exist");
		
		detl.setCargoDescription("Customer Personal Effects");
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		Assertions.assertFalse(list.contains(RULEID2), RULEID2+" should not exist");
	}
	
	@Test
	void testVR_TOW_CAR_03() throws Exception {
		final String RULEID = "VR-TOW-CAR-03";
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setSubPolicyTypeCd("CGO");
		policyTransaction.addSubPolicy(subPolicy);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		Premium prem = new Premium();
		subPolicy.setSubPolicyPremium(prem);

		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(0d);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(1d);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}

	@Test 
	void testVR_TOW_GAR_01() throws Exception {
		final String RULEID = "VR-TOW-GAR-01";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		GarageSubPolicy subPolicy = new GarageSubPolicy();
		subPolicy.setSubPolicyTypeCd("GAR");
		policyTransaction.addSubPolicy(subPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setNumFullTimeEmployees(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setNumFullTimeEmployees(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setNumFullTimeEmployees(0);
		subPolicy.setNumPartTimeEmployees(0);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setNumPartTimeEmployees(1);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.setNumFullTimeEmployees(1);
		subPolicy.setNumPartTimeEmployees(1);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_GAR_02() throws Exception {
		final String RULEID = "VR-TOW-GAR-02";
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setSubPolicyTypeCd("GAR");
		policyTransaction.addSubPolicy(subPolicy);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		Premium prem = new Premium();
		subPolicy.setSubPolicyPremium(prem);

		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(0d);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		prem.setTermPremium(1d);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}

	@Test
	void testVR_TOW_COV_01() throws Exception {
		final String RULEID = "VR-TOW-COV-01";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		autoSubPolicy.setFleetBasis("SCHD");
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		for(String fb : Arrays.asList("21AM", "21AR")) {
			autoSubPolicy.setFleetBasis(fb);
			list = runValidation(policyTransaction);
			Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
			
			Endorsement endorsement = new Endorsement();
			autoSubPolicy.getCoverageDetail().addEndorsement(endorsement);
			list = runValidation(policyTransaction);
			Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
			
			endorsement.setEndorsementCd("OPCF21A");
			list = runValidation(policyTransaction);
			Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
			
			autoSubPolicy.getCoverageDetail().removeEndorsement(endorsement);
		}
	}
	
	@Test
	void testVR_TOW_COV_02() throws Exception {
		final String RULEID = "VR-TOW-COV-02";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		autoSubPolicy.setFleetBasis("SCHD");
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		for(String fb : Arrays.asList("21B5", "21BQ", "21BS", "21BA")) {
			autoSubPolicy.setFleetBasis(fb);
			list = runValidation(policyTransaction);
			Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
			
			Endorsement endorsement = new Endorsement();
			autoSubPolicy.getCoverageDetail().addEndorsement(endorsement);
			list = runValidation(policyTransaction);
			Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
			
			endorsement.setEndorsementCd("OPCF21B");
			list = runValidation(policyTransaction);
			Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
			
			autoSubPolicy.getCoverageDetail().removeEndorsement(endorsement);
		}
	}
	
	@Test
	void testVR_TOW_COV_03() throws Exception {
		final String RULEID = "VR-TOW-COV-03";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), "VR-TOW-COV-03 should not exist");
		
		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("OPCF21A");
		Endorsement endorsementB = new Endorsement();
		endorsementB.setEndorsementCd("OPCF21B");
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		autoSubPolicy.getCoverageDetail().removeEndorsement(endorsementA);
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		autoSubPolicy.getCoverageDetail().removeEndorsement(endorsementB);
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
	}
	
	@Test
	void testVR_TOW_COV_04() throws Exception {
		final String RULEID = "VR-TOW-COV-04";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh2);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("OPCF5");
		veh2.getCoverageDetail().addEndorsement(endorsementA);
		
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
				
		veh1.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_05() throws Exception {
		final String RULEID = "VR-TOW-COV-05";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh2);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");

		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("OPCF23A");
		veh2.getCoverageDetail().addEndorsement(endorsementA);
		
		veh1.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_06() throws Exception {
		final String RULEID = "VR-TOW-COV-06";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("EON101");
		Endorsement endorsementB = new Endorsement();
		endorsementB.setEndorsementCd("OPCF5BL");
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		autoSubPolicy.getCoverageDetail().removeEndorsement(endorsementA);
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		autoSubPolicy.getCoverageDetail().removeEndorsement(endorsementB);
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_07() throws Exception {
		final String RULEID = "VR-TOW-COV-07";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("EON101");
		Endorsement endorsementC = new Endorsement();
		endorsementC.setEndorsementCd("OPCF5");
		
		veh.getCoverageDetail().addEndorsement(endorsementC);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		veh.getCoverageDetail().removeEndorsement(endorsementC);

		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh.getCoverageDetail().addEndorsement(endorsementC);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
	}
	
	@Test
	void testVR_TOW_COV_08() throws Exception {
		final String RULEID = "VR-TOW-COV-08";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		policyTransaction.addPolicyRisk(veh);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Endorsement endorsementA = new Endorsement();
		endorsementA.setEndorsementCd("OPCF23ABL");
		Endorsement endorsementB = new Endorsement();
		endorsementB.setEndorsementCd("OPCF23A");
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		autoSubPolicy.getCoverageDetail().removeEndorsement(endorsementA);
		
		veh.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		veh.getCoverageDetail().removeEndorsement(endorsementB);
		
		autoSubPolicy.getCoverageDetail().addEndorsement(endorsementA);
		veh.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
	}
	
	@Test
	void testVR_TOW_COV_09() throws Exception {
		final String RULEID = "VR-TOW-COV-09";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(new PolicyVersion());
		policyTransaction.getPolicyVersion().setInsurancePolicy(new InsurancePolicy());
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh2);
		
		Coverage cov = new Coverage();
		cov.setCoverageCode("AP");
		veh1.getCoverageDetail().addCoverage(cov);
		veh2.getCoverageDetail().addCoverage(cov);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage covCM = new Coverage();
		covCM.setCoverageCode("CM");
		Coverage covBI = new Coverage();
		covBI.setCoverageCode("BI");
		Coverage covPD = new Coverage();
		covPD.setCoverageCode("PD");
		Coverage covAB = new Coverage();
		covAB.setCoverageCode("AB");
		Coverage covUA = new Coverage();
		covUA.setCoverageCode("UA");
		Coverage covDCPD = new Coverage();
		covDCPD.setCoverageCode("DCPD");
		Coverage covTPL = new Coverage();
		covTPL.setCoverageCode("TPL");
		Coverage covCOLL = new Coverage();
		covCOLL.setCoverageCode("COLL");
		
		veh1.getCoverageDetail().addCoverage(covCM);
		veh2.getCoverageDetail().addCoverage(covCM);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covCOLL);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_10() throws Exception {
		final String RULEID = "VR-TOW-COV-10";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(new PolicyVersion());
		policyTransaction.getPolicyVersion().setInsurancePolicy(new InsurancePolicy());
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh2);
		
		Coverage cov = new Coverage();
		cov.setCoverageCode("AP");
		veh1.getCoverageDetail().addCoverage(cov);
		veh2.getCoverageDetail().addCoverage(cov);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage covCM = new Coverage();
		covCM.setCoverageCode("CM");
		Coverage covBI = new Coverage();
		covBI.setCoverageCode("BI");
		Coverage covPD = new Coverage();
		covPD.setCoverageCode("PD");
		Coverage covAB = new Coverage();
		covAB.setCoverageCode("AB");
		Coverage covUA = new Coverage();
		covUA.setCoverageCode("UA");
		Coverage covDCPD = new Coverage();
		covDCPD.setCoverageCode("DCPD");
		Coverage covTPL = new Coverage();
		covTPL.setCoverageCode("TPL");
		Coverage covSP = new Coverage();
		covSP.setCoverageCode("SP");
		Coverage covCL = new Coverage();
		covCL.setCoverageCode("CL");
		
		// CM
		veh1.getCoverageDetail().addCoverage(covCL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covCM);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		// SP
		veh2.getCoverageDetail().addCoverage(covCL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covSP);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh2.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_011() throws Exception {
		final String RULEID = "VR-TOW-COV-11";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		CGLSubPolicy subPolicy = new CGLSubPolicy();
		subPolicy.setSubPolicyTypeCd("CGL");
		policyTransaction.addSubPolicy(subPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage coverageA = new Coverage();
		coverageA.setCoverageCode("SPF6TPL");
		Endorsement endorsementB = new Endorsement();
		endorsementB.setEndorsementCd("SEF96");
		Endorsement endorsementC = new Endorsement();
		endorsementC.setEndorsementCd("SEF99");
		
		subPolicy.getCoverageDetail().addCoverage(coverageA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.getCoverageDetail().addEndorsement(endorsementC);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_012() throws Exception {
		final String RULEID = "VR-TOW-COV-12";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		
		GarageSubPolicy subPolicy = new GarageSubPolicy();
		subPolicy.setSubPolicyTypeCd("GAR");
		policyTransaction.addSubPolicy(subPolicy);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage coverageA = new Coverage();
		coverageA.setCoverageCode("SP64");
		Endorsement endorsementB = new Endorsement();
		endorsementB.setEndorsementCd("OEF71");
		
		subPolicy.getCoverageDetail().addCoverage(coverageA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		subPolicy.getCoverageDetail().addEndorsement(endorsementB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}
	
	@Test
	void testVR_TOW_COV_13() throws Exception {
		final String RULEID = "VR-TOW-COV-13";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(new PolicyVersion());
		policyTransaction.getPolicyVersion().setInsurancePolicy(new InsurancePolicy());
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh2);
		
		Coverage covAP = new Coverage();
		covAP.setCoverageCode("AP");
		veh1.getCoverageDetail().addCoverage(covAP);
		veh2.getCoverageDetail().addCoverage(covAP);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage covBI = new Coverage();
		covBI.setCoverageCode("BI");
		Coverage covPD = new Coverage();
		covPD.setCoverageCode("PD");
		Coverage covAB = new Coverage();
		covAB.setCoverageCode("AB");
		Coverage covUA = new Coverage();
		covUA.setCoverageCode("UA");
		Coverage covDCPD = new Coverage();
		covDCPD.setCoverageCode("DCPD");
		Coverage covTPL = new Coverage();
		covTPL.setCoverageCode("TPL");
		Coverage covSP = new Coverage();
		covSP.setCoverageCode("SP");
		Coverage covCL = new Coverage();
		covCL.setCoverageCode("CL");
		Coverage covCM = new Coverage();
		covCM.setCoverageCode("CM");
		
		// CM
		veh1.getCoverageDetail().getCoverages().clear();
		veh1.getCoverageDetail().addCoverage(covAP);
		veh1.getCoverageDetail().addCoverage(covCM);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		// SP
		veh1.getCoverageDetail().getCoverages().clear();
		veh1.getCoverageDetail().addCoverage(covAP);
		veh1.getCoverageDetail().addCoverage(covSP);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		// CL
		veh1.getCoverageDetail().getCoverages().clear();
		veh1.getCoverageDetail().addCoverage(covAP);
		veh1.getCoverageDetail().addCoverage(covCL);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
	}
	
	@Test
	void testVR_TOW_COV_14() throws Exception {
		final String RULEID = "VR-TOW-COV-14";
		
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(new PolicyVersion());
		policyTransaction.getPolicyVersion().setInsurancePolicy(new InsurancePolicy());
		
		SpecialityAutoSubPolicy autoSubPolicy = new SpecialityAutoSubPolicy();
		autoSubPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(autoSubPolicy);

		SpecialtyVehicleRisk veh1 = new SpecialtyVehicleRisk();
		veh1.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh1);
		
		SpecialtyVehicleRisk veh2 = new SpecialtyVehicleRisk();
		veh2.setRiskType("AU");
		autoSubPolicy.addSubPolicyRisk(veh2);
		
		Coverage cov = new Coverage();
		cov.setCoverageCode("AP");
		veh1.getCoverageDetail().addCoverage(cov);
		veh2.getCoverageDetail().addCoverage(cov);

		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		Coverage covSP = new Coverage();
		covSP.setCoverageCode("SP");
		Coverage covBI = new Coverage();
		covBI.setCoverageCode("BI");
		Coverage covPD = new Coverage();
		covPD.setCoverageCode("PD");
		Coverage covAB = new Coverage();
		covAB.setCoverageCode("AB");
		Coverage covUA = new Coverage();
		covUA.setCoverageCode("UA");
		Coverage covDCPD = new Coverage();
		covDCPD.setCoverageCode("DCPD");
		Coverage covTPL = new Coverage();
		covTPL.setCoverageCode("TPL");
		Coverage covCOLL = new Coverage();
		covCOLL.setCoverageCode("COLL");
		
		veh1.getCoverageDetail().addCoverage(covSP);
		veh2.getCoverageDetail().addCoverage(covSP);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
		
		veh1.getCoverageDetail().addCoverage(covBI);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covAB);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covUA);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covDCPD);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covTPL);
		list = runValidation(policyTransaction);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
		
		veh1.getCoverageDetail().addCoverage(covCOLL);
		list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");
	}

	@Test
	void testVR_TOW_OAP1_06() throws Exception {
		final String RULEID = "VR-TOW-OAP1-06";
		PolicyTransaction policyTransaction = new PolicyTransaction();

		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(subPolicy);

		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		veh.setVehicleDescription("V1");
		subPolicy.addSubPolicyRisk(veh);
		
		Endorsement end = new Endorsement();
		end.setEndorsementCd("OPCF27B");
		veh.addEndorsement(end);
		DataExtension dext = new DataExtension();
		end.setDataExtension(dext);
		ExtensionType extt = new ExtensionType();
		extt.setExtensionId("EXT27B");
		dext.addExtensionType(extt);
		ExtensionData extd = new ExtensionData();
		extd.setColumnId("OPCF27B_VehType");
		extd.setColumnValue("1");
		extt.addExtensionData(extd);

		veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		veh.setVehicleDescription("V2");
		subPolicy.addSubPolicyRisk(veh);
		
		end = new Endorsement();
		end.setEndorsementCd("OPCF27B");
		veh.addEndorsement(end);
		dext = new DataExtension();
		end.setDataExtension(dext);
		extt = new ExtensionType();
		extt.setExtensionId("EXT27B");
		dext.addExtensionType(extt);
		extd = new ExtensionData();
		extd.setColumnId("OPCF27B_VehType");
		extd.setColumnValue("2");
		extt.addExtensionData(extd);
		
		List<String> list = runValidation(policyTransaction);
		Assertions.assertFalse(list.contains(RULEID), RULEID+" should not exist");

		veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		veh.setVehicleDescription("V3");
		subPolicy.addSubPolicyRisk(veh);
		
		end = new Endorsement();
		end.setEndorsementCd("OPCF27B");
		veh.addEndorsement(end);
		dext = new DataExtension();
		end.setDataExtension(dext);
		extt = new ExtensionType();
		extt.setExtensionId("EXT27B");
		dext.addExtensionType(extt);
		extd = new ExtensionData();
		extd.setColumnId("OPCF27B_VehType");
		extd.setColumnValue(null);
		extt.addExtensionData(extd);
		
		List<PolicyMessage> msgs = getValidationResult(policyTransaction);
		PolicyMessage msg = msgs.stream().filter(o -> RULEID.equalsIgnoreCase(o.getRuleId())).findFirst().orElse(null);
		Assertions.assertNotNull(msg, RULEID+" should exist");
		Assertions.assertNotNull(msg.getRelatedEntities());
		Assertions.assertFalse(msg.getRelatedEntities().isEmpty());
		Assertions.assertNotNull(msg.getRelatedEntities().stream().filter(o ->
					"V3".equalsIgnoreCase(((SpecialtyVehicleRisk)o).getVehicleDescription())
				).findFirst().isPresent(),
				"V3 should fail");
	}
	
	@Test
	void test_document() throws Exception {
		final String RULEID = "VR-TOW-OAP1-06";
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(new PolicyVersion());
		policyTransaction.getPolicyVersion().setInsurancePolicy(new InsurancePolicy());

		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setSubPolicyTypeCd("AUTO");
		policyTransaction.addSubPolicy(subPolicy);

		SpecialtyVehicleRisk veh = new SpecialtyVehicleRisk();
		veh.setRiskType("AU");
		veh.setVehicleDescription("V1");
		subPolicy.addSubPolicyRisk(veh);
		
		Endorsement end = new Endorsement();
		end.setEndorsementCd("OPCF27B");
		veh.addEndorsement(end);
		DataExtension dext = new DataExtension();
		end.setDataExtension(dext);
		ExtensionType extt = new ExtensionType();
		extt.setExtensionId("EXT27B");
		dext.addExtensionType(extt);
		ExtensionData extd = new ExtensionData();
		extd.setColumnId("OPCF27B_VehType");
		extd.setColumnValue("");
		extt.addExtensionData(extd);
		
		List<String> list = runValidation(policyTransaction, SpecialtyAutoConstants.RULE_GROUP_DOCUMENT, true);
		Assertions.assertTrue(list.contains(RULEID), RULEID+" should exist");
	}
	
	private List<String> runValidation(PolicyTransaction policyTransaction) throws Exception {
		return runValidation(policyTransaction, Constants.VERSION_STATUS_QUOTED, false);
	}
	
	private List<String> runValidation(PolicyTransaction policyTransaction, String itemId, boolean isGroup) throws Exception {
		List<PolicyMessage> list = getValidationResult(policyTransaction, itemId, isGroup);
		
		List<String> results = new ArrayList<String>();
		list.stream().forEach(o -> results.add(o.getRuleId()));
		
		return results;
	}
	
	private List<PolicyMessage> getValidationResult(PolicyTransaction policyTransaction) throws Exception {
		return getValidationResult(policyTransaction, Constants.VERSION_STATUS_QUOTED, false);
	}
	
	private List<PolicyMessage> getValidationResult(PolicyTransaction policyTransaction, String itemId, boolean isGroup) throws Exception {
		List<PolicyMessage> list = (isGroup ? (new DSValidationService()).runRuleGroup(policyTransaction, itemId)
										    : (new DSValidationService()).validatePolicyTransaction(policyTransaction, itemId));
		Assertions.assertNotNull(list);
		return list;
	}
}
