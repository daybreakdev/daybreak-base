package com.echelon.dal.dao.specialitylines;

import java.util.List;

import org.hibernate.query.Query;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.echelon.domain.policy.specialtylines.VehicleRateGroupLHT;

public class VehicleRateGroupLHTDAO extends BaseHibernateDAO<VehicleRateGroupLHT, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return VehicleRateGroupLHT.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<VehicleRateGroupLHT> findBy_lowLimit_uppLimit(Double inLowLimit, Double inUppLimit) {
		List<VehicleRateGroupLHT> rv;
		try {
			startOperation();
        	session.getTransaction().begin();
			
        	rv = findBy_lowLimit_uppLimit_query(inLowLimit, inUppLimit);
        	
			// find by max of the upper limit
			if (rv == null || rv.isEmpty()) {
				rv = findBy_lowLimit_maxUppLimit(inLowLimit, inUppLimit);
			}
			
			session.getTransaction().commit();
        	//closeSession();
		} catch (RuntimeException re) {
			re.printStackTrace();
        	if (session != null && session.getTransaction() != null) {
        		session.getTransaction().rollback();
        	}
        	throw re;
        } finally {
        	closeSession();
        }
		return rv; 
	}
	
	@SuppressWarnings("unchecked")
	private List<VehicleRateGroupLHT> findBy_lowLimit_uppLimit_query(Double inLowLimit, Double inUppLimit) {
		List<VehicleRateGroupLHT> rv;
		
    	String queryString = "from VehicleRateGroupLHT as vrg where ";
    	String includeAnd = "";
		if (inLowLimit != null) {
			queryString += "vrg.lowLimitACV <= :pLowLimit ";
			includeAnd = "and";
		}
		if (inUppLimit != null) {
			queryString += includeAnd;
			queryString += " vrg.uppLimitACV >= :pUppLimit ";
			includeAnd = "and";
		}
		Query<VehicleRateGroupLHT> queryObject = session.createQuery(queryString);
		if (inLowLimit != null) {
			queryObject.setParameter("pLowLimit", inLowLimit);
		}
		if (inUppLimit != null) {
			queryObject.setParameter("pUppLimit", inUppLimit);
		}
		rv = queryObject.list();

		return rv;
	}
	
	@SuppressWarnings("unchecked")
	private List<VehicleRateGroupLHT> findBy_lowLimit_maxUppLimit(Double inLowLimit, Double inUppLimit) {
		// max limit
    	String queryString = "select max(vrg.uppLimitACV) from VehicleRateGroupLHT as vrg ";
    	Query<Double> queryObject = session.createQuery(queryString);
    	Double newUppLimit = queryObject.getSingleResult();
    	if (newUppLimit != null) {
    		if (inUppLimit != null && inUppLimit > newUppLimit) {
    			return findBy_lowLimit_uppLimit_query(newUppLimit, newUppLimit);
    		}
    	}
    	
    	return null;
	}

}
