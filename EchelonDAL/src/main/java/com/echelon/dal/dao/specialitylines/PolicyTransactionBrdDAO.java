package com.echelon.dal.dao.specialitylines;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.utils.Constants;

public class PolicyTransactionBrdDAO extends BaseHibernateDAO<PolicyTransaction<?>, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return PolicyTransaction.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<PolicyTransaction> getAllPoliciesRangeDates(LocalDate frmDate, LocalDate enDate, List<String> programCode) {
		try {
			startOperation();
			session.getTransaction().begin();
			List<PolicyTransaction> pcyTxnIdList = session
					.createQuery("FROM PolicyTransaction as pt WHERE pt.policyVersion.versionDate BETWEEN :stDate AND :edDate "
							+ " AND ( (pt.policyVersion.businessStatus IN ('" + Constants.VERSION_STATUS_ISSUED + "', '" + Constants.VERSION_STATUS_ISSUEDRENEWAL + "', '" + Constants.VERSION_STATUS_ISSUEDCHANGE + "', '" + Constants.VERSION_STATUS_ISSUEDREINSTATEMENT + "', '" + Constants.VERSION_STATUS_ISSUEDREISSUE + "', '" + Constants.VERSION_STATUS_ISSUEDADJUSTMENT + "') AND pt.transactionType = 'ONSET') OR (pt.policyVersion.businessStatus IN ('"+ Constants.VERSION_STATUS_ISSUEDCANCELLATION +"', '"+ Constants.VERSION_STATUS_ISSUEDADJUSTMENT +"') AND pt.transactionType = 'OFFSET') OR (pt.policyVersion.policyTxnType IN ('"+ Constants.VERS_TXN_TYPE_ADJUSTMENT +"')))"
							+ "AND pt.policyVersion.insurancePolicy.productCd in :product")
					
					.setParameter("stDate", frmDate).setParameter("edDate", enDate).setParameterList("product", programCode).getResultList();
			session.getTransaction().commit();
			return pcyTxnIdList;
		} catch (RuntimeException re) {
         	if (session != null && session.getTransaction() != null) {
         		session.getTransaction().rollback();
         	}
         	throw re;
        } finally {
        	closeSession();
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<PolicyTransaction> getAllPoliciesRangeDatesTemporal(LocalDateTime frmDate, LocalDateTime enDate, List<String> programCode) {
		PolicyTransaction<?> rv = null;
		try {
			startOperation();
			session.getTransaction().begin();
			List<PolicyTransaction> pcyTxnIdList = session
					.createQuery("FROM PolicyTransaction as pt WHERE pt.createdDate BETWEEN :stDate AND :edDate "
							+ "AND pt.policyVersion.insurancePolicy.productCd in :product")
					.setParameter("stDate", frmDate).setParameter("edDate", enDate).setParameterList("product", programCode).getResultList();
			session.getTransaction().commit();
			return pcyTxnIdList;
		} catch (RuntimeException re) {
         	if (session != null && session.getTransaction() != null) {
         		session.getTransaction().rollback();
         	}
         	throw re;
        } finally {
        	closeSession();
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<PolicyTransaction> getAllPolicyTransactionsReissueAdjustment(String policyNumber) {
		PolicyTransaction<?> rv = null;
		try {
			startOperation();
			session.getTransaction().begin();
			List<PolicyTransaction> pcyTxnIdList = session
					.createQuery("FROM PolicyTransaction as pt WHERE pt.policyVersion.insurancePolicy.basePolicyNum = :polNo  "
							+ " AND pt.policyVersion.policyTxnType IN ('" + Constants.VERS_TXN_TYPE_REISSUE + "','" + Constants.VERS_TXN_TYPE_ADJUSTMENT + "') "
							+ " AND pt.transactionType = ' " + Constants.TRANS_TYPE_OFFSET + "' ")
					.setParameter("polNo", policyNumber).getResultList();
			session.getTransaction().commit();
			return pcyTxnIdList;
		} catch (RuntimeException re) {
         	if (session != null && session.getTransaction() != null) {
         		session.getTransaction().rollback();
         	}
         	throw re;
        } finally {
        	closeSession();
        }
	}

}
