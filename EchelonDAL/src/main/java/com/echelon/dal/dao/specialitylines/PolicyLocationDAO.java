package com.echelon.dal.dao.specialitylines;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.echelon.domain.policy.specialtylines.PolicyLocation;

public class PolicyLocationDAO extends BaseHibernateDAO<PolicyLocation, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return PolicyLocation.class;
	}

}
