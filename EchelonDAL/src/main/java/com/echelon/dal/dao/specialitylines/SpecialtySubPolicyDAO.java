package com.echelon.dal.dao.specialitylines;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.dal.dao.SubPolicyDAO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO;

public class SpecialtySubPolicyDAO extends SubPolicyDAO {

	public SpecialtySubPolicyDAO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SpecialtyPolicyItemDTO getOriginalSvrValues(Long subpolicyPK, String coverageCode) {
		SpecialtyPolicyItemDTO dto = null;
		
		if (subpolicyPK != null && StringUtils.isNotBlank(coverageCode)) {
			try {
				startOperation();
				session.getTransaction().begin();
				String qryStr = "SELECT new com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO(sp.originatingPK, cov.numOfUnits, cov.originatingPK, cov.coveragePremium.unitPremium) "
								+ "FROM SubPolicy sp inner join sp.coverageDetail.coverages cov " 
								+ "WHERE sp.subPolicyPK = :p_subpolicyPK"
								+ "  AND cov.coverageCode = :p_coverageCode"
								;
				dto = session.createQuery(qryStr, SpecialtyPolicyItemDTO.class)
												.setParameter("p_subpolicyPK", subpolicyPK)
												.setParameter("p_coverageCode", coverageCode)
												.uniqueResult();
				
				
				if (dto != null && dto.getSubOriginatingPK() != null) {
					qryStr = "SELECT cov.numOfUnits FROM Coverage cov WHERE cov.coveragePK = :p_coveragePK";
					dto.setOriginatingUnits((Integer) session.createQuery(qryStr).setParameter("p_coveragePK", dto.getSubOriginatingPK()).uniqueResult());
				}
				
				session.getTransaction().commit();
				//closeSession();
			} catch (RuntimeException re) {
	         	if (session != null && session.getTransaction() != null) {
	         		session.getTransaction().rollback();
	         	}
	         	throw re;
	        } finally {
	        	closeSession();
	        }
		}
		
		return dto;
	}
}
