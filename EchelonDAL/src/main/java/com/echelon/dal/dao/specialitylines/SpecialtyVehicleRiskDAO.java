package com.echelon.dal.dao.specialitylines;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.dal.dao.ReinsurerDAO;
import com.ds.ins.dal.dao.RiskDAO;
import com.ds.ins.domain.dto.ReinsuranceItemDTO;
import com.ds.ins.domain.entities.Reinsurer;
import com.echelon.domain.dto.specialtylines.SpecialtyVehicleRiskDTO;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;

public class SpecialtyVehicleRiskDAO extends BaseHibernateDAO<SpecialtyVehicleRisk, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return SpecialtyVehicleRisk.class;
	}

	public Map<Long, SpecialtyVehicleRiskDTO> getSpecialtyVehicleRiskDTOMap(List<Long> pkList) {
		Map<Long, SpecialtyVehicleRiskDTO> rv = new HashMap<Long, SpecialtyVehicleRiskDTO>();
		if (pkList != null && !pkList.isEmpty()) {
			try {
				startOperation();
				session.getTransaction().begin();
				String qryStr = "SELECT new com.echelon.domain.dto.specialtylines.SpecialtyVehicleRiskDTO(sv.policyRiskPK, sv.numberOfUnits, sv.unitRate, sv.riskPremium.unitPremium) FROM SpecialtyVehicleRisk sv " + 
							"WHERE sv.policyRiskPK in :p_pkList";
				List<SpecialtyVehicleRiskDTO> dtoList = session.createQuery(qryStr, SpecialtyVehicleRiskDTO.class).setParameter("p_pkList", pkList).getResultList();
				for (SpecialtyVehicleRiskDTO dto : dtoList) {
					rv.put(dto.getPolicyRiskPK(), dto);
				}
				session.getTransaction().commit();
				//closeSession();
			} catch (RuntimeException re) {
	         	if (session != null && session.getTransaction() != null) {
	         		session.getTransaction().rollback();
	         	}
	         	throw re;
	        } finally {
	        	closeSession();
	        }
		}
		
		return rv;
	}
	
	public void loadSpecialtyVehicleRisk(SpecialtyVehicleRisk svr) {
		(new RiskDAO()).loadInstance(svr, null);
	}
	
	public ReinsuranceItemDTO getEchelonReinsuranceItemsByTxnPk(Long inPcyTxnPk, Long endLayer) {
		final String REINSURER_NAME = "Echelon Insurance";
		
		ReinsuranceItemDTO dto = new ReinsuranceItemDTO();
		
		String reinsurerName = REINSURER_NAME;
		List<Reinsurer> rinsList = (new ReinsurerDAO()).searchByName("ECHELON");
		if (rinsList != null && !rinsList.isEmpty()) {
			Reinsurer echelonReins = rinsList.get(0);
			reinsurerName = echelonReins.getDbaName();
		}

		try {
			startOperation();
			session.beginTransaction();
			String qryStr = "SELECT SUM(cov.coveragePremium.transactionPremium) "
					+ "FROM Coverage cov, Risk pr join pr.subPolicy sp join sp.policyTransaction ptx "
					+ "WHERE ptx.policyTransactionPK = :p_txnPk "
					+ "AND pr.coverageDetail.coverageDetailsPK = cov.coverageDetail.coverageDetailsPK "
					+ "AND cov.coverageCode not in ('BI', 'PD', 'AB')";
			Double riskTotal = (Double) session.createQuery(qryStr)
					.setParameter("p_txnPk", inPcyTxnPk).getSingleResult();

			qryStr = "SELECT SUM(cov.coveragePremium.transactionPremium) "
					+ "FROM Coverage cov, SubPolicy sp join sp.policyTransaction ptx "
					+ "WHERE ptx.policyTransactionPK = :p_txnPk "
					+ "AND sp.coverageDetail.coverageDetailsPK = cov.coverageDetail.coverageDetailsPK "
					+ "AND cov.coverageCode not in ('BIPD', 'BI', 'PD', 'AB')";
			Double subPolicyTotal = (Double) session.createQuery(qryStr)
					.setParameter("p_txnPk", inPcyTxnPk).getSingleResult();
			
			dto.setReinsurerName(reinsurerName);
			dto.setEndLayer(endLayer);
			dto.setNetPremium(0D);
			dto.setGrossPremium( (riskTotal != null ? riskTotal : 0D) + (subPolicyTotal != null ? subPolicyTotal : 0D) );
			dto.setGrossUpPerc(0D);
		} catch (RuntimeException re) {
		 	if (session != null && session.getTransaction() != null) {
		 		session.getTransaction().rollback();
		 	}
		 	throw re;
		} finally {
			closeSession();
		}
		return dto;
	}
}
