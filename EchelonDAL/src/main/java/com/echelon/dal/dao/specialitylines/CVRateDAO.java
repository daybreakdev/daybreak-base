package com.echelon.dal.dao.specialitylines;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.echelon.domain.policy.specialtylines.CVRate;

public class CVRateDAO extends BaseHibernateDAO<CVRate, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return CVRate.class;
	}

}
