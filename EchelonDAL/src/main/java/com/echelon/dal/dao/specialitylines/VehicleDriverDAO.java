package com.echelon.dal.dao.specialitylines;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;

public class VehicleDriverDAO extends BaseHibernateDAO<VehicleDriver, Long> {

  @SuppressWarnings("rawtypes")
  @Override
  protected Class getModelClass() {
    return VehicleDriver.class;
  }
  
  @SuppressWarnings("unchecked")
  public List<VehicleDriver> getVehicleDriversByDriver(Driver driver) {
	  List<VehicleDriver> rv = new ArrayList<VehicleDriver>();
	  if (driver != null) {
		  try {
			  startOperation();
			  session.getTransaction().begin();
			  String qryStr = "from VehicleDriver as vd where vd.driver = :pDriver";
			  Query<VehicleDriver> queryObject = session.createQuery(qryStr);
			  queryObject.setParameter("pDriver", driver);
			  rv = queryObject.list();
		  } catch (RuntimeException re) {
			  if (session != null && session.getTransaction() != null) {
				  session.getTransaction().rollback();
			  }
			  throw re;
		  } finally {
			  closeSession();
		  }
	  }
	  return rv;
  }
  
  // check for extra surcharges for same driver on an additional vehicle,
  @SuppressWarnings({ "rawtypes" })
public boolean anySameAttachedDriverVehiclesHasSurcharges(PolicyTransaction<?> policyTransaction) {
	  boolean result = false;
	  
	  try {
		  startOperation();
		  session.getTransaction().begin();
		  Query queryObject = session.getNamedQuery("get_vehicledrivers_vehicle_has_surcharges");
		  queryObject.setParameter("p_policyTransactionPK", policyTransaction.getPolicyTransactionPK());
		  List list = queryObject.list();
		  if (list != null && !list.isEmpty()) {
			  result = true;
		  }
	  } catch (RuntimeException re) {
		  if (session != null && session.getTransaction() != null) {
			  session.getTransaction().rollback();
		  }
		  throw re;
	  } finally {
		  closeSession();
	  }
	  
	  return result;
  }
}
