package com.echelon.dal.dao.specialitylines;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.query.Query;

import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.domain.policy.wheels.Driver;

public class VehicleDriverClaimDAO extends BaseHibernateDAO<VehicleDriverClaim, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return VehicleDriverClaim.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<VehicleDriverClaim> getVehicleDriverClaimsByDriver(Driver driver) {
		 List<VehicleDriverClaim> rv = new ArrayList<VehicleDriverClaim>();
		 if (driver != null) {
			 try {
				 startOperation();
				 session.getTransaction().begin();
				 String qryStr = "from VehicleDriverClaim as vdc where vdc.driver = :pDriver";
				 Query<VehicleDriverClaim> queryObject = session.createQuery(qryStr);
				 queryObject.setParameter("pDriver", driver);
				 rv = queryObject.list();
			 } catch (RuntimeException re) {
				 if (session != null && session.getTransaction() != null) {
					 session.getTransaction().rollback();
				 }
				 throw re;
			 } finally {
				 closeSession();
			 }
		 }
		 return rv;
	}

}
