package com.echelon.dal.dao.specialitylines;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;

import java.util.Map;

import org.hibernate.Hibernate;
import org.hibernate.LockMode;

import com.ds.ins.dal.dao.BaseHibernateDAO;

public class SpecialtyVehicleRiskAdjustmentDAO extends BaseHibernateDAO<SpecialtyVehicleRiskAdjustment, Long> {
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return SpecialtyVehicleRiskAdjustment.class;
	}
	
	@Override
	public void loadInstance(SpecialtyVehicleRiskAdjustment instance, Map<String, Boolean> loadParams) {
		super.loadInstance(instance, loadParams);
		try {
			startOperation();
			session.beginTransaction();
			if (!session.contains(instance)) {
				session.lock(instance, LockMode.NONE);
			}
			if (!Hibernate.isInitialized(instance.getCoverageDetail())) {
				Hibernate.initialize(instance.getCoverageDetail());
			}
			if (!Hibernate.isInitialized(instance.getCoverageDetail().getCoverages())) {
				Hibernate.initialize(instance.getCoverageDetail().getCoverages());
			}
			if (!Hibernate.isInitialized(instance.getCoverageDetail().getEndorsements())) {
				Hibernate.initialize(instance.getCoverageDetail().getEndorsements());
			}
			session.getTransaction().commit();
		} catch (RuntimeException re) {
         	if (session != null && session.getTransaction() != null) {
         		session.getTransaction().rollback();
         	}
         	throw re;
        } finally {
        	closeSession();
        }
	}
}
