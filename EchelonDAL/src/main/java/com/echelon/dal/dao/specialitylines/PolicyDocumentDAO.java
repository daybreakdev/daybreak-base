package com.echelon.dal.dao.specialitylines;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.domain.policy.PolicyDocuments;

public class PolicyDocumentDAO extends BaseHibernateDAO<PolicyDocuments, Long> {
	
	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return PolicyDocuments.class;
	}

}
