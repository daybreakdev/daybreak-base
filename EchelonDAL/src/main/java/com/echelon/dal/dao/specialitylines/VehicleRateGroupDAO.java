package com.echelon.dal.dao.specialitylines;

import java.util.List;

import org.hibernate.query.Query;

import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.dal.exceptions.DataAccessLayerException;
import com.echelon.domain.policy.specialtylines.VehicleRateGroup;

public class VehicleRateGroupDAO extends BaseHibernateDAO<VehicleRateGroup, Long> {

	@SuppressWarnings("rawtypes")
	@Override
	protected Class getModelClass() {
		return VehicleRateGroup.class;
	}
	
	@SuppressWarnings("unchecked")
	public List<VehicleRateGroup> findBy_VehAge_lowLimit_uppLimit(Integer inVehicleAge, Double inLowLimit, Double inUppLimit) {
		List<VehicleRateGroup> rv;
		if (inVehicleAge != null) {
			try {
				startOperation();
	        	session.getTransaction().begin();
				
	        	rv = findBy_VehAge_lowLimit_uppLimit_query(inVehicleAge, inLowLimit, inUppLimit);
	        	
				// find by max of the upper limit
				if (rv == null || rv.isEmpty()) {
					rv = findBy_VehAge_lowLimit_maxUppLimit(inVehicleAge, inLowLimit, inUppLimit);
				}
				
				session.getTransaction().commit();
	        	//closeSession();
			} catch (RuntimeException re) {
				re.printStackTrace();
	        	if (session != null && session.getTransaction() != null) {
	        		session.getTransaction().rollback();
	        	}
	        	throw re;
	        } finally {
	        	closeSession();
	        }
		} else {
			throw new DataAccessLayerException("The vehicel age parameter is null");
		}
		return rv; 
	}
	
	@SuppressWarnings("unchecked")
	private List<VehicleRateGroup> findBy_VehAge_lowLimit_uppLimit_query(Integer inVehicleAge, Double inLowLimit, Double inUppLimit) {
		List<VehicleRateGroup> rv;
		
    	String queryString = "from VehicleRateGroup as vrg where vrg.vehicleAge = :pVehileAge ";
		if (inLowLimit != null) {
			queryString += "and vrg.lowLimit <= :pLowLimit ";
		}
		if (inUppLimit != null) {
			queryString += "and vrg.uppLimit >= :pUppLimit ";
		}
		Query<VehicleRateGroup> queryObject = session.createQuery(queryString);
		queryObject.setParameter("pVehileAge", inVehicleAge);
		if (inLowLimit != null) {
			queryObject.setParameter("pLowLimit", inLowLimit);
		}
		if (inUppLimit != null) {
			queryObject.setParameter("pUppLimit", inUppLimit);
		}
		rv = queryObject.list();

		return rv;
	}
	
	@SuppressWarnings("unchecked")
	private List<VehicleRateGroup> findBy_VehAge_lowLimit_maxUppLimit(Integer inVehicleAge, Double inLowLimit, Double inUppLimit) {
		// max limit
    	String queryString = "select max(vrg.uppLimit) from VehicleRateGroup as vrg where vrg.vehicleAge = :pVehileAge ";
    	Query<Double> queryObject = session.createQuery(queryString);
    	queryObject.setParameter("pVehileAge", inVehicleAge);
    	Double newUppLimit = queryObject.getSingleResult();
    	if (newUppLimit != null) {
    		if (inUppLimit != null && inUppLimit > newUppLimit) {
    			return findBy_VehAge_lowLimit_uppLimit_query(inVehicleAge, newUppLimit, newUppLimit);
    		}
    	}
    	
    	return null;
	}

}
