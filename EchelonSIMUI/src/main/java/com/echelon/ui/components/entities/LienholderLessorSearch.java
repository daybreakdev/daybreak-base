package com.echelon.ui.components.entities;

import java.util.ArrayList;
import java.util.List;

import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSAddressFormat;
import com.echelon.ui.helpers.entities.LienholderLessorUIHelper;
import com.ds.ins.uicommon.components.DSToolbar;
import com.ds.ins.uicommon.components.IDSEnableShortcuts;
import com.ds.ins.domain.dto.LienholderLessorSearchResults;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;

public class LienholderLessorSearch extends BaseLayout implements IDSEnableShortcuts {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4270819249674989521L;
	private TextField				nameField;
	private TextField				cityField;
	private TextField				postalZipField;
	
	private Grid<LienholderLessorSearchResults> resultGrid;
	
	private DSToolbar				toolbar;
	
	public LienholderLessorSearch(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		initializeLayouts();
		buildButtons();
		buildFields();
		buildCriteriaForm();
		buildGrid();
	}
	
	private void initializeLayouts() {
		this.setHeightFull();
		this.setSpacing(true);
		this.setPadding(true);
	}

	private void buildButtons() {
		toolbar = new DSToolbar(JustifyContentMode.START);
		toolbar.buildButtons(new String[] {UIConstants.UIACTION_Search, UIConstants.UIACTION_Clear});
		toolbar.getButton(UIConstants.UIACTION_Search).addClickShortcut(Key.ENTER);
		toolbar.getButton(UIConstants.UIACTION_Search).addClickListener(this::searchButtonClicked);
		toolbar.getButton(UIConstants.UIACTION_Clear).addClickListener(this::clearButtonClicked);
	}

	private void buildFields() {
		// Criteria fields
		nameField 		= new TextField(labels.getString("LienLessorSearchNameLabel"));
		cityField 		= new TextField(labels.getString("LienLessorSearchCityLabel"));
		postalZipField 	= new TextField(labels.getString("LienLessorSearchPostalZipLabel"));
	}
	
	private void buildCriteriaForm() {		
		
		//nameField.setWidthFull();
		
		FormLayout criteriaForm = new FormLayout();
		criteriaForm.setWidthFull();
		criteriaForm.setResponsiveSteps(
			        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
			        );
		criteriaForm.add(nameField, 2);
		uiHelper.addFormEmptyColumn(criteriaForm);
		criteriaForm.add(cityField, postalZipField, toolbar);
		
		// Add to Criteria section
		HorizontalLayout criteriaLayout	= new HorizontalLayout();
		uiHelper.formatFullLayout(criteriaLayout);
		criteriaLayout.add(criteriaForm);
		//criteriaLayout.setVerticalComponentAlignment(Alignment.END, toolbar);
		
		this.add(criteriaLayout);
	}

	private void buildGrid() {
		resultGrid = new Grid<LienholderLessorSearchResults>();
		resultGrid.setSizeFull();

		VerticalLayout gridLayout = new VerticalLayout();
		uiHelper.formatFullLayout(gridLayout);
		gridLayout.setHeightFull();
		Label resultTitle = new Label(labels.getString("LienLessorSearchResultLabel"));
		uiHelper.setLabelHeaderStyle(resultTitle);
		gridLayout.add(resultTitle);
		
		uiHelper.setGridStyle(resultGrid);
		resultGrid.addColumn(LienholderLessorSearchResults::getCompanyName).setHeader(labels.getString("LienLessorSearchResultNameLabel"));
		resultGrid.addColumn(o -> (new DSAddressFormat().format(o.getAddressLine1(), o.getAddressCity(), o.getAddressStateProv(), o.getAddressPostalZip())))
								.setHeader(labels.getString("LienLessorSearchResultAddressLabel"));
		
		resultGrid.setSelectionMode(SelectionMode.SINGLE);
		resultGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		gridLayout.add(resultGrid);
		this.add(gridLayout);
	}
	
	private void searchButtonClicked(ClickEvent<Button> clickEvent) {
		runSearch();
	}
	
	private void runSearch() {
		if (nameField.getValue().isEmpty() && cityField.getValue().isEmpty() && postalZipField.getValue().isEmpty()) {
			uiHelper.notificationWarning(labels.getString("LienLessorSearchCriteriaEmptyError"));
			return;
		}

		if ((!nameField.getValue().isEmpty() && nameField.getValue().trim().length() < 2) || 
			(!cityField.getValue().isEmpty() && cityField.getValue().trim().length() < 2) ||
			(!postalZipField.getValue().isEmpty() && postalZipField.getValue().trim().length() < 2)) {
			uiHelper.notificationWarning(labels.getString("LienLessorSearchCriteria2CharactersError"));
			return;
		}
		
		(new LienholderLessorUIHelper()).searchLienholderLessor(nameField.getValue(), cityField.getValue(), postalZipField.getValue(), 
				new LienholderLessorUIHelper.SearchLienholderLessorCallback() {
					
					@Override
					public void resutIsReady(List<LienholderLessorSearchResults> results) {
						if (results == null || results.isEmpty()) {
							uiHelper.notificationMessage(labels.getString("LienLessorSearchResultNoResultsFoundMessage"));
						}
								
						if (results == null) {
							results = new ArrayList<LienholderLessorSearchResults>();
						}
						resultGrid.setItems(results);
					}
				});
	}
	
	private void clearButtonClicked(ClickEvent<Button> clickEvent) {
		nameField.clear();
		cityField.clear();
		postalZipField.clear();
		resultGrid.setItems(new ArrayList<LienholderLessorSearchResults>());
	}
	
	public LienholderLessorSearchResults getSelected() {
		if (resultGrid.getSelectedItems() != null && !resultGrid.getSelectedItems().isEmpty()) {
			return resultGrid.getSelectedItems().iterator().next();
		}
		
		return null;
	}

	@Override
	public void enableButtonShortcuts() {
		toolbar.setProcessEnded();
	}

	@Override
	public void disableButtonShortcuts() {
		toolbar.setProcessStarted();
	}
	
}
