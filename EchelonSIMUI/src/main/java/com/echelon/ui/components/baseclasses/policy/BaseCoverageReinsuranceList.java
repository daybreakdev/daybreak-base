package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.CoverageReinsuranceDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCovReinsuranceUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class BaseCoverageReinsuranceList extends BaseListLayout<CoverageReinsuranceDto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2450870705616052102L;
	protected boolean 						isCoverage;
	protected UIConstants.OBJECTTYPES 		parentObjectType;
	protected IDSDialog						containerDialog;
	protected FormLayout					topForm;
	protected CoverageDto 					parentCoverageDto;
	protected Grid<CoverageReinsuranceDto>	covRiGrid;
	protected TextField						coverageCodeField;
	protected DSNumberField<Integer>		totalLimitField;
	protected DSNumberField<Long> 			definedLimitField;
	protected PremiumField					totalPremiumField;
	protected Binder<CoverageDto> 			covBinder;
	protected BaseCovReinsuranceUIHelper	covReinsuranceUIHelper;

	public BaseCoverageReinsuranceList(String uiMode, UICONTAINERTYPES uiContainerType, UIConstants.OBJECTTYPES parentObjectType, boolean isCoverage) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.parentObjectType = parentObjectType;
		this.isCoverage = isCoverage;
		this.covReinsuranceUIHelper = CoverageUIFactory.getInstance().createCovReinsuranceUIHelper(parentObjectType);
		
		initializeLayouts();
		buildGrid();
	}

	protected void initializeLayouts() {
		this.setSizeFull();
		
		buildTopFormFields();
		buildTopFormLayout();
		
		String title = labels.getString(isCoverage ? "CoverageReinsuranceListTitle" : "EndorsementReinsuranceListTitle");
		DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
												title,
												UIConstants.UICOMPONENTTYPES.List,
												UIConstants.UIACTION_New, UIConstants.UIACTION_Update, UIConstants.UIACTION_Delete, UIConstants.UIACTION_Close
												);
		this.toolbar = headComponents.getToolbar();
		headComponents.getTitle().setVisible(false);
		headComponents.getHeaderLayout().removeAll();
		
		headComponents.getHeaderLayout().add(topForm, this.toolbar);
		headComponents.getHeaderLayout().setAlignSelf(Alignment.START, topForm);
		headComponents.getHeaderLayout().setAlignSelf(Alignment.END, this.toolbar);
		headComponents.getHeaderLayout().setFlexGrow(0, this.topForm);
		//Remove the max size
		this.toolbar.setSizeUndefined();
		headComponents.getHeaderLayout().setFlexGrow(1, this.toolbar);
		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupCloseButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	protected void buildTopFormFields() {
		coverageCodeField = new TextField(labels.getString(isCoverage ? "CoverageReinsuranceTopFormCoverageLabel" : "EndorsementReinsuranceTopFormCoverageLabel"));
		totalLimitField   = new DSNumberField<Integer>(labels.getString("CoverageReinsuranceTopFormTotalLimitLabel"), Integer.class);
		definedLimitField = new DSNumberField<Long>(labels.getString("CoverageReinsuranceTopFormDefinedLimitLabel"), Long.class);
		totalPremiumField = new PremiumField(labels.getString("CoverageReinsuranceTopFormTotalPremiumLabel"));
		
		covBinder = new Binder<CoverageDto>();
		covBinder.forField(coverageCodeField).bind(CoverageDto::getDescription, null);
		covBinder.forField(totalLimitField)
						.withNullRepresentation("")
						.withConverter(totalLimitField.getConverter())
						.bind(CoverageDto::getLimit1, null);
		covBinder.setReadOnly(true);
	}
	
	protected void buildTopFormLayout() {
		topForm = new FormLayout();
		
		topForm.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 5)
				);
		topForm.add(coverageCodeField, 2);
		topForm.add(totalLimitField, definedLimitField, totalPremiumField);
	}

	protected BaseCovReinsuranceUIHelper.ActionCallback createActionCallback() {
		return new BaseCovReinsuranceUIHelper.ActionCallback() {

			@Override
			public void actionComplete(String action, CoverageReinsuranceDto covReinsuranceDto) {
				if (covReinsuranceDto != null && BooleanUtils.isTrue(covReinsuranceDto.getIsPrimaryInsurer()) &&
					covReinsuranceDto.getCoverageDto() != null &&
					covReinsuranceDto.getCoverageDto().getEditIsPremiumOverrideField() != null) {
					covReinsuranceDto.getCoverageDto().getEditIsPremiumOverrideField().coverageReinsuranceValueChanged();
				}
				
				refreshList();
			}
		};
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (covRiGrid.asSingleSelect() != null && !covRiGrid.asSingleSelect().isEmpty()) {
					covReinsuranceUIHelper.onUpdateReinsurance(parentCoverageDto, covRiGrid.asSingleSelect().getValue(), getCurrentCoverageList(), this.parentObjectType, createActionCallback());
				}
			});
		}
	}

	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				covReinsuranceUIHelper.onNewReinsurance(parentCoverageDto, getCurrentCoverageList(), this.parentObjectType, createActionCallback());
			});
		}
	}

	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (covRiGrid.asSingleSelect() != null && !covRiGrid.asSingleSelect().isEmpty()) {
					covReinsuranceUIHelper.onDeleteReinsurance(parentCoverageDto, covRiGrid.asSingleSelect().getValue(), this.parentObjectType, createActionCallback());
				}
			});
		}
	}

	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (covRiGrid.asSingleSelect() != null && !covRiGrid.asSingleSelect().isEmpty()) {
					covReinsuranceUIHelper.onUndeleteReinsurance(parentCoverageDto, covRiGrid.asSingleSelect().getValue(), this.parentObjectType, createActionCallback());
				}
			});
		}
	}

	protected void setupCloseButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Close) != null) {
			if (UICONTAINERTYPES.Dialog.equals(uiContainerType)) {
				this.toolbar.getButton(UIConstants.UIACTION_Close).addClickListener(event -> {
					this.covReinsuranceUIHelper.onCloseReinsuranceList(containerDialog, this.uiMode);
				});
			}
			else {
				this.toolbar.getButton(UIConstants.UIACTION_Close).setEnabled(false);
				this.toolbar.getButton(UIConstants.UIACTION_Close).setVisible(false);
			}
		}
	}
	
	protected void buildGrid() {
		final String ISPRIMARYVALUE_TRUE  = labels.getString("CoverageReinsuranceColumnIsPrimaryTrueValue");
		final String ISPRIMARYVALUE_FALSE = labels.getString("CoverageReinsuranceColumnIsPrimaryFalseValue");
		
		covRiGrid = new Grid<CoverageReinsuranceDto>();
		covRiGrid.setSizeFull();
		uiHelper.setGridStyle(covRiGrid);
		createIndicatorColumn(covRiGrid);
		
		covRiGrid.addColumn(o -> o.getIsPrimaryInsurer() != null && o.getIsPrimaryInsurer() ? ISPRIMARYVALUE_TRUE : ISPRIMARYVALUE_FALSE).setHeader(labels.getString("CoverageReinsuranceColumnIsPrimaryLabel"))
						;
		covRiGrid.addColumn(o -> o.getPolicyReinsurer() != null && o.getPolicyReinsurer().getReinsurer() != null ? o.getPolicyReinsurer().getReinsurer().getLegalName() : "").setHeader(labels.getString("CoverageReinsuranceColumnInsurerLabel"))
						.setWidth("20%");
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getStartLayer, DSNumericFormat.getIntegerInstance())).setHeader(labels.getString("CoverageReinsuranceColumnLowerLimitLabel"))
						.setTextAlign(ColumnTextAlign.END)
						.setSortable(true)
						.setComparator(Comparator.comparing(CoverageReinsuranceDto::getStartLayer, Comparator.nullsFirst(Comparator.naturalOrder())));
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getEndLayer, DSNumericFormat.getIntegerInstance())).setHeader(labels.getString("CoverageReinsuranceColumnUpperLimitLabel"))
						.setTextAlign(ColumnTextAlign.END)
						.setSortable(true)
						.setComparator(Comparator.comparing(CoverageReinsuranceDto::getEndLayer, Comparator.nullsFirst(Comparator.naturalOrder())));
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getLayerPercentage, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnLayerPercentLabel"))
						.setTextAlign(ColumnTextAlign.END)
						;
		covRiGrid.addComponentColumn(o -> getIsPremiumOverrideColumn(o))
						.setHeader(labels.getString("CoverageReinsuranceColumnIsPremiumOverrideLabel"))
						.setTextAlign(ColumnTextAlign.END)
						.setFlexGrow(0)
						;
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getPrimaryOriginalPremium, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnRatedPremiumLabel"))
 						.setTextAlign(ColumnTextAlign.END)
 						;
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getInsurerPremium, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnInsurerPremLabel"))
						.setTextAlign(ColumnTextAlign.END)
						;
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getGrossUpPercentage, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnGrossUpPercentLabel"))
						.setTextAlign(ColumnTextAlign.END)
						;
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getGrossUpAmount, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnGrossUpAmountLabel"))
						;
		covRiGrid.addColumn(new NumberRenderer<>(CoverageReinsuranceDto::getReinsurancePremium, DSNumericFormat.getAmountInstance())).setHeader(labels.getString("CoverageReinsuranceColumnReinsurancePremLabel"))
						.setTextAlign(ColumnTextAlign.END)
						;
		
		covRiGrid.setSelectionMode(SelectionMode.SINGLE);
		covRiGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		covRiGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(covRiGrid);
				
		this.add(covRiGrid);
	}
	
	protected Checkbox getIsPremiumOverrideColumn(CoverageReinsuranceDto dto) {
		Checkbox checkbox = new Checkbox();
		
		if (dto != null && dto.getIsPremiumOverride() != null) {
			checkbox.setValue(dto.getIsPremiumOverride());
		}
		
		uiHelper.setEditFieldsReadonly(checkbox);
		
		return checkbox;
	}
	
	public void refreshList() {
		totalLimitField.setDSValue(0);
		definedLimitField.setDSValue(0);
		totalPremiumField.setDSValue(0D);
		
		List<CoverageReinsuranceDto> list = new ArrayList<CoverageReinsuranceDto>();
		if (this.parentCoverageDto != null && parentCoverageDto.getReinsuranceDtos() != null) {
			list.addAll(parentCoverageDto.getReinsuranceDtos().stream().filter(o -> o.isPendingOrActive() || o.includeThisDeleted()).collect(Collectors.toList()));
		}
		
		list.sort(Comparator.comparing(CoverageReinsuranceDto::getIsPrimaryInsurer, Comparator.nullsLast(Comparator.naturalOrder()))
				    .thenComparing(CoverageReinsuranceDto::getStartLayer, Comparator.nullsLast(Comparator.naturalOrder()))
					.thenComparing(CoverageReinsuranceDto::getEndLayer, Comparator.nullsLast(Comparator.naturalOrder()))
					.thenComparing(CoverageReinsuranceDto::getPolicyReinsurerName, Comparator.nullsLast(Comparator.naturalOrder()))
					);
		
		covRiGrid.setItems(list);
		refreshToolbar();
		
		covBinder.readBean(parentCoverageDto);
		
		definedLimitField.setDSValue(covReinsuranceUIHelper.getReinsuranceDetainedLimit(list));
		totalPremiumField.setDSValue(covReinsuranceUIHelper.getTotalReinsurancePremium(list));
		Integer policyLiabilityLimit = covReinsuranceUIHelper.getPolicyLiabilityLimit();
		totalLimitField.setDSValue(policyLiabilityLimit);
		
		uiHelper.setEditFieldsReadonly(totalLimitField, definedLimitField, totalPremiumField);
	}
	
	public boolean loadData(IDSDialog containerDialog, CoverageDto coverageDto) {
		this.containerDialog   = containerDialog;
		this.parentCoverageDto = coverageDto;
		
		refreshList();
		
		return true;
	}
	
	@Override
	protected Component createIndicator(Object object) {
		if (isIndicatorColumnRequired()) {
			CoverageReinsuranceDto dto = (CoverageReinsuranceDto) object;
			if (dto.isNew()) {
				return uiHelper.createIconNew();
			}
			else if (dto.isUpdated() || dto.isUndeleted()) {
				return uiHelper.createIconModified();
			}
			else if (dto.isDeleted() || dto.includeThisDeleted()) {
				return uiHelper.createIconDeleted();
			}
			else {
				return super.createIndicator(dto.getAuditable());
			}
		}
		
		return new Div();
	}
	
	@Override
	protected void refreshToolbar() {
		if (this.toolbar != null) {
			if (UIConstants.UIACTION_View.equalsIgnoreCase(uiMode)) {
				this.toolbar.disableAllButtons();
			}
			else {
				if (this.parentCoverageDto == null) {
					this.toolbar.disableAllButtons();
				}
				else {
					toolbar.refreshButtons((IBusinessEntity) parentCoverageDto.getCoverage(), covRiGrid);
					
					CoverageReinsuranceDto reinsuranceDto = null;
					if (covRiGrid.asSingleSelect() != null) {
						reinsuranceDto = covRiGrid.asSingleSelect().getValue();
						if (reinsuranceDto != null) {
							toolbar.refreshButtons((IBusinessEntity) parentCoverageDto.getCoverage(), reinsuranceDto.getAuditable());
							
							if (BooleanUtils.isTrue(reinsuranceDto.getIsPrimaryInsurer())) {
								if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null &&
									this.toolbar.getButton(UIConstants.UIACTION_Delete).isVisible()) {
									this.toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
								}
							}
							
							// current deleted (deleted reinsurance in current version will not be incldued in the list)
							// to hide delete and show undelete
							if (reinsuranceDto.isDeleted() || reinsuranceDto.getAuditable().isDeleted()) {
								boolean isParentRecordDeleted = false;
								boolean recordAvailable = true;
								boolean recordSelected = true;
								boolean isRecordDeleted = true;
								boolean wasRecordDeleted = false;
								toolbar.refreshButtons(isParentRecordDeleted, recordAvailable, recordSelected, isRecordDeleted, wasRecordDeleted);
							}
						}
					}
				}
				
				// Close should be enabled all the time
				if (this.toolbar.getButton(UIConstants.UIACTION_Close) != null &&
					this.toolbar.getButton(UIConstants.UIACTION_Close).isVisible()) {
					this.toolbar.getButton(UIConstants.UIACTION_Close).setEnabled(true);
				}
			}
		}
	}
	
	public boolean validEdit(IDSDialog dialog) {
		parentCoverageDto.resetValidation();
		
		boolean ok = covReinsuranceUIHelper.validateReinsurances(parentCoverageDto);
		
		if (parentCoverageDto.getEditMessages() != null && !parentCoverageDto.getEditMessages().isEmpty()) {
			if (ok) {
				uiHelper.notificationWarnings(parentCoverageDto.getEditMessages().stream().collect(Collectors.toList()));
			}
			else {
				uiHelper.notificationErrors(parentCoverageDto.getEditMessages().stream().collect(Collectors.toList()));
			}
		}
		
		return ok;
	}
	
	public boolean isDataInstanceModified() {
		return parentCoverageDto != null && parentCoverageDto.getReinsuranceDtos() != null &&
				parentCoverageDto.getReinsuranceDtos()
					.stream().filter(o -> o.isNew() || o.isUpdated() || o.isDeleted()).findFirst().isPresent();
	}
	
	@SuppressWarnings("unchecked")
	public List<CoverageReinsuranceDto> getCurrentCoverageList() {
		List<CoverageReinsuranceDto> list = (List<CoverageReinsuranceDto>) ((ListDataProvider<CoverageReinsuranceDto>)this.covRiGrid.getDataProvider()).getItems();
		if (list == null) {
			list = new ArrayList<CoverageReinsuranceDto>();
		}
		return list;
	}
}
