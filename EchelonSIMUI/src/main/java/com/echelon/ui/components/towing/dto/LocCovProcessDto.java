package com.echelon.ui.components.towing.dto;

import java.io.Serializable;
import java.util.List;

public class LocCovProcessDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9013802728366188320L;
	private List<LocCoverageDto> 	dtoList;
	private List<LocCoverageDto> 	deletedList;
	private List<LocCoverageDto> 	undeletedList;
	
	public LocCovProcessDto( 
			List<LocCoverageDto> dtoList, List<LocCoverageDto> deletedList,
			List<LocCoverageDto> undeletedList) {
		super();
		
		this.dtoList 		   = dtoList;
		this.deletedList 	   = deletedList;
		this.undeletedList 	   = undeletedList;
	}

	public List<LocCoverageDto> getDtoList() {
		return dtoList;
	}

	public void setDtoList(List<LocCoverageDto> dtoList) {
		this.dtoList = dtoList;
	}

	public List<LocCoverageDto> getDeletedList() {
		return deletedList;
	}

	public void setDeletedList(List<LocCoverageDto> deletedList) {
		this.deletedList = deletedList;
	}

	public List<LocCoverageDto> getUndeletedList() {
		return undeletedList;
	}

	public void setUndeletedList(List<LocCoverageDto> undeletedList) {
		this.undeletedList = undeletedList;
	}
	
}
