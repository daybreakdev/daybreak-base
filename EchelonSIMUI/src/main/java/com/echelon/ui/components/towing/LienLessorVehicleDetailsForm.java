package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.LienLessorVehicleDetailsUIHelper;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class LienLessorVehicleDetailsForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2428140504235916718L;
	private DSComboBox<String> 		lienLessorTypeField;
	private TextField				lienLessorNameField;
	private	Label					vehicleInfoLabel;
	private TextField				vehicleNumberField;
	private TextField 				descriptionField;
	private DSNumberField<Integer> 	vehicleYearField;
	private TextField				serialNoField;
	private DSComboBox<Double>		deductibleField;
	
	private Binder<LienholderLessorVehicleDetails>	binder;

	public LienLessorVehicleDetailsForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		//buildForm();
	}
	
	private void buildFields() {
		lienLessorTypeField 		= new DSComboBox<String>(labels.getString("LienLessorFormPayeeTypeLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_LienLessorType);
		lienLessorNameField 		= new TextField(labels.getString("LienLessorFormPayeeNameLabel"));
		
		lienLessorTypeField.setReadOnly(true);
		lienLessorNameField.setReadOnly(true);
				
		vehicleInfoLabel			= new Label(labels.getString("LienLessorVehDetailFormVehicleInfoLabel"));
		uiHelper.setLabelHeaderStyle(vehicleInfoLabel);
		vehicleNumberField			= new TextField(labels.getString("LienLessorVehDetailFormVehNumberLabel"));
		descriptionField			= new TextField(labels.getString("LienLessorVehDetailFormDescriptionLabel"));
		vehicleYearField			= new DSNumberField<Integer>(labels.getString("LienLessorVehDetailFormVehYearLabel"), DSNumberField.PRESENTFORMATS.Year);
		serialNoField				= new TextField(labels.getString("LienLessorVehDetailFormSerialNoLabel"));
		deductibleField			 	= new DSComboBox<Double>(labels.getString("LienLessorVehDetailFormDeductibleLabel"), Double.class);
				
		binder = new Binder<LienholderLessorVehicleDetails>(LienholderLessorVehicleDetails.class);
		binder.forField(vehicleNumberField)
										 .bind(LienholderLessorVehicleDetails::getVehicleNum, LienholderLessorVehicleDetails::setVehicleNum);		
		binder.forField(descriptionField).asRequired().bind(LienholderLessorVehicleDetails::getDescription, LienholderLessorVehicleDetails::setDescription);
		binder.forField(vehicleYearField).withNullRepresentation("")
										 .withConverter(vehicleYearField.getConverter())
										 .bind(LienholderLessorVehicleDetails::getVehicleYear, LienholderLessorVehicleDetails::setVehicleYear);
		binder.forField(serialNoField).asRequired().bind(LienholderLessorVehicleDetails::getSerialNum, LienholderLessorVehicleDetails::setSerialNum);
		binder.forField(deductibleField).bind(LienholderLessorVehicleDetails::getLessorDeductible, LienholderLessorVehicleDetails::setLessorDeductible);
		binder.setReadOnly(true);
	}
	
	private void bindingChanges(String lienLessorType) {
		binder.removeBinding(vehicleYearField);
		binder.removeBinding(deductibleField);
		
		if (UIConstants.LIENLESSORTYPE_HOLDER.equalsIgnoreCase(lienLessorType)) {
			binder.forField(vehicleYearField)
							.asRequired()
							.withNullRepresentation("")
							.withConverter(vehicleYearField.getConverter())
							.bind(LienholderLessorVehicleDetails::getVehicleYear, LienholderLessorVehicleDetails::setVehicleYear);
			binder.forField(deductibleField)
							.bind(LienholderLessorVehicleDetails::getLessorDeductible, LienholderLessorVehicleDetails::setLessorDeductible);
		}
		else {
			binder.forField(vehicleYearField)
							.withNullRepresentation("")
							.withConverter(vehicleYearField.getConverter())
							.bind(LienholderLessorVehicleDetails::getVehicleYear, LienholderLessorVehicleDetails::setVehicleYear);
			binder.forField(deductibleField).asRequired()
							.bind(LienholderLessorVehicleDetails::getLessorDeductible, LienholderLessorVehicleDetails::setLessorDeductible);
		}		
	}
	
	private void buildForm(String lienLessorType) {
		this.removeAll();
		
		FormLayout form = new FormLayout();
		
		if (UIConstants.LIENLESSORTYPE_HOLDER.equalsIgnoreCase(lienLessorType)) {
			form.setResponsiveSteps(
			        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 6)
			        );
			
			if (!UIConstants.UIACTION_New.equals(uiMode)) {
				form.add(lienLessorTypeField, 2);
				form.add(lienLessorNameField, 4);
				form.add(vehicleInfoLabel, 6);
			}
			
			form.add(vehicleNumberField, vehicleYearField);
			form.add(descriptionField, 2);
			form.add(serialNoField, 2);
		}
		else {
			form.setResponsiveSteps(
			        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 7)
			        );
			
			if (!UIConstants.UIACTION_New.equals(uiMode)) {
				form.add(lienLessorTypeField, 2);
				form.add(lienLessorNameField, 5);
				form.add(vehicleInfoLabel, 7);
			}
			
			form.add(vehicleNumberField, vehicleYearField);
			form.add(descriptionField, 2);
			form.add(serialNoField, 2);
			form.add(deductibleField);
		}
		
		this.add(form);
	}
	
	@Override
	protected boolean loadDataConfigs(Binder... binders) {
		boolean ok = super.loadDataConfigs(binders);
		
		(new LienLessorVehicleDetailsUIHelper()).setupDeductibleValues(deductibleField);
		
		return ok;
	}

	public void loadData(String lienLessorType, String lienLessorName, LienholderLessorVehicleDetails vehicleDetails) {
		bindingChanges(lienLessorType);
		buildForm(lienLessorType);
		
		loadDataConfigs(binder);
		lienLessorTypeField.loadDSItems();
		
		binder.readBean(vehicleDetails);
		
		uiHelper.setFieldValue(lienLessorTypeField, lienLessorType);
		uiHelper.setFieldValue(lienLessorNameField, lienLessorName);
		
		enableDataView(binder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);

		uiHelper.setEditFieldsReadonly(lienLessorTypeField, lienLessorNameField);

		setFocus();
	}
	
	public boolean saveEdit(LienholderLessorVehicleDetails vehicleDetails) {
		boolean ok1 = uiHelper.writeValues(binder, vehicleDetails);
		
		return ok1;
	}
	
	public boolean validateEdit(boolean showMessage) {
		boolean valid = uiHelper.validateValues(showMessage, binder);
		
		return valid;
	}

	public void setFocus() {
		vehicleNumberField.focus();
	}
}
