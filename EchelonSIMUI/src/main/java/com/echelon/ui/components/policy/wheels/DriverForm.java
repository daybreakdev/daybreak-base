package com.echelon.ui.components.policy.wheels;

import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverUIHelper;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class DriverForm extends BaseLayout implements IDSFocusAble {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6817099725800952208L;
	
	protected DriverUIHelper				driverUIHelper;
	protected ActionToolbar					toolbar;
	
	protected Label							nameLabel;
	/*ENFO-203 removed
	protected Label							contactLabel;
	*/
	protected Label							licenseLabel;
	protected Label							insuranceLabel;
	protected TextField						lastNameField;
	protected TextField						firstNameField;
	protected TextField						secondNameField;
	protected IntegerField					ageField;
	protected DSComboBox<String>			genderField;
	protected DatePicker					birthDateField;
	protected DSComboBox<String>			maritalStatusField;
	/*ENFO-203 removed
	protected TextField						addressField;
	protected TextField						cityField;
	protected DSComboBox<String>			provinceField;
	protected DSPostalZipCodeField			postalCodeField;
	protected TextField						countryField;
	protected TextField						phoneNumField;
	*/
	protected DSComboBox<String>			licenseProvStateField;
	protected DatePicker					licenseDateField;
	protected IntegerField 					licensedYearsField;
	protected TextField						licenseNumField;
	protected DSComboBox<String>			licenseClassField;
	protected DSComboBox<String>			licenseStatusField;
	protected DatePicker					insuredSinceField;
	protected IntegerField					drivingRecordField;
	protected DSComboBox<String>			prevInsuranceCompanyField;
	
	protected Driver 						currDriver;
	protected Binder<Driver> 				binder;
	
	public DriverForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.driverUIHelper = createDriverUIHelper();
		
		if (UIConstants.UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
														null,
														UIConstants.UICOMPONENTTYPES.Form);
			this.toolbar = (ActionToolbar) headComponents.getToolbar();
			buildButtonEvents();
		}

		buildFields();
		buildForm();
	}

	protected DriverUIHelper createDriverUIHelper() {
		return new DriverUIHelper();
	}
	
	protected DriverUIHelper getDriverUIHelper() {
		return driverUIHelper;
	}
	
	protected void buildFields() {
		nameLabel 					= new Label(labels.getString("DriverListFormNameInformationTitle"));
		uiHelper.setLabelHeaderStyle(nameLabel);
		lastNameField				= new TextField(labels.getString("DriverFormLastNameLabel"));
		firstNameField				= new TextField(labels.getString("DriverFormFirstNameLabel"));
		secondNameField				= new TextField(labels.getString("DriverFormSecondNameLabel"));
		ageField					= new IntegerField(labels.getString("DriverFormAgeLabel"));
		genderField					= new DSComboBox<String>(labels.getString("DriverFormGenderLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Genders);
		birthDateField				= new DatePicker(labels.getString("DriverFormBirthDateLabel"));
		maritalStatusField			= new DSComboBox<String>(labels.getString("DriverFormMaritalStatusLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_MaritalStatuses);
		/*ENFO-203 removed
		contactLabel 				= new Label(labels.getString("DriverListFormContactInformationTitle"));
		uiHelper.setLabelHeaderStyle(contactLabel);
		addressField				= new TextField(labels.getString("DriverFormAddressLabel"));
		cityField					= new TextField(labels.getString("DriverFormCityLabel"));
		provinceField				= new DSComboBox<String>(labels.getString("DriverFormProvinceLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		postalCodeField				= new DSPostalZipCodeField(labels.getString("DriverFormPostalCodeLabel"));
		countryField				= new TextField(labels.getString("DriverFormCountryLabel"));
		phoneNumField				= new TextField(labels.getString("DriverFormPhoneNumLabel"));
		*/
		licenseLabel 				= new Label(labels.getString("DriverListFormLicenseInformationTitle"));
		uiHelper.setLabelHeaderStyle(licenseLabel);
		licenseProvStateField		= new DSComboBox<String>(labels.getString("DriverFormLicenseProvLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		licenseDateField 			= new DatePicker(labels.getString("DriverFormDateLicensedLabel"));
		licensedYearsField  		= new IntegerField(labels.getString("DriverFormYearsLicensedLabel"));
		licenseNumField     		= new TextField(labels.getString("DriverFormLicenseNumLabel"));
		//Could not find any existing lookup table for Class or Status
		licenseClassField 			= new DSComboBox<String>(labels.getString("DriverFormLicenseClassLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_LicenseClass, false);
		licenseStatusField 			= new DSComboBox<String>(labels.getString("DriverFormLicenseStatusLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_LicenseStatus);
		insuranceLabel 				= new Label(labels.getString("DriverListFormInsuranceInformationTitle"));
		uiHelper.setLabelHeaderStyle(insuranceLabel);
		insuredSinceField 			= new DatePicker(labels.getString("DriverFormInsuredSinceLabel"));
		drivingRecordField			= new IntegerField(labels.getString("DriverFormDrivingRecordLabel"));
		prevInsuranceCompanyField	= new DSComboBox<String>(labels.getString("DriverFormPrevInsuranceCompanyLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_PreviousInsuranceCompany);
		
		binder = new Binder<Driver>(Driver.class);
		binder.forField(lastNameField).asRequired().bind(Driver::getFamilyName, Driver::setFamilyName);
		binder.forField(firstNameField).asRequired().bind(Driver::getGivenName, Driver::setGivenName);
		binder.forField(secondNameField).bind(Driver::getMiddleName, Driver::setMiddleName);
		binder.forField(genderField).asRequired().bind(Driver::getGender, Driver::setGender);
		binder.forField(birthDateField).asRequired().bind(Driver::getBirthDate, Driver::setBirthDate);
		binder.forField(maritalStatusField).bind(Driver::getMaritalStatus, Driver::setMaritalStatus);
		/*ENFO-203 removed
		binder.forField(addressField).asRequired()
				.withNullRepresentation("")
				.bind(driver -> driver.getDriverAddress().getAddressLine1(), (driver, address) -> driver.getDriverAddress().setAddressLine1(address));
		binder.forField(cityField).asRequired()
				.withNullRepresentation("")
				.bind(driver -> driver.getDriverAddress().getCity(), (driver, city) -> driver.getDriverAddress().setCity(city));
		binder.forField(provinceField).asRequired()
				.withNullRepresentation("")
				.bind(driver -> driver.getDriverAddress().getProvState(), (driver, provState) -> driver.getDriverAddress().setProvState(provState));
		binder.forField(postalCodeField)
				.withNullRepresentation("")
				.withConverter(postalCodeField.getConverter())
				.withValidator(postalCodeField.getValidator())
				.bind(driver -> driver.getDriverAddress().getPostalZip(), (driver, postalZip) -> driver.getDriverAddress().setPostalZip(postalZip));
		binder.forField(countryField).asRequired()
				.withNullRepresentation("")
				.bind(driver -> driver.getDriverAddress().getCountry(), (driver, country) -> driver.getDriverAddress().setCountry(country));
		binder.forField(phoneNumField).bind(driver -> driver.getHomePhone(), (driver, phoneNum) -> driver.setHomePhone(phoneNum));
		*/
		binder.forField(licenseProvStateField).asRequired().bind(Driver::getLicenseProvState, Driver::setLicenseProvState);
		binder.forField(licenseDateField).asRequired().bind(Driver::getDateLicensed, Driver::setDateLicensed);
		binder.forField(licenseNumField).asRequired().bind(Driver::getLicenseNumber, Driver::setLicenseNumber);
		binder.forField(licenseClassField).bind(Driver::getLicenseClass, Driver::setLicenseClass);
		binder.forField(licenseStatusField).bind(Driver::getLicenseStatus, Driver::setLicenseStatus);
		binder.forField(insuredSinceField).bind(Driver::getInsuredSince, Driver::setInsuredSince);
		binder.forField(drivingRecordField).bind(Driver::getDrivingRecord, Driver::setDrivingRecord);
		binder.forField(prevInsuranceCompanyField).bind(Driver::getPreviousInsuranceCompany, Driver::setPreviousInsuranceCompany);
		
		uiHelper.setEditFieldsReadonly(insuredSinceField);

		binder.setReadOnly(true);
		
		uiHelper.setEditFieldsReadonly(licensedYearsField, ageField);
	}
	
	protected void buildForm() {
		FormLayout nameForm = buildNameForm(); 
		/*ENFO-203 removed
		FormLayout contactForm = buildContactForm();
		*/ 
		FormLayout licenseForm = buildLicenseForm();
		FormLayout insuranceForm = buildInsuranceForm();

		/*ENFO-203 removed
		nameForm.add(contactForm, 4);
		*/ 
		nameForm.add(licenseForm, 4);
		nameForm.add(insuranceForm, 4);
		this.add(nameForm);
	}
	
	protected FormLayout buildNameForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );

		form.add(nameLabel, 4);
		form.add(lastNameField, firstNameField, secondNameField, genderField);
		form.add(birthDateField, ageField, maritalStatusField);		
		return form;
	}
	
	/*ENFO-203 removed
	protected FormLayout buildContactForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		
		form.add(contactLabel, 4);
		form.add(addressField, 2);
		form.add(cityField, provinceField);
		form.add(postalCodeField, countryField, phoneNumField);
		return form;

	}
	*/		
	
	protected FormLayout buildLicenseForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		
		form.add(licenseLabel, 4);
		form.add(licenseProvStateField, licenseDateField, licensedYearsField);
		form.add(licenseNumField, 2);
		form.add(licenseClassField, licenseStatusField);
		return form;
	}
	
	protected FormLayout buildInsuranceForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		
		form.add(insuranceLabel, 4);
		form.add(insuredSinceField, drivingRecordField);
		form.add(prevInsuranceCompanyField, 2);
		return form;
	}
	
	private void yearsLicensedValueChanged(ValueChangeEvent<?> event) {
		licensedYearsField.setValue(driverUIHelper.getYearsLicensed(licenseDateField.getValue()));	
	}

	private void birthDateValueChanged(ValueChangeEvent<?> event) {
		ageField.setValue(driverUIHelper.getDriverAge(birthDateField.getValue()));	
	}
	
	public void loadData(Driver driver) {
		loadDataConfigs(binder);
		
		this.currDriver = driver;
			
		binder.readBean(driver);
		
		if (driver.getDateLicensed() != null) {
			licensedYearsField.setValue(driverUIHelper.getYearsLicensed(licenseDateField.getValue()));	
		}
		
		if (driver.getBirthDate() != null) {
			ageField.setValue(driverUIHelper.getDriverAge(birthDateField.getValue()));	
		}
		
		enableDataView(binder);
		
		refreshToolbar(driver);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		setupFieldValueChanges();
		setFocus();
	}
	
	public boolean saveEdit(Driver driver) {
		boolean ok1 = uiHelper.writeValues(binder, driver);
				
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}
	
	private void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
						getDriverUIHelper().updateDriverAction(currDriver);
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(e -> {
						getDriverUIHelper().deleteDriverAction(currDriver);
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(e -> {
						getDriverUIHelper().undeleteDriverAction(currDriver);
				});
			}
		}
	}
	
	private void refreshToolbar(Driver driver) {
		if (toolbar != null) {
			toolbar.refreshButtons(null, driver);
			
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
			}
		}
	}

	public void setFocus() {
		lastNameField.focus();
	}
	
	protected void setupFieldValueChanges() {
		licenseDateField.addValueChangeListener(this::yearsLicensedValueChanged);
		birthDateField.addValueChangeListener(this::birthDateValueChanged);
	}
}
