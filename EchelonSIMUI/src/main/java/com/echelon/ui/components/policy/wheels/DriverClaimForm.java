package com.echelon.ui.components.policy.wheels;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverClaimUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverClaim;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

public class DriverClaimForm extends BaseLayout implements IDSFocusAble {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6788110682390153574L;
	protected DatePicker					lossDateField;
	protected TextField						vehicleField;
	protected DSComboBox<String>			claimTypeField;
	protected DSComboBox<String>			faultPctField;
	protected NumberField					totalPaidField;
	protected IntegerField					claimNumField;
	protected Checkbox						echelonClaimField;
	
	protected DriverClaimUIHelper			claimUIHelper;
	protected Binder<DriverClaim>			binder;

	public DriverClaimForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.claimUIHelper = new DriverClaimUIHelper();
		this.setSizeFull();
		
		buildFields();
		buildForm();
	}
	

	protected void buildFields() {
		lossDateField					= new DatePicker(labels.getString("DriverClaimFormLossDateLabel"));
		vehicleField					= new TextField(labels.getString("DriverClaimFormVehicleLabel"));
		claimTypeField					= new DSComboBox<String>(labels.getString("DriverClaimFormClaimTypeLabel"), String.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_ClaimType);
		faultPctField					= new DSComboBox<String>(labels.getString("DriverClaimFormFaultPctLabel"), String.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_ClaimFault);
		totalPaidField		 			= new NumberField(labels.getString("DriverClaimFormTotalPaidLabel"));
		claimNumField 					= new IntegerField(labels.getString("DriverClaimFormClaimNumLabel"));
		echelonClaimField				= new Checkbox(labels.getString("DriverClaimFormEchelonCheckboxLabel"));

		binder = new Binder<DriverClaim>(DriverClaim.class);
		binder.forField(lossDateField).asRequired().bind(DriverClaim::getLossDate, DriverClaim::setLossDate);
		binder.forField(vehicleField).asRequired().bind(DriverClaim::getVehicle, DriverClaim::setVehicle);
		binder.forField(claimTypeField).asRequired().bind(DriverClaim::getClaimType, DriverClaim::setClaimType);
		binder.forField(faultPctField).bind(DriverClaim::getFaultResponsibility, DriverClaim::setFaultResponsibility);
		binder.forField(totalPaidField).bind(DriverClaim::getTotalPaid, DriverClaim::setTotalPaid);
		binder.forField(claimNumField).bind(DriverClaim::getClaimNum, DriverClaim::setClaimNum);
		binder.forField(echelonClaimField).bind(DriverClaim::getIsInsurerClaim, DriverClaim::setIsInsurerClaim);
		
		binder.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout claimForm = buildClaimForm();
		this.add(claimForm);
	}
	
	protected FormLayout buildClaimForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );
		
		form.add(lossDateField, vehicleField, claimTypeField);
		form.add(faultPctField, 1);
		form.add(totalPaidField, 2);
		form.add(claimNumField, echelonClaimField);
		
		return form;
	}
		
	public void loadData(Driver driver, DriverClaim claim) {
		loadDataConfigs(binder);
				
		binder.readBean(claim);
		
		enableDataView(binder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		setFocus();
	}
	
	public boolean saveEdit(DriverClaim claim) {
		boolean ok1 = uiHelper.writeValues(binder, claim);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

}