package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.utils.Constants;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseTransactionHistoryForm;
import com.echelon.ui.components.baseclasses.process.policy.BaseTransactionHistoryProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;

public class BaseTransactionHistoryUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8129658387624105849L;
	
	public interface LoadPolicyTransactionHistoriesCallback {
		public void historiesIsReady(List<PolicyHistoryDTO> transactionHistores);
	}

	public BaseTransactionHistoryUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected BaseTransactionHistoryForm createTransactionHistoryForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new BaseTransactionHistoryForm(action, containerType);
	}
	
	protected DSUpdateDialog<BaseTransactionHistoryForm> createTransactionHistoryDialog(String title, String action) {
		DSUpdateDialog<BaseTransactionHistoryForm> dialog = (DSUpdateDialog<BaseTransactionHistoryForm>) createDialog(title, 
																createTransactionHistoryForm(action, UIConstants.UICONTAINERTYPES.Dialog), 
																action);
		dialog.setWidth("800px");
		return dialog;
	}
	
	public void loadPolicyTransactionHistories(LoadPolicyTransactionHistoriesCallback callback) {
		if (policyUIHelper.getCurrentPolicy() != null && 
			policyUIHelper.getCurrentPolicy().getTransactionHistores() != null) {
			callback.historiesIsReady(policyUIHelper.getCurrentPolicy().getTransactionHistores());
			return;
		}
		
		String  policyNumber = policyUIHelper.getCurrentPolicyVersion().getInsurancePolicy().getBasePolicyNum();
		String  quoteNumber  = policyUIHelper.getCurrentPolicyVersion().getInsurancePolicy().getQuoteNum();
		Integer termNumber	 = policyUIHelper.getCurrentPolicyVersion().getVersionTerm().getTermNumber();
		
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BaseTransactionHistoryProcessor processor = (BaseTransactionHistoryProcessor)taskRun.getProcessor();
					List<PolicyHistoryDTO> dtos = processor.getTransactionHistores();
					if (dtos != null && !dtos.isEmpty()) {
						List<LookupTableItem> lkItems = lookupUIHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_CancellationReasons);
						dtos.stream().filter(o -> Constants.VERS_TXN_TYPE_CANCELLATION.equalsIgnoreCase(o.getPolicyTxnType())).collect(Collectors.toList())
							.stream().forEach(o -> {
								LookupTableItem lkitem = lookupUIHelper.findLookupItemByKey(o.getVersionDescription(), lkItems, true);
								if (lkitem != null) {
									o.setVersionDescription(lkitem.getItemValue());
								}
							});
					}
					
					policyUIHelper.getCurrentPolicy().setTransactionHistores(dtos);
					callback.historiesIsReady(dtos);
				}
				else {
					policyUIHelper.getCurrentPolicy().setTransactionHistores(null);
					callback.historiesIsReady(null);
				}
				
				return true;
			}
			
		})
		.start(createPolicyDetailHistoryProcessor(policyNumber, quoteNumber, termNumber));
	}
	
	public void openPolicy(PolicyHistoryDTO dto) {
		InsurancePolicy insurancePolicy = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy();
		
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				BaseTransactionHistoryProcessor processor = (BaseTransactionHistoryProcessor) taskRun.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					if (processor.getPolicyTransaction() != null) {
						policyUIHelper.openPolicy(processor.getPolicyTransaction());
					}
				}
				return true;
			}
			
		})
		.start(createSelectPolicyProcessor(insurancePolicy.getProductCd(), 
						insurancePolicy.getBasePolicyNum(), insurancePolicy.getQuoteNum(), 
						dto.getTermNumber(), dto.getPolicyVersionNum()));
	}
	
	public void setupPolicyUpdateAction(PolicyHistoryDTO dto) {
		String title = uiHelper.getSessionLabels().getString("TransactionHistoryVerDescriptionUpdateTitle");
		DSUpdateDialog<BaseTransactionHistoryForm> dialog = createTransactionHistoryDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(dto));
		dialog.getContentComponent().loadData(dto);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<BaseTransactionHistoryForm> dialog = (DSUpdateDialog<BaseTransactionHistoryForm>) event.getSource();
		PolicyHistoryDTO dto = (PolicyHistoryDTO) dialog.getDataStore().getData1();
		
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(dto)) {
			(new TaskProcessor(UI.getCurrent(), dialog) {

				@Override
				protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
					if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
						BaseTransactionHistoryProcessor processor = (BaseTransactionHistoryProcessor)taskRun.getProcessor();
						dialog.close();
						reOpen(processor.getPolicyTransaction(), processor.getTransactionHistores());
					}
					
					return true;
				}
				
			})
			.start(createSavePolicyProcessor(dto));
		}
	}
	
	protected void reOpen(PolicyTransaction<?> policyTransaction, List<PolicyHistoryDTO> transactionHistores) {
		if (policyTransaction != null) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.TransactionHistory, transactionHistores);			
		}
		else {
			policyUIHelper.reopenPolicy(BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(), 
										POLICYTREENODES.TransactionHistory, transactionHistores);
		}
	}

	protected BaseTransactionHistoryProcessor createPolicyDetailHistoryProcessor(String policyNumber, String quoteNumber, Integer termNumber) {
		return new BaseTransactionHistoryProcessor(UIConstants.UIACTION_History, policyUIHelper.getCurrentPolicyTransaction(),
								policyNumber, quoteNumber, termNumber);
	}

	protected BaseTransactionHistoryProcessor createSelectPolicyProcessor(String productCd, String policyNumber, String quoteNumber, Integer termNumber, Integer versinNumber) {
		return new BaseTransactionHistoryProcessor(UIConstants.UIACTION_Open, policyUIHelper.getCurrentPolicyTransaction(),
								policyNumber, quoteNumber, termNumber, versinNumber);
	}

	protected BaseTransactionHistoryProcessor createSavePolicyProcessor(PolicyHistoryDTO dto) {
		return new BaseTransactionHistoryProcessor(UIConstants.UIACTION_Save, dto);
	}

}
