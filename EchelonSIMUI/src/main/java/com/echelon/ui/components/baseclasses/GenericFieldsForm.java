package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSGenericFieldsForm;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSGenericFieldGroupDto;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.vaadin.flow.component.formlayout.FormLayout;

public class GenericFieldsForm extends DSGenericFieldsForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3487303107828448426L;
	protected CoverageDto coverageDto;

	public GenericFieldsForm(String uiMode, UICONTAINERTYPES uiContainerType, DSGenericFieldGroupDto valueGroupDto,
			FormLayout followFormLayout, CoverageDto coverageDto) {
		super(uiMode, uiContainerType, valueGroupDto, followFormLayout);
		// TODO Auto-generated constructor stub
		
		this.coverageDto = coverageDto;
	}

}
