package com.echelon.ui.components.policy;

import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;

public interface INewPolicySubpolicyForm {

	public boolean 	loadData(NewPolicyDto newPolicyDto);
	public boolean 	enableEdit();
	public boolean 	saveEdit(NewPolicyDto newPolicyDto);	
}
