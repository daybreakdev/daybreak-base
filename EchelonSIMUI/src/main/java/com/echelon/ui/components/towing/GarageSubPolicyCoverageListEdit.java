package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageListEdit;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class GarageSubPolicyCoverageListEdit extends BaseCoverageListEdit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6698814744246576937L;

	public GarageSubPolicyCoverageListEdit(String uiMode, UICONTAINERTYPES uiContainerType,
			OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void enableEdit(CoverageDto dto) {
		// TODO Auto-generated method stub
		super.enableEdit(dto);
		
		if (SpecialtyAutoConstants.GARAGE_LOC_COVERAGES.contains(dto.getCoverageCode())) {
			dto.getEditLimitField().setEnabled(false);
			dto.getEditDeductibleField().setEnabled(false);
			dto.getEditPremiumField().setEnabled(false);
			dto.getEditIsPremiumOverrideField().setEnabled(false);
			dto.getEditUwAdjustmentField().setEnabled(false);
			
			// Do not change any value, so it keep unchange for undelete
			dto.getEditBinder().readBean(dto);
		}
	}
}
