package com.echelon.ui.components.baseclasses.policy;

import java.util.Arrays;
import java.util.List;

import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseRatefactorUIHelper;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.RateFactor;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.provider.SortDirection;

public class BaseRatefactorList extends BaseListLayout<RateFactor> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6566109923525880778L;

	protected Grid<RateFactor>			ratefactorGrid;
	protected BaseRatefactorUIHelper	ratefactorUIHelper;
	
	public BaseRatefactorList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		ratefactorUIHelper = UIFactory.getInstance().getRatefactorUIHelper(uiConfigHelper.getProductCode());
		
		initializeLayouts();
		buildGrid();
	}

	protected void initializeLayouts() {
		this.setSizeFull();
		
		if (!UICONTAINERTYPES.Dialog.equals(uiContainerType)) {
			String title = labels.getString("RatefactorListTitle");
			DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
															title,
															null);
		}
	}
	
	protected void buildGrid() {
		ratefactorGrid = new Grid<RateFactor>();
		uiHelper.setGridStyle(ratefactorGrid);
		ratefactorGrid.setSizeFull();
		
		Grid.Column col1 = ratefactorGrid.addColumn(RateFactor::getRateFactorId)
						.setResizable(true)
						.setSortable(true)
						.setAutoWidth(true)
						.setHeader(labels.getString("RatefactorListIdHeader"));
		
		ratefactorGrid.addColumn(RateFactor::getRateFactorStringValue)
						.setResizable(true)
						.setHeader(labels.getString("RatefactorListStringValueHeader"));
		
		ratefactorGrid.addColumn(RateFactor::getRateFactorNumericValue)
						.setResizable(true)
						.setHeader(labels.getString("RatefactorListNumericValueHeader"))
						.setTextAlign(ColumnTextAlign.END);
		
		
		ratefactorGrid.setSelectionMode(SelectionMode.SINGLE);
		ratefactorGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		//ratefactorGrid.setHeightByRows(true);
		//ratefactorGrid.setMinHeight("600px");
		
		GridSortOrder<RateFactor> order = new GridSortOrder<>(col1, SortDirection.ASCENDING);
		ratefactorGrid.sort(Arrays.asList(order));

		this.add(ratefactorGrid);
	}
	
	public boolean loadData(Object ratefactorParent) {
		List<RateFactor> list = ratefactorUIHelper.getRateFactors(ratefactorParent);
		
		ratefactorGrid.setItems(list);
		
		return true;
	}

	public Grid<RateFactor> getRatefactorGrid() {
		return ratefactorGrid;
	}
	
}
