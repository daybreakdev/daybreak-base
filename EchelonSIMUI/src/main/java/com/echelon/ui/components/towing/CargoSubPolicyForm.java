package com.echelon.ui.components.towing;

import java.util.Comparator;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class CargoSubPolicyForm extends BaseLayout implements IDSFocusAble {
	private static final long serialVersionUID = 5455536497913082386L;
	private DSComboBox<Integer> cargoLimitField;
	private DSComboBox<Integer> cargoDeductibleField;
	private Binder<CargoSubPolicy> binder;

	public CargoSubPolicyForm(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		buildFields();
		buildForm();
	}
	
	public void loadData(CargoSubPolicy<?> cargoSubPolicy) {
		loadDataConfigs(binder);

		binder.readBean(cargoSubPolicy);

		enableDataView(binder);
	}

	public void loadValues() {
		loadDataConfigs(binder);

		// If the fields only have one option, default to it
		if (cargoLimitField.getDSItemCount() == 1) {
			cargoLimitField.setDSFirstValue();
		}

		if (cargoDeductibleField.getDSItemCount() == 1) {
			cargoDeductibleField.setDSFirstValue();
		}

		enableDataView(binder);
	}

	private void buildFields() {
		cargoLimitField = new DSComboBox<Integer>(labels.getString("CargoSubPolicyCargoLimitLabel"), Integer.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_CargoLimitList);

		cargoDeductibleField = new DSComboBox<Integer>(labels.getString("CargoSubPolicyCargoDeductible"), Integer.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_CargoDeductLimitList);

		binder = new Binder<CargoSubPolicy>(CargoSubPolicy.class);
		binder.forField(cargoLimitField).asRequired().bind(CargoSubPolicy::getCargoLimit,
				CargoSubPolicy::setCargoLimit);
		binder.forField(cargoDeductibleField).asRequired().bind(CargoSubPolicy::getCargoDeductible,
				CargoSubPolicy::setCargoDeductible);
	}

	private void buildForm() {
		FormLayout form = new FormLayout();
		form.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2));

		form.add(cargoLimitField);
		form.add(cargoDeductibleField);

		this.add(form);
	}

	public boolean validate() {
		return uiHelper.validateValues(binder);
	}

	public boolean save(CargoSubPolicy<?> subPolicy) {
		return uiHelper.writeValues(binder, subPolicy);
	}

	public void enableEdit() {
		enableDataEdit(binder);

		uiLookupHelper.formatLookupIntegerListEdit(cargoLimitField.getDSLookupItems());
		cargoLimitField.getDataProvider().refreshAll();
		cargoLimitField.setDSValue(cargoLimitField.getValue());
		uiLookupHelper.formatLookupIntegerListEdit(cargoDeductibleField.getDSLookupItems());
		cargoDeductibleField.getDataProvider().refreshAll();
		cargoDeductibleField.setDSValue(cargoDeductibleField.getValue());

		// By default, items are sorted as Strings (1 < 10 < 2); we need to sort as
		// numbers, so that 1 < 2 <10
		cargoLimitField.getDSItems().sort(Comparator.comparingInt(item -> Integer.parseInt(item.toString())));
		cargoDeductibleField.getDSItems().sort(Comparator.comparingInt(item -> Integer.parseInt(item.toString())));

		setFocus();
	}

	@Override
	public void setFocus() {
		cargoLimitField.focus();
	}
}
