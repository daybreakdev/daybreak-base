package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.echelon.ui.constants.UIConstants;

public class BaseTransactionHistoryProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1274741910598046507L;
	private String 					policyNumber;
	private String 					quoteNumber;
	private Integer 				termNumber;
	private Integer 				versionNumber;
	private PolicyHistoryDTO		policyHistoryDTO;
	
	private List<PolicyHistoryDTO> transactionHistores;

	public BaseTransactionHistoryProcessor(String processAction, PolicyTransaction policyTransaction, 
			String policyNumber, String quoteNumber, Integer termNumber) {
		super(processAction, policyTransaction);
		this.policyNumber = policyNumber;
		this.quoteNumber = quoteNumber;
		this.termNumber = termNumber;
	}

	public BaseTransactionHistoryProcessor(String processAction, PolicyTransaction policyTransaction, 
			String policyNumber, String quoteNumber, Integer termNumber, Integer versionNumber) {
		super(processAction, policyTransaction);
		this.policyNumber = policyNumber;
		this.quoteNumber = quoteNumber;
		this.termNumber = termNumber;
		this.versionNumber = versionNumber;
	}

	public BaseTransactionHistoryProcessor(String processAction, PolicyHistoryDTO policyHistoryDTO) {
		super(processAction, null);
		this.policyHistoryDTO = policyHistoryDTO;
	}

	public List<PolicyHistoryDTO> getTransactionHistores() {
		return transactionHistores;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Open:
			try {
				this.policyTransaction = loadPolicyTransaction(policyNumber, quoteNumber, termNumber, versionNumber);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_History:
			try {
				transactionHistores = getAllPolicyTransactions(policyNumber, quoteNumber, termNumber);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;
			
		case UIConstants.UIACTION_Save:
			try {
				saveVersion(policyHistoryDTO);
				transactionHistores = getAllPolicyTransactions(policyNumber, quoteNumber, termNumber);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;
			
		default:
			break;
		}
	}
	
	protected List<PolicyHistoryDTO> getAllPolicyTransactions(String policyNumber, String quoteNumber, Integer termNumber) throws InsurancePolicyException {
		List<PolicyHistoryDTO> policyHistoryDTOs = null;
		
		if (policyNumber != null &&
			!policyNumber.trim().isEmpty()) {
			policyHistoryDTOs = policyProcessing.findPolicyHistoryByPolicyNumber(policyNumber);
			//policyVersions = policyProcessing.findAllPolicyVersionsByPolicyNumber(policyNumber);
		}
		else if (quoteNumber != null &&
				 !quoteNumber.trim().isEmpty()) {
			policyHistoryDTOs = policyProcessing.findPolicyHistoryByQuoteNumber(quoteNumber);
			//policyVersions = policyProcessing.findAllPolicyVersionByQuoteNumberAndTerm(quoteNumber, termNumber);
		}
		
		return policyHistoryDTOs;
	}
	
	protected void saveVersion(PolicyHistoryDTO policyHistoryDTO) throws InsurancePolicyException {
		policyProcessing.updatePolicyVersionDescription(policyHistoryDTO.getPolicyVersionPK(), policyHistoryDTO.getVersionDescription());
	}
}
