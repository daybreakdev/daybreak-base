package com.echelon.ui.components;

import java.util.ResourceBundle;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSPostalZipCodeField;
import com.ds.ins.uicommon.helpers.DSUIHelper;
import com.ds.ins.uicommon.session.DSSessionUI;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

/**
 * Form for displaying and editing an address.
 */
public class AddressForm extends FormLayout {
	private static final long serialVersionUID = 1994745268506237358L;

	private final DSUIHelper uiHelper = DSSessionUI.getHelperSession().getUiHelper();
	private ResourceBundle labels;
	private Binder<Address> binder = new Binder<>();

	private TextField addressField;
	private TextField cityField;
	private DSComboBox<String> provinceField;
	private DSPostalZipCodeField postalZipField;
	private DSComboBox<String> countryField;

	public AddressForm() {
		this(true);
	}

	public AddressForm(boolean showCountry) {
		this.labels = DSSessionUI.getUserSession().getSessionLabels();

		buildFields();
		bindFields();
		buildLayout(showCountry);
	}

	public Binder<Address> getBinder() {
		return binder;
	}

	public Registration addPostalZipValueChangeListener(
			ValueChangeListener<ComponentValueChangeEvent<TextField, String>> listener) {
		return postalZipField.addValueChangeListener(listener);
	}

	public void loadData(Address address) {
		uiHelper.loadDataConfigs(binder);

		binder.readBean(address);
		binder.setReadOnly(true);
	}

	private void buildFields() {
		addressField = new TextField(labels.getString("CommercialCustomerAddressLabel"));
		cityField = new TextField(labels.getString("CommercialCustomerCityLabel"));
		provinceField = new DSComboBox<String>(labels.getString("CommercialCustomerProvinceLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		provinceField.loadDSItems();
		postalZipField = new DSPostalZipCodeField(labels.getString("CommercialCustomerPostalZipLabel"));
		countryField = new DSComboBox<String>(labels.getString("CommercialCustomerCountryLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_Countries);
	}

	private void bindFields() {
		binder.forField(addressField).asRequired().bind(Address::getAddressLine1, Address::setAddressLine1);
		binder.forField(cityField).asRequired().bind(Address::getCity, Address::setCity);
		binder.forField(provinceField).asRequired().bind(Address::getProvState, Address::setProvState);
		binder.forField(postalZipField).asRequired().withNullRepresentation("")
				.withConverter(postalZipField.getConverter()).withValidator(postalZipField.getValidator())
				.bind(Address::getPostalZip, Address::setPostalZip);
		binder.forField(countryField).asRequired().bind(Address::getCountry, Address::setCountry);
		binder.setReadOnly(true);
	}

	private void buildLayout(boolean showCountry) {
		setResponsiveSteps(new ResponsiveStep(UIConstants.MINFIELDWIDTH, 4));

		add(addressField, 2);
		add(cityField, provinceField);
		add(postalZipField);
		if (showCountry) {
			add(countryField);
		}
	}
}
