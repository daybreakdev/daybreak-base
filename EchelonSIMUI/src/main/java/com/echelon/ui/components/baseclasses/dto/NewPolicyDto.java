package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.logging.Logger;

import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.Binding;

public class NewPolicyDto implements Serializable {
	private static final long serialVersionUID = -2176009102955792154L;
	private static final Logger log = Logger.getLogger(NewPolicyDto.class.getName());
	
	private String 					productCd;
	private boolean 				productCdChanged;
	private String 					province;
	private LocalDate				effectiveDate;
	private Integer					termLength;
	private Integer					liabilityLimit;
	private Boolean					hasAutomobileSubPolicy;
	private Boolean					hasCargoSubPolicy;
	private Boolean					hasCGLSubPolicy;
	private Boolean					hasGarageSubPolicy;
	private Boolean					hasCommercialPropertySubPolicy;	
	private String					customerType;
	private boolean					customerTypeChanged;
	private Customer 				customer;
	private Address	    			address;
	private SpecialityAutoSubPolicy autoSubPolicy;
	private CargoSubPolicy			cargoSubPolicy;
	private CGLSubPolicy			cGLSubPolicy;
	private GarageSubPolicy			garageSubPolicy;
	private CommercialPropertySubPolicy commercialPropertySubPolicy;
	private InsurancePolicy			insurancePolicy;
	private PolicyTransaction		policyTransaction;
	private String      			productVersion;
	private String 					productProgram;
	private String      			quoteNumber;
	private String					quotePreparedBy;
	private String					policyNumber;
	private Integer					policyTermNumber;
	private Integer 				policyVersionNumber;
	private String      			externalQuoteNumber;
	
	private Checkbox				hasAutoPolicyField;
	private Checkbox				hasMotorTruckCargoField;
	private Checkbox				hasCommGeneralLiabilityField;
	private Checkbox				hasGarageField;
	private Checkbox				hasCommercialPropertyField;
	
	private Binder<NewPolicyDto> 	binder;
	private Binding<?, ?> 			hasAutomobileSubPolicyBinding;
	
	private UIConstants.NEWPOLICYTYPES newPolicyType;

	public NewPolicyDto(UIConstants.NEWPOLICYTYPES newPolicyType) {
		super();
		
		this.newPolicyType = newPolicyType;
		this.customerType = UIConstants.CUST_TYPE_COMM; // Default to commercial customer
	}

	public boolean isQuote() {
		return UIConstants.NEWPOLICYTYPES.QUOTE.equals(this.newPolicyType);
	}

	public boolean isPolicy() {
		return UIConstants.NEWPOLICYTYPES.POLICY.equals(this.newPolicyType);
	}

	public boolean isRenewal() {
		return UIConstants.NEWPOLICYTYPES.RENEWAL.equals(this.newPolicyType);
	}
	
	public String getProductCd() {
		return productCd;
	}

	public void setProductCd(String productCd) {
		this.productCd = productCd;
	}

	public boolean isProductCdChanged() {
		return productCdChanged;
	}

	public void setProductCdChanged(boolean productCdChanged) {
		this.productCdChanged = productCdChanged;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getProductProgram() {
		return productProgram;
	}

	public void setProductProgram(String productProgram) {
		this.productProgram = productProgram;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public LocalDate getEffectiveDate() {
		return effectiveDate;
	}

	public void setEffectiveDate(LocalDate effectiveDate) {
		this.effectiveDate = effectiveDate;
	}

	public Integer getTermLength() {
		return termLength;
	}

	public void setTermLength(Integer termLength) {
		this.termLength = termLength;
	}

	public Integer getLiabilityLimit() {
		return liabilityLimit;
	}

	public void setLiabilityLimit(Integer liabilityLimit) {
		this.liabilityLimit = liabilityLimit;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Boolean getHasAutomobileSubPolicy() {
		return hasAutomobileSubPolicy;
	}

	public void setHasAutomobileSubPolicy(Boolean hasAutomobileSubPolicy) {
		this.hasAutomobileSubPolicy = hasAutomobileSubPolicy;
	}

	public Boolean getHasCargoSubPolicy() {
		return hasCargoSubPolicy;
	}

	public void setHasCargoSubPolicy(Boolean hasCargoSubPolicy) {
		this.hasCargoSubPolicy = hasCargoSubPolicy;
	}

	public Boolean getHasCGLSubPolicy() {
		return hasCGLSubPolicy;
	}

	public void setHasCGLSubPolicy(Boolean hasCGLSubPolicy) {
		this.hasCGLSubPolicy = hasCGLSubPolicy;
	}

	public Boolean getHasGarageSubPolicy() {
		return hasGarageSubPolicy;
	}

	public void setHasGarageSubPolicy(Boolean hasGarageSubPolicy) {
		this.hasGarageSubPolicy = hasGarageSubPolicy;
	}

	public Boolean getHasCommercialPropertySubPolicy() {
		return hasCommercialPropertySubPolicy;
	}

	public void setHasCommercialPropertySubPolicy(Boolean hasCommercialPropertySubPolicy) {
		this.hasCommercialPropertySubPolicy = hasCommercialPropertySubPolicy;
	}

	public String getCustomerType() {
		return customerType;
	}

	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}

	public boolean isCustomerTypeChanged() {
		return customerTypeChanged;
	}

	public void setCustomerTypeChanged(boolean customerTypeChanged) {
		this.customerTypeChanged = customerTypeChanged;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public SpecialityAutoSubPolicy getAutoSubPolicy() {
		return autoSubPolicy;
	}

	public void setAutoSubPolicy(SpecialityAutoSubPolicy autoSubPolicy) {
		this.autoSubPolicy = autoSubPolicy;
	}

	public CargoSubPolicy getCargoSubPolicy() {
		return cargoSubPolicy;
	}

	public void setCargoSubPolicy(CargoSubPolicy cargoSubPolicy) {
		this.cargoSubPolicy = cargoSubPolicy;
	}

	public CGLSubPolicy getcGLSubPolicy() {
		return cGLSubPolicy;
	}

	public void setcGLSubPolicy(CGLSubPolicy cGLSubPolicy) {
		this.cGLSubPolicy = cGLSubPolicy;
	}

	public String getQuotePreparedBy() {
		return quotePreparedBy;
	}

	public void setQuotePreparedBy(String quotePreparedBy) {
		this.quotePreparedBy = quotePreparedBy;
	}

	public GarageSubPolicy getGarageSubPolicy() {
		return garageSubPolicy;
	}

	public void setGarageSubPolicy(GarageSubPolicy garageSubPolicy) {
		this.garageSubPolicy = garageSubPolicy;
	}

	public CommercialPropertySubPolicy getCommercialPropertySubPolicy() {
		return commercialPropertySubPolicy;
	}

	public void setCommercialPropertySubPolicy(CommercialPropertySubPolicy commercialPropertySubPolicy) {
		this.commercialPropertySubPolicy = commercialPropertySubPolicy;
	}

	public InsurancePolicy getInsurancePolicy() {
		return insurancePolicy;
	}

	public void setInsurancePolicy(InsurancePolicy insurancePolicy) {
		this.insurancePolicy = insurancePolicy;
	}

	public PolicyTransaction getPolicyTransaction() {
		return policyTransaction;
	}

	public void setPolicyTransaction(PolicyTransaction policyTransaction) {
		this.policyTransaction = policyTransaction;
	}

	public String getQuoteNumber() {
		return quoteNumber;
	}

	public void setQuoteNumber(String quoteNumber) {
		this.quoteNumber = quoteNumber;
	}

	public String getPolicyNumber() {
		return policyNumber;
	}

	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}

	public Integer getPolicyTermNumber() {
		return policyTermNumber;
	}

	public void setPolicyTermNumber(Integer policyTermNumber) {
		this.policyTermNumber = policyTermNumber;
	}

	public Integer getPolicyVersionNumber() {
		return policyVersionNumber;
	}

	public void setPolicyVersionNumber(Integer policyVersionNumber) {
		this.policyVersionNumber = policyVersionNumber;
	}

	public Checkbox getHasAutoPolicyField() {
		return hasAutoPolicyField;
	}

	public void setHasAutoPolicyField(Checkbox hasAutoPolicyField) {
		this.hasAutoPolicyField = hasAutoPolicyField;
	}

	public Checkbox getHasMotorTruckCargoField() {
		return hasMotorTruckCargoField;
	}

	public void setHasMotorTruckCargoField(Checkbox hasMotorTruckCargoField) {
		this.hasMotorTruckCargoField = hasMotorTruckCargoField;
	}

	public Checkbox getHasCommGeneralLiabilityField() {
		return hasCommGeneralLiabilityField;
	}

	public void setHasCommGeneralLiabilityField(Checkbox hasCommGeneralLiabilityField) {
		this.hasCommGeneralLiabilityField = hasCommGeneralLiabilityField;
	}

	public Checkbox getHasGarageField() {
		return hasGarageField;
	}

	public void setHasGarageField(Checkbox hasGarageField) {
		this.hasGarageField = hasGarageField;
	}

	public Checkbox getHasCommercialPropertyField() {
		return hasCommercialPropertyField;
	}

	public void setHasCommercialPropertyField(Checkbox hasCommercialPropertyField) {
		this.hasCommercialPropertyField = hasCommercialPropertyField;
	}

	public Binding<?, ?> getHasAutomobileSubPolicyBinding() {
		return hasAutomobileSubPolicyBinding;
	}

	public void setHasAutomobileSubPolicyBinding(Binding<?, ?> hasAutomobileSubPolicyBinding) {
		this.hasAutomobileSubPolicyBinding = hasAutomobileSubPolicyBinding;
	}

	public Binder<NewPolicyDto> getBinder() {
		return binder;
	}

	public void setBinder(Binder<NewPolicyDto> binder) {
		this.binder = binder;
	}

	public String getExternalQuoteNumber() {
		return externalQuoteNumber;
	}

	public void setExternalQuoteNumber(String externalQuoteNumber) {
		this.externalQuoteNumber = externalQuoteNumber;
	}
	
}
