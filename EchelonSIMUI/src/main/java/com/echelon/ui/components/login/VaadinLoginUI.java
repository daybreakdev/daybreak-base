package com.echelon.ui.components.login;

import java.util.logging.Logger;

import com.echelon.ui.helpers.UIHelper;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.login.LoginForm;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("VaadinLogin")
public class VaadinLoginUI extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1843790722579612227L;
	private static final	Logger		log 				= Logger.getLogger(LoginUI.class.getName());
	
	private VerticalLayout	loginLayout;
	private LoginForm		loginComponent;
	private UIHelper		uiHelper = new UIHelper();
	
	public VaadinLoginUI() {
		super();
		
		buildLayout();
		buildLogin();
	}
	
	private void buildLayout() {
		this.setSizeFull();
		
		loginLayout = new VerticalLayout();
		loginLayout.setSizeUndefined();
		loginLayout.setMargin(false);
		loginLayout.setPadding(false);
		uiHelper.setBorder(loginLayout);
		
		this.add(loginLayout);
		this.setHorizontalComponentAlignment(Alignment.CENTER, loginLayout);
		this.setJustifyContentMode(JustifyContentMode.CENTER);
	}
	
	private void buildLogin() {
		Image dsLogo = uiHelper.createCompanyLogoLarge();
		dsLogo.setWidth("200px");
		dsLogo.setHeight("100px");
		
		loginLayout.add(dsLogo);
		loginLayout.setHorizontalComponentAlignment(Alignment.CENTER, dsLogo);

		loginComponent = new LoginForm();
		//loginComponent.setAction("login");
		loginComponent.setForgotPasswordButtonVisible(false);
		loginComponent.setError(true);
		
		loginLayout.add(loginComponent);
		loginLayout.setHorizontalComponentAlignment(Alignment.CENTER, loginComponent);
	}
}