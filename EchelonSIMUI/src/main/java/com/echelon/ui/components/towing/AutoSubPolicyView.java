package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyUIHelper;

public class AutoSubPolicyView extends TowingSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7206266012870323957L;
	private AutoSubPolicyUIHelper			autoSubPolicyUIHelper = new AutoSubPolicyUIHelper();
	private AutoSubPolicyForm				autoSubPolicyForm;

	public AutoSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeSubPolicyView(null);
		buildComponents();
	}

	private void buildComponents() {
		autoSubPolicyForm = new AutoSubPolicyForm(this.uiMode, this.uiContainerType);
		
		this.add(autoSubPolicyForm);
	}
	
	public boolean loadData(SpecialityAutoSubPolicy autoSubPolicy) {
		autoSubPolicyForm.loadData(autoSubPolicy);
		
		refreshToolbar(autoSubPolicy, false, true, false); // No new and delete
		
		return true;
	}
	
	public boolean enableEdit() {
		autoSubPolicyForm.enableEdit();
		return true;
	}
	
	public boolean validateEdit() {
		boolean ok1 = autoSubPolicyForm.validateEdit();
		
		return ok1;
	}
	
	public boolean saveEdit(SpecialityAutoSubPolicy autoSubPolicy) {
		boolean ok1 = autoSubPolicyForm.saveEdit(autoSubPolicy);
		
		return ok1;
	}

	protected void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
					autoSubPolicyUIHelper.updateSubPolicyAction();
				});
			}
		}
	}
}
