package com.echelon.ui.components.endorsements;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.towing.VehicleEndorsementForm;

/**
 * Coverage form for OPCF 20 endorsement.
 *
 * OPCF 20 limits require special handling.
 */
public class OPCF20EndorsementForm extends VehicleEndorsementForm {
	private static final long serialVersionUID = -3749943239618249938L;

	public OPCF20EndorsementForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	public void loadData(CoverageDto coverageDto) {
		super.loadData(coverageDto);

		/*
		 * Filter limits based on vehicle description.
		 *
		 * Vehicle descriptions are in prodconfig/lookups/<product>/VehDesc.json. All
		 * valid limits are specified in the product configuration
		 */
		switch (uiConfigHelper.getProductCode()) {
		case ConfigConstants.PRODUCTCODE_TOWING:
			switch (((SpecialtyVehicleRisk) coverageDto.getCoverageParent())
				.getVehicleDescription()) {
			case "HC": // Heavy Commercial (1 ton)
			case "TRC": // Tractor
			case "TT": // Tow Truck
			case "LTT": // Light Tow Truck
			case "MTT": // Medium Tow Truck
			case "HTT": // Heavy Tow Truck
				filterLimits(3000, 5000, 7500);
				setDefaultLimit(7500);
				break;
			case "LC": // Light Commercial
			case "PP": // Private Passenger
				filterLimits(1000, 1500, 2500, 3000);
				setDefaultLimit(1500);
				break;
			default:
				// default to showing all limits
				break;
			}

			break;
		case ConfigConstants.PRODUCTCODE_LHT:
			switch (((SpecialtyVehicleRisk) coverageDto.getCoverageParent())
				.getVehicleDescription()) {
			case "HC": // Heavy Commercial (1 ton)
			case "TRC": // Tractor
				filterLimits(3000, 5000, 7500);
				setDefaultLimit(7500);
				break;
			case "LC": // Light Commercial
			case "PP": // Private Passenger
				filterLimits(1000, 1500, 2500, 3000);
				setDefaultLimit(1500);
				break;
			default:
				// default to showing all limits
				break;
			}

			break;
		default:
			// Unknown product code (or one that doesn't require filtering). Show all
			// limits.
			break;
		}
	}

	/**
	 * Filters the coverage limit combo box so only the given limits are present.
	 *
	 * <p>
	 * <b>Note: this method will never add limits. If {@link allowedLimits} contains
	 * a limit that is not present in the configuration, it will be ignored.</b>
	 * </p>
	 *
	 * @param allowedLimits the limits which are allowed to appear.
	 */
	private void filterLimits(int... allowedLimits) {
		final List<String> limits = Arrays.stream(allowedLimits).mapToObj(Integer::toString)
			.collect(Collectors.toList());
		final DSComboBox<Integer> lookupField = coverageLimitField.getLookupField();
		final List<LookupTableItem> originalLookupList = lookupField.getDSLookupItems();

		final List<LookupTableItem> filteredLookupList = originalLookupList.stream()
			.filter(lookupItem -> limits.contains(lookupItem.getItemValue()))
			.collect(Collectors.toList());
		lookupField.setDSItems(filteredLookupList);
	}

	/**
	 * Sets the default coverage limit, if it is not already set.
	 *
	 * @param defaultLimit the default coverage limit.
	 */
	private void setDefaultLimit(int defaultLimit) {
		final DSComboBox<Integer> lookupField = coverageLimitField.getLookupField();
		final Optional<Integer> currentValue = lookupField.getOptionalValue();

		if (!currentValue.isPresent()) {
			lookupField.setDSValue(defaultLimit);
		}
	}
}
