package com.echelon.ui.components.baseclasses.policy;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.utils.Constants;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyRatingDetailsUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyRatingDetails extends BaseLayout {
	private static final long serialVersionUID = -2191955412764192063L;

	protected final PolicyUIHelper policyUIHelper = new PolicyUIHelper();
	protected final BasePolicyRatingDetailsUIHelper policyRatingDetailsUIHelper = new BasePolicyRatingDetailsUIHelper();

	protected ActionToolbar toolbar;
	protected TextField rateBookIdField;
	protected IntegerField rateBookVersionField;
	protected Checkbox isReRateReqField;
	protected BaseRatefactorList ratefactorList;

	protected Binder<PolicyTerm> ptermBinder;
	protected Binder<PolicyTransaction<?>> ptxnBinder;

	public BasePolicyRatingDetails(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		buildRatingDetails();
		buildRatefactorList();
	}

	protected void buildRatingDetails() {
		rateBookIdField = new TextField(labels.getString("PolicyRatingDetailsRatebookIdLabel"));
		rateBookVersionField = new IntegerField(labels.getString("PolicyRatingDetailsRatebookVerLabel"));
		isReRateReqField = new Checkbox(labels.getString("PolicyRatingDetailsIsReRateReq"));

		ptermBinder = new Binder<>();
		ptermBinder.forField(rateBookIdField).bind(PolicyTerm::getRatebookId, null);
		ptermBinder.forField(rateBookVersionField).bind(PolicyTerm::getRatebookVersion, null);
		ptermBinder.setReadOnly(true);

		ptxnBinder = new Binder<>();
		ptxnBinder.forField(isReRateReqField).bind(PolicyTransaction::getIsReRateReqd, null);
		ptxnBinder.setReadOnly(true);

		if (UICONTAINERTYPES.Inplace.equals(getUiContainerType())) {
			final DSHeaderComponents header = uiLayoutHelper.buildHeaderLayout(this,
					labels.getString("PolicyRatingDetailsSectionLabel"), UICOMPONENTTYPES.Form,
					UIConstants.UIACTION_Update);
			toolbar = (ActionToolbar) header.getToolbar();
			toolbar.getButton(UIConstants.UIACTION_Update)
					.addClickListener(__ -> policyRatingDetailsUIHelper.updateRatingDetails());
		}

		FormLayout form = new FormLayout();
		form.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3));
		form.add(rateBookIdField, rateBookVersionField, isReRateReqField);

		this.add(form);
	}

	protected void buildRatefactorList() {
		ratefactorList = new BaseRatefactorList(uiMode, uiContainerType);
		ratefactorList.getRatefactorGrid().setHeightByRows(true);
		this.add(ratefactorList);
	}

	public void loadData() {
		if (toolbar != null) {
			final PolicyVersion currPolVer = policyUIHelper.getCurrentPolicyVersion();
			toolbar.refreshButtons(null, currPolVer.getInsurancePolicy());

			// Ratebook versions can **only** be updated during new business or renewal.
			boolean allowed = Constants.SYSTEN_STATUS_PENDING.equalsIgnoreCase(currPolVer.getSystemStatus());
			allowed = allowed && StringUtils.equalsAnyIgnoreCase(currPolVer.getPolicyTxnType(),
					Constants.VERS_TXN_TYPE_NEWBUSINESS, Constants.VERS_TXN_TYPE_RENEWAL);
			if (!allowed) {
				toolbar.disableAllButtons();
			}
		}

		ratefactorList.loadData(uiPolicyHelper.getCurrentPolicyTransaction());

		ptermBinder.readBean(uiPolicyHelper.getCurrentPolicyVersion().getVersionTerm());
		ptxnBinder.readBean(uiPolicyHelper.getCurrentPolicyTransaction());
		enableDataView(ptermBinder, ptxnBinder);
	}
}
