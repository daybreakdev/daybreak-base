package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.Composite;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class LocationMultiForm extends Composite<VerticalLayout> implements HasComponents, HasStyle, HasSize {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7340203600955687403L;
	private String 				uiMode;
	private Button				removeBttn;
	private List<LocationForm> 	forms;
	private GarageSubPolicy<?> 	garageSubPolicy;

	public LocationMultiForm(String uiMode, GarageSubPolicy<?> garageSubPolicy) {
		this(uiMode, garageSubPolicy, null);
	}

	public LocationMultiForm(String uiMode, GarageSubPolicy<?> garageSubPolicy, PolicyLocation location) {
		this.uiMode = uiMode;
		this.garageSubPolicy = garageSubPolicy;
		this.forms = new ArrayList<LocationForm>(1);
		setComponentStyle();
        addNewForm(false, location);
	}

	public void addNewForm(boolean autoRemovable) {
		addNewForm(autoRemovable, null);
	}

    public void addNewForm(boolean autoRemovable, PolicyLocation location) {
    	HorizontalLayout row = new HorizontalLayout();
    	row.setAlignItems(Alignment.BASELINE);
    	LocationForm form = new LocationForm(uiMode, UICONTAINERTYPES.Dialog, garageSubPolicy);
    	row.add(form);
        if (uiMode.equals(UIConstants.UIACTION_New) && autoRemovable) {
        	removeBttn = new Button(VaadinIcon.CLOSE.create());
    		removeBttn.addClickListener(e -> {
            	forms.remove(form);
                this.remove(row);
            });
    		row.add(removeBttn);
        }
        add(row);

    	form.loadData(location);
    	if (!UIConstants.UIACTION_View.equals(uiMode)) {
    		form.enableEdit(location);
    	}
    	//form.checkConditions(location);
    	forms.add(form);
    }

    public boolean validate() {
    	for(LocationForm form: forms) {
    		if(!form.validate()) {
    			return false;
    		}
    	}
    	return true;
    }

    public boolean save(PolicyLocation location, LocationForm form) {
    	return form.save(location);
    }

    public boolean update(PolicyLocation location) {
    	return forms.get(0).update(location);
    }

    public List<LocationForm> getForms() {
    	return forms;
    }

    private void setComponentStyle() {
    	this.getElement().getStyle().set("overflow-y", "auto");
    	this.getElement().getStyle().set("max-height", "440px");
    	this.getElement().getStyle().set("border", "1px solid #9E9E9E");
	}

}
