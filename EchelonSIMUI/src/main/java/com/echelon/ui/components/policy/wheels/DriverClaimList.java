package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverClaimUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverClaim;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public class DriverClaimList extends BaseListLayout<DriverClaim> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8433735020077366650L;
	private DriverClaimUIHelper 		claimUIHelper = new DriverClaimUIHelper();	
	private Grid<DriverClaim> 			claimGrid;
	private Driver						driver;

	public DriverClaimList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}
	
	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("DriverClaimListTitle"
																+ ""),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	private void buildGrid() {
		claimGrid = new Grid<DriverClaim>();
		claimGrid.setSizeFull();
		uiHelper.setGridStyle(claimGrid);
		
		createIndicatorColumn(claimGrid);
		claimGrid.addColumn(DriverClaim::getLossDate) 
								.setHeader(labels.getString("DriverClaimListColumnLossDate"))
								.setAutoWidth(true);
		claimGrid.addColumn(DriverClaim::getVehicle)
								.setHeader(labels.getString("DriverClaimListColumnVehicle"))
								.setAutoWidth(true);
		claimGrid.addColumn(DriverClaim::getTotalPaid)
								.setHeader(labels.getString("DriverClaimListColumnTotalPaid"))
								.setAutoWidth(true);
		claimGrid.addColumn(new DSLookupItemRenderer<DriverClaim>(DriverClaim::getClaimType)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ClaimType))
								.setHeader(labels.getString("DriverClaimListColumnClaimType"))
								.setAutoWidth(true);
		claimGrid.addColumn(new DSLookupItemRenderer<DriverClaim>(DriverClaim::getFaultResponsibility)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ClaimFault))
								.setHeader(labels.getString("DriverClaimListColumnFaultPct"))
								.setAutoWidth(true);
		claimGrid.addColumn(driverClaim -> claimUIHelper.getEchelonClaimStatus(driverClaim.getIsInsurerClaim()))
								.setHeader(labels.getString("DriverClaimListColumnClaimNum"))
								.setAutoWidth(true);
		claimGrid.setSelectionMode(SelectionMode.SINGLE);
		claimGrid.addSelectionListener(event -> refreshToolbar());
		claimGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		setUpGridDbClickEvent(claimGrid);
		
		this.add(claimGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				claimUIHelper.newClaimAction(driver);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (claimGrid.asSingleSelect() != null) {
					claimUIHelper.updateClaimAction(driver, claimGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (claimGrid.asSingleSelect() != null) {
					claimUIHelper.deleteClaimAction(driver, claimGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (claimGrid.asSingleSelect() != null) {
					claimUIHelper.undeleteClaimAction(driver, claimGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(Driver driver, Set<DriverClaim> set) {
		this.driver = driver;
		List<DriverClaim> list = claimUIHelper.sortClaimList(set);

		this.claimGrid.setItems(list);
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(driver, claimGrid);
	}

}