package com.echelon.ui.components.nonfleet;

import java.util.List;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.policy.wheels.VehicleSurchargesForm;
import com.echelon.ui.components.towing.VehicleCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class NonFleetVehicleCoverageList extends VehicleCoverageList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 823714223353359299L;
	private VehicleSurchargesForm				surchargesForm;

	// no need surcharge form in transaction summary
	public NonFleetVehicleCoverageList(String uiMode, UICONTAINERTYPES uiContainerType, OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
		
		if (!UICONTAINERTYPES.Information.equals(uiContainerType)) {
			buildSurchargesForm();
		}
	}

	protected void buildSurchargesForm() {
		surchargesForm = new VehicleSurchargesForm(uiMode, uiContainerType);
	}

	@Override
	protected Component setupEndOfTotalListSection(CoverageDto dto) {
		// TODO Auto-generated method stub
		if (surchargesForm != null) {
			HorizontalLayout layout =  new HorizontalLayout();
			layout.setSizeFull();
			layout.setPadding(true);
			uiHelper.setBorder(layout);
			layout.getElement().getStyle().set("background-color","white");
			
			layout.add(surchargesForm);
			
			return layout;
		}
		
		return null;
	}

	@Override
	public boolean loadData(ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
			List<CoverageDto> coverags) {
		// TODO Auto-generated method stub
		boolean result = super.loadData(covConfigParent, covParent, coverags);
		
		if (surchargesForm != null) {
			surchargesForm.loadData((SpecialtyVehicleRisk) covParent);
		}
		
		return result;
	}
		
}
