package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLabelFormat;
import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BaseTransactionHistoryForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3773771127504656901L;
	private TextField 		transactionTypeField;
	private IntegerField	versionSeqField;
	private TextField		versionDescriptionField;
	private DatePicker		issueDateField;
	private DatePicker		versionDateField;
	private PremiumField	transactionPremiumField;
	private PremiumField	writtenPremiumField;
	private TextField		termStatusField;
	private TextField		versionStatusField;
	
	private Binder<PolicyHistoryDTO> binder;
	private Binder<PolicyHistoryDTO> vwBinder;
	
	private DSLabelFormat	labelFormat;

	public BaseTransactionHistoryForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.labelFormat = new DSLabelFormat();
		
		initializeLayouts();
		buildFields();
		buildForm();
	}
	
	protected void initializeLayouts() {
		
	}

	protected void buildFields() {
		transactionTypeField 	= new TextField(labels.getString("TransactionHistoryFormVerTxhTypeLabel"));
		versionSeqField			= new IntegerField(labels.getString("TransactionHistoryFormVerSeqLabel"));
		versionDescriptionField = new TextField(labels.getString("TransactionHistoryFormVerDescriptionLabel"));
		issueDateField			= new DatePicker(labels.getString("TransactionHistoryFormVerIssueDateLabel"));
		versionDateField		= new DatePicker(labels.getString("TransactionHistoryFormVerDateLabel"));
		transactionPremiumField = new PremiumField(labels.getString("TransactionHistoryFormTransPremLabel"));
		writtenPremiumField		= new PremiumField(labels.getString("TransactionHistoryFormWrittenPremLabel"));
		termStatusField			= new TextField(labels.getString("TransactionHistoryFormTermStatusLabel"));
		versionStatusField		= new TextField(labels.getString("TransactionHistoryFormVersionStatusLabel"));
		
		binder = new Binder<PolicyHistoryDTO>(PolicyHistoryDTO.class);
		binder.forField(versionDescriptionField).bind(PolicyHistoryDTO::getVersionDescription, PolicyHistoryDTO::setVersionDescription);
		binder.setReadOnly(true);
		
		vwBinder = new Binder<PolicyHistoryDTO>(PolicyHistoryDTO.class);
		
		vwBinder.forField(transactionTypeField).bind(PolicyHistoryDTO::getPolicyTxnType, null);
		vwBinder.forField(versionSeqField).bind(PolicyHistoryDTO::getPolicyVersionNum, null);
		vwBinder.forField(issueDateField).bind(PolicyHistoryDTO::getVersionIssueDate, null);
		vwBinder.forField(versionDateField).bind(PolicyHistoryDTO::getVersionDate, null);
		vwBinder.forField(transactionPremiumField)
							.withNullRepresentation("")
							.withConverter(transactionPremiumField.getConverter())
							.bind(PolicyHistoryDTO::getTxnNetPremiumChange, null);
		vwBinder.forField(writtenPremiumField)
							.withNullRepresentation("")
							.withConverter(writtenPremiumField.getConverter())
							.bind(PolicyHistoryDTO::getTxnWrittenPremium, null);
		vwBinder.forField(termStatusField).bind(PolicyHistoryDTO::getTermBusinessStatus, null);
		vwBinder.forField(versionStatusField).bind(PolicyHistoryDTO::getVersionBusinessStatus, null);
		
		vwBinder.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );

		form.add(transactionTypeField);
		form.add(versionDescriptionField, 2);
		form.add(versionSeqField, versionDateField);
		form.add(transactionPremiumField, writtenPremiumField);
		//form.add(termStatusField);
		form.add(versionStatusField);
		
		this.add(form);
	}
	
	public void loadData(PolicyHistoryDTO dto) {
		binder.readBean(dto);
		vwBinder.readBean(dto);
			
		if (dto != null) {
			uiHelper.setFieldValue(transactionTypeField, 
					this.labelFormat.getFormattedValue("PolicyTransactionType", null, 
														dto.getPolicyTxnType()));
			uiHelper.setFieldValue(termStatusField, 
					this.labelFormat.getFormattedValue("PolicyBusinessStatus", null, 
														dto.getTermBusinessStatus()));
			uiHelper.setFieldValue(versionStatusField, 
					this.labelFormat.getFormattedValue("PolicyBusinessStatus", null, 
														dto.getVersionBusinessStatus()));
		}
		
		enableDataView(binder, vwBinder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		uiHelper.setEditFieldsReadonly(vwBinder);
		
		setFocus();
	}

	public boolean saveEdit(PolicyHistoryDTO dto) {
		boolean ok1 = uiHelper.writeValues(binder, dto);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid1 = uiHelper.validateValues(binder);
		
		return valid1;
	}
	
	public void setFocus() {
		versionDescriptionField.focus();
	}
	
}
