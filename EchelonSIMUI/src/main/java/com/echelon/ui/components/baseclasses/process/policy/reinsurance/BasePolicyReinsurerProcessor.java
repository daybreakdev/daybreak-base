package com.echelon.ui.components.baseclasses.process.policy.reinsurance;

import java.io.Serializable;
import java.util.Set;

import com.ds.ins.domain.entities.Reinsurer;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyReinsurerProcessor extends BasePolicyProcessor implements Serializable {
	private static final long serialVersionUID = 3062988281326418487L;

	private Set<Reinsurer> reinsurers;
	private PolicyReinsurer policyReinsurer;

	public BasePolicyReinsurerProcessor(String processAction, PolicyTransaction<?> policyTransaction,
		Set<Reinsurer> reinsurers) {
		super(processAction, policyTransaction);

		this.reinsurers = reinsurers;
	}

	public BasePolicyReinsurerProcessor(String processAction,
		PolicyTransaction<?> policyTransaction, PolicyReinsurer policyReinsurer) {
		super(processAction, policyTransaction);

		this.policyReinsurer = policyReinsurer;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Save:
			createNewPolicyReinsurers();
			break;
		case UIConstants.UIACTION_Delete:
			deletePolicyReinsurer();
			break;
		}
	}

	private void createNewPolicyReinsurers() {
		reinsurers.stream().map(this::createPolicyReinsurer)
			.forEach(policyTransaction::addPolicyReinsurer);
		updatePolicyTransaction();
		loadPolicyTransaction();
	}

	private void deletePolicyReinsurer() {
		policyTransaction.removePolicyReinsurer(policyReinsurer);
		updatePolicyTransaction();
		loadPolicyTransaction();
	}

	private PolicyReinsurer createPolicyReinsurer(Reinsurer reinsurer) {
		PolicyReinsurer policyReinsurer = new PolicyReinsurer();
		policyReinsurer.setReinsurer(reinsurer);
		return policyReinsurer;
	}
}
