package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;

import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.NEWPOLICYSTEPS;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.customer.ICommercialCustomerProcessing;

public class BaseNewPolicyProcessor<INSPCY extends InsurancePolicy, PCYVER extends PolicyVersion, PCYTXN extends PolicyTransaction<?>, SUBPCY extends SubPolicy<?>> extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 852440061867426017L;
	protected ICommercialCustomerProcessing commercialCustomerProcessing;
	protected NewPolicyDto newPolicyDto;
	protected NEWPOLICYSTEPS step;

	public BaseNewPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			ICommercialCustomerProcessing commercialCustomerProcessing, NewPolicyDto newPolicyDto,
			NEWPOLICYSTEPS step) {
		super(processAction, policyTransaction);
		this.commercialCustomerProcessing = commercialCustomerProcessing;
		this.newPolicyDto = newPolicyDto;
		this.step = step;
	}

	public NEWPOLICYSTEPS getStep() {
		return step;
	}

	public NewPolicyDto getNewPolicyDto() {
		return newPolicyDto;
	}

	// This cannot run updatePolicyTransaction before getting address UI values 
	// System need to first update the address and create default location
	@Override
	public void run() {
		boolean toRefreshNewPolicyDto = true;
		
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				switch (this.step) {
				case Policy:
					if (!createPolicy()) {
						setProcessFail(true);
						return;
					}
					break;

				case CreateNewSubPolices:
					if (!createNewSubPolicies()) {
						setProcessFail(true);
						return;
					}
					
					// do not refresh newPolicyDto because it contains empty subpolicies
					toRefreshNewPolicyDto = false;
					break;

				case SaveNewSubPolices:
					if (!saveNewSubPolicies()) {
						setProcessFail(true);
						return;
					}
					break;

				default:
					break;
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				switch (this.step) {
				case Policy:
					if (!savePolicy()) {
						setProcessFail(true);
						return;
					}
					break;

				case Customer:
					if (!saveCustomer()) {
						setProcessFail(true);
						return;
					}
					break;

				case UpdateSubPolices:
					if (!updateSubPolicies()) {
						setProcessFail(true);
						return;
					}
					break;

				default:
					break;
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			break;
			
		default:
			break;
		}
		
		if (toRefreshNewPolicyDto) {
			try {
				refreshNewPolicyDto();
				if (newPolicyDto != null) {
					this.policyTransaction = newPolicyDto.getPolicyTransaction();
				}
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}
		}
	}

	protected boolean createPolicy() throws Exception {
		Exception policyException = null;
		INSPCY newInsurancePolicy = null;
		
		if (newPolicyDto.getPolicyTransaction().getPolicyTransactionPK() == null) {
			try {
				if (newPolicyDto.isRenewal()) {
					if (newPolicyDto.getCustomer().getCustomerPk() == null) {
						newInsurancePolicy = (INSPCY) policyProcessing.createNewCustomerAndManualPolicyRenewal(newPolicyDto.getCustomerType(),
																				newPolicyDto.getProductCd(), newPolicyDto.getProductProgram(),
																				newPolicyDto.getQuoteNumber(), newPolicyDto.getPolicyNumber(), newPolicyDto.getPolicyTermNumber(),
																				newPolicyDto.getEffectiveDate(),
																				null,
																				newPolicyDto.getTermLength(),
																				getUserProfile()
																				);
					}
					else {
						newInsurancePolicy = (INSPCY) policyProcessing.createNewManualPolicyRenewal(newPolicyDto.getCustomer(), 
																				newPolicyDto.getProductCd(), newPolicyDto.getProductProgram(),
																				newPolicyDto.getQuoteNumber(), newPolicyDto.getPolicyNumber(), newPolicyDto.getPolicyTermNumber(),
																				newPolicyDto.getEffectiveDate(),
																				null,
																				newPolicyDto.getTermLength(),
																				newPolicyDto.getAddress(),
																				getUserProfile()
																				);
					}
				}
				else {
					if (newPolicyDto.getCustomer().getCustomerPk() == null) {
						newInsurancePolicy = (INSPCY) policyProcessing.createNewCustomerAndInsurancePolicy(newPolicyDto.getCustomerType(),
																				newPolicyDto.getProductCd(), newPolicyDto.getProductProgram(), 
																				newPolicyDto.getEffectiveDate(),
																				null,
																				newPolicyDto.getTermLength(),
																				getUserProfile()
																				);
					}
					else {
						newInsurancePolicy = (INSPCY) policyProcessing.createNewInsurancePolicy(newPolicyDto.getCustomer(), 
																				newPolicyDto.getProductCd(), newPolicyDto.getProductProgram(), 
																				newPolicyDto.getEffectiveDate(),
																				null,
																				newPolicyDto.getTermLength(),
																				newPolicyDto.getAddress(),
																				getUserProfile()
																				);
					}
				}
				PolicyVersion policyVersion = newInsurancePolicy.getPolicyVersions().get(0);
				newPolicyDto.setPolicyTransaction(policyVersion.getPolicyTransactions().iterator().next());
				newPolicyDto.setQuoteNumber(newInsurancePolicy.getQuoteNum());
				newPolicyDto.setInsurancePolicy(newInsurancePolicy);
				newPolicyDto.setPolicyNumber(newInsurancePolicy.getBasePolicyNum());
				newPolicyDto.setPolicyTermNumber(policyVersion.getVersionTerm().getTermNumber());
				newPolicyDto.setPolicyVersionNumber(policyVersion.getPolicyVersionNum());
			} catch (Exception e) {
				policyException = e;
			}
		}
		else {
			refreshNewPolicyDto();
			newInsurancePolicy = (INSPCY) newPolicyDto.getInsurancePolicy();
		}
		
		if (policyException != null) {
			throw new InsurancePolicyException(policyException.getMessage(), policyException);
		}
		if (newInsurancePolicy == null || newInsurancePolicy.getInsurancePolicyPK() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyCreateFail");
		}
		if (newInsurancePolicy.getPolicyCustomer() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyCustomerFail");
		}
		if (newInsurancePolicy.getInsuredAddress() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyInsuredAddressFail");
		}
		
		return true;
	}
	
	protected boolean createNewSubPolicies() throws Exception {
		return true;
	}
	
	protected boolean savePolicy() throws Exception {
		boolean ok = true;
		
		if (newPolicyDto.getInsurancePolicy() != null) {
			newPolicyDto.getInsurancePolicy().setExternalQuoteNum(newPolicyDto.getExternalQuoteNumber());
			
			ok = updatePolicyTransaction();
		}
		
		return ok;
	}
	
	protected boolean saveCustomer() throws Exception {
		boolean ok = false;
		
		// Save Policy Customer
		/*
		try {
			commercialCustomerProcessing.saveCommercialCustomer(newPolicyDto.getCustomer(), getUserProfile());
			ok = true;
		} catch (Exception e) {
			throw e;
		}
		*/
		
		// Save InsuredAddress and Create Policy Location
		ok = updatePolicyTransaction();
		
		return ok;
	}
	
	protected boolean saveNewSubPolicies() throws Exception {
		return true;
	}
	
	protected boolean updateSubPolicies() throws Exception {
		return true;
	}

	protected boolean refreshNewPolicyDto() throws InsurancePolicyException {
		PolicyTransaction<Risk> polTransaction = null;
		if (newPolicyDto.isRenewal()) {
			polTransaction = (PolicyTransaction<Risk>) getPolicyProcessing().findLatestPolicyTransaction(newPolicyDto.getPolicyNumber(), newPolicyDto.getPolicyTermNumber(), newPolicyDto.getPolicyVersionNumber());
		}
		else {
			polTransaction = (PolicyTransaction<Risk>) getPolicyProcessing().findLatestPolicyTransactionByQuoteNumber(newPolicyDto.getQuoteNumber());
		}
		if (polTransaction == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyPolicyTransactionFail");
		}
		PolicyVersion policyVersion = polTransaction.getPolicyVersion();
		if (policyVersion == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyPolicyVersionFail");
		}
		PolicyLocation policyLocation = getPolicyInsurerLocation(polTransaction);
		if (policyLocation == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyPolicyLocationFail");
		}
		
		polTransaction = (PolicyTransaction<Risk>) loadFullPolicyTransactionData(polTransaction);
		
		newPolicyDto.setPolicyTransaction(polTransaction);
		newPolicyDto.setCustomer(policyVersion.getInsurancePolicy().getPolicyCustomer());
		//newPolicyDto.setAddress(policyVersion.getInsurancePolicy().getInsuredAddress());
		newPolicyDto.setAddress(policyLocation.getLocationAddress());
		newPolicyDto.setInsurancePolicy(policyVersion.getInsurancePolicy());
		
		if (newPolicyDto.getCustomer() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyCustomerFail");
		}
		if (newPolicyDto.getAddress() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyCustomerFail");
		}
		if (newPolicyDto.getInsurancePolicy() == null) {
			throw new InsurancePolicyException((Exception)null, true, "ErrorNewPolicyPolicyFail");
		}
		
		return true;
	}
	
	protected PolicyLocation getPolicyInsurerLocation(PolicyTransaction<Risk> policyTransaction) {
		PolicyLocation result = null;
		
		if (policyTransaction != null) {
			result = policyTransaction.getPolicyRisks().stream()
							.filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
							.map(o -> (PolicyLocation)o)
							.filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId()))
							.findFirst().orElse(null);
		}
		
		return result;
	}

}
