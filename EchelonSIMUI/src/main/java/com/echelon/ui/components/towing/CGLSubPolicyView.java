package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyUIHelper;

public class CGLSubPolicyView extends TowingSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2996494037748480867L;
	private CGLSubPolicyUIHelper	cglHelper = new CGLSubPolicyUIHelper();
	private CGLSubPolicyForm		cglSubPolicyForm;

	public CGLSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeSubPolicyView(null);
		buildComponents();
	}

	private void buildComponents() {
		cglSubPolicyForm = new CGLSubPolicyForm(this.uiMode, this.uiContainerType);
		this.add(cglSubPolicyForm);
	}

	public boolean loadData(CGLSubPolicy<?> cglSubPolicy) {
		boolean selectedSubPolicy = cglSubPolicy != null;
		cglSubPolicyForm.loadData(cglSubPolicy);

		refreshToolbar(cglSubPolicy, true, true, true);
		return true;
	}

	@Override
	protected void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).addClickListener(e -> {
					cglHelper.newSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
					cglHelper.updateSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(e -> {
					cglHelper.deleteSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(e -> {
					cglHelper.undeleteSubPolicyAction();
				});
			}
		}
	}

}
