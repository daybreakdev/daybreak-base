package com.echelon.ui.components.nonfleet;

import java.util.List;

import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSGenericFieldDto;
import com.ds.ins.uicommon.dto.DSGenericFieldGroupDto;
import com.echelon.ui.components.baseclasses.GenericFieldsForm;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.helpers.policy.nonfleet.NonFleetVehicleUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.formlayout.FormLayout;

public class NonFleetGenericFieldsForm extends GenericFieldsForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3183371459383765206L;
	private NonFleetVehicleUIHelper vehicleUIHelper;

	public NonFleetGenericFieldsForm(String uiMode, UICONTAINERTYPES uiContainerType,
			DSGenericFieldGroupDto valueGroupDto, FormLayout followFormLayout, CoverageDto coverageDto) {
		super(uiMode, uiContainerType, valueGroupDto, followFormLayout, coverageDto);
		// TODO Auto-generated constructor stub
		
		this.vehicleUIHelper = new NonFleetVehicleUIHelper();
	}

	@Override
	public void enableEdit() {
		// TODO Auto-generated method stub
		super.enableEdit();
		
		if (SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_ID_27B.equalsIgnoreCase(valueGroupDto.getGroupId())) {
			setupOPCF27B();
		}
	}

	@SuppressWarnings("unchecked")
	private void setupOPCF27B() {
		List<DSGenericFieldDto> allFields = valueGroupDto.getAllFields();
		if (allFields != null && !allFields.isEmpty()) {
			DSGenericFieldDto vehYearField 	= valueGroupDto.getFieldDto(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHYEAR);
			DSGenericFieldDto vehLPNField 	= valueGroupDto.getFieldDto(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHLPN);
			DSGenericFieldDto implRGField 	= valueGroupDto.getFieldDto(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_ImplRG);
			DSGenericFieldDto selRGField 	= valueGroupDto.getFieldDto(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_SelRG);

			if (vehYearField != null) {
				vehYearField.getFieldWidget().addValueChangeListener(e -> getVehicleRateGroup(vehYearField, vehLPNField, implRGField, selRGField));
			}
			if (vehLPNField != null) {
				if (coverageDto.getEditLimitField().getLookupField() != null) {
					coverageDto.getEditLimitField().getLookupField().addValueChangeListener(e -> {
								LookupTableItem item = coverageDto.getEditLimitField().getLookupField().getLookupTableItemValue();
								if (item != null) {
									updateVehLPNFromLimitField(vehLPNField, item.getItemValue());
								}
								else {
									updateVehLPNFromLimitField(vehLPNField, null);
								}
							});
				}
				else if (coverageDto.getEditLimitField().getIntegerField() != null) {
					coverageDto.getEditLimitField().getIntegerField().addValueChangeListener(e -> 
								updateVehLPNFromLimitField(vehLPNField, coverageDto.getEditLimitField().getIntegerField().getValue()));
				}
				vehLPNField.getFieldWidget().addValueChangeListener(e -> getVehicleRateGroup(vehYearField, vehLPNField, implRGField, selRGField));
				uiHelper.setEditFieldsReadonly(vehLPNField.getFieldWidget());
			}
		}
	}
	
	private void getVehicleRateGroup(DSGenericFieldDto vehYearField, DSGenericFieldDto vehLPNField, DSGenericFieldDto implRGField, DSGenericFieldDto selRGField) {
		this.vehicleUIHelper.setVehicleRateGroupFields((vehYearField != null ? vehYearField.getFieldWidget() : null), 
														(vehLPNField != null ? vehLPNField.getFieldWidget()  : null), 
														(implRGField != null ? implRGField.getFieldWidget()  : null), 
														(selRGField  != null ? selRGField.getFieldWidget()   : null));
	}
	
	private void updateVehLPNFromLimitField(DSGenericFieldDto vehLPNField, String limit) {
		uiHelper.setFieldValue(vehLPNField.getFieldWidget(), limit);
	}
}
