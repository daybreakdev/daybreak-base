package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="NotesContentView", layout = BasePolicyView.class)
public class BasePolicyNoteContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5134711523413226002L;
	private BasePolicyNoteList policyNoteList;
	private BasePolicyNoteList brokerNoteList;

	public BasePolicyNoteContentView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BasePolicyNoteContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			/*
			uiLayoutHelper.buildHeaderLayout(this,
												labels.getString("PolicyNoteTitle"),
												null);
												*/
		}

		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
                labels.getString("PolicyNoteTabLabel"),
                labels.getString("BrokerNoteTabLabel")
		};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {
				getPolicyNoteList(),
				getBrokerNoteList()
		};
	}
	
	protected Component getPolicyNoteList() {
		if (policyNoteList == null) {
			policyNoteList = new BasePolicyNoteList(true, uiMode, uiContainerType);
		}
		
		return policyNoteList;
	}
	
	protected Component getBrokerNoteList() {
		if (brokerNoteList == null) {
			brokerNoteList = new BasePolicyNoteList(false, uiMode, uiContainerType);
		}
		
		return brokerNoteList;
	}
	
	public void loadData() {
		if (policyNoteList != null) {
			policyNoteList.loadData();
		}
		
		if (brokerNoteList != null) {
			brokerNoteList.loadData();
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		loadData();
		refreshPage();
	}

}
