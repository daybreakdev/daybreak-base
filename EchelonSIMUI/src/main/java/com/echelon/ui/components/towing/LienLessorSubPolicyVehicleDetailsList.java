package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;

public class LienLessorSubPolicyVehicleDetailsList extends LienLessorVehicleDetailsList<SpecialityAutoSubPolicy<Risk>, 
																LienholderLessorSubPolicy> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1368790187619043333L;

	public LienLessorSubPolicyVehicleDetailsList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<LienholderLessorVehicleDetails> getVehicleDetailsFromParent() {
		List<LienholderLessorVehicleDetails> list = new ArrayList<LienholderLessorVehicleDetails>();
		
		if (vehicleDetailsParent != null && vehicleDetailsParent.getLienholderLessorVehicleDetails() != null &&
				parentLienLessorHolder != null) {
			list.addAll(vehicleDetailsParent.getLienholderLessorVehicleDetails().stream()
								.filter(o -> (o.getLienholderLessor() != null && parentLienLessorHolder.getLienholderLessor().equals(o.getLienholderLessor())))
								.collect(Collectors.toList()));
		}
		
		return list;
	}

	@Override
	protected void newAction() {
		vehicleDetailsUIHelper.newAction(vehicleDetailsParent, parentLienLessorHolder);
	}

	@Override
	protected void updateAction(LienholderLessorVehicleDetails vehicleDetails) {
		vehicleDetailsUIHelper.updateAction(vehicleDetailsParent, parentLienLessorHolder, vehicleDetails);
	}

	@Override
	protected void deleteAction(LienholderLessorVehicleDetails vehicleDetails) {
		vehicleDetailsUIHelper.deleteAction(vehicleDetailsParent, parentLienLessorHolder, vehicleDetails);
	}

	@Override
	protected void undeleteAction(LienholderLessorVehicleDetails vehicleDetails) {
		vehicleDetailsUIHelper.undeleteAction(vehicleDetailsParent, parentLienLessorHolder, vehicleDetails);
	}

	@Override
	protected void viewAction(LienholderLessorVehicleDetails vehicleDetails) {
		vehicleDetailsUIHelper.viewAction(vehicleDetailsParent, parentLienLessorHolder, vehicleDetails);
	}

	@Override
	protected String getLienLessorType(LienholderLessorSubPolicy lienLessor) {
		return lienLessor != null ? lienLessor.getLienLessorType() : null;
	}

}
