package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;

public class CGLSubPolicyDialog extends DSUpdateDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6854684791027702963L;
	private CGLSubPolicyForm 	cglSubPolicyForm;

	public CGLSubPolicyDialog (String title, String action) {
		super();
		this.cglSubPolicyForm = new CGLSubPolicyForm(action, UICONTAINERTYPES.Dialog);
		initDialog(title, cglSubPolicyForm, action);
	}

	public void loadData(CGLSubPolicy<?> subPolicy) {
		cglSubPolicyForm.loadData(subPolicy);
		cglSubPolicyForm.enableEdit();
	}

	public boolean validateEdit() {
		return cglSubPolicyForm.validateEdit();
	}

	public boolean saveEdit(CGLSubPolicy<?> subPolicy) {
		return cglSubPolicyForm.saveEdit(subPolicy);
	}

}
