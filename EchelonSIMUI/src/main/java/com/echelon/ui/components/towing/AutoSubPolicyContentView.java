package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.entities.AdditionalInsuredAndVehiclesView;
import com.echelon.ui.components.entities.LienholderLessorSubPolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.echelon.ui.helpers.entities.LienholderLessorSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="AutomobileSubPolicyNodeContentView", layout = BasePolicyView.class)
public class AutoSubPolicyContentView extends BaseTabLayout implements IPolicyViewContent {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3201821105536126L;
	private AutoSubPolicyUIHelper				subPolicyUIHelper 	= new AutoSubPolicyUIHelper();
	private AutoSubPolicyView					subPolicyView;
	private SubPolicyCoverageList				covList;
	private AdditionalInsuredAndVehiclesView	addInsuredList;
	private AdditionalInsuredUIHelper			addInsuredUIHelper  = new AdditionalInsuredUIHelper();
	private LienholderLessorSubPolicyView		lienLessorView;
	private LienholderLessorSubPolicyUIHelper	lienLessorUIHelper	= new LienholderLessorSubPolicyUIHelper();

	public AutoSubPolicyContentView() {
		super();
	}
	
	public AutoSubPolicyContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this,
												null,
												null);
		}

		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		// TODO Auto-generated method stub
		return new String[] {
		                  labels.getString("AutoSubPolicyDetailsTitle"),
		                  labels.getString("AdditionalInsuredTabLabel"),
		                  labels.getString("LienLessorTabLabel"),
		                  labels.getString("CoverageTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		// TODO Auto-generated method stub
		return new Component[] {
				getSubPolicyView(),
				getAdditionalInsuredList(),
				getLienholderLessorList(),
				getSubPolicyCoverageList()
		};
	}
	
	protected Component getSubPolicyView() {
		if (subPolicyView == null) {
			subPolicyView = new AutoSubPolicyView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return subPolicyView;
	}
	
	protected Component getAdditionalInsuredList() {
		if (addInsuredList == null) {
			addInsuredList = new AdditionalInsuredAndVehiclesView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return addInsuredList;
	}
	
	protected Component getSubPolicyCoverageList() {
		if (covList == null) {
			covList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.AUTOSubPolicy, uiMode, UICONTAINERTYPES.Tab);
		}
		
		return covList;
	}
	
	protected Component getLienholderLessorList() {
		if (lienLessorView == null) {
			lienLessorView = new LienholderLessorSubPolicyView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return lienLessorView;
	}

	public void loadData(SpecialityAutoSubPolicy autoSubPolicy) {
		if (subPolicyView != null) {
			subPolicyUIHelper.loadView(subPolicyView, autoSubPolicy);
		}
		if (addInsuredList != null) {
			addInsuredUIHelper.loadView(addInsuredList, autoSubPolicy);
		}
		if (covList != null) {
			((AutoSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.AUTOSubPolicy)).loadView(covList, autoSubPolicy);
		}
		if (lienLessorView != null) {
			lienLessorUIHelper.loadView(lienLessorView, autoSubPolicy);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		SpecialityAutoSubPolicy autoSubPolicy = null;
		if (nodeItem != null) {
			autoSubPolicy = (SpecialityAutoSubPolicy) nodeItem.getNodeObject();
		}

		final SpecialityAutoSubPolicy openSubPolicy = autoSubPolicy;
		subPolicyUIHelper.loadSubpolicyData(new AutoSubPolicyUIHelper.LoadSubPolicyDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData(openSubPolicy);
				refreshPage();
			}
		}, autoSubPolicy);
	}
}
