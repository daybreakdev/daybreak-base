package com.echelon.ui.components.policy.wheels;


import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverSuspensionUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverSuspension;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

public class DriverSuspensionForm extends BaseLayout implements IDSFocusAble {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7392889579947019830L;
	protected DSComboBox<String>			suspensionTypeField;
	protected DSComboBox<String>			suspensionReasonField;
	protected DatePicker					suspensionStartField;
	protected DatePicker					suspensionEndField;
	protected IntegerField					suspensionLengthField;
	
	protected DriverSuspensionUIHelper		suspensionUIHelper;
	protected DriverSuspension 				currSuspension;
	protected Binder<DriverSuspension>		binder;

	public DriverSuspensionForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.suspensionUIHelper = new DriverSuspensionUIHelper();
		this.setSizeFull();
		
		buildFields();
		buildForm();
	}
	

	protected void buildFields() {
		
		suspensionTypeField					= new DSComboBox<String>(labels.getString("DriverSuspensionFormTypeLabel"), String.class)
													.withLookupTableId(ConfigConstants.LOOKUPTABLE_SuspensionType);
		suspensionReasonField				= new DSComboBox<String>(labels.getString("DriverSuspensionFormReasonLabel"), String.class)
													.withLookupTableId(ConfigConstants.LOOKUPTABLE_SuspensionReason);
		suspensionStartField				= new DatePicker(labels.getString("DriverSuspensionFormStartDateLabel"));
		suspensionEndField					= new DatePicker(labels.getString("DriverSuspensionFormEndDateLabel"));
		suspensionLengthField				= new IntegerField(labels.getString("DriverSuspensionFormLengthLabel"));

		

		binder = new Binder<DriverSuspension>(DriverSuspension.class);
		binder.forField(suspensionTypeField).asRequired().bind(DriverSuspension::getSuspensionType, DriverSuspension::setSuspensionType);
		binder.forField(suspensionReasonField).asRequired().bind(DriverSuspension::getSuspensionReason, DriverSuspension::setSuspensionReason);
		binder.forField(suspensionStartField).asRequired().bind(DriverSuspension::getSuspensionStartDate, DriverSuspension::setSuspensionStartDate);
		binder.forField(suspensionEndField).asRequired().bind(DriverSuspension::getSuspensionEndDate, DriverSuspension::setSuspensionEndDate);
		
		binder.setReadOnly(true);
		uiHelper.setEditFieldsReadonly(suspensionLengthField);

	}
	
	protected void buildForm() {
		FormLayout suspensionForm = buildSuspensionForm();
		this.add(suspensionForm);
	}
	
	protected FormLayout buildSuspensionForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );
		
		form.add(suspensionTypeField, suspensionReasonField, suspensionLengthField);
		form.add(suspensionStartField, suspensionEndField);
		
		return form;
	}
	
	
	private void suspensionDateValueChanged(ValueChangeEvent<?> event) {
		suspensionLengthField.setValue(suspensionUIHelper.getSuspensionLength(suspensionStartField.getValue(), 
																				suspensionEndField.getValue()));		
	}

		
	public void loadData(Driver driver, DriverSuspension suspension) {
		loadDataConfigs(binder);
				
		binder.readBean(suspension);
					
		if (suspension.getSuspensionStartDate() != null && 
				suspension.getSuspensionEndDate() != null) {
			suspensionLengthField.setValue(suspensionUIHelper.getSuspensionLength(suspensionStartField.getValue(), 
																					suspensionEndField.getValue()));
		} 
		
		enableDataView(binder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		suspensionTypeValueChanged(null);
		
		setupFieldValueChanges();
		setFocus();
	}
	
	public boolean saveEdit(DriverSuspension suspension) {
		boolean ok1 = uiHelper.writeValues(binder, suspension);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}
	
	protected void setupFieldValueChanges() {
		suspensionStartField.addValueChangeListener(this::suspensionDateValueChanged);
		suspensionStartField.addValueChangeListener(e -> suspensionEndField.setMin(e.getValue()));
		suspensionEndField.addValueChangeListener(this::suspensionDateValueChanged);
		suspensionTypeField.addValueChangeListener(this::suspensionTypeValueChanged);
	}

	private void suspensionTypeValueChanged(ValueChangeEvent<?> event) {
		suspensionReasonField.loadDSItems(suspensionTypeField.getValue());
	}
}