package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.data.binder.Binder;

@SuppressWarnings("serial")
public abstract class BaseCovLimitAndDedField extends Div {

	protected Binder<CoverageDto> 		binder;
	protected CoverageDto				coverageDto;
	protected DSNumberField<Integer> 	integerField;
	protected DSComboBox<Integer> 		lookupField;
	//protected Registration			lookupRegistration;
	
	protected BaseCoverageConfigUIHelper coverageConfigUIHelper;
	protected BaseCoverageUIHelper		 coverageUIHelper;
	protected UIHelper 				     uiHelper;
	
	public BaseCovLimitAndDedField(BaseCoverageConfigUIHelper coverageConfigUIHelper, BaseCoverageUIHelper coverageUIHelper) {
		super();

		this.coverageConfigUIHelper = coverageConfigUIHelper;
		this.coverageUIHelper		= coverageUIHelper;
		this.uiHelper				= BrokerUI.getHelperSession().getUiHelper();
		
		this.integerField 		= new DSNumberField<Integer>(null, Integer.class);
		this.lookupField  		= new DSComboBox<Integer>(null, Integer.class);
		//this.lookupRegistration = this.coverageConfigUIHelper.setupComboBoxField(this.lookupField);
		//this.lookupField.clearDSItems();
		
		this.setWidthFull();
		this.add(this.integerField, this.lookupField);
	}

	public DSNumberField<Integer> getIntegerField() {
		return integerField;
	}

	public DSComboBox<Integer> getLookupField() {
		return lookupField;
	}

	public void setLabel(String label) {
		integerField.setLabel(label);
		lookupField.setLabel(label);
	}

	public void setWidth(String width) {
		integerField.setWidth(width);
		lookupField.setWidth(width);
	}
	
	public void setBinder(Binder<CoverageDto> binder) {
		this.binder = binder;
	}
	
	protected void removeBinding() {
		try {
			binder.removeBinding(integerField);
		} catch (Exception e) {
		}
		try {
			binder.removeBinding(lookupField);
		} catch (Exception e) {
		}
	}
	
	protected boolean isInteger() {
		if (this.coverageDto == null) {
			return false;
		}

		return false;
	}
	
	protected boolean isAllowOverride() {
		if (this.coverageDto == null) {
			return false;
		}
		
		return false;
	}
	
	protected boolean isDeletedCoverage() {
		if (this.coverageDto == null) {
			return true;
		}
		
		return this.coverageDto.isDeletedCoverage() || this.coverageDto.wasDeletedCoverage();
	}
	
	protected void clearLookup() {
		lookupField.clear();
		lookupField.clearDSItems();
	}
	
	public void initializeField(CoverageDto dto) {
		this.coverageDto = dto;
		
		if (isInteger() || !isAllowOverride() || isDeletedCoverage()) {
			integerField.setVisible(true);
			lookupField.setVisible(false);
			clearLookup();
		}
		else {
			integerField.setVisible(false);
			lookupField.setVisible(true);
			clearLookup();
			setFieldItems(dto);
		}
	}
	
	protected void setFieldItems(CoverageDto dto) {

	}
	
	public void determineReadOnly() {
		if (isAllowOverride() && !isDeletedCoverage()) {
			uiHelper.setEditFieldsNotReadonly(integerField, lookupField);
			uiHelper.fieldTextAlightRight(lookupField);
		}
		else {
			uiHelper.setEditFieldsReadonly(integerField, lookupField);
		}
	}
	
	public void setFocus() {
		if (isInteger()) {
			integerField.focus();
		}
		else {
			lookupField.focus();
		}
	}
	
	public boolean isEnabled() {
		if (isInteger()) {
			return integerField.isEnabled();
		}
		else {
			return lookupField.isEnabled();
		}
	}
	
	public boolean isReadOnly() {
		if (isInteger()) {
			return integerField.isReadOnly();
		}
		else {
			return lookupField.isReadOnly();
		}
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && isAllowOverride() && !isDeletedCoverage() && value >= 0) {
			if (isInteger()) {
				integerField.setTabIndex(value);
				lookupField.setTabIndex(-1);
			}
			else {
				lookupField.setTabIndex(value);
				integerField.setTabIndex(-1);
			}
			return true;
		}
		else {
			integerField.setTabIndex(-1);
			lookupField.setTabIndex(-1);
			return false;
		}
	}
}
