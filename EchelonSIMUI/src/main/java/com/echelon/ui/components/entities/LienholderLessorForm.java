package com.echelon.ui.components.entities;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSPostalZipCodeField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class LienholderLessorForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8751010367849496298L;
	private DSComboBox<String> 			lienLessorTypeField;
	private TextField					lienLessorNameField;
	private	Label						addressLabel;
	private TextField					addressField;
	private	TextField					cityField;
	private	DSComboBox<String>			provinceField;
	private	DSPostalZipCodeField		postalzipField;
	private DSComboBox<String>			countryField;
	
	private Binder<LienholderLessorSubPolicy> 	spBinder;
	private Binder<LienholderLessor>			binder;
	private Binder<Address>						addBinder;

	public LienholderLessorForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForm();
	}
	
	private void buildFields() {
		lienLessorTypeField 		= new DSComboBox<String>(labels.getString("LienLessorFormPayeeTypeLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_LienLessorType);
		lienLessorNameField 		= new TextField(labels.getString("LienLessorFormPayeeNameLabel"));
		addressLabel				= new Label(labels.getString("LienLessorFormAddressSectionLabel"));
		uiHelper.setLabelHeaderStyle(addressLabel);
		addressField				= new TextField(labels.getString("LienLessorFormAddressLabel"));
		cityField					= new TextField(labels.getString("LienLessorFormCityLabel"));
		provinceField				= new DSComboBox<String>(labels.getString("LienLessorFormProvinceLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		postalzipField				= new DSPostalZipCodeField(labels.getString("LienLessorFormPostalZipLabel"));
		countryField				= new DSComboBox<String>(labels.getString("LienLessorFormCountryLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Countries);
		
		spBinder = new Binder<LienholderLessorSubPolicy>();
		spBinder.forField(lienLessorTypeField).asRequired()
							.bind(LienholderLessorSubPolicy::getLienLessorType, LienholderLessorSubPolicy::setLienLessorType);
		spBinder.setReadOnly(true);
		
		
		binder = new Binder<LienholderLessor>(LienholderLessor.class);
		binder.forField(lienLessorNameField).asRequired().bind(LienholderLessor::getCompanyName, LienholderLessor::setCompanyName);
		binder.setReadOnly(true);
		
		addBinder = new Binder<Address>(Address.class);
		addBinder.forField(addressField).asRequired()
											.bind(Address::getAddressLine1, Address::setAddressLine1);
		addBinder.forField(cityField).asRequired()
											.bind(Address::getCity, Address::setCity);
		addBinder.forField(provinceField).asRequired()
											.bind(Address::getProvState, Address::setProvState);
		addBinder.forField(postalzipField).asRequired()
											.withNullRepresentation("")
											.withConverter(postalzipField.getConverter())
											.withValidator(postalzipField.getValidator())
											.bind(Address::getPostalZip, Address::setPostalZip);
		addBinder.forField(countryField).asRequired().bind(Address::getCountry, Address::setCountry);
		addBinder.setReadOnly(true);
	}
	
	private void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		
		form.add(lienLessorTypeField, 2);
		uiHelper.addFormEmptyColumn(form);
		uiHelper.addFormEmptyColumn(form);
		form.add(lienLessorNameField, 2);
		uiHelper.addFormEmptyColumn(form);
		uiHelper.addFormEmptyColumn(form);
		
		form.add(addressLabel, 4);
		form.add(addressField, 2);
		form.add(cityField, provinceField);
		form.add(postalzipField, countryField);
		uiHelper.addFormEmptyColumn(form);
		uiHelper.addFormEmptyColumn(form);
		
		this.add(form);
	}
	
	public void loadData(LienholderLessorSubPolicy lessorHolder) {
		loadDataConfigs(spBinder);
		spBinder.readBean(lessorHolder);
		
		enableDataView(spBinder);
		
		loadData(lessorHolder.getLienholderLessor());
	}
	
	protected void loadData(LienholderLessor lessor) {
		loadDataConfigs(binder, addBinder);
		
		binder.readBean(lessor);
		addBinder.readBean(lessor.getLienholderLessorAddress());
		
		enableDataView(binder, addBinder);
	}
	
	public void enableEdit() {
		if (spBinder != null) {
			enableDataEdit(spBinder);
		}
		
		enableDataEdit(binder, addBinder);

		setFocus();
	}
	
	public boolean saveEdit(LienholderLessorSubPolicy lessorHolder) {
		boolean ok1 = uiHelper.writeValues(spBinder, lessorHolder);
		
		return ok1 && saveEdit(lessorHolder.getLienholderLessor());
	}
	
	protected boolean saveEdit(LienholderLessor lienLessor) {
		boolean ok1 = uiHelper.writeValues(binder, lienLessor);
		boolean ok2 = uiHelper.writeValues(addBinder, lienLessor.getLienholderLessorAddress()); 
		
		return ok1 && ok2;
	}
	
	public boolean validateEdit() {
		boolean ok1 = uiHelper.validateValues(spBinder, binder, addBinder);
		
		return ok1;
	}
	
	public void setFocus() {
		lienLessorTypeField.focus();
	}
	
	
}
