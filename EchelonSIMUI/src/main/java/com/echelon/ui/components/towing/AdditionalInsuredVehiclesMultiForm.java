package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.ui.components.baseclasses.MultiNewEntryForm;
import com.echelon.ui.helpers.policy.towing.AdditionalInsuredVehiclesUIHelper;

public class AdditionalInsuredVehiclesMultiForm extends MultiNewEntryForm<AdditionalInsuredVehiclesForm, AdditionalInsuredVehicles> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9117958395228886766L;
	private AdditionalInsuredVehiclesUIHelper addlInsuredVehiclesUIHelper = new AdditionalInsuredVehiclesUIHelper();

	public AdditionalInsuredVehiclesMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, 7);
	}

	@Override
	protected DSMultiNewEntryDto<AdditionalInsuredVehiclesForm, AdditionalInsuredVehicles> createMultiNewEntryDto() {
		// TODO Auto-generated method stub
		DSMultiNewEntryDto<AdditionalInsuredVehiclesForm, AdditionalInsuredVehicles> dto = super.createMultiNewEntryDto();
		dto.setData(addlInsuredVehiclesUIHelper.createEmptyAdditionalInsuredVehicles());
		
		AdditionalInsuredVehiclesForm form = new AdditionalInsuredVehiclesForm(uiMode, uiContainerType);
		form.loadData(dto.getData());
		form.enableEdit();
		dto.setForm(form);
		
		return dto;
	}

	@Override
	protected boolean formValidateEdit(
			DSMultiNewEntryDto<AdditionalInsuredVehiclesForm, AdditionalInsuredVehicles> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().validateEdit(false);
	}

	@Override
	protected boolean formSaveEdit(
			DSMultiNewEntryDto<AdditionalInsuredVehiclesForm, AdditionalInsuredVehicles> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().saveEdit(dto.getData());
	}

	public void enableEdit() {
		super.enableEdit();
		this.initialzieRow();
	}
}
