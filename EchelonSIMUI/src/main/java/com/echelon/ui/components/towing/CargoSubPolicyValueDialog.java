package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.session.UIFactory;

public class CargoSubPolicyValueDialog extends DSUpdateDialog {
	private static final long serialVersionUID = 330531847674414032L;
	CargoSubPolicyValueForm cargoSubPolicyValueForm;
	
	public CargoSubPolicyValueDialog (String title, String action) {
		super();
		this.cargoSubPolicyValueForm = UIFactory.getInstance().getCargoSubPolicyValueForm(action, UICONTAINERTYPES.Dialog, null);
		initDialog(title, cargoSubPolicyValueForm, action);
	}

	public CargoSubPolicyValueForm getCargoSubPolicyValueForm() {
		return cargoSubPolicyValueForm;
	}

	public void loadData(CargoSubPolicy<?> parentSubPolicy, CargoDetails cargoDetails) {
		cargoSubPolicyValueForm.loadData(parentSubPolicy, cargoDetails);
	}

	public boolean validate() {
		return cargoSubPolicyValueForm.validate();
	}

	public boolean save(CargoDetails cargoDetails) {
		return cargoSubPolicyValueForm.save(cargoDetails);
	}
	
	public void enableEdit() { 
		cargoSubPolicyValueForm.enableEdit();
	}
}
