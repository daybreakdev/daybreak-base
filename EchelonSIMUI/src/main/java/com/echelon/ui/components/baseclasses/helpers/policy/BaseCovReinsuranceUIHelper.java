package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalLong;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.Range;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.CoverageReinsuranceDto;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageReinsuranceForm;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageReinsuranceList;
import com.echelon.ui.components.baseclasses.process.policy.BaseCoverageReinsuranceProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.CoverageUIFactory;

public class BaseCovReinsuranceUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3046815385159844857L;
	
	public interface ActionCallback {
		public void actionComplete(String action, CoverageReinsuranceDto covReinsuranceDto);
	}

	public BaseCovReinsuranceUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected BaseCoverageReinsuranceProcessor getCoverageReinsuranceProcessor() {
		return (BaseCoverageReinsuranceProcessor) new BaseCoverageReinsuranceProcessor(UIConstants.UIACTION_New, policyUIHelper.getCurrentPolicyTransaction())
				.withProcessorValues(BrokerUI.getUserSession().getUserProfile(), configUIHelper.getCurrentProductConfig(), policyUIHelper.getPolicyProcessing());
	}
	
	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<BaseCoverageReinsuranceList> createCovReinsuranceListDialog(String action, UIConstants.OBJECTTYPES parentObjectType, boolean isCoverage) {
		DSUpdateDialog<BaseCoverageReinsuranceList> dialog = (DSUpdateDialog<BaseCoverageReinsuranceList>) createDialog(
														uiHelper.getSessionLabels().getString(isCoverage ? "CoverageReinsuranceListTitle" : "EndorsementReinsuranceListTitle"), 
														CoverageUIFactory.getInstance().createCoverageReinsuranceList(parentObjectType, isCoverage, action, UICONTAINERTYPES.Dialog), 
														action);
		dialog.setWidth("1550px");
		dialog.setHeight("550px");
		return dialog;
	}

	public void onOpenReinsuranceList(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, UIConstants.OBJECTTYPES parentObjectType) {
		DSUpdateDialog<BaseCoverageReinsuranceList> dialog = createCovReinsuranceListDialog(UIConstants.UIACTION_Open, parentObjectType, coverageDto.isACoverage());
		
		// hide all the default buttons
		if (dialog.getToolbar() != null && 
			dialog.getToolbar().getButtons() != null &&
			!dialog.getToolbar().getButtons().isEmpty()) {
			dialog.getToolbar().getButtons().forEach(o -> {o.setEnabled(false); o.setVisible(false);});
		}
		
		dialog.setDataStore(new DSDialogDataStore(covConfigParent, covParent, coverageDto));
		dialog.getContentComponent().loadData(dialog, coverageDto);
		dialog.open();
	}
	
	@SuppressWarnings("unchecked")
	public void onCloseReinsuranceList(IDSDialog _dialog, String action) {
		if (_dialog != null) {
			if (UIConstants.UIACTION_View.equalsIgnoreCase(action)) {
				_dialog.close();
			}
			else {
				DSUpdateDialog<BaseCoverageReinsuranceList> dialog = (DSUpdateDialog<BaseCoverageReinsuranceList>) _dialog;
				//Show the messages only and let the parent screen (coverage edit grid) handles the exit
				dialog.getContentComponent().validEdit(dialog);
				//if (dialog.getContentComponent().validEdit(dialog)) {
					dialog.close();
				//}
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<BaseCoverageReinsuranceForm> createCovReinsuranceFormDialog(String action, String title, UIConstants.OBJECTTYPES parentObjectType, boolean isCoverage, Boolean isPrimary) {
		DSUpdateDialog<BaseCoverageReinsuranceForm> dialog = (DSUpdateDialog<BaseCoverageReinsuranceForm>) createDialog(
														uiHelper.getSessionLabels().getString(title), 
														new BaseCoverageReinsuranceForm(action, UICONTAINERTYPES.Dialog, parentObjectType, isCoverage, isPrimary), 
														action);
		return dialog;
	}

	public void onNewReinsurance(CoverageDto coverageDto, List<CoverageReinsuranceDto> covReinsuranceDtos, UIConstants.OBJECTTYPES parentObjectType, ActionCallback callback) {
		try {
			CoverageReinsuranceDto covReinsuranceDto = null;
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				covReinsuranceDto = new CoverageReinsuranceDto(coverageDto, getCoverageReinsuranceProcessor().createEmptyCoverageReinsurance(coverageDto));
			}
			else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				covReinsuranceDto = new CoverageReinsuranceDto(coverageDto, getCoverageReinsuranceProcessor().createEmptyEndorsementReinsurance(coverageDto));
			}
			
			DSUpdateDialog<BaseCoverageReinsuranceForm> dialog = createCovReinsuranceFormDialog(UIConstants.UIACTION_New, 
																						(coverageDto.isACoverage() ? "CoverageReinsuranceNewDialogTitle" : "EndorsementReinsuranceNewDialogTitle"), 
																						parentObjectType,
																						coverageDto.isACoverage(),
																						covReinsuranceDto.getIsPrimaryInsurer());
			dialog.setDataStore(new DSDialogDataStore(coverageDto, covReinsuranceDto, callback));
			dialog.getContentComponent().loadData(coverageDto, covReinsuranceDto, covReinsuranceDtos);
			dialog.getContentComponent().enabledEdit();
			dialog.open();
		} catch (Exception e) {
			uiHelper.notificationException(e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<BaseCoverageReinsuranceForm> dialog = event.getSource();
		CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData1();
		CoverageReinsuranceDto covReinsuranceDto = (CoverageReinsuranceDto) dialog.getDataStore().getData2();
		ActionCallback callBack = (ActionCallback) dialog.getDataStore().getData3();
		if (dialog.getContentComponent().validEdit() && dialog.getContentComponent().saveEdit(covReinsuranceDto)) {
			coverageDto.addReinsuranceDto(covReinsuranceDto);
			covReinsuranceDto.setNew(true);
			dialog.close();
			callBack.actionComplete(UIConstants.UIACTION_New, covReinsuranceDto);
		}
	}

	public void onUpdateReinsurance(CoverageDto coverageDto, CoverageReinsuranceDto covReinsuranceDto, List<CoverageReinsuranceDto> covReinsuranceDtos, UIConstants.OBJECTTYPES parentObjectType, ActionCallback callback) {
		DSUpdateDialog<BaseCoverageReinsuranceForm> dialog = createCovReinsuranceFormDialog(UIConstants.UIACTION_Update, 
																					(coverageDto.isACoverage() ? "CoverageReinsuranceUpdateDialogTitle" : "EndorsementReinsuranceUpdateDialogTitle"), 
																					parentObjectType,
																					coverageDto.isACoverage(),
																					covReinsuranceDto.getIsPrimaryInsurer());
		dialog.setDataStore(new DSDialogDataStore(coverageDto, covReinsuranceDto, callback));
		dialog.getContentComponent().loadData(coverageDto, covReinsuranceDto, covReinsuranceDtos);
		dialog.getContentComponent().enabledEdit();
		dialog.open();
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<BaseCoverageReinsuranceForm> dialog = event.getSource();
		CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData1();
		CoverageReinsuranceDto covReinsuranceDto = (CoverageReinsuranceDto) dialog.getDataStore().getData2();
		ActionCallback callBack = (ActionCallback) dialog.getDataStore().getData3();
		if (dialog.getContentComponent().validEdit() && dialog.getContentComponent().saveEdit(covReinsuranceDto)) {
			covReinsuranceDto.setUpdated(true);
			dialog.close();
			callBack.actionComplete(UIConstants.UIACTION_Save, covReinsuranceDto);
		}
	}

	public void onDeleteReinsurance(CoverageDto coverageDto, CoverageReinsuranceDto covReinsuranceDto, UIConstants.OBJECTTYPES parentObjectType, ActionCallback callback) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString(coverageDto.isACoverage() ? "CoverageReinsuranceFormDeleteDialogMessage" : "EndorsementReinsuranceFormDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(coverageDto, covReinsuranceDto, callback));
		dialog.open();
	}

	@SuppressWarnings("unused")
	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData1();
		CoverageReinsuranceDto covReinsuranceDto = (CoverageReinsuranceDto) dialog.getDataStore().getData2();
		ActionCallback callBack = (ActionCallback) dialog.getDataStore().getData3();
		covReinsuranceDto.setDeleted(true);
		covReinsuranceDto.setUndeleted(false);
		dialog.close();
		callBack.actionComplete(UIConstants.UIACTION_Delete, covReinsuranceDto);
	}

	public void onUndeleteReinsurance(CoverageDto coverageDto, CoverageReinsuranceDto covReinsuranceDto, OBJECTTYPES parentObjectType, ActionCallback callback) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString(coverageDto.isACoverage() ? "CoverageReinsuranceFormUndeleteDialogMessage" : "EndorsementReinsuranceFormUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(coverageDto, covReinsuranceDto, callback));
		dialog.open();
	}
	
	@Override
	protected void onUndelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		@SuppressWarnings("unused")
		CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData1();
		CoverageReinsuranceDto covReinsuranceDto = (CoverageReinsuranceDto) dialog.getDataStore().getData2();
		ActionCallback callback = (ActionCallback) dialog.getDataStore().getData3();
		covReinsuranceDto.setDeleted(false);
		covReinsuranceDto.setUndeleted(true);
		dialog.close();
		callback.actionComplete(UIConstants.UIACTION_Undelete, covReinsuranceDto);
	}
	
	public List<LookupTableItem> getPolicyReinsurerLookupFiltered(Boolean isPrimary, List<CoverageReinsuranceDto> dtos) {
		List<Long> reinsurerPks = null;
		if (dtos != null && !dtos.isEmpty()) {
			reinsurerPks = dtos.stream().filter(o -> o.isPendingOrActive())
				.map(CoverageReinsuranceDto::getPolicyReinsurer).collect(Collectors.toList())
				.stream().map(PolicyReinsurer::getPolicyReinsurerPK).collect(Collectors.toList());
		}
		
		if (BooleanUtils.isNotTrue(isPrimary)) {
			if (reinsurerPks == null) {
				reinsurerPks = new ArrayList<>();
			}
			
			PolicyReinsurer primaryPolicyReinsurer = getPrimaryPolicyReinsurer();
			if (primaryPolicyReinsurer != null) {
				reinsurerPks.add(primaryPolicyReinsurer.getPolicyReinsurerPK());
			}
		}
		
		return getPolicyReinsurerLookup(reinsurerPks);
	}
	
	//filterValues = list of PolicyReinsurerPK
	public List<LookupTableItem> getPolicyReinsurerLookup(List<Long> filterValues) {
		List<LookupTableItem> results = new ArrayList<LookupTableItem>();
		
		if (policyUIHelper.getCurrentPolicyTransaction() != null &&
				policyUIHelper.getCurrentPolicyTransaction().getPolicyReinsurers() != null && 
				!policyUIHelper.getCurrentPolicyTransaction().getPolicyReinsurers().isEmpty()) {
			for(PolicyReinsurer pr : policyUIHelper.getCurrentPolicyTransaction().getPolicyReinsurers()) {
				if (filterValues != null && !filterValues.isEmpty() && filterValues.contains(pr.getPolicyReinsurerPK())) {
					// selected
				}
				else {
					results.add(new LookupTableItem(pr.getPolicyReinsurerPK().toString(), pr.getReinsurer().getLegalName(), pr));
				}
			}
		}
		
		return results;
	}
	
	/*
	 * Formulas:
	 * 
	 * Gross-up factor = 1 / ( ( 1 - [“gross-up percentage figure”] ) % )
	 * 
	 * Grossed-up reinsurance premium = [reinsurance premium] * [gross-up factor]
	 * 
	 * Gross-up amount = [gross-up reinsurance premium] - [reinsurance premium]
	 */	
	public Double[] calculateGrossUpAmount(Double reinsurancePremium, Double grossUpPercent) {
		Double[] results = new Double[] {0D, 0D};
		
		if (reinsurancePremium != null && reinsurancePremium > 0D) {
			Double grossUpFactor = 0D;
			Double grossAmount   = 0D;
			Double grossPremium  = 0D;
			
			if (grossUpPercent != null && grossUpPercent != 0) {
				grossUpFactor = 1 / (1 - (grossUpPercent / 100));
			}
			else {
				grossUpFactor = 1D;
			}
			
			grossPremium = new BigDecimal(reinsurancePremium * grossUpFactor).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			grossAmount  = new BigDecimal(grossPremium - reinsurancePremium).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
			
			results[0] = grossAmount;
			results[1] = grossPremium;
		}
		
		return results;
	}

	public Long getReinsuranceDetainedLimit(List<CoverageReinsuranceDto> list) {
		return getReinsuranceDetainedLimit(list, null, null);
	}
	
	public Long getReinsuranceDetainedLimit(List<CoverageReinsuranceDto> list, CoverageReinsuranceDto covReinsuranceDto, Long newValue) {
		Long result = 0L;
		if (list != null && !list.isEmpty()) {
			OptionalLong rsLong = list.stream().filter(o -> (covReinsuranceDto == null || !o.equals(covReinsuranceDto)) 
															&& o.isPendingOrActive() && o.getEndLayer() != null).mapToLong(CoverageReinsuranceDto::getEndLayer).max();
			result = (rsLong.isPresent() ? rsLong.getAsLong() : 0);
		}
		
		if (newValue != null) {
			result = Long.max(result, newValue);
		}
		
		return result;
	}

	public Double getTotalReinsurancePremium(List<CoverageReinsuranceDto> list) {
		return getTotalReinsurancePremium(list, null, null);
	}
	
	public Double getTotalReinsurancePremium(List<CoverageReinsuranceDto> list, CoverageReinsuranceDto covReinsuranceDto, Double newValue) {
		Double result = 0D;
		if (list != null && !list.isEmpty()) {
			result = list.stream().filter(o -> (covReinsuranceDto == null || !o.equals(covReinsuranceDto))
												&& o.isPendingOrActive() && o.getReinsurancePremium() != null).mapToDouble(CoverageReinsuranceDto::getReinsurancePremium).sum();
		}
		
		if (newValue != null) {
			result = Double.sum(result, newValue);
		}
		
		return result;
	}
	
	// try to keep the dialog open until all message shown
	public boolean validateReinsurances(CoverageDto coverageDto) {
		boolean ok = true;
		
		List<String> errors = validateReinsurancesErrors(coverageDto);
		if (errors != null && !errors.isEmpty()) {
			ok = false;
			
			errors.stream().forEach(o -> coverageDto.addEditMessage(o));
		}
		
		List<String> warns = validateReinsurancesWarns(coverageDto);
		if (warns != null && !warns.isEmpty()) {
			warns.stream().forEach(o -> coverageDto.addEditMessage(o));
		}
		
		return ok;
	}
	
	protected List<String> validateReinsurancesErrors(CoverageDto coverageDto) {
		List<String> errors = new ArrayList<String>();
		
		if (coverageDto.getReinsuranceDtos() != null && !coverageDto.getReinsuranceDtos().isEmpty()) {
			List<CoverageReinsuranceDto> list = coverageDto.getReinsuranceDtos().stream().filter(o -> o.isPendingOrActive()).collect(Collectors.toList());
			
			boolean nullLowerLimit = list.stream().filter(o -> o.getStartLayer() == null).findFirst().isPresent();
			boolean nullUpperLimit = list.stream().filter(o -> o.getEndLayer() == null).findFirst().isPresent();
			
			if (nullLowerLimit) {
				errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesNullLowerLimitError"));
			}
			
			if (nullUpperLimit) {
				errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesNullUpperLimitError"));
			}
			
			if (!nullLowerLimit && !nullUpperLimit) {
				if (!list.stream().filter(o -> o.getStartLayer().longValue() == 0).findFirst().isPresent()) {
					errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesMissingZeroLowerLimitError"));
				}
				
				List<Range<Long>> layerRanges = new ArrayList<Range<Long>>();
				for(CoverageReinsuranceDto dto : list) {
					double sum = list.stream().filter(o ->  o.getStartLayer().longValue() == dto.getStartLayer().longValue() 
														 && o.getEndLayer().longValue()   == dto.getEndLayer().longValue()
														 && o.getLayerPercentage() 		  != null)
										.mapToDouble(CoverageReinsuranceDto::getLayerPercentage)
										.sum();
					if (sum != 100) {
						errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesSamePositionNot100PercentError"));
						break;
					}
					
					layerRanges.add(Range.between(dto.getStartLayer(), dto.getEndLayer()));
				}
				
				layerRanges.sort(Comparator.comparing(Range<Long>::getMinimum).thenComparing(Range<Long>::getMaximum));
				
				Range<Long> prevRange = null;
				for(Range<Long> range : layerRanges) {
					if (prevRange == null) {
					}
					else if (!prevRange.equals(range) &&
							 prevRange.getMaximum().longValue() > range.getMinimum().longValue()) {
						errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesLimitRangeOverlapError"));
						break;
					}
					
					prevRange = range;
				}
				
				Long max = getReinsuranceDetainedLimit(coverageDto.getReinsuranceDtos());
				prevRange = null;
				for(Range<Long> range : layerRanges.stream().filter(o -> o.getMaximum() != max).collect(Collectors.toList())) {
					if (prevRange == null) {
					}
					else if (!prevRange.equals(range) &&
							 prevRange.getMaximum().longValue() < range.getMinimum().longValue()) {
						errors.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesLimitRangeGapError"));
						break;
					}
					
					prevRange = range;
				}
			}
		}
		
		return errors;
	}
	
	protected List<String> validateReinsurancesWarns(CoverageDto coverageDto) {
		List<String> warns = new ArrayList<String>();
		
		if (coverageDto.getReinsuranceDtos() != null && !coverageDto.getReinsuranceDtos().isEmpty()) {
			List<CoverageReinsuranceDto> list = coverageDto.getReinsuranceDtos().stream().filter(o -> o.isPendingOrActive()).collect(Collectors.toList());
			
			Integer policyLiabilityLimit = getPolicyLiabilityLimit();
			if (policyLiabilityLimit != null && policyLiabilityLimit > 0) {
				Long detainedLimit = getReinsuranceDetainedLimit(list);
				if (detainedLimit.intValue() != policyLiabilityLimit.intValue()) {
					warns.add(uiHelper.getSessionLabels().getString("CoverageReinsurancesExceedLimitWarn"));
				}
			}
		}
		
		return warns;
	}

	public Integer getPolicyLiabilityLimit() {
		Integer result = null;

		return result;
	}

	protected PolicyReinsurer getPrimaryPolicyReinsurer() {
		return null;
	}
}
