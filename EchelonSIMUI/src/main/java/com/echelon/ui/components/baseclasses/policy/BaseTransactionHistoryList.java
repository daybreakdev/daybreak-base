package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLabelRenderder;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseTransactionHistoryUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.data.renderer.LocalDateRenderer;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.router.Route;

@Route(value="TransactionHistoryContentView", layout = BasePolicyView.class)
public class BaseTransactionHistoryList extends BaseLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9172653208729965985L;
	private static final String COLUMN_DELETEBUTTON = "DEL";
	
	protected Grid<PolicyHistoryDTO> 			historyGrid;
	protected BaseTransactionHistoryUIHelper	transHistoryUIHelper;
	
	public BaseTransactionHistoryList() {
		super();
		// TODO Auto-generated constructor stub
		
		this.transHistoryUIHelper = new BaseTransactionHistoryUIHelper();
		
		initializeLayouts();
	}

	public BaseTransactionHistoryList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
	}
	
	protected void initializeLayouts() {
		this.setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													labels.getString("TransactionHistoryTitle"),
													null);
		}
		
		buildGrid();
	}

	protected void buildGrid() {
		historyGrid = new Grid<PolicyHistoryDTO>();
		historyGrid.setSizeFull();
		uiHelper.setGridStyle(historyGrid);
		
		historyGrid.addColumn(new DSLabelRenderder<>(PolicyHistoryDTO::getPolicyTxnType, "PolicyTxnType", null)).setHeader(labels.getString("TransactionHistoryColumnVerTxhTypeLabel"))
						.setSortable(true);
		historyGrid.addColumn(PolicyHistoryDTO::getVersionDescription).setHeader(labels.getString("TransactionHistoryColumnVerDescriptionLabel"))
						.setSortable(true);
		historyGrid.addColumn(new NumberRenderer<>(PolicyHistoryDTO::getTermNumber, DSNumericFormat.getIntegerInstance())).setHeader(labels.getString("TransactionHistoryColumnVerTermLabel"))
						.setTextAlign(ColumnTextAlign.END);
		historyGrid.addColumn(new NumberRenderer<>(PolicyHistoryDTO::getPolicyVersionNum, DSNumericFormat.getIntegerInstance())).setHeader(labels.getString("TransactionHistoryColumnVerSeqLabel"))
								.setTextAlign(ColumnTextAlign.END)
						.setSortable(true);
		//historyGrid.addColumn(new LocalDateRenderer<>(PolicyHistoryDTO::getTransactionDate, DSUIConstants.SYSDATEFORMAT)).setHeader(labels.getString("TransactionHistoryColumnTransDateLabel"));
		//historyGrid.addColumn(new LocalDateRenderer<>(PolicyHistoryDTO::getIssueDate, DSUIConstants.SYSDATEFORMAT)).setHeader(labels.getString("TransactionHistoryColumnVerIssueDateLabel"));
		historyGrid.addColumn(new LocalDateRenderer<>(PolicyHistoryDTO::getVersionDate, UIConstants.SYSDATEFORMAT)).setHeader(labels.getString("TransactionHistoryColumnVerDateLabel"));
		historyGrid.addColumn(new LocalDateTimeRenderer<>(PolicyHistoryDTO::getTxnCreatedDate, UIConstants.SYSDATEFORMAT)).setHeader(labels.getString("TransactionHistoryColumnCreatedDateLabel"));
		historyGrid.addColumn(new NumberRenderer<>(PolicyHistoryDTO::getTxnNetPremiumChange, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("TransactionHistoryColumnTransPremLabel"))
								.setTextAlign(ColumnTextAlign.END);
		historyGrid.addColumn(new NumberRenderer<>(PolicyHistoryDTO::getTxnWrittenPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("TransactionHistoryColumnWrittenPremLabel"))
								.setTextAlign(ColumnTextAlign.END);
		//historyGrid.addColumn(new DSLabelRenderder<>(PolicyHistoryDTO::getTermStatus, "PolicyBusinessStatus", null)).setHeader(labels.getString("TransactionHistoryColumnTermStatusLabel"));
		historyGrid.addColumn(new DSLabelRenderder<>(PolicyHistoryDTO::getVersionBusinessStatus, "PolicyBusinessStatus", null)).setHeader(labels.getString("TransactionHistoryColumnVersionStatusLabel"));
		historyGrid.addComponentColumn(o -> {
				Button btn = new Button(VaadinIcon.EDIT.create());
				btn.addClickListener(e -> {
					transHistoryUIHelper.setupPolicyUpdateAction(o);
				});
				return btn;
			}).setWidth("6px").setKey(COLUMN_DELETEBUTTON);
		
		historyGrid.setSelectionMode(SelectionMode.SINGLE);
		historyGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		historyGrid.addItemClickListener(this::openPolicy);
		
		this.add(historyGrid);
	}
	
	private void openPolicy(ItemClickEvent<PolicyHistoryDTO> event) {
		transHistoryUIHelper.openPolicy(event.getItem());
	}
	
	public void loadData() {
		if (BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable()) {
			historyGrid.getColumnByKey(COLUMN_DELETEBUTTON).setVisible(true);
		}
		else {
			historyGrid.getColumnByKey(COLUMN_DELETEBUTTON).setVisible(false);
		}
		
		transHistoryUIHelper.loadPolicyTransactionHistories(new BaseTransactionHistoryUIHelper.LoadPolicyTransactionHistoriesCallback() {
			
			@Override
			public void historiesIsReady(List<PolicyHistoryDTO> transactionHistores) {
				List<PolicyHistoryDTO> wrkList2 = new ArrayList<PolicyHistoryDTO>();
				if (transactionHistores != null && !transactionHistores.isEmpty()) {
					wrkList2 = transactionHistores.stream().sorted(Comparator.comparing(PolicyHistoryDTO::getPolicyVersionNum))
										.collect(Collectors.toList());
				}		
				historyGrid.setItems(wrkList2);
			}
		});
	}
	
	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		loadData();
	}
}
