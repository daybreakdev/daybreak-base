package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.entities.AdditionalInsuredList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="GarageSubPolicyNodeContentView", layout = BasePolicyView.class)
public class GarageSubPolicyContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132388023659436469L;
	private GarageSubPolicyUIHelper			subPolicyUIHelper 	= new GarageSubPolicyUIHelper();
	private GarageSubPolicyView				subPolicyView;
	private SubPolicyCoverageList	  		covList;
	private AdditionalInsuredList			addInsuredList;
	private AdditionalInsuredUIHelper		addInsuredUIHelper  = new AdditionalInsuredUIHelper();

	public GarageSubPolicyContentView() {
		super();
	}

	public GarageSubPolicyContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this, null, null);
		}
		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
		                  labels.getString("GarageSubPolicyDetailsTitle"),
		                  //labels.getString("AdditionalInsuredTabLabel"),
		                  labels.getString("CoverageTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {
				getSubPolicyView(),
				//getAdditionalInsuredList(),
				getSubPolicyCoverageList()
		};
	}

	protected Component getSubPolicyView() {
		if (subPolicyView == null) {
			subPolicyView = new GarageSubPolicyView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return subPolicyView;
	}

	protected Component getAdditionalInsuredList() {
		if (addInsuredList == null) {
			addInsuredList = new AdditionalInsuredList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return addInsuredList;
	}

	protected Component getSubPolicyCoverageList() {
		if (covList == null) {
			covList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.GARSubPolicy, uiMode, UICONTAINERTYPES.Tab);
		}

		return covList;
	}

	public void loadData(GarageSubPolicy<?> garageSubPolicy) {
		if (subPolicyView != null) {
			subPolicyUIHelper.loadView(subPolicyView, garageSubPolicy);
		}
		if (addInsuredList != null) {
			addInsuredUIHelper.loadView(addInsuredList, garageSubPolicy);
		}
		if (covList != null) {
			((GarageSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.GARSubPolicy)).loadView(covList, garageSubPolicy);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		if (nodeItem == null) {
			return;
		}
		
		final GarageSubPolicy<?> garageSubPolicy = (GarageSubPolicy<?>) nodeItem.getNodeObject();

		subPolicyUIHelper.loadSubpolicyData(new GarageSubPolicyUIHelper.LoadSubPolicyDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData(garageSubPolicy);
				refreshPage();
				
				tab.setEnabled(nodeItem.getNodeObject() != null);
			}
		}, garageSubPolicy);
	}

}
