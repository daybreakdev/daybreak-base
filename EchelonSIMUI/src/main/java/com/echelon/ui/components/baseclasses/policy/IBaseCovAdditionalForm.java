package com.echelon.ui.components.baseclasses.policy;

public interface IBaseCovAdditionalForm extends IBaseCovAndEndorseForm {

	public boolean hasAnyAdditionalFields();
	public boolean isDataInstanceModified();
}
