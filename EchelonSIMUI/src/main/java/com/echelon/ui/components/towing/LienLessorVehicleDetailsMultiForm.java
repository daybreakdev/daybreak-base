package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.ui.components.baseclasses.MultiNewEntryForm;
import com.echelon.ui.helpers.policy.towing.LienLessorVehicleDetailsUIHelper;

public class LienLessorVehicleDetailsMultiForm extends MultiNewEntryForm<LienLessorVehicleDetailsForm, LienholderLessorVehicleDetails> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4813778013969065354L;
	private LienLessorVehicleDetailsUIHelper vehicleDetailsUIHelper = new LienLessorVehicleDetailsUIHelper();
	private String lienLessorType;
	private String lienLessorName;

	public LienLessorVehicleDetailsMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, 7);
	}

	@Override
	protected DSMultiNewEntryDto<LienLessorVehicleDetailsForm, LienholderLessorVehicleDetails> createMultiNewEntryDto() {
		// TODO Auto-generated method stub
		DSMultiNewEntryDto<LienLessorVehicleDetailsForm, LienholderLessorVehicleDetails> dto = super.createMultiNewEntryDto();
		dto.setData(vehicleDetailsUIHelper.createEmptyLienLessorVehicleDetails());
		
		LienLessorVehicleDetailsForm form = new LienLessorVehicleDetailsForm(uiMode, uiContainerType);
		form.loadData(lienLessorType, lienLessorName, dto.getData());
		form.enableEdit();
		dto.setForm(form);
		
		return dto;
	}

	@Override
	protected boolean formValidateEdit(
			DSMultiNewEntryDto<LienLessorVehicleDetailsForm, LienholderLessorVehicleDetails> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().validateEdit(false);
	}

	@Override
	protected boolean formSaveEdit(
			DSMultiNewEntryDto<LienLessorVehicleDetailsForm, LienholderLessorVehicleDetails> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().saveEdit(dto.getData());
	}

	public void enableEdit() {
		super.enableEdit();
		this.initialzieRow();
	}
	
	public void loadData(String lienLessorType, String lienLessorName) {
		this.lienLessorType = lienLessorType;
		this.lienLessorName = lienLessorName;
	}
}
