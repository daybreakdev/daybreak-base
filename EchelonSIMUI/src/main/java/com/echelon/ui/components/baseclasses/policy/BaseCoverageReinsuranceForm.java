package com.echelon.ui.components.baseclasses.policy;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.CoverageReinsuranceDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCovReinsuranceUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.LongRangeValidator;

public class BaseCoverageReinsuranceForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 204959612414674800L;
	protected boolean 							isCoverage;
	protected Boolean 							isPrimary;
	
	//coverage
	protected TextField							coverageCodeField;
	protected DSNumberField<Integer> 			definedLimitField;
	protected PremiumField						totalPremiumField;
	protected FormLayout						covForm;
	
	//Reinsurance
	protected Checkbox							isPrimaryField;
	protected DSComboBox<PolicyReinsurer> 		policyReinsurerField;
	protected DSNumberField<Long> 				startLayerField;
	protected DSNumberField<Long> 				endLayerField;
	protected DSNumberField<Double> 			layerPercentageField;
	protected CovIsReinsurancePremOvrField		isPremiumOverrideField;
	protected PremiumField						ratedPremiumField;
	protected PremiumField						reinsurancePremiumField;
	protected DSNumberField<Double> 			grossUpPercentageField;
	protected PremiumField			 			grossUpAmountField;
	protected PremiumField						insurerPremiumField;
	protected FormLayout						risForm;
	protected LongRangeValidator				upperLimitValidator;
	
	protected Binder<CoverageDto>				covBinder;
	protected Binder<CoverageReinsuranceDto>	risBinder;
	
	protected BaseCovReinsuranceUIHelper 		covReinsuranceUIHelper;
	
	protected CoverageReinsuranceDto			currCovReinsuranceDto;
	protected List<CoverageReinsuranceDto> 		currCovReinsuranceDtos;

	public BaseCoverageReinsuranceForm(String uiMode, UICONTAINERTYPES uiContainerType, UIConstants.OBJECTTYPES parentObjectType, boolean isCoverage, Boolean isPrimary) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.isCoverage = isCoverage;
		this.isPrimary  = isPrimary;
		this.covReinsuranceUIHelper = CoverageUIFactory.getInstance().createCovReinsuranceUIHelper(parentObjectType);
		
		initializeLayouts();
		buildFields();
		buildForm();
	}
	
	protected void initializeLayouts() {
		setSizeFull();
	}
	
	protected void buildFields() {
		buildCoverageFields();
		buildReinsuranceFields();
	}
	
	protected void buildCoverageFields() {
		coverageCodeField = new TextField(labels.getString(isCoverage ? "CoverageReinsuranceFormCoverageLabel" : "EndorsementReinsuranceFormCoverageLabel"));
		definedLimitField = new DSNumberField<Integer>(labels.getString("CoverageReinsuranceFormDefinedLimitLabel"), Integer.class);
		totalPremiumField = new PremiumField(labels.getString("CoverageReinsuranceFormTotalPremiumLabel"));
		
		covBinder = new Binder<CoverageDto>();
		covBinder.forField(coverageCodeField).bind(CoverageDto::getDescription, null);
		covBinder.setReadOnly(true);
	}
	
	protected void buildReinsuranceFields() {
		upperLimitValidator = new LongRangeValidator(labels.getString("CoverageReinsurancesUpperLimitRangeError"), null, null);

		isPrimaryField			= new Checkbox();
		policyReinsurerField	= new DSComboBox<PolicyReinsurer>(labels.getString("CoverageReinsuranceFormInsurerLabel"), PolicyReinsurer.class);
		startLayerField			= new DSNumberField<Long>(labels.getString("CoverageReinsuranceFormLowerLimitLabel"), Long.class);
		endLayerField			= new DSNumberField<Long>(labels.getString("CoverageReinsuranceFormUpperLimitLabel"), Long.class);
		layerPercentageField	= new DSNumberField<Double>(labels.getString("CoverageReinsuranceFormLayerPercentLabel"), Double.class);
		isPremiumOverrideField	= new CovIsReinsurancePremOvrField(labels.getString("CoverageReinsuranceFormIsPremiumOverrideLabel"));
		ratedPremiumField		= new PremiumField(labels.getString("CoverageReinsuranceFormRatedPremLabel"));
		reinsurancePremiumField = new PremiumField(labels.getString("CoverageReinsuranceFormReinsurancePremLabel"));
		grossUpPercentageField	= new DSNumberField<Double>(labels.getString("CoverageReinsuranceFormGrossUpPercentLabel"), Double.class);
		grossUpAmountField		= new PremiumField(labels.getString("CoverageReinsuranceFormGrossUpAmountLabel"));
		insurerPremiumField		= new PremiumField(labels.getString("CoverageReinsuranceFormInsurerPremLabel"));
		
		risBinder = new Binder<CoverageReinsuranceDto>();
		risBinder.forField(isPrimaryField).bind(CoverageReinsuranceDto::getIsPrimaryInsurer, null);
		risBinder.forField(policyReinsurerField).asRequired().bind(CoverageReinsuranceDto::getPolicyReinsurer, CoverageReinsuranceDto::setPolicyReinsurer);
		risBinder.forField(startLayerField).asRequired().withNullRepresentation("").withConverter(startLayerField.getConverter())
						.bind(CoverageReinsuranceDto::getStartLayer, CoverageReinsuranceDto::setStartLayer);
		risBinder.forField(endLayerField).asRequired().withNullRepresentation("").withConverter(endLayerField.getConverter())
						.withValidator(upperLimitValidator)
						.bind(CoverageReinsuranceDto::getEndLayer, CoverageReinsuranceDto::setEndLayer);
		risBinder.forField(layerPercentageField).withNullRepresentation("").withConverter(layerPercentageField.getConverter())
						.bind(CoverageReinsuranceDto::getLayerPercentage, CoverageReinsuranceDto::setLayerPercentage);
		risBinder.forField(ratedPremiumField).withNullRepresentation("").withConverter(ratedPremiumField.getConverter())
						.bind(CoverageReinsuranceDto::getPrimaryOriginalPremium, null);
		risBinder.forField(insurerPremiumField).withNullRepresentation("").withConverter(insurerPremiumField.getConverter())
						.bind(CoverageReinsuranceDto::getInsurerPremium, CoverageReinsuranceDto::setInsurerPremium);
		risBinder.forField(grossUpPercentageField).withNullRepresentation("").withConverter(grossUpPercentageField.getConverter())
						.bind(CoverageReinsuranceDto::getGrossUpPercentage, CoverageReinsuranceDto::setGrossUpPercentage);
		risBinder.forField(grossUpAmountField).withNullRepresentation("").withConverter(grossUpAmountField.getConverter())
						.bind(CoverageReinsuranceDto::getGrossUpAmount, CoverageReinsuranceDto::setGrossUpAmount);
		risBinder.forField(reinsurancePremiumField).withNullRepresentation("").withConverter(reinsurancePremiumField.getConverter())
						.bind(CoverageReinsuranceDto::getReinsurancePremium, CoverageReinsuranceDto::setReinsurancePremium);
		risBinder.setReadOnly(true);
				
	}
	
	protected void buildForm() {
		buildCoverageForm();
		buildReinsuranceForm();
		
		this.add(covForm);
		this.add(risForm);
	}
	
	protected void buildCoverageForm() {
		covForm = new FormLayout();
		
		covForm.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
				);
		
		covForm.add(coverageCodeField, 2);
		covForm.add(definedLimitField, totalPremiumField);
	}
	
	protected void buildReinsuranceForm() {
		risForm = new FormLayout();
		
		risForm.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
				);
		
		risForm.add(policyReinsurerField, 2);
		risForm.add(startLayerField, endLayerField);
		if (BooleanUtils.isTrue(isPrimary)) {
			risForm.add(layerPercentageField, ratedPremiumField);
			risForm.add(isPremiumOverrideField, insurerPremiumField);
		}
		else {
			risForm.add(layerPercentageField, insurerPremiumField);
		}		
		risForm.add(grossUpPercentageField, grossUpAmountField);
		risForm.add(reinsurancePremiumField);
		uiHelper.addFormEmptyColumn(risForm, 1);
	}
	
	public void loadData(CoverageDto coverageDto, CoverageReinsuranceDto covReinsuranceDto, List<CoverageReinsuranceDto> covReinsuranceDtos) {
		this.currCovReinsuranceDto  = covReinsuranceDto;
		this.currCovReinsuranceDtos = covReinsuranceDtos;
		loadDataConfigs(coverageDto, covReinsuranceDto, covBinder, risBinder);
		
		isPremiumOverrideField.initializeField(covReinsuranceDto);
		isPremiumOverrideField.bindField(risBinder);
		
		covBinder.readBean(coverageDto);
		risBinder.readBean(covReinsuranceDto);
		
		displayHeaderValues();
	}

	public void enabledEdit() {
		enableDataEdit(risBinder);

		uiHelper.setEditFieldsReadonly(grossUpAmountField, reinsurancePremiumField, ratedPremiumField, isPremiumOverrideField);

		if (!UIConstants.UIACTION_New.equalsIgnoreCase(uiMode)) {
			uiHelper.setEditFieldsReadonly(policyReinsurerField);

			if (BooleanUtils.isTrue(isPrimaryField.getValue())) {
				uiHelper.setEditFieldsReadonly(layerPercentageField, insurerPremiumField, grossUpPercentageField, startLayerField, endLayerField);
				isPremiumOverrideField.determineReadOnly();
			}
		}

		setFieldEvents();
		isPremiumOverrideFieldValueChanged();
	}

	public boolean validEdit() {
		calculateGrossUpAmounts();
		boolean ok = uiHelper.validateValues(risBinder);
		return ok;
	}
	
	public boolean saveEdit(CoverageReinsuranceDto covReinsuranceDto) {
		uiHelper.writeValues(risBinder, covReinsuranceDto);
		return true;
	}

	@SuppressWarnings("rawtypes")
	protected boolean loadDataConfigs(CoverageDto coverageDto, CoverageReinsuranceDto covReinsuranceDto, Binder... binders) {
		// TODO Auto-generated method stub
		boolean ok = super.loadDataConfigs(binders);
		
		List<LookupTableItem> reinsurerLookup = this.covReinsuranceUIHelper.getPolicyReinsurerLookupFiltered(covReinsuranceDto.getIsPrimaryInsurer(),
														coverageDto != null ? coverageDto.getReinsuranceDtos().stream().filter(o -> !o.equals(covReinsuranceDto)).collect(Collectors.toList()) : null
													);
		policyReinsurerField.setDSItems(reinsurerLookup);
		if (covReinsuranceDto != null && covReinsuranceDto.getPolicyReinsurer() != null) {
			policyReinsurerField.setDSValue(covReinsuranceDto.getPolicyReinsurer());
		}
		
		return ok;
	}

	protected void setFieldEvents() {
		upperLimitValidator.setMinValue(startLayerField.getModelValue());
		startLayerField.addValueChangeListener(e -> {
			upperLimitValidator.setMinValue(startLayerField.getModelValue());
		});
		endLayerField.addValueChangeListener(e -> displayHeaderValues());
		insurerPremiumField.addValueChangeListener(e -> calculateGrossUpAmounts());
		grossUpPercentageField.addValueChangeListener(e -> calculateGrossUpAmounts());
		if (!isPremiumOverrideField.isReadOnly()) {
			isPremiumOverrideField.addValueChangeListener(e -> isPremiumOverrideFieldValueChanged());
		}
		reinsurancePremiumField.addValueChangeListener(e -> displayHeaderValues());
	}
	
	protected void calculateGrossUpAmounts() {
		Double[] amounts = covReinsuranceUIHelper.calculateGrossUpAmount(insurerPremiumField.getModelValue(), grossUpPercentageField.getModelValue());
		grossUpAmountField.setDSValue(amounts[0]);
		reinsurancePremiumField.setDSValue(amounts[1]);
	}

	protected void isPremiumOverrideFieldValueChanged() {
		if (BooleanUtils.isTrue(isPrimaryField.getValue())) {
			if (BooleanUtils.isTrue(isPremiumOverrideField.getValue())) {
				uiHelper.setEditFieldsNotReadonly(insurerPremiumField);
			}
			else {
				insurerPremiumField.setDSValue(ratedPremiumField.getModelValue());
				uiHelper.setEditFieldsReadonly(insurerPremiumField);
			}
		}
	}
	
	protected void displayHeaderValues() {
		definedLimitField.setDSValue(covReinsuranceUIHelper.getReinsuranceDetainedLimit(currCovReinsuranceDtos, currCovReinsuranceDto, endLayerField.getModelValue()));
		totalPremiumField.setDSValue(covReinsuranceUIHelper.getTotalReinsurancePremium(currCovReinsuranceDtos, currCovReinsuranceDto, reinsurancePremiumField.getModelValue()));		
	}
}	
