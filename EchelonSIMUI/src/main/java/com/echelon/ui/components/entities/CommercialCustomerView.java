package com.echelon.ui.components.entities;

import com.ds.ins.domain.entities.CommercialCustomer;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.helpers.entities.CommercialCustomerUIHelper;
import com.echelon.ui.helpers.entities.CustomerUIHelper;

public class CommercialCustomerView extends CustomerView<CommercialCustomer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6018698645077552547L;

	public CommercialCustomerView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected CustomerUIHelper<CommercialCustomer> createCustomerUIHelper() {
		return new CommercialCustomerUIHelper();
	}
}
