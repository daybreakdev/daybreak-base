package com.echelon.ui.components.policy.wheels;

import java.util.List;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.VehicleDriverClaimUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

public class VehicleDriverClaimForm extends BaseLayout implements IDSFocusAble {


	/**
	 * 
	 */
	private static final long serialVersionUID = 229079675585665425L;
	private DSComboBox<Driver> 			assignedDriverField;
	private DatePicker					dateOfLossField;
	private DSComboBox<String> 			claimTypeField;
	private IntegerField				claimNumField;
	private DSComboBox<String> 			faultPercentField;
	private DSNumberField<Double> 		lossPaidField;
	private VehicleDriverClaimUIHelper	vehDrvClaimUIHelper;
	
	private SpecialtyVehicleRisk		vehicle;
	private VehicleDriverClaim			vehicleDriverClaim;
	
	private Binder<VehicleDriverClaim> 	binder;

	public VehicleDriverClaimForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.vehDrvClaimUIHelper = new VehicleDriverClaimUIHelper();
		this.setSizeFull();
		
		buildFields();
		buildForm();
	}
	

	protected void buildFields() {
		assignedDriverField = new DSComboBox<Driver>(labels.getString("VehicleDriverClaimFormAssignedDriverLabel"), Driver.class);
		dateOfLossField		= new DatePicker(labels.getString("VehicleDriverClaimFormDateOfLossLabel"));
		claimTypeField		= new DSComboBox<String>(labels.getString("VehicleDriverClaimFormClaimTypeLabel"), String.class)
									.withLookupTableId(ConfigConstants.LOOKUPTABLE_ClaimType);
		claimNumField		= new IntegerField(labels.getString("VehicleDriverClaimFormClaimNumLabel"));
		faultPercentField   = new DSComboBox<String>(labels.getString("VehicleDriverClaimFormFaultPercentLabel"), String.class)
									.withLookupTableId(ConfigConstants.LOOKUPTABLE_ClaimFault);
		lossPaidField		= new DSNumberField<Double>(labels.getString("VehicleDriverClaimFormLossPaidLabel"), Double.class);
		
		binder = new Binder<VehicleDriverClaim>(VehicleDriverClaim.class);
		binder.forField(assignedDriverField).asRequired().bind(VehicleDriverClaim::getDriver, VehicleDriverClaim::setDriver);
		binder.forField(dateOfLossField).asRequired().bind(VehicleDriverClaim::getLossDate, VehicleDriverClaim::setLossDate);
		binder.forField(claimTypeField).bind(VehicleDriverClaim::getClaimType, VehicleDriverClaim::setClaimType);
		binder.forField(claimNumField).bind(VehicleDriverClaim::getClaimNum, VehicleDriverClaim::setClaimNum);
		binder.forField(faultPercentField).bind(VehicleDriverClaim::getFaultResponsibility, VehicleDriverClaim::setFaultResponsibility);
		binder.forField(lossPaidField)
							.withNullRepresentation("")
							.withConverter(lossPaidField.getConverter())
							.bind(VehicleDriverClaim::getTotalPaid, VehicleDriverClaim::setTotalPaid);
	}
	
	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		
		form.add(assignedDriverField, 2);
		form.add(dateOfLossField, claimTypeField);
		form.add(claimNumField, faultPercentField);
		form.add(lossPaidField);
		uiHelper.addFormEmptyColumn(form);
		
		this.add(form);
	}
	
	public void loadData(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		loadDataConfigs(binder);
		loadDriverList(vehicle, vehDrvClaim);
				
		this.vehicle = vehicle;
		this.vehicleDriverClaim = vehDrvClaim;
			
		binder.readBean(vehDrvClaim);
		
		enableDataView(binder);
	}
	
	private void loadDriverList(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		List<LookupTableItem> lookupDrivers = vehDrvClaimUIHelper.getAvailableDrivers(vehicle, vehDrvClaim);
		assignedDriverField.setDSItems(lookupDrivers);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		setFocus();
	}
	
	public boolean saveEdit(VehicleDriverClaim vehDrvClaim) {
		boolean ok1 = uiHelper.writeValues(binder, vehDrvClaim);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		assignedDriverField.focus();
	}

}
