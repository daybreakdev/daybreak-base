package com.echelon.ui.components.baseclasses;

import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.echelon.ui.session.BrokerUI;

public class LookupItemValue extends LookupTableItem {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5653951979425837767L;

	public LookupItemValue() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LookupItemValue(String itemKey, String itemValue, Object itemObject) {
		super(itemKey, itemValue, itemObject);
		// TODO Auto-generated constructor stub
	}

	public LookupItemValue(String key, String value) {
		super(key, value);
		// TODO Auto-generated constructor stub
	}

	public LookupItemValue(LookupTableItem lookupTableItem) {
		this(lookupTableItem.getItemKey(), lookupTableItem.getItemValue(), lookupTableItem.getItemObject());
		
		if (lookupTableItem.getTranslations() != null && !lookupTableItem.getTranslations().isEmpty()) {
			this.getTranslations().addAll(lookupTableItem.getTranslations());
		}
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return getTranslation(BrokerUI.getUserSession().getUserProfile().getDefaultLanguage());
	}
}
