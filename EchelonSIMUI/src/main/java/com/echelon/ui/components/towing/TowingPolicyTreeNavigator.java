package com.echelon.ui.components.towing;

import java.util.List;
import java.util.Optional;

import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.prodconf.baseclasses.NavigationTreeItemConfig;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.converters.DSLocalDateFormat;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.policy.BasePolicyTreeNavigator;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;

public class TowingPolicyTreeNavigator extends BasePolicyTreeNavigator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4384732550821090173L;

	@Override
	protected DSTreeGridItem createTreeItem(DSTreeGridItem parentItem, NavigationTreeItemConfig treeItemConfig, Object dataObject) {
		// assign node data object based on current tree node configuration
		DSTreeGridItem treeItem = null;
		if (UIConstants.POLICYTREENODES.Vehicle.name().equalsIgnoreCase(treeItemConfig.getNodeType())) {
			buildVehicleItems(parentItem, treeItemConfig);
		}
		else {
			Object currentObject = calcCurrentItemObejct(treeItemConfig, dataObject);
			
			// create current tree node
			treeItem = super.createTreeItem(parentItem, treeItemConfig, currentObject);
	
			// build child level of current node
			buildSubpolicyItems(treeItem, treeItemConfig, dataObject);
		}
		
		return treeItem;
	}
	
	@Override
	protected boolean isMatch(DSTreeGridItem item, String itemNodeType, Object itemNodeObject) {
		// TODO Auto-generated method stub
		// Same node type
		boolean isOk = super.isMatch(item, itemNodeType, itemNodeObject);
		
		if (isOk) {
			if (UIConstants.POLICYTREENODES.Vehicle.name().equalsIgnoreCase(item.getNodeType())) {
				isOk = false;
				
				if (item.getNodeObject() != null && itemNodeObject != null) {
					SpecialtyVehicleRisk risk1 = (SpecialtyVehicleRisk) item.getNodeObject();
					SpecialtyVehicleRisk risk2 = (SpecialtyVehicleRisk) itemNodeObject;
					if (risk1.getPolicyRiskPK() != null &&
							risk2.getPolicyRiskPK() != null && 
							risk1.getPolicyRiskPK().longValue() == risk2.getPolicyRiskPK().longValue()) {
						isOk = true;
					}
				}
			}
			else if (UIConstants.POLICYTREENODES.Driver.name().equalsIgnoreCase(item.getNodeType())) {
				isOk = false;
				
				if (item.getNodeObject() != null && itemNodeObject != null) {
					Driver driver1 = (Driver) item.getNodeObject();
					Driver driver2 = (Driver) itemNodeObject;
					if (driver1.getPersonPk() != null &&
						driver2.getPersonPk() != null && 
						driver1.getPersonPk().longValue() == driver2.getPersonPk().longValue()) {
						isOk = true;
					}
				}
			}
			else if (isSubPolicyNode(item)) {
				isOk = false;
				
				// subpolicy instance node
				if (item.getNodeObject() != null && itemNodeObject != null) {
					// Tree node object
					SubPolicy<?> sp1 = (SubPolicy<?>) item.getNodeObject();
					// Selection
					SubPolicy<?> sp2 = (SubPolicy<?>) itemNodeObject;
					
					// existing subpolicy
					if (sp1.getSubPolicyPK() != null &&
							sp2.getSubPolicyPK() != null && 
							sp1.getSubPolicyPK().longValue() == sp2.getSubPolicyPK().longValue()) {
						isOk = true;
					}
					
					// new subpolicy is added
					else if (sp2.getSubPolicyPK() == null && sp1.isNew()) {
						isOk = true;
					}
				}
				
				// No multiple subpolicies
				// Parent node is not subpolicy node
				if (!isOk) {
					if (!isSubPolicyNode(item.getParentItem())) {
						if (item.getChildItems() == null || item.getChildItems().isEmpty()) {
							isOk = true;
						}
					}
				}
			}
		}
		
		return isOk;
	}
	
	protected boolean isSubPolicyNode(DSTreeGridItem item) {
		if (item != null &&
			 UIConstants.POLICYTREENODES.CGLSubPolicyNode.name().equalsIgnoreCase(item.getNodeType()) ||
			 UIConstants.POLICYTREENODES.CargoSubPolicyNode.name().equalsIgnoreCase(item.getNodeType()) ||
			 UIConstants.POLICYTREENODES.GarageSubPolicyNode.name().equalsIgnoreCase(item.getNodeType()) ||
			 UIConstants.POLICYTREENODES.CommercialPropertySubPolicyNode.name().equalsIgnoreCase(item.getNodeType())) {
			return true;
		}
		
		return false;
	}
	
	private Object calcCurrentItemObejct(NavigationTreeItemConfig treeItemConfig, Object dataObject) {
		Object currentObject = dataObject;
		
		String subpolicyCode = getSubpolicyCode(treeItemConfig.getNodeType());
		if (subpolicyCode != null) {
			currentObject = null;
			if (this.policyTransaction != null && policyTransaction.findSubPoliciesByType(subpolicyCode).size() == 1) {
				currentObject = policyTransaction.findSubPolicyByType(subpolicyCode);
			}
		}

		return currentObject;
	}
	
	private void buildSubpolicyItems(DSTreeGridItem parentItem, NavigationTreeItemConfig treeItemConfig, Object dataObject) {
		String subpolicyCode = getSubpolicyCode(treeItemConfig.getNodeType());
		
		if (subpolicyCode != null) {
			List<SubPolicy<?>> subPolicies = UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(subpolicyCode).getSortedSubPolicies(policyTransaction, subpolicyCode);
			if (subPolicies != null && subPolicies.size() > 1) {
				DSLocalDateFormat dateFormat = new DSLocalDateFormat();
				
				subPolicies
						.stream().forEach(o -> {
							String text = dateFormat.format(o.getEffectiveDate())+" / "+dateFormat.format(o.getExpiryDate());
							
							DSTreeGridItem vehItem = new DSTreeGridItem(parentItem, text, treeItemConfig.getNodeType(), treeItemConfig.getUiName(), 
																		policyTransaction, o);
							parentItem.addChildItem(vehItem);
						});
			}
		}
	}
	
	// build the vehicle at the same level as vehicles node
	// this is called after vehicles node is created
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void buildVehicleItems(DSTreeGridItem parentItem, NavigationTreeItemConfig itemConfig) {
		if (parentItem != null && parentItem.getNodeObject() != null && parentItem.getNodeObject() instanceof SpecialityAutoSubPolicy) {
			VehicleUIHelper vehicleUIHelper = new VehicleUIHelper();
			
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy = (SpecialityAutoSubPolicy) parentItem.getNodeObject();
			List<SpecialtyVehicleRisk> vehicles = (new VehicleUIHelper()).sortVehicleList(autoSubPolicy.getSubPolicyRisks());	
			vehicles.stream().forEach(o -> {
					String vehText = vehicleUIHelper.getTreeNodeText(o);
					
					DSTreeGridItem vehItem = new DSTreeGridItem(parentItem, vehText, UIConstants.POLICYTREENODES.Vehicle.name(), itemConfig.getUiName(),
																autoSubPolicy, o);
					parentItem.addChildItem(vehItem);
				});
		}		
	}

	@Override
	protected Label getTreeNodeLabel(DSTreeGridItem treeGridItem) {
		Label label = super.getTreeNodeLabel(treeGridItem);
		
		String subpolicyCode = getSubpolicyCode(treeGridItem.getNodeType());
		if (subpolicyCode != null &&
			// No Subpolicy  
			(treeGridItem.getNodeObject() == null &&
			 (this.policyTransaction == null || policyTransaction.findSubPoliciesByType(subpolicyCode).isEmpty())
		    )) {
			uiHelper.setLabelShowNotAvailable(label);
		}
		
		return label;
	}

	@Override
	protected Icon getTreeNodeIcon(DSTreeGridItem treeGridItem) {
		
		if (UIConstants.POLICYTREENODES.Policy.name().equalsIgnoreCase(treeGridItem.getNodeType())) {
			// Need the customer type in order to obtain the customer UI helper. Given that
			// we're looking at the Policy tree node, treeGridItem.getNodeObject *should* be
			// the policy transaction. Verify that it is, then walk the policy hierarchy (in
			// a null-safe manner) to retrieve the customer type.
			//
			// Once we have the customer type, we can use it to get the customer UI helper,
			// and finally check whether or not the entity has been modified.
			final String customerType = Optional.ofNullable(treeGridItem.getNodeObject())
					.filter(object -> object instanceof PolicyTransaction<?>).map(PolicyTransaction.class::cast)
					.map(PolicyTransaction::getPolicyVersion).map(PolicyVersion::getInsurancePolicy)
					.map(InsurancePolicy::getPolicyCustomer).map(Customer::getCustomerType).orElse(null);
			if (customerType != null && UIFactory.getInstance().getCustomerUIHelper(customerType).isEntityModified()) {
				return uiHelper.createIconModified();
			}
			
			return null;
		}
		
		return super.getTreeNodeIcon(treeGridItem);
	}	
	
	private String getSubpolicyCode(String nodeType) {
		String subpolicyCode = null;
		
		if (UIConstants.POLICYTREENODES.AutomobileSubPolicyNode.name().equalsIgnoreCase(nodeType)) {
			subpolicyCode = SpecialtyAutoConstants.SUBPCY_CODE_AUTO;
		}
		else if (UIConstants.POLICYTREENODES.CGLSubPolicyNode.name().equalsIgnoreCase(nodeType)) {
			subpolicyCode = SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY;
		}
		else if (UIConstants.POLICYTREENODES.CargoSubPolicyNode.name().equalsIgnoreCase(nodeType)) {
			subpolicyCode = SpecialtyAutoConstants.SUBPCY_CODE_CARGO;
		}
		else if (UIConstants.POLICYTREENODES.GarageSubPolicyNode.name().equalsIgnoreCase(nodeType)) {
			subpolicyCode = SpecialtyAutoConstants.SUBPCY_CODE_GARAGE;
		} else if (UIConstants.POLICYTREENODES.CommercialPropertySubPolicyNode.name().equalsIgnoreCase(nodeType)) {
			subpolicyCode = SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY;
		}
		
		return subpolicyCode;
	}
}
