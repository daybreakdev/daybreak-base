package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.MainView;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RouterLayout;

@ParentLayout(value = MainView.class)
public class BasePolicySearchView extends BaseLayout implements AfterNavigationObserver, BeforeEnterObserver, RouterLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4648864418379926161L;
	private Div	 	contentLayout;
	
	public BasePolicySearchView() {
		super();
		
		buildLayouts();
	}

	private void buildLayouts() {
		this.setHeightFull();
		
		contentLayout = new Div();
		contentLayout.setSizeFull();
		this.add(contentLayout);
	}
	
	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		// TODO Auto-generated method stub
		// Reload screen
		uiPolicyHelper.resetSessionPolicyVersion();
		uiPolicyHelper.refreshPolicyMenuItems();
	}
	
	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		//uiHelper.handleBeforeEnter();
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		if (uiHelper.handleBeforeEnter()) {
			contentLayout.removeAll();
			if (content != null) {
				contentLayout.getElement().appendChild(content.getElement());
			}
		}
	}
}
