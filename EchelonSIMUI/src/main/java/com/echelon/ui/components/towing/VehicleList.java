package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.ds.ins.domain.policy.Premium;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.baseclasses.policy.BaseRiskList;
import com.echelon.ui.components.baseclasses.policy.PremiumField;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;
import com.vaadin.flow.router.Route;

@Route(value="VehicleListNodeContentView", layout = BasePolicyView.class)
public class VehicleList extends BaseRiskList<SpecialtyVehicleRisk> implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7456207045620891344L;
	private VehicleUIHelper 	vehicleUIHelper = new VehicleUIHelper();
	private PremiumField		totalCovPremium;
	
	private SpecialityAutoSubPolicy	parentSubPolicy;
	private SpecialtyVehicleRisk 	lastRisk;

	public VehicleList() {
		super();
		// TODO Auto-generated constructor stub
		buildVehicleList();
	}

	public VehicleList(String uiMode, UICONTAINERTYPES uiContainerType, String title) {
		super(uiMode, uiContainerType, title);
		// TODO Auto-generated constructor stub
		buildVehicleList();
	}

	public VehicleList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		buildVehicleList();
	}
	
	protected void buildVehicleList() {
		initializeLayouts(labels.getString("VehicleListTitle"));
		buildTotalPremiumFields();
		buildGrid();
	}
	
	protected void buildTotalPremiumFields() {
		totalCovPremium = new PremiumField("");
		totalCovPremium.setReadOnly(true);
		
		uiHelper.setTextFooterStyle(totalCovPremium);
	}
	
	protected Component buildTotalPremiumLayout(SpecialtyVehicleRisk risk) {
		if (lastRisk != null && lastRisk.equals(risk)) {
			VerticalLayout col = new VerticalLayout();
			uiHelper.formatFullLayout(col);
			col.getElement().getStyle().set("margin-top","2px");
			
			HorizontalLayout row = new HorizontalLayout();
			Label label = new Label(labels.getString("VehicleListTotalPremLabel"));
			row.add(label, totalCovPremium);
			row.setVerticalComponentAlignment(Alignment.CENTER, label);
			col.add(row);
			col.setHorizontalComponentAlignment(Alignment.END, row);
			
			return col;
		}
		
		return new HorizontalLayout();
	}

	@Override
	protected void addGridColumns() {
		createIndicatorColumn(riskGrid);
		riskGrid.addColumn(SpecialtyVehicleRisk::getVehicleSequence).setWidth("10px");	
		riskGrid.addColumn(SpecialtyVehicleRisk::getDisplayString)
							.setHeader(labels.getString("VehicleListColumnVehGroupLabel"))
							.setAutoWidth(true)
							//.setSortable(true)
							;	
		riskGrid.addColumn(SpecialtyVehicleRisk::getNumberOfUnits).setHeader(labels.getString("VehicleListColumnUnitsLabel"));	
		riskGrid.addColumn(new DSLookupItemRenderer<SpecialtyVehicleRisk>(SpecialtyVehicleRisk::getVehicleType)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_VehType))
							.setHeader(labels.getString("VehicleListColumnVehTypeLabel"));	
		riskGrid.addColumn(SpecialtyVehicleRisk::getUnitYear).setHeader(labels.getString("VehicleListColumnVehYearLabel"));	
		riskGrid.addColumn(SpecialtyVehicleRisk::getUnitMake).setHeader(labels.getString("VehicleListColumnVehMakeLabel"));	
		riskGrid.addColumn(new NumberRenderer<>(o -> ((SpecialtyVehicleRisk)o).getRiskPremium() != null
								? ((SpecialtyVehicleRisk)o).getRiskPremium().getNetPremiumChange()
								: null, DSNumericFormat.getPremiumInstance()))
							.setHeader(labels.getString("VehicleListColumnTransPremiumLabel"))
							.setTextAlign(ColumnTextAlign.END);	
		riskGrid.addColumn(new NumberRenderer<>(o -> ((SpecialtyVehicleRisk)o).getRiskPremium() != null
								? ((SpecialtyVehicleRisk)o).getRiskPremium().getWrittenPremium()
								: null, DSNumericFormat.getPremiumInstance()))
							.setHeader(labels.getString("VehicleListColumnFullPremiumLabel"))
							.setTextAlign(ColumnTextAlign.END);
		
		riskGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		riskGrid.setDetailsVisibleOnClick(false);
		
		riskGrid.addSelectionListener(o -> {
			if (o.getSource().getSelectedItems() != null && !o.getSource().getSelectedItems().isEmpty()) {
				uiPolicyHelper.selectPolicyItem(UIConstants.POLICYTREENODES.Vehicle, 
												o.getSource().getSelectedItems().iterator().next());
			}
		});
		
		riskGrid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
			return buildTotalPremiumLayout(o);
		}));
	}

	@Override
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				vehicleUIHelper.newVehicleAction(parentSubPolicy);
			});
		}
	}

	public boolean loadData(SpecialityAutoSubPolicy	parentSubPolicy, Set<SpecialtyVehicleRisk> set) {
		this.parentSubPolicy = parentSubPolicy;
		List<SpecialtyVehicleRisk> list = vehicleUIHelper.sortVehicleList(set);

		lastRisk = null;
		if (!list.isEmpty()) {
			lastRisk = list.get(list.size() - 1);
			
			List<Premium> premiums = new ArrayList<Premium>();
			list.stream().forEach(o -> {
				if (o.getRiskPremium() != null) {
					premiums.add(o.getRiskPremium());
				}
			});
			Double sum = premiums.stream().mapToDouble(Premium::getWrittenPremium).sum();
			totalCovPremium.setDSValue(sum);
		}

		this.riskGrid.setItems(list);
		refreshToolbar();
		
		if (lastRisk != null) {
			riskGrid.setDetailsVisible(lastRisk, true);
		}
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		// TODO Auto-generated method stub
		super.refreshToolbar(parentSubPolicy);
		
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			toolbar.getButton(UIConstants.UIACTION_Update).setVisible(false);
		}
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
		}
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			toolbar.getButton(UIConstants.UIACTION_View).setVisible(false);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		vehicleUIHelper.loadView(this, nodeItem);
	}
}
