package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.config.rating.RatebookVersionConfiguration;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyRatingDetailsUpdateForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyRatingDetailsProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.vaadin.flow.component.UI;

public class BasePolicyRatingDetailsUIHelper extends ContentUIHelper implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BasePolicyRatingDetailsUIHelper.class.getName());

	public void updateRatingDetails() {
		final BasePolicyRatingDetailsProcessor processor = createProcessor(UIConstants.UIACTION_LoadData,
				policyUIHelper.getCurrentPolicyTransaction());
		final TaskProcessor taskProcessor = createFindRatebookVersionTaskProcessor();
		taskProcessor.start(processor);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<BasePolicyRatingDetailsUpdateForm> dialog = event.getSource();
		final BasePolicyRatingDetailsUpdateForm form = dialog.getContentComponent();
		final PolicyTransaction<?> policyTransaction = (PolicyTransaction<?>) dialog.getDataStore().getData1();

		if (form.getSelectedRatebookVersion() == null) {
			// This shouldn't ever happen...
			log.warning("Form was saved without a ratebook version. No action will be taken");
			dialog.close();
		} else {
			final Integer versionNum = form.getSelectedRatebookVersion().getVersionNum();
			final BasePolicyRatingDetailsProcessor processor = createProcessor(UIConstants.UIACTION_Update,
					policyTransaction, versionNum);
			final TaskProcessor taskProcessor = createRatebookUpdateTaskProcessor(dialog);
			taskProcessor.start(processor);
		}
	}

	protected BasePolicyRatingDetailsUpdateForm createForm(String action, UICONTAINERTYPES uiContainerType,
			List<RatebookVersionConfiguration> ratebookVersions, Integer currentRatebookVersion) {
		final BasePolicyRatingDetailsUpdateForm form = new BasePolicyRatingDetailsUpdateForm(action, uiContainerType);
		form.loadData(ratebookVersions, currentRatebookVersion);
		return form;
	}

	protected DSUpdateDialog<BasePolicyRatingDetailsUpdateForm> createDialog(String title, String action,
			PolicyTransaction<?> policyTransaction, List<RatebookVersionConfiguration> ratebookVersions,
			Integer currentRatebookVersion) {
		final BasePolicyRatingDetailsUpdateForm form = createForm(action, UICONTAINERTYPES.Dialog, ratebookVersions,
				currentRatebookVersion);
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<BasePolicyRatingDetailsUpdateForm> dialog = (DSUpdateDialog<BasePolicyRatingDetailsUpdateForm>) createDialog(
				title, form, action);
		return dialog;
	}

	protected BasePolicyRatingDetailsProcessor createProcessor(String processAction,
			PolicyTransaction<?> policyTransaction) {
		return new BasePolicyRatingDetailsProcessor(processAction, policyTransaction);
	}

	protected BasePolicyRatingDetailsProcessor createProcessor(String processAction,
			PolicyTransaction<?> policyTransaction, Integer ratebookVersion) {
		final BasePolicyRatingDetailsProcessor processor = createProcessor(processAction, policyTransaction);
		processor.setRatebookId(policyTransaction.getPolicyTerm().getRatebookId());
		processor.setRatebookVersion(ratebookVersion);
		return processor;
	}

	protected TaskProcessor createFindRatebookVersionTaskProcessor() {
		@SuppressWarnings("serial")
		final TaskProcessor taskProcessor = new TaskProcessor(UI.getCurrent()) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				final BasePolicyRatingDetailsProcessor processor = (BasePolicyRatingDetailsProcessor) taskRun
						.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					final String dialogTitle = uiHelper.getSessionLabels().getString("PolicyRatingDetailsUpdateTitle");
					final List<RatebookVersionConfiguration> ratebookVersions = processor.getRatebookVersions();
					final PolicyTransaction<?> policyTransaction = processor.getPolicyTransaction();
					final Integer currentRatebookVersion = policyTransaction.getPolicyTerm().getRatebookVersion();
					final DSUpdateDialog<BasePolicyRatingDetailsUpdateForm> dialog = createDialog(dialogTitle,
							UIConstants.UIACTION_Update, policyTransaction, ratebookVersions, currentRatebookVersion);
					dialog.setDataStore(new DSDialogDataStore(policyTransaction));
					dialog.open();
				}

				return true;
			}
		};
		return taskProcessor;
	}

	protected TaskProcessor createRatebookUpdateTaskProcessor(IDSDialog dialog) {
		@SuppressWarnings("serial")
		final TaskProcessor taskProcessor = new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				final BasePolicyRatingDetailsProcessor processor = (BasePolicyRatingDetailsProcessor) taskRun
						.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					dialog.close();
					policyUIHelper.reopenPolicy(processor.getPolicyTransaction(), POLICYTREENODES.Policy, null);
				}

				return true;
			}
		};
		return taskProcessor;
	}
}
