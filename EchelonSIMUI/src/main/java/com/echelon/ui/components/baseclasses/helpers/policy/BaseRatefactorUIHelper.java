package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;

public class BaseRatefactorUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1900161440731956645L;

	public BaseRatefactorUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RateFactorsLink getRateFactorsLink(Object ratefactorParent) {
		RateFactorsLink result = null;
		
		if (ratefactorParent != null) {
			if (ratefactorParent instanceof Coverage) {
				result = ((Coverage)ratefactorParent).getRateFactorsLink();
			}
			else if (ratefactorParent instanceof Endorsement) {
				result = ((Endorsement)ratefactorParent).getRateFactorsLink();
			}
			else if (ratefactorParent instanceof PolicyTransaction) {
				result = ((PolicyTransaction)ratefactorParent).getRateFactorsLink();
			}
		}

		return result;
	}

	public String getRateFactorPrefix(Object ratefactorParent) {
		String result = "";
		
		if (ratefactorParent != null) {
			if (ratefactorParent instanceof Coverage) {
				result = ((Coverage)ratefactorParent).getCoverageCode();
			}
			else if (ratefactorParent instanceof Endorsement) {
				result = ((Endorsement)ratefactorParent).getEndorsementCd();
			}
		}

		return result;
	}
	
	public List<RateFactor> getRateFactors(Object ratefactorParent) {
		List<RateFactor> list = new ArrayList<RateFactor>();
		
		RateFactorsLink rateFactorsLink = getRateFactorsLink(ratefactorParent);
		if (rateFactorsLink != null && rateFactorsLink.getRateFactors() != null) {
			list.addAll(rateFactorsLink.getRateFactors());
		}
		
		// Filter UI ratefactors
		List<LookupTableItem> excludeList = lookupUIHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_RateFactorExcludeList);
		if (excludeList != null && !excludeList.isEmpty()) {
			String rateFactorPrefix = getRateFactorPrefix(ratefactorParent);
			
			list.removeIf(o1 -> excludeList.stream().filter(o2 -> 
											o2.getItemKey().equalsIgnoreCase(o1.getRateFactorId()) ||
											(rateFactorPrefix+o2.getItemKey()).equalsIgnoreCase(o1.getRateFactorId())
											).findFirst().isPresent());
		}
		
		return list;
	}
}
