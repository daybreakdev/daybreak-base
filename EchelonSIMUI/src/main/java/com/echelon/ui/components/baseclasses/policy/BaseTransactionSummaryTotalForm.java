package com.echelon.ui.components.baseclasses.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLabelFormat;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.ds.ins.utils.Constants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class BaseTransactionSummaryTotalForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8589128240148660551L;
	private Grid<SummaryTotalData> summaryGrid;
	
	private static final String COLUMN_ADJAMT = "ADJAMT";
	
	protected class SummaryTotalData implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6975565605191529511L;
		private 	String 			subPolicyTypeCd;
		private 	String 			subPolicyTypeDesc;
		private		Double			transactionPremium;
		private		Double			writtenPremium;
		private 	Double			annualPremium;
		private 	Double 			netPremiumChange;
		
		public SummaryTotalData(String subPolicyTypeCd, String subPolicyTypeDesc, Double transactionPremium,
				Double writtenPremium, Double annualPremium, Double netPremiumChange) {
			super();
			this.subPolicyTypeCd = subPolicyTypeCd;
			this.subPolicyTypeDesc = subPolicyTypeDesc;
			this.transactionPremium = transactionPremium;
			this.writtenPremium = writtenPremium;
			this.annualPremium = annualPremium;
			this.netPremiumChange = netPremiumChange;
		}

		public String getSubPolicyTypeCd() {
			return subPolicyTypeCd;
		}

		public String getSubPolicyTypeDesc() {
			return subPolicyTypeDesc;
		}

		public Double getTransactionPremium() {
			return transactionPremium;
		}

		public Double getWrittenPremium() {
			return writtenPremium;
		}

		public Double getAnnualPremium() {
			return annualPremium;
		}

		public Double getNetPremiumChange() {
			return netPremiumChange;
		}
		
	}

	public BaseTransactionSummaryTotalForm() {
		super();
		// TODO Auto-generated constructor stub
		
		initializeLayout();
	}

	public BaseTransactionSummaryTotalForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayout();
	}
	
	protected void initializeLayout() {
		// Remove gaps between details
		//this.setSizeFull();
		/*
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													labels.getString("TransactionSummaryTitle"),
													null);
		}
		*/
		
		buildGrid();
	}

	protected void buildGrid() {
		summaryGrid = new Grid<SummaryTotalData>();
		summaryGrid.setSizeFull();
		uiHelper.setGridStyle(summaryGrid);
		
		summaryGrid.addColumn(SummaryTotalData::getSubPolicyTypeDesc)
					.setHeader(labels.getString("TransactionSummaryColumnSubpolicyLabel"));
		summaryGrid.addColumn(new NumberRenderer<SummaryTotalData>(o -> o.getNetPremiumChange(), DSNumericFormat.getPremiumInstance()))
					.setHeader(labels.getString("TransactionSummaryColumnNetPremChangeLabel"))
					.setTextAlign(ColumnTextAlign.END)
					.setKey(COLUMN_ADJAMT)
					.setVisible(false);
		summaryGrid.addColumn(new NumberRenderer<SummaryTotalData>(o -> o.getTransactionPremium(), DSNumericFormat.getPremiumInstance()))
					.setHeader(labels.getString("TransactionSummaryColumnTransPremLabel"))
					.setTextAlign(ColumnTextAlign.END);
		summaryGrid.addColumn(new NumberRenderer<SummaryTotalData>(o -> o.getAnnualPremium(), DSNumericFormat.getPremiumInstance()))
					.setHeader(labels.getString("TransactionSummaryColumnAnnualPremLabel"))
					.setTextAlign(ColumnTextAlign.END);
		summaryGrid.addColumn(new NumberRenderer<SummaryTotalData>(o -> o.getWrittenPremium(), DSNumericFormat.getPremiumInstance()))
					.setHeader(labels.getString("TransactionSummaryColumnWrittenPremLabel"))
					.setTextAlign(ColumnTextAlign.END);
		
		summaryGrid.setSelectionMode(SelectionMode.SINGLE);
		summaryGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		summaryGrid.setHeightByRows(true);
		
		VerticalLayout layout = new VerticalLayout();
		uiHelper.formatFullLayout(layout);
		layout.setMinWidth(BaseTransactionSummaryContentView.SUMMARYWIDTH);
		Label title = new Label(labels.getString("TransactionSummaryTitle"));
		uiHelper.setLabelFooterStyle(title);
		layout.add(title);
		
		Details details = new Details();
		details.setSummary(layout);
		details.setContent(summaryGrid);
		details.setOpened(true);
		details.addThemeVariants(DetailsVariant.FILLED, DetailsVariant.REVERSE);
		this.add(details);
	}
	
	public Integer loadData() {
		List<SummaryTotalData> datalist = new ArrayList<SummaryTotalData>();
		
		PolicyTransaction<?> policyTransaction = uiPolicyHelper.getCurrentPolicyTransaction();
		
		if (StringUtils.equalsIgnoreCase(policyTransaction.getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_ADJUSTMENT)) {
			summaryGrid.getColumnByKey(COLUMN_ADJAMT).setVisible(true);
		}
		
		if (policyTransaction != null) {
			SubPolicy<?> subPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
			if (subPolicy != null) {
				addSummaryData(subPolicy, datalist);
			}
			
			subPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
			if (subPolicy != null) {
				addSummaryData(subPolicy, datalist);
			}
			
			subPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
			if (subPolicy != null) {
				addSummaryData(subPolicy, datalist);
			}
			
			subPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
			if (subPolicy != null) {
				addSummaryData(subPolicy, datalist);
			}
			
			subPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
			if (subPolicy != null) {
				addSummaryData(subPolicy, datalist);
			}
		}

		Double tot1 = 0D; //new BigDecimal(0).setScale(uiConfigHelper.getPremiumDecimals(), BigDecimal.ROUND_HALF_UP);
		Double tot2 = 0D; //new BigDecimal(0).setScale(uiConfigHelper.getPremiumDecimals(), BigDecimal.ROUND_HALF_UP);
		Double tot3 = 0D;
		Double tot4 = 0D;
		for(SummaryTotalData data : datalist) {
			if (data.getTransactionPremium() != null) {
				tot1 = tot1 + data.getTransactionPremium();
				//tot1 = tot1.add(new BigDecimal(data.getTransactionPremium()));
			}
			if (data.getWrittenPremium() != null) {
				tot2 = tot2 + data.getWrittenPremium();
				//tot2 = tot2.add(new BigDecimal(data.getWrittenPremium()));
			}
			if (data.getAnnualPremium() != null) {
				tot3 = tot3 + data.getAnnualPremium();
				//tot2 = tot2.add(new BigDecimal(data.getAnnualPremium()));
			}
			if (data.getNetPremiumChange() != null) {
				tot4 = tot4 + data.getNetPremiumChange();
			}
		}
		tot1 = uiHelper.roundNumeric(tot1);
		tot2 = uiHelper.roundNumeric(tot2);
		tot3 = uiHelper.roundNumeric(tot3);
		addSummaryData("", labels.getString("TransactionSummaryTotLabel"), tot1, tot2, tot3, tot4, datalist);

		summaryGrid.setItems(datalist);
		
		return datalist.size();
	}
	
	protected void addSummaryData(SubPolicy<?> subPolicy, List<SummaryTotalData> list) {
		addSummaryData(subPolicy.getSubPolicyTypeCd(), 
						(new DSLabelFormat()).getFormattedValue("TransactionSummarySubpolicyDes", null, subPolicy.getSubPolicyTypeCd()), 
						subPolicy.getSubPolicyPremium().getTransactionPremium(), 
						subPolicy.getSubPolicyPremium().getWrittenPremium(), 
						subPolicy.getSubPolicyPremium().getAnnualPremium(),
						subPolicy.getSubPolicyPremium().getNetPremiumChange(),
						list);
	}
	
	protected void addSummaryData(String cd, String desc, Double transactionPremium, Double writtenPremium, Double annualPremium, Double netPremiumChange, 
									List<SummaryTotalData> list) {
		list.add(new SummaryTotalData(cd, desc, transactionPremium, writtenPremium, annualPremium, netPremiumChange));
	}
}
