package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class CommercialPropertySubPolicyForm extends BaseLayout
	implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5248450288214060745L;

	private Binder<CommercialPropertySubPolicy<?>> binder;

	public CommercialPropertySubPolicyForm(String uiMode,
		UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildFields();
		buildForm();
	}

	public void loadData(CommercialPropertySubPolicy<?> subPolicy) {
		loadDataConfigs(binder);
		binder.readBean(subPolicy);
		enableDataView(binder);
	}

	public void loadValues() {
		loadDataConfigs(binder);
		// uiHelper.setFieldValue(...)
		enableDataView(binder);
	}

	private void buildFields() {
		labels = uiHelper.getSessionLabels();

		// Initialize fields

		// Bind fields to binder
		// binder.forField(...).bind(Class::get..., Clas::set...)
		binder = new Binder<>();
	}

	private void buildForm() {
		FormLayout form = new FormLayout();
		form.setResponsiveSteps(
			new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2));

		this.add(form);
	}

	public boolean validateEdit() {
		return uiHelper.validateValues(binder);
	}

	public boolean saveEdit(CommercialPropertySubPolicy<?> subPolicy) {
		return uiHelper.writeValues(binder, subPolicy);
	}

	public void enableEdit() {
		enableDataEdit(binder);

		setFocus();
	}

	@Override
	public void setFocus() {
		// Set focus to first field
	}
}
