package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.SIMFactorField;
import com.echelon.ui.components.towing.CargoSubPolicyValueForm;
import com.echelon.ui.components.towing.CargoSubPolicyValueMultiForm;

public class LHTCargoSubPolicyValueForm extends CargoSubPolicyValueForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1256063333974837563L;

	public LHTCargoSubPolicyValueForm(String uiMode, UICONTAINERTYPES uiContainerType,
			CargoSubPolicyValueMultiForm parentForm) {
		super(uiMode, uiContainerType, parentForm);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSNumberField<Double> createPartialPremField() {
		return new SIMFactorField(labels.getString("CargoSubPolicyValuePartialPremiumLabel"));
	}

}
