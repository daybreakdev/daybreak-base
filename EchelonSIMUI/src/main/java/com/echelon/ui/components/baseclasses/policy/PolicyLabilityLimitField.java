package com.echelon.ui.components.baseclasses.policy;

import java.util.Objects;
import java.util.Optional;

import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.uicommon.components.DSComboBox;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;

public class PolicyLabilityLimitField extends DSComboBox<Integer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7554270061954368235L;
	private UIHelper uiHelper;
	private final PolicyUIHelper policyUIHelper;
	private ProductConfigUIHelper configUIHelper;
	private final boolean isNewPolicy;

	public PolicyLabilityLimitField(String label) {
		this(label, false); // default to updating an existing policy
	}

	/**
	 * Initialize a new policy liability limit field.
	 * 
	 * @param label       the field's label
	 * @param isNewPolicy whether the field is for a newly-created policy or an
	 *                    update to an existing policy
	 */
	public PolicyLabilityLimitField(String label, boolean isNewPolicy) {
		super(label, Integer.class);
		// TODO Auto-generated constructor stub
		this.isNewPolicy = isNewPolicy;

		this.uiHelper = BrokerUI.getHelperSession().getUiHelper();
		this.policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		this.configUIHelper = BrokerUI.getHelperSession().getConfigUIHelper();
		this.addValueChangeListener(o -> onValueChanged());
		this.withLookupTableId(ConfigConstants.LOOKUPTABLE_LiabLimitList);
	}

	protected void onValueChanged() {
		if (!this.isReadOnly()) {
			final IGenericProductConfig productConfig = configUIHelper.getCurrentProductConfig();
			if (productConfig != null && productConfig.isReinsurable() && productConfig.getMaxRetainedLimit() > 0
					&& this.getValue() != null) {
				final int maxRetainedLimit = getPolicyMaxRetainedLimit(policyUIHelper.getCurrentPolicyTransaction(),
						productConfig);
				final int currentLimit = this.getValue().intValue();

				if (currentLimit > maxRetainedLimit) {
					uiHelper.notificationWarning(
							uiHelper.getSessionLabels().getString("WarnNewPolicyLimitExceedMaxRetainedLimit"));
				} else if (!isNewPolicy && currentLimit <= maxRetainedLimit) {
					// No need to warn if it's a new policy; there won't be any reinsurance entries.
					uiHelper.notificationWarning(
							uiHelper.getSessionLabels().getString("WarnPolicyLimitBelowMaxRetainedLimit"));
				}
			}
		}
	}

	private int getPolicyMaxRetainedLimit(PolicyTransaction<?> pcyTxn, IGenericProductConfig prodConfig) {
		/*
		 * This code is intended to be temporary. The intent is to allow LHT ON policies
		 * issued using the old (v1) ratebook to use the old maxRetainedLimit value of
		 * 2,000,000. Policies issued using v2 will use the current value of 5M instead.
		 * 
		 * See ES-106 for further information.
		 */
		final Optional<PolicyTerm> policyTerm = Optional.ofNullable(pcyTxn).map(PolicyTransaction::getPolicyTerm);
		if (policyTerm.isPresent()) {
			final PolicyTerm pt = policyTerm.get();
			if ("ONLHT".equalsIgnoreCase(pt.getRatebookId()) && Objects.equals(1, pt.getRatebookVersion())) {
				return 2_000_000;
			}
		}
		// End of ES-106 "maxRetainedLimit override" code

		return prodConfig.getMaxRetainedLimit();
	}
}
