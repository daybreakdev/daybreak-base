package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.dto.ReinsuranceItemDTO;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class BaseTransactionSummaryReinsuranceForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1346048287194381372L;
	private Grid<ReinsuranceItemDTO> 	summaryGrid;
	private ReinsuranceItemDTO			lastRowDTO;

	public BaseTransactionSummaryReinsuranceForm() {
		super();
		// TODO Auto-generated constructor stub
		initializeLayout();
	}

	public BaseTransactionSummaryReinsuranceForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
	}
	
	protected void initializeLayout() {
		buildGrid();
	}

	protected void buildGrid() {
		summaryGrid = new Grid<ReinsuranceItemDTO>();
		summaryGrid.setSizeFull();
		uiHelper.setGridStyle(summaryGrid);
		
		summaryGrid.addColumn(ReinsuranceItemDTO::getReinsurerName)
					.setHeader(labels.getString("TransSummaryReinsuranceInsurerLabel"))
					.setFlexGrow(3);
		summaryGrid.addColumn(ReinsuranceItemDTO::getEndLayer)
					.setHeader(labels.getString("TransSummaryReinsuranceLayerLabel"))
					.setFlexGrow(0);
		summaryGrid.addColumn(new NumberRenderer<ReinsuranceItemDTO>(o -> o.getGrossUpPerc(), DSNumericFormat.getAmountInstance()))
					.setHeader(labels.getString("TransSummaryReinsuranceGrossUpLabel"))
					.setTextAlign(ColumnTextAlign.END)
					.setFlexGrow(0);
		summaryGrid.addColumn(new NumberRenderer<ReinsuranceItemDTO>(o -> o.getNetPremium(), DSNumericFormat.getAmountInstance()))
					.setHeader(labels.getString("TransSummaryReinsuranceNetPremiumLabel"))
					.setTextAlign(ColumnTextAlign.END)
					.setFlexGrow(1);
		summaryGrid.addColumn(new NumberRenderer<ReinsuranceItemDTO>(o -> o.getGrossPremium(), DSNumericFormat.getAmountInstance()))
					.setHeader(labels.getString("TransSummaryReinsuranceGrossPremiumLabel"))
					.setTextAlign(ColumnTextAlign.END)
					.setFlexGrow(1);
		
		summaryGrid.setSelectionMode(SelectionMode.SINGLE);
		summaryGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		summaryGrid.setHeightByRows(true);
		summaryGrid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
			return setupRowLine(o);
		}));
		
		VerticalLayout layout = new VerticalLayout();
		uiHelper.formatFullLayout(layout);
		layout.setMinWidth(BaseTransactionSummaryContentView.SUMMARYWIDTH);
		Label title = new Label(labels.getString("TransSummaryReinsuranceTitle"));
		uiHelper.setLabelFooterStyle(title);
		layout.add(title);
		
		Details details = new Details();
		details.setSummary(layout);
		details.setContent(summaryGrid);
		details.setOpened(true);
		details.addThemeVariants(DetailsVariant.FILLED, DetailsVariant.REVERSE);
		this.add(details);
	}
	
	public Integer loadData() {
		lastRowDTO = null;
		ReinsuranceReportDTO reportDTO = uiPolicyHelper.getCurrentPolicy().getReinsuranceReportDTO();
		List<ReinsuranceItemDTO> itemDTOs = new ArrayList<ReinsuranceItemDTO>();
		
		if (reportDTO != null) {
			if (reportDTO.getReisnuranceItems() != null && !reportDTO.getReisnuranceItems().isEmpty()) {
				itemDTOs.addAll(reportDTO.getReisnuranceItems());
				lastRowDTO = itemDTOs.get(itemDTOs.size()-1);
			}
			
			itemDTOs.add(createTotalItem("TransSummaryReinsuranceReinsuranceTotalLabel", reportDTO.getNetPremReisnurnaceTotal(), reportDTO.getGrossPremReisnurnaceTotal()));
			itemDTOs.add(createTotalItem("TransSummaryReinsuranceRetentionTotalLabel", reportDTO.getNetPremRetentionTotal(), reportDTO.getGrossPremRetentionTotal()));
			itemDTOs.add(createTotalItem("TransSummaryReinsuranceGrandTotalLabel", reportDTO.getNetPremGrandTotal(), reportDTO.getGrossPremGrandTotal()));
		}
		
		summaryGrid.setItems(itemDTOs);
		
		if (lastRowDTO != null) {
			summaryGrid.setDetailsVisible(lastRowDTO, true);
		}
		
		return 1;
	}
	
	private ReinsuranceItemDTO createTotalItem(String nameCode, Double netValue, Double grossValue) {
		ReinsuranceItemDTO tot = new ReinsuranceItemDTO();
		tot.setReinsurerName(labels.getString(nameCode));
		tot.setNetPremium(netValue);
		tot.setGrossPremium(grossValue);
		return tot;
	}
	
	private Component setupRowLine(ReinsuranceItemDTO dto) {
		if (dto != null && lastRowDTO != null && dto.equals(lastRowDTO)) {
			return uiHelper.addGridRowLineBottom(true);
		}
		
		return null;
	}
}
