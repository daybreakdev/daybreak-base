package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyUIHelper;

public class GarageSubPolicyView extends TowingSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1032705103569827418L;
	private GarageSubPolicyUIHelper	garageHelper = new GarageSubPolicyUIHelper();
	private GarageSubPolicyForm		garageSubPolicyForm;

	public GarageSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeSubPolicyView(null);
		buildComponents();
	}

	private void buildComponents() {
		garageSubPolicyForm = new GarageSubPolicyForm(this.uiMode, this.uiContainerType);
		this.add(garageSubPolicyForm);
	}

	public boolean loadData(GarageSubPolicy<?> garageSubPolicy) {
		boolean selectedSubPolicy = garageSubPolicy != null;
		garageSubPolicyForm.loadData(garageSubPolicy);

		refreshToolbar(garageSubPolicy, true, true, true);
		return true;
	}

	@Override
	protected void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).addClickListener(e -> {
					garageHelper.newSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
					garageHelper.updateSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(e -> {
					garageHelper.deleteSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(e -> {
					garageHelper.undeleteSubPolicyAction();
				});
			}
		}
	}

}
