package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;

public class GarageSubPolicyLocCovForm extends SubPolicyLocCovForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5260692413810971838L;

	public GarageSubPolicyLocCovForm(String uiMode, UICONTAINERTYPES uiContainerType, OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildCoverageTopForm() {
		super.buildCoverageTopForm();
		
		coverageTopForm.add(maxNumOfVehiclesField, coverageLimitField, coverageDeductibleField);
	}
	
	@Override
	protected void buildPremiunmForm(CoverageDto coverageDto) {
		super.buildPremiunmForm(coverageDto);
		
		premiumForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 6)
		        );
		
		//First half
		premiumForm.add(coverageIsPremiumOverrideField, 2);
		//Second half
		premiumForm.add(coverageRatedPremiumField, coverageAnnualPremiumField, coverageNetPremiumChangeField, coverageWrittenPremiumField);
		
		premiumForm.setVisible(true);
	}

}
