package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSTreeGridItem;

public interface IPolicyViewContent {
	
	public void loadContentData(DSTreeGridItem nodeItem);
	
}
