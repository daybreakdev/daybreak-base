package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.helpers.policy.towing.LocationLinkUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.provider.ListDataProvider;

public class LocationPicker extends LocationList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7429082812731860513L;
	private Set<PolicyLocation> 	selectedLocations;
	private LocationLinkUIHelper 	locationLinkUIHelper;

	public LocationPicker(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.selectedLocations 		= new HashSet<PolicyLocation>();
		this.locationLinkUIHelper	= new LocationLinkUIHelper();
	}
	
	@Override
	protected void createIndicatorColumn(Grid<?> grid) {
		// TODO Auto-generated method stub
		//super.createIndicatorColumn(grid);
		//Indicator for location picker
	}

	@Override
	protected void addGridColumns() {
		riskGrid.addComponentColumn(o -> createColumnCheckBox(o))
						.setHeader(createHeaderColumnCheckBox());
		super.addGridColumns();
	}
	
	protected Checkbox createHeaderColumnCheckBox() {
		Checkbox checkbox = new Checkbox();
		checkbox.addValueChangeListener(new HasValue.ValueChangeListener<ValueChangeEvent<?>>() {
			@Override
			public void valueChanged(ValueChangeEvent<?> event) {
				boolean isChecked = (event.getValue() != null && (Boolean)event.getValue());
				if (!isChecked) {
					selectedLocations.clear();
				}
				else {
					ListDataProvider<PolicyLocation> dataProvider = (ListDataProvider<PolicyLocation>) riskGrid.getDataProvider();
					if (dataProvider.getItems() != null) {
						dataProvider.getItems().stream().forEach(o -> selectedLocations.add(o));
					}
				}
				
				riskGrid.getDataProvider().refreshAll();
			}
		});
		return checkbox;
	}
	
	protected Checkbox createColumnCheckBox(PolicyLocation location) {
		Checkbox checkbox = new Checkbox();
		checkbox.setValue(selectedLocations.contains(location));
		
		checkbox.addValueChangeListener(new HasValue.ValueChangeListener<ValueChangeEvent<?>>() {
			@Override
			public void valueChanged(ValueChangeEvent<?> event) {
				boolean isChecked = (event.getValue() != null && (Boolean)event.getValue());
				if (isChecked) {
					selectedLocations.add(location);
				}
				else {
					selectedLocations.remove(location);
				}
			}
		});
		return checkbox;
	}

	@Override
	protected void buildGrid() {
		// TODO Auto-generated method stub
		super.buildGrid();
		
		riskGrid.setSelectionMode(SelectionMode.NONE);
	}
	
	@Override
	public boolean loadData(List<PolicyLocation> selectedLocations) {
		List<PolicyLocation> avaLocations = new ArrayList<PolicyLocation>();
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			avaLocations = locationLinkUIHelper.getLocationLookupItems();
			
			if (selectedLocations != null && !selectedLocations.isEmpty()) {
				avaLocations.removeIf(o -> selectedLocations.contains(o));
			}
		}
		
		return super.loadData(avaLocations);
	}
	
	public List<PolicyLocation> getSelected() {
		List<PolicyLocation> selected = new ArrayList<PolicyLocation>();
		selected.addAll(selectedLocations);
		
		return selected;
	}
	
	public boolean validateSelection() {
		if (selectedLocations.isEmpty()) {
			uiHelper.notificationWarning(labels.getString("LocationLinkPickerNoSelectionError"));
			return false;
		}
		
		return true;
	}
}
