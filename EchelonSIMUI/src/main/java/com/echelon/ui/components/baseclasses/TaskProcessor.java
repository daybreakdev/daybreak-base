package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSTaskProcessor;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.processor.DSBaseProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;

public class TaskProcessor extends DSTaskProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2075468837434416887L;

	public TaskProcessor(UI taskU1, IDSDialog dataDialog) {
		super(taskU1, dataDialog);
		// TODO Auto-generated constructor stub
	}

	public TaskProcessor(UI taskU1) {
		super(taskU1);
		// TODO Auto-generated constructor stub
	}

	public TaskProcessor() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	public void initializeProcessor(DSBaseProcessor processor) {
		if (processor instanceof BasePolicyProcessor) {
			((BasePolicyProcessor)processor)
				.setProcessorValues(BrokerUI.getUserSession().getUserProfile(), 
									BrokerUI.getHelperSession().getConfigUIHelper().getCurrentProductConfig(), 
									BrokerUI.getHelperSession().getPolicyUIHelper().getPolicyProcessing());
			return;
		}
		
		super.initializeProcessor(processor);
	}
	
	public void manualStart(DSBaseProcessor processor) {
		initializeProcessor(processor);
		
		DSTaskRun taskRun = new DSTaskRun(processor);
		taskRun.run();
		
		showRunResult(taskRun);
	}
}
