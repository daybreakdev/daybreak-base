package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.DSNumberField.PRESENTFORMATS;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;

public class UWAdjustmentForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 612954258935210805L;
	private DSNumberField<Double> uwAfjuestmentField;

	public UWAdjustmentForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
 
		buildLayout();
		buildFields();
		buildForm();
	}
	
	private void buildLayout() {
		this.setWidth("200px");
	}

	private void buildFields() {
		uwAfjuestmentField = new DSNumberField<Double>(labels.getString("CoverageFormUWAdjustmentLabel"), PRESENTFORMATS.UWAdjustment);
		uwAfjuestmentField.setRequired(true);
		uwAfjuestmentField.setRequiredIndicatorVisible(true);
	}
	
	private void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 1)
		        );
		
		form.add(uwAfjuestmentField);
		
		this.add(form);
	}
	
	public Double getUwAfjuestmentValue() {
		return uwAfjuestmentField.getModelValue();
	}
	
	public void setFocus() {
		uwAfjuestmentField.focus();
	}
	
	public boolean validEdit() {
		try {
			uwAfjuestmentField.validate();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		if (uwAfjuestmentField.isInvalid()) {
			uiHelper.showWaringRequiredFields();
			return false;
		}
		
		return true;
	}
}
