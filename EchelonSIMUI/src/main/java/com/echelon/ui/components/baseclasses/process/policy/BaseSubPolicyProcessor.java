package com.echelon.ui.components.baseclasses.process.policy;

import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;

public abstract class BaseSubPolicyProcessor<MODEL extends SubPolicy<?>> extends BasePolicyProcessor {

	protected String subPolicyTypeCd;
	protected MODEL  subPolicy;

	public BaseSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	public BaseSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			String subPolicyTypeCd) {
		super(processAction, policyTransaction);
		this.subPolicyTypeCd = subPolicyTypeCd;
	}

	public BaseSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction, MODEL subPolicy) {
		super(processAction, policyTransaction);
		this.subPolicy = subPolicy;
	}

	public MODEL getSubPolicy() {
		return this.subPolicy;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Update:
			try {
				subPolicy = loadSubPolicy();
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			break;

		case UIConstants.UIACTION_New:
			try {
				subPolicy = createSubPolicy();
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewSubPolicy(subPolicy);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;

		case UIConstants.UIACTION_Save:
			try {
				subPolicy = updateSubPolicy(subPolicy);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
		
		case UIConstants.UIACTION_Delete:
			try {
				subPolicy = loadSubPolicy();
				deleteSubPolicy(subPolicy);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		case UIConstants.UIACTION_Undelete:
			try {
				subPolicy = loadSubPolicy();
				undeleteSubPolicy(subPolicy);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		case UIConstants.UIACTION_LoadData:
			try {
				subPolicy = (MODEL) loadSubPolicyData(subPolicy);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
			}

		default:
			break;
		}
	}
	
	protected MODEL createSubPolicy() throws Exception  {
		return null;
	}

	protected void saveNewSubPolicy(MODEL subPolicy) throws InsurancePolicyException {
		policyTransaction = subPolicy.getPolicyTransaction();
		updatePolicyTransaction();
	}

	protected MODEL updateSubPolicy(MODEL subPolicy) throws InsurancePolicyException {
		return (MODEL) ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSubPolicy(subPolicy, getUserProfile());
	}

	protected void deleteSubPolicy(MODEL subPolicy) throws InsurancePolicyException {
		//policyTransaction.removeSubPolicy(subPolicy);
		subPolicy = loadSubPolicy();
		policyTransaction = policyProcessing.removeSubPolicy(policyTransaction, subPolicy);
		updatePolicyTransaction();
	}

	protected void undeleteSubPolicy(MODEL subPolicy) throws InsurancePolicyException {
		//policyTransaction = loadFullPolicyTransactionData(policyTransaction);
		subPolicy = loadSubPolicy();
		policyTransaction = policyProcessing.undeleteSubPolicy(policyTransaction, subPolicy);
		updatePolicyTransaction();
	}
		
	protected MODEL loadSubPolicy() throws InsurancePolicyException {
		// Require to update location risk
		for(Object _risk : policyTransaction.getPolicyRisks()) {
			loadPolicyRiskData((Risk) _risk);
		}
		return (MODEL) loadSubPolicy(subPolicyTypeCd);
	}

	protected boolean endRunProcesses() {
		ratePolicyTransaction(subPolicy, null);
		return true;
	}
}
