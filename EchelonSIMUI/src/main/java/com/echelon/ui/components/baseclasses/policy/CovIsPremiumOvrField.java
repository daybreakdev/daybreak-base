package com.echelon.ui.components.baseclasses.policy;

import org.apache.commons.lang3.BooleanUtils;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

public class CovIsPremiumOvrField extends Checkbox {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3372001438813406190L;
	
	protected static enum BINDTYPES {REINSURANCE, PREMIUMOVERRIDE, DEFAULT};
	
	protected UIHelper					uiHelper;
	protected Binder<CoverageDto> 		binder;
	protected CoverageDto				coverageDto;
	protected CovIsReinsuredField		isReinsuredField;
	protected Registration				isReinsuredValueChangeListener;
	protected BINDTYPES					lastBindType;
	
	public CovIsPremiumOvrField(String label) {
		super(label);
		
		uiHelper = BrokerUI.getHelperSession().getUiHelper();
	}
	
	public void initializeField(CoverageDto dto, CovIsReinsuredField isReinsuredField) {
		this.coverageDto = dto;
		this.isReinsuredField = isReinsuredField;
	}
	
	protected boolean isReinsured() {
		return (isReinsuredField != null && BooleanUtils.isTrue(isReinsuredField.getValue()));
	}
	
	protected boolean isAllowPremiumOverride() {
		return coverageDto.isAllowPremiumOverride();
	}
	
	public void bindField(Binder<CoverageDto> binder) {
		this.binder = binder;
		
		BINDTYPES bindType = null;
		if (isReinsured()) {
			bindType = BINDTYPES.REINSURANCE;
		}
		else if ((isAllowPremiumOverride() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) || BooleanUtils.isTrue(coverageDto.isPremiumOverride())) {
			bindType = BINDTYPES.PREMIUMOVERRIDE;
		}
		else {
			bindType = BINDTYPES.DEFAULT;
		}
		
		if (lastBindType == null || !bindType.equals(lastBindType)) {
			binder.removeBinding(this);
			if (BINDTYPES.REINSURANCE.equals(bindType)) {
				
			}
			else if (BINDTYPES.PREMIUMOVERRIDE.equals(bindType)) {
				binder.forField(this)
									.bind(CoverageDto::isPremiumOverride, 
										  CoverageDto::setIsPremiumOverride);
			}
			else {
				binder.forField(this)
									.bind(CoverageDto::isPremiumOverride, null);
			}
			
			lastBindType = bindType;
		}
	}
	
	public void determineReadOnly() {
		if (coverageDto.isDeletedCoverage() || coverageDto.wasDeletedCoverage()) {
			uiHelper.setEditFieldsReadonly(this);
			
			if (this.isReinsuredValueChangeListener != null) {
				this.isReinsuredValueChangeListener.remove();
				this.isReinsuredValueChangeListener = null;
			}
		}
		else {
			if (this.isReinsuredValueChangeListener == null && isReinsuredField != null) {
				this.isReinsuredValueChangeListener = isReinsuredField.addValueChangeListener(e -> this.coverageReinsuranceValueChanged());
			}
			
			coverageReinsuranceValueChanged();			
		}
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && isAllowPremiumOverride() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) {
			this.setTabIndex(value);
			return true;
		}
		else {
			this.setTabIndex(-1);
			return false;
		}
	}
	
	public void coverageReinsuranceValueChanged() {
		bindField(this.binder);
		
		if (isReinsured()) {
			uiHelper.setFieldValue(this, coverageDto.isPrimaryReinsurancePremiumOverride());
			uiHelper.setEditFieldsReadonly(this);
		}
		else if (isAllowPremiumOverride()) {
			uiHelper.setEditFieldsNotReadonly(this);
		}
		else if (BooleanUtils.isTrue(this.getValue())) {
			uiHelper.setFieldValue(this, Boolean.FALSE);
			uiHelper.setEditFieldsReadonly(this);
		}
	}
}
