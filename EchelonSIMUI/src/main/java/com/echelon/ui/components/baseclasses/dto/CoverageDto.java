package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.entities.DataExtension;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.policy.CovDeductibleField;
import com.echelon.ui.components.baseclasses.policy.CovIsPremiumOvrField;
import com.echelon.ui.components.baseclasses.policy.CovIsReinsuredField;
import com.echelon.ui.components.baseclasses.policy.CovLimitField;
import com.echelon.ui.components.baseclasses.policy.CovPremiumField;
import com.echelon.ui.components.baseclasses.policy.CovUWAdjustmentField;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAdditionalForm;
import com.echelon.ui.components.towing.dto.LocCovProcessDto;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.data.binder.Binder;

public class CoverageDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2871529570078856677L;
	private IBusinessEntity			 coverageParent; 
	private ICoverageConfig			 coverageConfig;
	private Coverage 				 coverage;
	private Endorsement				 endorsement;
	private boolean 				 isDetailRequired;
	private boolean					 detailEntered;
	private Integer					 numOfUnits;
	
	// Edit List
	private UIConstants.COV_EDIT_STATUSES editStatus;
	
	private Button 					 deleteButton;
	private Button 					 undeleteButton;
	private Boolean					 isEditValid;
	private	boolean 				 isAllowPremiumOverride;
	private boolean 				 isLimitInteger;
	private boolean 				 isLimitMandatory;
	private boolean 				 isAllowLimitOverride;
	private boolean 				 isDeductibleInteger;
	private boolean 				 isDeductibleMandatory;
	private boolean 				 isAllowDeductibleOverride;
	private boolean 				 isAllowReinsurance;
	
	private CovLimitField			 editLimitField;
	private CovDeductibleField	 	 editDeductibleField;
	private CovPremiumField		 	 editPremiumField;
	private CovIsPremiumOvrField	 editIsPremiumOverrideField;
	private CovIsReinsuredField		 editIsReinsuredField;
	private CovUWAdjustmentField   	 editUwAdjustmentField;
	private Binder<CoverageDto> 	 editBinder;
	private boolean					 editHasGeneralError;
	private Set<String> 			 editMessages;

	// edit form
	private IBaseCovAdditionalForm 	 coverageAdditionalForm;
	
	// location process
	private LocCovProcessDto		 locCovProcessDto;
	
	// e.g. total row
	private boolean 				 isCustomRow;
	
	private List<CoverageReinsuranceDto> reinsuranceDtos;
	
	public CoverageDto(ICoverageConfig coverageConfig, Coverage coverage, IBusinessEntity coverageParent) {
		super();
		this.coverageConfig 	= coverageConfig;
		this.coverage 			= coverage;
		this.coverageParent		= coverageParent;
		this.reinsuranceDtos	= new ArrayList<CoverageReinsuranceDto>();
		
		if (this.coverage.getCoverageReinsurances() != null && !this.coverage.getCoverageReinsurances().isEmpty()) {
			this.coverage.getCoverageReinsurances().forEach(o -> this.reinsuranceDtos.add(
						new CoverageReinsuranceDto(this, o)
					));
		}
	}

	public CoverageDto(ICoverageConfig coverageConfig, Endorsement endorsement, IBusinessEntity coverageParent) {
		super();
		this.coverageConfig 	= coverageConfig;
		this.endorsement 		= endorsement;
		this.coverageParent		= coverageParent;
		this.reinsuranceDtos	= new ArrayList<CoverageReinsuranceDto>();
		
		if (this.endorsement.getEndorsementReinsurances() != null && !this.endorsement.getEndorsementReinsurances().isEmpty()) {
			this.endorsement.getEndorsementReinsurances().forEach(o -> this.reinsuranceDtos.add(
						new CoverageReinsuranceDto(this, o)
					));
		}
	}

	public ICoverageConfig getCoverageConfig() {
		return coverageConfig;
	}
	
	public Object getCoverage() {
		if (this.coverage != null) {
			return coverage;
		}
		else if (this.endorsement != null) {
			return endorsement;
		}

		return null;
	}
	
	public void setCoverage(ICoverageConfig coverageConfig, Object value) {
		this.coverageConfig = coverageConfig;
		if (value != null) {
			if (value instanceof Coverage) {
				this.coverage = (Coverage) value;
			}
			else if (value instanceof Endorsement) {
				this.endorsement = (Endorsement) value;
			}
		}
	}

	public boolean isACoverage() {
		return ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(getCoverageType());
	}

	public boolean isAnEndorsement() {
		return ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(getCoverageType());
	}
	
	public String getCoverageType() {
		String result = null;
		if (this.coverage != null) {
			result = ConfigConstants.COVERAGETYPE_COVERAGE;
		}
		else if (this.endorsement != null) {
			result = ConfigConstants.COVERAGETYPE_ENDORSEMENT;
		}
		
		if (result == null) {
			result = "";
		}
		
		return result;
	}
	
	public Integer getCoverageOrder() {
		Integer result = null;
		if (this.coverage != null) {
			result = this.coverage.getCoverageSequenceNum();
		}
		else if (this.endorsement != null) {
			result = this.endorsement.getEndorsementOrder();
		}
		
		if (result == null) {
			result = 999;
		}
		
		return result;
	}
	
	public Long getCoveragePK() {
		if (this.coverage != null) {
			return this.coverage.getCoveragePK();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getEndorsementsk();
		}
		
		return null;
	}
	
	public String getDescription() {
		if (this.coverage != null) {
			return this.coverage.getDescription();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getDescription();
		}
		
		return null;
	}
	
	public String getCoverageCode() {
		if (this.coverage != null) {
			return this.coverage.getCoverageCode();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getEndorsementCd();
		}
		
		return null;
	}
	
	public Integer getLimit1() {
		if (this.coverage != null) {
			return this.coverage.getLimit1();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getLimit1amount();
		}
		
		return null;
	}
	
	public void setLimit1(Integer limit1) {
		if (this.coverage != null) {
			this.coverage.setLimit1(limit1);
		}
		else if (this.endorsement != null) {
			this.endorsement.setLimit1amount(limit1);
		}
	}
	
	public Integer getDeductible1() {
		if (this.coverage != null) {
			return this.coverage.getDeductible1();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getDeductible1amount();
		}
		
		return null;
	}
	
	public void setDeductible1(Integer deductible1) {
		if (this.coverage != null) {
			this.coverage.setDeductible1(deductible1);
		}
		else if (this.endorsement != null) {
			this.endorsement.setDeductible1amount(deductible1);
		}
	}
	
	public Premium getCoveragePremium() {
		if (this.coverage != null) {
			return this.coverage.getCoveragePremium();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getEndorsementPremium();
		}
		
		return null;
	}
	
	public void setCoveragePremium(Premium premium) {
		if (this.coverage != null) {
			this.coverage.setCoveragePremium(premium);
		}
		else if (this.endorsement != null) {
			this.endorsement.setEndorsementPremium(premium);
		}
	}

	public Double getOriginalPremium() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getOriginalPremium();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getOriginalPremium();
		}
		
		return null;
	}

	public Double getAnnualPremium() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getAnnualPremium();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getAnnualPremium();
		}
		
		return null;
	}

	public void setAnnualPremium(Double value) {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			this.coverage.getCoveragePremium().setAnnualPremium(value);
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			this.endorsement.getEndorsementPremium().setAnnualPremium(value);
		}
	}

	public Boolean isPremiumOverride() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().isPremiumOverride();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().isPremiumOverride();
		}
		
		return null;
	}

	public void setIsPremiumOverride(Boolean value) {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			this.coverage.getCoveragePremium().setPremiumOverride(value);
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			this.endorsement.getEndorsementPremium().setPremiumOverride(value);
		}
	}

	public Boolean isReinsured() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.isReinsured();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.isReinsured();
		}
		
		return null;
	}

	public void setIsReinsured(Boolean value) {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			this.coverage.setReinsured(value);
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			this.endorsement.setReinsured(value);
		}
	}

	public Double getTransactionPremium() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getTransactionPremium();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getTransactionPremium();
		}
		
		return null;
	}
	
	public Double getTermPremium() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getTermPremium();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getTermPremium();
		}
		
		return null;
	}
	
	public Double getNetPremiumChange() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getNetPremiumChange();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getNetPremiumChange();
		}
		
		return null;
	}
	
	public Double getWrittenPremium() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getWrittenPremium();
		}
		else if (this.endorsement != null && this.endorsement.getEndorsementPremium() != null) {
			return this.endorsement.getEndorsementPremium().getWrittenPremium();
		}
		
		return null;
	}

	public Double getUwAdjustment() {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			return this.coverage.getCoveragePremium().getPremiumModifier1();
		}
		
		return null;
	}

	public void setUwAdjustment(Double value) {
		if (this.coverage != null && this.coverage.getCoveragePremium() != null) {
			this.coverage.getCoveragePremium().setPremiumModifier1(value);
		}
	}
	
	public DataExtension getDataExtension() {
		if (this.coverage != null) {
			return this.coverage.getDataExtension();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getDataExtension();
		}
		
		return null;
	}
	
	public void createDataExtensionIfNone() {
		if (getDataExtension() == null) {
			if (this.coverage != null) {
				this.coverage.setDataExtension(new DataExtension());
			}
			else if (this.endorsement != null) {
				this.endorsement.setDataExtension(new DataExtension());
			}
		}
	}
	
	public void removeDataExtensionIfEmpty() {
		DataExtension dataExtension = getDataExtension();
		if (dataExtension != null) {
			if (dataExtension.getExtensionTypes() == null ||
				dataExtension.getExtensionTypes().isEmpty()) {
				
				if (this.coverage != null) {
					this.coverage.setDataExtension(null);
				}
				else if (this.endorsement != null) {
					this.endorsement.setDataExtension(null);
				}
			}
		}		
	}
	
	public LocalDate getCoverageEffDate() {
		if (this.coverage != null) {
			return this.coverage.getCoverageEffDate();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getEndorsementEffective();
		}
		
		return null;
	}
	
	public void setCoverageEffDate(LocalDate coverageEffDate) {
		if (this.coverage != null) {
			this.coverage.setCoverageEffDate(coverageEffDate);
		}
		else if (this.endorsement != null) {
			this.endorsement.setEndorsementEffective(coverageEffDate);
		}
	}
	
	public LocalDate getCoverageExpDate() {
		if (this.coverage != null) {
			return this.coverage.getCoverageExpDate();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getEndorsementExpiry();
		}
		
		return null;
	}
	
	public void setCoverageExpDate(LocalDate coverageExpDate) {
		if (this.coverage != null) {
			this.coverage.setCoverageExpDate(coverageExpDate);
		}
		else if (this.endorsement != null) {
			this.endorsement.setEndorsementExpiry(coverageExpDate);
		}
	}

	public boolean isDetailEntered() {
		return detailEntered;
	}

	public void setDetailEntered(boolean detailEntered) {
		this.detailEntered = detailEntered;
	}

	public Integer getCoverageNumOfUnits() {
		if (this.coverage != null) {
			return this.coverage.getNumOfUnits();
		}
		else if (this.endorsement != null) {
			return this.endorsement.getNumOfUnits();
		}
		
		return null;
	}

	public void setCoverageNumOfUnits(Integer numOfUnits) {
		if (this.coverage != null) {
			this.coverage.setNumOfUnits(numOfUnits);
		}
		else if (this.endorsement != null) {
			this.endorsement.setNumOfUnits(numOfUnits);
		}
	}

	public boolean isDetailRequired() {
		return isDetailRequired;
	}

	public void setDetailRequired(boolean isDetailRequired) {
		this.isDetailRequired = isDetailRequired;
	}

	public boolean hasAnyAdditionalFields() {
		return coverageAdditionalForm != null &&
				coverageAdditionalForm.hasAnyAdditionalFields();
	}

	public IBaseCovAdditionalForm getCoverageAdditionalForm() {
		return coverageAdditionalForm;
	}

	public void setCoverageAdditionalForm(IBaseCovAdditionalForm coverageAdditionalForm) {
		this.coverageAdditionalForm = coverageAdditionalForm;
	}

	public boolean isLimitInteger() {
		return isLimitInteger;
	}

	public void setLimitInteger(boolean isLimitInteger) {
		this.isLimitInteger = isLimitInteger;
	}

	public boolean isDeductibleInteger() {
		return isDeductibleInteger;
	}

	public void setDeductibleInteger(boolean isDeductibleInteger) {
		this.isDeductibleInteger = isDeductibleInteger;
	}

	public Boolean isEditValid() {
		return isEditValid;
	}

	public void setEditValid(Boolean isEditValid) {
		this.isEditValid = isEditValid;
	}

	public UIConstants.COV_EDIT_STATUSES getEditStatus() {
		return editStatus;
	}

	public void setEditStatus(UIConstants.COV_EDIT_STATUSES editStatus) {
		this.editStatus = editStatus;
	}

	public Button getDeleteButton() {
		return deleteButton;
	}

	public void setDeleteButton(Button deleteButton) {
		this.deleteButton = deleteButton;
	}

	public Button getUndeleteButton() {
		return undeleteButton;
	}

	public void setUndeleteButton(Button undeleteButton) {
		this.undeleteButton = undeleteButton;
	}
	
	public Integer getDispNumOfUnits() {
		return numOfUnits;
	}

	public void setDispNumOfUnits(Integer numOfUnits) {
		this.numOfUnits = numOfUnits;
	}

	public Double getDispUnitPremium() {
//		Double result = getDispAnnualPremium();
/*
		Double result = getDispTransactionPremium();
		
		if (result != null && result != 0 && this.numOfUnits != null && this.numOfUnits > 0) {
			result = result / this.numOfUnits;
		}
		
		return result;
*/		
		Double result = null;
		
		// getDispTransactionPremium != null to make sure not to show 0 for no premium
		Double tp = getDispTransactionPremium();
		if (tp != null) {
			if (coverage != null && coverage.getCoveragePremium() != null) {
				result = coverage.getCoveragePremium().getUnitPremium();
			}
			else if (endorsement != null && endorsement.getEndorsementPremium() != null) {
				result = endorsement.getEndorsementPremium().getUnitPremium();
			}
		}
		
		return result;
	}
	
	public Double getDispOriginalPremium() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getOriginalPremium();
		}
		else if (this.getOriginalPremium() != null && 
				this.getOriginalPremium().doubleValue() != 0) {
			return this.getOriginalPremium();
		}
		else {
			return null;
		}
	}
	
	public Double getDispAnnualPremium() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getAnnualPremium();
		}
		else if (this.getAnnualPremium() != null && 
				this.getAnnualPremium().doubleValue() != 0) {
			return this.getAnnualPremium();
		}
		else {
			return null;
		}
	}
	
	public Double getDispTermPremium() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getTermPremium();
		}
		else if (this.getTermPremium() != null && 
				this.getTermPremium().doubleValue() != 0) {
			return this.getTermPremium();
		}
		else {
			return null;
		}
	}
	
	public Double getDispNetPremiumChange() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getNetPremiumChange();
		}
		else if (this.getNetPremiumChange() != null && 
				this.getNetPremiumChange().doubleValue() != 0) {
			return this.getNetPremiumChange();
		}
		else {
			return null;
		}
	}
	
	public Double getDispWrittenPremium() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getWrittenPremium();
		}
		else if (this.getWrittenPremium() != null && 
				this.getWrittenPremium().doubleValue() != 0) {
			return this.getWrittenPremium();
		}
		else {
			return null;
		}
	}
	
	public Double getDispTransactionPremium() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getTransactionPremium();
		}
		else if (this.getTransactionPremium() != null && 
				this.getTransactionPremium().doubleValue() != 0) {
			return this.getTransactionPremium();
		}
		else {
			return null;
		}
	}
	
	public Boolean getDispIsPremiumOverride() {
		if (BooleanUtils.isTrue(getDispIsReinsured())) {
			return this.isPrimaryReinsurancePremiumOverride();
		}
		else if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.isPremiumOverride();
		}
		else if (this.isPremiumOverride() != null) {
			return this.isPremiumOverride();
		}
		else {
			return null;
		}
	}
	
	public Boolean getDispIsReinsured() {
		if (this.getCoverageConfig() != null &&
				this.getCoverageConfig().isReinsurable()) {
			return this.isReinsured();
		}
		else if (this.isReinsured() != null) {
			return this.isReinsured();
		}
		else {
			return null;
		}
	}

	public Double getDispUwAdjustment() {
		if (this.getCoverageConfig() != null && 
				this.getCoverageConfig().isAllowPremiumOverride()) {
			return this.getUwAdjustment();
		}
		else if (this.getUwAdjustment() != null && 
				this.getUwAdjustment().doubleValue() != 0) {
			return this.getUwAdjustment();
		}
		else {
			return null;
		}
	}

	public boolean isCustomRow() {
		return isCustomRow;
	}

	public void setCustomRow(boolean isCustomRow) {
		this.isCustomRow = isCustomRow;
	}

	public boolean isLimitMandatory() {
		return isLimitMandatory;
	}

	public void setLimitMandatory(boolean isLimitMandatory) {
		this.isLimitMandatory = isLimitMandatory;
	}

	public boolean isDeductibleMandatory() {
		return isDeductibleMandatory;
	}

	public void setDeductibleMandatory(boolean isDeductibleMandatory) {
		this.isDeductibleMandatory = isDeductibleMandatory;
	}

	public boolean isAllowPremiumOverride() {
		return isAllowPremiumOverride;
	}

	public void setAllowPremiumOverride(boolean isAllowPremiumOverride) {
		this.isAllowPremiumOverride = isAllowPremiumOverride;
	}

	public boolean isAllowLimitOverride() {
		return isAllowLimitOverride;
	}

	public void setAllowLimitOverride(boolean isAllowLimitOverride) {
		this.isAllowLimitOverride = isAllowLimitOverride;
	}

	public boolean isAllowDeductibleOverride() {
		return isAllowDeductibleOverride;
	}

	public void setAllowDeductibleOverride(boolean isAllowDeductibleOverride) {
		this.isAllowDeductibleOverride = isAllowDeductibleOverride;
	}

	public boolean isAllowReinsurance() {
		return isAllowReinsurance;
	}

	public void setAllowReinsurance(boolean isAllowReinsurance) {
		this.isAllowReinsurance = isAllowReinsurance;
	}

	/*
	public boolean isLimitEditValid() {
		if (isAllowLimitOverride() && isLimitMandatory()) {
			if (isLimitInteger()) {
				return getEditLimit1integer() != null;
			}
			else {
				return getEditLimit1amount() != null;
			}
		}
		
		return true;
	}
	
	public boolean isDeductibleEditValid() {
		if (isAllowDeductibleOverride() && isDeductibleMandatory()) {
			if (isDeductibleInteger()) {
				return getEditDeductible1integer() != null;
			}
			else {
				return getEditDeductible1amount() != null;
			}
		}
		
		return true;
	}
	*/
	public CovLimitField getEditLimitField() {
		return editLimitField;
	}

	public void setEditLimitField(CovLimitField editLimitField) {
		this.editLimitField = editLimitField;
	}

	public CovDeductibleField getEditDeductibleField() {
		return editDeductibleField;
	}

	public void setEditDeductibleField(CovDeductibleField editDeductibleField) {
		this.editDeductibleField = editDeductibleField;
	}

	public CovPremiumField getEditPremiumField() {
		return editPremiumField;
	}

	public void setEditPremiumField(CovPremiumField editPremiumField) {
		this.editPremiumField = editPremiumField;
	}

	public CovUWAdjustmentField getEditUwAdjustmentField() {
		return editUwAdjustmentField;
	}

	public void setEditUwAdjustmentField(CovUWAdjustmentField editUwAdjustmentField) {
		this.editUwAdjustmentField = editUwAdjustmentField;
	}

	public Binder<CoverageDto> getEditBinder() {
		return editBinder;
	}

	public void setEditBinder(Binder<CoverageDto> editBinder) {
		this.editBinder = editBinder;
	}

	public CovIsPremiumOvrField getEditIsPremiumOverrideField() {
		return editIsPremiumOverrideField;
	}

	public void setEditIsPremiumOverrideField(CovIsPremiumOvrField editIsPremiumOverrideField) {
		this.editIsPremiumOverrideField = editIsPremiumOverrideField;
	}

	public CovIsReinsuredField getEditIsReinsuredField() {
		return editIsReinsuredField;
	}

	public void setEditIsReinsuredField(CovIsReinsuredField editIsReinsuredField) {
		this.editIsReinsuredField = editIsReinsuredField;
	}

	public IBusinessEntity getCoverageParent() {
		return coverageParent;
	}
	
	public boolean isNewCoverage() {
		return getAuditable() != null && getAuditable().isNew();
	}
	
	public void resetEditStatus() {
		this.editStatus = null;
	}
	
	public boolean isActiveCoverage() {
		return (getAuditable() != null && getAuditable().isActive());
	}
	
	public boolean isModifiedCoverage() {
		return (getAuditable() != null && getAuditable().isModified()) ||
 			    UIConstants.COV_EDIT_STATUSES.MODIFIED.equals(this.editStatus); 
	}
	
	public boolean isEditModified() {
		return (getAuditable() != null && getAuditable().isModified()) ||
			   UIConstants.COV_EDIT_STATUSES.MODIFIED.equals(this.editStatus);
	}
	
	public void setIsEditModified() {
		this.editStatus = UIConstants.COV_EDIT_STATUSES.MODIFIED;
	}
	
	public boolean isDeletedCoverage() {
		return (getAuditable() != null && getAuditable().isDeleted()) ||
			    UIConstants.COV_EDIT_STATUSES.DELETED.equals(this.editStatus);
	}
	
	public boolean wasDeletedCoverage() {
		return (getAuditable() != null && getAuditable().wasDeleted());
	}
	
	public boolean isEditDeleted() {
		return (getAuditable() != null && getAuditable().isDeleted()) ||
			   UIConstants.COV_EDIT_STATUSES.DELETED.equals(this.editStatus); 
	}
	
	public void setIsEditDeleted() {
		this.editStatus = UIConstants.COV_EDIT_STATUSES.DELETED;
	}
	
	public boolean isEditUndeleted() {
		return UIConstants.COV_EDIT_STATUSES.UNDELETED.equals(this.editStatus); 
	}
	
	public void setIsEditUndeleted() {
		this.editStatus = UIConstants.COV_EDIT_STATUSES.UNDELETED;
	}
	
	private IAuditable getAuditable() {
		if (this.coverage != null) {
			return this.coverage;
		}
		else if (this.endorsement != null) {
			return this.endorsement;
		}
		
		return null;
	}

	public LocCovProcessDto getLocCovProcessDto() {
		return locCovProcessDto;
	}

	public void setLocCovProcessDto(LocCovProcessDto locCovProcessDto) {
		this.locCovProcessDto = locCovProcessDto;
	}

	public Set<String> getEditMessages() {
		if (editMessages == null) {
			editMessages = new HashSet<String>();
		}
		return editMessages;
	}
	
	public void addEditMessage(String message) {
		if (message != null) {
			getEditMessages().add(message);
		}
	}
	
	public boolean isEditHasGeneralError() {
		return editHasGeneralError;
	}

	public void setEditHasGeneralError(boolean editHasGeneralError) {
		this.editHasGeneralError = editHasGeneralError;
	}

	public void resetValidation() {
		this.isEditValid  = null;
		this.editHasGeneralError = false;
		this.editMessages 		 = null;
	}

	public List<CoverageReinsuranceDto> getReinsuranceDtos() {
		return reinsuranceDtos;
	}

	public void addReinsuranceDto(CoverageReinsuranceDto dto) {
		this.reinsuranceDtos.add(dto);
	}

	public boolean anyReinsuranceChanges() {
		boolean result = false;

		if (this.reinsuranceDtos != null && !this.reinsuranceDtos.isEmpty()) {
			result = this.reinsuranceDtos.stream()
				.filter(o -> o.getAuditable() != null
					&& (o.getAuditable().isNew()
						|| o.getAuditable().isDeleted()
						|| o.getAuditable().wasDeleted()
						|| o.getAuditable().isModified()))
				.findFirst()
				.isPresent();
		}

		return result;
	}

	public boolean anyCoverageChanges() {
		return this.isNewCoverage() || this.isDeletedCoverage() || this.isModifiedCoverage()
				|| this.wasDeletedCoverage() || this.anyReinsuranceChanges();
	}
	
	public CoverageReinsuranceDto getPrimaryReinsurance() {
		CoverageReinsuranceDto result = null;
		
		if (this.reinsuranceDtos != null && !this.reinsuranceDtos.isEmpty()) {
			result = this.reinsuranceDtos.stream()
						.filter(o -> o.getAuditable() != null
							&& (o.getAuditable().isPending() || o.getAuditable().isActive())
							&& BooleanUtils.isTrue(o.getIsPrimaryInsurer())
							&& BooleanUtils.isTrue(o.getIsPremiumOverride()))
						.findFirst()
						.orElse(null);
		}
		
		return result;
	}
	
	public Boolean isPrimaryReinsurancePremiumOverride() {
		Boolean result = null;
		
		CoverageReinsuranceDto primaryReinsuranceDto = getPrimaryReinsurance();
		if (primaryReinsuranceDto != null) {
			result = primaryReinsuranceDto.getIsPremiumOverride();
		}

		return result;
	}
}
