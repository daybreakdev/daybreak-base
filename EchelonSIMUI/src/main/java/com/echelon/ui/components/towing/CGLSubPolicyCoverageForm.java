package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.components.baseclasses.policy.PremiumField;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

public class CGLSubPolicyCoverageForm extends BaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5518590057138620577L;
	private IntegerField 	numOfPowerUnitsField;
	private PremiumField	coverageUnitPremiumField;

	public CGLSubPolicyCoverageForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, UIConstants.OBJECTTYPES.CGLSubPolicy);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildFields() {
		super.buildFields();
		
		numOfPowerUnitsField = new IntegerField(labels.getString("CGLSubPolicyCoverageFormNumInUsePowerUnitsLabel"));
		numOfPowerUnitsField.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
		
		coverageUnitPremiumField = new PremiumField(labels.getString("CGLSubPolicyCoverageFormUnitPremiumLabel"));
		uiHelper.setEditFieldsReadonly(coverageUnitPremiumField);
	}

	@Override
	protected void buildPremiunmForm(CoverageDto coverageDto) {
		premiumForm.removeAll();
		
		if (coverageDto != null) {
			if (SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD.equalsIgnoreCase(coverageDto.getCoverageCode())) {
				premiumForm.setResponsiveSteps(
				        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 8)
				        );
				
				//First half
				premiumForm.add(coverageUWAfjuestmentField);
				premiumForm.add(numOfPowerUnitsField);
				attachIsPremOvrAndIsReinsuredToForm(premiumForm, 2);
				//Second half
				premiumForm.add(coverageUnitPremiumField, coverageRatedPremiumField, coverageAnnualPremiumField,
								coverageWrittenPremiumField);
				
				return;
			}
		}
		
		premiumForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 6)
		        );
		
		//First half
		premiumForm.add(coverageUWAfjuestmentField, numOfPowerUnitsField);
		attachIsPremOvrAndIsReinsuredToForm(premiumForm, 1);
		//Second half
		premiumForm.add(coverageRatedPremiumField, coverageAnnualPremiumField, coverageWrittenPremiumField);
	}

	@Override
	protected void buildBinderForCoverage(CoverageDto coverageDto) {
		super.buildBinderForCoverage(coverageDto);
		covBinder.forField(numOfPowerUnitsField).bind(CoverageDto::getCoverageNumOfUnits, CoverageDto::setCoverageNumOfUnits);
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.enableEdit(coverageDto);
		
		uiHelper.setEditFieldsReadonly(numOfPowerUnitsField);
		uiHelper.setEditFieldsReadonly(coverageUnitPremiumField);
	}

	@Override
	public void loadData(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.loadData(coverageDto);

		if (coverageDto != null) {
			if (SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD.equalsIgnoreCase(coverageDto.getCoverageCode())) {
				coverageUnitPremiumField.setDSValue(coverageUIHelper.calculateUnitPremium(coverageDto));
			}
		}
 	}
	
}
