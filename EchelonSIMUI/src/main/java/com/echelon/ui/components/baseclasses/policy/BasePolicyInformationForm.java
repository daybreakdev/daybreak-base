package com.echelon.ui.components.baseclasses.policy;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyDetailsUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.ds.ins.uicommon.converters.DSLookupItemKeyToValueConverter;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyInformationForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4268502503865342800L;
	protected DatePicker			issueDateField;
	protected TextField				productField;
	protected DatePicker			termEffectiveDateField;
	protected DatePicker			termExpiryDateField;
	protected DSComboBox<Integer>	termLengthField;
	protected DatePicker			inceptionDateField;
	protected PremiumField			termPremiumField;
	protected PremiumField			txnPremiumField;
	protected DatePicker			quotePreparedDateField;
	protected TextField				quotePreparedByField;
	protected ActionToolbar	toolbar;
	
	protected Binder<PolicyTransaction> binder;
	protected Binder<InsurancePolicy>   ipBinder;

	public BasePolicyInformationForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
		buildFields();
		buildForm();
	}
	
	protected BasePolicyDetailsUIHelper createPolicyDetailsUIHelper() {
		return UIFactory.getInstance().getPolicyDetailsUIHelper();
	}
	
	protected void initializeLayouts() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			// Note Update button is not visible; just for setting enabled
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													labels.getString("PolicyInfoFormSectionLabel"),
													UIConstants.UICOMPONENTTYPES.Form);
			toolbar = (ActionToolbar) headComponents.getToolbar();
			
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
					createPolicyDetailsUIHelper().setupPolicyUpdateAction();
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
		
	}

	protected void buildFields() {
		issueDateField 			= new DatePicker(labels.getString("PolicyInfoFormIssueDateLabel"));
		productField 			= new TextField(labels.getString("PolicyInfoFormProductLabel"));
		termEffectiveDateField 	= new DatePicker(labels.getString("PolicyInfoFormTermEffectiveDateLabel"));
		termExpiryDateField 	= new DatePicker(labels.getString("PolicyInfoFormTermExpiryLabel"));
		termLengthField 		= new DSComboBox<Integer>(labels.getString("PolicyInfoFormTermLengthLabel"), Integer.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_TermLengths)
										.withSortList(false);
		inceptionDateField 		= new DatePicker(labels.getString("PolicyInfoFormInceptionDateLabel"));
		termPremiumField 		= new PremiumField(labels.getString("PolicyInfoFormTermPremiumLabel"));
		txnPremiumField 		= new PremiumField(labels.getString("PolicyInfoFormTxnPremiumLabel"));

		uiHelper.fieldTextAlightLeft(termLengthField);
		
		binder = new Binder<PolicyTransaction>(PolicyTransaction.class);
		binder.forField(issueDateField).bind("policyVersion.versionIssueDate");
		binder.forField(productField)
						.withConverter(new DSLookupItemKeyToValueConverter<String>(String.class)
										.setConfigLookupTableId(ConfigConstants.LOOKUPTABLE_ProductCd))
						.bind("policyVersion.insurancePolicy.productCd");
		binder.forField(termEffectiveDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("policyVersion.versionTerm.termEffDate");
		binder.forField(termExpiryDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("policyVersion.versionTerm.termExpDate");
		binder.forField(termLengthField).bind("policyVersion.versionTerm.termMonths");
		binder.forField(inceptionDateField).bind("policyVersion.insurancePolicy.originalInceptionDate");
		binder.forField(termPremiumField).withNullRepresentation("")
										 .withConverter(termPremiumField.getConverter())
										 .bind("policyVersion.versionTerm.termPremium.termPremium");
		binder.forField(txnPremiumField).withNullRepresentation("")
										.withConverter(txnPremiumField.getConverter())
										.bind("policyVersion.versionTerm.termPremium.transactionPremium");
		binder.setReadOnly(true);
		
		ipBinder = new Binder<InsurancePolicy>(InsurancePolicy.class);
		quotePreparedByField	= new TextField(labels.getString("PolicyInfoFormQuotePreparedBy"));
		quotePreparedDateField	= new DatePicker(labels.getString("PolicyInfoFormQuotePreparedDate"));
		
		ipBinder.forField(quotePreparedByField).bind("quotePreparedBy");
		ipBinder.forField(quotePreparedDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("quotePreparedDate");
		
		ipBinder.setReadOnly(true);
		
	}
	
	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );
		
		buildFormLine1(form);
		buildFormLine2(form);
		buildFormLine3(form);
		buildFormLine4(form);
		
		this.add(form);
	}
	
	protected void buildFormLine1(FormLayout form) {
		form.add(productField, issueDateField);
		uiHelper.addFormEmptyColumn(form);
	}
	
	protected void buildFormLine2(FormLayout form) {
		form.add(termEffectiveDateField, termExpiryDateField, termLengthField);
	}
	
	protected void buildFormLine3(FormLayout form) {
		form.add(inceptionDateField, termPremiumField, txnPremiumField);
	}
	
	protected void buildFormLine4(FormLayout form) {
		form.add(quotePreparedByField);
		uiHelper.addFormEmptyColumn(form, 2);
	}
	
	public void loadData(PolicyTransaction policyTransaction) {
		loadDataConfigs(binder, ipBinder);
		binder.readBean(policyTransaction);
		
		if (policyTransaction != null) {
			ipBinder.readBean(policyTransaction.getPolicyVersion().getInsurancePolicy());
			refreshToolbar(policyTransaction.getPolicyVersion().getInsurancePolicy());
		}
		else {
			refreshToolbar(null);
		}
		
		enableDataView(binder, ipBinder);
	}			
	
	protected void refreshToolbar(InsurancePolicy insurancePolicy) {
		if (toolbar != null) {
			toolbar.refreshButtons(null, insurancePolicy);
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
	}
}
