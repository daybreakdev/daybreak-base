package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.AdjustmentUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="VehicleContentView", layout = BasePolicyView.class)
public class VehicleContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132388023659436469L;
	private VehicleUIHelper			vehicleUIHelper;
	private VehicleForm				vehicleForm;
	private VehicleCoverageList		covList;
	private	VehicleAdjustmentList	adjustmentList;

	public VehicleContentView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public VehicleContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}
	
	protected VehicleUIHelper getVehicleUIHelper() {
		if (vehicleUIHelper == null) {
			vehicleUIHelper = new VehicleUIHelper();
		}
		
		return vehicleUIHelper;
	}

	/* Title is not necessary
	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this,
												uiHelper.getSessionLabels().getString("VehicleDetailsTitle"),
												null);
		}
		
		super.buildLayout();
	}
	*/

	@Override
	protected String[] getTabLabels() {
		// TODO Auto-generated method stub
		return new String[] {
		                  labels.getString("VehicleTabLabel"),
		                  labels.getString("CoverageTabLabel"),
		                  labels.getString("AdjustmentTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {getVehicleForm(), getVehicleCoverageList(), getAdjustmentList()};
	}
	
	protected Component getVehicleForm() {
		if (vehicleForm == null) {
			vehicleForm = getVehicleUIHelper().createVehicleForm(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return vehicleForm;
	}
	
	protected Component getVehicleCoverageList() {
		if (covList == null) {
			covList = (VehicleCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.Vehicle, uiMode, UICONTAINERTYPES.Tab);
		}
		
		return covList;
	}

	protected Component getAdjustmentList() {
		if (adjustmentList == null) {
			adjustmentList = new VehicleAdjustmentList(uiMode, UICONTAINERTYPES.Tab);
		}

		return adjustmentList;
	}

	protected void loadData(SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		if (vehicleForm != null) {
			getVehicleUIHelper().loadView(vehicleForm, autoSubPolicy, vehicle);
		}
		
		if (covList != null) {
			((VehicleCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Vehicle)).loadView(covList, vehicle);
		}

		if (adjustmentList != null) {
			new AdjustmentUIHelper().loadView(adjustmentList, vehicle);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) nodeItem.getNodeParentObject();
		SpecialtyVehicleRisk vehicleRisk = (SpecialtyVehicleRisk)nodeItem.getNodeObject();
		
		getVehicleUIHelper().loadVehicleData(new VehicleUIHelper.LoadVehicleDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData(autoSubPolicy, vehicleRisk);
				refreshPage();
			}
		}, autoSubPolicy, vehicleRisk);
	}
}
