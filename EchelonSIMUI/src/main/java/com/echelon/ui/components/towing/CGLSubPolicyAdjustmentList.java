package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.towing.dto.AdjustmentDto;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class CGLSubPolicyAdjustmentList extends AdjustmentList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5082817176560719506L;

	public CGLSubPolicyAdjustmentList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, OBJECTTYPES.CGLSubPolicy);
		// TODO Auto-generated constructor stub
	}
	
	public boolean loadData(CGLSubPolicy<?> parent, List<SpecialtyVehicleRiskAdjustment> adjustments) {
		// TODO Auto-generated method stub
		return super.loadData(adjustments);
	}

	@Override
	protected void refreshToolbar() {
		// TODO Auto-generated method stub
		if (toolbar != null) {
			toolbar.disableAllButtons();
			toolbar.setVisible(false);
		}
	}

	@Override
	protected void loadCoverageList(AdjustmentCoverageList adjCoverageList, AdjustmentDto dto) {
		// TODO Auto-generated method stub
		adjCoverageUIHelper.loadView(adjCoverageList, dto.getAdjustment().getCglSubPolicy(), dto.getAdjustment());
	}
}
