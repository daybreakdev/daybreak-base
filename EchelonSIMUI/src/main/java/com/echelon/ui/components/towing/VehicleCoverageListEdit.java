package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageListEdit;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class VehicleCoverageListEdit extends BaseCoverageListEdit {

	/**
	 * 
	 */
	private static final long serialVersionUID = -824483193753962035L;
	
	public VehicleCoverageListEdit(String uiMode, UICONTAINERTYPES uiContainerType, OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildGridDataColumns() {
		// TODO Auto-generated method stub
		super.buildGridDataColumns();
		
		coverageGrid.getColumnByKey(COLUMNKEY_UNITPREMIUM).setVisible(true);
	}
}
