package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.policy.PolicyNotes;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.utils.Constants;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyNoteForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyNoteProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;

public class BasePolicyNoteUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4289570778499425943L;
	protected boolean isInternal;

	public BasePolicyNoteUIHelper(boolean isInternal) {
		super();
		
		this.isInternal = isInternal;
	}

	protected BasePolicyNoteForm createPolicyNoteForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new BasePolicyNoteForm(action, containerType);
	}
	
	protected DSUpdateDialog<BasePolicyNoteForm> createPolicyNoteDialog(String title, String action) {
		DSUpdateDialog<BasePolicyNoteForm> dialog = (DSUpdateDialog<BasePolicyNoteForm>) createDialog(title, createPolicyNoteForm(action, UICONTAINERTYPES.Dialog), action);
		dialog.removeButtonSaveShortcut();
		return dialog;
	}
	
	protected void setupVehicleAction(String title, String action, PolicyNotes policyNotes) {
		DSUpdateDialog<BasePolicyNoteForm> dialog = createPolicyNoteDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(policyNotes));
		dialog.getContentComponent().loadData(policyNotes);
		if (!UIConstants.UIACTION_View.equals(action)) {
			dialog.getContentComponent().enableEdit(policyNotes);
		}
		else {
			dialog.getContentComponent().setViewMode();
		}
		dialog.open();
	}
	
	protected PolicyNotes createEmptyPolicyNotes() {
		PolicyNotes policyNotes = new PolicyNotes();
		policyNotes.setIsInternal(isInternal);
		return policyNotes;
	}
	
	public void newVehicleAction() {
		setupVehicleAction(uiHelper.getSessionLabels().getString("PolicyNoteAddActionLabel"), 
							UIConstants.UIACTION_New, 
							createEmptyPolicyNotes());
	}

	public void deleteAction(PolicyNotes policyNotes) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("PolicyNoteDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(policyNotes));
		dialog.open();
	}
			
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<BasePolicyNoteForm> dialog = event.getSource();
		PolicyNotes policyNotes = (PolicyNotes) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(policyNotes)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
	
			createPolicyNoteTaskProcessor(dialog).start(
						createPolicyNoteProcessor(UIConstants.UIACTION_SaveNew, policyNotes)
					);
		}
	}
	
	public void updateVehicleAction(PolicyNotes policyNotes) {
		setupVehicleAction(uiHelper.getSessionLabels().getString("PolicyNoteUpdateActionLabel"), 
							UIConstants.UIACTION_Update, 
							policyNotes);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		// TODO Auto-generated method stub
		super.onSaveUpdate(event);
		DSUpdateDialog<BasePolicyNoteForm> dialog = event.getSource();
		PolicyNotes policyNotes = (PolicyNotes) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(policyNotes)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
	
			createPolicyNoteTaskProcessor(dialog).start(
						createPolicyNoteProcessor(UIConstants.UIACTION_Save, policyNotes)
					);
		}
	}
	
	public void viewNoteAction(PolicyNotes policyNotes) {
		setupVehicleAction(uiHelper.getSessionLabels().getString("PolicyNoteViewActionLabel"), 
							UIConstants.UIACTION_View, 
							policyNotes);
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog  = event.getSource();
		PolicyNotes policyNotes = (PolicyNotes) dialog.getDataStore().getData1();
		
		createPolicyNoteTaskProcessor(dialog).start(
				createPolicyNoteProcessor(UIConstants.UIACTION_Delete, policyNotes)
			);
	}

	protected BasePolicyNoteProcessor createPolicyNoteProcessor(String action, PolicyNotes policyNotes) {
		return new BasePolicyNoteProcessor(action, policyUIHelper.getCurrentPolicyTransaction(), 
							policyNotes);
	}
	
	protected TaskProcessor createPolicyNoteTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyNoteProcessor processor = (BasePolicyNoteProcessor) taskRun.getProcessor();
					dialog.close();
					policyUIHelper.reopenPolicy(processor.getPolicyTransaction(), UIConstants.POLICYTREENODES.Notes, null);
				}
				
				return true;
			}
				
		};
	}
	
	// ESL-1947 - Changes (replace all) for Notes-to-broker - PolicyNotes.isInternal = false:
	// allow the user to override/update the broker notes in SIM. Once the policy is issued, this can be disabled.
	// Prev requirement for all type of notes:
	// note should be locked for edit 24 hrs after it was created (even by the creator).
	// note cannot be editable by anyone else other than the creator.
	// and note must added for the current version
	public boolean isNoteEditable(PolicyNotes note) {
		boolean ok = false;
		
		if (BrokerUI.getUserSession().getUserProfile() != null &&
			BrokerUI.getUserSession().getUserProfile().isHasFullAccess() &&
			note != null && note.isNew()) {
			
			// Personal notes
			if (BooleanUtils.isTrue(note.getIsInternal())) {
				ok = uiHelper.isBelongToUser(note.getOriginatingAuthor()) &&
				 	 !uiHelper.isAfter24Hours(note.getOriginatingDate());
			}
			else
			// Broker notes
			{
				ok = policyUIHelper.getCurrentPolicyVersion() != null &&
					policyUIHelper.getCurrentPolicyVersion().getVersionIssueDate() == null;
			}
		}
		else {
			
		}
		
		return ok;
	}
	
	public List<PolicyNotes> buildNoteList(PolicyVersion policyVersion) {
		List<PolicyNotes> list = new ArrayList<PolicyNotes>();
		
		if (policyVersion != null && policyVersion.getPolicyNotes() != null) {
			
			List<PolicyNotes> ls = new ArrayList<PolicyNotes>();
			policyVersion.getPolicyNotes()
				.parallelStream().filter(o -> 
							(isInternal && (o.getIsInternal() == null || o.getIsInternal())) ||
							(!isInternal && o.getIsInternal() != null && !o.getIsInternal())
							)
				.collect(Collectors.toList())
				.stream().forEach(o -> ls.add(o));
			
			list.addAll(ls);
		}
		
		list = list.stream().sorted(Comparator.comparing(PolicyNotes::getOriginatingDate).reversed())
				.collect(Collectors.toList());
		
		if (isInternal && 
			(Constants.VERSION_STATUS_DECLINED.equalsIgnoreCase(policyVersion.getBusinessStatus()) ||
			 Constants.VERSION_STATUS_NOTREQUIRED.equalsIgnoreCase(policyVersion.getBusinessStatus()))) {
			PolicyNotes note = new PolicyNotes();
			note.setIsInternal(true);
			note.setDescription(uiHelper.getSessionLabels().getString("PolicyNoteSubject"+policyVersion.getBusinessStatus()));
			
			if (policyVersion.getChangeReasonCd() != null) {
				LookupTableItem lookupItem = lookupUIHelper.findLookupItemByKey(policyVersion.getChangeReasonCd(), lookupUIHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_DeclineReasons), true);
				if (lookupItem != null) {
					note.setNoteDetails(lookupItem.getTranslation(BrokerUI.getUserSession().getUserProfile().getDefaultLanguage()));
				}
			}
			
			list.add(0, note);
		}
		
		return list;
	}
	
	public boolean canCreateNewNote() {
		return BrokerUI.getUserSession().getUserProfile() != null &&
				BrokerUI.getUserSession().getUserProfile().isHasFullAccess() &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getBusinessStatus() != null;
	}	
}
