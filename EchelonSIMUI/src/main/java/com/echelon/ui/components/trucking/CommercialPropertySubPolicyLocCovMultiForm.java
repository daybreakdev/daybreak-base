package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.towing.SubPolicyLocCovMultiForm;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class CommercialPropertySubPolicyLocCovMultiForm
	extends SubPolicyLocCovMultiForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5150041540272854566L;

	public CommercialPropertySubPolicyLocCovMultiForm(String uiMode,
		UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, OBJECTTYPES.CPSubPolicy, false);
	}
	
	@Override
	protected IBaseCovAndEndorseForm createCoverageForm() {
		return new CommercialPropertySubPolicyCoverageForm(uiMode, uiContainerType, OBJECTTYPES.CPSubPolicy);
	}

}
