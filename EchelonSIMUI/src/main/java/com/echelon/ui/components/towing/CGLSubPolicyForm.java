package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.components.DSComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class CGLSubPolicyForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1370690893968005421L;
	private Binder<CGLSubPolicy<?>> binder;
	private	DSComboBox<Integer> 	cglLimitField;
	private DSComboBox<Integer> 	cglDeductibleField;

	public CGLSubPolicyForm(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildFields();
		buildForm();
	}

	public void loadData(CGLSubPolicy<?> cglSubPolicy) {
		loadDataConfigs(binder);
		
		binder.readBean(cglSubPolicy);
		
		enableDataView(binder);
	}
	
	public void loadValues(Integer cglLimit) {
		loadDataConfigs(binder);
		
		uiHelper.setFieldValue(cglLimitField, cglLimit);
		
		enableDataView(binder);
	}

	private void buildFields() {
		labels = uiHelper.getSessionLabels();
		cglLimitField = new DSComboBox<Integer>(labels.getString("CGLSubPolicyFormLimitLabel"), Integer.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_LiabLimitList);
		cglDeductibleField = new DSComboBox<Integer>(labels.getString("CGLSubPolicyFormDeductibleLabel"), Integer.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_DeductLimitList, false);

		binder = new Binder<CGLSubPolicy<?>>();
		binder.forField(cglLimitField)
			.bind(CGLSubPolicy::getCglLimit, null);
		binder.forField(cglDeductibleField).asRequired()
			.bind(CGLSubPolicy::getCglDeductible, CGLSubPolicy::setCglDeductible);
	}

	private void buildForm() {
		FormLayout form = new FormLayout();
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		form.add(cglLimitField);
		form.add(cglDeductibleField);
		this.add(form);
	}

	public boolean validateEdit() {
		return uiHelper.validateValues(binder);
	}

	public boolean saveEdit(CGLSubPolicy<?> subPolicy) {
		return uiHelper.writeValues(binder, subPolicy);
	}

	public void enableEdit() {
		enableDataEdit(binder);

		uiLookupHelper.formatLookupIntegerListEdit(cglLimitField.getDSLookupItems());
		cglLimitField.getDataProvider().refreshAll();
		cglLimitField.setDSValue(cglLimitField.getValue());
		uiLookupHelper.formatLookupIntegerListEdit(cglDeductibleField.getDSLookupItems());
		cglDeductibleField.getDataProvider().refreshAll();
		cglDeductibleField.setDSValue(cglDeductibleField.getValue());
		
		uiHelper.setEditFieldsReadonly(cglLimitField);

		setFocus();
	}

	public void setFocus() {
		cglDeductibleField.focus();
	}
}
