package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;
import java.time.LocalDate;

public class PolicyConfirmDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6283286564386075520L;
	private String				policyAction;
	private String				changeMethod;
	private	String				changeReasonCd;
	private String				changeReasonDetails;
	private LocalDate			changeEffectiveDate;
	private String				changeDescription;
	private Boolean				isRenewalResetUWA;
	private boolean				lastActionFailed;
	private Integer				changeDays;
	private Integer				renewalTermLength;

	public PolicyConfirmDto(String policyAction) {
		super();
		this.policyAction = policyAction;
	}

	public String getPolicyAction() {
		return policyAction;
	}

	public void setPolicyAction(String policyAction) {
		this.policyAction = policyAction;
	}

	public String getChangeMethod() {
		return changeMethod;
	}

	public void setChangeMethod(String changeMethod) {
		this.changeMethod = changeMethod;
	}

	public String getChangeReasonCd() {
		return changeReasonCd;
	}

	public void setChangeReasonCd(String changeReasonCd) {
		this.changeReasonCd = changeReasonCd;
	}

	public String getChangeReasonDetails() {
		return changeReasonDetails;
	}

	public void setChangeReasonDetails(String changeReasonDetails) {
		this.changeReasonDetails = changeReasonDetails;
	}

	public LocalDate getChangeEffectiveDate() {
		return changeEffectiveDate;
	}

	public void setChangeEffectiveDate(LocalDate changeEffectiveDate) {
		this.changeEffectiveDate = changeEffectiveDate;
	}

	public String getChangeDescription() {
		return changeDescription;
	}

	public void setChangeDescription(String changeDescription) {
		this.changeDescription = changeDescription;
	}

	public Boolean getIsRenewalResetUWA() {
		return isRenewalResetUWA;
	}

	public void setIsRenewalResetUWA(Boolean isRenewalResetUWA) {
		this.isRenewalResetUWA = isRenewalResetUWA;
	}

	public boolean isLastActionFailed() {
		return lastActionFailed;
	}

	public void setLastActionFailed(boolean lastActionFailed) {
		this.lastActionFailed = lastActionFailed;
	}
	
	/**
	 * Gets the renewal term's length.
	 * @return the number of months the policy is to be renewed for.
	 */
	public Integer getRenewalTermLength() {
		return renewalTermLength;
	}
	
	/**
	 * Sets the renewal term's length.
	 * @param renewalTermLength the number of months the policy is to be renewed for.
	 */
	public void setRenewalTermLength(Integer renewalTermLength) {
		this.renewalTermLength = renewalTermLength;
	}

	public Integer getChangeDays() {
		return changeDays;
	}

	public void setChangeDays(Integer changeDays) {
		this.changeDays = changeDays;
	}
	
}
