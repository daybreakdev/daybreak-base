package com.echelon.ui.components.towing;

import java.time.LocalDate;
import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.utils.Constants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.functions.policy.specialtylines.SpecialtyAutoPolicyFunctions;
import com.echelon.ui.components.baseclasses.policy.BasePolicyCancelConfirmForm;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;

public class TowingPolicyCancelConfirmForm extends BasePolicyCancelConfirmForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 726299673036933010L;

	public TowingPolicyCancelConfirmForm(String uiMode, UICONTAINERTYPES uiContainerType,
											String policyAction, String policyVerTxnType, String policyVerStatus) {
		super(uiMode, uiContainerType, policyAction, policyVerTxnType, policyVerStatus);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validateEdit() {
		if (StringUtils.isBlank(cancelReasonField.getValue())) {
			cancelReasonField.setErrorMessage(uiHelper.getSessionLabels().getString("PolicyConfirmCancelFormReasonMandatoryMessage"));
			cancelReasonField.setInvalid(true);
			return false;
		}
		
		return super.validateEdit();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void onConfirmEffectiveDateChanged(ValueChangeEvent event) {
		super.onConfirmEffectiveDateChanged(event);
		
		LocalDate cancelDate = confirmEffectiveDate.getValue();
		if (cancelDate != null &&
			BrokerUI.getUserSession() != null &&
			BrokerUI.getUserSession().getCurrentPolicy() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getVersionTerm() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getVersionTerm().getTermEffDate() != null &&
			cancelDate.isAfter(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate())) {
			SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction()
															.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
			if (autoSubPolicy != null) {
				if (!isProRataCancellation(autoSubPolicy.getFleetBasis())) {
					cancelTypeField.setDSItems(new ArrayList<LookupTableItem>());
					if (updDialog != null && updDialog.getButton(UIConstants.UIACTION_Confirm) != null) {
						updDialog.getButton(UIConstants.UIACTION_Confirm).setEnabled(false);
					}
					uiHelper.notificationMessage(uiHelper.getSessionLabels().getString("PolicyConfirmCancelFormNonSchdMessage"));
					return;
				}
			}
		}
		
		if (updDialog != null && updDialog.getButton(UIConstants.UIACTION_Confirm) != null) {
			updDialog.getButton(UIConstants.UIACTION_Confirm).setEnabled(true);
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void filterCancelTypes(PolicyTransaction policyTransaction) {
		LocalDate cancelDate = confirmEffectiveDate.getValue();
		
		if (cancelDate != null) {
			//If the cancellation is on inception date (version date of the latest active transaction), 
			//the only option available for Cancellation Type is “Flat”
			if (policyTransaction.getPolicyVersion().getVersionTerm() != null &&
				policyTransaction.getPolicyVersion().getVersionTerm().getTermEffDate() != null &&
				policyTransaction.getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate().isEqual(cancelDate)) {
				cancelTypeField.filterDSItems(Constants.CANCELLATION_TYPE_FLAT);
				return;
			}
			
			//If the cancellation is after the inception date (version date of the latest active transaction), 
			//For Scheduled policy(specialityautosubpolicy.FleetBasis = SCHD), the options available are Prorata & Short rate
			//For non-scheduled policies (specialityautosubpolicy.FleetBasis != SCHD), display a message “Non Scheduled policies cannot be cancelled”.
			if (policyTransaction.getPolicyVersion().getVersionTerm() != null &&
				policyTransaction.getPolicyVersion().getVersionTerm().getTermEffDate() != null &&
				cancelDate.isAfter(policyTransaction.getPolicyVersion().getVersionTerm().getTermEffDate().toLocalDate())) {
				SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
				if (autoSubPolicy != null) {
					if (isProRataCancellation(autoSubPolicy.getFleetBasis())) {
						cancelTypeField.filterDSItems(Constants.CANCELLATION_TYPE_PRORATA, Constants.CANCELLATION_TYPE_SHORTRATE);
						return;
					}
				}
			}
		}
		else {
			cancelTypeField.setDSItems(new ArrayList<LookupTableItem>());
		}
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCancelTypeChanged(ValueChangeEvent event) {
		selectionWarning();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void onCancelReasonChanged(ValueChangeEvent event) {
		cancelReasonField.setErrorMessage("");
		cancelReasonField.setInvalid(false);
		selectionWarning();
	}

	protected void selectionWarning() {
		if (Constants.CANCELLATION_TYPE_PRORATA.equalsIgnoreCase(cancelTypeField.getValue()) &&
			UIConstants.CANCELLATION_REASON_INSUREDREQUEST.equalsIgnoreCase(cancelReasonField.getValue())) {
			uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("PolicyConfirmDialogInsuredAndProrataSelectionWarning"));
		}
	}
	
	protected boolean isProRataCancellation(String fleetBasis) {
		return SpecialtyAutoPolicyFunctions.getInstance().isProRataCancellation(fleetBasis);
	}
}
