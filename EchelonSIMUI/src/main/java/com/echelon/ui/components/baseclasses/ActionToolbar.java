package com.echelon.ui.components.baseclasses;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.uicommon.components.DSBaseActionToolbar;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.dto.DSActionDto;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;

public class ActionToolbar extends DSBaseActionToolbar {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4094656588929557663L;
	protected PolicyUIHelper policyUIHelper;
	
	public ActionToolbar(UICOMPONENTTYPES uiComponentType, boolean isButtonTextAndIcon, DSActionDto... actionAndIcons) {
		super(uiComponentType, isButtonTextAndIcon, actionAndIcons);
		// TODO Auto-generated constructor stub
	}

	public ActionToolbar(UICOMPONENTTYPES uiComponentType, String... actions) {
		super(uiComponentType, actions);
		// TODO Auto-generated constructor stub
	}

	public ActionToolbar(UICOMPONENTTYPES uiComponentType) {
		super(uiComponentType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initBaseActionToolbar(UICOMPONENTTYPES uiComponentType, String... actions) {
		// TODO Auto-generated method stub
		policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		
		super.initBaseActionToolbar(uiComponentType, actions);
	}

	@Override
	protected boolean isParentRecordDeleted(IBusinessEntity parentEntity) {
		return (policyUIHelper != null && policyUIHelper.getAuditable(parentEntity) != null &&
				(policyUIHelper.getAuditable(parentEntity).isDeleted() ||
						 policyUIHelper.getAuditable(parentEntity).wasDeleted()));
	}
	
	@Override
	protected boolean isRecordDeleted(IBusinessEntity busEntity) {
		return (policyUIHelper != null && policyUIHelper.getAuditable(busEntity) != null &&
				policyUIHelper.getAuditable(busEntity).isDeleted());
	}
	
	@Override
	protected boolean wasRecordDeleted(IBusinessEntity busEntity) {
		return (policyUIHelper != null && policyUIHelper.getAuditable(busEntity) != null &&
				policyUIHelper.getAuditable(busEntity).wasDeleted());
	}
	
	@Override
	protected boolean isCurrentTransactionEditable() {
		return BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable();
	}
}
