package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;

public class TotalPremiumDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6603461699616252251L;
	private String  key;
	private Integer	seq;
	private Double 	totalPremium;
	private String 	totalLabel;

	public TotalPremiumDto(String key, Integer seq, String totalLabel, Double totalPremium) {
		super();
		this.key  = key;
		this.seq  = seq;
		this.totalPremium = totalPremium;
		this.totalLabel   = totalLabel;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Integer getSeq() {
		return seq;
	}

	public void setSeq(Integer seq) {
		this.seq = seq;
	}

	public Double getTotalPremium() {
		return totalPremium;
	}

	public void setTotalPremium(Double totalPremium) {
		this.totalPremium = totalPremium;
	}

	public String getTotalLabel() {
		return totalLabel;
	}

	public void setTotalLabel(String totalLabel) {
		this.totalLabel = totalLabel;
	}
}
