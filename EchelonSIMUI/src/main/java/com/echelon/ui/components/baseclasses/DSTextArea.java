package com.echelon.ui.components.baseclasses;

import java.text.MessageFormat;

import com.ds.ins.uicommon.helpers.DSUIHelper;
import com.ds.ins.uicommon.session.DSSessionUI;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.data.value.ValueChangeMode;

public class DSTextArea extends TextArea {
	private Integer	lastFieldLength;
	private Integer fieldLength;
	private String  warningMaxLengthMessage;

	public DSTextArea() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DSTextArea(String label, String initialValue, String placeholder) {
		super(label, initialValue, placeholder);
		// TODO Auto-generated constructor stub
	}

	public DSTextArea(String label, String placeholder) {
		super(label, placeholder);
		// TODO Auto-generated constructor stub
	}

	public DSTextArea(String label) {
		super(label);
		// TODO Auto-generated constructor stub
	}
	
	protected String getFieldReachMaxLengthWMessage() {
		DSUIHelper uiHelper = DSSessionUI.getHelperSession().getUiHelper();
		String message = MessageFormat.format(uiHelper.getCommonLabels().getString("WarningTextFieldMaxLengthMessage"), this.getLabel(), fieldLength+"");
		return message;
	}

	public DSTextArea(String label, Integer fieldLength) {
		super(label);
		this.fieldLength = fieldLength;
		
		if (this.fieldLength != null && this.fieldLength > 0) {
			DSUIHelper uiHelper = DSSessionUI.getHelperSession().getUiHelper();
			warningMaxLengthMessage = getFieldReachMaxLengthWMessage();
			
			this.setMaxLength(fieldLength+1);
			
			this.setValueChangeMode(ValueChangeMode.EAGER);
			this.addKeyDownListener(e -> {
	 			if (doesFieldReachMaxLength()) {
					uiHelper.notificationWarning(warningMaxLengthMessage);
					if (this.getValue().length() > this.fieldLength) {
						this.setValue(this.getValue().substring(0, this.fieldLength));
					}
				}
	 			
	 			updateLastFieldLength();
			});
		}
	}
	
	private boolean doesFieldReachMaxLength() {
		boolean result = false;
		
		//Length changed
		if (lastFieldLength == null || lastFieldLength != getFieldLength()) {
			if (this.getValue() != null && this.getValue().length() > fieldLength) {
				result = true;
			}
		}
		
		return result;
	}
	
	private int getFieldLength() {
		return this.getValue() != null ? this.getValue().length() : 0;
	}
	
	private void updateLastFieldLength() {
		lastFieldLength = getFieldLength();
	}

	@Override
	public void setValue(String value) {
		// TODO Auto-generated method stub
		super.setValue(value);
	
		updateLastFieldLength();
	}
}
