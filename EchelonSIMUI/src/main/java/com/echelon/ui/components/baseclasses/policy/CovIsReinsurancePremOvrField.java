package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageReinsuranceDto;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.data.binder.Binder;

public class CovIsReinsurancePremOvrField extends Checkbox {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1138370890084053920L;
	protected UIHelper							uiHelper;
	protected Binder<CoverageReinsuranceDto> 	binder;
	protected CoverageReinsuranceDto			covReinsuranceDto;

	public CovIsReinsurancePremOvrField(String label) {
		super(label);
		
		uiHelper = BrokerUI.getHelperSession().getUiHelper();
	}
	
	public void initializeField(CoverageReinsuranceDto dto) {
		this.covReinsuranceDto = dto;
	}
	
	public void bindField(Binder<CoverageReinsuranceDto> binder) {
		this.binder = binder;
		binder.removeBinding(this);
		if ((covReinsuranceDto.getCoverageDto().isAllowPremiumOverride() && !covReinsuranceDto.getCoverageDto().isDeletedCoverage() && !covReinsuranceDto.getCoverageDto().wasDeletedCoverage())) {
			binder.forField(this)
								.bind(CoverageReinsuranceDto::getIsPremiumOverride, 
									  CoverageReinsuranceDto::setIsPremiumOverride);
		}
		else {
			binder.forField(this)
								.bind(CoverageReinsuranceDto::getIsPremiumOverride, null);
		}
	}
	
	public void determineReadOnly() {
		if (covReinsuranceDto.getCoverageDto().isDeletedCoverage() || covReinsuranceDto.getCoverageDto().wasDeletedCoverage()) {
			uiHelper.setEditFieldsReadonly(this);
		}
		else if (covReinsuranceDto.getCoverageDto().isAllowPremiumOverride()) {
			uiHelper.setEditFieldsNotReadonly(this);
		}
		else {
			if (this.getValue() != null && this.getValue()) {
				uiHelper.setFieldValue(this, Boolean.FALSE);
			}
			uiHelper.setEditFieldsReadonly(this);
		}
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && covReinsuranceDto.getCoverageDto().isAllowPremiumOverride() && !covReinsuranceDto.getCoverageDto().isDeletedCoverage() && !covReinsuranceDto.getCoverageDto().wasDeletedCoverage()) {
			this.setTabIndex(value);
			return true;
		}
		else {
			this.setTabIndex(-1);
			return false;
		}
	}
}