package com.echelon.ui.components;

import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.converters.DSNumericFormat;

public class SIMMoneyField extends DSNumberField<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6494782366615076586L;

	public SIMMoneyField(String label) {
		super(label, Double.class);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setDoubleFormat() {
		// TODO Auto-generated method stub
		super.setDoubleFormat();
		
		modelPattern = "\\d{0,9}(\\.\\d{0,2}){0,1}";
		modelFormatter = DSNumericFormat.getAmountInstance();
	}

}
