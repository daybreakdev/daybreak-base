package com.echelon.ui.components.entities;

import java.util.Optional;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.ui.components.AddressForm;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.HasCustomer;
import com.ds.ins.domain.entities.Producer;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.exceptions.BrokerException;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.binder.Binder;

public abstract class CustomerForm<T extends HasCustomer> extends BaseLayout implements IDSFocusAble {
	private static final long serialVersionUID = 6693994000706873797L;

	protected CustomerUIHelper<T> customerUIHelper;

	protected Binder<T> customerBinder = new Binder<>();
	protected Binder<SpecialtyAutoPackage> specialtyAutoPackageBinder = new Binder<>();
	protected Binder<InsurancePolicy> policyBinder = new Binder<>();
	protected Binder<Producer> producerBinder = new Binder<>();
	protected Binder<PolicyVersion> policyVersionBinder = new Binder<>();
	protected Binder<PolicyTerm> policyTermBinder = new Binder<>();

	// Policy holder section
	protected Label policyHolderSectionLabel;
	protected Checkbox doNotRenew;
	protected TextField doNotRenewReason;

	// Customer address section
	protected Label addressSectionLabel;
	protected AddressForm address;
	protected TextField statCode;
	protected TextField territory;
	protected TextField ruralityInd;

	// Broker section
	protected Label brokerSectionLabel;
	protected ComboBox<Producer> brokerName;
	protected TextField brokerContact;
	protected TextField brokerId;
	protected DSNumberField<Double> brokerCommission;
	protected AddressForm brokerAddress;

	protected boolean isSpecialtyAutoPackage;

	protected CustomerForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		customerUIHelper = createCustomerUIHelper();
		buildFields();
		bindFields();
		buildLayout();
	}

	public void loadData(Customer customer, Address address, PolicyTransaction<?> policyTransaction,
			String selectedProduct) {
		loadDataConfigs(selectedProduct);
		final PolicyVersion policyVersion = policyTransaction.getPolicyVersion();
		final InsurancePolicy insurancePolicy = policyVersion.getInsurancePolicy();

		customerBinder.readBean(getCustomer(customer));
		customerBinder.setReadOnly(true);

		this.address.loadData(address);

		policyBinder.readBean(insurancePolicy);
		policyBinder.setReadOnly(true);

		refreshBroker(insurancePolicy.getPolicyProducer());
		producerBinder.setReadOnly(true);

		policyVersionBinder.readBean(policyVersion);
		policyVersionBinder.setReadOnly(true);

		isSpecialtyAutoPackage = false;
		if (insurancePolicy instanceof SpecialtyAutoPackage) {
			isSpecialtyAutoPackage = true;
		}

		policyTermBinder.readBean(policyTransaction.getPolicyTerm());

		if (Boolean.TRUE.equals(doNotRenew.getValue())) {
			uiHelper.setFieldRedLabel(doNotRenew, doNotRenewReason);
		}

		if (isSpecialtyAutoPackage) {
			specialtyAutoPackageBinder.readBean((SpecialtyAutoPackage) insurancePolicy);

			statCode.setVisible(true);
			territory.setVisible(true);
		} else {
			statCode.setVisible(false);
			territory.setVisible(false);
		}

		enableDataView(customerBinder, this.address.getBinder(), policyBinder, producerBinder,
				brokerAddress.getBinder(), policyVersionBinder, specialtyAutoPackageBinder, policyTermBinder);
	}

	public void enableEdit() {
		enableDataEdit(customerBinder, address.getBinder(), policyBinder, policyVersionBinder);

		if (isSpecialtyAutoPackage) {
			enableDataEdit(specialtyAutoPackageBinder);
		}

		uiHelper.setEditFieldsReadonly(producerBinder, brokerAddress.getBinder());
		uiHelper.setEditFieldsReadonly(statCode, territory);

		setFocus();
	}

	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(customerBinder, address.getBinder(), policyBinder, policyVersionBinder);
		if (isSpecialtyAutoPackage) {
			valid = valid && uiHelper.validateValues(specialtyAutoPackageBinder);
		}

		return valid;
	}

	public boolean saveEdit(Customer customer, Address address, PolicyTransaction<?> policyTransaction) {
		final PolicyVersion policyVersion = policyTransaction.getPolicyVersion();

		final boolean customerOk = uiHelper.writeValues(customerBinder, getCustomer(customer));
		final boolean addressOk = uiHelper.writeValues(this.address.getBinder(), address);
		final InsurancePolicy insurancePolicy = policyVersion.getInsurancePolicy();
		final boolean policyOk = uiHelper.writeValues(policyBinder, insurancePolicy);
		final boolean policyVersionOk = uiHelper.writeValues(policyVersionBinder, policyVersion);
		final boolean specialtyAutoOk = isSpecialtyAutoPackage
				? uiHelper.writeValues(specialtyAutoPackageBinder, insurancePolicy)
				: true;

		return customerOk && addressOk && policyOk && policyVersionOk && specialtyAutoOk;
	}

	protected abstract CustomerUIHelper<T> createCustomerUIHelper();

	protected abstract T getCustomer(Customer customer);

	protected void buildFields() {
		buildPolicyHolderFields();
		buildAddressFields();
		buildBrokerFields();
	}

	protected void buildPolicyHolderFields() {
		policyHolderSectionLabel = buildSectionHeaderLabel("CommercialCustomerCompanySectionLabel");
		doNotRenew = new Checkbox(labels.getString("PolicyInfoFormIsDoNotRenewLabel"));
		doNotRenewReason = new TextField(labels.getString("PolicyInfoFormDoNotRenewReasonLabel"));
	}

	protected void buildAddressFields() {
		addressSectionLabel = buildSectionHeaderLabel("CommercialCustomerAddressSectionLabel");
		address = new AddressForm();
		statCode = new TextField(labels.getString("CommercialCustomerStatCodeLabel"));
		territory = new TextField(labels.getString("CommercialCustomerTerritoryLabel"));
		ruralityInd = new TextField();

		address.addPostalZipValueChangeListener(this::onPostalCodeValueChange);
	}

	protected void buildBrokerFields() {
		brokerSectionLabel = buildSectionHeaderLabel("CommercialCustomerBrokerSectionLabel");
		brokerName = new ComboBox<Producer>(labels.getString("CommercialCustomerBrkNameLabel"));
		brokerContact = new TextField(labels.getString("CommercialCustomerBrkContactLabel"));
		brokerId = new TextField(labels.getString("CommercialCustomerBrkIdLabel"));
		brokerCommission = new DSNumberField<Double>(labels.getString("CommercialCustomerBrkCommissionLabel"),
				Double.class);
		brokerAddress = new AddressForm(false);

		brokerName.setClearButtonVisible(true);
		brokerName.setItemLabelGenerator(Producer::getLegalName);
		brokerCommission.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);

		brokerName.addValueChangeListener(event -> refreshBroker(event.getValue()));
	}

	protected void bindFields() {
		bindCustomer();
		bindPolicy();
		bindProducer();
		bindPolicyVersion();
		bindSpecialtyAutoPackage();
		bindPolicyTerm();
	}

	protected abstract void bindCustomer();

	protected void bindPolicy() {
		policyBinder.forField(brokerName).asRequired().bind(InsurancePolicy::getPolicyProducer,
				InsurancePolicy::setPolicyProducer);
		policyBinder.forField(brokerContact).asRequired().bind(InsurancePolicy::getProducerContactName,
				InsurancePolicy::setProducerContactName);

		policyBinder.setReadOnly(true);
	}

	protected void bindProducer() {
		producerBinder.forField(brokerId).asRequired().bind(Producer::getProducerId, null);

		producerBinder.setReadOnly(true);
	}

	protected void bindPolicyVersion() {
		policyVersionBinder.forField(brokerCommission).asRequired().withNullRepresentation(StringUtils.EMPTY)
				.withConverter(brokerCommission.getConverter())
				.bind(PolicyVersion::getProducerCommissionRate, PolicyVersion::setProducerCommissionRate);

		policyVersionBinder.setReadOnly(true);
	}

	protected void bindSpecialtyAutoPackage() {
		specialtyAutoPackageBinder.forField(statCode)
				.asRequired(labels.getString("CommercialCustomerStatCodeRequiredError"))
				.bind(SpecialtyAutoPackage::getStatCode, SpecialtyAutoPackage::setStatCode);
		specialtyAutoPackageBinder.forField(territory)
				.asRequired(labels.getString("CommercialCustomerTerritoryRequiredError"))
				.bind(SpecialtyAutoPackage::getTerritoryCode, SpecialtyAutoPackage::setTerritoryCode);
		specialtyAutoPackageBinder.forField(ruralityInd).bind(SpecialtyAutoPackage::getRuralityInd,
				SpecialtyAutoPackage::setRuralityInd);

		specialtyAutoPackageBinder.setReadOnly(true);
	}

	protected void bindPolicyTerm() {
		policyTermBinder.forField(doNotRenew).bind(PolicyTerm::isDoNotRenew, PolicyTerm::setDoNotRenew);
		policyTermBinder.forField(doNotRenewReason).bind(PolicyTerm::getDoNotRenewReason,
				PolicyTerm::setDoNotRenewReason);

		policyTermBinder.setReadOnly(true);
	}

	protected void buildLayout() {
		final VerticalLayout layout = new VerticalLayout();

		layout.add(buildPolicyHolderLayout());
		layout.add(buildAddressLayout());
		layout.add(buildBrokerLayout());

		add(layout);
	}

	protected FormLayout buildPolicyHolderLayout() {
		final FormLayout form = createFormLayout(4);
		if (StringUtils.equalsAnyIgnoreCase(getUiMode(), UIConstants.UIACTION_New, UIConstants.UIACTION_Update)) {
			form.add(policyHolderSectionLabel, 4);
		} else {
			form.add(policyHolderSectionLabel);
			final VerticalLayout dnrLayout = new VerticalLayout(doNotRenew);
			uiHelper.formatFullLayout(dnrLayout);
			dnrLayout.setHorizontalComponentAlignment(Alignment.END, doNotRenew);
			final FormLayout dnrForm = createFormLayout(6);
			dnrForm.add(dnrLayout, 2);
			dnrForm.add(doNotRenewReason, 4);
			form.add(dnrForm, 3);
		}

		return form;
	}

	protected FormLayout buildAddressLayout() {
		final FormLayout form = createFormLayout(4);
		form.add(addressSectionLabel, 4);
		address.add(statCode, territory); // Want them on the same line as the address
		form.add(address, 4);

		return form;
	}

	protected FormLayout buildBrokerLayout() {
		final FormLayout form = createFormLayout(4);
		form.add(brokerSectionLabel, 4);
		form.add(brokerName, 2);
		form.add(brokerContact, 2);
		form.add(brokerId, brokerCommission);
		form.add(brokerAddress, 4);

		return form;
	}

	public void loadDataConfigs(String selectedProduct) {
		super.loadDataConfigs(customerBinder, policyBinder, policyVersionBinder, specialtyAutoPackageBinder,
				policyTermBinder);

		try {
			brokerName.setItems(uiLookupHelper.getAllBrokersByInsuranceProduct(selectedProduct));
		} catch (BrokerException e) {
			uiHelper.notificationException(e);
		}
	}

	private void refreshBroker(Producer producer) {
		producerBinder.readBean(producer);
		Optional.ofNullable(producer).map(Producer::getLocations).filter(CollectionUtils::isNotEmpty)
				.map(set -> set.iterator().next()).ifPresent(brokerAddress::loadData);
	}

	private void onPostalCodeValueChange(ComponentValueChangeEvent<TextField, String> event) {
		if (isSpecialtyAutoPackage) {
			final String[] codes = customerUIHelper.getStateAndTerritoryCodesByPostalCode(event.getValue());
			uiHelper.setFieldValue(statCode, codes[0]);
			uiHelper.setFieldValue(territory, codes[1]);
			uiHelper.setFieldValue(ruralityInd, codes[2]);
		}
	}
}
