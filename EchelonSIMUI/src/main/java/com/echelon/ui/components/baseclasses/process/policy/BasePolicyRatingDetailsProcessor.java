package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.prodconf.config.rating.RatebookConfiguration;
import com.ds.ins.prodconf.config.rating.RatebookVersionConfiguration;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyRatingDetailsProcessor extends BasePolicyProcessor implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BasePolicyRatingDetailsProcessor.class.getName());

	private List<RatebookVersionConfiguration> ratebookVersions;
	private String ratebookId;
	private Integer ratebookVersion;

	public BasePolicyRatingDetailsProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
	}

	public List<RatebookVersionConfiguration> getRatebookVersions() {
		return ratebookVersions;
	}

	public void setRatebookId(String ratebookId) {
		this.ratebookId = ratebookId;
	}

	public void setRatebookVersion(Integer ratebookVersion) {
		this.ratebookVersion = ratebookVersion;
	}

	@Override
	public void run() {
		final String processAction = getProcessAction();
		switch (processAction) {
			case UIConstants.UIACTION_LoadData:
				try {
					ratebookVersions = findRatebookVersions(getPolicyTransaction());
				} catch (Exception e) {
					setProcessException(e);
					return;
				}

				break;
			case UIConstants.UIACTION_Update:
				if (!updateRatebook(getPolicyTransaction(), ratebookId, ratebookVersion)) {
					return;
				}

				loadPolicyTransaction();
				break;
			default:
				log.log(Level.WARNING, "Unknown process action: {0}", processAction);
				break;
		}
	}

	protected List<RatebookVersionConfiguration> findRatebookVersions(PolicyTransaction<?> policyTransaction) {
		final PolicyVersion policyVersion = policyTransaction.getPolicyVersion();
		final InsurancePolicy insurancePolicy = policyVersion.getInsurancePolicy();
		final String productCd = insurancePolicy.getProductCd();
		final String programCd = insurancePolicy.getProductProgram();
		final String province = insurancePolicy.getControllingJurisdiction();
		final RatebookConfiguration ratebook = PolicyRatingFactory.getInstance().getPolicyRatingManager()
				.getRatebookConfiguration(productCd, programCd, province);

		Stream<RatebookVersionConfiguration> filteredRatebookVersions;
		if (ratebook != null) {
			final PolicyTerm policyTerm = policyVersion.getVersionTerm();
			final LocalDateTime termEffectiveDate = policyTerm.getTermEffDate();
			if (policyTerm.isNewBusiness()) {
				/*
				 * New business (or manual renewal): users can select from any current or
				 * expired ratebook version. Ratebooks that have yet to take effect are
				 * excluded.
				 */
				filteredRatebookVersions = ratebook.getVersions().stream()
						.filter(versionConfig -> !versionConfig.getNewBusinessEffDate().isAfter(termEffectiveDate));
			} else if (policyTerm.getTermNumber() != null && policyTerm.getTermNumber() > 1) {
				/*
				 * For renewal, users can select from any current or expired ratebook version
				 * that is not older than the ratebook used for the previous policy term.
				 * 
				 * For example, a policy issued with v1 can be renewed with v1 or newer. A
				 * policy issued with v2 can be renewed with v2 or newer, but not v1.
				 */
				try {
					@SuppressWarnings("unchecked")
					final Integer previousRatebookVersion = getPolicyProcessing()
							.findPrecedingPolicyTerm(policyTransaction).getRatebookVersion();

					filteredRatebookVersions = ratebook.getVersions().stream()
							.filter(versionConfig -> !versionConfig.getRenewalEffDate().isAfter(termEffectiveDate))
							.filter(versionConfig -> previousRatebookVersion == null ? true
									: versionConfig.getVersionNum() >= previousRatebookVersion);
				} catch (InsurancePolicyException e) {
					log.log(Level.SEVERE, "Unable to retrieve preceding policy term", e);
					filteredRatebookVersions = Stream.empty();
				}
			} else {
				log.log(Level.SEVERE, "Term is not marked as new business, but policy term number is {0}",
						policyTerm.getTermNumber());
				filteredRatebookVersions = Stream.empty();
			}
		} else {
			log.log(Level.SEVERE, "Could not retrieve ratebook configuration for {0}/{1}/{2}",
					new Object[] { productCd, programCd, province, });
			filteredRatebookVersions = Stream.empty();
		}

		final List<RatebookVersionConfiguration> ratebookVersions = filteredRatebookVersions
				.sorted(Comparator.comparing(RatebookVersionConfiguration::getVersionNum)).collect(Collectors.toList());
		return ratebookVersions;
	}

	protected boolean updateRatebook(PolicyTransaction<?> policyTransaction, String ratebookId,
			Integer ratebookVersion) {
		Objects.requireNonNull(policyTransaction);

		log.log(Level.INFO, "Setting ratebook version to {0} v{1}", new Object[] { ratebookId, ratebookVersion, });
		try {
			policyTransaction = getPolicyProcessing().updatePolicyRatingDetails(policyTransaction, ratebookId,
					ratebookVersion, getUserProfile());

			/*
			 * ES-106: need to trigger logic to enable/disable reinsurance. This is run
			 * whenever the policy's liability limit is changed, but changing the ratebook
			 * might also change the maxRetainedLimit.
			 * 
			 * The simplest way to trigger this is to simply "update" the specialty auto
			 * package data with the current values.
			 */
			if (policyProcessing instanceof ISpecialtyAutoPolicyProcessing) {
				final SpecialtyAutoPackage insurancePolicy = (SpecialtyAutoPackage) policyTransaction.getPolicyVersion()
						.getInsurancePolicy();
				policyTransaction = ((ISpecialtyAutoPolicyProcessing) policyProcessing).updateSpecialtyAutoPackageData(
						insurancePolicy.getLiabilityLimit(), insurancePolicy.getQuotePreparedBy(),
						policyTransaction.getPolicyTerm().getTermEffDate(), policyTransaction.getPolicyTransactionPK(),
						getUserProfile());
			}
		} catch (InsurancePolicyException | SpecialtyAutoPolicyException e) {
			setProcessException(e);
			return false;
		}

		// ratePolicyTransaction will update this.policyTransaction for us
		return ratePolicyTransaction(null, null); // Rate whole policy
	}
}
