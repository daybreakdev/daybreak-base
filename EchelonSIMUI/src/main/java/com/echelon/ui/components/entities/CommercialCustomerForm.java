package com.echelon.ui.components.entities;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.CommercialCustomer;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSPhoneField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.helpers.entities.CommercialCustomerUIHelper;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder.BindingBuilder;
import com.vaadin.flow.data.validator.RegexpValidator;

public class CommercialCustomerForm extends CustomerForm<CommercialCustomer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5806286856703979201L;

	private TextField companyName;
	private DSComboBox<String> businessDescription;
	private TextField rin;
	private DSPhoneField phoneNumber;
	private DSComboBox<String> preferredLanguage;
	private TextField caaContractNumber;
	private DSComboBox<String> caaClub;
	
	private static final String RIN_PATTERN_REQUIRED 			= "\\d{9}?";
	private static final String RIN_PATTERN_REQUIRED_OR_EMPTY 	= "\\d{9}?|\\d{0}";

	public CommercialCustomerForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	public void setFocus() {
		companyName.setAutofocus(true);
	}

	@Override
	public void loadData(Customer customer, Address address, PolicyTransaction<?> policyTransaction,
			String selectedProduct) {
		super.loadData(customer, address, policyTransaction, selectedProduct);

		if (isSpecialtyAutoPackage) {
			caaContractNumber.setVisible(true);
			caaClub.setVisible(true);

			// Special handling for Trucking policies
			if (ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(selectedProduct)) {
				// Hide CAA fields
				caaContractNumber.setVisible(false);
				caaClub.setVisible(false);

				// RIN is mandatory for LHT
				createRINBinding(true);
			}
		} else {
			caaContractNumber.setVisible(false);
			caaClub.setVisible(false);
		}
	}

	public boolean isRINEmpty() {
		return StringUtils.isBlank(rin.getValue());
	}

	@Override
	protected CustomerUIHelper<CommercialCustomer> createCustomerUIHelper() {
		return new CommercialCustomerUIHelper();
	}

	@Override
	protected CommercialCustomer getCustomer(Customer customer) {
		return customer.getCommercialCustomer();
	}

	@Override
	protected void buildPolicyHolderFields() {
		super.buildPolicyHolderFields();

		companyName = new TextField(labels.getString("CommercialCustomerCompanyNameLabel"));
		businessDescription = new DSComboBox<String>(labels.getString("CommercialCustomerBusDescriptionLabel"),
				String.class).withLookupTableId(ConfigConstants.LOOKUPTABLE_BusinessDescriptions);
		rin = new TextField(labels.getString("CommercialCustomerRINLabel"));
		phoneNumber = new DSPhoneField(labels.getString("PersonalCustomerPhoneNumberLabel"));
		preferredLanguage = new DSComboBox<String>(labels.getString("CommercialCustomerLanguageLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_Languages);
		caaContractNumber = new TextField(labels.getString("CommercialCustomerCaaContractNumLabel"));
		caaClub = new DSComboBox<String>(labels.getString("CommercialCustomerCaaClubLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_CAAClubList);

		rin.setPattern("\\d{0,9}");
		rin.setPreventInvalidInput(true);
		rin.setMaxLength(9);
	}

	@Override
	protected void bindCustomer() {
		customerBinder.forField(companyName).asRequired().bind(CommercialCustomer::getLegalName,
				CommercialCustomer::setLegalName);
		customerBinder.forField(businessDescription).asRequired().bind(CommercialCustomer::getBusinessDescription,
				CommercialCustomer::setBusinessDescription);
		createRINBinding(false);
		customerBinder.forField(phoneNumber).bind(CommercialCustomer::getPhoneNumber,
				CommercialCustomer::setPhoneNumber);
		customerBinder.forField(preferredLanguage).asRequired().bind(CommercialCustomer::getPreferredLanguage,
				CommercialCustomer::setPreferredLanguage);

		customerBinder.setReadOnly(true);
	}

	@Override
	protected void bindSpecialtyAutoPackage() {
		specialtyAutoPackageBinder.forField(caaContractNumber).bind(SpecialtyAutoPackage::getAutoAssocMemberNum,
				SpecialtyAutoPackage::setAutoAssocMemberNum);
		specialtyAutoPackageBinder.forField(caaClub).bind(SpecialtyAutoPackage::getAutoAssocClub,
				SpecialtyAutoPackage::setAutoAssocClub);

		super.bindSpecialtyAutoPackage();
	}

	@Override
	protected FormLayout buildPolicyHolderLayout() {
		final FormLayout form = super.buildPolicyHolderLayout();

		form.add(companyName, 2);
		form.add(businessDescription, 2);

		final FormLayout leftSide = createFormLayout(3);
		leftSide.add(rin, phoneNumber, preferredLanguage);
		form.add(leftSide, 2);

		final FormLayout rightSide = createFormLayout(2);
		rightSide.add(caaContractNumber, caaClub);
		form.add(rightSide, 2);

		return form;
	}

	private void createRINBinding(boolean isRequired) {
		customerBinder.removeBinding(rin);
		BindingBuilder<CommercialCustomer, String> builder = customerBinder.forField(rin);
		if (isRequired) {
			builder = builder.asRequired();
		}

		builder.withValidator(new RegexpValidator(labels.getString("CommercialCustomerRINInvalid"), 
								(isRequired ? RIN_PATTERN_REQUIRED : RIN_PATTERN_REQUIRED_OR_EMPTY)
								))
				.bind(CommercialCustomer::getRin, CommercialCustomer::setRin);
	}
}
