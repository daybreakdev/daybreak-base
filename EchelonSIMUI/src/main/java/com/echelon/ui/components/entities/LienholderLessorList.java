package com.echelon.ui.components.entities;

import java.util.ArrayList;
import java.util.List;

import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.LienholderLessorUIHelper;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public abstract class LienholderLessorList<MODEL, PARENTMODEL, UIHELPER extends LienholderLessorUIHelper<MODEL, PARENTMODEL>> 
											extends BaseListLayout<MODEL> {

	protected Grid<MODEL> 		lienLessorPayeeGrid;
	protected UIHELPER			lienLessorUIHelper;
	protected PARENTMODEL		lienLessorParent;
	
	private	ComponentEventListener<SelectionChangeEvent> selectionChangeListener;

    /**
     * @DomEvent is sent when the user clicks button
     */
    @DomEvent("change")
    public static class SelectionChangeEvent extends ComponentEvent<LienholderLessorList> {
        public SelectionChangeEvent(LienholderLessorList source, boolean fromClient) {
            super(source, fromClient);
        }
    }
    
    protected UIHELPER getLienholderLessorUIHelper() {
    	return null;
    }

	public LienholderLessorList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		this.lienLessorUIHelper = getLienholderLessorUIHelper();
		initializeLayouts(labels.getString("LienLessorListLabel"));
		buildGrid();
	}
	
	public void setSelectionChangeListener(ComponentEventListener<SelectionChangeEvent> selectionChangeListener) {
		this.selectionChangeListener = selectionChangeListener;
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														title,
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	protected void addGridColumns() {
		
	}
	
	protected void buildGrid() {
		lienLessorPayeeGrid = new Grid<MODEL>();
		uiHelper.setGridStyle(lienLessorPayeeGrid);
		lienLessorPayeeGrid.setSizeFull();
		
		createIndicatorColumn(lienLessorPayeeGrid);
		
		addGridColumns();
		
		lienLessorPayeeGrid.setDetailsVisibleOnClick(false);
		lienLessorPayeeGrid.setSelectionMode(SelectionMode.SINGLE);
		lienLessorPayeeGrid.addSelectionListener(event -> selectionChanged());
		lienLessorPayeeGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		setUpGridDbClickEvent(lienLessorPayeeGrid);
		
		this.add(lienLessorPayeeGrid);
	}
	
	protected void selectionChanged() {
		refreshToolbar();
		if (selectionChangeListener != null) {
			selectionChangeListener.onComponentEvent(new SelectionChangeEvent(this, true));
		}
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				lienLessorUIHelper.selectAction(lienLessorParent);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!lienLessorPayeeGrid.getSelectedItems().isEmpty()) {
					lienLessorUIHelper.updateAction(lienLessorParent, lienLessorPayeeGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!lienLessorPayeeGrid.getSelectedItems().isEmpty()) {
					lienLessorUIHelper.deleteAction(lienLessorParent, lienLessorPayeeGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (!lienLessorPayeeGrid.getSelectedItems().isEmpty()) {
					lienLessorUIHelper.undeleteAction(lienLessorParent, lienLessorPayeeGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!lienLessorPayeeGrid.getSelectedItems().isEmpty()) {
					lienLessorUIHelper.viewAction(lienLessorParent, lienLessorPayeeGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}

	public PARENTMODEL getLienLessorParent() {
		return lienLessorParent;
	}

	public MODEL getSelectedLienLessor() {
		MODEL selected = null;
		if (!lienLessorPayeeGrid.getSelectedItems().isEmpty()) {
			selected = lienLessorPayeeGrid.getSelectedItems().iterator().next();
		}

		return selected;
	}	
	
	protected List<MODEL> getLienholderLessorsFromParent() {
		return null;
	}
	
	public boolean loadData(PARENTMODEL parent) {
		this.lienLessorParent = parent;
		
		List<MODEL> list = getLienholderLessorsFromParent();
		if (list == null) {
			list = new ArrayList<MODEL>();
		}

		this.lienLessorPayeeGrid.setItems(list);
		if (!list.isEmpty()) {
			this.lienLessorPayeeGrid.select(list.get(0));
		}
		selectionChanged();
		
		return true;
	}
	
	protected void refreshToolbar() {
		if (lienLessorParent == null) {
			toolbar.disableAllButtons();
		}
		else {
			toolbar.refreshButtons((IBusinessEntity) lienLessorParent, lienLessorPayeeGrid);
		}
	}

}
