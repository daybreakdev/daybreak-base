package com.echelon.ui.components;


import java.util.Locale;
import java.util.ResourceBundle;

import com.ds.ins.uicommon.components.DSDownload;
import com.ds.ins.utils.Configurations;
import com.echelon.ui.components.login.LoginUI;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.bordereau.BourderauxUIHelper;
import com.echelon.ui.reporting.helpers.BaseReportHelper;
import com.echelon.ui.reporting.helpers.PolicyByTransactionsReportHelper;
import com.echelon.ui.reporting.helpers.PolicyRenewalReportHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.menubar.MenuBar;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.NavigationTrigger;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLayout;
import com.vaadin.flow.server.PWA;
import com.vaadin.flow.server.VaadinServlet;

/**
 * The main view contains a button and a click listener.
 */
@Push(com.vaadin.flow.shared.communication.PushMode.MANUAL)
@Route("")
@PWA(name = "Daystar Application", shortName = "Daystar")
@CssImport("./styles/shared-styles.css")
@CssImport(value = "./styles/vaadin-text-field-styles.css", themeFor = "vaadin-text-field")
@CssImport(value = "./styles/vaadin-text-area-styles.css",  themeFor = "vaadin-text-area")
@CssImport(value = "./styles/vaadin-checkbox-styles.css",   themeFor = "vaadin-checkbox")
@CssImport(value = "./styles/vaadin-combo-box-styles.css",  themeFor = "vaadin-combo-box")
@CssImport(value = "./styles/vaadin-button-styles.css",  	themeFor = "vaadin-button")
@CssImport(value = "./styles/vaadin-grid-styles.css",  		themeFor = "vaadin-grid")
public class MainView extends AppLayout implements BeforeEnterObserver, BeforeLeaveObserver, RouterLayout {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3941716139269484334L;
	private ResourceBundle		labels;
	private Label				ratingIndicatorLabel;
	private MenuBar				mainMenuBar;
	private MenuItem			reportMItem;
	private MenuItem			bordereauxMItemTab;
	private MenuItem			bordereauxMItemComma;
	private MenuItem			bordereauxAllMItem;
	private MenuItem			policyByTrxReportMItem;
	private MenuItem			policyRenewalReportMItem;
	private MenuItem			langMItem;
	private MenuItem			langEnMItem;
	private MenuItem			langFrMItem;
	private MenuItem 			actionMItem;
	private Div					mainContent;
	private DSDownload 			policyByTrxReportDownload;
	private DSDownload 			policyRenewalReportDownload;
	private static final String MENUITEM_BORDEREAUXSUMMARYTAB 		= "BORDEREAUXSUMMARYTAB";
	private static final String MENUITEM_BORDEREAUXSUMMARYCOMMA 	= "BORDEREAUXSUMMARYCOMMA";
	private static final String MENUITEM_BORDEREAUXALL 				= "BORDEREAUXALL";
	private static final String MENUITEM_POLICYTRXREPORT			= "POLICYBYTRXREPORT";
	private static final String MENUITEM_POLICYRENEWALREPORT		= "POLICYRENEWALREPORT";
	
	private UIHelper					uiHelper;
	
	public MainView() {
    	super();
    	
		BrokerUI.initSession();
    	
    	if (BrokerUI.getUserSession() != null) {
    		BrokerUI.getUserSession().setMainView(this);
    	}
    	
    	BrokerUI.getHelperSession().getSessionUIHelper().setupRunDataFixes();
    	this.uiHelper = BrokerUI.getHelperSession().getUiHelper();
    	
    	buildLayout();
    	refreshMainViewMenuItems();
    	initReportService();
    }
	
	private void buildLayout() {
		labels = uiHelper.getSessionLabels();
		
		VerticalLayout topContainer = new VerticalLayout();
		uiHelper.formatFullLayout(topContainer);
		HorizontalLayout topLayout = new HorizontalLayout();
		uiHelper.formatFullLayout(topLayout);
		
		VerticalLayout iconLayout = addIcons(topLayout);
		VerticalLayout menuLayout = createMenuBar(topLayout);
		topLayout.add(iconLayout, menuLayout);
		topLayout.setVerticalComponentAlignment(Alignment.CENTER, iconLayout, menuLayout);
		
		topLayout.setWidth("99%");
		topContainer.add(topLayout);
		topContainer.setHorizontalComponentAlignment(Alignment.CENTER, topLayout);
		addToNavbar(topContainer);
		
		mainContent = new Div();
		mainContent.setSizeFull();
		setContent(mainContent);
	}
	
	private VerticalLayout addIcons(HorizontalLayout topLayout) {
		VerticalLayout iconLayout = new VerticalLayout();
		uiHelper.formatFullLayout(iconLayout);
		
		Image dsLogo = uiHelper.createCompanyLogoSmall();
		dsLogo.setWidth("90px");
		dsLogo.setHeight("50px");
		iconLayout.add(dsLogo);
		iconLayout.setHorizontalComponentAlignment(Alignment.START, dsLogo);
		
		return iconLayout;
	}
    
    private VerticalLayout createMenuBar(HorizontalLayout topLayout) {
    	VerticalLayout menuSectionLayout = new VerticalLayout();
    	uiHelper.formatFullLayout(menuSectionLayout);
    	HorizontalLayout menuLayout = new HorizontalLayout();
    	//uiHelper.formatFullLayout(menuLayout);
    	
    	ratingIndicatorLabel = new Label(labels.getString("MainViewMenuItemRatingIndLabel"));
    	ratingIndicatorLabel.setVisible(false);
    	mainMenuBar = new MenuBar();
    	menuLayout.add(ratingIndicatorLabel, mainMenuBar);
    	menuLayout.setVerticalComponentAlignment(Alignment.END, ratingIndicatorLabel);
    	menuSectionLayout.add(menuLayout);
    	menuSectionLayout.setHorizontalComponentAlignment(Alignment.END, menuLayout);
    	
    	setupNewActionsMenuItems();
    	
    	MenuItem polInquiryMItem = mainMenuBar.addItem(labels.getString("MainViewMenuItemSubPolicyInquiryLabel"));
    	polInquiryMItem.addClickListener(o -> UI.getCurrent().navigate(UIConstants.POLICYSEARCH_ROUTE));
    	
    	setupReportsMenuItems(menuLayout);
    	setupLanguageMenuItems(menuLayout);
    	
    	//RouterLink logoutLink = new RouterLink(labels.getString("MainViewMenuItemLogoutLabel"), DSLoginUI.class);
    	//MenuItem logoutMItem = mainMenuBar.addItem(logoutLink);
    	MenuItem logoutMItem = mainMenuBar.addItem(labels.getString("MainViewMenuItemLogoutLabel"));
    	logoutMItem.addClickListener(o -> {
    					uiHelper.inactivateUISession();
    				});
    	
    	return menuSectionLayout;
    }
    
    private void setupNewActionsMenuItems() {
    	actionMItem = mainMenuBar.addItem(labels.getString("MainViewMenuItemActionsLabel"));
    }
    
    
    private void setupReportsMenuItems(HorizontalLayout menuLayout) {
    	
	reportMItem = mainMenuBar.addItem(labels.getString("MainViewMenuItemReportsLabel"));
		
	if (BrokerUI.getUserSession().getUserProfile() != null &&
		BrokerUI.getUserSession().getUserProfile().isHasFullAccess()) {
		bordereauxMItemTab=reportMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemBrdTabLabel"), this::BordereauxTabClicked);
	    	new BourderauxUIHelper(bordereauxMItemTab);
	    	
		bordereauxMItemComma=reportMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemBrdCommaLabel"), this::BordereauxCommaClicked);
	    	new BourderauxUIHelper(bordereauxMItemComma);
	    	
			MenuItem bordereauxSummaryMItemTab = reportMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemBrdSumTabLabel"));
			bordereauxSummaryMItemTab.setId(MENUITEM_BORDEREAUXSUMMARYTAB);
			bordereauxSummaryMItemTab.addClickListener(this::BordereauxSummarizeTabClicked);
	    	

			MenuItem bordereauxSummaryMItemComma = reportMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemBrdSumCommaLabel"));
			bordereauxSummaryMItemComma.setId(MENUITEM_BORDEREAUXSUMMARYCOMMA);
			bordereauxSummaryMItemComma.addClickListener(this::BordereauxSummarizeCommaClicked);
			
	    	bordereauxAllMItem = reportMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemBrdTabAllLabel"));
	    	bordereauxAllMItem.setId(MENUITEM_BORDEREAUXALL);
	    	bordereauxAllMItem.addClickListener(this::BordereauxAllClicked);
	}
	
    	policyByTrxReportDownload = new DSDownload(MENUITEM_POLICYTRXREPORT + "DOWNLOAD");
    	menuLayout.add(policyByTrxReportDownload);
    	String lblPolicyByTrxReport = labels.getString("MainViewMenuItemPolicyByTrxReport");
    	policyByTrxReportMItem = reportMItem.getSubMenu().addItem(lblPolicyByTrxReport);
    	policyByTrxReportMItem.setId(MENUITEM_POLICYTRXREPORT);
    	policyByTrxReportMItem.addClickListener(this::printPolicyByTrxReport);
    	
    	policyRenewalReportDownload = new DSDownload(MENUITEM_POLICYRENEWALREPORT + "DOWNLOAD");
    	menuLayout.add(policyRenewalReportDownload);
    	String lblPolicyRenewalReport = labels.getString("MainViewMenuItemPolicyRenewalReport");
    	policyRenewalReportMItem = reportMItem.getSubMenu().addItem(lblPolicyRenewalReport);
    	policyRenewalReportMItem.setId(MENUITEM_POLICYRENEWALREPORT);
    	policyRenewalReportMItem.addClickListener(this::printPolicyRenewalReport);
    	
    	
    	
		reportMItem.addClickListener(this::BordereauxClicked);
    }
    
    private void setupLanguageMenuItems(HorizontalLayout menuLayout) {
    	// Engish first
    	langMItem = mainMenuBar.addItem(labels.getString("MainViewMenuItemLangEnglishLabel"));
    	langEnMItem = langMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemLangEnglishLabel"));
    	langEnMItem.addClickListener(this::switchLanguageClicked);
    	langFrMItem = langMItem.getSubMenu().addItem(labels.getString("MainViewMenuItemLangFrenchLabel"));
    	langFrMItem.addClickListener(this::switchLanguageClicked);
    	langEnMItem.setVisible(false);
    }
    
    private void BordereauxClicked(ClickEvent<MenuItem> event) {
       	// TODO: implement Bordereaux
    	if (bordereauxMItemTab != null) {
    		new BourderauxUIHelper(bordereauxMItemTab);
    	}
    	if (bordereauxMItemComma != null) {
    		new BourderauxUIHelper(bordereauxMItemComma);
    	}
    	if (bordereauxAllMItem != null) {
    		new BourderauxUIHelper(bordereauxAllMItem);
    	}
    }
    
    private void BordereauxTabClicked(ClickEvent<MenuItem> event) {
       	// TODO: implement Bordereaux
    	BourderauxUIHelper buihelper=new BourderauxUIHelper();
    	buihelper.getBourdereauxCsvCustomerSide("tab");

    }
    
    private void BordereauxCommaClicked(ClickEvent<MenuItem> event) {
       	// TODO: implement Bordereaux
    	BourderauxUIHelper buihelper=new BourderauxUIHelper();
    	buihelper.getBourdereauxCsvCustomerSide("comma");

    }
    
    private void BordereauxSummarizeCommaClicked(ClickEvent<MenuItem> event) {
    	// TODO: implement Bordereaux
       	BourderauxUIHelper buihelper=new BourderauxUIHelper();
    	buihelper.getBourdereauxForm("comma");
    }
    
    private void BordereauxSummarizeTabClicked(ClickEvent<MenuItem> event) {
    	// TODO: implement Bordereaux
       	BourderauxUIHelper buihelper=new BourderauxUIHelper();
    	buihelper.getBourdereauxForm("tab");
    }
    
    private void BordereauxAllClicked(ClickEvent<MenuItem> event) {
    	// TODO: implement Bordereaux
       	BourderauxUIHelper buihelper=new BourderauxUIHelper();
    	buihelper.getBourdereauxAllForm("tab");
    }
    
    private void printPolicyByTrxReport(ClickEvent<MenuItem> event) {
    	BaseReportHelper helper = new PolicyByTransactionsReportHelper();
    	helper.openDialog();
    }
    
    private void printPolicyRenewalReport(ClickEvent<MenuItem> event) {
    	BaseReportHelper helper = new PolicyRenewalReportHelper();
    	helper.openDialog();
    }
    
    private void switchLanguageClicked(ClickEvent<MenuItem> event) {
    	if (event.getSource() != null) {
	    	if (event.getSource().equals(langFrMItem)) {
	    		langFrMItem.setVisible(false);
	    		langEnMItem.setVisible(true);
	    		langMItem.setText(langFrMItem.getText());
	    		UI.getCurrent().setLocale(Locale.FRENCH);
	    	}
	    	else {
	    		langFrMItem.setVisible(true);
	    		langEnMItem.setVisible(false);
	    		langMItem.setText(langEnMItem.getText());
	    		UI.getCurrent().setLocale(Locale.ENGLISH);
	    	}
    		BrokerUI.getUserSession().resetSessionLanguage();
    		UI.getCurrent().navigate("");
    		UI.getCurrent().navigate(UIConstants.POLICYSEARCH_ROUTE);
    	}
    }

	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		// TODO Auto-generated method stub
		
		//System.out.println("Enter....");
		
		// E.g. open new page
		if (MainView.class.equals(event.getNavigationTarget())) {
			if (event.getTrigger().equals(NavigationTrigger.PAGE_LOAD)) {
				BrokerUI.removeUserSession();
				event.rerouteTo(UIConstants.DEFAULT_PAGE);
				return;
			}
		}
		
		if (!event.getNavigationTarget().equals(LoginUI.class)) {
			if (!BrokerUI.isValidUserSession()) {
				event.rerouteTo(UIConstants.DEFAULT_PAGE);
			}
			
			// In case reopen - may be handled by the codes above
			else if (event.getNavigationTarget().equals(MainView.class)) {
				if (mainContent == null || 
						mainContent.getChildren() == null ||
						mainContent.getChildren().count() < 1) {
					event.rerouteTo(UIConstants.POLICYSEARCH_ROUTE);
				}
			}
			/*
			else {
				if (NavigationTrigger.PAGE_LOAD.equals(event.getTrigger())) {
					event.rerouteTo("PolicySearchView");
				}
			}
			*/
		}
	}

	@Override
	public void beforeLeave(BeforeLeaveEvent event) {
		// TODO Auto-generated method stub
		//System.out.println("Left....");
	}
/*
	@Override
	public void setParameter(BeforeEvent event, @OptionalParameter String parameter) {
		// TODO Auto-generated method stub
		if (THEMEVARIANTS.VariantDark.name().equalsIgnoreCase(parameter)) {
			UI.getCurrent().getElement().getThemeList().add(Lumo.DARK);
		}
		
		(new DSPolicyUIHelper()).resetSessionPolicyVersion();
		refreshMainViewMenuItems();
	}
*/	
	public void refreshMainViewMenuItems() {
		refreshPolicyMenuItems();
	}
	
	public void refreshPolicyMenuItems() {
		UIFactory.getInstance().getPolicyActionsUIHelper().refreshActionMenuItems(actionMItem, ratingIndicatorLabel);
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		if (uiHelper.handleBeforeEnter()) {
			mainContent.removeAll();
			if (content != null) {
				mainContent.getElement().appendChild(content.getElement());
			}
		}
	}
	
	private void initReportService() {
    	try {
    		UIFactory.getInstance().getReportService(
    				Configurations.getInstance().getProperties(), 
    				VaadinServlet.getCurrent().getServletContext());
    	} catch (Exception e) {
    		uiHelper.notificationError(e.getMessage());
    	}
	}
}
