package com.echelon.ui.components.nonfleet;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.towing.VehicleForm;
import com.echelon.ui.helpers.policy.nonfleet.NonFleetVehicleUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.Binding;

public class NonFleetVehicleForm extends VehicleForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6774263276820433067L;
	
	private static final String DEFAULT_VEHICLECLASS = "43T";
	
	private Checkbox 			isNonStandardField;
	private DSComboBox<Long>	towingVehicleField;
	private Binding<?, ?>		towingVehicleFdBinding;

	public NonFleetVehicleForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected VehicleUIHelper createVehicleUIHelper() {
		// TODO Auto-generated method stub
		return new NonFleetVehicleUIHelper();
	}

	@Override
	protected NonFleetVehicleUIHelper getVehicleUIHelper() {
		// TODO Auto-generated method stub
		return (NonFleetVehicleUIHelper) super.getVehicleUIHelper();
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();
		
		isNonStandardField = new Checkbox(labels.getString("VehicleFormNonStandardLabel"));
		towingVehicleField = new DSComboBox<Long>(labels.getString("VehicleFormTowingVehicleLabel"), Long.class);
		
		binder.forField(isNonStandardField).bind(SpecialtyVehicleRisk::getIsNonStandard, SpecialtyVehicleRisk::setIsNonStandard);
		towingVehicleFdBinding = binder.forField(towingVehicleField)
					.asRequired()
					.bind(SpecialtyVehicleRisk::getTowingVehiclePK, SpecialtyVehicleRisk::setTowingVehiclePK);
	}

	@Override
	protected void buildFirstLine(FormLayout form) {
		// TODO Auto-generated method stub
		//super.buildFirstLine(form);
		form.add(numOfUnitsField, descriptionField, towingVehicleField);
	}

	@Override
	protected FormLayout buildBottomForm() {
		// TODO Auto-generated method stub
		FormLayout form = super.buildBottomForm();
		
		form.add(isNonStandardField);
		
		return form;
	}
	
	protected String getDefaultVehicleClass() {
		return DEFAULT_VEHICLECLASS;
	}

	@Override
	protected boolean loadDataConfigs(Binder... binders) {
		// TODO Auto-generated method stub
		boolean ok = super.loadDataConfigs(binders);
		
		String vehicleDecription = null;
		if (currVehicle != null) {
			vehicleDecription = currVehicle.getVehicleDescription();
		}
		loadTowingVehicleFieldLookup(vehicleDecription);
		
		return ok;
	}

	private void loadTowingVehicleFieldLookup(String vehicleDecription) {
		towingVehicleField.setDSItems(getVehicleUIHelper().getAvailableTowingVehicleLookups(parentSubPolicy));
	}

	@Override
	protected void vehicleDescFieldValueChanged(ValueChangeEvent<?> event) {
		// TODO Auto-generated method stub
		super.vehicleDescFieldValueChanged(event);
		
		if (getVehicleUIHelper().isTrailer(vehicleDescField.getValue())) {
			towingVehicleFdBinding.setAsRequiredEnabled(true);
			loadTowingVehicleFieldLookup(vehicleDescField.getValue());
			uiHelper.setEditFieldsNotReadonly(towingVehicleField);
		}
		else {
			towingVehicleFdBinding.setAsRequiredEnabled(false);
			towingVehicleField.setDSValue(null);
			towingVehicleField.setDSItems(null);
			uiHelper.setEditFieldsReadonly(towingVehicleField);
		}
	}
	
}
