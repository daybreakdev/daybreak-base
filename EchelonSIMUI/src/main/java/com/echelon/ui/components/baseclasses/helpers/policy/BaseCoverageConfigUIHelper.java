package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.ds.ins.prodconf.policy.DeductibleConfig;
import com.ds.ins.prodconf.policy.LimitConfig;
import com.ds.ins.uicommon.components.DSComboBox;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.vaadin.flow.component.AbstractField;

public class BaseCoverageConfigUIHelper extends ContentUIHelper implements Serializable {
	private static final long serialVersionUID = 7189849662375439606L;

	private final Logger logger = Logger.getLogger(getClass().getName());

	public BaseCoverageConfigUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected String getLabelString(String labelCode) {
		return uiHelper.getSessionLabels().getString(labelCode);
	}
	
	public LimitConfig getLimitConfig(CoverageDto coverageDto) {
		if (coverageDto != null) {
			return getLimitConfig(coverageDto.getCoverageConfig());
		}
		
		return null;
	}
	
	public LimitConfig getLimitConfig(ICoverageConfig coverageConfig) {
		if (coverageConfig != null &&
				coverageConfig.getLimitConfigs() != null && 
				!coverageConfig.getLimitConfigs().isEmpty()) {
			return coverageConfig.getLimitConfigs().get(0);
		}
		
		return null;
	}
	
	public DeductibleConfig getDeductibleConfig(CoverageDto coverageDto) {
		if (coverageDto != null) {
			return getDeductibleConfig(coverageDto.getCoverageConfig());
		}
		
		return null;
	}
	
	public DeductibleConfig getDeductibleConfig(ICoverageConfig coverageConfig) {
		if (coverageConfig != null &&
				coverageConfig.getDeductibleConfigs() != null && 
				!coverageConfig.getDeductibleConfigs().isEmpty()) {
			return coverageConfig.getDeductibleConfigs().get(0);
		}
		
		return null;
	}
	
	public boolean useLimitIntegerField(CoverageDto coverageDto) {
		LimitConfig limitConfig = getLimitConfig(coverageDto);
		return (limitConfig != null && 
				(limitConfig.getValidValues() == null || limitConfig.getValidValues().trim().length() < 1));
	}
	
	public boolean isLimitMandatory(CoverageDto coverageDto) {
		LimitConfig limitConfig = getLimitConfig(coverageDto);
		return (limitConfig != null && limitConfig.isMandatory());
	}
		
	public boolean useDeductibleIntegerField(CoverageDto coverageDto) {
		DeductibleConfig deductibleConfig = getDeductibleConfig(coverageDto);
		return (deductibleConfig != null && 
				(deductibleConfig.getValidValues() == null || deductibleConfig.getValidValues().trim().length() < 1));
	}
		
	public boolean isDeductibleMandatort(CoverageDto coverageDto) {
		DeductibleConfig deductibleConfig = getDeductibleConfig(coverageDto);
		return (deductibleConfig != null && deductibleConfig.isMandatory());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setLimitFieldItems(AbstractField limitField, CoverageDto coverageDto) {
		if (!useLimitIntegerField(coverageDto)) {
			DSComboBox coverageLimitField = (DSComboBox) limitField;
			coverageLimitField.clearDSItems();
			LimitConfig limitConfig = getLimitConfig(coverageDto);
			if (limitConfig != null) {
				coverageLimitField.setDSItems(configUIHelper.convertToLimitValues(limitConfig.getValidValues()), false);
			}			
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void setLimitFieldItems(AbstractField limitField, ICoverageConfig coverageConfig, IBusinessEntity covParent) {
		setLimitFieldItems(limitField, new CoverageDto(coverageConfig, new Coverage(), covParent));
	}

	public boolean isAllowLimitOverride(CoverageDto coverageDto) {
		LimitConfig limitConfig = getLimitConfig(coverageDto);
		
		if (limitConfig != null && limitConfig.isAllowLimitOverride()) {
			return true;
		}
		
		return false;
	}
		
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void setDeductibleFieldItems(AbstractField deductibleField, CoverageDto coverageDto) {
		if (!useDeductibleIntegerField(coverageDto)) {
			DSComboBox coverageDeductibleField = (DSComboBox) deductibleField;
			coverageDeductibleField.clearDSItems();
			
			DeductibleConfig deductibleConfig = getDeductibleConfig(coverageDto);
			if (deductibleConfig != null) {
				coverageDeductibleField.setDSItems(configUIHelper.convertToLimitValues(deductibleConfig.getValidValues()), false);
			}			
		}
	}
	
	@SuppressWarnings("rawtypes")
	public void setDeductibleFieldItems(AbstractField limitField, ICoverageConfig coverageConfig, IBusinessEntity covParent) {
		setDeductibleFieldItems(limitField, new CoverageDto(coverageConfig, new Coverage(), covParent));
	}

	public boolean isAllowDeductibleOverride(CoverageDto coverageDto) {
		DeductibleConfig deductibleConfig = getDeductibleConfig(coverageDto);
		
		if (deductibleConfig != null && deductibleConfig.isAllowDeductibleOverride()) {
			return true;
		}
		
		return false;
	}
	
	public boolean isDetailRequired(ICoverageConfig	coverageConfig) {
		boolean openDetail 		= true;
		boolean limitOverride 	= false;
		boolean dedOverride 	= false;
		boolean hasAddlFields 	= false;
		// Premium override, no need to check limit and deductible
		if (coverageConfig != null && !coverageConfig.isAllowPremiumOverride()) {
			if (coverageConfig.getLimitConfigs() != null && !coverageConfig.getLimitConfigs().isEmpty()) {
				LimitConfig limitConfig = coverageConfig.getLimitConfigs().get(0);
				if (limitConfig.isAllowLimitOverride()) {
					limitOverride = true;
				}
				/* always based on flag
				else if (limitConfig.getValidValues() != null && limitConfig.getValidValues().trim().length() > 0) {
					limitOverride = true;
				}
				*/
			}

			if (coverageConfig.getDeductibleConfigs() != null && !coverageConfig.getDeductibleConfigs().isEmpty()) {
				DeductibleConfig dedConfig = coverageConfig.getDeductibleConfigs().get(0);
				if (dedConfig.isAllowDeductibleOverride()) {
					dedOverride = true;
				}
				/* always based on flag
				else if (dedConfig.getValidValues() != null && dedConfig.getValidValues().trim().length() > 0) {
					dedOverride = true;
				}
				*/
			}
			
			// Any Additional Fields
			if (coverageConfig.getDataExtensionConfigs() != null && !coverageConfig.getDataExtensionConfigs().isEmpty()) {
				hasAddlFields = coverageConfig.getDataExtensionConfigs().stream()
										.filter(o -> o.getDataDefinitionConfigs() != null && !o.getDataDefinitionConfigs().isEmpty())
										.findFirst()
										.orElse(null) != null;
			}
		}
		
		openDetail = coverageConfig != null && coverageConfig.isAllowPremiumOverride() || limitOverride || dedOverride || hasAddlFields;
		
		return openDetail;
	}

	public boolean isAllowReinsurance(CoverageDto coverageDto) {
		final Object coverage = coverageDto.getCoverage();
		String code = null;
		Optional<Boolean> isReinsured = Optional.empty();

		if (coverage instanceof Coverage) {
			code = ((Coverage) coverage).getCoverageCode();
			isReinsured = Optional.ofNullable(((Coverage) coverage).isReinsured());
		} else if (coverage instanceof Endorsement) {
			code = ((Endorsement) coverage).getEndorsementCd();
			isReinsured = Optional.ofNullable(((Endorsement) coverage).isReinsured());
		}

		final boolean isAllowReinsurance = isReinsured.orElse(false);
		logger.log(Level.FINE, "Coverage {0} is reinsured: {1}", new Object[] { code, isAllowReinsurance, });
		return isAllowReinsurance;
	}
}
