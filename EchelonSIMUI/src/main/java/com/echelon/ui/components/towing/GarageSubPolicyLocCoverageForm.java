package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.CovIsReinsuredField;
import com.echelon.ui.components.baseclasses.policy.CovUWAdjustmentField;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class GarageSubPolicyLocCoverageForm extends BaseLayout implements IBaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1116223123964282186L;
	private TextField 				coverageTypeField;
	private DatePicker 				coverageEffectiveField;
	private DatePicker 				coverageExpiryField;
	private CovUWAdjustmentField 	coverageUWAfjuestmentField;
	private CovIsReinsuredField		coverageIsReinsuredField;
	private Binder<CoverageDto>	covBinder;
	private Binder<CoverageDto>	covBinder2;

	public GarageSubPolicyLocCoverageForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForms();
	}

	private void buildFields() {
		coverageTypeField 		   = new TextField(labels.getString("CoverageFormCoverageTypeLabel"));
		coverageEffectiveField 	   = new DatePicker(labels.getString("CoverageFormEffectiveLabel"));
		coverageExpiryField 	   = new DatePicker(labels.getString("CoverageFormExpiryLabel"));
		coverageUWAfjuestmentField = new CovUWAdjustmentField(labels.getString("CoverageFormUWAdjustmentLabel"));
		coverageIsReinsuredField   = new CovIsReinsuredField(labels.getString("CoverageFormIsReinsuredLabel"));
		
		covBinder = new Binder<CoverageDto>(CoverageDto.class);
		covBinder.forField(coverageTypeField).bind(CoverageDto::getDescription, null);
		covBinder.forField(coverageEffectiveField).bind(CoverageDto::getCoverageEffDate, null);
		covBinder.forField(coverageExpiryField).bind(CoverageDto::getCoverageExpDate, null);
		covBinder.setReadOnly(true);
		
		covBinder2 = new Binder<CoverageDto>(CoverageDto.class);
		coverageUWAfjuestmentField.bindField(covBinder2); // "coveragePremium.premiumModifier1"
		covBinder2.setReadOnly(true);
	}
	
	private void buildForms() {
		FormLayout coverageForm = new FormLayout();
		
		coverageForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		
		coverageForm.add(coverageTypeField);
		coverageForm.setColspan(coverageTypeField, 4);
		coverageForm.add(coverageEffectiveField, coverageExpiryField, coverageUWAfjuestmentField, coverageIsReinsuredField);
		
		this.add(coverageForm);
	}
	
	@Override
	public void loadData(CoverageDto coverageDto) {
		coverageIsReinsuredField.initializeField(coverageDto);
		coverageIsReinsuredField.bindField(covBinder2);

		covBinder.readBean(coverageDto);
		covBinder2.readBean(coverageDto);
		
		enableDataView(covBinder, covBinder2);
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		uiHelper.setEditFieldsReadonly(covBinder);
		enableDataEdit(covBinder2);
		
		coverageIsReinsuredField.determineReadOnly();
	}

	@Override
	public boolean validateEdit(boolean showMessage) {
		return true;
	}

	@Override
	public boolean saveEdit(CoverageDto coverageDto) {
		boolean ok = uiHelper.writeValues(covBinder2, coverageDto);
		
		return ok;
	}

	@Override
	public boolean anyExtraFields() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setFormVisible(boolean isVisible) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isDataInstanceModified() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setIsReinsuredValueChangeListener(
			ValueChangeListener<? super ComponentValueChangeEvent<?, ?>> listener) {
		// TODO Auto-generated method stub
		coverageIsReinsuredField.addValueChangeListener(listener);
	}

}
