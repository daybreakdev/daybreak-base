package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyDocuments;

public class DocumentDialog extends DSUpdateDialog{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4601319608908860376L;
	private String				action;
	private DocumentForm			documentForm;

	public DocumentDialog(String title, String action) {
		this(title, action, null);
	}

	public DocumentDialog(String title, String action, PolicyDocuments document) {
		super();
		this.action = action;
		this.documentForm = new DocumentForm(action, UIConstants.UICONTAINERTYPES.Dialog);
		initDialog(title, documentForm, action);
	}
	
	public void loadData(PolicyDocuments document) {
		documentForm.loadData(document);
	}
	
	public boolean validateEdit() {
		return documentForm.validateEdit();
	}
	
	public boolean saveEdit(PolicyDocuments document) {
		return documentForm.saveEdit(document);
	}

}
