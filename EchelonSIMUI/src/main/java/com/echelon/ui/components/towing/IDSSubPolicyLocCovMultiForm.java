package com.echelon.ui.components.towing;

import java.util.List;

import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;

public interface IDSSubPolicyLocCovMultiForm {
	
	public void 					locationChanged();
	public List<LookupTableItem> 	getAvailableLocations(PolicyLocation include);

}
