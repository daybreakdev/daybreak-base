package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.vaadin.flow.data.binder.Binder;

public class CovLimitField extends BaseCovLimitAndDedField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8790548562468217418L;

	public CovLimitField(BaseCoverageConfigUIHelper coverageConfigUIHelper, BaseCoverageUIHelper coverageUIHelper) {
		super(coverageConfigUIHelper, coverageUIHelper);
	}
	
	public void bindField(Binder<CoverageDto> binder) {
		super.setBinder(binder);
		
		removeBinding();
		
		if (!isAllowOverride() || isDeletedCoverage()) {
			binder.forField(integerField)
								.withNullRepresentation("")
								.withConverter(integerField.getConverter())
								.bind(CoverageDto::getLimit1, null);
		}
		else if (isInteger()) {
			if (isMandatory()) {
				binder.forField(integerField).asRequired()
							 	.withNullRepresentation("")
							 	.withConverter(integerField.getConverter())
							 	.bind(CoverageDto::getLimit1, CoverageDto::setLimit1);
			}
			else {
				binder.forField(integerField)
								.withNullRepresentation("")
								.withConverter(integerField.getConverter())
								.bind(CoverageDto::getLimit1, CoverageDto::setLimit1);
			}
		}
		else {
			if (isMandatory()) {
				binder.forField(lookupField).asRequired()
								.bind(CoverageDto::getLimit1, CoverageDto::setLimit1);
			}
			else {
				binder.forField(lookupField)
								.bind(CoverageDto::getLimit1, CoverageDto::setLimit1);
			}
		}
	}
	
	protected boolean isInteger() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isInteger = this.coverageDto.isLimitInteger();

		return isInteger;
	}
	
	protected boolean isMandatory() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isMandatory = this.coverageDto.isLimitMandatory();

		return isMandatory;
	}

	protected boolean isAllowOverride() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isAllowOverride = this.coverageDto.isAllowLimitOverride();

		return isAllowOverride;
	}
	
	protected void setFieldItems(CoverageDto dto) {
		coverageConfigUIHelper.setLimitFieldItems(lookupField, dto);
	}

	public void validate() {
		if (isAllowOverride() && !isDeletedCoverage()) {
			if (isInteger()) {
				integerField.validate();
				integerField.setRequiredIndicatorVisible(true);
			}
			else {
				lookupField.isInvalid();
				lookupField.setRequiredIndicatorVisible(true);
			}
		}
	}
}
