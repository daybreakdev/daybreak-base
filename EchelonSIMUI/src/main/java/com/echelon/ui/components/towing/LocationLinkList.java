package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.LocationLinkUIHelper;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.provider.ListDataProvider;

public class LocationLinkList extends BaseListLayout<CGLLocation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5960310517594701574L;
	private CGLSubPolicy<?> 		parentCGLSubPolicy;
	private Grid<CGLLocation>		locGrid;
	private LocationLinkUIHelper	locLinkUIHelper;

	public LocationLinkList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.locLinkUIHelper = new LocationLinkUIHelper();
		
		initializeLayouts();
		buildGrid();
	}

	protected void initializeLayouts() {
		this.setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType) ||
			UIConstants.UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			String title = labels.getString("CGLLocationDetailsTitle");
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													title,
													UIConstants.UICOMPONENTTYPES.List);
			this.toolbar = headComponents.getToolbar();			
			setupNewButtonEvent();
			setupUpdateButtonEvent();
			setupDeleteButtonEvent();
		}		
	}

	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).setVisible(false);
		}
	}

	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> 
				(new LocationLinkUIHelper()).setupSelectionAction(parentCGLSubPolicy, getLocationList())
			);
		}
	}

	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!locGrid.getSelectedItems().isEmpty()) {
					(new LocationLinkUIHelper()).setupDeleteAction(parentCGLSubPolicy, locGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void buildGrid() {
		locGrid = new Grid<CGLLocation>();
		locGrid.setSizeFull();
		uiHelper.setGridStyle(locGrid);
		addGridColumns();
		locGrid.setSelectionMode(SelectionMode.SINGLE);
		locGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(locGrid);
		
		this.add(locGrid);
	}
	
	protected void addGridColumns() {
		createIndicatorColumn(locGrid);
		locGrid.addColumn(loc -> loc.getPolicyLocation().getLocationId()).setHeader(labels.getString("LocationNumLabel")).setSortable(true);
		locGrid.addColumn(loc -> loc.getPolicyLocation().getLocationAddress().getAddressLine1()).setHeader(labels.getString("LocationAddressLabel"));
		locGrid.addColumn(loc -> loc.getPolicyLocation().getLocationAddress().getCity()).setHeader(labels.getString("LocationCityLabel"));
		locGrid.addColumn(locLinkUIHelper.getRenderer(uiLookupHelper)).setHeader(labels.getString("LocationProvinceLabel"));
		locGrid.addColumn(loc -> loc.getPolicyLocation().getLocationAddress().getPostalZip()).setHeader(labels.getString("LocationPostalCodeLabel"));
	}


	public boolean loadData(CGLSubPolicy<?> cglSubPolicy) {
		this.parentCGLSubPolicy = cglSubPolicy;
		
		List<CGLLocation> list = new ArrayList<CGLLocation>();
		if (this.parentCGLSubPolicy != null) {
			if (cglSubPolicy != null && cglSubPolicy.getCglLocations() != null) {
				list.addAll(cglSubPolicy.getCglLocations());
			}
		}
		
		list = list.stream().sorted((o1, o2) -> o1.getPolicyLocation().getLocationId().compareTo(o2.getPolicyLocation().getLocationId())).collect(Collectors.toList());
		
		locGrid.setItems(list);
		refreshToolbar();
		return true;
	}
	
	@Override
	protected void refreshToolbar() {
		if (this.parentCGLSubPolicy == null) {
			if (this.toolbar != null) {
				this.toolbar.disableAllButtons();
			}
		}
		else {
			if (this.toolbar != null) {
				toolbar.refreshButtons(parentCGLSubPolicy, locGrid);
				
				Button udButton = this.toolbar.getButton(UIConstants.UIACTION_Undelete);
				Button deButton = this.toolbar.getButton(UIConstants.UIACTION_Delete);
				
				// The policylocation of policy can be soft-deleted
				// System dose not support "soft delete" for GL location 
				if (udButton != null) {
					udButton.setVisible(false);
					
					// the core codes will disable Delete and make Undelete available based
					// on the object "policylocation" 
					if (udButton.isEnabled()) {
						if (deButton != null) {
							deButton.setEnabled(true);
							deButton.setVisible(true);
						}
					}
				}
			}
		}
	}

	protected void refreshDeleteButton() {
		// Delete allowed
	}

	protected List<CGLLocation> getLocationList() {
		List<CGLLocation> list = (List<CGLLocation>) ((ListDataProvider<CGLLocation>)this.locGrid.getDataProvider()).getItems();
		if (list == null) {
			list = new ArrayList<CGLLocation>();
		}
		return list;
	}

}
