package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSGenericFieldsList;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;

public class GenericFieldsList extends DSGenericFieldsList {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1651657394074965434L;

	public GenericFieldsList(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType,
			com.ds.ins.uicommon.dto.DSGenericFieldGroupDto valueGroupDto, FormLayout followFormLayout) {
		super(uiMode, uiContainerType, valueGroupDto, followFormLayout);
		// TODO Auto-generated constructor stub
	}

}
