package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverConvictionUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverConviction;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public class DriverConvictionList extends BaseListLayout<DriverConviction> {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5096491630938219407L;
	private DriverConvictionUIHelper 	convictionUIHelper = new DriverConvictionUIHelper();	
	private Grid<DriverConviction> 		convictionGrid;
	private Driver						driver;

	public DriverConvictionList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}
	
	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("DriverConvictionListTitle"
																+ ""),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	private void buildGrid() {
		convictionGrid = new Grid<DriverConviction>();
		convictionGrid.setSizeFull();
		uiHelper.setGridStyle(convictionGrid);
		
		createIndicatorColumn(convictionGrid);
		convictionGrid.addColumn(o -> (o.getMvrDate() != null ? o.getMvrDate().toLocalDate() : ""))
								.setHeader(labels.getString("DriverConvictionListColumnMVRDate"))
								.setAutoWidth(true);
		convictionGrid.addColumn(DriverConviction::getConvictionDate)
								.setHeader(labels.getString("DriverConvictionListColumnConvDate"))
								.setAutoWidth(true);
		convictionGrid.addColumn(new DSLookupItemRenderer<DriverConviction>(DriverConviction::getConvictionCode).withIncludeCodeItemLabel(true)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ConvictionCode))
								.setHeader(labels.getString("DriverConvictionListColumnConvCode"))
								.setAutoWidth(true);
		convictionGrid.addColumn(new DSLookupItemRenderer<DriverConviction>(DriverConviction::getConvictionType)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ConvictionType))
								.setHeader(labels.getString("DriverConvictionListColumnConvType"))
								.setAutoWidth(true);
		convictionGrid.setSelectionMode(SelectionMode.SINGLE);
		convictionGrid.addSelectionListener(event -> refreshToolbar());
		convictionGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		setUpGridDbClickEvent(convictionGrid);
		
		this.add(convictionGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				convictionUIHelper.newConvictionAction(driver);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (convictionGrid.asSingleSelect() != null) {
					convictionUIHelper.updateConvictionAction(driver, convictionGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (convictionGrid.asSingleSelect() != null) {
					convictionUIHelper.deleteConvictionAction(driver, convictionGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (convictionGrid.asSingleSelect() != null) {
					convictionUIHelper.undeleteConvictionAction(driver, convictionGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(Driver driver, Set<DriverConviction> set) {
		this.driver = driver;
		List<DriverConviction> list = convictionUIHelper.sortConvictionList(set);

		this.convictionGrid.setItems(list);
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(driver, convictionGrid);
	}

}


