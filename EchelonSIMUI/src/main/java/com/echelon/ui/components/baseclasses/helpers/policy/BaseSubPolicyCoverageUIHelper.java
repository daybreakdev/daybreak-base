package com.echelon.ui.components.baseclasses.helpers.policy;

import java.util.List;

import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.policy.SubPolicyConfig;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.towing.SubPolicyCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;

public class BaseSubPolicyCoverageUIHelper extends BaseCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6735804009080805918L;
	
	public BaseSubPolicyCoverageUIHelper(OBJECTTYPES parentObjectType, POLICYTREENODES policyTreeNode) {
		super(parentObjectType, policyTreeNode);
		// TODO Auto-generated constructor stub
	}

	public void loadView(SubPolicyCoverageList component, SubPolicy<?> subPlicy) {
		if (subPlicy != null) {
			SubPolicyConfig subPolicyConfig = configUIHelper.getSubPolicyConfig(subPlicy.getSubPolicyTypeCd());
			List<CoverageDto> coverageDtoList = buildFullCoverageList(subPolicyConfig, subPlicy);
			component.loadData(subPolicyConfig, subPlicy, coverageDtoList);
		}
		else {
			component.loadData(null, null, null);
		}
	}
}
