package com.echelon.ui.components.entities;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.echelon.ui.components.baseclasses.MultiNewEntryForm;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.SubPolicy;

public class AdditionalInsuredMultiForm extends MultiNewEntryForm<AdditionalInsuredForm, AdditionalInsured> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4813778013969065354L;
	private AdditionalInsuredUIHelper addlInsuredUIHelper = new AdditionalInsuredUIHelper();
	private SubPolicy subPolicy;

	public AdditionalInsuredMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, 4);
	}

	@Override
	protected DSMultiNewEntryDto<AdditionalInsuredForm, AdditionalInsured> createMultiNewEntryDto() {
		// TODO Auto-generated method stub
		DSMultiNewEntryDto<AdditionalInsuredForm, AdditionalInsured> dto = super.createMultiNewEntryDto();
		dto.setData(addlInsuredUIHelper.createEmptyAdditionalInsured(subPolicy));
		
		AdditionalInsuredForm form = new AdditionalInsuredForm(uiMode, uiContainerType);
		form.loadData(subPolicy, dto.getData());
		form.enableEdit();
		dto.setForm(form);
		
		return dto;
	}

	@Override
	protected boolean formValidateEdit(DSMultiNewEntryDto<AdditionalInsuredForm, AdditionalInsured> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().validateEdit(false);
	}

	@Override
	protected boolean formSaveEdit(DSMultiNewEntryDto<AdditionalInsuredForm, AdditionalInsured> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().saveEdit(dto.getData());
	}
	
	public void loadData(SubPolicy subPolicy) {
		this.subPolicy = subPolicy;
	}
	
	public void enableEdit() {
		super.enableEdit();
		this.initialzieRow();
	}

}
