package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;

import com.ds.ins.uicommon.components.DSComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;

public class AutoSubPolicyForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3827357022773614201L;
	private RadioButtonGroup<Boolean> 	isFleetField;
	private DSComboBox<String>  		fleetBasisField;
	
	private Binder<SpecialityAutoSubPolicy>	binder;

	public AutoSubPolicyForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForm();
	}

	private void buildFields() {
		isFleetField = new RadioButtonGroup<Boolean>();
		isFleetField.setItems(Boolean.TRUE, Boolean.FALSE);
		isFleetField.setRenderer(new ComponentRenderer(o -> {
			String valueLabel = 
					((Boolean)o 
						? labels.getString("AutoSubPolicyFormIsFleetLabel")
						: labels.getString("AutoSubPolicyFormIsNonFleetLabel")
						);
			Div div = new Div();
			div.setText(valueLabel);
		    return div;
		}));
		fleetBasisField = new DSComboBox<String>(labels.getString("AutoSubPolicyFormFleetBasisLabel"), String.class)
								.withLookupTableId(ConfigConstants.LOOKUPTABLE_fleetbasis);
		
		binder = new Binder<SpecialityAutoSubPolicy>(SpecialityAutoSubPolicy.class);
		binder.forField(isFleetField).asRequired().bind(SpecialityAutoSubPolicy::getIsFleet, SpecialityAutoSubPolicy::setIsFleet); 
		binder.forField(fleetBasisField).asRequired().bind(SpecialityAutoSubPolicy::getFleetBasis, SpecialityAutoSubPolicy::setFleetBasis);
	}
	
	private void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );

		form.add(isFleetField);
		uiHelper.addFormEmptyColumn(form);
		form.add(fleetBasisField);
		uiHelper.addFormEmptyColumn(form);
		
		this.add(form);
	}

	public void loadDataConfigs() {
		loadDataConfigs(binder);
	}
	
	public void loadData(SpecialityAutoSubPolicy autoSubPolicy) {
		loadDataConfigs();
		
		binder.readBean(autoSubPolicy);
		
		enableDataView(binder);
	}
	
	public void loadValues(Boolean isFleet, String fleetBasis) {
		uiHelper.setFieldValue(isFleetField, isFleet);
		fleetBasisField.setDSValue(fleetBasis);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		uiHelper.setEditFieldsReadonly(isFleetField);
		
		setFocus();
	}
	
	public boolean saveEdit(SpecialityAutoSubPolicy autoSubPolicy) {
		boolean ok1 = uiHelper.writeValues(binder, autoSubPolicy);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	@Override
	public void setFocus() {
		fleetBasisField.focus();
	}
}
