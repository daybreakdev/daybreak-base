package com.echelon.ui.components.policy.wheels;

import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.prodconf.baseclasses.NavigationTreeItemConfig;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.echelon.ui.components.towing.TowingPolicyTreeNavigator;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverUIHelper;

public class NonFleetPolicyTreeNavigator extends TowingPolicyTreeNavigator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7349506216948915088L;

	public NonFleetPolicyTreeNavigator() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSTreeGridItem createTreeItem(DSTreeGridItem parentItem, NavigationTreeItemConfig treeItemConfig,
			Object dataObject) {
		
		if (UIConstants.POLICYTREENODES.Driver.name().equalsIgnoreCase(treeItemConfig.getNodeType())) {
			buildDriverItems(parentItem, treeItemConfig);
			return null;
		}

		return super.createTreeItem(parentItem, treeItemConfig, dataObject);
	}
	
	// build the driver at the same level as drivers node
	// this is called after vehicles node is created
	private void buildDriverItems(DSTreeGridItem parentItem, NavigationTreeItemConfig itemConfig) {
		if (parentItem != null && parentItem.getNodeObject() != null && parentItem.getNodeObject() instanceof PolicyTransaction) {
			DriverUIHelper driverUIHelper = new DriverUIHelper();
			
			PolicyTransaction<?> policyTransaction =  (PolicyTransaction<?>) parentItem.getNodeObject();
			List<Driver> drivers = driverUIHelper.sortDriverList(policyTransaction.getDrivers());	
			drivers.stream().forEach(o -> {
					String driverText = driverUIHelper.getTreeNodeText(o);
					
					DSTreeGridItem vehItem = new DSTreeGridItem(parentItem, driverText, itemConfig.getNodeType(), itemConfig.getUiName(),
																policyTransaction, o);
					parentItem.addChildItem(vehItem);
				});
		}
	}

}
