package com.echelon.ui.components.baseclasses.policy;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;
import com.echelon.ui.components.baseclasses.validators.BasePolicyChangeEffectiveDateValidator;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.ds.ins.utils.Constants;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSComboBox;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyCancelConfirmForm extends BaseLayout implements IBasePolicyConfirmForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5254663548241714196L;
	protected DSComboBox<String>	cancelTypeField;
	protected DSComboBox<String>	cancelReasonField;
	protected DatePicker			confirmEffectiveDate;
	protected TextField				quotePolicyNumField;
	protected TextField				insuredField;
	protected DatePicker			effectiveDateField;
	protected DatePicker			expiryDateField;
	protected DSUpdateDialog<?>		updDialog;
	
	protected String				policyAction;
	protected String 			 	policyVerTxnType;
	protected String				policyVerStatus;
	
	protected PolicyTransaction<?> 	policyTransaction;
	
	protected Binder<PolicyConfirmDto> 		binder;
	protected Binder<PolicyTransaction> 	ptBinder;
	
	protected PersonalCustomerUIHelper		personalCustomerUIHelper;
	
	protected BasePolicyChangeEffectiveDateValidator policyChangeEffectiveDateValidator;

	public BasePolicyCancelConfirmForm(String uiMode, UICONTAINERTYPES uiContainerType,
											String 	policyAction,
											String 	policyVerTxnType,
											String 	policyVerStatus) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.personalCustomerUIHelper = new PersonalCustomerUIHelper();
		
		this.policyAction 	  = policyAction;
		this.policyVerTxnType = policyVerTxnType;
		this.policyVerStatus  = policyVerStatus;
		buildFields();
		buildBinder();
		buildForm();
	}
	
	protected void buildFields() {
		cancelTypeField 			= new DSComboBox<String>(labels.getString("PolicyConfirmCancelFormTypeLabel"), String.class)
														.withLookupTableId(ConfigConstants.LOOKUPTABLE_CancellationTypes);
		cancelReasonField		= new DSComboBox<String>(labels.getString("PolicyConfirmCancelFormReasonLabel"), String.class)
														.withLookupTableId(ConfigConstants.LOOKUPTABLE_CancellationReasons);
		confirmEffectiveDate 	= new DatePicker(labels.getString("PolicyConfirmCancelFormEffectiveDateLabel"));
		insuredField			= new TextField(labels.getString("PolicyConfirmFormInsuredLabel"));
		effectiveDateField		= new DatePicker(labels.getString("PolicyConfirmFormEffDateLabel"));
		expiryDateField			= new DatePicker(labels.getString("PolicyConfirmFormExpDateLabel"));
		quotePolicyNumField 	= new TextField(labels.getString("PolicyConfirmFormPolicyNumLabel"));
		
		policyChangeEffectiveDateValidator = new BasePolicyChangeEffectiveDateValidator();
		
		confirmEffectiveDate.addValueChangeListener(this::onConfirmEffectiveDateChanged);
		cancelTypeField.addValueChangeListener(this::onCancelTypeChanged);
		cancelReasonField.addValueChangeListener(this::onCancelReasonChanged);
	}
	
	protected void buildBinder() {
		binder = new Binder<PolicyConfirmDto>(PolicyConfirmDto.class);
		binder.forField(cancelTypeField).asRequired().bind(PolicyConfirmDto::getChangeMethod, PolicyConfirmDto::setChangeMethod);
		binder.forField(cancelReasonField).asRequired().bind(PolicyConfirmDto::getChangeReasonDetails, PolicyConfirmDto::setChangeReasonDetails);
		binder.forField(confirmEffectiveDate).asRequired().withValidator(policyChangeEffectiveDateValidator)
												.bind(PolicyConfirmDto::getChangeEffectiveDate, PolicyConfirmDto::setChangeEffectiveDate);
		binder.setReadOnly(true);
		
		ptBinder = new Binder<PolicyTransaction>(PolicyTransaction.class);
		//ptBinder.forField(insuredField).bind("policyVersion.insurancePolicy.policyCustomer.commercialCustomer.legalName");
		ptBinder.forField(effectiveDateField).withConverter(new DSLocaldateTimeToLocalDateConverter())
										.bind("policyTerm.termEffDate");
		ptBinder.forField(expiryDateField).withConverter(new DSLocaldateTimeToLocalDateConverter())
										.bind("policyTerm.termExpDate");
		ptBinder.forField(quotePolicyNumField).bind("policyVersion.insurancePolicy.basePolicyNum");
		
		ptBinder.setReadOnly(true);
	}
	
	protected void bindInsuredField(PolicyTransaction policyTransaction) {
		if (policyTransaction != null) {
			if (policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
				uiHelper.setFieldValue(insuredField, policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName());
			}
			else if (policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
				uiHelper.setFieldValue(insuredField, personalCustomerUIHelper.getCustomerName(policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer()));
			}
		}
		
		insuredField.setReadOnly(true);
	}

	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
	        );

		form.add(confirmEffectiveDate);
		uiHelper.addFormEmptyColumn(form);
		form.add(cancelTypeField, 2);
		form.add(cancelReasonField, 2);
		form.add(quotePolicyNumField, insuredField);
		form.add(effectiveDateField, expiryDateField);
		
		this.add(form);
	}
	
	public void loadData(PolicyTransaction<?> policyTransaction, PolicyConfirmDto dto, DSUpdateDialog<?> updDialog) {
		this.policyTransaction = policyTransaction;
		this.updDialog = updDialog;
		
		policyChangeEffectiveDateValidator.setPolicyTransaction(policyTransaction);
		
		loadDataConfigs(binder, ptBinder);

		binder.setBean(dto);
		ptBinder.readBean(policyTransaction);
		bindInsuredField(policyTransaction);
		
		filterCancelTypes(policyTransaction);
		
 		enableDataView(binder, ptBinder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
	}
	
	public boolean validateEdit() {
		boolean ok = uiHelper.validateValues(binder);
		return ok;
	}
	
	public PolicyConfirmDto getConfirmDto() {		
		if (!uiHelper.writeValues(binder, binder.getBean())) {
			return null;
		}
		return binder.getBean();
	}
	
	protected void filterCancelTypes(PolicyTransaction policyTransaction) {
		
	}
	
	protected void onConfirmEffectiveDateChanged(ValueChangeEvent event) {
		filterCancelTypes(policyTransaction);
	}
	
	protected void onCancelTypeChanged(ValueChangeEvent event) {
		
	}
	
	protected void onCancelReasonChanged(ValueChangeEvent event) {
		
	}
}
