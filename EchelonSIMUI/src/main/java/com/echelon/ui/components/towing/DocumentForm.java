package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.utils.Constants;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.upload.Upload;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.data.binder.Binder;

public class DocumentForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -682613419454609374L;
	protected Button saveButton;
	protected DSComboBox<String> documentIdField;
	protected TextField descriptionField;
	protected DSComboBox<String> distributionTargetField;
	protected DSComboBox<String> storageTypeField;
	protected Upload uploadField;
	protected Binder<PolicyDocuments> binder;
	protected FormLayout documentForm;
	protected MemoryBuffer memoryBuffer = new MemoryBuffer();
	protected String businessStatus;
	protected String txnBusinessStatus;
	protected String policyTxnType;

	public DocumentForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub

		buildLayout();
		buildFields();
		buildForm();
	}

	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}

	protected void buildFields() {
		documentIdField = new DSComboBox<String>(labels.getString("DocumentFormDocumentIdLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_DocId);
		descriptionField = new TextField(labels.getString("DocumentFormDescriptionLabel"));
		distributionTargetField = new DSComboBox<String>(labels.getString("DocumentFormDistributionTargetLabel"),
				String.class).withLookupTableId(ConfigConstants.LOOKUPTABLE_DistTarget);
		storageTypeField = new DSComboBox<String>(labels.getString("DocumentFormStorageTypeLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_FileType);
		uploadField = new Upload(memoryBuffer);
		uploadField.setAcceptedFileTypes("application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/pdf","text/plain");

		documentIdField.loadDSItems();
		distributionTargetField.loadDSItems();
		storageTypeField.loadDSItems();
	}

	protected void buildForm() {
		documentForm = new FormLayout();
		documentForm.removeAll();

		documentForm.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 1));
		documentForm.add(documentIdField);
		documentForm.add(descriptionField);
		documentForm.add(distributionTargetField);
		documentForm.add(storageTypeField);
		documentForm.add(uploadField);
		this.add(documentForm);
	}

	protected void initialzieFields(PolicyDocuments document) {

	}

	protected void buildBinder() {
		binder = new Binder<PolicyDocuments>(PolicyDocuments.class);
		binder.forField(documentIdField).asRequired().bind(PolicyDocuments::getDocumentId,PolicyDocuments::setDocumentId);
		binder.forField(descriptionField).bind(PolicyDocuments::getDescription, PolicyDocuments::setDescription);
		binder.forField(distributionTargetField).asRequired().bind(PolicyDocuments::getDistributionTarget,PolicyDocuments::setDistributionTarget);
		binder.forField(storageTypeField).asRequired().bind(PolicyDocuments::getStorageType, PolicyDocuments::setStorageType);
		binder.setReadOnly(true);
	}
	
	private void documentIdFieldValueChanged(ValueChangeEvent event) {
		boolean isUpload = false;
		if (event != null && event.getValue() != null) {
			String value = (String) event.getValue();
			isUpload = value.equalsIgnoreCase("UP");
		}

		if (documentIdField != null && documentIdField.getValue() != null) {
			isUpload = documentIdField.getValue().equalsIgnoreCase("UP");
		}

		if (isUpload) {
			uploadField.setVisible(true);
			distributionTargetField.setValue("NA");
			storageTypeField.setVisible(false);
			if (uiMode.equals(UIConstants.UIACTION_New)) {
				saveButton.setText(uiHelper.getCommonLabels().getString("ButtonUploadLabel"));
			}
		} else {
			uploadField.setVisible(false);
			distributionTargetField.setValue("BR");
			storageTypeField.setVisible(true);
			if (uiMode.equals(UIConstants.UIACTION_New)) {
				storageTypeField.setValue("DOC");
			}
			saveButton.setText(uiHelper.getCommonLabels().getString("ButtonSaveLabel"));
		}

	}
	
	public void loadData(PolicyDocuments document) {
		initialzieFields(document);
		buildForm();
		buildBinder();
		
		binder.readBean(document);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		documentIdFieldValueChanged(null);
		documentIdField.addValueChangeListener(this::documentIdFieldValueChanged);
		boolean isTxIssued = Constants.BUSINESS_STATUS_ISSUED.equalsIgnoreCase(txnBusinessStatus);
		boolean isReinstatement = Constants.VERSION_STATUS_ISSUEDREINSTATEMENT.equalsIgnoreCase(businessStatus)
				|| Constants.VERSION_STATUS_PENDINGREINSTATEMENT.equalsIgnoreCase(businessStatus);
		boolean isReissue = Constants.VERSION_STATUS_ISSUEDREISSUE.equalsIgnoreCase(businessStatus)
				|| Constants.VERSION_STATUS_PENDINGREISSUE.equalsIgnoreCase(businessStatus);
		
		if(uiMode.equals(UIConstants.UIACTION_Update)) {
			uiHelper.setEditFieldsReadonly(documentIdField);
			if(documentIdField.getValue().equalsIgnoreCase("UP")) {
				uiHelper.setEditFieldsReadonly(distributionTargetField);
				uploadField.setVisible(false);
			}
		}
		
		if (uiMode.equals(UIConstants.UIACTION_New)) {
			List<String> itemsExcluded = new ArrayList<String>();
			if(isTxIssued || isReinstatement) {
				uiHelper.setEditFieldsReadonly(documentIdField);
				documentIdField.setValue("UP");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_NEWBUSINESS)
					|| policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_RENEWAL)) {
				itemsExcluded.add("PC");
				itemsExcluded.add("CA");
				itemsExcluded.add("EP");
				itemsExcluded.add("ADJ");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_EXTENSION)) {
				itemsExcluded.add("QL");
				itemsExcluded.add("PP");
				itemsExcluded.add("AF");
				itemsExcluded.add("PC");
				itemsExcluded.add("CA");
				itemsExcluded.add("ADJ");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_REISSUE)) {
				itemsExcluded.add("PC");
				itemsExcluded.add("CA");
				itemsExcluded.add("QL");
				itemsExcluded.add("EP");
				itemsExcluded.add("ADJ");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_POLICYCHANGE)) {
				itemsExcluded.add("QL");
				itemsExcluded.add("PP");
				itemsExcluded.add("AF");
				itemsExcluded.add("CA");
				itemsExcluded.add("EP");
				itemsExcluded.add("ADJ");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_CANCELLATION)) {
				itemsExcluded.add("QL");
				itemsExcluded.add("PP");
				itemsExcluded.add("AF");
				itemsExcluded.add("PC");
				itemsExcluded.add("EP");
				itemsExcluded.add("ADJ");
			} else if (policyTxnType.equalsIgnoreCase(Constants.VERS_TXN_TYPE_ADJUSTMENT)) {
				itemsExcluded.add("QL");
				itemsExcluded.add("PP");
				itemsExcluded.add("AF");
				itemsExcluded.add("PC");
				itemsExcluded.add("EP");
				itemsExcluded.add("CA");
			}

			List<LookupTableItem> items = BrokerUI.getHelperSession().getLookupUIHelper()
					.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_DocId);
			
			List<LookupTableItem> newItems = new ArrayList<LookupTableItem>();
			for (LookupTableItem item : items) {
				newItems.add(item);
			}

			for (String itemExcluded : itemsExcluded) {
				newItems.removeIf(item -> item.getItemKey().equalsIgnoreCase(itemExcluded));
			}

			documentIdField.setDSItems(newItems);
		}
		
		if(uiMode.equals(UIConstants.UIACTION_New)) {
			List<LookupTableItem> items = BrokerUI.getHelperSession().getLookupUIHelper().getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_FileType);
			List<LookupTableItem> newItems = new ArrayList<LookupTableItem>();
			for (LookupTableItem item : items) {
				if(!item.getItemKey().equalsIgnoreCase("TXT")) {
					newItems.add(item);
				}
			}
			
			storageTypeField.setDSItems(newItems);
		}
		
		if(uiMode.equals(UIConstants.UIACTION_Update)) {
			storageTypeField.setReadOnly(true);
		}
		
		setFocus();
	}
	
	public boolean saveEdit(PolicyDocuments document) {
		boolean ok = uiHelper.writeValues(binder, document);
		
		return ok;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}
	
	public MemoryBuffer getMemoryBuffer() {
		return memoryBuffer;
	}
	
	public void setSaveButton(Button addButton) {
		this.saveButton = addButton;
	}
	
	public Button getSaveButton() {
		return this.saveButton;
	}

	public void setFocus() {
		if (documentIdField.isReadOnly()) {
			descriptionField.focus();
		}
		else {
			documentIdField.focus();
		}
	}
	
	public void setBusinessStatus(String businessStatus) {
		this.businessStatus = businessStatus;
	}
	
	public void setTxnBusinessStatus(String txnBusinessStatus) {
		this.txnBusinessStatus = txnBusinessStatus;
	}
	
	public void setPolicyTxnType(String policyTxnType) {
		this.policyTxnType = policyTxnType;
	}
}
