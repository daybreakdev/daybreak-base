package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.HeaderRow.HeaderCell;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class BaseCoverageList extends BaseListLayout<CoverageDto> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1671991658462030356L;
	
	protected final static String COLUMNKEY_DESCRIPTION 	 = "DES";
	protected final static String COLUMNKEY_LIMIT 			 = "LIMIT";
	protected final static String COLUMNKEY_DEDUCTIBLE 		 = "DED";
	protected final static String COLUMNKEY_UWADJUSTMENT 	 = "UWADJUST";
	protected final static String COLUMNKEY_ISOVRPREMIUM	 = "ISOVRPREM";
	protected final static String COLUMNKEY_ISREINSURED	 	 = "ISREINSURED";
	protected final static String COLUMNKEY_UNITPREMIUM 	 = "UPREM";
	protected final static String COLUMNKEY_RATEDPREMIUM 	 = "RPREM";
	protected final static String COLUMNKEY_ANNUALPREMIUM 	 = "APREM";
	protected final static String COLUMNKEY_NETCHANGEPREMIUM = "NCPREM";
	protected final static String COLUMNKEY_WRITTENPREMIUM   = "WPREM";

	protected IBusinessEntity			covParent;
	protected ICoverageConfigParent		covConfigParent;
	protected Grid<CoverageDto>			coverageGrid;
	protected CoverageDto				lastDSCoverageDto;
	protected CoverageDto				lastTotalRowDto;
	protected UIConstants.OBJECTTYPES 	parentObjectType;
	protected BaseCoverageUIHelper		coverageUIHelper;
	
	public BaseCoverageList(String uiMode, UICONTAINERTYPES uiContainerType, UIConstants.OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		this.parentObjectType 		= parentObjectType;
		this.coverageUIHelper 		= CoverageUIFactory.getInstance().createCoverageHelper(parentObjectType);
		initializeLayouts(labels.getString("CoverageListTitle"));
		buildGrid();
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		if (!UICONTAINERTYPES.Information.equals(uiContainerType)) {
			DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
															title,
															UIConstants.UICOMPONENTTYPES.List,
															getCoverageActions()
															);
			this.toolbar = headerComponents.getToolbar(); 
		}
		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupOpenButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	protected String[] getCoverageActions() {
		return new String[] {
			UIConstants.UIACTION_New, UIConstants.UIACTION_Update, UIConstants.UIACTION_Open, UIConstants.UIACTION_Delete, UIConstants.UIACTION_View
		};
	}
	
	protected void buildGrid() {
		coverageGrid = new Grid<CoverageDto>();
		uiHelper.setGridStyle(coverageGrid);
		coverageGrid.setSizeFull();
		/** sample code to show details
		coverageGrid.addColumn(new ComponentRenderer<Component, DSCoverageDto>(o -> {
			if (DSUIConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(o.getCoverageType()) ||
				DSUIConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(o.getCoverageType())) {
				Icon icon = new Icon(VaadinIcon.CIRCLE);
				icon.addClickListener(e -> {
					coverageGrid.setDetailsVisible(o, !coverageGrid.isDetailsVisible(o));	
				});
				return icon;
			}
			
			return new Div();
		}));
		**/
		
		createIndicatorColumn(coverageGrid);

		buildGridDataColumns();
		
		coverageGrid.setSelectionMode(SelectionMode.SINGLE);
		coverageGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		coverageGrid.setDetailsVisibleOnClick(false);
		coverageGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(coverageGrid);
		coverageGrid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
				return setupRowLine(o);
		}));
		if (UICONTAINERTYPES.Information.equals(uiContainerType)) {
			coverageGrid.setHeightByRows(true);
			coverageGrid.setWidthFull();
		}
		
		HeaderRow hRow = coverageGrid.prependHeaderRow();
		buildGridHeaderPremiumColumns(hRow);
		
		this.add(coverageGrid);
	}
	
	protected void buildGridDataColumns() {
		coverageGrid.addColumn(CoverageDto::getDescription)
								.setHeader(labels.getString("CoverageListCoverageTypeLabel"))
								.setWidth("28%")
								.setKey(COLUMNKEY_DESCRIPTION);
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getLimit1, DSNumericFormat.getIntegerInstance()))
								.setHeader(labels.getString("CoverageListCoverageLimitLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_LIMIT);
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDeductible1, DSNumericFormat.getIntegerInstance()))
								.setHeader(labels.getString("CoverageListCoverageDedLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_DEDUCTIBLE);
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispUwAdjustment, DSNumericFormat.getUWAdjustmentInstance()))
								.setHeader(labels.getString("CoverageListUWAdjustmentShort2Label"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_UWADJUSTMENT)
								;
		coverageGrid.addComponentColumn(o -> createColumnIsPremiumOverride(o))
								.setHeader(labels.getString("CoverageListIsPremiumOverrideShort2Label"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_ISOVRPREMIUM)
								;
		coverageGrid.addComponentColumn(o -> createColumnIsReinsured(o))
								.setHeader(labels.getString("CoverageListIsReinsured"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_ISREINSURED)
								.setFlexGrow(0)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispUnitPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_UNITPREMIUM)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispOriginalPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_RATEDPREMIUM)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispAnnualPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_ANNUALPREMIUM)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispNetPremiumChange, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListAmountShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_NETCHANGEPREMIUM)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispWrittenPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_WRITTENPREMIUM)
								;
		
		coverageGrid.getColumnByKey(COLUMNKEY_UNITPREMIUM).setVisible(false);
	}
	
	@Override
	protected Component createIndicator(Object object) {
		if (isIndicatorColumnRequired()) {
			CoverageDto dto = (CoverageDto) object;
			if (dto.isNewCoverage()) { 
				return uiHelper.createIconNew();
			}
			else if (dto.isDeletedCoverage() || dto.wasDeletedCoverage()) {
				return uiHelper.createIconDeleted();
			}
			else if (dto.isModifiedCoverage() || dto.anyReinsuranceChanges()) {
				return uiHelper.createIconModified();
			}
		}
		
		/*
		else if (dto.isEditDeleted() != null && dto.isEditDeleted()) { 
			Icon icon = new Icon(VaadinIcon.CLOSE_SMALL);
			icon.setSize("15px");
			icon.setColor("red");
			return icon;
		}
		*/
		return new Div();
	}

	protected Checkbox createColumnIsPremiumOverride(CoverageDto dto) {
		Checkbox checkbox = new Checkbox();
		if (dto.getCoveragePK() != null) {
			checkbox.setValue(dto.getDispIsPremiumOverride() != null && dto.getDispIsPremiumOverride());
			checkbox.setEnabled(false);
		}
		else {
			checkbox.setVisible(false);
		}
		return checkbox;
	}

	protected Checkbox createColumnIsReinsured(CoverageDto dto) {
		Checkbox checkbox = new Checkbox();
		if (dto.getCoveragePK() != null) {
			checkbox.setValue(dto.getDispIsReinsured() != null && dto.getDispIsReinsured());
			checkbox.setEnabled(false);
		}
		else {
			checkbox.setVisible(false);
		}
		return checkbox;
	}
	
	protected void buildGridHeaderPremiumColumns(HeaderRow hRow) {
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_UWADJUSTMENT, 		labels.getString("CoverageListUWAdjustmentShort1Label"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_ISOVRPREMIUM,   	labels.getString("CoverageListIsPremiumOverrideShort1Label"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_UNITPREMIUM,   	labels.getString("CoverageListUnitPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_RATEDPREMIUM,   	labels.getString("CoverageListRatedPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_ANNUALPREMIUM,   	labels.getString("CoverageListAnnualPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_NETCHANGEPREMIUM,  labels.getString("CoverageListNetChangePremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_WRITTENPREMIUM, 	labels.getString("CoverageListWrittenPremiumShortLabel"));
	}
	
	protected void buildGridHeaderPremiumColumn(HeaderRow hRow, String colKey, String colLabel) {
		HeaderCell hCell = hRow.getCell(coverageGrid.getColumnByKey(colKey));
		VerticalLayout hLayoput = new VerticalLayout();
		uiHelper.formatFullLayout(hLayoput);
		hLayoput.setDefaultHorizontalComponentAlignment(Alignment.END);
		Label hLabel = new Label(colLabel);
		hLayoput.add(hLabel);
		hCell.setComponent(hLayoput);
	}

	protected void setupNewButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				coverageUIHelper.pickCoverageAction(covConfigParent, covParent, this);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!coverageGrid.getSelectedItems().isEmpty()) {
					coverageUIHelper.updateCoverageAction(covConfigParent, covParent, coverageGrid.getSelectedItems().iterator().next(), null);
				}
			});
		}
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!coverageGrid.getSelectedItems().isEmpty()) {
					coverageUIHelper.viewCoverageAction(covConfigParent, covParent, coverageGrid.getSelectedItems().iterator().next(), null);
				}
			});
		}
	}
	
	protected void setupOpenButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_Open) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Open).addClickListener(event -> {
				coverageUIHelper.editCoverageGridAction(covConfigParent, covParent, getCurrentCoverageList());
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!coverageGrid.getSelectedItems().isEmpty()) {
					coverageUIHelper.deleteCoverageAction(covConfigParent, covParent, coverageGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar != null &&
			this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (!coverageGrid.getSelectedItems().isEmpty()) {
					coverageUIHelper.undeleteCoverageAction(covConfigParent, covParent, coverageGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	public List<CoverageDto> getCurrentCoverageList() {
		List<CoverageDto> results = new ArrayList<CoverageDto>();
		
		List<CoverageDto> list = (List<CoverageDto>) ((ListDataProvider<CoverageDto>)this.coverageGrid.getDataProvider()).getItems();
		if (list != null) {
			results = list.stream().filter(o -> !o.isCustomRow()).collect(Collectors.toList());
		}

		return results;
	}
	
	public ICoverageConfigParent getCoverageConfigParent() {
		return covConfigParent;
	}
	
	public boolean loadData(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverags) {
		// TODO Auto-generated method stub
		this.covConfigParent   = covConfigParent;
		this.covParent 		   = covParent;
		this.lastDSCoverageDto = null;
		
		initializeReinsurance();
				
		List<CoverageDto> coverageDtos = new ArrayList<CoverageDto>();
		if (coverags != null && !coverags.isEmpty()) {
			coverageDtos.addAll(coverags);
			coverageDtos = setupAllRows(coverageDtos);
			
			boolean colIndVisible = false;
			if (isIndicatorColumnRequired()) {
				colIndVisible = coverageDtos.stream().filter(o -> o.anyCoverageChanges()).findFirst().isPresent();
			}
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setVisible(colIndVisible);
			
		}
		this.coverageGrid.setItems(coverageDtos);

		refreshToolbar();
		
		return true;
	}
	
	protected List<CoverageDto> setupAllRows(List<CoverageDto> coverageDtos) {
		coverageDtos = setupGridRows(coverageDtos);
		coverageDtos = setupTotalRows(coverageDtos, coverageUIHelper.createTotalPremiumDtos(covParent));
		
		return coverageDtos;
	}
	
	protected List<CoverageDto> setupGridRows(List<CoverageDto> coverageDtos) {
		coverageDtos = coverageDtos.stream().sorted(Comparator.comparing(CoverageDto::getCoverageType)
						.thenComparing(Comparator.comparing(CoverageDto::getCoverageOrder))
							).collect(Collectors.toList());
		lastDSCoverageDto = null;
		if (!coverageDtos.isEmpty()) {
			lastDSCoverageDto = coverageDtos.get(coverageDtos.size() - 1);
			coverageGrid.setDetailsVisible(lastDSCoverageDto, true);
		}
		
		return coverageDtos;
	}
	
	// Use last row details
	protected Component setupRowLine(CoverageDto dto) {
		Component line = null;
		
		if (isLastCoverageRow(dto)) {
			line = setupEndOfCoverageListSection(dto);
		}
		if (line == null) {
			if (isLastTotalRow(dto)) {
				line = setupEndOfTotalListSection(dto);
			}
		}
		
		if (line == null) {
			line = new Div();
		}
		
		return line;
	}
	
	// add a line under the last coverage row
	protected Component setupEndOfCoverageListSection(CoverageDto dto) {
		Component result = addRowLine(dto);
		return result;
	}

	// No need for default
	protected Component setupEndOfTotalListSection(CoverageDto dto) {
		Component result = null; //addRowLine(dto);
		return result;
	}
	
	protected Component addRowLine(CoverageDto dto) {
		return uiHelper.addGridRowLineBottom(dto != null);
	}
	
	protected boolean isLastCoverageRow(CoverageDto dto) {
		return 	dto != null &&
				lastDSCoverageDto != null && 
				lastDSCoverageDto.equals(dto);
	}
	
	protected boolean isLastTotalRow(CoverageDto dto) {
		return 	dto != null &&
				lastTotalRowDto != null && 
				lastTotalRowDto.equals(dto);
	}
	
	protected List<CoverageDto> setupTotalRows(List<CoverageDto> coverageDtos, List<TotalPremiumDto> totalPremiumDtos) {
		lastTotalRowDto = null;
		
		if (coverageDtos != null && !coverageDtos.isEmpty() && totalPremiumDtos != null && !totalPremiumDtos.isEmpty()) {
			totalPremiumDtos.sort(Comparator.comparing(TotalPremiumDto::getSeq));
			totalPremiumDtos.stream().forEach(o -> {
						if (o.getTotalLabel() != null) {
							lastTotalRowDto = addTotalRow(coverageDtos, o.getTotalLabel(), o.getTotalPremium());
						}
					});
			coverageGrid.setDetailsVisible(lastTotalRowDto, true);
		}
		
		return coverageDtos;
	}

	protected CoverageDto addTotalRow(List<CoverageDto> coverageDtos, String totalLabel, Double totalValue) {
		CoverageConfig cc = new CoverageConfig();
		cc.setAllowPremiumOverride(true);
		
		Coverage cov = new Coverage();
		cov.setCoveragePremium(new Premium());
		cov.setDescription(totalLabel);
		
		CoverageDto dto = new CoverageDto(cc, cov, covParent);
		dto.setCustomRow(true);
		
		setTotalLineAmount(dto, cov, totalValue);
		
		coverageDtos.add(dto);
		
		return dto;
	}
	
	// Total amount showed on the last column
	protected void setTotalLineAmount(CoverageDto dto, Coverage cov, Double totalValue) {
		cov.getCoveragePremium().setWrittenPremium(totalValue);
	}
	
	@Override
	protected void refreshToolbar() {
		if (this.toolbar != null) {
			//coverageGrid.setDetailsVisibleOnClick(false);
			
			if (covConfigParent == null) {
				toolbar.disableAllButtons();
			}
			else {
				toolbar.refreshButtons(covParent, coverageGrid);
		
				CoverageDto selected = null;
				if (!coverageGrid.getSelectedItems().isEmpty()) {
					selected = coverageGrid.getSelectedItems().iterator().next();
				}
		
				Button deleteBtn   = toolbar.getButton(UIConstants.UIACTION_Delete);
				Button undeleteBtn = toolbar.getButton(UIConstants.UIACTION_Undelete);
				if (selected != null) {
					// Total Rows
					if (selected.getCoveragePK() == null) {
						if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
							toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
						}
						if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
							toolbar.getButton(UIConstants.UIACTION_View).setEnabled(false);
						}
						if (deleteBtn != null) {
							deleteBtn.setEnabled(false);
						}
						if (undeleteBtn != null) {
							undeleteBtn.setEnabled(false);
						}
					}
					// New is enabled = allowed to edit
					else if (toolbar.getButton(UIConstants.UIACTION_New) != null && toolbar.getButton(UIConstants.UIACTION_New).isEnabled()) { 
						if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
							toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(coverageUIHelper.canCoverageListUpdate(covConfigParent, selected));
						}
						if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
							toolbar.getButton(UIConstants.UIACTION_View).setEnabled(coverageUIHelper.canCoverageView(covConfigParent, selected));
						}
						
						if (deleteBtn != null) {
							deleteBtn.setEnabled(coverageUIHelper.canCoverageDelete(covConfigParent, selected));
						}
						if (undeleteBtn != null) {
							undeleteBtn.setEnabled(coverageUIHelper.canCoverageUndelete(covConfigParent, selected));
						}
						
						if (undeleteBtn.isEnabled()) {
							deleteBtn.setVisible(false);
							undeleteBtn.setVisible(true);
						}
						else {
							undeleteBtn.setVisible(false);
							deleteBtn.setVisible(true);
						}
					}
					// e.g. parent is deleted
					else {
						
					}
					
					// Total lines
					/*
					if (!DSUIConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(selected.getCoverageType()) &&
						!DSUIConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(selected.getCoverageType())) {
						if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
							toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
						}
						if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
							toolbar.getButton(UIConstants.UIACTION_View).setEnabled(false);
						}
						if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
							toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
						}
					}
					else {
						if (!selected.isDetailRequired()) {
							if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
								toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
							}
							if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
								toolbar.getButton(UIConstants.UIACTION_View).setEnabled(false);
							}
						}
						
						ICoverageConfig covConfig = covConfigParent.getCoverageConfig(selected.getCoverageType(), selected.getCoverageCode());
						if (toolbar.getButton(UIConstants.UIACTION_Delete) != null && toolbar.getButton(UIConstants.UIACTION_Delete).isEnabled()) {
							toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(!(covConfig != null && covConfig.isMandatory()));
						}
					}
					*/
				}
				else if (undeleteBtn != null) {
					undeleteBtn.setVisible(false);
					undeleteBtn.setEnabled(false);
				}
			}
			
			//if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				//toolbar.getButton(UIConstants.UIACTION_Update).setVisible(false); 
			//}
			
			if (toolbar.getButton(UIConstants.UIACTION_Open) != null) {
				toolbar.getButton(UIConstants.UIACTION_Open).setVisible(
						toolbar.getButton(UIConstants.UIACTION_New) != null && toolbar.getButton(UIConstants.UIACTION_New).isVisible());
				toolbar.getButton(UIConstants.UIACTION_Open).setEnabled(
						toolbar.getButton(UIConstants.UIACTION_New) != null && toolbar.getButton(UIConstants.UIACTION_New).isEnabled());
			}
		}
	}
	
	protected void initializeReinsurance() {
		if (uiConfigHelper.isCurrentAllowProductReinsurance()) {
			this.coverageGrid.getColumnByKey(COLUMNKEY_ISREINSURED).setVisible(true);
		}
		else {
			this.coverageGrid.getColumnByKey(COLUMNKEY_ISREINSURED).setVisible(false);
		}
	}
}
