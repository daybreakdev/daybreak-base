package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;

public class GarageSubPolicyDialog extends DSUpdateDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4195330192295623054L;
	private GarageSubPolicyForm 	garageSubPolicyForm;

	public GarageSubPolicyDialog (String title, String action) {
		super();
		this.garageSubPolicyForm = new GarageSubPolicyForm(action, UICONTAINERTYPES.Dialog);
		initDialog(title, garageSubPolicyForm, action);
	}

	public void loadData(GarageSubPolicy<?> subPolicy) {
		garageSubPolicyForm.loadData(subPolicy);
		garageSubPolicyForm.enableEdit();
	}

	public boolean validate() {
		return garageSubPolicyForm.validate();
	}

	public boolean save(GarageSubPolicy<?> subPolicy) {
		return garageSubPolicyForm.save(subPolicy);
	}

	
	
}
