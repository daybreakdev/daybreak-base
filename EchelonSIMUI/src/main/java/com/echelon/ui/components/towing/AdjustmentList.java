package com.echelon.ui.components.towing;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.towing.dto.AdjustmentDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.policy.towing.AdjustmentCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.AdjustmentLayoutUIHelper;
import com.echelon.ui.helpers.policy.towing.AdjustmentUIHelper;
import com.ds.ins.uicommon.components.DSBaseActionToolbar;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;

public abstract class AdjustmentList extends BaseListLayout<AdjustmentDto> {
	private static final long serialVersionUID = 2923613916497507933L;

	private final AdjustmentUIHelper adjustmentUIHelper = new AdjustmentUIHelper();
	private Grid<AdjustmentDto> grid;
	private OBJECTTYPES parentObjectType;

	protected AdjustmentCoverageUIHelper adjCoverageUIHelper;
	protected SpecialtyVehicleRisk parentVehicle;

	public AdjustmentList(String uiMode, UICONTAINERTYPES uiContainerType, OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType);
		this.parentObjectType = parentObjectType;
		adjCoverageUIHelper = new AdjustmentCoverageUIHelper(OBJECTTYPES.Vehicle, POLICYTREENODES.Vehicle);
		uiLayoutHelper = new AdjustmentLayoutUIHelper();
		buildGrid();
		initializeLayouts(labels.getString("AdjustmentListTitle"));
		setupToolbarButtonEvents();
	}

	public boolean loadData(List<SpecialtyVehicleRiskAdjustment> adjustments) {
		List<AdjustmentDto> list = new ArrayList<AdjustmentDto>();
		if (adjustments != null) {
			for(SpecialtyVehicleRiskAdjustment adj : adjustments) {
				list.add(new AdjustmentDto(adj));
			}
		}
		
		grid.setItems(list);
		refreshToolbar();
		return true;
	}

	@Override
	protected void refreshToolbar() {
		if (toolbar != null) {
			toolbar.refreshButtons(parentVehicle, grid);
		}
	}

	private void initializeLayouts(String title) {
		this.setSizeFull();

		final DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, title, UICOMPONENTTYPES.List,
				UIConstants.UIACTION_New, UIConstants.UIACTION_Update, UIConstants.UIACTION_Delete);
		this.toolbar = headerComponents.getToolbar();

		this.add(grid);
	}

	private void buildGrid() {
		grid = new Grid<AdjustmentDto>();
		uiHelper.setGridStyle(grid);
		grid.setSizeFull();
		createIndicatorColumn(grid);

		/*
		grid.addColumn(new NumberRenderer<>(dto -> dto.getAdjustment().getAdjustmentNum(),
				DSNumericFormat.getIntegerInstance()))
			//.setHeader(labels.getString("AdjustmentVehicleNumberLabel"));
			.setHeader("");
			*/
		grid.addColumn(dto -> dto.getAdjustment().getAdjustmentDate())
			.setHeader(labels.getString("AdjustmentAdjustmentDateLabel"));
		grid.addColumn(new NumberRenderer<>(dto -> adjustmentUIHelper.getPrevAdjustmentNumOfUnits(dto.getAdjustment()),
				DSNumericFormat.getIntegerInstance()))
			.setHeader(labels.getString("AdjustmentNumberOfUnitsLabel"));
		grid.addColumn(dto -> dto.getAdjustment().getVehicleDescription())
			.setHeader(labels.getString("AdjustmentVehicleDescriptionLabel"));
		grid.addColumn(new NumberRenderer<>(dto -> dto.getAdjustment().getNumOfUnits(),
				DSNumericFormat.getIntegerInstance()))
			.setHeader(labels.getString("AdjustmentAdjustedUnitsLabel"));
		grid.addColumn(new NumberRenderer<>(dto -> adjustmentUIHelper.getOriginatingAdjustmentUnitRate(dto.getAdjustment()),
				NumberFormat.getCurrencyInstance()))
			.setTextAlign(ColumnTextAlign.END)
			.setHeader(labels.getString("AdjustmentRatePerUnitLabel"));
		grid.addColumn(new NumberRenderer<>(dto -> dto.getAdjustment().getProRataFactor(),
				DSNumericFormat.getAmountInstance(3)))
			.setTextAlign(ColumnTextAlign.END)
			.setHeader(labels.getString("AdjustmentProRataFactorLabel"));
		grid.addColumn(new NumberRenderer<>(dto -> dto.getAdjustment().getPremiumChange(),
				NumberFormat.getCurrencyInstance()))
			.setTextAlign(ColumnTextAlign.END)
			.setHeader(labels.getString("AdjustmentPremiumChangeLabel"));

		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.setDetailsVisibleOnClick(false);
		setUpGridDbClickEvent(grid);
		grid.addSelectionListener(event -> refreshToolbar());
		grid.getColumns().stream().forEach(col -> col.setResizable(true));
		
		grid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
			return showRowDetails(o);
		}));
	}

	@Override
	protected Component createIndicator(Object object) {
		// TODO Auto-generated method stub
		Component indComp = super.createIndicator(object);
		Div div = (Div) indComp;
		
		HorizontalLayout inds = new HorizontalLayout();
		uiHelper.formatFullLayout(inds);
		
		AdjustmentDto dto = (AdjustmentDto) object;
		Icon iconDetl = createIconDetl(dto);

		inds.add(div, iconDetl);
		
		return inds;
	}
	
	protected Icon createIconDetl(AdjustmentDto dto) {
		Icon icon = new Icon(VaadinIcon.BULLETS);
		icon.setSize("15px");
		//icon.setColor("red");		
		
		icon.addClickListener(e -> {
			if (dto.isVisible()) {
				dto.setVisible(false);
				grid.setDetailsVisible(dto, false);
			}
			else {
				dto.setVisible(true);
				grid.setDetailsVisible(dto, true);
			}
		});
		
		return icon;
	}

	protected void onNewButtonClick(ClickEvent<Button> event) {
		adjustmentUIHelper.newAction(parentVehicle);
	}

	protected void onUpdateButtonClick(ClickEvent<Button> event) {
		final Optional<AdjustmentDto> selected = getSelected();
		selected.ifPresent(selection -> adjustmentUIHelper.updateAction(parentVehicle, selection.getAdjustment()));
	}

	protected void onDeleteButtonClick(ClickEvent<Button> event) {
		final Optional<AdjustmentDto> selected = getSelected();
		selected.ifPresent(selection -> adjustmentUIHelper.deleteAction(parentVehicle, selection.getAdjustment()));
	}

	protected void onUndeleteButtonClick(ClickEvent<Button> event) {
		final Optional<AdjustmentDto> selected = getSelected();
		selected.ifPresent(selection -> adjustmentUIHelper.undeleteAction(parentVehicle, selection.getAdjustment()));
	}

	private void setupToolbarButtonEvents() {
		final Optional<DSBaseActionToolbar> toolbar = Optional.ofNullable(this.toolbar);

		toolbar.map(t -> t.getButton(UIConstants.UIACTION_New))
			.ifPresent(button -> button.addClickListener(this::onNewButtonClick));
		toolbar.map(t -> t.getButton(UIConstants.UIACTION_Update))
			.ifPresent(button -> button.addClickListener(this::onUpdateButtonClick));
		toolbar.map(t -> t.getButton(UIConstants.UIACTION_Delete))
			.ifPresent(button -> button.addClickListener(this::onDeleteButtonClick));
		toolbar.map(t -> t.getButton(UIConstants.UIACTION_Undelete))
			.ifPresent(button -> button.addClickListener(this::onUndeleteButtonClick));
	}

	private Optional<AdjustmentDto> getSelected() {
		return grid.asSingleSelect().getOptionalValue();
	}
	
	private Component showRowDetails(AdjustmentDto dto) {
		if (dto.getCovListContainer() == null) {
			HorizontalLayout layout =  new HorizontalLayout();
			layout.setSizeFull();
			layout.setPadding(true);
			uiHelper.setBorder(layout);
			layout.getElement().getStyle().set("background-color","white");
			dto.setCovListContainer(layout);
		}
		
		if (dto.getCoverageList() == null) {
			AdjustmentCoverageList adjCoverageList = new AdjustmentCoverageList(uiMode, UICONTAINERTYPES.Information, OBJECTTYPES.Adjustment);
			loadCoverageList(adjCoverageList, dto);
			dto.setCoverageList(adjCoverageList);
			dto.getCovListContainer().add(adjCoverageList);
		}	
		
		return dto.getCovListContainer();
	}
	
	protected void loadCoverageList(AdjustmentCoverageList adjCoverageList, AdjustmentDto dto) {
				
	}
}
