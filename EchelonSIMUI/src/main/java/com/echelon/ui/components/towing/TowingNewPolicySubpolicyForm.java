package com.echelon.ui.components.towing;

import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.policy.INewPolicySubpolicyForm;
import com.echelon.ui.components.policy.NewPolicySubpolicyForm;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;

public class TowingNewPolicySubpolicyForm extends NewPolicySubpolicyForm implements INewPolicySubpolicyForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7630243891086455331L;

	public TowingNewPolicySubpolicyForm(String uiMode, UICONTAINERTYPES uiContainerType, NewPolicyDto newPolicyDto) {
		super(uiMode, uiContainerType, newPolicyDto);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForm();
	}

	private void buildFields() {
		
	}
	
	private void buildForm() {
		FormLayout form = new FormLayout();
		uiHelper.formatFullLayout(form);
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		
		form.add(newPolicyDto.getHasAutoPolicyField(), newPolicyDto.getHasMotorTruckCargoField());
		form.add(newPolicyDto.getHasCommGeneralLiabilityField(), newPolicyDto.getHasGarageField());
		form.add(newPolicyDto.getHasCommercialPropertyField());
		
		this.add(form);
	}
	
	public boolean loadData(NewPolicyDto newPolicyDto) {
		return true;
	}
	
	public boolean enableEdit() {
		newPolicyDto.getHasAutoPolicyField().setReadOnly(true);
		return true;
	}
	
	public boolean saveEdit(NewPolicyDto newPolicyDto) {
		boolean ok = uiHelper.writeValues(newPolicyDto.getBinder(), newPolicyDto);
		return ok;
	}
}
