package com.echelon.ui.components.entities;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.SubPolicy;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.icon.Icon;

public class AdditionalInsuredList extends BaseListLayout<AdditionalInsured> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9212350144941952254L;
	
	private static final String COLUMN_RELATIONSHIP = "RELATIONSHIP";
	
	private Grid<AdditionalInsured> 		addInsuredGrid;
	private AdditionalInsuredUIHelper		addInsuredUIHelper;
	private SubPolicy 						parentSubPolicy;
	
	private	ComponentEventListener<SelectionChangeEvent> selectionChangeListener;
	
    /**
     * @DomEvent is sent when the user clicks button
     */
    @DomEvent("change")
    public static class SelectionChangeEvent extends ComponentEvent<AdditionalInsuredList> {
        public SelectionChangeEvent(AdditionalInsuredList source, boolean fromClient) {
            super(source, fromClient);
        }
    }

	public AdditionalInsuredList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		this.addInsuredUIHelper = new AdditionalInsuredUIHelper();
		initializeLayouts(labels.getString("AdditionalInsuredListLabel"));
		buildGrid();
	}
	
	public void setSelectionChangeListener(ComponentEventListener<SelectionChangeEvent> selectionChangeListener) {
		this.selectionChangeListener = selectionChangeListener;
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														title,
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar(); 
		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	protected void buildGrid() {
		addInsuredGrid = new Grid<AdditionalInsured>();
		uiHelper.setGridStyle(addInsuredGrid);
		addInsuredGrid.setSizeFull();
		createIndicatorColumn(addInsuredGrid);
		addInsuredGrid.addColumn(AdditionalInsured::getAdditionalInsuredName).setHeader(labels.getString("AdditionalInsuredListNameLabel"));
		addInsuredGrid.addColumn(o -> o.getAdditionalInsuredAddress().getAddressLine1()).setHeader(labels.getString("AdditionalInsuredListAddressLabel"));
		addInsuredGrid.addColumn(o -> o.getAdditionalInsuredAddress().getCity()).setHeader(labels.getString("AdditionalInsuredListCityLabel"));
		addInsuredGrid.addColumn(new DSLookupItemRenderer<AdditionalInsured>(o -> o.getAdditionalInsuredAddress().getProvState(),
									uiLookupHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_Jurisdictions)))
								.setHeader(labels.getString("AdditionalInsuredListProvinceLabel"));
		addInsuredGrid.addColumn(o -> o.getAdditionalInsuredAddress().getPostalZip()).setHeader(labels.getString("AdditionalInsuredListPostalCodeLabel"));
		addInsuredGrid.addColumn(AdditionalInsured::getRelationship).setHeader(labels.getString("AdditionalInsuredListRelationshipLabel"))
						.setKey(COLUMN_RELATIONSHIP);
		
		addInsuredGrid.setDetailsVisibleOnClick(false);
		addInsuredGrid.setSelectionMode(SelectionMode.SINGLE);
		addInsuredGrid.addSelectionListener(event -> selectionChanged());
		addInsuredGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		setUpGridDbClickEvent(addInsuredGrid);
		
		this.add(addInsuredGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				addInsuredUIHelper.newAction(parentSubPolicy);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (getCurrentAdditionalInsured() != null) {
					addInsuredUIHelper.updateAction(parentSubPolicy, getCurrentAdditionalInsured());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (getCurrentAdditionalInsured() != null) {
					addInsuredUIHelper.deleteAction(parentSubPolicy, getCurrentAdditionalInsured());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (getCurrentAdditionalInsured() != null) {
					addInsuredUIHelper.undeleteAction(parentSubPolicy, getCurrentAdditionalInsured());
				}
			});
		}
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (getCurrentAdditionalInsured() != null) {
					addInsuredUIHelper.viewAction(parentSubPolicy, getCurrentAdditionalInsured());
				}
			});
		}
	}
	
	public boolean loadData(SubPolicy subPolicy) {
		// TODO Auto-generated method stub

		this.addInsuredGrid.getColumnByKey(COLUMN_RELATIONSHIP).setVisible(false);
		
		this.parentSubPolicy = subPolicy;
		List<AdditionalInsured> list = new ArrayList<AdditionalInsured>();
		if (subPolicy != null) {
			list.addAll(subPolicy.getAdditionalInsureds());
		}
		
		this.addInsuredGrid.setItems(list);
		if (!list.isEmpty()) {
			
			if (BrokerUI.getUserSession().getCurrentPolicy().getLastData() != null && 
				BrokerUI.getUserSession().getCurrentPolicy().getLastData() instanceof AdditionalInsured) {
				AdditionalInsured additionalInsured = (AdditionalInsured) BrokerUI.getUserSession().getCurrentPolicy().getLastData();
				preSelectRow(addInsuredGrid, additionalInsured, (additionalInsured.getAdditionalInsuredPK() == null));
				BrokerUI.getUserSession().getCurrentPolicy().resetLastData();
			}
			else {
				this.addInsuredGrid.select(list.get(0));
			}
		}

		selectionChanged();
		
		return true;
	}
	
	protected void selectionChanged() {
		refreshToolbar();
		if (selectionChangeListener != null) {
			selectionChangeListener.onComponentEvent(new SelectionChangeEvent(this, true));
		}
	}

	protected void refreshToolbar() {
		if (parentSubPolicy == null) {
			toolbar.disableAllButtons();
		}
		else {
			toolbar.refreshButtons(parentSubPolicy, addInsuredGrid);
		}
	}

	public SubPolicy getParentSubPolicy() {
		return parentSubPolicy;
	}

	public AdditionalInsured getCurrentAdditionalInsured() {
		if (!addInsuredGrid.getSelectedItems().isEmpty()) {
			return addInsuredGrid.getSelectedItems().iterator().next();			
		}
		
		return null;
	}

	@Override
	protected Icon getColumnIcon(IBusinessEntity busEntity) {
		// TODO Auto-generated method stub
		Icon icon = super.getColumnIcon(busEntity);
		
		if (icon == null && busEntity != null) {
			if (busEntity instanceof AdditionalInsured) {
				AdditionalInsured ai = (AdditionalInsured) busEntity;
				icon = super.getColumnIcon(ai.getAdditionalInsuredAddress());
			}
		}
		
		return icon;
	}
	
	
}
