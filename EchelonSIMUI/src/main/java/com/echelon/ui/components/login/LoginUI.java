package com.echelon.ui.components.login;

import java.util.ResourceBundle;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.echelon.services.ServiceConstants;
import com.echelon.ui.components.ProductVersionInfo;
import com.echelon.ui.helpers.LoginUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.PasswordField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;

@Route("UserLogin")
public class LoginUI extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1843790722579612227L;
	private static final	Logger		log 				= Logger.getLogger(LoginUI.class.getName());
	
	private		VerticalLayout				vLayout;
	private 	TextField					userIDField;
	private		PasswordField				userPasswordField;
	private		IntegerField				brokerNumField;
	private		IntegerField				officeNumField;
	private		ComboBox<LookupTableItem> 	provinceField;
	private		Button 						loginButton;
	private 	Label						productVersionInfoLabel;
	private 	Binder<WkLoginUser>			binder;
	private		ResourceBundle 				labels;
	private		String						userRequiredMessage;
	private		String						pwdRequiredMessage;

	@Inject
	private 	UIHelper					uiHelper;
	@Inject
	private		LoginUIHelper				loginUIHelper;
	
	private class WkLoginUser {
		private String 			userID;
		private String 			userPassword;
		private Integer 		brokerNo;
		private Integer 		officeNo;
		private LookupTableItem	province;
		
		public String getUserID() {
			return userID;
		}
		public void setUserID(String userID) {
			this.userID = userID;
		}
		public String getUserPassword() {
			return userPassword;
		}
		public void setUserPassword(String userPassword) {
			this.userPassword = userPassword;
		}
		public Integer getBrokerNo() {
			return brokerNo;
		}
		public void setBrokerNo(Integer brokerNo) {
			this.brokerNo = brokerNo;
		}
		public Integer getOfficeNo() {
			return officeNo;
		}
		public void setOfficeNo(Integer officeNo) {
			this.officeNo = officeNo;
		}
		public LookupTableItem getProvince() {
			return province;
		}
		public void setProvince(LookupTableItem province) {
			this.province = province;
		}	
	}
	
	public LoginUI() {
		super();
	}
	
	@PostConstruct
	private void postConstruct() {
		BrokerUI.initSession();
		
		this.labels = uiHelper.getCommonLabels();
		this.userRequiredMessage = labels.getString("LoginUserIdRequiredMessage");
		this.pwdRequiredMessage  = labels.getString("LoginPasswordRequiredMessage");
		
		buildLayout();
		buildFields();
		buildProductVersionInfo();
		buildForm();
		configureUI();
		buildBrokerProvinceItems();
		initState();
	}
	
	private void buildLayout() {
		vLayout = new VerticalLayout();
		//uiHelper.formatFullLayout(vLayout);
	}

	private void buildFields() {
		userIDField			= new TextField(labels.getString("LoginUserIdLabel"));
		userPasswordField	= new PasswordField(labels.getString("LoginPasswordLabel"));
		brokerNumField		= new IntegerField(labels.getString("LoginBrokerNumberLabel"));
		officeNumField		= new IntegerField(labels.getString("LoginBrokerOfficeLabel"));
		provinceField		= new ComboBox<LookupTableItem>(labels.getString("LoginBrokerJurisdictionLabel"));
		loginButton 		= new Button(labels.getString("LoginButtonLabel"));

		userIDField.addInputListener(event -> {
			userIDField.setInvalid(false);
			userIDField.setErrorMessage(userRequiredMessage);
		});

		userPasswordField.addInputListener(event -> {
			userPasswordField.setInvalid(false);
			userPasswordField.setErrorMessage(pwdRequiredMessage);
		});
		provinceField.setItemLabelGenerator(LookupTableItem::getItemValue);

		binder = new Binder<WkLoginUser>();
		binder.forField(userIDField).asRequired(userRequiredMessage).bind(WkLoginUser::getUserID, WkLoginUser::setUserID);
		binder.forField(userPasswordField).asRequired(pwdRequiredMessage).bind(WkLoginUser::getUserPassword, WkLoginUser::setUserPassword);
	}
	
	private void buildForm() {
		uiHelper.setBorder(vLayout);
		
		Image dsLogo = uiHelper.createCompanyLogoLarge();
		dsLogo.setWidth("200px");
		dsLogo.setHeight("100px");
		vLayout.add(dsLogo);
		vLayout.setHorizontalComponentAlignment(Alignment.CENTER, dsLogo);
		vLayout.add(userIDField);
		vLayout.add(userPasswordField);
		vLayout.add(brokerNumField);
		vLayout.add(officeNumField);
		vLayout.add(provinceField);
		vLayout.add(loginButton);
		vLayout.add(productVersionInfoLabel);
		vLayout.setHorizontalComponentAlignment(Alignment.START, productVersionInfoLabel);
		
		initLoginButton();
		
		this.add(vLayout);
		this.setHorizontalComponentAlignment(Alignment.CENTER, vLayout);
	}
	
	private void configureUI() {
		setSizeFull();
		
		vLayout.setSizeUndefined();
		vLayout.setWidth("270px");
		vLayout.setSpacing(true);
		vLayout.setMargin(true);
		userIDField.setWidthFull();
		userPasswordField.setWidthFull();
		vLayout.setHorizontalComponentAlignment(Alignment.CENTER, userIDField, userPasswordField, brokerNumField, officeNumField, provinceField, loginButton);
		
		this.setHorizontalComponentAlignment(Alignment.CENTER, vLayout);
		this.setJustifyContentMode(JustifyContentMode.CENTER);
		this.setMargin(true);
		this.brokerNumField.setWidthFull();
		this.officeNumField.setWidthFull();
		this.provinceField.setWidthFull();
	}
	
	private void initState() {
		userIDField.setEnabled(true);
		userPasswordField.setEnabled(true);
		
		brokerNumField.setVisible(false);
		officeNumField.setVisible(false);
		provinceField.setVisible(false);
		
		binder.forField(brokerNumField).bind(WkLoginUser::getBrokerNo, WkLoginUser::setBrokerNo);
		binder.forField(officeNumField).bind(WkLoginUser::getOfficeNo, WkLoginUser::setOfficeNo);
		binder.forField(provinceField).bind(WkLoginUser::getProvince, WkLoginUser::setProvince);
		
		userIDField.focus();
	}
	
	private void loginPassState() {		
		binder.forField(brokerNumField).asRequired("Please enter a valid broker number").bind(WkLoginUser::getBrokerNo, WkLoginUser::setBrokerNo);
		binder.forField(officeNumField).asRequired("Please enter a valid broker office").bind(WkLoginUser::getOfficeNo, WkLoginUser::setOfficeNo);
		binder.forField(provinceField).asRequired().bind(WkLoginUser::getProvince, WkLoginUser::setProvince);
		
		brokerNumField.setVisible(true);
		officeNumField.setVisible(true);
		provinceField.setVisible(true);
		
		brokerNumField.focus();
	}
	
	private void buildProductVersionInfo() {
		try{
			String verLabel = uiHelper.getCommonLabels().getString("LoginProductVersionLabel") + ": " + ProductVersionInfo.getVersionNumber();
			if(BrokerUI.getUserSession().getDeploymentEnvironment() != null && 
			   !BrokerUI.getUserSession().getDeploymentEnvironment().trim().isEmpty()) {
				verLabel = verLabel + BrokerUI.getUserSession().getDeploymentEnvironment();
			}
			
			productVersionInfoLabel = new Label(verLabel);
			productVersionInfoLabel.getElement().getStyle().set("font-size", "12px");
		}
		catch(Exception e){
			uiHelper.notificationError(uiHelper.getCommonLabels().getString("LoginProductVersionLabelError"));
		}
	}
	
	@SuppressWarnings("serial")
	private void initLoginButton() {
		loginButton.addClickShortcut(Key.ENTER);
		loginButton.addClickListener(this::loginButtonClicked);
	}
	
	private void loginButtonClicked(ClickEvent<Button> clickEvent) {
		if (!validate()) {
			return;
		}
		
		/*
		if(brokerNumField.isVisible() && officeNumField.isVisible()){
			loginSucceed();
		}
		else {
			loginPassState();
		}
		*/
		
		loginRequest();
	}
	
	private void buildBrokerProvinceItems() {
		
	}
	
	private boolean validate() {
		binder.validate();
		if (!binder.isValid()) {
			return false;
		}
		
		return true;
	}
	
	private void loginRequest() {
		String provinceCd = (provinceField.getValue() != null
								? provinceField.getValue().getItemKey() : null);
		
		String authenticationResult = loginUIHelper.login(userIDField.getValue(), userPasswordField.getValue(), 
															brokerNumField.getValue(), officeNumField.getValue(), 
															provinceCd);
		if (!ServiceConstants.AUTHENTICATION_SUCCEED.equalsIgnoreCase(authenticationResult)) {
			try {
				String message = labels.getString("Login"+authenticationResult+"Message");
				userIDField.setErrorMessage("");
				userIDField.setInvalid(true);
				userPasswordField.setErrorMessage(message);
				userPasswordField.setInvalid(true);
			} catch (Exception e) {
				uiHelper.notificationException(e);
			}
		}
	}
}
