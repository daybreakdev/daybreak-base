package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSDefaultContentView;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.vaadin.flow.router.Route;

@Route(value="DefaultContentView", layout = BasePolicyView.class)
public class DefaultContentView extends DSDefaultContentView implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7334003411751440032L;

	public DefaultContentView() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
