package com.echelon.ui.components.baseclasses.policy;

import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyExposureRatingUIHelper;
import com.echelon.ui.constants.UIConstants;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyExposureRatingForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2590214314960542942L;
	protected MileageField						expectedMileageField;
	protected DSNumberField<Integer>			numberOfPowerUnitsField;
	protected MileageField						averageKMSField;	
	protected DSNumberField<Double>				claimsFrequencyField;
	protected DSNumberField<Integer>			yearsInBusinessField;
	protected DSNumberField<Double>				dangerousMaterialsField;
	protected DSComboBox<Double>				preventionRatingField;
	protected DSComboBox<Integer>				commercialCreditScoreField;
	protected DSNumberField<Double>				modificationFactorField;
	//protected DSNumberField<Double>				offBalanceFactorField;
	protected DSNumberField<Double>				vehicleZone1Field;
	protected DSNumberField<Double>				vehicleZone2Field;
	protected DSNumberField<Double>				vehicleZone3Field;
	protected DSNumberField<Double>				vehicleZone4Field;
	protected DSNumberField<Double>				vehicleZone5Field;
	protected DSNumberField<Double>				vehicleZone6Field;
	protected DSNumberField<Double>				vehicleZone7Field;
	protected DSNumberField<Double>				vehicleZone8Field;
	protected DSNumberField<Double>				vehicleZone9Field;
	protected DSNumberField<Double>				vehicleZoneTotalField;
	protected List<VehicleZoneFieldTable> 		vehicleZoneFieldList;
	protected Grid<VehicleZoneFieldTable>		vehicleZoneFieldGrid;

	protected ActionToolbar						toolbar;
	
	protected Binder<SpecialityAutoExpRating> 	binder;
	
	private static final int FACTOR_DECIMALS = 4;
	
	private class MileageField extends DSNumberField<Double> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 6396677429972788928L;

		public MileageField(String label) {
			super(label, Double.class);
			// TODO Auto-generated constructor stub
		}

		@Override
		protected void setDoubleFormat() {
			// TODO Auto-generated method stub
			super.setDoubleFormat();
			
			modelPattern = "\\d{0,12}";
		}
		
	}
	
	protected class VehicleZoneFieldTable {
		private int						id;
		private String					fieldLabel;
		private DSNumberField<Double> 	fieldWidget;
		
		public VehicleZoneFieldTable(int id, String fieldLabel, DSNumberField<Double> fieldWidget) {
			super();
			this.id = id;
			this.fieldLabel = fieldLabel;
			this.fieldWidget = fieldWidget;
		}

		public int getId() {
			return id;
		}

		public String getFieldLabel() {
			return fieldLabel;
		}

		public DSNumberField<Double> getFieldWidget() {
			return fieldWidget;
		}
		
		public Double getFieldValue() {
			return this.fieldWidget.getModelValue();
		}
	}

	public BasePolicyExposureRatingForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
		buildFields();
		buildGrid();
		buildForm();
	}
	
	protected BasePolicyExposureRatingUIHelper createPolicyExposureRatingFormUIHelper() {
		return new BasePolicyExposureRatingUIHelper();
	}
	
	protected void initializeLayouts() {
		setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			// Note Update button is not visible; just for setting enabled
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													labels.getString("PolicyExposureRatingFactorsTitle"),
													UIConstants.UICOMPONENTTYPES.Form);
			toolbar = (ActionToolbar) headComponents.getToolbar();
			
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
					createPolicyExposureRatingFormUIHelper().setupPolicyExposureRatingUpdateAction();
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
		
	}

	protected void buildFields() {
		vehicleZoneFieldList = new ArrayList<BasePolicyExposureRatingForm.VehicleZoneFieldTable>();
		
		expectedMileageField 		= new MileageField(labels.getString("PolicyExposureRatingExpectedMileageLabel"));
		numberOfPowerUnitsField 	= new DSNumberField<Integer>(labels.getString("PolicyExposureRatingNumberOfPowerUnitsLabel"), Integer.class);
		averageKMSField 			= new MileageField(labels.getString("PolicyExposureRatingAverageKMSLabel"));
		claimsFrequencyField 		= new DSNumberField<Double>(labels.getString("PolicyExposureRatingClaimsFrequencyLabel"), Double.class);
		yearsInBusinessField 		= new DSNumberField<Integer>(labels.getString("PolicyExposureRatingYearsInBusinessLabel"), Integer.class);
		dangerousMaterialsField 	= new DSNumberField<Double>(labels.getString("PolicyExposureRatingDangerousMaterialsLabel"), Double.class);
		preventionRatingField 		= new DSComboBox<Double>(labels.getString("PolicyExposureRatingPreventionRatingLabel"), Double.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_PreventionRatings);
		preventionRatingField.setAllowCustomValue(true);
		commercialCreditScoreField 	= new DSComboBox<Integer>(labels.getString("PolicyExposureRatingCommercialCreditScoreLabel"), Integer.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_CommCreditScores);
		modificationFactorField 	= new DSNumberField<Double>(labels.getString("PolicyExposureRatingModificationFactorLabel"), Double.class, FACTOR_DECIMALS);
		//offBalanceFactorField 		= new DSNumberField<Double>(labels.getString("PolicyExposureRatingOffBalanceFactorLabel"), Double.class);
		
		vehicleZone1Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(1, labels.getString("PolicyExposureRatingVehicleZone1Label"), vehicleZone1Field));
		vehicleZone2Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(2, labels.getString("PolicyExposureRatingVehicleZone2Label"), vehicleZone2Field));
		vehicleZone3Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(3, labels.getString("PolicyExposureRatingVehicleZone3Label"), vehicleZone3Field));
		vehicleZone4Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(4, labels.getString("PolicyExposureRatingVehicleZone4Label"), vehicleZone4Field));
		vehicleZone5Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(5, labels.getString("PolicyExposureRatingVehicleZone5Label"), vehicleZone5Field));
		vehicleZone6Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(6, labels.getString("PolicyExposureRatingVehicleZone6Label"), vehicleZone6Field));
		vehicleZone7Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(7, labels.getString("PolicyExposureRatingVehicleZone7Label"), vehicleZone7Field));
		vehicleZone8Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(8, labels.getString("PolicyExposureRatingVehicleZone8Label"), vehicleZone8Field));
		vehicleZone9Field 			= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(9, labels.getString("PolicyExposureRatingVehicleZone9Label"), vehicleZone9Field));
		vehicleZoneTotalField		= new DSNumberField<Double>("", Double.class, FACTOR_DECIMALS);
		vehicleZoneFieldList.add(new VehicleZoneFieldTable(999, labels.getString("PolicyExposureRatingVehicleZoneTotalLabel"), vehicleZoneTotalField));
		
		binder = new Binder<SpecialityAutoExpRating>(SpecialityAutoExpRating.class);
		binder.forField(expectedMileageField)
							.withConverter(expectedMileageField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getExpectedMileage, SpecialityAutoExpRating::setExpectedMileage);
		binder.forField(numberOfPowerUnitsField)
							.withConverter(numberOfPowerUnitsField.getConverter())
							.withValidator(o -> o != null && o > 0, labels.getString("PolicyExposureRatingNumberOfPowerUnitsMustBeGE0Message"))
							.withNullRepresentation(0)
							.bind(SpecialityAutoExpRating::getNumberOfPowerUnits, SpecialityAutoExpRating::setNumberOfPowerUnits);
		binder.forField(averageKMSField)
							.withConverter(averageKMSField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getAverageKMS, SpecialityAutoExpRating::setAverageKMS);
		binder.forField(claimsFrequencyField)
							.withConverter(claimsFrequencyField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getClaimsFrequency, SpecialityAutoExpRating::setClaimsFrequency);
		binder.forField(yearsInBusinessField)
							.withConverter(yearsInBusinessField.getConverter())
							.withNullRepresentation(0)
							.bind(SpecialityAutoExpRating::getYearsInBusiness, SpecialityAutoExpRating::setYearsInBusiness);
		binder.forField(dangerousMaterialsField)
							.withConverter(dangerousMaterialsField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getDangerousMaterials, SpecialityAutoExpRating::setDangerousMaterials);
		binder.forField(preventionRatingField)
							.bind(SpecialityAutoExpRating::getPreventionRating, SpecialityAutoExpRating::setPreventionRating);
		binder.forField(commercialCreditScoreField)
							.bind(SpecialityAutoExpRating::getCommercialCreditScore, SpecialityAutoExpRating::setCommercialCreditScore);
		binder.forField(modificationFactorField)
							.withConverter(modificationFactorField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getModificationFactor, SpecialityAutoExpRating::setModificationFactor);
		/*
		binder.forField(offBalanceFactorField)
							.withConverter(offBalanceFactorField.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getOffBalanceFactor, SpecialityAutoExpRating::setOffBalanceFactor);
							*/
		binder.forField(vehicleZone1Field)
							.withConverter(vehicleZone1Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone1Exp, SpecialityAutoExpRating::setZone1Exp);
		binder.forField(vehicleZone1Field)
							.withConverter(vehicleZone1Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone1Exp, SpecialityAutoExpRating::setZone1Exp);
		binder.forField(vehicleZone2Field)
							.withConverter(vehicleZone2Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone2Exp, SpecialityAutoExpRating::setZone2Exp);
		binder.forField(vehicleZone3Field)
							.withConverter(vehicleZone3Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone3Exp, SpecialityAutoExpRating::setZone3Exp);
		binder.forField(vehicleZone4Field)
							.withConverter(vehicleZone4Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone4Exp, SpecialityAutoExpRating::setZone4Exp);
		binder.forField(vehicleZone5Field)
							.withConverter(vehicleZone5Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone5Exp, SpecialityAutoExpRating::setZone5Exp);
		binder.forField(vehicleZone6Field)
							.withConverter(vehicleZone6Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone6Exp, SpecialityAutoExpRating::setZone6Exp);
		binder.forField(vehicleZone7Field)
							.withConverter(vehicleZone7Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone7Exp, SpecialityAutoExpRating::setZone7Exp);
		binder.forField(vehicleZone8Field)
							.withConverter(vehicleZone8Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone8Exp, SpecialityAutoExpRating::setZone8Exp);
		binder.forField(vehicleZone9Field)
							.withConverter(vehicleZone9Field.getConverter())
							.withNullRepresentation(0D)
							.bind(SpecialityAutoExpRating::getZone9Exp, SpecialityAutoExpRating::setZone9Exp);
		binder.setReadOnly(true);
		vehicleZoneTotalField.setReadOnly(true);
	}
	
	protected void buildGrid() {
		vehicleZoneFieldGrid = new Grid<BasePolicyExposureRatingForm.VehicleZoneFieldTable>();
		vehicleZoneFieldGrid.setSizeFull();
		uiHelper.setGridStyle(vehicleZoneFieldGrid);
		
		vehicleZoneFieldGrid.addColumn(VehicleZoneFieldTable::getFieldLabel).setHeader(labels.getString("PolicyExposureRatingZoneFactorsVehicleLabel"));
		vehicleZoneFieldGrid.addComponentColumn(VehicleZoneFieldTable::getFieldWidget).setHeader(labels.getString("PolicyExposureRatingZoneFactorsIRPLabel"));
		
		vehicleZoneFieldGrid.setItems(vehicleZoneFieldList);
		vehicleZoneFieldGrid.setHeightByRows(true);
		vehicleZoneFieldGrid.setSelectionMode(SelectionMode.SINGLE);
		vehicleZoneFieldGrid.getColumns().stream().forEach(o -> o.setResizable(true));
	}
	
	protected void buildForm() {
		FormLayout form1 = new FormLayout();
		form1.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		form1.add(new Label());
		form1.add(expectedMileageField, 2);
		form1.add(numberOfPowerUnitsField, 2);
		form1.add(averageKMSField, 2);
		form1.add(claimsFrequencyField, 2);
		form1.add(yearsInBusinessField, 2);
		form1.add(dangerousMaterialsField, 2);
		form1.add(preventionRatingField);
		uiHelper.addFormEmptyColumn(form1);
		form1.add(commercialCreditScoreField);
		uiHelper.addFormEmptyColumn(form1);
		form1.add(modificationFactorField, 2);
		//form1.add(offBalanceFactorField, 2);
		form1.setWidth("400px");
		
		VerticalLayout zoneFactorLayout = new VerticalLayout();
		zoneFactorLayout.add(vehicleZoneFieldGrid);
		zoneFactorLayout.setWidth("500px");
		
		HorizontalLayout contentLayout = new HorizontalLayout();
		uiHelper.formatFullLayout(contentLayout);
		contentLayout.add(form1);
		contentLayout.add(zoneFactorLayout);
		
		this.add(contentLayout);
	}
	
	public void loadData(SpecialityAutoExpRating specialityAutoExpRating) {
		super.loadDataConfigs(binder);

		if (specialityAutoExpRating != null) {
			binder.readBean(specialityAutoExpRating);
		}
		
		updateVehicleZoneTotalField();
		
		refreshToolbar();
	}
	
	public void enabledEdit() {
		enableDataEdit(binder);
		
		averageKMSField.setReadOnly(true);
		vehicleZoneTotalField.setReadOnly(true);

		for(VehicleZoneFieldTable zoneFieldTable : vehicleZoneFieldList) {
			if (!zoneFieldTable.equals(vehicleZoneTotalField)) {
				zoneFieldTable.getFieldWidget().addValueChangeListener(o -> updateVehicleZoneTotalField());
			}
		}
		
		expectedMileageField.addValueChangeListener(o -> calculateAverageKMS());
		numberOfPowerUnitsField.addValueChangeListener(o -> calculateAverageKMS());

		setFocus();
	}
	
	public boolean validateEdit() {
		calculateAverageKMS();
		updateVehicleZoneTotalField();
		
		boolean ok1 = uiHelper.validateValues(binder);
		
		return ok1 && validateTotalZoneFactors();
	}
	
	private boolean validateTotalZoneFactors() {
		Double tot = vehicleZoneTotalField.getModelValue();
		if (tot == null || tot.doubleValue() != 100) {
			uiHelper.notificationError(labels.getString("PolicyExposureRatingVehicleZoneTotal100PercenMessage"));
			return false;
		}
		
		return true;
	}

	public boolean saveEdit(SpecialityAutoExpRating specialityAutoExpRating) {
		return uiHelper.writeValues(binder, specialityAutoExpRating);
	}
			
	
	protected void setFocus() {
		expectedMileageField.focus();
	}
	
	protected void refreshToolbar() {
		if (toolbar != null) {
			toolbar.refreshButtons(null, uiPolicyHelper.getCurrentPolicyTransaction());
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
	}
	
	private void updateVehicleZoneTotalField() {
		Double tot = vehicleZoneFieldList.stream().filter(o -> o.getFieldValue() != null && !o.getFieldWidget().equals(vehicleZoneTotalField))
											.mapToDouble(VehicleZoneFieldTable::getFieldValue)
											.sum();
		vehicleZoneTotalField.setDSValue(tot);
	}
	
	private void calculateAverageKMS() {
		Double avgKMS = 0D;
		
		if (expectedMileageField.getModelValue() != null && 
			numberOfPowerUnitsField.getModelValue() != null && numberOfPowerUnitsField.getModelValue() != 0) {
			avgKMS = new BigDecimal(expectedMileageField.getModelValue()).divide(new BigDecimal(numberOfPowerUnitsField.getModelValue()), 2).doubleValue();
		}
		
		averageKMSField.setDSValue(avgKMS);
	}

}
