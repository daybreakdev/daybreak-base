package com.echelon.ui.components.baseclasses.validators;

import java.time.LocalDate;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.Validator;
import com.vaadin.flow.data.binder.ValueContext;

public class BasePolicyChangeEffectiveDateValidator implements Validator<LocalDate> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2519123822121411619L;
	private PolicyTransaction<?> policyTransaction;

	public BasePolicyChangeEffectiveDateValidator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PolicyTransaction<?> getPolicyTransaction() {
		return policyTransaction;
	}

	public void setPolicyTransaction(PolicyTransaction<?> policyTransaction) {
		this.policyTransaction = policyTransaction;
	}

	@Override
	public ValidationResult apply(LocalDate value, ValueContext context) {
		boolean isOk = true; 
		if (value != null && policyTransaction != null) {
			// within the term
			if (value.isBefore(policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate()) ||
				value.isAfter(policyTransaction.getPolicyTerm().getTermExpDate().toLocalDate())) {
				isOk = false;
			}
			
			// any opened policy
			if (isOk) {
				
			}
		}
		
		if (isOk) {
			return ValidationResult.ok();
		}
		else {
			UIHelper uiHelper = BrokerUI.getHelperSession().getUiHelper();
			return ValidationResult.error(uiHelper.getSessionLabels().getString("PolicyConfirmFormPolicyChangeEffectiveDateError"));
		}
	}

}
