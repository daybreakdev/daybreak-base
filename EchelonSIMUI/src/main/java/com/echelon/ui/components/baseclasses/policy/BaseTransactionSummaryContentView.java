package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyTransactionUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.UIFactory;
import com.vaadin.flow.router.Route;

@Route(value="TransactionSummaryContentView", layout = BasePolicyView.class)
public class BaseTransactionSummaryContentView extends BaseLayout implements IPolicyViewContent {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6600042796286620261L;
	private BaseTransactionSummaryTotalForm 		summaryTotal;
	private BaseTransactionSummaryDetailForm		summaryDetails;
	private BaseTransactionSummaryReinsuranceForm	summaryReinsurance;
	private BasePolicyTransactionUIHelper			transactionUIHelper;
	
	public static final String SUMMARYWIDTH = "1250px";

	public BaseTransactionSummaryContentView() {
		super();
		// TODO Auto-generated constructor stub
		
		this.transactionUIHelper = new BasePolicyTransactionUIHelper();
		
		initializeLayout();
	}

	public BaseTransactionSummaryContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayout();
	}
	
	protected void initializeLayout() {
		setSizeFull();
				
		summaryTotal 	= new BaseTransactionSummaryTotalForm(UIConstants.UIACTION_View, UICONTAINERTYPES.Information);
		summaryDetails	= UIFactory.getInstance().getTransactionSummaryDetailForm();
		
		if (uiConfigHelper.isCurrentAllowProductReinsurance()) {
			summaryReinsurance	= new BaseTransactionSummaryReinsuranceForm(UIConstants.UIACTION_View, UICONTAINERTYPES.Information);
			this.add(summaryTotal, summaryReinsurance, summaryDetails);
		}
		else {
			this.add(summaryTotal, summaryDetails);
		}
	}

	public void loadData() {
		summaryTotal.loadData();
		if (uiConfigHelper.isCurrentAllowProductReinsurance()) {
			summaryReinsurance.loadData();
		}
		summaryDetails.loadData();
	}
	
	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		
		transactionUIHelper.loadFullPolicytransactionData(new BasePolicyTransactionUIHelper.LoadPolicyTransactionDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData();
			}
		});
	}

}
