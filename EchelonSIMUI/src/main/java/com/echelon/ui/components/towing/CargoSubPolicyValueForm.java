package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.echelon.domain.policy.specialtylines.CVRate;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyValueUIHelper;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class CargoSubPolicyValueForm extends BaseLayout implements IDSFocusAble {
	private static final long serialVersionUID = 1250693053180523222L;
	private DSComboBox<String>  			valueDescriptionField;
	private DSNumberField<Double>			averageField;
	private DSNumberField<Double>			uwManualField;
	private DSNumberField<Double>			rateField;
	private DSNumberField<Double>			exposureField;
	private DSNumberField<Double>			partialPremField;
	private Binder<CargoDetails> 			binder;
	private CargoSubPolicyValueMultiForm	parentForm;
	private boolean							isBuildingValueDescription;
	private CargoSubPolicyValueUIHelper	    cargoSubPolicyValueUIHelper;
	private CargoSubPolicy<?>				parentSubPolicy;
	private FormLayout 						editForm;
	
	public CargoSubPolicyValueForm(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType,
										CargoSubPolicyValueMultiForm parentForm) {
		super(uiMode, uiContainerType);
		
		this.cargoSubPolicyValueUIHelper = new CargoSubPolicyValueUIHelper();
		this.parentForm = parentForm;
		
		buildFields();
		buildForm();
	}

	private void buildFields() {
		labels = uiHelper.getSessionLabels();
		valueDescriptionField = new DSComboBox<String>(labels.getString("CargoSubPolicyValueDescriptionLabel"), String.class);
		averageField = new DSNumberField<Double>(labels.getString("CargoSubPolicyValueAverageLabel"), Double.class, false);
		uwManualField = new DSNumberField<Double>(labels.getString("CargoSubPolicyValueUWManualLabel"), Double.class, false);
		rateField = new DSNumberField<Double>(labels.getString("CargoSubPolicyValueRateLabel"), Double.class);
		exposureField = new DSNumberField<Double>(labels.getString("CargoSubPolicyValueExposureLabel"), DSNumberField.PRESENTFORMATS.Percentage);
		partialPremField = createPartialPremField();
		
		binder = new Binder<CargoDetails>();
		binder.forField(valueDescriptionField).asRequired()
			 .bind(CargoDetails::getCargoDescription, CargoDetails::setCargoDescription);
		binder.forField(averageField)
			 .withNullRepresentation("")
			 .withConverter(averageField.getConverter())
			 .bind(CargoDetails::getAverage, CargoDetails::setAverage);
		binder.forField(uwManualField)
			.withNullRepresentation("")
			.withConverter(uwManualField.getConverter())
			.bind(CargoDetails::getuWManual, CargoDetails::setuWManual);
		binder.forField(rateField)
			.withNullRepresentation("")
		  	.withConverter(rateField.getConverter())
		  	.bind(CargoDetails::getCargoValue, CargoDetails::setCargoValue);
		binder.forField(exposureField).asRequired()
			.withNullRepresentation("")
			.withConverter(exposureField.getConverter())
			.bind(CargoDetails::getCargoExposure, CargoDetails::setCargoExposure);
		binder.forField(partialPremField)
			.withNullRepresentation("")
			.withConverter(partialPremField.getConverter())
			.bind(CargoDetails::getCargoPartialPremiums, CargoDetails::setCargoPartialPremiums);
		binder.setReadOnly(true);
	}
	
	protected DSNumberField<Double> createPartialPremField() {
		return new DSNumberField<Double>(labels.getString("CargoSubPolicyValuePartialPremiumLabel"), Double.class);
	}

	private void buildForm() {
		editForm = new FormLayout();
		editForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 9)
		        );
		editForm.add(valueDescriptionField, 3);
		editForm.add(averageField);
		editForm.add(uwManualField);
		editForm.add(rateField);
		editForm.add(exposureField);
		editForm.add(partialPremField, 2);
		this.add(editForm);
	}
	
	public DSComboBox<String> getValueDescriptionField() {
		return valueDescriptionField;
	}

	public DSNumberField<Double> getExposureField() {
		return exposureField;
	}

	public void refreshValueDescriptionSelection() {
		loadValueDescriptionSelection(valueDescriptionField.getValue());
	}
	
	public void loadValueDescriptionSelection(CargoDetails cargoDetails) {
		loadValueDescriptionSelection(
					cargoDetails != null
					? cargoDetails.getCargoDescription()
					: null
				);
	}
	
	public void loadValueDescriptionSelection(String valueDescription) {
		isBuildingValueDescription = true;
		
		List<LookupTableItem> items = null;
		if (parentForm != null) {
			items = cargoSubPolicyValueUIHelper.getValueDescriptionSelections(valueDescription, parentSubPolicy, parentForm.getAllEntries());
		}
		else {
			items = cargoSubPolicyValueUIHelper.getValueDescriptionSelections(valueDescription, parentSubPolicy, null);
		}
		valueDescriptionField.setDSItems(items);
		
		isBuildingValueDescription = false;
	}
	
	protected void valueDescriptionChanged() {
		if (!isBuildingValueDescription) {
			LookupTableItem item = uiLookupHelper.getCVRateByDesciption(valueDescriptionField.getValue());
			if (item != null && item.getItemObject() != null) {
				CVRate cvRate = (CVRate) item.getItemObject();
				averageField.setDSValue(cvRate.getAvgCV());
				rateField.setDSValue(cvRate.getCvRate());
			}
			
			if (parentForm != null) {
				parentForm.valueDescriptionSelectionChanged();
			}
		}
	}

	public void loadData(CargoSubPolicy<?> parentSubPolicy, CargoDetails cargoDetails) {
		this.parentSubPolicy = parentSubPolicy;
		
		loadDataConfigs(binder);
		loadValueDescriptionSelection(cargoDetails);

		binder.readBean(cargoDetails);
		
		enableDataView(binder);
	}

	public boolean validateEdit(boolean showMessage) {
		return uiHelper.validateValues(showMessage, binder);
	}
	
	public boolean validate() {
		return uiHelper.validateValues(binder);
	}

	public boolean save(CargoDetails cargoDetails) {
		return uiHelper.writeValues(binder, cargoDetails);
	}

	public void enableEdit() {
		enableDataEdit(binder);
		
		valueDescriptionField.addValueChangeListener(event -> valueDescriptionChanged());
		
		if (UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(valueDescriptionField.getValue())) {
			uiHelper.setEditFieldsReadonly(valueDescriptionField, exposureField);
		}
		
		uiHelper.setEditFieldsReadonly(averageField, rateField, partialPremField);
		
		setFocus();
	}
	
	public void setFocus() {
		uiHelper.setFocus(editForm);
	}
}
