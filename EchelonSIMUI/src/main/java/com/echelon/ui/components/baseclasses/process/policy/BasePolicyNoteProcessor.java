package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyNotes;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyNoteProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8404198729492426565L;
	private PolicyNotes policyNotes;

	public BasePolicyNoteProcessor(String processAction, PolicyTransaction policyTransaction, 
										PolicyNotes policyNotes) {
		super(processAction, policyTransaction);
		this.policyNotes = policyNotes;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			createPolicyNote(this.policyTransaction, policyNotes);
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			updatePolicyTransaction();
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			deletePolicyNote(this.policyTransaction, policyNotes);
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}

	public PolicyNotes getPolicyNotes() {
		return policyNotes;
	}

	private void createPolicyNote(PolicyTransaction poTransaction, PolicyNotes note) {
		poTransaction.getPolicyVersion().addPolicyNote(note);
		updatePolicyTransaction();
	}

	private void deletePolicyNote(PolicyTransaction poTransaction, PolicyNotes note) {
		poTransaction.getPolicyVersion().removePolicyNote(note);
		updatePolicyTransaction();
	}
}
