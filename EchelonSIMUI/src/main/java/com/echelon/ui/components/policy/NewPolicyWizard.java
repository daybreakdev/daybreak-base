package com.echelon.ui.components.policy;

import org.apache.commons.lang3.StringUtils;

import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.entities.CommercialCustomerForm;
import com.echelon.ui.components.entities.CustomerForm;
import com.echelon.ui.components.towing.AutoSubPolicyForm;
import com.echelon.ui.components.towing.CGLSubPolicyForm;
import com.echelon.ui.components.towing.CargoSubPolicyForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.helpers.policy.NewPolicyWizardHelper;
import com.echelon.ui.helpers.policy.NewPolicyWizardHelper.NewPolicyWizardCallback;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSWizard;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTEPS;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class NewPolicyWizard extends DSWizard {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6640229519185923290L;
	private NewPolicyForm 				newPolicyUI;
	private VerticalLayout				customerLayout;
	private CustomerForm<?>  			customerUI;
	private AutoSubPolicyForm			autoSubPolicyUI;
	private CGLSubPolicyForm			cglSubPolicyUI;
	private CargoSubPolicyForm 			cargoSubPolicyUI;
	private NewPolicyDto 				newPolicyDto;
	private NewPolicyWizardCallback 	newPolicyWizardCallback;
	private boolean 					isRINnumberIsNotListedConfirmed;
	private NewPolicyWizardHelper		newPolicyWizardHelper;
	
	private static final int PAGE_POLICY   	= 0;
	private static final int PAGE_CUSTOMER 	= 1;
	private static final int PAGE_AUTO     	= 2;
	private static final int PAGE_CGL 	   	= 3;
	private static final int PAGE_CARGO 	= 4;

	public NewPolicyWizard() {
		super();
		
		this.newPolicyWizardHelper = new NewPolicyWizardHelper();
	}

	public void initWizard(NewPolicyDto newPolicyDto, NewPolicyWizardCallback newPolicyWizardCallback) {
		this.newPolicyWizardCallback = newPolicyWizardCallback;
		this.newPolicyDto = newPolicyDto;

		if (newPolicyDto.isRenewal()) {
			super.initWizard(labels.getString("NewRenewalDialogTitle"));
		}
		else if (newPolicyDto.isQuote()) {
			super.initWizard(labels.getString("NewQuoteDialogTitle"));
		}
		else {
			super.initWizard(labels.getString("NewPolicyDialogTitle"));
		}
	}
	
	@Override
	protected String[] getTabLabels() {
		return new String[] {
				(newPolicyDto.isQuote() ? labels.getString("NewPolicyWizardTabQuoteLabel")
							  			: labels.getString("NewPolicyWizardTabPolicyLabel")),
				labels.getString("NewPolicyWizardTabCustomerLabel"),
				labels.getString("NewPolicyWizardTabAutomobilePolicyLabel"),
				labels.getString("NewPolicyWizardTabCommercialGenLiabPolicyLabel"),
				labels.getString("NewPolicyWizardTabMotorTruckCargoPolicyLabel"), };
	}

	@Override
	protected Component[] getTabComponents() {
		// TODO Auto-generated method stub
		return new Component[] { getPolicyForm(), getCustomerForm(), getAutoSubPolicyForm(), getCGLSubPolicyForm(),
				getCargoSubPolicyForm() };
	}
	
	@Override
	protected Component getTabComponent(int page) {
		Component component = super.getTabComponent(page);
		if (component != null) {
			if (component instanceof IDSFocusAble) {
				// If the component is focusable, set focus on it.
				((IDSFocusAble) component).setFocus();
			}
			else if (page == PAGE_CUSTOMER) {
				if (customerUI != null) {
					customerUI.setFocus();
				}
			}
		}
		
		return component;
	}

	public NewPolicyForm getPolicyForm() {
		if (newPolicyUI == null) {
			newPolicyUI = new NewPolicyForm(UIConstants.UIACTION_Wizzard, UICONTAINERTYPES.Wizzard, this.newPolicyDto,
							this::onProductChange,
							this::onCustomerTypeChange);
		}
		
		return newPolicyUI;
	}
	
	protected Component getCustomerForm() {
		if (customerLayout == null) {
			customerLayout = new VerticalLayout();
			customerLayout.setSizeFull();
		}

		return customerLayout;
	}

	protected Component getAutoSubPolicyForm() {
		if (autoSubPolicyUI == null) {
			autoSubPolicyUI = new AutoSubPolicyForm(UIConstants.UIACTION_New, UIConstants.UICONTAINERTYPES.Wizzard);
		}
		
		return autoSubPolicyUI;
	}

	protected Component getCGLSubPolicyForm() {
		if (cglSubPolicyUI == null) {
			cglSubPolicyUI = new CGLSubPolicyForm(UIConstants.UIACTION_New, UIConstants.UICONTAINERTYPES.Wizzard);
		}
		
		return cglSubPolicyUI;
	}

	protected Component getCargoSubPolicyForm() {
		if (cargoSubPolicyUI == null) {
			cargoSubPolicyUI = new CargoSubPolicyForm(UIConstants.UIACTION_New, UICONTAINERTYPES.Wizzard);
		}

		return cargoSubPolicyUI;
	}

	@Override
	protected VALIDATIONSTATUSES validateEditTabComponent(int page, VALIDATIONSTEPS validStep) {
		VALIDATIONSTATUSES result = VALIDATIONSTATUSES.Succeed;
		
		switch (page) {
		case PAGE_POLICY:
			if (newPolicyUI != null) {
				if (!newPolicyUI.validateEdit()) {
					result = VALIDATIONSTATUSES.Fail;
				}
				else {
					endPolicyPage();
				}
			}
			else {
				result = VALIDATIONSTATUSES.Mandatory; // Mandatory page
			}
			break;
			
		case PAGE_CUSTOMER:
			if (customerUI != null) {
				result = validateCustomer(validStep);
				if (!VALIDATIONSTATUSES.Fail.equals(result)) {
					endCustomerPage();
				}
			}
			else {
				result = VALIDATIONSTATUSES.Mandatory; // Mandatory page
			}
			break;
			
		case PAGE_AUTO:
			if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy())) {
				if (autoSubPolicyUI != null) {
					if (!autoSubPolicyUI.validateEdit()) {
						result = VALIDATIONSTATUSES.Fail;
					}
					else {
						endAutoPage();
					}
				}
				else {
					result = VALIDATIONSTATUSES.Mandatory; // Mandatory page
				}
			}
			break;
			
		case PAGE_CGL:
			if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy())) {
				if (cglSubPolicyUI != null) {
					if (!cglSubPolicyUI.validateEdit()) {
						result = VALIDATIONSTATUSES.Fail;
					}
					else {
						endCGLPage();
					}
				}
				else {
					result = VALIDATIONSTATUSES.Mandatory; // Mandatory page
				}
			}
			break;

		case PAGE_CARGO:
			if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy())) {
				if (cargoSubPolicyUI != null) {
					if (!cargoSubPolicyUI.validate()) {
						result = VALIDATIONSTATUSES.Fail;
					}
					else {
						endCargoPage();
					}
				} 
				else {
					result = VALIDATIONSTATUSES.Mandatory; // Mandatory page
				}
			}
			break;
			
		default:
			break;
		}
		
		return result;
	}
	
	protected VALIDATIONSTATUSES validateCustomer(VALIDATIONSTEPS validStep) {
		VALIDATIONSTATUSES result = VALIDATIONSTATUSES.Succeed;
		
		if (!customerUI.validateEdit()) {
			result = VALIDATIONSTATUSES.Fail;
		}
		else if (customerUI instanceof CommercialCustomerForm && !isRINnumberIsNotListedConfirmed
				&& ((CommercialCustomerForm) customerUI).isRINEmpty()) {
			result = VALIDATIONSTATUSES.Confirm;
			this.setProcessStarted();
			
			DSConfirmDialog confirmDialog = new DSConfirmDialog(
					null, 
					uiHelper.getButtonLabel(UIConstants.UIACTION_Yes),
					this::onConfirmUpdateCustomerEmptyRIN,
					uiHelper.getButtonLabel(UIConstants.UIACTION_No),
					this::onCancelUpdateCustomerEmptyRIN,
					uiHelper.getSessionLabels().getString("CommercialCustomerRINEmptyWarnMessage"));
			confirmDialog.setDataStore(new DSDialogDataStore(validStep));
			confirmDialog.open();
		}
		
		return result;
	}
	
	protected void onConfirmUpdateCustomerEmptyRIN(DSConfirmDialog.ConfirmEvent event) {
		isRINnumberIsNotListedConfirmed = true;
		
		DSConfirmDialog dialog = event.getSource();
		VALIDATIONSTEPS validStep = (VALIDATIONSTEPS) dialog.getDataStore().getData1();
		dialog.close();
		
		this.setProcessEnded();
		
		if (VALIDATIONSTEPS.BeforeNext.equals(validStep)) {
			continueNextEvent();
		}
		else if (VALIDATIONSTEPS.ValidateCurrent.equals(validStep)) {
			continueWizardValidateEdit(1);
		}
		else if (VALIDATIONSTEPS.ValidatePage.equals(validStep)) {
			continueWizardValidateEdit(1);
		}
	}
	
	protected void onCancelUpdateCustomerEmptyRIN(DSConfirmDialog.CancelEvent event) {
		DSConfirmDialog dialog = event.getSource();
		dialog.close();
		
		this.setProcessEnded();
	}
	
	public boolean saveEdit(NewPolicyDto newPolicyDto) {
		if (newPolicyUI != null) {
			return newPolicyUI.saveEdit(newPolicyDto);
		}
		
		return true;
	}
	
	public boolean saveEdit(Customer customer, Address address, PolicyTransaction<?> policyTransaction) {
		if (customerUI != null) {
			return customerUI.saveEdit(customer, address, policyTransaction);
		}
		
		return true;
	}
	
	@SuppressWarnings("rawtypes")
	public boolean saveEdit(SpecialityAutoSubPolicy autoSubPolicy) {
		if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy()) && autoSubPolicyUI != null) {
			return autoSubPolicyUI.saveEdit(autoSubPolicy);
		}
		
		return true;
	}
	
	public boolean saveEdit(CGLSubPolicy<?> cglSubPolicy) {
		if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()) && cglSubPolicyUI != null) {
			return cglSubPolicyUI.saveEdit(cglSubPolicy);
		}
		
		return true;
	}

	public boolean saveEdit(CargoSubPolicy<?> cargoSubPolicy) {
		if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()) && cargoSubPolicyUI != null) {
			return cargoSubPolicyUI.save(cargoSubPolicy);
		}

		return true;
	}

	public void loadData() {
		if (newPolicyUI != null) {
			newPolicyUI.loadData();
			newPolicyUI.enableEdit();
		}
		
		// The fields has the same values as the UI when loading the wizard initially
		// Set value changed to TRUE
		if (newPolicyDto != null) {
			newPolicyDto.setProductCdChanged(true);
			newPolicyDto.setCustomerTypeChanged(true);
		}
		
		// Cannot load other UI(s) because they require product config
		// Product config will be available when use
		
		refreshPage();
	}
	
	protected void onProductChange(NewPolicyForm.ProductChangeEvent event) {
		String newValue = event.getSource().getValue();
		if (StringUtils.isNotBlank(newValue) &&
			(StringUtils.isBlank(newPolicyDto.getProductCd()) ||
			 !StringUtils.equalsAnyIgnoreCase(newPolicyDto.getProductCd(), newValue))) {
			newPolicyDto.setProductCdChanged(true);
		}
		else { // In case user changed the value back to original
			newPolicyDto.setProductCdChanged(false);
		}
	}
	
	protected void onCustomerTypeChange(NewPolicyForm.CustomerTypeChangeEvent event) {
		String newValue = event.getSource().getValue();
		if (StringUtils.isNotBlank(newValue) &&
			(StringUtils.isBlank(newPolicyDto.getCustomerType()) ||
			 !StringUtils.equalsAnyIgnoreCase(newPolicyDto.getCustomerType(), newValue))) {
			newPolicyDto.setCustomerTypeChanged(true);
		}
		else { // In case user changed the value back to original
			newPolicyDto.setCustomerTypeChanged(false);
		}
	}

	@Override
	protected boolean continueWizardValidateEdit(int fromPage) {
		// TODO Auto-generated method stub
		boolean ok = super.continueWizardValidateEdit(fromPage);
		
		if (ok) {
			if (newPolicyWizardCallback != null) {
				newPolicyWizardCallback.continueSaveNewPolicy();
			}
		}
		
		return ok;
	}

	protected void endPolicyPage() {
		// Need to save the flag/codes for actions
		//newPolicyUI.saveEdit(newPolicyDto);
		saveEdit(newPolicyDto);
		
		onCustomerTypeChangeIfRequired();
		if (customerUI != null) {
			customerUI.enableEdit();
		}
		
		onProductCdChangeIfRequired();
		if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy())) {
			if (autoSubPolicyUI != null) {
				if (StringUtils.equalsAnyIgnoreCase(newPolicyDto.getProductCd(), ConfigConstants.PRODUCTCODE_TOWING_NONFLEET)) {
					autoSubPolicyUI.loadValues(Boolean.FALSE, SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED);
				}
				
				autoSubPolicyUI.enableEdit();
			}
		}
		
		// update the limit and refresh lookups 
		if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy())) {
			if (cglSubPolicyUI != null) {
				cglSubPolicyUI.loadValues(newPolicyDto.getLiabilityLimit());
				cglSubPolicyUI.enableEdit();
			}
		}

		if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy())) {
			if (cargoSubPolicyUI != null) {
				cargoSubPolicyUI.loadValues();
				cargoSubPolicyUI.enableEdit();
			}
		}
	}
	
	protected void endCustomerPage() {
		// Need to save the flag/codes for actions
		//customerUI.saveEdit(newPolicyDto.getCustomer(), newPolicyDto.getAddress(), newPolicyDto.getPolicyTransaction());
		if (newPolicyDto.getCustomer() != null && newPolicyDto.getAddress() != null && newPolicyDto.getPolicyTransaction() != null) {
			saveEdit(newPolicyDto.getCustomer(), newPolicyDto.getAddress(), newPolicyDto.getPolicyTransaction());
		}
	}
	
	protected void endAutoPage() {
		// Need to save the flag/codes for actions
		//autoSubPolicyUI.saveEdit(newPolicyDto.getAutoSubPolicy());
		if (newPolicyDto.getAutoSubPolicy() != null) {
			saveEdit(newPolicyDto.getAutoSubPolicy());
		}
	}
	
	protected void endCGLPage() {
		// Need to save the flag/codes for actions
		//cglSubPolicyUI.saveEdit(newPolicyDto.getcGLSubPolicy());
		if (newPolicyDto.getcGLSubPolicy() != null) {
			saveEdit(newPolicyDto.getcGLSubPolicy());
		}
	}
	
	protected void endCargoPage() {
		// Need to save the flag/codes for actions
		//cargoSubPolicyUI.save(newPolicyDto.getCargoSubPolicy());
		if (newPolicyDto.getCargoSubPolicy() != null) {
			saveEdit(newPolicyDto.getCargoSubPolicy());
		}
	}
	
	@Override
	protected void refreshTabs() {
		// TODO Auto-generated method stub
		super.refreshTabs();
		
		setTabVisible(PAGE_AUTO, 	Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy()));
		setTabVisible(PAGE_CGL,  	Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()));
		setTabVisible(PAGE_CARGO,  	Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()));
	}
	
	private void onProductCdChangeIfRequired() {
		if (newPolicyDto != null && newPolicyDto.isProductCdChanged()) {
			onProductCdChange(newPolicyDto.getProductCd());
			newPolicyDto.setProductCdChanged(false);
		}
	}

	private void onProductCdChange(String productCd) {
		if (customerUI != null) {
			customerUI.loadDataConfigs(productCd);
		}
		
		if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy())) {
			if (autoSubPolicyUI != null) {
				if (autoSubPolicyUI != null && newPolicyDto.getAutoSubPolicy() != null) {
					autoSubPolicyUI.loadData(newPolicyDto.getAutoSubPolicy());
				}
				if (StringUtils.equalsAnyIgnoreCase(newPolicyDto.getProductCd(), ConfigConstants.PRODUCTCODE_TOWING_NONFLEET)) {
					autoSubPolicyUI.loadValues(Boolean.FALSE, SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED);
				}
			}
		}
		
		if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy())) {
			if (cglSubPolicyUI != null && newPolicyDto.getcGLSubPolicy() != null) {
				cglSubPolicyUI.loadData(newPolicyDto.getcGLSubPolicy());
			}
		}

		if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy())) {
			if (cargoSubPolicyUI != null && newPolicyDto.getCargoSubPolicy() != null) {
				cargoSubPolicyUI.loadData(newPolicyDto.getCargoSubPolicy());
			}
		}
	}
	
	private void onCustomerTypeChangeIfRequired() {
		if (newPolicyDto != null && newPolicyDto.isCustomerTypeChanged()) {
			onCustomerTypeChange(newPolicyDto.getCustomerType(), newPolicyDto.getProductCd());
			newPolicyDto.setCustomerTypeChanged(false);
		}
	}
	
	private void onCustomerTypeChange(String customerType, String selectedProduct) {
		// Need to recreate the customer object based on the selected customer type. 
		// User can switch customer type; Commercial and Personal  
		try {
			newPolicyDto = newPolicyWizardHelper.reCreateEmptyCustomert(newPolicyDto);
		} catch (Exception e) {
			uiHelper.notificationException(e);
			return;
		} 
		final CustomerUIHelper<?> customerUIHelper = UIFactory.getInstance().getCustomerUIHelper(customerType);
		customerUI = customerUIHelper.createCustomerForm(UIConstants.UIACTION_New, UICONTAINERTYPES.Wizzard);
		customerUI.loadData(newPolicyDto.getCustomer(), newPolicyDto.getAddress(),
								newPolicyDto.getPolicyTransaction(), selectedProduct);
		customerUI.enableEdit();
		
		customerLayout.removeAll();
		customerLayout.add(customerUI);
	}

	/**
	 * Checks to see whether a page should be disabled. Disabled pages are skipped
	 * by the wizard.
	 * 
	 * @param page the page to test
	 * @return {@code true} if the page is disabled; {@code false} otherwise
	 */
	@Override
	protected boolean isPageDisabled(int page) {
		switch (page) {
		case PAGE_AUTO:
			return !newPolicyDto.getHasAutomobileSubPolicy();
		case PAGE_CGL:
			return !newPolicyDto.getHasCGLSubPolicy();
		case PAGE_CARGO:
			return !newPolicyDto.getHasCargoSubPolicy();
		default:
			return false; // All other pages are mandatory
		}
	}
}
