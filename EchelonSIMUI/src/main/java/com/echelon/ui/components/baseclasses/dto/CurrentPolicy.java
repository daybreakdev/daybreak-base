package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;

public class CurrentPolicy implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3352322893739756602L;
	private PolicyVersion						policyVersion;
	private PolicyTransaction<?>				policyTransaction;
	private List<PolicyHistoryDTO>				transactionHistores;
	private Object								lastData;
	private SpecialtyPolicyDTO					specialtyPolicyDTO;
	private boolean								isFullTransDataLoaded;
	private ReinsuranceReportDTO				reinsuranceReportDTO;

	public CurrentPolicy() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PolicyVersion getPolicyVersion() {
		return policyVersion;
	}

	public PolicyTransaction<?> getPolicyTransaction() {
		return policyTransaction;
	}

	public List<PolicyHistoryDTO> getTransactionHistores() {
		return transactionHistores;
	}

	public SpecialtyPolicyDTO getSpecialtyPolicyDTO() {
		return specialtyPolicyDTO;
	}

	public Object getLastData() {
		return lastData;
	}

	public void resetLastData() {
		this.lastData = null;
	}

	public boolean isFullTransDataLoaded() {
		return isFullTransDataLoaded;
	}

	public void setFullTransDataLoaded(boolean isFullTransDataLoaded) {
		this.isFullTransDataLoaded = isFullTransDataLoaded;
	}

	public void setTransactionHistores(List<PolicyHistoryDTO> transactionHistores) {
		this.transactionHistores = transactionHistores;
	}

	public ReinsuranceReportDTO getReinsuranceReportDTO() {
		return reinsuranceReportDTO;
	}

	public void setReinsuranceReportDTO(ReinsuranceReportDTO reinsuranceReportDTO) {
		this.reinsuranceReportDTO = reinsuranceReportDTO;
	}

	public void resetCurrentPolicy() {
		policyTransaction 		= null;
		policyVersion 			= null;
		transactionHistores		= null;
		specialtyPolicyDTO	 	= null;
		lastData	        	= null;
		isFullTransDataLoaded	= false;
		reinsuranceReportDTO	= null;
	}
	
	public void setCurrentPolicy(PolicyVersion policyVersion, PolicyTransaction policyTransaction, 
									SpecialtyPolicyDTO specialtyPolicyDTO,
									Object lastData) {
		resetCurrentPolicy();
		
		this.policyTransaction 		= policyTransaction;
		this.policyVersion 			= policyVersion;
		this.specialtyPolicyDTO 	= specialtyPolicyDTO;
		this.lastData				= lastData;
	}
}
