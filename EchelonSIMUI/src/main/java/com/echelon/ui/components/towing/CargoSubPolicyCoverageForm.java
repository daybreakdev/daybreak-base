package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextFieldVariant;

public class CargoSubPolicyCoverageForm extends BaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2555405863285518550L;
	private IntegerField numOfPowerUnitsField;

	public CargoSubPolicyCoverageForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, UIConstants.OBJECTTYPES.CGOSubPolicy);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();
		
		numOfPowerUnitsField = new IntegerField(labels.getString("CargoSubPolicyCoverageFormNumInUsePowerUnitsLabel"));
		numOfPowerUnitsField.addThemeVariants(TextFieldVariant.LUMO_ALIGN_RIGHT);
	}

	@Override
	protected void buildPremiunmForm(CoverageDto coverageDto) {
		premiumForm.removeAll();
		
		premiumForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 6)
		        );
		
		//First half
		premiumForm.add(coverageUWAfjuestmentField, numOfPowerUnitsField);
		attachIsPremOvrAndIsReinsuredToForm(premiumForm, 1);
		//Second half
		premiumForm.add(coverageRatedPremiumField, coverageAnnualPremiumField, coverageWrittenPremiumField);
	}	

	@Override
	protected void buildBinderForCoverage(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.buildBinderForCoverage(coverageDto);
		
		covBinder.forField(numOfPowerUnitsField).bind(CoverageDto::getCoverageNumOfUnits, CoverageDto::setCoverageNumOfUnits);
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.enableEdit(coverageDto);
		
		uiHelper.setEditFieldsReadonly(numOfPowerUnitsField);
	}
	
}
