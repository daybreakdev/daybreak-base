package com.echelon.ui.components.baseclasses.policy;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.data.binder.Binder;

public class CovUWAdjustmentField extends DSNumberField<Double> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7543679932720783970L;
	protected Binder<CoverageDto> 	  binder;
	protected CoverageDto			  coverageDto;
	protected CovIsPremiumOvrField 	  isPremiumOvrField;
	protected UIConstants.OBJECTTYPES parentObjectType;

	public CovUWAdjustmentField(String label) {
		super(label , PRESENTFORMATS.UWAdjustment);
		// TODO Auto-generated constructor stub
	}
	
	public void initializeField(UIConstants.OBJECTTYPES parentObjectType, CoverageDto dto, CovIsPremiumOvrField isPremiumOvrField) {
		this.uiHelper    		= BrokerUI.getHelperSession().getUiHelper();
		this.parentObjectType   = parentObjectType;
		this.coverageDto 		= dto;
		this.isPremiumOvrField  = isPremiumOvrField;
	}
	
	public void bindField(Binder<CoverageDto> binder) {
		// do not save UW Adjust if it is readonly.
		this.binder = binder;
		if (binder != null) {
			binder.removeBinding(this);
			
			if (this.isReadOnly()) {
				binder.forField(this)
						.withNullRepresentation("")
						.withConverter(this.getConverter())
						.bind(CoverageDto::getUwAdjustment, null);
			}
			else {
				binder.forField(this)
						.withNullRepresentation("")
						.withConverter(this.getConverter())
						.bind(CoverageDto::getUwAdjustment, CoverageDto::setUwAdjustment);
			}
		}		
	}
	
	protected boolean editableForTxnType() {
		if (Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType()) ||
			Constants.VERS_TXN_TYPE_RENEWAL.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType())) {
			return true;
		}
		
		if (Constants.VERS_TXN_TYPE_POLICYCHANGE.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType()) &&
			this.coverageDto != null &&
			this.coverageDto.getCoverageParent() != null &&
			this.coverageDto.isNewCoverage()) {
			return true;
		}
		
		return false;
	}
	
	protected boolean editableForCoverageParent() {
		if (StringUtils.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), ConfigConstants.PRODUCTCODE_TOWING_NONFLEET) &&
			this.coverageDto != null &&
			this.coverageDto.getCoverageParent() != null && 
			(this.coverageDto.getCoverageParent() instanceof SpecialtyVehicleRisk || this.coverageDto.getCoverageParent() instanceof SpecialityAutoSubPolicy)) {
			return false;
		}
		
		return true;
	}
	
	protected boolean isAllowPremiumOverride() {
		return coverageDto.isAllowPremiumOverride() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage();
	}
	
	public void determineReadOnly() {
		if (this.coverageDto != null &&
			CoverageUIFactory.getInstance().isUWAdjustmentFieldEditable(parentObjectType, this.coverageDto.getCoverageType(), this.coverageDto.getCoverageCode()) &&
			isAllowPremiumOverride() &&
			BrokerUI.getUserSession() != null && BrokerUI.getUserSession().getCurrentPolicy() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
			editableForTxnType() &&
			editableForCoverageParent()
			) {
			uiHelper.setEditFieldsNotReadonly(this);
		}
		else {
			uiHelper.setEditFieldsReadonly(this);
		}
		
		bindField(binder);
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && isAllowPremiumOverride()) {
			this.setTabIndex(value);
			return true;
		}
		else {
			this.setTabIndex(-1);
			return false;
		}
	}
}
