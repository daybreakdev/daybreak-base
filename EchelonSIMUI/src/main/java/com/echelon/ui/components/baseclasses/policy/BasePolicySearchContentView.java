package com.echelon.ui.components.baseclasses.policy;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.renderers.DSLabelRenderder;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.PolicySearchCriteria;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyDetailsUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.uicommon.components.DSToolbar;
import com.ds.ins.uicommon.components.IDSEnableShortcuts;
import com.ds.ins.domain.dto.PolicySearchResults;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.grid.Grid.Column;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.PropertyId;
import com.vaadin.flow.data.renderer.LocalDateRenderer;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeLeaveEvent;
import com.vaadin.flow.router.BeforeLeaveObserver;
import com.vaadin.flow.router.Route;

@Route(value="PolicySearchView", layout = BasePolicySearchView.class)
public class BasePolicySearchContentView extends BaseLayout implements AfterNavigationObserver, BeforeLeaveObserver, IDSEnableShortcuts {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4270819249674989521L;
	@PropertyId("name")
	private TextField			nameField;
	@PropertyId("policyNum")
	private TextField			policyNumField;
	@PropertyId("broker")
	private TextField			brokerField;
	@PropertyId("quoteNum")
	private TextField			quoteNumField;
	@PropertyId("extQuoteNum")
	private TextField			extQuoteNumField;
	private Binder<PolicySearchCriteria>  binder 		= null;
	private Grid<PolicySearchResults> 	  resultGrid	= new Grid<PolicySearchResults>();
	
	private DSToolbar			toolbar;
	
	private static final String COLUMN_POLNUM 	 = "POLNUM";
	private static final String COLUMN_INSURED 	 = "INSURED";
	private static final String COLUMN_BROKER 	 = "BROKER";
	private static final String COLUMN_EFF 		 = "EFF";
	private static final String COLUMN_EXP 		 = "EXP";
	private static final String COLUMN_STATUS 	 = "STATUS";
	private static final String COLUMN_PRODUCTCD = "PRODUCTCD";
	
	public BasePolicySearchContentView() {
		super(UIConstants.UIACTION_Search, UIConstants.UICONTAINERTYPES.Route);
		
		initializeLayouts();
		buildButtons();
		buildFields();
		buildCriteriaForm();
		buildGrid();
	}
	
	private void initializeLayouts() {
		this.setHeightFull();
		//this.setSpacing(true);
		//this.setPadding(true);
		//this.setMargin(true);
		resultGrid.setSizeFull();
	}

	private void buildButtons() {
		toolbar = new DSToolbar(JustifyContentMode.START);
		toolbar.buildButtons(new String[] {UIConstants.UIACTION_Search, UIConstants.UIACTION_Clear});
		toolbar.getButton(UIConstants.UIACTION_Search).addClickShortcut(Key.ENTER);
		toolbar.getButton(UIConstants.UIACTION_Search).addClickListener(this::searchButtonClicked);
		toolbar.getButton(UIConstants.UIACTION_Clear).addClickListener(this::clearButtonClicked);
	}

	private void buildFields() {
		// Criteria fields
		nameField 			= new TextField(labels.getString("PolicySearchCriteriaNameLabel"));
		policyNumField 		= new TextField(labels.getString("PolicySearchCriteriaPolicyNumberLabel"));
		brokerField 		= new TextField(labels.getString("PolicySearchCriteriaBrokerLabel"));
		quoteNumField 		= new TextField(labels.getString("PolicySearchCriteriaQuoteNumberLabel"));
		extQuoteNumField 	= new TextField(labels.getString("PolicySearchCriteriaExternalQuoteNumberLabel"));
		
		binder = new Binder<PolicySearchCriteria>(PolicySearchCriteria.class);
		binder.setBean(new PolicySearchCriteria());
	}
	
	private void buildCriteriaForm() {		
		
		//nameField.setWidthFull();
		
		FormLayout criteriaForm = new FormLayout();
		criteriaForm.setWidthFull();
		criteriaForm.setResponsiveSteps(
			        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
			        );
		criteriaForm.add(nameField, 2);
		criteriaForm.add(brokerField, new Label(null));
		criteriaForm.add(extQuoteNumField, quoteNumField, policyNumField, toolbar);
		
		binder.bindInstanceFields(this);		
		
		// Add to Criteria section
		HorizontalLayout criteriaLayout	= new HorizontalLayout();
		uiHelper.formatFullLayout(criteriaLayout);
		criteriaLayout.add(criteriaForm);
		criteriaLayout.setSpacing(true);
		//criteriaLayout.setVerticalComponentAlignment(Alignment.END, toolbar);
		criteriaLayout.setWidth("99%");
		
		this.add(criteriaLayout);
		this.setHorizontalComponentAlignment(Alignment.CENTER, criteriaLayout);
	}

	private void buildGrid() {
		VerticalLayout gridLayout = new VerticalLayout();
		uiHelper.formatFullLayout(gridLayout);
		gridLayout.setHeightFull();
		Label resultTitle = new Label(labels.getString("PolicySearchResultGridLabel"));
		uiHelper.setLabelHeaderStyle(resultTitle);
		gridLayout.add(resultTitle);
		
		uiHelper.setGridStyle(resultGrid);
		resultGrid.addColumn(o -> (StringUtils.isNotBlank(o.getPolicyNum())
									? o.getPolicyNum()
									: o.getQuoteNum()))
							.setHeader(labels.getString("PolicySearchResultPolicyQuoteNumberLabel"))
							.setKey(COLUMN_POLNUM)
							.setSortable(true);
		resultGrid.addColumn(PolicySearchResults::getInsured).setHeader(labels.getString("PolicySearchResultInsuredLabel"))
							.setKey(COLUMN_INSURED)
							.setSortable(true);
		resultGrid.addColumn(PolicySearchResults::getBrokerName).setHeader(labels.getString("PolicySearchResultBrokerLabel"))
							.setKey(COLUMN_BROKER)
							.setSortable(true);
		resultGrid.addColumn(new LocalDateRenderer<>(PolicySearchResults::getEffDate, UIConstants.SYSDATEFORMAT)).setHeader(labels.getString("PolicySearchResultPolicyEffectiveLabel"))
							.setKey(COLUMN_EFF)
							.setSortable(true)
							.setComparator(new Comparator<PolicySearchResults>() {
								
								@Override
								public int compare(PolicySearchResults o1, PolicySearchResults o2) {
									return o1.getEffDate().compareTo(o2.getEffDate());
								}
							});
		resultGrid.addColumn(new LocalDateRenderer<>(PolicySearchResults::getExpDate, UIConstants.SYSDATEFORMAT)).setHeader(labels.getString("PolicySearchResultPolicyExpiryLabel"))
							.setKey(COLUMN_EXP)
							.setSortable(true)
							.setComparator(new Comparator<PolicySearchResults>() {
								
								@Override
								public int compare(PolicySearchResults o1, PolicySearchResults o2) {
									return o1.getExpDate().compareTo(o2.getExpDate());
								}
							});
		resultGrid.addColumn(new DSLabelRenderder<>(PolicySearchResults::getVersionDisplayStr, "PolicyBusinessStatus", null))
									.setHeader(labels.getString("PolicySearchResultPolicyTermStatusLabel"))
									.setKey(COLUMN_STATUS);
		resultGrid.addColumn(new DSLookupItemRenderer<PolicySearchResults>(PolicySearchResults::getProductCd,
									uiConfigHelper.getActiveProductLookups(LocalDate.now())))
									.setHeader(labels.getString("PolicySearchResultPolicyProductLabel"))
									.setKey(COLUMN_PRODUCTCD);
		
		resultGrid.setSelectionMode(SelectionMode.SINGLE);
		resultGrid.addItemClickListener(this::openPolicy);
		resultGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		gridLayout.add(resultGrid);
		gridLayout.setWidth("99%");
		this.add(gridLayout);
		this.setHorizontalComponentAlignment(Alignment.CENTER, gridLayout);
	}
	
	private void searchButtonClicked(ClickEvent<Button> clickEvent) {
		runSearch();
	}
	
	private void runSearch() {
		PolicySearchCriteria criteria = binder.getBean();
		
		if (StringUtils.isBlank(criteria.getPolicyNum()) && StringUtils.isBlank(criteria.getName()) && StringUtils.isBlank(criteria.getBroker()) &&
			StringUtils.isBlank(criteria.getQuoteNum()) && StringUtils.isBlank(criteria.getExtQuoteNum())) {
			uiHelper.notificationWarning(labels.getString("PolicySearchCriteriaEmptyError"));
			return;
		}
		
		if ((StringUtils.isNotBlank(criteria.getName()) && criteria.getName().trim().length() < 2) || 
			(StringUtils.isNotBlank(criteria.getBroker()) && criteria.getBroker().trim().length() < 2)) {
			uiHelper.notificationWarning(labels.getString("PolicySearchCriteria2CharactersError"));
			return;
		}
		
		resultGrid.setItems(new ArrayList<PolicySearchResults>());
		UIFactory.getInstance().getPolicyDetailsUIHelper().policySearch(criteria, new BasePolicyDetailsUIHelper.PolicySearchCallback() {
			
			@Override
			public void resultIsReady(PolicySearchCriteria criteria) {
				resultGrid.setItems(criteria.getSearchResults());
			}
		});
	}
	
	private void clearButtonClicked(ClickEvent<Button> clickEvent) {
		binder.setBean(new PolicySearchCriteria());
		resultGrid.setItems(new ArrayList<PolicySearchResults>());
	}
	
	private void openPolicy(ItemClickEvent<PolicySearchResults> event) {
		PolicySearchResults searchResult = event.getItem();
		if (searchResult != null) {
			UIFactory.getInstance().getPolicyDetailsUIHelper()
				.openPolicy(searchResult.getProductCd(), searchResult.getPolicyNum(), searchResult.getQuoteNum(),
							searchResult.getPolicyTerm(), searchResult.getVersionNumber());
		}
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		// TODO Auto-generated method stub
		
		if (BrokerUI.getUserSession() != null) {
			BrokerUI.getUserSession().setPolicySearchView(this);
			restoreValues(BrokerUI.getUserSession().getPolicySearchCriteria());
		}
	}

	@Override
	public void beforeLeave(BeforeLeaveEvent event) {
		// TODO Auto-generated method stub
		if (BrokerUI.getUserSession() != null) {
			// Try to save the order
			PolicySearchCriteria store = binder.getBean();
			store.setSortOrders(resultGrid.getSortOrder());
			
			BrokerUI.getUserSession().setPolicySearchCriteria(binder.getBean());
			BrokerUI.getUserSession().setPolicySearchView(null);
		}
	}

	private void restoreValues(PolicySearchCriteria store) {
		if (binder != null) {
			if (store == null) {
				store = new PolicySearchCriteria();
			}
			
			binder.setBean(store);
			
			if (store.getSortOrders() != null && !store.getSortOrders().isEmpty()) {
				List<GridSortOrder<PolicySearchResults>> orders = new ArrayList<GridSortOrder<PolicySearchResults>>();
				for(GridSortOrder<PolicySearchResults> order : store.getSortOrders()) {
					Column<PolicySearchResults> col = resultGrid.getColumnByKey(order.getSorted().getKey());
					if (col != null) {
						orders.add(new GridSortOrder<PolicySearchResults>(col, order.getDirection()));
					}
				}
				
				resultGrid.sort(orders);
			}
			
			resultGrid.setItems(store.getSearchResults());
		}
	}

	@Override
	public void enableButtonShortcuts() {
		toolbar.setProcessEnded();
	}

	@Override
	public void disableButtonShortcuts() {
		toolbar.setProcessStarted();
	}

}
