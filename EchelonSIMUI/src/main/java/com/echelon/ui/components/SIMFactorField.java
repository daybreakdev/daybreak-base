package com.echelon.ui.components;

import com.ds.ins.uicommon.components.DSNumberField;
import com.echelon.ui.constants.UIConstants;

public class SIMFactorField extends DSNumberField<Double> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5648353227562529181L;

	public SIMFactorField(String label) {
		super(label, Double.class, 
				UIConstants.DECIMALPLACES_FACTOR_MIN_DISPLAY, UIConstants.DECIMALPLACES_FACTOR_MAX_DISPLAY);
		// TODO Auto-generated constructor stub
	}

}
