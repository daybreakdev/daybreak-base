package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.entities.LienholderLessorList;
import com.echelon.ui.components.entities.LienholderLessorSubPolicyList;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.helpers.policy.towing.LienLessorVehicleDetailsUIHelper;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.GridSortOrder;
import com.vaadin.flow.data.provider.SortDirection;
import com.vaadin.flow.data.renderer.NumberRenderer;

public abstract class LienLessorVehicleDetailsList<PARENTMODEL, LIENLESSORMODEL> 
													extends BaseListLayout<LienholderLessorVehicleDetails> {

	protected static final String COLUMN_SEQ  = "SEQ";
	protected static final String COLUMN_DED  = "DED";
	
	protected PARENTMODEL			vehicleDetailsParent;
	protected LIENLESSORMODEL		parentLienLessorHolder;
	
	protected LienLessorVehicleDetailsUIHelper	vehicleDetailsUIHelper = new LienLessorVehicleDetailsUIHelper();
	protected Grid<LienholderLessorVehicleDetails> 	vehDetailGrid;
	
	public LienLessorVehicleDetailsList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}

	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("LienLessorVehDetailListTitle"),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	private void buildGrid() {
		vehDetailGrid = new Grid<LienholderLessorVehicleDetails>();
		vehDetailGrid.setSizeFull();
		uiHelper.setGridStyle(vehDetailGrid);

		createIndicatorColumn(vehDetailGrid);
		
		vehDetailGrid.addColumn(LienholderLessorVehicleDetails::getVehicleDetailsSequence)
									.setKey(COLUMN_SEQ)
									.setVisible(false);
		vehDetailGrid.addColumn(LienholderLessorVehicleDetails::getVehicleNum).setHeader(labels.getString("LienLessorVehDetailListVehNumberLabel"))
									.setFlexGrow(1);
		vehDetailGrid.addColumn(LienholderLessorVehicleDetails::getVehicleYear).setHeader(labels.getString("LienLessorVehDetailListVehYearLabel"))
									.setTextAlign(ColumnTextAlign.END)
									.setFlexGrow(1);
		vehDetailGrid.addColumn(LienholderLessorVehicleDetails::getDescription).setHeader(labels.getString("LienLessorVehDetailListDescriptionLabel"))
									.setAutoWidth(true)
									.setFlexGrow(4);
		vehDetailGrid.addColumn(LienholderLessorVehicleDetails::getSerialNum).setHeader(labels.getString("LienLessorVehDetailListSerialNoLabel"))
									.setAutoWidth(true)
									.setFlexGrow(3);
		vehDetailGrid.addColumn(new NumberRenderer<>(LienholderLessorVehicleDetails::getLessorDeductible, DSNumericFormat.getIntegerInstance()))
									.setHeader(labels.getString("LienLessorVehDetailListDeductibleLabel"))
									.setTextAlign(ColumnTextAlign.END)
									.setKey(COLUMN_DED)
									.setFlexGrow(1);
		
		vehDetailGrid.setSelectionMode(SelectionMode.SINGLE);
		vehDetailGrid.addSelectionListener(event -> refreshToolbar());
		vehDetailGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		setUpGridDbClickEvent(vehDetailGrid);
		vehDetailGrid.sort(Arrays.asList(
				new GridSortOrder<LienholderLessorVehicleDetails>(vehDetailGrid.getColumnByKey(COLUMN_SEQ), SortDirection.ASCENDING)
				));
		
		this.add(vehDetailGrid);
	}
	
	protected void newAction() {
		
	}

	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				newAction();
			});
		}
	}
	
	protected void updateAction(LienholderLessorVehicleDetails vehicleDetails) {
		
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!vehDetailGrid.getSelectedItems().isEmpty()) {
					updateAction(vehDetailGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}

	protected void deleteAction(LienholderLessorVehicleDetails vehicleDetails) {
		
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!vehDetailGrid.getSelectedItems().isEmpty()) {
					deleteAction(vehDetailGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}

	protected void undeleteAction(LienholderLessorVehicleDetails vehicleDetails) {
		
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (!vehDetailGrid.getSelectedItems().isEmpty()) {
					undeleteAction(vehDetailGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void viewAction(LienholderLessorVehicleDetails vehicleDetails) {
		
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!vehDetailGrid.getSelectedItems().isEmpty()) {
					viewAction(vehDetailGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	public void lienholderLessorChanged(LienholderLessorList.SelectionChangeEvent event) {
		if (event.getSource() instanceof LienholderLessorSubPolicyList) {
			LienholderLessorSubPolicyList lienLessorList = (LienholderLessorSubPolicyList) event.getSource();
			loadData((PARENTMODEL)lienLessorList.getLienLessorParent(), (LIENLESSORMODEL)lienLessorList.getSelectedLienLessor());
		}
	}
	
	protected List<LienholderLessorVehicleDetails> getVehicleDetailsFromParent() {
		return null;
	}
	
	protected String getLienLessorType(LIENLESSORMODEL lienLessor) {
		return null;
	}
	
	protected void loadData(PARENTMODEL parent, LIENLESSORMODEL lienLessor) {
		if (UIConstants.LIENLESSORTYPE_HOLDER.equalsIgnoreCase(getLienLessorType(lienLessor))) {
			vehDetailGrid.getColumnByKey(COLUMN_DED).setVisible(false);
		}
		else {
			vehDetailGrid.getColumnByKey(COLUMN_DED).setVisible(true);
		}
		
		this.vehicleDetailsParent 	= parent;
		this.parentLienLessorHolder = lienLessor;
		
		List<LienholderLessorVehicleDetails> list = getVehicleDetailsFromParent();
		if (list == null) {
			list = new ArrayList<LienholderLessorVehicleDetails>();
		}
		
		vehDetailGrid.setItems(list);
		
		refreshToolbar();
	}

	protected void refreshToolbar() {
		if (vehicleDetailsParent == null || parentLienLessorHolder == null) {
			toolbar.disableAllButtons();
		}
		else {
			toolbar.refreshButtons((IBusinessEntity) parentLienLessorHolder, vehDetailGrid);
		}
	}
}
