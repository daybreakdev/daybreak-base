package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.towing.dto.AdjustmentDto;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class VehicleAdjustmentList extends AdjustmentList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5559913164296474372L;

	public VehicleAdjustmentList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, OBJECTTYPES.Vehicle);
		// TODO Auto-generated constructor stub
	}

	public boolean loadData(SpecialtyVehicleRisk parent, List<SpecialtyVehicleRiskAdjustment> adjustments) {
		// TODO Auto-generated method stub
		this.parentVehicle = parent;
		
		return super.loadData(adjustments);
	}

	@Override
	protected void loadCoverageList(AdjustmentCoverageList adjCoverageList, AdjustmentDto dto) {
		// TODO Auto-generated method stub
		adjCoverageUIHelper.loadView(adjCoverageList, dto.getAdjustment().getSpecialtyVehicleRisk(), dto.getAdjustment());
	}
}
