package com.echelon.ui.components.trucking;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.SIMMoneyField;
import com.echelon.ui.components.baseclasses.LookupItemValue;
import com.echelon.ui.components.towing.VehicleForm;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.helpers.policy.trucking.LHTVehicleUIHelper;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;

public class LHTVehicleForm extends VehicleForm {

	/**
	 *
	 */
	private static final long serialVersionUID = 4650986441617792228L;
	private static final Logger logger = Logger.getLogger(LHTVehicleForm.class.getName());

	private IntegerField						irpField;
	private RadioButtonGroup<LookupItemValue> 	irpSelectionField;
	private DSComboBox<String>		 			typeOfUnitField;
	private SIMMoneyField		 	 			actualCashValueField;

	private List<LookupItemValue> 				irpLookupItemValues;
	private List<LookupTableItem> typeOfUnitLookups;

	public LHTVehicleForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected VehicleUIHelper createVehicleUIHelper() {
		// TODO Auto-generated method stub
		return new LHTVehicleUIHelper();
	}

	@Override
	protected LHTVehicleUIHelper getVehicleUIHelper() {
		return (LHTVehicleUIHelper) super.getVehicleUIHelper();
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();

		irpField				= new IntegerField();
		irpSelectionField		= new RadioButtonGroup<LookupItemValue>();
		irpSelectionField.setLabel(labels.getString("VehicleFormIRPLabel"));
		typeOfUnitField			= new DSComboBox<String>(labels.getString("VehicleFormTypeOfUnitLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_TypeOfUnits);
		actualCashValueField	= new SIMMoneyField(labels.getString("VehicleFormActualCashValueLabel"));

		binder.forField(irpField).bind(SpecialtyVehicleRisk::getIrp, SpecialtyVehicleRisk::setIrp);
		binder.forField(typeOfUnitField)
			.asRequired()
			.bind(SpecialtyVehicleRisk::getTypeOfUnit, SpecialtyVehicleRisk::setTypeOfUnit);
		binder.forField(actualCashValueField)
									.withNullRepresentation("")
									.withConverter(actualCashValueField.getConverter())
									.bind(SpecialtyVehicleRisk::getActualCashValue, SpecialtyVehicleRisk::setActualCashValue);

		uiHelper.setEditFieldsReadonly(irpSelectionField);
	}

	@Override
	protected FormLayout buildTopForm() {
		// TODO Auto-generated method stub
		FormLayout form = super.buildTopForm();

		form.add(irpSelectionField, typeOfUnitField, actualCashValueField);

		return form;
	}

	@Override
	protected boolean loadDataConfigs(Binder... binders) {
		// TODO Auto-generated method stub
		boolean ok = super.loadDataConfigs(binders);

		irpLookupItemValues = new ArrayList<LookupItemValue>();
		List<LookupTableItem> irpLookups = uiLookupHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_IRPs);
		if (irpLookups != null && !irpLookups.isEmpty()) {
			irpLookupItemValues = uiLookupHelper.convertLookupItemValues(irpLookups);
		}
		irpSelectionField.setItems(irpLookupItemValues);

		typeOfUnitLookups = uiLookupHelper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_TypeOfUnits);

		return ok;
	}

	@Override
	public void loadData(SpecialityAutoSubPolicy parentSubPolicy, SpecialtyVehicleRisk vehicleRisk) {
		// TODO Auto-generated method stub
		super.loadData(parentSubPolicy, vehicleRisk);

		setIrpSelectionValue(vehicleRisk.getIrp());
	}

	@Override
	public void enableEdit() {
		// TODO Auto-generated method stub
		super.enableEdit();
	}

	@Override
	public boolean saveEdit(SpecialtyVehicleRisk vehicleRisk) {
		// TODO Auto-generated method stub

		LookupTableItem itpLookup = irpSelectionField.getValue();
		if (itpLookup == null) {
			irpField.setValue(null);
		}
		else {
			irpField.setValue(Integer.valueOf(itpLookup.getItemKey()));
		}

		return super.saveEdit(vehicleRisk);
	}

	@Override
	protected void unitYearFieldValueChanged(ValueChangeEvent<?> event) {
		// TODO Auto-generated method stub
		// do not trigger getVehicleRateGroup
	}

	@Override
	protected void unitLPNFieldValueChanged(ValueChangeEvent<?> event) {
		// TODO Auto-generated method stub
		// do not trigger getVehicleRateGroup
	}

	protected void actualCashValueFieldValueChanged(ValueChangeEvent<?> event) {
		getVehicleRateGroup();
	}

	/**
	 * Filters the Type of Unit combo box based on the selected vehicle description.
	 */
	protected void filterTypeofUnitFieldBasedOnVehicleDescription(ValueChangeEvent<String> event) {
		typeOfUnitField.clear();

		// Switch statements don't support nulls, so replace null with an empty string.
		final String vehicleDescription = StringUtils.defaultString(event.getValue());
		List<String> allowedTypesOfUnit = new ArrayList<>();
		switch (vehicleDescription) {
		case "HC": // Heavy Commercial
		case "LC": // Light Commercial
		case "TRC": // Tractor
		case "ST": // Straight Truck
			allowedTypesOfUnit.add("Power Unit");
			allowedTypesOfUnit.add("Power Unit Parked");
			break;
		case "TR": // Trailer
			allowedTypesOfUnit.add("Trailer");
			break;
		case "ET": // Excess Trailer"
			allowedTypesOfUnit.add("Excess Trailer");
			break;
		case "PP": // Private Passenger
		case "SHT": // Shunter
			allowedTypesOfUnit.add("Power Unit");
			allowedTypesOfUnit.add("Power Unit Parked");
			allowedTypesOfUnit.add("Others");
			break;
		case "OP27": // OPCF 27B
			allowedTypesOfUnit.add("OPCF27B");
			break;
		default:
			// If we don't recognize the vehicle description, "fail open" by allowing all
			// types of unit
			logger.info("Unknown vehicle description. Skipping type of unit filtering");
			typeOfUnitField.setDSItems(typeOfUnitLookups);
			return;
		}

		List<LookupTableItem> filteredLookupTableItemList = typeOfUnitLookups.stream()
				.filter(item -> allowedTypesOfUnit.contains(item.getItemKey())).collect(Collectors.toList());
		typeOfUnitField.setDSItems(filteredLookupTableItemList);
		if (filteredLookupTableItemList.size() == 1) {
			typeOfUnitField.setDSFirstValue();
		}
	}

	@Override
 	protected void getVehicleRateGroup() {
		getVehicleUIHelper().setVehicleRateGroupFields(actualCashValueField, impliedRateGroupField, selectedRateGroupField);
	}

	@Override
	protected void setupFieldValueChanges() {
		// TODO Auto-generated method stub
		super.setupFieldValueChanges();

		actualCashValueField.addValueChangeListener(this::actualCashValueFieldValueChanged);
		unitExposureField.addValueChangeListener(e -> unitExposureValueChanged());
		vehicleDescField.addValueChangeListener(this::filterTypeofUnitFieldBasedOnVehicleDescription);

		// Add guard for Type of Unit == Power Unit Parked
		// These three fields are the ones that determine whether or not Power Unit
		// Parked is a valid choice. Type of Unit is where the choie is made.
		vehicleClassField.addValueChangeListener(this::verifyPowerUnitParkedIsAllowed);
		unitExposureField.addValueChangeListener(this::verifyPowerUnitParkedIsAllowed);
		vehicleUseField.addValueChangeListener(this::verifyPowerUnitParkedIsAllowed);
		typeOfUnitField.addValueChangeListener(this::verifyPowerUnitParkedIsAllowed);
	}

	//ESL-1831 - codes sync with LHTPOlicyProcesses.setRiskDefaults
	protected void unitExposureValueChanged() {
		Integer irp = 1;

		if (uiPolicyHelper.getCurrentPolicyVersion() != null &&
			Constants.PROVINCE_ONTARIO.equalsIgnoreCase(uiPolicyHelper.getCurrentPolicyVersion().getInsurancePolicy().getControllingJurisdiction()) &&
			Constants.PROVINCE_ONTARIO.equalsIgnoreCase(unitExposureField.getValue())) {
			irp = 0;
		}

		setIrpSelectionValue(irp);
	}

	private void setIrpSelectionValue(Integer irp) {
		irpSelectionField.setValue(null);
		if (irp != null) {
			irpSelectionField.setValue((LookupItemValue) uiLookupHelper.findLookupItemByKey(irp, uiLookupHelper.convertLookupTableItems(irpLookupItemValues), false));
		}
	}
	
	/**
	 * Ensures a type of unit of "Power Unit Parked" is valid. All of the following
	 * conditions must be met:
	 * <ol>
	 * <li>Vehicle class must be 46</li>
	 * <li>Unit exposure must be Ontario Only</li>
	 * <li>Vehicle use must be Storage</li>
	 * </ol>
	 * 
	 * @param event the value change event
	 */
	protected void verifyPowerUnitParkedIsAllowed(ValueChangeEvent<String> event) {
		// Guard against NullPointerExceptions; Java doesn't allow switching on a null
		// value. No logic necessary if Type of Unit has not been selected.
		if (typeOfUnitField.getValue() == null) {
			return;
		}

		switch (typeOfUnitField.getValue()) {
		case "Power Unit Parked":
			// ESL-1940: if the new unit type is Power Unit Parked, error unless all of
			// {class, exposure, vehicle use} match the expected values

			// There's no way to get these values from the config without already knowing
			// what they are. The specified values should be valid according
			// to the config file in which the value would be listed.
			final String expectedVehicleClass = "46"; // prodconfig/lookups/trucking/VehClassList.txt
			final String expectedUnitExposure = "ON"; // prodconfig/lookups/Jurisdictions.json
			final String expectedVehicleUse = "Storage"; // prodconfig/lookups/trucking/VehUseList.txt

			if (!(expectedVehicleClass.equals(vehicleClassField.getValue())
					&& expectedUnitExposure.equals(unitExposureField.getValue())
					&& expectedVehicleUse.equals(vehicleUseField.getValue()))) {
				uiHelper.notificationError(labels.getString("VehicleFormTypeOfUnitPowerUnitParkedInvalidErrorMessage"));
				typeOfUnitField.clear(); // Don't let the invalid value remain selected
			}

			break;
		default:
			break;
		}
	}
}
