package com.echelon.ui.components.entities;

import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.HasCustomer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.button.Button;

public abstract class CustomerView<T extends HasCustomer> extends BaseLayout {
	private static final long serialVersionUID = -2887707756979507169L;

	private CustomerUIHelper<T> customerUIHelper;
	private CustomerForm<T> customerForm;
	private ActionToolbar toolbar;

	protected CustomerView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		if (UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			final DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this, null,
					UICOMPONENTTYPES.Form);
			toolbar = (ActionToolbar) headComponents.getToolbar();
			buildButtonEvents();
		}

		customerUIHelper = createCustomerUIHelper();
		buildComponents();
		buildLayout();
	}

	public void loadData(Customer customer, Address address, PolicyTransaction<?> policyTransaction) {
		final String productCode = policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd();
		customerForm.loadData(customer, address, policyTransaction, productCode);

		refreshToolbar(customer);
	}

	public void enableEdit() {
		customerForm.enableEdit();
	}

	public boolean validateEdit() {
		return customerForm.validateEdit();
	}

	public boolean saveEdit(Customer customer, Address address, PolicyTransaction<?> policyTransaction) {
		return customerForm.saveEdit(customer, address, policyTransaction);
	}

	protected abstract CustomerUIHelper<T> createCustomerUIHelper();

	private void buildButtonEvents() {
		if (toolbar != null) {
			final Button updateButton = toolbar.getButton(UIConstants.UIACTION_Update);
			if (updateButton != null) {
				updateButton.addClickListener(event -> customerUIHelper.updateCustomerViewAction());
			}
		}
	}

	private void buildComponents() {
		customerForm = customerUIHelper.createCustomerForm(uiMode, uiContainerType);
	}

	private void buildLayout() {
		add(customerForm);
	}

	private void refreshToolbar(Customer customer) {
		if (toolbar != null) {
			toolbar.refreshButtons(null, customer);
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
			}

			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
	}
}
