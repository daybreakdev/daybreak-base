package com.echelon.ui.components.baseclasses;

import com.ds.ins.uicommon.components.DSBaseLayout;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.helpers.LayoutUIHelper;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;

@SuppressWarnings("serial")
public abstract class BaseLayout extends DSBaseLayout<UIHelper, ProductConfigUIHelper, LookupUIHelper, LayoutUIHelper> {

	protected PolicyUIHelper uiPolicyHelper;

	public BaseLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseLayout(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void postConstruct() {
		uiPolicyHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		
		super.postConstruct();
	}
}
