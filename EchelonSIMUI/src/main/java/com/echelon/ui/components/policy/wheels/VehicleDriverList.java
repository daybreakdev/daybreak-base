package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverUIHelper;
import com.echelon.ui.helpers.policy.wheels.VehicleDriverUIHelper;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public class VehicleDriverList extends BaseListLayout<VehicleDriver> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8447781291469559418L;
	private VehicleDriverUIHelper 		vehicleDriverUIHelper = new VehicleDriverUIHelper();	
	private DriverUIHelper 				driverUIHelper = new DriverUIHelper();	
	private Grid<VehicleDriver> 		attachedDriverGrid;
	private SpecialtyVehicleRisk		vehicle;

	public VehicleDriverList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}
	
	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("VehicleDriverListTitle"),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	private void buildGrid() {
		attachedDriverGrid = new Grid<VehicleDriver>();
		attachedDriverGrid.setSizeFull();
		uiHelper.setGridStyle(attachedDriverGrid);
		
		createIndicatorColumn(attachedDriverGrid);

		attachedDriverGrid.addColumn(vehDriver -> driverUIHelper.getDriverLabel(vehDriver.getDriver()))
									.setHeader(labels.getString("VehicleDriverListColumnNameLabel"))
									.setAutoWidth(true);
		attachedDriverGrid.addColumn(new DSLookupItemRenderer<VehicleDriver>(vehDriver -> vehDriver.getDriver().getMaritalStatus())
									.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_MaritalStatuses))
									.setHeader(labels.getString("VehicleDriverListColumnMaritalStatusLabel"));
		attachedDriverGrid.addColumn(new DSLookupItemRenderer<VehicleDriver>(vehDriver -> vehDriver.getDriver().getGender())
									.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_Genders))
									.setHeader(labels.getString("VehicleDriverListColumnSexLabel"));
		attachedDriverGrid.addColumn(vehDriver -> driverUIHelper.getYearsLicensed(vehDriver.getDriver().getDateLicensed()))
									.setHeader(labels.getString("VehicleDriverListColumnYearsLicensedLabel"));
		attachedDriverGrid.addColumn(new DSLookupItemRenderer<VehicleDriver>(VehicleDriver::getDriverType)
									.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_AttachedDriverType))
									.setHeader(labels.getString("VehicleDriverListColumnDriverTypeLabel"));
		
		attachedDriverGrid.setSelectionMode(SelectionMode.SINGLE);
		attachedDriverGrid.addSelectionListener(event -> refreshToolbar());
		attachedDriverGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		setUpGridDbClickEvent(attachedDriverGrid);
		
		this.add(attachedDriverGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				vehicleDriverUIHelper.newVehicleDriverAction(vehicle);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (attachedDriverGrid.asSingleSelect() != null) {
					vehicleDriverUIHelper.updateVehicleDriverAction(vehicle, attachedDriverGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (attachedDriverGrid.asSingleSelect() != null) {
					vehicleDriverUIHelper.deleteVehicleDriverAction(vehicle, attachedDriverGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (attachedDriverGrid.asSingleSelect() != null) {
					vehicleDriverUIHelper.undeleteVehicleDriverAction(vehicle, attachedDriverGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(SpecialtyVehicleRisk vehicle, Set<VehicleDriver> set) {
		this.vehicle = vehicle;
		List<VehicleDriver> list = vehicleDriverUIHelper.sortDriverList(set);

		this.attachedDriverGrid.setItems(list);
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(uiPolicyHelper.getCurrentPolicyTransaction(), attachedDriverGrid);
	}

}


