package com.echelon.ui.components.towing;

import org.apache.commons.lang3.StringUtils;

import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.prodconf.interfaces.ILifecycleStatusConfig;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.dto.DSActionDto;

/**
 * Adjustments require some special handling: they can only be created when the
 * policy is in the pending adjustment phase.
 */
public class AdjustmentActionToolbar extends ActionToolbar {
	private static final long serialVersionUID = 2716895261949278524L;

	public AdjustmentActionToolbar(UICOMPONENTTYPES uiComponentType) {
		super(uiComponentType);
	}

	public AdjustmentActionToolbar(UICOMPONENTTYPES uiComponentType, String... actions) {
		super(uiComponentType, actions);
	}

	public AdjustmentActionToolbar(UICOMPONENTTYPES uiComponentType, boolean isButtonTextAndIcon,
			DSActionDto... actionAndIcons) {
		super(uiComponentType, isButtonTextAndIcon, actionAndIcons);
	}

	@Override
	protected boolean isCurrentTransactionEditable() {
		final ProductConfigUIHelper configUIHelper = BrokerUI.getHelperSession().getConfigUIHelper();
		final ILifecycleStatusConfig lifecycleStatus = configUIHelper.getCurrentLifecycleStatusConfig();

		if (lifecycleStatus != null && StringUtils.equalsIgnoreCase("PENDINGADJUSTMENT", lifecycleStatus.getLifecycleStatus())) {
			return true;
		} else {
			return false;
		}
	}
}
