package com.echelon.ui.components.baseclasses.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.prodconf.baseclasses.NavigationTreeConfig;
import com.ds.ins.prodconf.baseclasses.NavigationTreeItemConfig;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.treegrid.CollapseEvent;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.function.ValueProvider;

public class BasePolicyTreeNavigator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 904896093658479209L;
	protected TreeGrid<DSTreeGridItem> 	treeGrid 		= null;
	protected ResourceBundle			labels			= null;
	protected UIHelper					uiHelper;
	protected PolicyUIHelper			uiPolicyHelper;
	protected List<DSTreeGridItem> 		rootItems		= null;
	protected PolicyVersion				policyVersion;
	protected PolicyTransaction			policyTransaction;
	
	protected ValueProvider<DSTreeGridItem, Collection<DSTreeGridItem>> treeValueProvider;
	
	public BasePolicyTreeNavigator() {
		super();
		// TODO Auto-generated constructor stub
		
		this.uiHelper  	 	= BrokerUI.getHelperSession().getUiHelper();
		this.uiPolicyHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		initializeNavigator();
		buildTree();
	}
	
	public TreeGrid<DSTreeGridItem> getTreeGrid() {
		return treeGrid;
	}

	public List<DSTreeGridItem> getRootItems() {
		return rootItems;
	}
	
	private void initializeNavigator() {
		labels = uiHelper.getSessionLabels();		
		
		treeValueProvider = new ValueProvider<DSTreeGridItem, Collection<DSTreeGridItem>>() {
			
			@Override
			public Collection<DSTreeGridItem> apply(DSTreeGridItem source) {
				// TODO Auto-generated method stub
				return source.getChildItems();
			}
		};
	}

	private void buildTree() {
		treeGrid = new TreeGrid<DSTreeGridItem>();
		
		buildTreeColumn();
		//treeGrid.addItemClickListener(o -> System.out.println("tree click"));
		//treeGrid.addSelectionListener(o -> System.out.println("tree select"));
		treeGrid.addCollapseListener(new ComponentEventListener<CollapseEvent<DSTreeGridItem,TreeGrid<DSTreeGridItem>>>() {
			
			@Override
			public void onComponentEvent(CollapseEvent<DSTreeGridItem, TreeGrid<DSTreeGridItem>> event) {
				if (event.getItems() != null && !event.getItems().isEmpty()) {
					treeGrid.expand(event.getItems());
				}
			}
			
		});
	}
	
	protected void buildTreeColumn() {
		//treeGrid.addHierarchyColumn(DSTreeGridItem::getNodeText);
		treeGrid.addComponentHierarchyColumn(o -> {
				Label label = getTreeNodeLabel(o);
				Icon  icon  = getTreeNodeIcon(o);
				
				Div div = new Div();
				if (icon != null) {
					div.add(icon);
				}
				div.add(label);
				div.addClickListener(event -> {
						treeGrid.select(o);
					});
				return div;
			});
	}
	
	protected Label getTreeNodeLabel(DSTreeGridItem treeGridItem) {
		Label label = new Label(treeGridItem.getNodeText());
		return label;
	}
	
	protected Icon getTreeNodeIcon(DSTreeGridItem treeGridItem) {
		Icon icon = UIFactory.getInstance().getPolicyTreeNodeIcon(treeGridItem);
		return icon;
	}

	protected NavigationTreeConfig getTreeConfig() {
		return BrokerUI.getHelperSession().getConfigUIHelper().getCurrentNavigationTreeConfig();
	}
	
	public void loadData(PolicyVersion policyVersion, PolicyTransaction policyTransaction) {
		this.policyVersion 		= policyVersion;
		this.policyTransaction 	= policyTransaction;
		
		rootItems = new ArrayList<DSTreeGridItem>();
		if (treeGrid != null) {
			treeGrid.setItems(new ArrayList<DSTreeGridItem>());
		}
		
		buildTree();
		
		NavigationTreeConfig treeConfig = getTreeConfig();
		
		createRootItems(treeConfig, rootItems);
		treeGrid.setItems(rootItems, treeValueProvider);
		
		expandNodes(rootItems);
	}
	
	protected void createRootItems(NavigationTreeConfig treeConfig, List<DSTreeGridItem> rootItems) {
		if (treeConfig != null && treeConfig.getRootNodes() != null && !treeConfig.getRootNodes().isEmpty()) {
			for(NavigationTreeItemConfig rootItemConfig : treeConfig.getRootNodes()) {
				DSTreeGridItem rootItem = createTreeItem((DSTreeGridItem)null, rootItemConfig, policyTransaction);
				rootItems.add(rootItem);
				
				createChildItems(rootItemConfig, rootItem, policyTransaction);
			}
		}
	}
	
	protected void createChildItems(NavigationTreeItemConfig parentItemConfig, DSTreeGridItem parentItem, Object parentDataObject) {
		if (parentItemConfig.getChildNodes() != null && !parentItemConfig.getChildNodes().isEmpty()) {
			for(NavigationTreeItemConfig itemConfig : parentItemConfig.getChildNodes()) {
				createChildItemsForParent(parentItemConfig, parentItem, parentDataObject, itemConfig);
			}
		}
	}
	
	// This can include a loop to create child items for the parent data object
	protected void createChildItemsForParent(NavigationTreeItemConfig parentItemConfig, DSTreeGridItem parentItem, Object parentDataObject,
												NavigationTreeItemConfig itemConfig) {
		DSTreeGridItem item = createTreeItem(parentItem, itemConfig, parentDataObject);
		if (item != null) {
			parentItem.addChildItem(item);
			
			createChildItems(itemConfig, item, item.getNodeObject());
		}
	}
	
	protected DSTreeGridItem createTreeItem(DSTreeGridItem parentItem, NavigationTreeItemConfig treeItemConfig, Object dataObject) {
		DSTreeGridItem treeItem = new DSTreeGridItem(parentItem, treeItemConfig.getNodeType(), treeItemConfig.getUiName(), dataObject);
		treeItem.setNodeText(treeItemConfig.getNodeName(UI.getCurrent().getLocale()));
		return treeItem;
	}
	
	protected void expandNodes(List<DSTreeGridItem> items) {
		treeGrid.expand(items);
		
		for(DSTreeGridItem item : items) {
			expandNodes(item.getChildItems());
		}
	}
	
	public DSTreeGridItem searchTreeItem(String itemNodeType, Object itemNodeObject) {
		for(DSTreeGridItem item : treeGrid.getTreeData().getRootItems()) {
			if (isMatch(item, itemNodeType, itemNodeObject)) {
				return item;
			}
			
			DSTreeGridItem childItem = searchTreeItem(item, itemNodeType, itemNodeObject);
			if (childItem != null) {
				treeGrid.expand(item);
				return childItem;
			}
		}
		
		return null;
	}
	
	private DSTreeGridItem searchTreeItem(DSTreeGridItem parentItem, String itemNodeType, Object itemNodeObject) {
		for(DSTreeGridItem item : parentItem.getChildItems()) {
			if (isMatch(item, itemNodeType, itemNodeObject)) {
				return item;
			}
			
			DSTreeGridItem childItem = searchTreeItem(item, itemNodeType, itemNodeObject);
			if (childItem != null) {
				treeGrid.expand(item);
				return childItem;
			}
		}
		
		return null;
	}
	
	protected boolean isMatch(DSTreeGridItem item, String itemNodeType, Object itemNodeObject) {
		boolean isOk = itemNodeType.equalsIgnoreCase(item.getNodeType());
		return isOk;
	}
}
