package com.echelon.ui.components.baseclasses.policy.reinsurance;

import java.util.ResourceBundle;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Reinsurer;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.uicommon.components.DSComboBox;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.AddressForm;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;

/**
 * Dialog for viewing details about a {@link PolicyReinsurer}.
 */
public class PolicyReinsurerViewDialog extends Dialog {
	private static final long serialVersionUID = 120643512723490499L;

	private final PolicyReinsurer policyReinsurer;
	private ResourceBundle labels;

	public PolicyReinsurerViewDialog(PolicyReinsurer policyReinsurer, ResourceBundle labels) {
		this.policyReinsurer = policyReinsurer;
		this.labels = labels;

		Button close = new Button(UIConstants.UIACTION_Close);
		close.addClickListener(event -> close());

		VerticalLayout layout = new VerticalLayout(buildForm(), close);
		layout.setAlignSelf(Alignment.END, close);

		add(layout);
	}

	private FormLayout buildForm() {
		final Reinsurer reinsurer = policyReinsurer.getReinsurer();

		Component legalName = createTextField(labels.getString("PolicyReinsurerLegalNameLabel"),
			reinsurer.getLegalName());
		Component mainEmail = createEmailField(
			labels.getString("PolicyReinsurerViewDialogEmailLabel"),
			reinsurer.getMainEmailAddress());
		Component phoneNumber = createTextField(
			labels.getString("PolicyReinsurerViewDialogPhoneNumberLabel"),
			reinsurer.getPhoneNumber());
		Component preferredLang = createComboBox(
			labels.getString("CommercialCustomerLanguageLabel"), String.class,
			ConfigConstants.LOOKUPTABLE_Languages, reinsurer.getPreferredLanguage());

		Address address = reinsurer.getLocations().iterator().next();
		AddressForm addressForm = new AddressForm();
		addressForm.loadData(address);

		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4));
		layout.add(legalName, 2);
		layout.add(mainEmail, 2);
		layout.add(phoneNumber, 2);
		layout.add(preferredLang, 2);
		layout.add(addressForm, 4);

		return layout;
	}

	private TextField createTextField(String label, String value) {
		if (value == null) {
			// TextFields don't support null values
			value = "";
		}

		TextField field = new TextField(label);
		field.setValue(value);
		field.setReadOnly(true);

		return field;
	}

	private EmailField createEmailField(String label, String value) {
		if (value == null) {
			// EmailField is a subclass of TextField, which doesn't support null values
			value = "";
		}

		EmailField field = new EmailField(label);
		field.setValue(value);
		field.setReadOnly(true);

		return field;
	}

	private <MODEL> DSComboBox<MODEL> createComboBox(String label, Class<?> modelClass,
		String lookupTableId, MODEL value) {
		DSComboBox<MODEL> field = new DSComboBox<MODEL>(label, modelClass)
			.withLookupTableId(lookupTableId);
		field.loadDSItems();
		field.setValue(value);
		field.setReadOnly(true);

		return field;
	}
}
