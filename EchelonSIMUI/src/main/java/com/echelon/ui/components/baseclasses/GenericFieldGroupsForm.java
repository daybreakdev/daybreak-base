package com.echelon.ui.components.baseclasses;

import com.ds.ins.domain.entities.DataExtension;
import com.ds.ins.prodconf.interfaces.IDataExtensionConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericUIConfig;
import com.ds.ins.uicommon.components.DSGenericFieldGroupsForm;
import com.ds.ins.uicommon.components.DSGenericFieldsForm;
import com.ds.ins.uicommon.components.DSGenericFieldsList;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSGenericFieldGroupDto;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.session.UIFactory;

public class GenericFieldGroupsForm extends DSGenericFieldGroupsForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2118883785045520860L;
	protected CoverageDto coverageDto;

	public GenericFieldGroupsForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSGenericFieldsList createGenericFieldsList(DSGenericFieldGroupDto groupDto) {
		// TODO Auto-generated method stub
		return new GenericFieldsList(uiMode, uiContainerType, groupDto, followFormLayout);
	}

	@Override
	protected DSGenericFieldsForm createGenericFieldsForm(DSGenericFieldGroupDto groupDto) {
		// TODO Auto-generated method stub
		return UIFactory.getInstance().getGenericFieldsForm(uiMode, uiContainerType, groupDto, followFormLayout, coverageDto);
	}

	public void loadData(IDataExtensionConfigParent dataExtensionConfigParent, DataExtension dataExtension,
			IGenericUIConfig uiConfig, CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		this.coverageDto = coverageDto;
		super.loadData(dataExtensionConfigParent, dataExtension, uiConfig);
	}

}
