package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.baseclasses.policy.BaseRiskList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.LocationUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.router.Route;

@Route(value="LocationsContentView", layout = BasePolicyView.class)
public class LocationList extends BaseRiskList<PolicyLocation> implements IPolicyViewContent  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 541047082714095075L;
	private LocationUIHelper locationUIHelper = new LocationUIHelper();

	public LocationList() {
		super();
		initializeLayouts(labels.getString("LocationDetailsTitle"));
		buildGrid();
	}

	public LocationList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeLayouts(labels.getString("LocationDetailsTitle"));
		buildGrid();
	}

	public LocationList(String uiMode, UICONTAINERTYPES uiContainerType, String title) {
		super(uiMode, uiContainerType, title);
	}

	@Override
	protected void initializeLayouts(String title) {
		// TODO Auto-generated method stub
		super.initializeLayouts(title);
	}

	@Override
	protected void addGridColumns() {
		createIndicatorColumn(riskGrid);
		riskGrid.addColumn(PolicyLocation::getLocationId).setHeader(labels.getString("LocationNumLabel")).setSortable(true);
		riskGrid.addColumn(loc -> loc.getLocationAddress().getAddressLine1()).setHeader(labels.getString("LocationAddressLabel"));
		riskGrid.addColumn(loc -> loc.getLocationAddress().getCity()).setHeader(labels.getString("LocationCityLabel"));
		riskGrid.addColumn(locationUIHelper.getRenderer(uiLookupHelper)).setHeader(labels.getString("LocationProvinceLabel"));
		riskGrid.addColumn(loc -> loc.getLocationAddress().getPostalZip()).setHeader(labels.getString("LocationPostalCodeLabel"));
	}

	@Override
	protected String[] getListActions() {
		return new String[] {DSUIConstants.UIACTION_New,
								DSUIConstants.UIACTION_Update, 
								DSUIConstants.UIACTION_Delete,
								DSUIConstants.UIACTION_Copy,
								DSUIConstants.UIACTION_View};
	}

	@Override
	protected void setupNewButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_New);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				locationUIHelper.newLocationAction();
			});
		}
	}

	@Override
	protected void setupUpdateButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_Update);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					PolicyLocation currLocation = riskGrid.asSingleSelect().getValue();
					locationUIHelper.updateLocationAction(currLocation);
				}
			});
		}
	}

	@Override
	protected void setupDeleteButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_Delete);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					PolicyLocation currLocation = riskGrid.asSingleSelect().getValue();
					locationUIHelper.deleteLocationAction(currLocation);
				}
			});
		}
		
		setupUndeleteButtonEvent();
	}

	protected void setupUndeleteButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_Undelete);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					PolicyLocation currLocation = riskGrid.asSingleSelect().getValue();
					locationUIHelper.undeleteLocationAction(currLocation);
				}
			});
		}
	}

	@Override
	protected void setupViewButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_View);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					PolicyLocation currLocation = riskGrid.asSingleSelect().getValue();
					locationUIHelper.viewLocationAction(currLocation);
				}
			});
		}
	}

	@Override
	protected void setupCopyButtonEvent() {
		Button bttn = toolbar.getButton(UIConstants.UIACTION_Copy);
		if (bttn != null) {
			bttn.addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					PolicyLocation currLocation = riskGrid.asSingleSelect().getValue();
					locationUIHelper.copyLocationAction(currLocation);
				}
			});
		}
	}

	@SuppressWarnings("unchecked")
	public boolean loadData(Set<? extends Risk> set) {
		List<PolicyLocation> list = new ArrayList<PolicyLocation>();
		if (set != null) {
			list.addAll((Collection<? extends PolicyLocation>) set);
		}
		return loadData(list);
	}

	public boolean loadData(List<PolicyLocation> list) {
		if (list == null) {
			list = new ArrayList<PolicyLocation>();
		}
		else if (UIConstants.UIACTION_Select.equals(uiMode)) {
			list = list.stream().filter(o -> !o.isDeleted() && !o.wasDeleted()).collect(Collectors.toList());
		}
		
		PolicyLocation mailingLoc = list.stream().filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId())).findFirst().orElse(null);
		if (mailingLoc != null) {
			list.remove(mailingLoc);
			list = list.stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toList());
			list.add(0, mailingLoc);
		}
		else {
			list = list.stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toList());
		}
		
		riskGrid.setItems(list);
		refreshToolbar();
		return true;
	}

	@Override
	protected void refreshToolbar() {
		super.refreshToolbar(null);
		refreshDeleteButton();
	}
	
	protected void refreshDeleteButton() {
		if (!riskGrid.getSelectedItems().isEmpty()) {
			PolicyLocation selectedLocation = riskGrid.asSingleSelect().getValue();
			boolean mainLocation = locationUIHelper.isMainLocation(selectedLocation);
			if (toolbar.getButton(UIConstants.UIACTION_Update).isEnabled()) {
				toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(!mainLocation);
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete).isEnabled()) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(!mainLocation);
			}
			if (toolbar.getButton(UIConstants.UIACTION_Copy).isEnabled()) {
				toolbar.getButton(UIConstants.UIACTION_Copy).setEnabled(mainLocation);
			}
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		locationUIHelper.loadView(this, nodeItem);
	}

	@Override
	protected Icon getColumnIcon(IBusinessEntity busEntity) {
		// TODO Auto-generated method stub
		Icon icon = super.getColumnIcon(busEntity);
		
		if (icon == null && busEntity != null) {
			if (busEntity instanceof PolicyLocation) {
				PolicyLocation ai = (PolicyLocation) busEntity;
				icon = super.getColumnIcon(ai.getLocationAddress());
			}
		}
		
		return icon;
	}

}
