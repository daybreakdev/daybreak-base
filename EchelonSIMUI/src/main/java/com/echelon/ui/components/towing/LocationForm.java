package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.DSPostalZipCodeField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSToUpperCaseConverter;
import com.ds.ins.utils.Constants;
import com.echelon.ui.helpers.policy.towing.LocationUIHelper;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class LocationForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3588019061221504222L;
	private Binder<PolicyLocation> 	locBinder;
	private TextField				locationNumField;
	private TextField				addressField;
	private TextField				cityField;
	private DSPostalZipCodeField	postalCodeField;
	private DSComboBox<String>		provinceField;
	private Checkbox 				isVehiclesInBuildingField;
	private Checkbox 				isVehiclesOnLotField;
	private DSNumberField<Integer> 	numVehiclesField;
	private boolean					garageFieldsRequired;
	private LocationUIHelper 		locationUIHelper = new LocationUIHelper();

	public LocationForm(String uiMode, UICONTAINERTYPES uiContainerType, GarageSubPolicy<?> garageSubPolicy) {
		super(uiMode, uiContainerType);
		garageFieldsRequired = garageSubPolicy != null;
		buildFields();
		buildForm();
	}

	private void buildFields() {
		locationNumField = new TextField(labels.getString("LocationNumLabel"));
		addressField = new TextField(labels.getString("LocationAddressLabel"));
		cityField = new TextField(labels.getString("LocationCityLabel"));
		postalCodeField = new DSPostalZipCodeField(labels.getString("LocationPostalCodeLabel"));
		provinceField = new DSComboBox<String>(labels.getString("LocationProvinceLabel"), String.class)
						.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		isVehiclesInBuildingField = new Checkbox(labels.getString("LocationBuildignLabel"));
		isVehiclesOnLotField = new Checkbox(labels.getString("LocationLotLabel"));
		numVehiclesField = new DSNumberField<Integer>("", Integer.class);

		locBinder = new Binder<PolicyLocation>(PolicyLocation.class);
		locBinder.forField(locationNumField)
				.asRequired()
				.withNullRepresentation("")
				.withConverter(new DSToUpperCaseConverter())
				.bind(PolicyLocation::getLocationId, PolicyLocation::setLocationId);
		locBinder.forField(addressField)
				.asRequired()
				.withNullRepresentation("")
				.bind(loc -> loc.getLocationAddress().getAddressLine1(), (loc, address) -> loc.getLocationAddress().setAddressLine1(address));
		locBinder.forField(cityField)
				.asRequired()
				.withNullRepresentation("")
				.bind(loc -> loc.getLocationAddress().getCity(), (loc, city) -> loc.getLocationAddress().setCity(city));
		locBinder.forField(postalCodeField)
		        .asRequired()
		        .withNullRepresentation("")
				.withConverter(postalCodeField.getConverter())
				.withValidator(postalCodeField.getValidator())
				.bind(loc -> loc.getLocationAddress().getPostalZip(), (loc, code) -> loc.getLocationAddress().setPostalZip(code));
		locBinder.forField(provinceField)
				.asRequired()
				.bind(loc -> loc.getLocationAddress().getProvState(), (loc, province) -> loc.getLocationAddress().setProvState(province));
		locBinder.forField(isVehiclesInBuildingField)
//				.withValidator(val -> validateGarageFields(val), labels.getString("LocationGarageFieldRequired"))
				.bind(PolicyLocation::getIsVehiclesInBuilding, PolicyLocation::setIsVehiclesInBuilding);
		locBinder.forField(isVehiclesOnLotField)
//				.withValidator(val -> validateGarageFields(val), labels.getString("LocationGarageFieldRequired"))
				.bind(PolicyLocation::getIsVehiclesOnLot, PolicyLocation::setIsVehiclesOnLot);
		locBinder.forField(numVehiclesField)
				.withNullRepresentation("")
				//.withValidator(val -> validateGarageFields(val), labels.getString("LocationGarageFieldRequired"))
				.withConverter(numVehiclesField.getConverter())
				.bind(PolicyLocation::getNumVehicles, PolicyLocation::setNumVehicles);
		locBinder.setReadOnly(true);
	}

	private void buildForm() {
		Label label = null;
		FormLayout form1 = new FormLayout();
		FormLayout form2 = new FormLayout();
		HorizontalLayout hLayout = new HorizontalLayout();

		form1.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 5)
				);
		form2.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 5)
				);
		form1.add(locationNumField, addressField, cityField, provinceField, postalCodeField);
		form2.add(isVehiclesInBuildingField, isVehiclesOnLotField);
		form2.addFormItem(numVehiclesField, labels.getString("LocationNumVehiclesLabel"));
		label = new Label(labels.getString("LocationForGarageSubPolicyLabel"));
		setLabelStyle(label);
		hLayout.setAlignItems(Alignment.START);

		add(form1);
		hLayout.add(label, form2);
		add(hLayout);
	}

	public void loadData(PolicyLocation location) {
		PolicyLocation loc = location;
		boolean isNew = false;
		if (loc == null) {
			loc = new PolicyLocation();
			isNew = true;
		}
		loadDataConfigs(locBinder);
		locBinder.readBean(loc);
		if (isNew) {
			provinceField.setDSValue(Constants.PROVINCE_ONTARIO);
		}
	
		enableDataView(locBinder);
	}

	public boolean validate() {
		return uiHelper.validateValues(locBinder);
	}

	public boolean update(PolicyLocation location) {
		return uiHelper.writeValues(locBinder, location);
	}

	public boolean save(PolicyLocation location) {
		return uiHelper.writeValues(locBinder, location);
	}

	public void enableEdit(PolicyLocation location) {
		enableDataEdit(locBinder);
		
		checkConditions(location);
		
		setFocus();
	}

	private void checkConditions(PolicyLocation location) {
		boolean isMainLoc = locationUIHelper.isMainLocation(location);
		if (isMainLoc) {
			uiHelper.setEditFieldsReadonly(locationNumField);
		}
	}

	private boolean validateGarageFields(String val) {
		return (garageFieldsRequired && val != null) || !garageFieldsRequired;
	}

	private void setLabelStyle(Label label) {
		label.getElement().getStyle().set("font-size", "12px");
		label.getElement().getStyle().set("font-weight", "bold");
		label.getElement().getStyle().set("max-width", "100px");
	}
	
	public void setFocus() {
		if (locationNumField.isReadOnly()) {
			addressField.focus();
		}
		else {
			locationNumField.focus();
		}
	}

}
