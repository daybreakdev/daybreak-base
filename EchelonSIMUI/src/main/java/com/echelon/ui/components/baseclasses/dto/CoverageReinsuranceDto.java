package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.CoverageReinsurance;
import com.ds.ins.domain.policy.EndorsementReinsurance;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.Premium;

public class CoverageReinsuranceDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6064183710571841235L;
	private CoverageDto					coverageDto;
	private CoverageReinsurance 		covReinsurance;
	private EndorsementReinsurance		endReinsurance;

	private 		boolean				isDeleted;
	private 		boolean				isUpdated;
	private 		boolean				isNew;
	private			boolean				isUndeleted;

	public CoverageReinsuranceDto(CoverageDto coverageDto) {
		super();
		// TODO Auto-generated constructor stub
		this.coverageDto = coverageDto;
	}

	public CoverageReinsuranceDto(CoverageDto coverageDto, CoverageReinsurance covReinsurance) {
		this(coverageDto);
		// TODO Auto-generated constructor stub
		
		setCovReinsurance(covReinsurance);
	}

	public CoverageReinsuranceDto(CoverageDto coverageDto, EndorsementReinsurance endReinsurance) {
		this(coverageDto);
		// TODO Auto-generated constructor stub
		
		setEndReinsurance(endReinsurance);
	}

	public CoverageDto getCoverageDto() {
		return coverageDto;
	}

	public CoverageReinsurance getCovReinsurance() {
		return covReinsurance;
	}
	
	public EndorsementReinsurance getEndorsementReinsurance() {
		return endReinsurance;
	}

	public void setCovReinsurance(CoverageReinsurance covReinsurance) {
		this.covReinsurance 		= covReinsurance;
	}

	public EndorsementReinsurance getEndReinsurance() {
		return endReinsurance;
	}

	public void setEndReinsurance(EndorsementReinsurance endReinsurance) {
		this.endReinsurance = endReinsurance;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public boolean isUpdated() {
		return isUpdated;
	}

	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	public boolean isDeleted() {
		return isDeleted;
	}

	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public boolean isUndeleted() {
		return isUndeleted;
	}

	public void setUndeleted(boolean isUndeleted) {
		this.isUndeleted = isUndeleted;
	}

	// Created from previous version and deleted from this version
	// Was deleted
	public boolean includeThisDeleted() {
		return ((isDeleted() && getAuditable() != null && getAuditable().isActive())
				|| (getAuditable() != null && getAuditable().isDeleted())
				|| (getAuditable() != null && getAuditable().wasDeleted()));
	}

	public boolean isPendingOrActive() {
		return isUndeleted() || (!isDeleted() && !includeThisDeleted());
	}

	public Boolean getIsPrimaryInsurer() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getIsPrimaryInsurer();
		}
		else if (this.endReinsurance != null) {
			return null;
		}
		
		return null;
	}

	public void setIsPrimaryInsurer(Boolean isPrimaryInsurer) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setIsPrimaryInsurer(isPrimaryInsurer);
		}
		else if (this.endReinsurance != null) {
			
		}
	}

	public PolicyReinsurer getPolicyReinsurer() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getPolicyReinsurer();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getPolicyReinsurer();
		}
		
		return null;
	}

	public void setPolicyReinsurer(PolicyReinsurer policyReinsurer) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setPolicyReinsurer(policyReinsurer);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setPolicyReinsurer(policyReinsurer);
		}
	}

	public Long getStartLayer() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getStartLayer();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getStartLayer();
		}
		
		return null;
	}

	public void setStartLayer(Long startLayer) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setStartLayer(startLayer);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setStartLayer(startLayer);
		}
	}

	public Long getEndLayer() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getEndLayer();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getEndLayer();
		}
		
		return null;
	}

	public void setEndLayer(Long endLayer) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setEndLayer(endLayer);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setEndLayer(endLayer);
		}
	}

	public Double getLayerPercentage() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getLayerPercentage();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getLayerPercentage();
		}
		
		return null;
	}

	public void setLayerPercentage(Double layerPercentage) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setLayerPercentage(layerPercentage);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setLayerPercentage(layerPercentage);
		}
	}
	
	public Double getReinsurancePremium() {
		if (this.covReinsurance != null && this.covReinsurance.getCovReinsurancePremium() != null) {
			return this.covReinsurance.getCovReinsurancePremium().getAnnualPremium();
		}
		else if (this.endReinsurance != null && this.endReinsurance.getEndReinsurancePremium() != null) {
			return this.endReinsurance.getEndReinsurancePremium().getAnnualPremium();
		}
		
		return null;
	}
	
	public void setReinsurancePremium(Double reinsurancePremium) {
		if (this.covReinsurance != null && this.covReinsurance.getCovReinsurancePremium() != null) {
			this.covReinsurance.getCovReinsurancePremium().setAnnualPremium(reinsurancePremium);
		}
		else if (this.endReinsurance != null && this.endReinsurance.getEndReinsurancePremium() != null) {
			this.endReinsurance.getEndReinsurancePremium().setAnnualPremium(reinsurancePremium);
		}
	}

	public Double getGrossUpPercentage() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getGrossUpPercentage();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getGrossUpPercentage();
		}
		
		return null;
	}

	public void setGrossUpPercentage(Double grossUpPercentage) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setGrossUpPercentage(grossUpPercentage);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setGrossUpPercentage(grossUpPercentage);
		}
	}

	public Double getGrossUpAmount() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getGrossUpAmount();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getGrossUpAmount();
		}
		
		return null;
	}

	public void setGrossUpAmount(Double grossUpAmount) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setGrossUpAmount(grossUpAmount);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setGrossUpAmount(grossUpAmount);
		}
	}

	public Double getInsurerPremium() {
		if (this.covReinsurance != null) {
			return this.covReinsurance.getInsurerPremium();
		}
		else if (this.endReinsurance != null) {
			return this.endReinsurance.getInsurerPremium();
		}
		
		return null;
	}

	public void setInsurerPremium(Double insurerPremium) {
		if (this.covReinsurance != null) {
			this.covReinsurance.setInsurerPremium(insurerPremium);
		}
		else if (this.endReinsurance != null) {
			this.endReinsurance.setInsurerPremium(insurerPremium);
		}
		
		syncAnnualPremium();
	}

	public long getLayerLimts() {
		if (this.getEndLayer() == null) {
			return 0;
		}
		
		if (this.getStartLayer() == null) {
			return this.getEndLayer();
		}
				
		return this.getEndLayer() - this.getStartLayer() + 1;
	}

	public IAuditable getAuditable() {
		if (this.covReinsurance != null) {
			return this.covReinsurance;
		} else if (this.endReinsurance != null) {
			return this.endReinsurance;
		}

		return null;
	}
	
	public Boolean getIsPremiumOverride() {
		Boolean result = null;
		
		Premium premium = new Premium();
		if (this.covReinsurance != null) {
			premium = this.covReinsurance.getCovReinsurancePremium();
		}
		else if (this.endReinsurance != null) {
			premium = this.endReinsurance.getEndReinsurancePremium();
		}
		
		if (premium != null) {
			result = premium.isPremiumOverride();
		}
		
		return result;
	}
	
	public void setIsPremiumOverride(Boolean value) {
		Premium premium = new Premium();
		if (this.covReinsurance != null) {
			premium = this.covReinsurance.getCovReinsurancePremium();
		}
		else if (this.endReinsurance != null) {
			premium = this.endReinsurance.getEndReinsurancePremium();
		}
		
		if (premium != null) {
			premium.setPremiumOverride(value);
		}
	}
	
	public Double getPrimaryOriginalPremium() {
		Double result = null;
		
		Premium premium = new Premium();
		if (this.covReinsurance != null) {
			if (BooleanUtils.isTrue(this.covReinsurance.getIsPrimaryInsurer())) {
				premium = this.covReinsurance.getCovReinsurancePremium();
			}
		}
		else if (this.endReinsurance != null) {
			if (BooleanUtils.isTrue(this.endReinsurance.getIsPrimaryInsurer())) {
				premium = this.endReinsurance.getEndReinsurancePremium();
			}
		}
		
		if (premium != null) {
			result = premium.getOriginalPremium();
		}
		
		return result;
	}

	public String getPolicyReinsurerName() {
		if (this.covReinsurance != null) {
			if (this.covReinsurance.getPolicyReinsurer() != null) {
				return this.covReinsurance.getPolicyReinsurer().getReinsurer().getLegalName();
			}
		}
		else if (this.endReinsurance != null) {
			if (this.covReinsurance.getPolicyReinsurer() != null) {
				return this.endReinsurance.getPolicyReinsurer().getReinsurer().getLegalName();
			}
		}

		return null;
	}
	
	public void syncAnnualPremium() {
		if (this.covReinsurance != null) {
			if (this.covReinsurance.getCovReinsurancePremium() != null) {
				this.covReinsurance.getCovReinsurancePremium().setAnnualPremium(this.covReinsurance.getInsurerPremium());
			}
		}
		else if (this.endReinsurance != null) {
			if (this.endReinsurance.getEndReinsurancePremium() != null) {
				this.endReinsurance.getEndReinsurancePremium().setAnnualPremium(this.endReinsurance.getInsurerPremium());
			}
		}
	}	
}

