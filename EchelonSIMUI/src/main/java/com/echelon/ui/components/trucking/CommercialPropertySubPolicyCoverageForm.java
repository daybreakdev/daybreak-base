package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class CommercialPropertySubPolicyCoverageForm extends BaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5647298123192133181L;

	public CommercialPropertySubPolicyCoverageForm(String uiMode, UICONTAINERTYPES uiContainerType,
			OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected VerticalLayout attachIsPremOvrAndIsReinsuredToForm(FormLayout form, int assignedColumns) {
		// TODO Auto-generated method stub
		VerticalLayout fieldLayout = super.attachIsPremOvrAndIsReinsuredToForm(form, assignedColumns);
		if (fieldLayout != null) {
			fieldLayout.getElement().getStyle().set("margin-top","27px");
		}
		return fieldLayout;
	}
	
}
