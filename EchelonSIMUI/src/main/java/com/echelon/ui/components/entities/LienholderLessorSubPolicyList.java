package com.echelon.ui.components.entities;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.helpers.entities.LienholderLessorSubPolicyUIHelper;
import com.echelon.ui.renderers.AddressRenderer;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.Risk;

public class LienholderLessorSubPolicyList extends LienholderLessorList<LienholderLessorSubPolicy, 
																SpecialityAutoSubPolicy<Risk>,
																LienholderLessorSubPolicyUIHelper> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4207347667888647992L;
	private HashMap<Long, Integer> linLessorNums = new HashMap<Long, Integer>();

	public LienholderLessorSubPolicyList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}
	
	
	@Override
	protected LienholderLessorSubPolicyUIHelper getLienholderLessorUIHelper() {
		// TODO Auto-generated method stub
		return new LienholderLessorSubPolicyUIHelper();
	}


	@Override
	protected void addGridColumns() {
		lienLessorPayeeGrid.addColumn(o -> linLessorNums.get(o.getLienholderLessorSubPolicyPK())).setHeader(labels.getString("LienLessorListPayeeNumLabel"))
										.setFlexGrow(1);
		lienLessorPayeeGrid.addColumn(o -> o.getLienholderLessor().getCompanyName()).setHeader(labels.getString("LienLessorListNameLabel"))
										.setFlexGrow(4);
		lienLessorPayeeGrid.addColumn(new AddressRenderer<LienholderLessorSubPolicy>(o -> o.getLienholderLessor().getLienholderLessorAddress()))
										.setHeader(labels.getString("LienLessorListAddressLabel"))
										.setFlexGrow(4);
		lienLessorPayeeGrid.addColumn(new DSLookupItemRenderer<LienholderLessorSubPolicy>(LienholderLessorSubPolicy::getLienLessorType)
										.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_LienLessorType))
										.setHeader(labels.getString("LienLessorListPayeeType"))
										.setFlexGrow(2);
	}
	
	@Override
	protected List<LienholderLessorSubPolicy> getLienholderLessorsFromParent() {
		List<LienholderLessorSubPolicy> list = new ArrayList<LienholderLessorSubPolicy>();
		
		linLessorNums.clear();
		if (lienLessorParent != null && lienLessorParent.getLienholderLessors() != null) {
			lienLessorParent.getLienholderLessors()
								.stream().sorted(Comparator.comparing(LienholderLessorSubPolicy::getLienholderLessorSubPolicyPK))
								.collect(Collectors.toList())
								.stream().forEach(o -> list.add(o));
			
			int ii=1;
			for(LienholderLessorSubPolicy lienLessorSubPolicy : list) {
				linLessorNums.put(lienLessorSubPolicy.getLienholderLessorSubPolicyPK(), ii++);
			}
		}
		
		return list;
	}

	// Try to select the last created/updated row
	@Override
	public boolean loadData(SpecialityAutoSubPolicy<Risk> parent) {
		// TODO Auto-generated method stub
		boolean ok = super.loadData(parent);
		
		if (ok) {
			if (BrokerUI.getUserSession().getCurrentPolicy().getLastData() != null && 
				BrokerUI.getUserSession().getCurrentPolicy().getLastData() instanceof LienholderLessorSubPolicy) {
				LienholderLessorSubPolicy lienLessorHolder = (LienholderLessorSubPolicy) BrokerUI.getUserSession().getCurrentPolicy().getLastData();
				preSelectRow(lienLessorPayeeGrid, lienLessorHolder, (lienLessorHolder.getLienholderLessorSubPolicyPK() == null));
				BrokerUI.getUserSession().getCurrentPolicy().resetLastData();
			}
		}
		
		return ok;
	}

}
