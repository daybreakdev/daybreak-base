package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.VehicleDriverClaimUIHelper;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class VehicleDriverClaimList extends BaseListLayout<VehicleDriverClaim> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6829370224351841853L;
	private VehicleDriverClaimUIHelper 	vehicleDriverClaimUIHelper = new VehicleDriverClaimUIHelper();	
	private Grid<VehicleDriverClaim> 	driverClaimGrid;
	private SpecialtyVehicleRisk		vehicle;

	public VehicleDriverClaimList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}
	
	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("VehicleDriverClaimListTitle"),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	private void buildGrid() {
		driverClaimGrid = new Grid<VehicleDriverClaim>();
		driverClaimGrid.setSizeFull();
		uiHelper.setGridStyle(driverClaimGrid);

		createIndicatorColumn(driverClaimGrid);

		driverClaimGrid.addColumn(o -> vehicleDriverClaimUIHelper.getDriverLabel(o))
							.setHeader(labels.getString("VehicleDriverClaimListAssignedDriverLabel"))
							.setAutoWidth(true);
		driverClaimGrid.addColumn(VehicleDriverClaim::getLossDate)
							.setHeader(labels.getString("VehicleDriverClaimListDateOfLossLabel"));
		driverClaimGrid.addColumn(new DSLookupItemRenderer<VehicleDriverClaim>(VehicleDriverClaim::getClaimType)
							.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ClaimType))
							.setHeader(labels.getString("VehicleDriverClaimListClaimTypeLabel"));
		driverClaimGrid.addColumn(VehicleDriverClaim::getClaimNum)
							.setHeader(labels.getString("VehicleDriverClaimListClaimNumLabel"));
		driverClaimGrid.addColumn(new DSLookupItemRenderer<VehicleDriverClaim>(VehicleDriverClaim::getFaultResponsibility)
							.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_ClaimFault))
							.setHeader(labels.getString("VehicleDriverClaimListFaultPercentLabel"));
		driverClaimGrid.addColumn(new NumberRenderer<>(VehicleDriverClaim::getTotalPaid, DSNumericFormat.getAmountInstance()))
							.setHeader(labels.getString("VehicleDriverClaimListLossPaidLabel"))
							.setTextAlign(ColumnTextAlign.END);
		
		driverClaimGrid.setSelectionMode(SelectionMode.SINGLE);
		driverClaimGrid.addSelectionListener(event -> refreshToolbar());
		driverClaimGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		setUpGridDbClickEvent(driverClaimGrid);
						
		this.add(driverClaimGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				vehicleDriverClaimUIHelper.newVehicleDriverClaimAction(vehicle);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (driverClaimGrid.asSingleSelect() != null) {
					vehicleDriverClaimUIHelper.updateVehicleDriverClaimAction(vehicle, driverClaimGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (driverClaimGrid.asSingleSelect() != null) {
					vehicleDriverClaimUIHelper.deleteVehicleDriverClaimAction(vehicle, driverClaimGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (driverClaimGrid.asSingleSelect() != null) {
					vehicleDriverClaimUIHelper.undeleteVehicleDriverClaimAction(vehicle, driverClaimGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(SpecialtyVehicleRisk vehicle, Set<VehicleDriverClaim> set) {
		this.vehicle = vehicle;
		List<VehicleDriverClaim> list = vehicleDriverClaimUIHelper.sortDriverClaimList(set);

		this.driverClaimGrid.setItems(list);
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(uiPolicyHelper.getCurrentPolicyTransaction(), driverClaimGrid);
	}

}
