package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.constants.UIConstants;

public class CargoSubPolicyDialog extends DSUpdateDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5367099806409950779L;
	private CargoSubPolicyForm 	cargoSubPolicyForm;

	public CargoSubPolicyDialog (String title, String action) {
		super();
		this.cargoSubPolicyForm = new CargoSubPolicyForm(action, UIConstants.UICONTAINERTYPES.Dialog);
		initDialog(title, cargoSubPolicyForm, action);
	}

	public void loadData(CargoSubPolicy<?> subPolicy) {
		cargoSubPolicyForm.loadData(subPolicy);
		cargoSubPolicyForm.enableEdit();
	}

	public boolean validate() {
		return cargoSubPolicyForm.validate();
	}

	public boolean save(CargoSubPolicy<?> subPolicy) {
		return cargoSubPolicyForm.save(subPolicy);
	}

}
