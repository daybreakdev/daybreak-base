package com.echelon.ui.components.towing.dto;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;

public class LocCoverageDto extends CoverageDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1003394699713545165L;
	private PolicyLocation 	policyLocation;

	public LocCoverageDto(ICoverageConfig coverageConfig, Coverage coverage, IBusinessEntity coverageParent) {
		super(coverageConfig, coverage, coverageParent);
		// TODO Auto-generated constructor stub
	}

	public LocCoverageDto(ICoverageConfig coverageConfig, Endorsement endorsement, IBusinessEntity coverageParent) {
		super(coverageConfig, endorsement, coverageParent);
		// TODO Auto-generated constructor stub
	}

	public PolicyLocation getPolicyLocation() {
		return policyLocation;
	}

	public void setPolicyLocation(PolicyLocation policyLocation) {
		this.policyLocation = policyLocation;
	}

	public Integer getNumVehicles() {
		if (policyLocation != null) {
			return policyLocation.getNumVehicles();
		}
		
		return null;
	}

	public void setNumVehicles(Integer numVehicles) {
		if (policyLocation != null) {
			this.policyLocation.setNumVehicles(numVehicles);
		}
	}
}
