package com.echelon.ui.components.baseclasses.helpers;

import com.ds.ins.uicommon.components.DSBaseActionToolbar;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.dto.DSActionDto;
import com.ds.ins.uicommon.helpers.DSBaseLayoutUIHelper;
import com.echelon.ui.components.baseclasses.ActionToolbar;

public class LayoutUIHelper extends DSBaseLayoutUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6670304833810657084L;

	public LayoutUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public DSBaseActionToolbar createActionToolbar(UICOMPONENTTYPES uiComponentType, String... actions) {
		if (actions == null || actions.length < 1) {
			return new ActionToolbar(uiComponentType);	
		}
		
		return new ActionToolbar(uiComponentType, actions);
	}

	@Override
	public DSBaseActionToolbar createActionToolbar(UICOMPONENTTYPES uiComponentType,
			boolean isButtonTextAndIcon, DSActionDto... actionAndIcons) {
		if (actionAndIcons == null || actionAndIcons.length < 1) {
			return new ActionToolbar(uiComponentType);	
		}
		
		boolean anyIcon = false;
		String[] actions = new String[actionAndIcons.length];
		for(int ii=0; ii<actionAndIcons.length; ii++) {
			actions[ii] = actionAndIcons[ii].getName();
			if (actionAndIcons[ii].getIcon() != null) {
				anyIcon = true;
			}
		}
		
		if (anyIcon) {
			return new ActionToolbar(uiComponentType, isButtonTextAndIcon, actionAndIcons);
		}
		
		return new ActionToolbar(uiComponentType, actions);
	}
	
}
