package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.entities.AdditionalInsuredList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.echelon.ui.helpers.policy.towing.AdjustmentUIHelper;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="CGLSubPolicyNodeContentView", layout = BasePolicyView.class)
public class CGLSubPolicyContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132388023659436469L;
	private CGLSubPolicyUIHelper			subPolicyUIHelper 	= new CGLSubPolicyUIHelper();
	private CGLSubPolicyView				subPolicyView;
	private SubPolicyCoverageList	 		covList;
	private AdditionalInsuredList			addInsuredList;
	private AdditionalInsuredUIHelper		addInsuredUIHelper  = new AdditionalInsuredUIHelper();
	private LocationLinkList				locationList;
	private CGLSubPolicyAdjustmentList		adjustmentList;

	public CGLSubPolicyContentView() {
		super();
	}

	public CGLSubPolicyContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this, null, null);
		}
		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
		                  labels.getString("CGLSubPolicyDetailsTitle"),
		                  labels.getString("LocationLinkTabLabel"),
		                  labels.getString("AdditionalInsuredTabLabel"),
		                  labels.getString("CoverageTabLabel"),
		                  labels.getString("AdjustmentTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {
				getSubPolicyView(),
				getLocationList(),
				getAdditionalInsuredList(),
				getSubPolicyCoverageList(),
				getAdjustmentList()
		};
	}

	protected Component getSubPolicyView() {
		if (subPolicyView == null) {
			subPolicyView = new CGLSubPolicyView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return subPolicyView;
	}

	protected Component getAdditionalInsuredList() {
		if (addInsuredList == null) {
			addInsuredList = new AdditionalInsuredList(uiMode, UICONTAINERTYPES.Tab);
		}

		return addInsuredList;
	}

	protected Component getSubPolicyCoverageList() {
		if (covList == null) {
			covList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.CGLSubPolicy, uiMode, UICONTAINERTYPES.Tab);
		}
		
		return covList;
	}

	protected Component getLocationList() {
		if (locationList == null) {
			locationList = new LocationLinkList(uiMode, UICONTAINERTYPES.Tab);
		}

		return locationList;
	}

	protected Component getAdjustmentList() {
		if (adjustmentList == null) {
			adjustmentList = new CGLSubPolicyAdjustmentList(uiMode, UICONTAINERTYPES.Tab);
		}

		return adjustmentList;
	}
	
	public void loadData(CGLSubPolicy<?> cglSubPolicy) {
		if (subPolicyView != null) {
			subPolicyUIHelper.loadView(subPolicyView, cglSubPolicy);
		}
		if (addInsuredList != null) {
			addInsuredUIHelper.loadView(addInsuredList, cglSubPolicy);
		}
		if (covList != null) {
			((CGLSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.CGLSubPolicy)).loadView(covList, cglSubPolicy);
		}
		if (locationList != null) {
			subPolicyUIHelper.loadView(locationList, cglSubPolicy);
		}
		if (adjustmentList != null) {
			new AdjustmentUIHelper().loadView(adjustmentList, cglSubPolicy);
		}	
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		if (nodeItem == null) {
			return;
		}
		
		final CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) nodeItem.getNodeObject();

		subPolicyUIHelper.loadSubpolicyData(new CGLSubPolicyUIHelper.LoadSubPolicyDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData(cglSubPolicy);
				refreshPage();
				
				tab.setEnabled(nodeItem.getNodeObject() != null);
			}
		}, cglSubPolicy);
	}

}
