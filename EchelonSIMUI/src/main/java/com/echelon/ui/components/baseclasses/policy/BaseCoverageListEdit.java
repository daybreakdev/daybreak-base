package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.uicommon.components.DSToolbar;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.grid.HeaderRow;
import com.vaadin.flow.component.grid.HeaderRow.HeaderCell;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.provider.ListDataProvider;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class BaseCoverageListEdit extends BaseListLayout<CoverageDto> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2366103326011989574L;
	protected IDSDialog						containerDialog;
	protected IBusinessEntity				covParent;
	protected ICoverageConfigParent			covConfigParent;
	protected Grid<CoverageDto>				coverageGrid;
	protected UIConstants.OBJECTTYPES 		parentObjectType;
	protected BaseCoverageUIHelper			coverageUIHelper;
	protected BaseCoverageConfigUIHelper 	coverageConfigUIHelper;
	protected Integer						tabIndex;
	protected boolean						isDataInstanceModified;
	protected FormLayout					totalsForm;
		
	protected final static String COLUMNKEY_DESCRIPTION 	 = "DES";
	protected final static String COLUMNKEY_TOOLBAR  		 = "toolbar";
	protected final static String COLUMNKEY_LIMIT 			 = "LIMIT";
	protected final static String COLUMNKEY_DEDUCTIBLE 		 = "DED";
	protected final static String COLUMNKEY_UWADJUSTMENT 	 = "UWADJUST";
	protected final static String COLUMNKEY_ISOVRPREMIUM	 = "ISOVRPREM";
	protected final static String COLUMNKEY_ISREINSURED	 	 = "ISREINSURED";
	protected final static String COLUMNKEY_UNITPREMIUM 	 = "UPREM";
	protected final static String COLUMNKEY_RATEDPREMIUM 	 = "RPREM";
	protected final static String COLUMNKEY_ANNUALPREMIUM 	 = "APREM";
	protected final static String COLUMNKEY_NETCHANGEPREMIUM = "NCPREM";
	protected final static String COLUMNKEY_WRITTENPREMIUM 	 = "WPREM";
	protected final static String COLUMN_LIMITWIDTH			 = "135px";
	protected final static String COLUMN_PREMWIDTH			 = "105px";
	protected final static String COLUMN_INDWITH2			 = "50px";
	
	public BaseCoverageListEdit(String uiMode, UICONTAINERTYPES uiContainerType, UIConstants.OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		this.parentObjectType 		= parentObjectType;
		this.coverageUIHelper 		= CoverageUIFactory.getInstance().createCoverageHelper(parentObjectType);
		this.coverageConfigUIHelper	= CoverageUIFactory.getInstance().createCoverageConfigHelper(parentObjectType);
		
		initializeLayouts(labels.getString("CoverageListTitle"));
		
		buildGrid();
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
	}
	
	protected void buildGrid() {
		coverageGrid = new Grid<CoverageDto>();
		uiHelper.setGridStyle(coverageGrid);
		coverageGrid.setTabIndex(-1);
		coverageGrid.setSizeFull();
		
		createIndicatorColumn(coverageGrid);
		
		buildGridDataColumns();
		coverageGrid.addComponentColumn(o -> createColumnToolbar(o))
											.setWidth("50px")
											.setKey(COLUMNKEY_TOOLBAR)
											.setResizable(true)
											.setFlexGrow(0);

		coverageGrid.setSelectionMode(SelectionMode.SINGLE);
		coverageGrid.setDetailsVisibleOnClick(false);
		coverageGrid.addSelectionListener(event -> { 
				if (event.getFirstSelectedItem().isPresent()) {
					CoverageDto dto = event.getFirstSelectedItem().get();
					setRowSelected(dto);
				}
				else {
					setRowSelected(null);
				}
		});
		
		coverageGrid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
			return setupRowDetails(o);
		}));
		
		/*
		coverageGrid.addItemClickListener(event -> {
			editGridItem(event.getItem());
			showCoverageDetails(event.getItem());
		});
		*/
		
		HeaderRow hRow = coverageGrid.prependHeaderRow();
		buildGridHeaderPremiumColumns(hRow);
				
		this.add(coverageGrid);
	}
	
	protected void buildGridDataColumns() {
		coverageGrid.addColumn(CoverageDto::getDescription)
								.setHeader(labels.getString("CoverageListCoverageTypeLabel"))
								.setWidth("22%")
								.setResizable(true)
								.setKey(COLUMNKEY_DESCRIPTION)
								;
		coverageGrid.addComponentColumn(o -> o.getEditLimitField())
								.setHeader(labels.getString("CoverageListCoverageLimitLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_LIMIT)
								.setAutoWidth(true)
								;
		coverageGrid.addComponentColumn(o -> o.getEditDeductibleField())
								.setHeader(labels.getString("CoverageListCoverageDedLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_DEDUCTIBLE)
								.setAutoWidth(true)
								;
		coverageGrid.addComponentColumn(o -> o.getEditUwAdjustmentField())
								.setHeader(labels.getString("CoverageListUWAdjustmentShort2Label"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_UWADJUSTMENT)
								.setAutoWidth(true)
								;
		coverageGrid.addComponentColumn(o -> o.getEditIsPremiumOverrideField())
								.setHeader(labels.getString("CoverageListIsPremiumOverrideShort2Label"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_ISOVRPREMIUM)
								.setFlexGrow(0)
								;
		coverageGrid.addComponentColumn(o -> o.getEditIsReinsuredField())
								.setHeader(labels.getString("CoverageListIsReinsured"))
								.setTextAlign(ColumnTextAlign.END)
								.setKey(COLUMNKEY_ISREINSURED)
								.setFlexGrow(0)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispUnitPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_UNITPREMIUM)
								.setWidth(COLUMN_PREMWIDTH)
								.setFlexGrow(0);
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispOriginalPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_RATEDPREMIUM)
								.setWidth(COLUMN_PREMWIDTH)
								.setFlexGrow(0);
		coverageGrid.addComponentColumn(o -> o.getEditPremiumField())
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_ANNUALPREMIUM)
								.setAutoWidth(true)
								;
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispNetPremiumChange, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListAmountShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_NETCHANGEPREMIUM)
								.setWidth(COLUMN_PREMWIDTH)
								.setFlexGrow(0);
		coverageGrid.addColumn(new NumberRenderer<>(CoverageDto::getDispWrittenPremium, DSNumericFormat.getPremiumInstance()))
								.setHeader(labels.getString("CoverageListPremiumShortLabel"))
								.setTextAlign(ColumnTextAlign.END)
								.setResizable(true)
								.setKey(COLUMNKEY_WRITTENPREMIUM)
								.setWidth(COLUMN_PREMWIDTH)
								.setFlexGrow(0);
		
		coverageGrid.getColumnByKey(COLUMNKEY_UNITPREMIUM).setVisible(false);
	}
	
	protected void buildGridHeaderPremiumColumns(HeaderRow hRow) {
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_UWADJUSTMENT, 		labels.getString("CoverageListUWAdjustmentShort1Label"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_ISOVRPREMIUM,   	labels.getString("CoverageListIsPremiumOverrideShort1Label"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_UNITPREMIUM,   	labels.getString("CoverageListUnitPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_RATEDPREMIUM,   	labels.getString("CoverageListRatedPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_ANNUALPREMIUM,  	labels.getString("CoverageListAnnualPremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_NETCHANGEPREMIUM, 	labels.getString("CoverageListNetChangePremiumShortLabel"));
		buildGridHeaderPremiumColumn(hRow, COLUMNKEY_WRITTENPREMIUM, 	labels.getString("CoverageListWrittenPremiumShortLabel"));
	}
	
	protected void buildGridHeaderPremiumColumn(HeaderRow hRow, String colKey, String colLabel) {
		HeaderCell hCell = hRow.getCell(coverageGrid.getColumnByKey(colKey));
		VerticalLayout hLayoput = new VerticalLayout();
		uiHelper.formatFullLayout(hLayoput);
		hLayoput.setDefaultHorizontalComponentAlignment(Alignment.END);
		Label hLabel = new Label(colLabel);
		hLayoput.add(hLabel);
		hCell.setComponent(hLayoput);
	}
		
	protected Component createColumnToolbar(CoverageDto dto) {
		if (dto.getDeleteButton() != null || dto.getUndeleteButton() != null) {
			FlexLayout colLayout = new FlexLayout();
			//colLayout.setFlexDirection(FlexDirection.ROW);
			colLayout.setAlignItems(Alignment.START);
			if (dto.getDeleteButton() != null) {
				colLayout.add(dto.getDeleteButton());
			}
			if (dto.getUndeleteButton() != null) {
				colLayout.add(dto.getUndeleteButton());
			}
			return colLayout;
		}
		else {
			return new Div();
		}
	}
	
	@Override
	protected Component createIndicator(Object object) {
		CoverageDto dto = (CoverageDto) object;
		
		Div div = new Div();
		
		Icon covIcon = null; 
		if (isIndicatorColumnRequired()) {
			if (dto.isNewCoverage()) { 
				covIcon = uiHelper.createIconNew();
			}
			else if (dto.isModifiedCoverage()) {
				covIcon = uiHelper.createIconModified();
			}
		}
		
		if (dto.isDeletedCoverage() || dto.wasDeletedCoverage()) {
			covIcon = uiHelper.createIconDeleted();
		}
		
		Icon validIcon = null;
		if (dto.isEditValid() != null && !dto.isEditValid()) { 
			validIcon = uiHelper.createIconInvalid();
		}

		if (validIcon != null) {
			div.add(validIcon);
		}
		if (covIcon != null) {
			div.add(covIcon);
		}

		return div;
	}
	
	protected Component setupRowDetails(CoverageDto dto) {
		HorizontalLayout layout =  new HorizontalLayout();
		layout.setSizeFull();
		layout.setPadding(true);
		uiHelper.setBorder(layout);
		layout.getElement().getStyle().set("background-color","white");
		
		if (dto.isDetailRequired() && dto.hasAnyAdditionalFields()) {
			layout.add((Component)dto.getCoverageAdditionalForm());
		}
		
		return layout;
	}

	protected void selectGridItem(CoverageDto dto) {
		coverageGrid.select(dto);
		showCoverageDetails(dto);
	}
	
	protected void editGridItem(CoverageDto dto) {
		if (dto == null) {
			return;
		}
		
		dto.setEditValid(null);
		setEditFocus(dto);
	}
	
	public List<CoverageDto> getCurrentCoverageList() {
		List<CoverageDto> list = (List<CoverageDto>) ((ListDataProvider<CoverageDto>)this.coverageGrid.getDataProvider()).getItems();
		if (list == null) {
			list = new ArrayList<CoverageDto>();
		}
		return list;
	}
	
	public ICoverageConfigParent getCoverageConfigParent() {
		return covConfigParent;
	}
	
	public CoverageDto getSelectedCoverageDto() {
		if (coverageGrid.asSingleSelect() != null && !coverageGrid.asSingleSelect().isEmpty()) {
			return coverageGrid.asSingleSelect().getValue();
		}
		
		return null;
	}

	public void deselectRow() {
		coverageGrid.deselectAll();
		setRowSelected(null);
	}

	protected void setRowSelected(CoverageDto dto) {
		setButtonReinsuranceEnable(dto);
		editGridItem(dto);
		showCoverageDetails(dto);
	}

	protected void setFirstEditFocus() {
		List<CoverageDto> ls = getCurrentCoverageList();
		if (ls != null && !ls.isEmpty()) {
			CoverageDto firstEdit = ls.stream().filter(o -> 
							(o.getEditLimitField()      	   != null && !o.getEditLimitField().isReadOnly()) ||
							(o.getEditDeductibleField() 	   != null && !o.getEditDeductibleField().isReadOnly()) ||
							(o.getEditPremiumField() 		   != null && !o.getEditPremiumField().isReadOnly()) ||
							(o.getEditIsPremiumOverrideField() != null && !o.getEditIsPremiumOverrideField().isReadOnly()) ||
							(o.getEditIsReinsuredField() 	   != null && !o.getEditIsReinsuredField().isReadOnly()) ||
							(o.getEditUwAdjustmentField() 	   != null && !o.getEditUwAdjustmentField().isReadOnly())
							)
							.findFirst().orElse(null);
			
			if (firstEdit != null) {
				coverageGrid.scrollToStart();
			}
			
			setEditFocus(firstEdit);
		}
	}
	
	protected void setEditFocus(CoverageDto dto) {
		if (dto != null) {
			if (dto.getEditLimitField() != null && !dto.getEditLimitField().isReadOnly()) {
				dto.getEditLimitField().setFocus();
				return;
			}
			
			if (dto.getEditDeductibleField() != null && !dto.getEditDeductibleField().isReadOnly()) {
				dto.getEditDeductibleField().setFocus();
				return;
			}
			
			if (dto.getEditUwAdjustmentField() != null && !dto.getEditUwAdjustmentField().isReadOnly()) {
				dto.getEditUwAdjustmentField().focus();
				return;
			}

			if (dto.getEditIsReinsuredField() != null && !dto.getEditIsReinsuredField().isReadOnly()) {
				dto.getEditIsReinsuredField().focus();
				return;
			}

			if (dto.getEditIsPremiumOverrideField() != null && !dto.getEditIsPremiumOverrideField().isReadOnly()) {
				dto.getEditIsPremiumOverrideField().focus();
				return;
			}
			
			if (dto.getEditPremiumField() != null && !dto.getEditPremiumField().isReadOnly()) {
				dto.getEditPremiumField().focus();
				return;
			}
		}
	}
	
	public void setContainerDialog(IDSDialog containerDialog) {
		this.containerDialog = containerDialog;
		
		totalsForm = new FormLayout();
		containerDialog.getDialogTopInfoSection().add(totalsForm);
		containerDialog.getDialogTopInfoSection().setHorizontalComponentAlignment(Alignment.END, totalsForm);
	}

	public boolean isDataInstanceModified() {
		boolean result = isDataInstanceModified;
		
		if (!isDataInstanceModified) {
			List<CoverageDto> list = getCurrentCoverageList();
			if (list != null && !list.isEmpty()) {
				result = list.stream().filter(o -> o.getCoverageAdditionalForm() != null 
												&& o.getCoverageAdditionalForm().isDataInstanceModified())
							 .findFirst().isPresent();
				
				if (!result) {
					result = list.stream().filter(o -> o.getReinsuranceDtos() != null && !o.getReinsuranceDtos().isEmpty()).collect(Collectors.toList())
									.stream().flatMap(o -> o.getReinsuranceDtos().stream()).collect(Collectors.toList())
									.stream().filter(o -> o.isNew() || o.isUpdated() || o.isDeleted()).findFirst().isPresent();
				}
			}
		}
		
		return result;
	}

	public void setDataInstanceModified(boolean isDataInstanceModified) {
		this.isDataInstanceModified = isDataInstanceModified;
	}

	protected void resetCoverageIndicators(List<CoverageDto> coverages) {
		boolean indFound = false;
		boolean errFound = false;
		
		if (coverages != null && !coverages.isEmpty()) {
			errFound = coverages.stream().filter(o -> 
											o.isEditValid()   != null && !o.isEditValid() && 
											!o.isEditDeleted()).findFirst().isPresent();
			if (!indFound && isIndicatorColumnRequired()) {
				if (coverages.stream().filter(o -> o.isNewCoverage() || o.isDeletedCoverage() || o.isModifiedCoverage() || o.wasDeletedCoverage())
												.findFirst().isPresent()) {
					indFound = true;
				}
			}

		}
		if (indFound && errFound) {
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setWidth(COLUMN_INDWITH2);
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setVisible(true);
		}
		else if (indFound || errFound) {
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setWidth(COLUMN_INDWITH);
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setVisible(true);	
		}
		else {
			coverageGrid.getColumnByKey(COLUMNKEY_INDICATOR).setVisible(false);
		}
	}
	
	public boolean reLoadData(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverags) {
		loadData(covConfigParent, covParent, coverags);
		this.isDataInstanceModified = true;
		
		return true;
	}
	
	public boolean loadData(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverages) {
		// TODO Auto-generated method stub
		this.isDataInstanceModified = false;
		this.covParent 		    	= covParent;
		this.covConfigParent    	= covConfigParent;
		if (coverages == null) {
			coverages = new ArrayList<CoverageDto>();
		}
		
		initializeReinsurance();
		initializeCoverageDtos(coverages);
		
		coverages = coverages.stream().sorted(Comparator.comparing(CoverageDto::getCoverageType)
											.thenComparing(Comparator.comparing(CoverageDto::getCoverageOrder))
										).collect(Collectors.toList());
		
		resetEditFieldsTabindex(coverages);
		resetCoverageIndicators(coverages);
		
		this.coverageGrid.setItems(coverages);

		ListDataProvider<CoverageDto> dataProvider = (ListDataProvider<CoverageDto>) coverageGrid.getDataProvider();
		dataProvider.clearFilters();
		dataProvider.addFilter(o -> !(o.isNewCoverage() && o.isEditDeleted()));
		coverageGrid.recalculateColumnWidths();
		
		loadTotals(covParent);

		return true;
	}
	
	protected void loadTotals(IBusinessEntity covParent) {
		List<TotalPremiumDto> totalPremiumDtos = coverageUIHelper.createTotalPremiumDtos(covParent);
		totalsForm.removeAll();
		
		if (totalPremiumDtos != null && !totalPremiumDtos.isEmpty()) {
			int cols = totalPremiumDtos.stream().filter(o -> o.getTotalLabel() != null).collect(Collectors.toList())
									   .size();
			
			totalsForm.setResponsiveSteps(
					new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, cols)
					);
			
			totalPremiumDtos.sort(Comparator.comparing(TotalPremiumDto::getSeq));
			totalPremiumDtos.stream().forEach(o -> {
					if (o.getTotalLabel() != null) {
						Label label = new Label(o.getTotalLabel());
						label.getStyle().set("text-align", "end");
						totalsForm.add(label);
					}
				});
			totalPremiumDtos.stream().forEach(o -> {
						if (o.getTotalLabel() != null) {
							PremiumField field = new PremiumField(null);
							field.setDSValue(o.getTotalPremium());
							uiHelper.setEditFieldsReadonly(field);
							totalsForm.add(field);
						}
					});
		}
	}

	protected void createEditFields(CoverageDto dto) {
		CovLimitField 	 		editLimitField 				= new CovLimitField(coverageConfigUIHelper, coverageUIHelper);
		CovDeductibleField 		editDeductibleField			= new CovDeductibleField(coverageConfigUIHelper, coverageUIHelper);
		CovPremiumField 	 	editManualPremiumField 		= new CovPremiumField(null);
		CovIsPremiumOvrField	editIsPremiumOverrideField	= new CovIsPremiumOvrField(null);
		CovIsReinsuredField		editIsReinsuredField		= new CovIsReinsuredField(null);
		CovUWAdjustmentField  	editUwAdjustmentField	    = new CovUWAdjustmentField(null);
		
		editLimitField.setWidth(COLUMN_LIMITWIDTH);
		editDeductibleField.setWidth(COLUMN_LIMITWIDTH);
		editManualPremiumField.setWidth(COLUMN_PREMWIDTH);
		editUwAdjustmentField.setWidth(COLUMN_LIMITWIDTH);
		
		// For SP64; all will be hidden
		editLimitField.setVisible(true);
		editDeductibleField.setVisible(true);
		editManualPremiumField.setVisible(true);
		editIsPremiumOverrideField.setVisible(true);
		editIsReinsuredField.setVisible(true);
		editUwAdjustmentField.setVisible(true);
		
		editLimitField.initializeField(dto);
		editDeductibleField.initializeField(dto);
		editIsReinsuredField.initializeField(dto);
		editIsPremiumOverrideField.initializeField(dto, editIsReinsuredField);
		editManualPremiumField.initializeField(dto, editIsPremiumOverrideField, editIsReinsuredField);
		editUwAdjustmentField.initializeField(this.parentObjectType, dto, editIsPremiumOverrideField);
		
		Binder<CoverageDto> editBinder = new Binder<CoverageDto>(CoverageDto.class);
		
		editLimitField.bindField(editBinder);
		editDeductibleField.bindField(editBinder);
		editIsPremiumOverrideField.bindField(editBinder);
		editIsReinsuredField.bindField(editBinder);
		editManualPremiumField.bindField(editBinder);
		editUwAdjustmentField.bindField(editBinder);

		dto.setEditBinder(editBinder);
		dto.setEditLimitField(editLimitField);
		dto.setEditDeductibleField(editDeductibleField);
		dto.setEditPremiumField(editManualPremiumField);
		dto.setEditIsPremiumOverrideField(editIsPremiumOverrideField);
		dto.setEditIsReinsuredField(editIsReinsuredField);
		dto.setEditUwAdjustmentField(editUwAdjustmentField);
		
		editBinder.setReadOnly(true);
	}
	
	protected void resetEditFieldsTabindex(List<CoverageDto> coverags) {
		// Reset to no tab
		coverags.stream().forEach(o -> resetEditFieldTabindexes(o));
	}
	
	protected void resetEditFieldTabindexes(CoverageDto dto) {
		dto.getEditLimitField().setTabindexIfEditiable(-1);
		dto.getEditDeductibleField().setTabindexIfEditiable(-1);
		dto.getEditPremiumField().setTabindexIfEditiable(-1);
		dto.getEditIsPremiumOverrideField().setTabindexIfEditiable(-1);
		dto.getEditIsReinsuredField().setTabindexIfEditiable(-1);
		dto.getEditUwAdjustmentField().setTabIndex(-1);
	}
	
	protected void assignEditFieldsTabindex(List<CoverageDto> coverags) {
		// Set the tab indexes
		tabIndex = 10;
		coverags.stream().forEach(o -> assignEditFieldTabindex(o));
	}
	
	protected void assignEditFieldTabindex(CoverageDto dto) {
		CovLimitField 	 	 editLimitField 			= dto.getEditLimitField();
		CovDeductibleField 	 editDeductibleField		= dto.getEditDeductibleField();
		CovPremiumField 	 editManualPremiumField 	= dto.getEditPremiumField();
		CovIsPremiumOvrField editIsPremiumOverrideField = dto.getEditIsPremiumOverrideField();
		CovIsReinsuredField	 ediCovIsReinsuredField 	= dto.getEditIsReinsuredField();
		CovUWAdjustmentField editUwAdjustmentField 	    = dto.getEditUwAdjustmentField(); 
		
		if (editLimitField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}
		if (editDeductibleField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}
		if (editUwAdjustmentField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}
		if (editIsPremiumOverrideField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}		
		if (ediCovIsReinsuredField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}		
		if (editManualPremiumField.setTabindexIfEditiable(tabIndex)) {
			tabIndex++;
		}
	}
	
	protected void createDeleteButton(CoverageDto dto) {
		if (coverageUIHelper.canCoverageRemove(covConfigParent, dto)) {
			Button btn = new Button(VaadinIcon.CLOSE_SMALL.create());
			btn.setTabIndex(-1);
			btn.addClickListener(e -> {
				isDataInstanceModified = true;
				dto.getDeleteButton().setVisible(false);
				coverageUIHelper.deleteCoverage(covConfigParent, covParent, dto);
				dto.setIsEditDeleted();
				resetCoverageIndicators(getCurrentCoverageList());
				// make sure the coverage is not new coverage 
				if (!dto.isNewCoverage() && 
					dto.getUndeleteButton() != null) {
					dto.getUndeleteButton().setVisible(true);
					coverageGrid.getDataProvider().refreshItem(dto);
					enableEdit(dto);
					
					dto.setCoverageAdditionalForm(null);
					showCoverageDetails(dto);
				}
				else {
					coverageGrid.getDataProvider().refreshAll();
				}
			});
			
			dto.setDeleteButton(btn);
			btn.setVisible(!dto.isDeletedCoverage() && !dto.wasDeletedCoverage());
		}
	}
	
	protected void createUndeleteButton(CoverageDto dto) {
		if (coverageUIHelper.canCoverageRemove(covConfigParent, dto)) {
			Button btn = new Button(VaadinIcon.ROTATE_LEFT.create());
			btn.setTabIndex(-1);
			btn.addClickListener(e -> {
				isDataInstanceModified = true;
				dto.getDeleteButton().setVisible(true);
				dto.getUndeleteButton().setVisible(false);
				coverageUIHelper.undeleteCoverage(covConfigParent, covParent, dto);
				dto.setIsEditUndeleted();
				resetCoverageIndicators(getCurrentCoverageList());
				coverageGrid.getDataProvider().refreshItem(dto);
				enableEdit(dto);
				
				dto.setCoverageAdditionalForm(null);
				showCoverageDetails(dto);
			});
			
			dto.setUndeleteButton(btn);
			btn.setVisible(dto.isDeletedCoverage() && !dto.wasDeletedCoverage());
		}
	}
	
	protected void initializeCoverageDtos(List<CoverageDto> coverags) {
		coverags.stream().forEach(o -> {
			o.setCoverageAdditionalForm(null);
			createDeleteButton(o);
			createEditFields(o);
			createUndeleteButton(o);
			o.getEditBinder().readBean(o);
			
			initializeCoverageDetailForm(o);
			initializeCoverageReinsuranceButton(o);
		});
	}
	
	protected void initializeCoverageDetailForm(CoverageDto coverageDto) {
		if (coverageDto != null && coverageDto.isDetailRequired()) {
			String detlUI = uiMode;
			if (coverageDto.isDeletedCoverage() || coverageDto.wasDeletedCoverage()) {
				detlUI = UIConstants.UIACTION_View;
			}
			
			coverageDto.setCoverageAdditionalForm(
					CoverageUIFactory.getInstance().createCoverageAddlFieldsForm(parentObjectType, coverageDto.getCoverageCode(), detlUI, UICONTAINERTYPES.Grid)
					);
			// LoadData build the fields
			coverageDto.getCoverageAdditionalForm().loadData(coverageDto);
			if (coverageDto.hasAnyAdditionalFields()) {
				if (UIConstants.UIACTION_Update.equals(detlUI)) {
					coverageDto.getCoverageAdditionalForm().enableEdit(coverageDto);
					coverageDto.getCoverageAdditionalForm().setFocus();
				}
			}
			
			coverageDto.getCoverageAdditionalForm().setFormVisible(false);
		}
	}
	
	//editToolbar in dialog
	public void enableEdit(DSToolbar editToolbar) {
		getCurrentCoverageList().stream().forEach(o -> enableEdit(o));
		
		refreshEditToolbar(editToolbar);
		
		assignEditFieldsTabindex(getCurrentCoverageList());
		
		setFirstEditFocus();
	}
	
	protected void enableEdit(CoverageDto dto) {
		enableDataEdit(dto.getEditBinder());
		
		dto.getEditLimitField().determineReadOnly();
		dto.getEditDeductibleField().determineReadOnly();
		dto.getEditIsPremiumOverrideField().determineReadOnly();
		dto.getEditIsReinsuredField().determineReadOnly();
		dto.getEditPremiumField().determineReadOnly();
		dto.getEditUwAdjustmentField().determineReadOnly();
	}
	
	public boolean validateEdit() {
		getCurrentCoverageList().stream().filter(o -> !o.isEditDeleted()).collect(Collectors.toList())
								.stream().forEach(o -> {
										if (!uiHelper.validateValues(false, o.getEditBinder())) {
											coverageUIHelper.setGeneralError(o);
											o.setEditValid(false);
										}
										/*
										if (o.getCoverageAdditionalForm() != null &&
											!o.getCoverageAdditionalForm().validateEdit(false)) {
											o.setEditValid(false);
										}
										*/
									});
		
		// false for any exception
		return true;
	}
	
	public boolean updateValidateEdit() {
		showCoverageDetails(null);
		resetCoverageIndicators(getCurrentCoverageList());

		coverageGrid.getDataProvider().refreshAll();
		
		return true;
	}
	
	public boolean saveEdit() {
		getCurrentCoverageList().stream().filter(o -> !o.isEditDeleted()).collect(Collectors.toList())
								.stream().forEach(o -> {
										try {
											o.getEditBinder().writeBean(o);	
										} catch (Exception e) {
										}});
		
		// false for any exception
		return true;
	}

	protected void showCoverageDetails(CoverageDto dto) {
		getCurrentCoverageList().stream().filter(o -> coverageGrid.isDetailsVisible(o))
									.collect(Collectors.toList())
								.stream().forEach(o -> {
				if (o.getCoverageAdditionalForm() != null) {
					o.getCoverageAdditionalForm().setFormVisible(false);
				}
				coverageGrid.setDetailsVisible(o, false);
		});
		
		if (dto != null && dto.isDetailRequired()) {
			createCoverageDetails(dto);
			if (dto.hasAnyAdditionalFields()) {
				coverageGrid.setDetailsVisible(dto, true);
				return;
			}
		}
	}

	protected void createCoverageDetails(CoverageDto dto) {
		if (dto != null && dto.isDetailRequired()) {
			// Main detail form is created in initializeCoverageDtos
			
			if (dto.hasAnyAdditionalFields()) {
				dto.getCoverageAdditionalForm().setFormVisible(true);
			}
		}
	}
	
	protected void refreshEditToolbar(DSToolbar editToolbar) {
		if (editToolbar.getButton(UIConstants.UIACTION_ApplyAdjUW) != null) {
			editToolbar.getButton(UIConstants.UIACTION_ApplyAdjUW).setEnabled(isUwAdjustmentRequired());
		}
	}
	
	protected boolean isUwAdjustmentRequired() {
		List<CoverageDto> coverageDtos = getCurrentCoverageList();
		if (coverageDtos != null && !coverageDtos.isEmpty()) {
			return coverageDtos.stream().filter(o -> 
							!o.getEditUwAdjustmentField().isReadOnly()
						).findFirst().isPresent();
		}
		
		return false;
	}

	public void applyAdjUW(Double value) {
		List<CoverageDto> coverageDtos = getCurrentCoverageList();
		if (coverageDtos != null && !coverageDtos.isEmpty()) {
			coverageDtos.stream().forEach(o -> {
				if (!o.getEditUwAdjustmentField().isReadOnly()) {
					o.getEditUwAdjustmentField().setDSValue(value);
				}
			});
		}
	}
	
	protected void setButtonReinsuranceEnable(CoverageDto dto) {
		if (containerDialog != null) {
			Button bt = ((DSUpdateDialog)containerDialog).getButton(UIConstants.UIACTION_Reinsurance);
			if (bt != null) {
				if (dto != null && BooleanUtils.isTrue(dto.getEditIsReinsuredField().getValue())) {
					bt.setEnabled(true);
				}
				else {
					bt.setEnabled(false);
				}
			}
		}
	}
	
	protected void initializeCoverageReinsuranceButton(CoverageDto dto) {
		if (dto.getEditIsReinsuredField() != null) {
			dto.getEditIsReinsuredField().addValueChangeListener(event -> {
				CoverageDto selDto = coverageGrid.asSingleSelect().getValue();
				if (selDto != null && dto != null && dto.equals(selDto)) {
					setButtonReinsuranceEnable(dto);
				}
			});
		}
	}
	
	protected void initializeReinsurance() {
		Button btReinsurance = null;
		if (containerDialog != null) {
			btReinsurance = ((DSUpdateDialog)containerDialog).getButton(UIConstants.UIACTION_Reinsurance);
		}		
		
		if (uiConfigHelper.isCurrentAllowProductReinsurance()) {
			btReinsurance.setVisible(true);
			this.coverageGrid.getColumnByKey(COLUMNKEY_ISREINSURED).setVisible(true);
		}
		else {
			btReinsurance.setVisible(false);
			this.coverageGrid.getColumnByKey(COLUMNKEY_ISREINSURED).setVisible(false);
		}
		
	}
}
