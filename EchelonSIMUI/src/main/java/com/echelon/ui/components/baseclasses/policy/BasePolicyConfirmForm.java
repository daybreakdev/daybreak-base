package com.echelon.ui.components.baseclasses.policy;

import java.time.LocalDate;
import java.util.List;

import com.ds.ins.domain.dto.PolicyTermDTO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.constants.DSUIConstants;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.ds.ins.utils.Constants;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;
import com.echelon.ui.components.baseclasses.validators.BasePolicyChangeEffectiveDateValidator;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyConfirmForm extends BaseLayout implements IBasePolicyConfirmForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6980245373898304748L;
	
	protected DSComboBox<String>			confirmReasonCodeField;
	protected TextField						confirmReasonDetailsField;
	protected DatePicker					confirmEffectiveDate;
	protected TextField						confirmDescription;
	protected TextField						quotePolicyNumField;
	protected TextField						insuredField;
	protected DatePicker					effectiveDateField;
	protected DatePicker					expiryDateField;
	protected DSComboBox<Integer>			confirmDaysField;
	protected DSComboBox<Integer>			renewalTermLengthField;
	
	protected String						policyAction;
	protected String 			 			policyVerTxnType;
	protected String						policyVerStatus;
	
	protected Binder<PolicyConfirmDto> 		binder;
	protected Binder<PolicyTransaction> 	ptBinder;
	
	protected PersonalCustomerUIHelper		personalCustomerUIHelper;
	
	protected BasePolicyChangeEffectiveDateValidator policyChangeEffectiveDateValidator;

	public BasePolicyConfirmForm(String uiMode, UICONTAINERTYPES uiContainerType,
									String	 policyAction,
									String 	 policyVerTxnType,
									String 	 policyVerStatus) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.personalCustomerUIHelper = new PersonalCustomerUIHelper();
		
		this.policyAction 	  = policyAction;
		this.policyVerTxnType = policyVerTxnType;
		this.policyVerStatus  = policyVerStatus;
		buildFields();
		buildBinder();
		buildForm();
	}
	
	protected boolean isNewBusinessActions() {
		return Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(policyVerTxnType) &&
			   (policyVerStatus == null || !policyVerStatus.startsWith("ISSUED"));
	}
	
	protected BasePolicyChangeEffectiveDateValidator createPolicyChangeEffectiveDateValidator() {
		return new BasePolicyChangeEffectiveDateValidator();
	}
	
	protected void buildFields() {
		if (ConfigConstants.LIFECYCLEACTION_DeclineQuote.equals(policyAction) 			||
			ConfigConstants.LIFECYCLEACTION_DeclinePolicyChange.equals(policyAction) 	||
			ConfigConstants.LIFECYCLEACTION_SpoilPolicyChange.equals(policyAction) 		||
			ConfigConstants.LIFECYCLEACTION_DeclineRenewal.equals(policyAction) 		||
			ConfigConstants.LIFECYCLEACTION_SpoilRenewal.equals(policyAction) 			||
			ConfigConstants.LIFECYCLEACTION_DeclineCancel.equals(policyAction) 			|| 
			ConfigConstants.LIFECYCLEACTION_DeclineReinstate.equals(policyAction) 		||
			ConfigConstants.LIFECYCLEACTION_DeclineReissue.equals(policyAction) 		||
			ConfigConstants.LIFECYCLEACTION_DeclineExtension.equals(policyAction) 		||
			ConfigConstants.LIFECYCLEACTION_DeclineAdjustment.equals(policyAction)) {
			createConfirmReasonFields("PolicyConfirmFormReasonDeclineQuoteLabel",
										ConfigConstants.LOOKUPTABLE_DeclineReasons);
		}
		else if (ConfigConstants.LIFECYCLEACTION_NotRequiredQuote.equals(policyAction)) {
			createConfirmReasonFields("PolicyConfirmFormReasonNotRequiredQuoteLabel",
										ConfigConstants.LOOKUPTABLE_DeclineReasons);			
		}
		else if (ConfigConstants.LIFECYCLEACTION_PolicyChange.equals(policyAction)) {
			policyChangeEffectiveDateValidator = createPolicyChangeEffectiveDateValidator();
			confirmEffectiveDate = new DatePicker(labels.getString("PolicyConfirmFormPolicyChangeEffectiveDateLabel"));
			confirmDescription   = new TextField(labels.getString("PolicyConfirmFormPolicyChangeDescriptionLabel"));
		}
		else if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(policyAction)) {
			confirmDescription = new TextField(labels.getString("PolicyConfirmFormPolicyChangeDescriptionLabel"));
			
			createRenewalTermLengthField();
		}
		else if (DSUIConstants.UIACTION_DoNotRenew.equalsIgnoreCase(policyAction)) {
			confirmReasonDetailsField = new TextField(labels.getString("PolicyConfirmFormReasonLabel"));
		}
		else if (ConfigConstants.LIFECYCLEACTION_Extension.equals(policyAction)) {
			confirmDescription = new TextField(labels.getString("PolicyConfirmFormExtensionDescriptionLabel"));
			confirmDaysField   = new DSComboBox<Integer>(labels.getString("PolicyConfirmFormExtensionDaysLabel"), Integer.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_ExtensionDays);
		}
		else if (ConfigConstants.LIFECYCLEACTION_Adjustment.equals(policyAction)) {
			policyChangeEffectiveDateValidator = createPolicyChangeEffectiveDateValidator();
			confirmEffectiveDate = new DatePicker(labels.getString("PolicyConfirmFormAdjustmentEffectiveDateLabel"));
			confirmDescription   = new TextField(labels.getString("PolicyConfirmFormAdjustmentDescriptionLabel"));
		}
		
		insuredField			= new TextField(labels.getString("PolicyConfirmFormInsuredLabel"));
		effectiveDateField		= new DatePicker(labels.getString("PolicyConfirmFormEffDateLabel"));
		expiryDateField			= new DatePicker(labels.getString("PolicyConfirmFormExpDateLabel"));
		
		if (isNewBusinessActions()) {
			quotePolicyNumField	= new TextField(labels.getString("PolicyConfirmFormQuoteNumLabel"));
		}
		else {
			quotePolicyNumField = new TextField(labels.getString("PolicyConfirmFormPolicyNumLabel"));
		}
	}
	
	protected void createConfirmReasonFields(String reasonLabelId, String reasonLookupId) {
		confirmReasonCodeField 		= new DSComboBox<String>(labels.getString(reasonLabelId), String.class)
											.withLookupTableId(reasonLookupId);
		confirmReasonDetailsField	= new TextField();
	}
	
	private void createRenewalTermLengthField() {
		renewalTermLengthField = new DSComboBox<Integer>(labels.getString("PolicyConfirmFormRenewalTermLengthLabel"), Integer.class)
			.withLookupTableId(ConfigConstants.LOOKUPTABLE_TermLengthsRenewal);
		renewalTermLengthField.addValueChangeListener(event -> {
			final Integer numMonths = event.getValue();
			
			if (numMonths != null) {
				final LocalDate newExpiryDate = effectiveDateField.getValue().plusMonths(numMonths);
				expiryDateField.setValue(newExpiryDate);
			}
		});
	}
	
	protected void buildBinder() {
		binder = new Binder<PolicyConfirmDto>(PolicyConfirmDto.class);
		if (confirmReasonCodeField != null) {
			binder.forField(confirmReasonCodeField).asRequired().bind(PolicyConfirmDto::getChangeReasonCd, PolicyConfirmDto::setChangeReasonCd);
		}
		if (confirmReasonDetailsField != null) {
			binder.forField(confirmReasonDetailsField).bind(PolicyConfirmDto::getChangeReasonDetails, PolicyConfirmDto::setChangeReasonDetails);
		}
		if (confirmEffectiveDate != null) {
			binder.forField(confirmEffectiveDate).asRequired().withValidator(policyChangeEffectiveDateValidator)
													.bind(PolicyConfirmDto::getChangeEffectiveDate, PolicyConfirmDto::setChangeEffectiveDate);
		}
		if (confirmDescription != null) {
			binder.forField(confirmDescription).bind(PolicyConfirmDto::getChangeDescription, PolicyConfirmDto::setChangeDescription);
		}
		if (confirmDaysField != null) {
			binder.forField(confirmDaysField).asRequired()
												.bind(PolicyConfirmDto::getChangeDays, PolicyConfirmDto::setChangeDays);
		}
		if (renewalTermLengthField != null) {
			binder.forField(renewalTermLengthField)
				.asRequired()
				.bind(PolicyConfirmDto::getRenewalTermLength, PolicyConfirmDto::setRenewalTermLength);
		}
		binder.setReadOnly(true);
		
		ptBinder = new Binder<PolicyTransaction>(PolicyTransaction.class);
		//ptBinder.forField(insuredField).bind("policyVersion.insurancePolicy.policyCustomer.commercialCustomer.legalName");
		ptBinder.forField(effectiveDateField).withConverter(new DSLocaldateTimeToLocalDateConverter())
										.bind("policyTerm.termEffDate");
		ptBinder.forField(expiryDateField).withConverter(new DSLocaldateTimeToLocalDateConverter())
										.bind("policyTerm.termExpDate");
		
		if (isNewBusinessActions()) {
			ptBinder.forField(quotePolicyNumField).bind("policyVersion.insurancePolicy.quoteNum");
		}
		else {
			ptBinder.forField(quotePolicyNumField).bind("policyVersion.insurancePolicy.basePolicyNum");
		}
		
		ptBinder.setReadOnly(true);
	}
	
	protected void bindInsuredField(PolicyTransaction policyTransaction) {
		if (policyTransaction != null) {
			if (policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
				uiHelper.setFieldValue(insuredField, policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName());
			}
			else if (policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_PERS)) {
				uiHelper.setFieldValue(insuredField, personalCustomerUIHelper.getCustomerName(policyTransaction.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getPersonalCustomer()));
			}
		}
		
		insuredField.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
	        );

		if (confirmReasonCodeField != null) {
			form.add(confirmReasonCodeField, 2);
		}
		else if (confirmReasonDetailsField != null) {
			form.add(confirmReasonDetailsField, 2);
		}
		
		if (renewalTermLengthField != null) {
			form.add(renewalTermLengthField, 2);
		}
		
		if (confirmEffectiveDate != null) {
			form.add(confirmEffectiveDate, 1);
			uiHelper.addFormEmptyColumn(form);
			form.add(confirmDescription, 2);
		}
		else if (confirmDescription != null) {
			if (confirmDaysField != null) {
				form.add(confirmDaysField, 1);
				uiHelper.addFormEmptyColumn(form);
			}
			
			form.add(confirmDescription, 2);
		}
		else if (confirmDaysField != null) {
			form.add(confirmDaysField, 1);
		}
		
		form.add(quotePolicyNumField, insuredField);
		form.add(effectiveDateField, expiryDateField);
		
		this.add(form);
	}
	
	public void loadData(PolicyTransaction policyTransaction, PolicyConfirmDto dto) {
		loadData(policyTransaction, dto, null);
	}
	
	public void loadData(PolicyTransaction policyTransaction, PolicyConfirmDto dto, List<LookupTableItem> renewalTermLengths) {
		loadDataConfigs(binder, ptBinder);

		binder.setBean(dto);
		ptBinder.readBean(policyTransaction);
		bindInsuredField(policyTransaction);
		
		enableDataView(binder, ptBinder);
		
		if (policyChangeEffectiveDateValidator != null) {
			policyChangeEffectiveDateValidator.setPolicyTransaction(policyTransaction);
		}
		
		if (renewalTermLengthField != null) {
			renewalTermLengthField.setDSItems(renewalTermLengths);
			if (renewalTermLengthField.getDSItemCount() == 1) {
				// If only one renewal term length is configured, default to that.
				renewalTermLengthField.setDSFirstValue();
			} else if (renewalTermLengthField.getDSItems().contains(12)) {
				// If 12 months is an option, default to that.
				renewalTermLengthField.setDSValue(12);
			}
		}
		
		if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(policyAction)) {
			try {
				PolicyTermDTO termDTO = uiPolicyHelper.getPolicyProcessing().getRenewalEffectiveAndExpiryDates(effectiveDateField.getValue().atStartOfDay());
				if (termDTO != null) {
					effectiveDateField.setValue(termDTO.getTermEffectiveDate().toLocalDate());
					expiryDateField.setValue(termDTO.getTermExpiryDate().toLocalDate());
				}
			} catch (InsurancePolicyException e) {
				uiHelper.notificationException(e);
			}
		}
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
	}
	
	@Override
	public boolean validateEdit() {
		boolean ok = uiHelper.validateValues(binder);
		return ok;
	}
	
	@Override
	public PolicyConfirmDto getConfirmDto() {		
		if (!uiHelper.writeValues(binder, binder.getBean())) {
			return null;
		}
		return binder.getBean();
	}
}
