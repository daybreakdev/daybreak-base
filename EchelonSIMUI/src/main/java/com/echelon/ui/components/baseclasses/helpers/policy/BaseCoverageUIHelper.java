package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.baseclasses.CoverageConfigDto;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.ActionEvent;
import com.ds.ins.uicommon.components.DSUpdateDialog.CancelEvent;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageList;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageListEdit;
import com.echelon.ui.components.baseclasses.policy.BaseCoveragePicker;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageTabs;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.baseclasses.process.policy.BaseCoverageProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyTransactionProcessor;
import com.echelon.ui.components.towing.UWAdjustmentForm;
import com.echelon.ui.components.towing.VehicleCoverageList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.UI;

public class BaseCoverageUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2825997543805743044L;
	protected BaseCoverageConfigUIHelper 	coverageConfigUIHelper = null;
	protected UIConstants.OBJECTTYPES 		parentObjectType 	   = null;
	protected BaseCovReinsuranceUIHelper	covReinsuranceUIHelper;
	
	protected static final String TOTALKEY_COV 	  	 = "COV";
	protected static final String TOTALKEY_END    	 = "END";
	protected static final String TOTALKEY_SUBTOT 	 = "SUBTOT";
	protected static final String TOTALKEY_COVNPC 	 = "COVNPC";
	protected static final String TOTALKEY_ENDNPC 	 = "ENDNPC";
	protected static final String TOTALKEY_SUBTOTNPC = "SUBTOTNPC";
	protected static final String TOTALKEY_GRAND  	 = "GRAND";
	
	public BaseCoverageUIHelper(UIConstants.OBJECTTYPES parentObjectType, POLICYTREENODES policyTreeNode) {
		super();
		
		this.parentObjectType 		= parentObjectType;
		this.policyTreeNode   		= policyTreeNode;
		this.coverageConfigUIHelper = CoverageUIFactory.getInstance().createCoverageConfigHelper(parentObjectType);
		this.covReinsuranceUIHelper = CoverageUIFactory.getInstance().createCovReinsuranceUIHelper(parentObjectType);		
	}

	public interface CoverageEntryCallback {
		public void saveEvent(CoverageDto coverageDto);
		public void cancelEvent(CoverageDto coverageDto);
	}
	
	public void setCurrentCoverageCode(String currentCoverageCode) {
		//this.currentCoverageCode = currentCoverageCode;
	}
	
	public List<TotalPremiumDto> createTotalPremiumDtos(IBusinessEntity covParent) {
		return new ArrayList<TotalPremiumDto>();
	}
	
	protected List<TotalPremiumDto> createSingleTotalPremium(IBusinessEntity covParent, Premium covParentPremium, CoverageDetails coverageDetails) {
		List<TotalPremiumDto> dtos = new ArrayList<TotalPremiumDto>();
		
		TotalPremiumDto totPrem = new TotalPremiumDto(TOTALKEY_GRAND, 10, 
										uiHelper.getSessionLabels().getString("CoverageListTotalPremiumLabel"),
										null);
		if (coverageDetails != null && coverageDetails.getCoveragePremium() != null) {
			totPrem.setTotalPremium(coverageDetails.getCoveragePremium().getWrittenPremium());
		}
		
		dtos.add(totPrem);
		
		return dtos;
	}
	
	protected List<TotalPremiumDto> createBreakDownTotalPremiums(IBusinessEntity covParent, Premium covParentPremium, CoverageDetails coverageDetails) {
		List<TotalPremiumDto> dtos = new ArrayList<TotalPremiumDto>();
		
		TotalPremiumDto covTotPrem 	= new TotalPremiumDto(TOTALKEY_COV, 1, uiHelper.getSessionLabels().getString("CoverageListTotalCovPremLabel"), 0D);
		dtos.add(covTotPrem);
		TotalPremiumDto endTotPrem 	= new TotalPremiumDto(TOTALKEY_END, 2, uiHelper.getSessionLabels().getString("CoverageListTotalEndPremLabel"), 0D);
		dtos.add(endTotPrem);
		TotalPremiumDto subTotPrem 	= new TotalPremiumDto(TOTALKEY_SUBTOT, 3, uiHelper.getSessionLabels().getString("CoverageListTotalPremiumLabel"), 0D);
		dtos.add(subTotPrem);
		TotalPremiumDto totPrem 	    = new TotalPremiumDto(TOTALKEY_GRAND, 10, uiHelper.getSessionLabels().getString("CoverageListTotalPremiumLabel"), null);
		dtos.add(totPrem);
		TotalPremiumDto covTotNPCPrem = new TotalPremiumDto(TOTALKEY_COVNPC, 21, uiHelper.getSessionLabels().getString("CoverageListTotalCovNPCPremLabel"), 0D);
		dtos.add(covTotNPCPrem);
		TotalPremiumDto endTotNPCPrem = new TotalPremiumDto(TOTALKEY_ENDNPC, 22, uiHelper.getSessionLabels().getString("CoverageListTotalEndNPCPremLabel"), 0D);
		dtos.add(endTotNPCPrem);
		TotalPremiumDto subTotNPCPrem = new TotalPremiumDto(TOTALKEY_SUBTOTNPC, 23, uiHelper.getSessionLabels().getString("CoverageListTotalNPCPremiumLabel"), 0D);
		dtos.add(subTotNPCPrem);
		
		if (coverageDetails != null) {
			List<Premium> premList = new ArrayList<Premium>();
			getCoveragesForTotal(covParent, coverageDetails).stream().forEach(o -> premList.add(o.getCoveragePremium()));
			covTotPrem.setTotalPremium(calculateTotalPremium(covParent, premList));
			covTotNPCPrem.setTotalPremium(calculateTotalNPCPremium(covParent, premList));
			
			premList.clear();
			getEndorsementsForTotal(covParent, coverageDetails).stream().forEach(o -> premList.add(o.getEndorsementPremium()));
			endTotPrem.setTotalPremium(calculateTotalPremium(covParent, premList));
			endTotNPCPrem.setTotalPremium(calculateTotalNPCPremium(covParent, premList));
			
			subTotPrem.setTotalPremium(uiHelper.roundNumeric(covTotPrem.getTotalPremium() + endTotPrem.getTotalPremium())
										/*(new BigDecimal(covTotPrem.getTotalPremium()).add(new BigDecimal(endTotPrem.getTotalPremium()))
										.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP)
										.doubleValue())
										*/
										);
			
			subTotNPCPrem.setTotalPremium(uiHelper.roundNumeric(covTotNPCPrem.getTotalPremium() + endTotNPCPrem.getTotalPremium())
										/*(new BigDecimal(covTotNPCPrem.getTotalPremium()).add(new BigDecimal(endTotNPCPrem.getTotalPremium()))
										.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP)
										.doubleValue())
										*/
										);
			
			if (coverageDetails.getCoveragePremium() != null) {
				totPrem.setTotalPremium(coverageDetails.getCoveragePremium().getWrittenPremium());
			}
		}
		
		if (!Constants.VERS_TXN_TYPE_POLICYCHANGE.equalsIgnoreCase(policyUIHelper.getCurrentPolicyVersion().getPolicyTxnType()) &&
			!Constants.VERS_TXN_TYPE_CANCELLATION.equalsIgnoreCase(policyUIHelper.getCurrentPolicyVersion().getPolicyTxnType())) {
			dtos.removeIf(o -> TOTALKEY_COVNPC.equalsIgnoreCase(o.getKey()) ||
								TOTALKEY_ENDNPC.equalsIgnoreCase(o.getKey()) ||
								TOTALKEY_SUBTOTNPC.equalsIgnoreCase(o.getKey()));
		}
		
		return dtos;
	}
	
	protected Set<Coverage> getCoveragesForTotal(IBusinessEntity covParent, CoverageDetails coverageDetails) {
		return coverageDetails.getCoverages();
	}
	
	protected Set<Endorsement> getEndorsementsForTotal(IBusinessEntity covParent, CoverageDetails coverageDetails) {
		return coverageDetails.getEndorsements();
	}
	
	protected int getDecimals() {
		int decimals = 0;
		if (configUIHelper.getCurrentProductConfig() != null) {
			decimals = configUIHelper.getCurrentProductConfig().getPremiumDecimals();
		}
		
		return decimals;
	}
	
	protected Double calculateTotalPremium(IBusinessEntity covParent, List<Premium> premList) {
		Double result = 0D;
		
		if (premList != null && !premList.isEmpty()) {
			Double totPremium = premList.stream().mapToDouble(Premium::getWrittenPremium).sum();
			result = uiHelper.roundNumeric(totPremium)
					/*(new BigDecimal(totPremium))
						.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP)
						.doubleValue()
						*/
					;
		}
		
		return result;
	}
	
	protected Double calculateTotalNPCPremium(IBusinessEntity covParent, List<Premium> premList) {
		Double result = 0D;
		
		if (premList != null && !premList.isEmpty()) {
			Double totPremium = premList.stream().mapToDouble(Premium::getNetPremiumChange).sum();
			result = uiHelper.roundNumeric(totPremium)
					/* (new BigDecimal(totPremium))
						.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP)
						.doubleValue()
						*/
					;
		}
		
		return result;
	}

	protected BaseCoveragePicker createCoveragePicker(String action, UIConstants.UICONTAINERTYPES containerType) {
		BaseCoveragePicker form = new BaseCoveragePicker(action, containerType, this);
		return form;
	}
		
	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<BaseCoveragePicker> createCoveragePickerDialog(String action) {
		DSUpdateDialog<BaseCoveragePicker> dialog = (DSUpdateDialog<BaseCoveragePicker>) createDialog(
															uiHelper.getSessionLabels().getString("CoveragePickerTitle"), 
															createCoveragePicker(action, UICONTAINERTYPES.Dialog), 
															action);
		dialog.setWidth("800px");
		dialog.setHeight("800px");

		return dialog;
	}
	
	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<BaseCoverageTabs> createCoverageDialog(CoverageDto coverageDto, String title, String action) {
		DSUpdateDialog<BaseCoverageTabs> dialog = (DSUpdateDialog<BaseCoverageTabs>) createDialogWithActions(title, action,
															new BaseCoverageTabs(parentObjectType, coverageDto.getCoverageType(), coverageDto.getCoverageCode(), coverageDto.isAllowReinsurance(), action, UICONTAINERTYPES.Dialog),
															UIConstants.UIACTION_Save, UIConstants.UIACTION_ManualCancel
															);		
		dialog.getContentComponent().setParentDialog(dialog);
		return dialog;
	}

	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<BaseCoverageListEdit> createCoverageListEditDialog(String title, String action) {
		DSUpdateDialog<BaseCoverageListEdit> dialog = (DSUpdateDialog<BaseCoverageListEdit>) createDialogWithActions(title, action,
															CoverageUIFactory.getInstance().createCoverageListEdit(parentObjectType, action, UICONTAINERTYPES.Dialog),
															UIConstants.UIACTION_Reinsurance, UIConstants.UIACTION_Rate, UIConstants.UIACTION_ApplyAdjUW, UIConstants.UIACTION_Save, UIConstants.UIACTION_ManualCancel
															);
		dialog.setWidth("1700px");
		dialog.setHeight("750px");
		//dialog.setResizable(true);
		//dialog.setDraggable(true);
		dialog.getContentComponent().setContainerDialog(dialog);
		
		return dialog;
	}
	
	@SuppressWarnings("unchecked")
	private DSUpdateDialog<UWAdjustmentForm> createUWAdjustmentDialog() {
		DSUpdateDialog<UWAdjustmentForm> dialog = (DSUpdateDialog<UWAdjustmentForm>) createDialogWithActions(
															uiHelper.getSessionLabels().getString("CoverageUWAdjustmentDialogTitle"), 
															UIConstants.UIACTION_ApplyAdjUW,
															new UWAdjustmentForm(UIConstants.UIACTION_ApplyAdjUW, UICONTAINERTYPES.Dialog),
															UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel
															);
		
		dialog.setWidth("400px");
		
		return dialog;
	}
	
	public void pickCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, BaseCoverageList coverageListUI) {
		IGenericProductConfig config = configUIHelper.getCurrentProductConfig();
		if (config != null) {
			DSUpdateDialog<BaseCoveragePicker> dialog = createCoveragePickerDialog(UIConstants.UIACTION_Add);
			dialog.setDataStore(new DSDialogDataStore(covConfigParent, covParent));
			dialog.getContentComponent().loadData(covParent,
											getAvailableCoverages(covConfigParent, coverageListUI.getCurrentCoverageList(),
																	BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType()));
			dialog.open();
		}
	}
	
	protected List<CoverageDto> buildFullCoverageList(ICoverageConfigParent covConfigParent, IBusinessEntity covParent) {
		List<CoverageDto> results = 
				((BaseCoverageProcessor)(new TaskProcessor(UI.getCurrent())).processRun(
								createBuildCoverageListProcessor(covConfigParent, covParent)
								)).getCoverageDtoList();
		
		return results;
	}
	
	protected List<CoverageConfigDto> getAvailableCoverages(ICoverageConfigParent covConfigParent, List<CoverageDto> currCoverages,
															String versionTxnType) {
		HashMap<String, CoverageConfigDto> avaList = configUIHelper.getAvailableCoverages(covConfigParent, versionTxnType);
		
		List<CoverageConfigDto> results = new ArrayList<CoverageConfigDto>();
		if (avaList != null && !avaList.isEmpty()) {
			results.addAll(avaList.values());
			results = results.stream().filter(o1 ->
							o1.getCoverageConfig().isAllowMultiple() ||
							currCoverages.stream().filter(o2 -> !o2.wasDeletedCoverage() &&
																o1.getType().equalsIgnoreCase(o2.getCoverageType()) &&
																o1.getCoverageConfig().getCode().equalsIgnoreCase(o2.getCoverageCode())
															).findFirst().orElse(null) == null
											).collect(Collectors.toList());
		}
		
		return results;
	}
	
	protected DSUpdateDialog<?> setupCoverageAction(String title, String action,
										ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, 
										boolean isMultiple,
										CoverageEntryCallback callback,
										List<CoverageDto> newCoverages) {
		return setupCovCoverageAction(title, action, covConfigParent, covParent, coverageDto, 
										isMultiple, callback, newCoverages);
	}
	
	protected DSUpdateDialog<?> setupCoverageGridAction(String title, String action,
										ICoverageConfigParent covConfigParent, IBusinessEntity covParent, 
										List<CoverageDto> coverageDtos) {
		DSUpdateDialog<BaseCoverageListEdit> dialog = createCoverageListEditDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(covConfigParent, covParent, coverageDtos));
		dialog.getContentComponent().loadData(covConfigParent, covParent, coverageDtos);
		if (UIConstants.UIACTION_View.equals(action)) {
			if (dialog.getButton(UIConstants.UIACTION_Reinsurance) != null) {
				dialog.getButton(UIConstants.UIACTION_Reinsurance).setEnabled(false);
				dialog.getButton(UIConstants.UIACTION_Reinsurance).setVisible(false);
			}
		}
		else {
			if (dialog.getButton(UIConstants.UIACTION_Reinsurance) != null) {
				dialog.getButton(UIConstants.UIACTION_Reinsurance).setEnabled(false);
			}
			
			dialog.getContentComponent().enableEdit(dialog.getToolbar());
		}
		dialog.open();
		return dialog;
	}
	
	protected DSDialogDataStore createDialogDataStore(ICoverageConfigParent covConfigParent, 
														IBusinessEntity covParent, CoverageDto coverageDto, 
														boolean isMultiple,
														CoverageEntryCallback callback,
														List<CoverageDto> newCoverages) {
		return new DSDialogDataStore(covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages);
	}
	
	protected DSUpdateDialog<?> setupCovCoverageAction(String title, String action,
											ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, 
											boolean isMultiple,
											CoverageEntryCallback callback,
											List<CoverageDto> newCoverages) {
		DSUpdateDialog<BaseCoverageTabs> dialog = createCoverageDialog(coverageDto, title, action);
		/*
		if (coverageDto.getCoveragePremium() == null) {
			coverageDto.setCoveragePremium(new Premium());
		}
		*/
		/*
		if (coverage.getCoverage().getDataExtension() == null) {
			coverage.getCoverage().setDataExtension(new DataExtension());
		}
		*/
		dialog.setDataStore(createDialogDataStore(covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages));
		dialog.getContentComponent().loadData(coverageDto);
		if (!UIConstants.UIACTION_View.equals(action)) {
			dialog.getContentComponent().getCovAndEndorseForm().enableEdit(coverageDto);
			dialog.getContentComponent().getCovAndEndorseForm().setFocus();
		}
		dialog.open();
		return dialog;
	}
	
	public DSUpdateDialog<?> updateNewCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, 
														boolean isMultiple, CoverageEntryCallback callback,
														List<CoverageDto> newCoverages) {
		String title = uiHelper.getSessionLabels().getString("CoverageNewDialogTitle");		
		return setupCoverageAction(title, UIConstants.UIACTION_New, covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages);
	}
	
	public DSUpdateDialog<?> updateCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, CoverageEntryCallback callback) {
		String title = uiHelper.getSessionLabels().getString("CoverageUpdateDialogTitle");		
		return setupCoverageAction(title, UIConstants.UIACTION_Update, covConfigParent, covParent, coverageDto, false, callback, null);
	}
	
	public DSUpdateDialog<?> viewCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, CoverageEntryCallback callback) {
		String title = uiHelper.getSessionLabels().getString("CoverageViewDialogTitle");		
		return setupCoverageAction(title, UIConstants.UIACTION_View, covConfigParent, covParent, coverageDto, false, callback, null);
	}
	
	public DSUpdateDialog<?> editCoverageGridAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverageDtos) {
		String title = uiHelper.getSessionLabels().getString("CoverageUpdateDialogTitle");
		return setupCoverageGridAction(title, UIConstants.UIACTION_Update, covConfigParent, covParent, coverageDtos);
	}
	
	public DSUpdateDialog<?> viewCoverageGridAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverageDtos) {
		String title = uiHelper.getSessionLabels().getString("CoverageViewDialogTitle");
		return setupCoverageGridAction(title, UIConstants.UIACTION_View, covConfigParent, covParent, coverageDtos);
	}

	// For picker
	@SuppressWarnings("unchecked")
	@Override
	protected void onSaveAdd(SaveEvent event) {
		DSUpdateDialog<BaseCoveragePicker> dialog = event.getSource();
		ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
		IBusinessEntity 	  covParent 	  = (IBusinessEntity) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateSelection()) {
			List<CoverageConfigDto> selectedCoverages = dialog.getContentComponent().getSelectedCoverageConfigs();

			if (validateCoverageSelections(dialog, covConfigParent, covParent, selectedCoverages)) {
				processNewCoverages(dialog, covConfigParent, covParent, selectedCoverages);
			}
		}
	}
	
	protected boolean validateCoverageSelections(DSUpdateDialog<BaseCoveragePicker> dialog, ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
													List<CoverageConfigDto> selectedCoverages) {
		return true;
	}
	
	protected void processNewCoverages(DSUpdateDialog<BaseCoveragePicker> dialog, ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
										List<CoverageConfigDto> selectedCoverages) {
		dialog.close();
		
		List<CoverageDto> newCoverageDtos = 
				((BaseCoverageProcessor)(new TaskProcessor(UI.getCurrent())).processRun(
						createNewCoverageSelectionsProcessor(
													covConfigParent,
													covParent, 
													selectedCoverages)
						)).getCoverageDtoList();
		
		if (newCoverageDtos != null && !newCoverageDtos.isEmpty()) {
			if (newCoverageDtos.size() == 1) {
				CoverageDto coverageDto = newCoverageDtos.get(0);
				processSingleNewCoverage(covConfigParent, covParent, coverageDto, newCoverageDtos);
			}
			else {
				processMultipleNewCoverages(covConfigParent, covParent, newCoverageDtos);
			}
		}
	}
	
	protected void processSingleNewCoverage(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, 
										 	CoverageDto coverageDto, List<CoverageDto> newCoverages) {
		if (coverageDto.isDetailRequired()) {
			updateNewCoverageAction(covConfigParent, covParent, coverageDto, false, null, newCoverages);
		}
		else {
			runSave(covConfigParent, covParent, coverageDto, null, UIConstants.UIACTION_SaveNew);
		}
	}
	
	// OnSaveNew -> will run the callback defined in processNextNewCoverage
	// the coverages will be saved at the end when all details data are collected
	protected void processMultipleNewCoverages(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, 
												List<CoverageDto> newCoverages) {
		
		// Collect details of coverages
		boolean reqDetailEntry = false;
		for(CoverageDto coverageDto : newCoverages) {
			if (!coverageDto.isDetailEntered() && coverageDto.isDetailRequired()) {
				processNextNewCoverage(covConfigParent, covParent, newCoverages, coverageDto);
				reqDetailEntry = true;
				break;
			}
		}
		
		// all details entered
		if (!reqDetailEntry) {
			createCoverageTaskProcessor().start(
						createMultipleCoverageProcessor(UIConstants.UIACTION_SaveNew,
														policyUIHelper.getParentPolicyTransaction(covParent), 
														covConfigParent, covParent, newCoverages)
					);
		}
	}

	protected void processNextNewCoverage(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> newCoverages, 
		 									CoverageDto coverageDto) {
		CoverageEntryCallback callback = new CoverageEntryCallback() {
			
			@Override
			public void saveEvent(CoverageDto coverageDto) {
				coverageDto.setDetailEntered(true);
				processMultipleNewCoverages(covConfigParent, covParent, newCoverages);
			}
			
			@Override
			public void cancelEvent(CoverageDto coverageDto) {
				
			}
		};
		
		updateNewCoverageAction(covConfigParent, covParent, coverageDto, true, callback, newCoverages);
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		setupSaveAction(event, UIConstants.UIACTION_SaveNew);
	}
	
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<?> dialog = event.getSource();
		CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData3();
		if (coverageDto != null && coverageDto.getCoveragePK() == null) { // new coverage
			setupSaveAction(event, UIConstants.UIACTION_SaveNew);
		}
		else {
			setupSaveAction(event, UIConstants.UIACTION_Save);
		}
	}
	
	@Override
	protected void onCancel(CancelEvent event) {
		// TODO Auto-generated method stub
		super.onCancel(event);
		
		DSUpdateDialog<?> dialog = event.getSource();
		if (dialog.getContentComponent() instanceof BaseCoverageTabs) {
			CoverageDto coverageDto = (CoverageDto) dialog.getDataStore().getData3();
			if (coverageDto.getCoveragePK() != null) { // not creating new coverage 
				resetEditEnd(coverageDto);
			}
		}
		else if (dialog.getContentComponent() instanceof BaseCoverageListEdit) {
			List<CoverageDto> coverageDtoList = ((BaseCoverageListEdit)dialog.getContentComponent()).getCurrentCoverageList();
			resetEditValid(coverageDtoList);
			resetEditDelete(coverageDtoList);
			resetEditEnd(coverageDtoList);
		}
	}

	protected void resetEditValid(List<CoverageDto> coverageDtoList) {
		coverageDtoList.stream().forEach(o -> {
						o.resetValidation();
						});
	}

	protected void resetEditDelete(List<CoverageDto> coverageDtoList) {
		coverageDtoList.stream().forEach(o -> {
						o.resetEditStatus();
						o.setDeleteButton(null);
						o.setUndeleteButton(null);
						});
	}
	
	protected void resetEditEnd(List<CoverageDto> coverageDtoList) {
		coverageDtoList.stream().forEach(o -> resetEditEnd(o));
	}
	
	protected void resetEditEnd(CoverageDto dto) {
		dto.setCoverageAdditionalForm(null);
		dto.setEditLimitField(null);
		dto.setEditDeductibleField(null);
		dto.setEditPremiumField(null);
		dto.setEditIsPremiumOverrideField(null);
		dto.setEditIsReinsuredField(null);
		dto.setEditUwAdjustmentField(null);
	}
	
	@SuppressWarnings("unchecked")
	protected void setupSaveGridAction(SaveEvent event, String action) {
		DSUpdateDialog<BaseCoverageListEdit> dialog = event.getSource();
		ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
		IBusinessEntity		  covParent	 	  = (IBusinessEntity) dialog.getDataStore().getData2();
		List<CoverageDto>     coverageDtoList = dialog.getContentComponent().getCurrentCoverageList();
				
		resetEditValid(coverageDtoList);
		
		if (!dialog.getContentComponent().validateEdit()) {
			return;
		}
		
		// Validate details/form
		// Save details/form screen values
		for(CoverageDto dto : coverageDtoList) {
			if (dto.isDeletedCoverage() || dto.wasDeletedCoverage()) {
				continue;
			}
			
			preValidAndSaveCoverage(covParent, dto.getCoverageAdditionalForm(), dto, action, coverageDtoList);
			// not deleted
			if (!dto.isEditDeleted()) {
				/* moved back to grid's validateEdit
				if (!dto.isLimitEditValid() || !dto.isDeductibleEditValid()) {
					dto.setEditValid(false);
					ok = false;
				}
				*/
				if (!validAndSaveCoverageAdditionalFields(dto, covParent, coverageDtoList, action)) {
					dto.setEditValid(false);
				}
			}
		}
		
		// Any invalid
		boolean allValid = true;
		if (coverageDtoList.stream().filter(o -> o.isEditValid() != null && !o.isEditValid()).findFirst().isPresent()) {
			allValid = false;
			//uiHelper.showWaringRequiredFields();
		}
		showEditMessages(coverageDtoList);
		
		if (!allValid) {
			dialog.getContentComponent().updateValidateEdit();
			return;
		}
		
		if (!dialog.getContentComponent().saveEdit()) {
			return;
		}
		
		getRatringWarningsAndSaveGrid(dialog, covConfigParent, covParent, coverageDtoList);
	}
	
	protected void showEditMessages(List<CoverageDto> dtos) {
		if (dtos == null || ! dtos.isEmpty()) {
			// Any general error
			boolean generalMsgFound = dtos.stream().filter(o -> o.isEditHasGeneralError()).findFirst().isPresent();
			
			// Any error message
			List<String> messages = dtos.stream().filter(o -> o.getEditMessages() != null && !o.getEditMessages().isEmpty()).collect(Collectors.toList())
									.stream().map(CoverageDto::getEditMessages).collect(Collectors.toSet())
									.stream().flatMap(Collection::stream).collect(Collectors.toSet()) // make message unique
									.stream().collect(Collectors.toList());
			if (generalMsgFound || (messages != null && !messages.isEmpty())) {
				Collections.sort(messages);
				
				if (generalMsgFound) {
					messages.add(0, uiHelper.getSessionLabels().getString(UIConstants.ERRORCODE_GENERAL));
				}
				
				uiHelper.notificationWarnings(messages);
			}
		}
	}
	
	protected boolean isCoverageInGridHasAdditionalFields(CoverageDto dto) {
		return dto.hasAnyAdditionalFields();
	}
	
	protected boolean validAndSaveCoverageAdditionalFields(CoverageDto dto, IBusinessEntity covParent, List<CoverageDto> workingList, String action) {
		boolean ok = true;
		
		//validAndSaveCoverage : Original design is for validate data extension only 
		if (isCoverageInGridHasAdditionalFields(dto)) {
			ok = validAndSaveCoverage(covParent, dto.getCoverageAdditionalForm(), dto, workingList, action);
		}
		else {
			ok = validReinsurances(dto);
			
			if (dto.getCoverageAdditionalForm() != null && !dto.getCoverageAdditionalForm().validateEdit(false)) {
				setGeneralError(dto);
				ok = false;
			}
		}
		
		return ok;
	}
	
	public void setGeneralError(CoverageDto dto) {
		dto.setEditHasGeneralError(true);
	}
	
	@SuppressWarnings("unchecked")
	protected void setupSaveAction(SaveEvent event, String action) {
		DSUpdateDialog<?> 		dialog  		  = event.getSource();
		ICoverageConfigParent 	covConfigParent   = (ICoverageConfigParent) dialog.getDataStore().getData1();
		IBusinessEntity			covParent		  = (IBusinessEntity) dialog.getDataStore().getData2();
		CoverageDto 			coverageDto		  = (CoverageDto) dialog.getDataStore().getData3();
		boolean					isMultiple	   	  = dialog.getDataStore().getData4() != null && (Boolean) dialog.getDataStore().getData4();
		CoverageEntryCallback 	callback 		  = (CoverageEntryCallback) dialog.getDataStore().getData5();
		List<CoverageDto> 		coverageDtoList   = (List<CoverageDto>) dialog.getDataStore().getData6();
		IBaseCovAndEndorseForm	covAndEndorseForm = ((BaseCoverageTabs)dialog.getContentComponent()).getCovAndEndorseForm();
		
		preValidAndSaveCoverage(covParent, covAndEndorseForm, coverageDto, action, coverageDtoList);
		resetEditValid(Arrays.asList(coverageDto));
		
		if (validAndSaveCoverage(covParent, covAndEndorseForm, coverageDto, null, action)) {
			getRatringWarningsAndSave(dialog, covConfigParent, covParent, coverageDto, isMultiple, callback, action);
		}
		else {
			showEditMessages(Arrays.asList(coverageDto));
		}
	}
	
	protected boolean preValidAndSaveCoverage(IBusinessEntity covParent, IBaseCovAndEndorseForm form, CoverageDto coverageDto, 
												String action,
												List<CoverageDto> newCoverages) {
		return true;
	}
	
	protected boolean validAndSaveCoverage(IBusinessEntity covParent, IBaseCovAndEndorseForm form, CoverageDto coverageDto,
											List<CoverageDto> workingList,
											String action) {
		
		boolean ok = validReinsurances(coverageDto);
		
		if (form.validateEdit(false)) {
			coverageDto.createDataExtensionIfNone();
			
			if (form.saveEdit(coverageDto)) {
				coverageDto.removeDataExtensionIfEmpty();
			}
			else {
				ok = false;
			}
		}
		else {
			ok = false;
		}
		
		if (!ok) {
			setGeneralError(coverageDto);
		}
		
		return ok;
	}

	protected void runSave(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, DSUpdateDialog<?> dialog, String action) {
		createCoverageTaskProcessor(dialog).start(
				createCoverageActionProcessor(action, covConfigParent, covParent, coverageDto));
	}
	
	public void deleteCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("CoverageDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(covConfigParent, coverageDto, covParent));
		dialog.open();
	}
	
	public void undeleteCoverageAction(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("CoverageUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(covConfigParent, coverageDto, covParent));
		dialog.open();
	}
	
	@Override
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog    = event.getSource();
		ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
		CoverageDto 	coverageDto 		  = (CoverageDto) dialog.getDataStore().getData2(); 
		IBusinessEntity covParent   		  = (IBusinessEntity) dialog.getDataStore().getData3();
		
		runDelete(covConfigParent, covParent, coverageDto, dialog, UIConstants.UIACTION_Delete);
	}
	
	protected void runDelete(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, DSConfirmDialog dialog, String action) {
		createCoverageTaskProcessor(dialog).start(
				createCoverageActionProcessor(UIConstants.UIACTION_Delete, covConfigParent, covParent, coverageDto));
	}
	
	@Override
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog    = event.getSource();
		ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
		CoverageDto 	coverageDto 		  = (CoverageDto) dialog.getDataStore().getData2(); 
		IBusinessEntity covParent   		  = (IBusinessEntity) dialog.getDataStore().getData3();
		
		runUndelete(covConfigParent, covParent, coverageDto, dialog, UIConstants.UIACTION_Undelete);
	}
	
	protected void runUndelete(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, DSConfirmDialog dialog, String action) {
		createCoverageTaskProcessor(dialog).start(
				createCoverageActionProcessor(UIConstants.UIACTION_Undelete, covConfigParent, covParent, coverageDto));
	}
	
	protected void reopenCoverageList(PolicyTransaction<?> policyTransaction, IBusinessEntity covParent, CoverageDto coverageDto, String action) {
		if (policyTreeNode != null) {
			policyUIHelper.reopenPolicy(policyTransaction, this.policyTreeNode, covParent);
		}
	}
	
	protected DSDialogDataStore createTaskDataStore(IBusinessEntity covParent, CoverageDto coverageDto) {
		return new DSDialogDataStore(covParent, coverageDto);
	}

	protected void runRate(IDSDialog dialog, List<CoverageDto> coverageDtos, ICoverageConfigParent covConfigParent, IBusinessEntity covParent, boolean loadPolicyTransactionFull) {
		createCoverageTaskProcessor(dialog).start(
				createRateCoverageProcessor(coverageDtos, covConfigParent, covParent, loadPolicyTransactionFull));
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onAction(ActionEvent event) {
		if (event.getSource().getContentComponent() instanceof BaseCoverageTabs) {
			if (UIConstants.UIACTION_Save.equals(event.getAction())) {
				DSUpdateDialog.SaveEvent saveEvent = new DSUpdateDialog.SaveEvent(event.getSource(), true);
				onSaveUpdate(saveEvent);
			}
			else if (UIConstants.UIACTION_ManualCancel.equals(event.getAction())) {
				onManualCancel(event);
			}
		}
		else if (event.getSource().getContentComponent() instanceof UWAdjustmentForm) {
			DSUpdateDialog<UWAdjustmentForm> dialog = event.getSource();
			if (UIConstants.UIACTION_Save.equals(event.getAction())) {
				onApplyAdjUW(event);
			}
			else if (UIConstants.UIACTION_Cancel.equals(event.getAction())) {
				dialog.close();
			}
		}
		else if (event.getSource().getContentComponent() instanceof BaseCoverageListEdit) {
			if (UIConstants.UIACTION_Rate.equals(event.getAction())) {
				onRate(event);
			}
			else if (UIConstants.UIACTION_ApplyAdjUW.equals(event.getAction())) {
				onEnterAdjUW(event);
			}
			else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
				DSUpdateDialog.SaveEvent saveEvent = new DSUpdateDialog.SaveEvent(event.getSource(), true);
				setupSaveGridAction(saveEvent, UIConstants.UIACTION_Save);
			}
			else if (UIConstants.UIACTION_ManualCancel.equals(event.getAction())) {
				onManualCancel(event);
			}
			else if (UIConstants.UIACTION_Reinsurance.equals(event.getAction())) {
				onReinsurance(event);
			}
		}
	}

	@SuppressWarnings("unchecked")
	protected void onRate(ActionEvent event) {
		DSUpdateDialog<BaseCoverageListEdit> dialog = event.getSource();
		
		if (dialog.getContentComponent().saveEdit()) {
			dialog.getContentComponent().deselectRow();

			// Need to move screen values of additional fields to coverage object
			List<CoverageDto> editCovDtos = dialog.getContentComponent().getCurrentCoverageList();
			if (editCovDtos != null && !editCovDtos.isEmpty()) {
				for(CoverageDto covDto : editCovDtos) {
					if (covDto.hasAnyAdditionalFields()) {
						covDto.createDataExtensionIfNone();
						
						if (covDto.getCoverageAdditionalForm().saveEdit(covDto)) {
							covDto.removeDataExtensionIfEmpty();
						}
					}
				}
			}

			ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
			IBusinessEntity covParent 			  = (IBusinessEntity) dialog.getDataStore().getData2();
			// try not to load transaction again
			boolean loadPolicyTransactionFull     = !BrokerUI.getUserSession().getCurrentPolicy().isFullTransDataLoaded();
			
			runRate(dialog, dialog.getContentComponent().getCurrentCoverageList(), covConfigParent, covParent, loadPolicyTransactionFull);
		}
	}

	// coverageDtos is the newly rebuilt from the processor
	@SuppressWarnings("rawtypes")
	protected void onRateComplete(IDSDialog dialog, List<CoverageDto> coverageDtos) {
		if (dialog != null && dialog instanceof DSUpdateDialog<?> &&
				((DSUpdateDialog)dialog).getContentComponent() != null &&
				((DSUpdateDialog)dialog).getContentComponent() instanceof BaseCoverageListEdit) {
			DSUpdateDialog updDialog = (DSUpdateDialog)dialog;
			BaseCoverageListEdit coverageListEdit = (BaseCoverageListEdit) updDialog.getContentComponent();
			ICoverageConfigParent covConfigParent = (ICoverageConfigParent) updDialog.getDataStore().getData1();
			IBusinessEntity covParent 			  = (IBusinessEntity) updDialog.getDataStore().getData2();
			
			// The list in the edit screen
			// copy the edit mode
			List<CoverageDto> editCovDtos = coverageListEdit.getCurrentCoverageList();
			if (editCovDtos != null && !editCovDtos.isEmpty()) {
				for(CoverageDto ratedCovDto : coverageDtos) {
					// All coverage should have PK; cannot create new coverage in edit grid
					if (ratedCovDto.getCoveragePK() != null) {
						CoverageDto editCovDto 
							= editCovDtos.stream().filter(o ->
										o.getCoveragePK() != null && 
										o.getCoveragePK().longValue() == ratedCovDto.getCoveragePK().longValue()
									).findFirst().orElse(null);
						if (editCovDto != null) {
							ratedCovDto.setEditStatus(editCovDto.getEditStatus());
						}
					}
				}
			}
			
			BrokerUI.getUserSession().getCurrentPolicy().setFullTransDataLoaded(true);
			coverageListEdit.reLoadData(covConfigParent, covParent, coverageDtos);
			coverageListEdit.enableEdit(updDialog.getToolbar());
		}
	
		dialog.setProcessEnded();
	}

	@SuppressWarnings("unchecked")
	protected void onEnterAdjUW(ActionEvent event) {
		DSUpdateDialog<BaseCoverageListEdit> listEditdialog = event.getSource();
		DSUpdateDialog<UWAdjustmentForm> uwDialog = createUWAdjustmentDialog();
		uwDialog.setDataStore(new DSDialogDataStore(listEditdialog));
		uwDialog.getContentComponent().setFocus();
		uwDialog.open();
	}
	
	@SuppressWarnings("unchecked")
	protected void onApplyAdjUW(ActionEvent event) {
		DSUpdateDialog<UWAdjustmentForm> uwDialog = event.getSource();
		if (uwDialog.getContentComponent().validEdit()) {
			Double uwAfjuestmentValue = uwDialog.getContentComponent().getUwAfjuestmentValue();
			uwDialog.close();
			
			DSUpdateDialog<BaseCoverageListEdit> listEditdialog = (DSUpdateDialog<BaseCoverageListEdit>) uwDialog.getDataStore().getData1();
			listEditdialog.getContentComponent().applyAdjUW(uwAfjuestmentValue);
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected void onManualCancel(ActionEvent event) {
		DSUpdateDialog<?> dialog = event.getSource();
		
		if (dialog != null && dialog instanceof DSUpdateDialog<?> &&
			((DSUpdateDialog)dialog).getContentComponent() != null) {
			if (((DSUpdateDialog)dialog).getContentComponent() instanceof BaseCoverageTabs) {
				BaseCoverageTabs coverageTabs = (BaseCoverageTabs) ((DSUpdateDialog)dialog).getContentComponent();
				if ((coverageTabs.getCovAndEndorseForm()  != null && coverageTabs.getCovAndEndorseForm().isDataInstanceModified()) ||
					(coverageTabs.getCovReinsuranceList() != null && coverageTabs.getCovReinsuranceList().isDataInstanceModified())) {
					IBusinessEntity covParent = (IBusinessEntity) dialog.getDataStore().getData2();
					runReOpen(dialog, covParent);
					return;
				}
			}
			else if (((DSUpdateDialog)dialog).getContentComponent() instanceof BaseCoverageListEdit) {
				BaseCoverageListEdit coverageListEdit = (BaseCoverageListEdit) ((DSUpdateDialog)dialog).getContentComponent();
				if (coverageListEdit.isDataInstanceModified()) {
					IBusinessEntity covParent = (IBusinessEntity) dialog.getDataStore().getData2();
					runReOpen(dialog, covParent);
					return;
				}
			}
		}
		
		DSUpdateDialog.CancelEvent cancelEvent = new DSUpdateDialog.CancelEvent(event.getSource(), true);
		onCancel(cancelEvent);
	}

	public void loadView(VehicleCoverageList component, SpecialtyVehicleRisk vehicle) {
		if (vehicle != null) {
			if (vehicle.getSubPolicy() != null) {
				final RiskConfig riskConfig = configUIHelper.getSubPolicyRiskConfig(vehicle.getSubPolicy().getSubPolicyTypeCd(), vehicle.getRiskType());
				if (riskConfig != null) {
					List<CoverageDto> coverageDtoList = buildFullCoverageList(riskConfig, vehicle);
					component.loadData(riskConfig, vehicle, coverageDtoList);
				}
			}
		}
	}
	
	@SuppressWarnings("serial")
	protected void runReOpen(IDSDialog dialog, IBusinessEntity covParent) {
		(new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyTransactionProcessor processor = (BasePolicyTransactionProcessor)taskRun.getProcessor();
					openOpenComplete(dialog, processor.getPolicyTransaction());
					return true;
				}
				
				return true;
			}			
		}
		).start(
			new BasePolicyTransactionProcessor(UIConstants.UIACTION_Open,
							BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction()
							)
		);				
	}
	
	protected void openOpenComplete(IDSDialog dialog, PolicyTransaction<?> policyTransaction) {
		if (dialog != null) {
			DSUpdateDialog<?> updDialog = (DSUpdateDialog<?>) dialog;
			IBusinessEntity covParent   = (IBusinessEntity) updDialog.getDataStore().getData2();
			reopenCoverageList(policyTransaction, covParent, null, UIConstants.UIACTION_Open);
		}
		
		DSUpdateDialog.CancelEvent cancelEvent = new DSUpdateDialog.CancelEvent((DSUpdateDialog<?>) dialog, true);
		onCancel(cancelEvent);
	}
	
	protected BaseCoverageProcessor getCoverageProcessor(String action) {
		return CoverageUIFactory.getInstance().getCoverageProcessor(
											parentObjectType, action, 
											policyUIHelper.getCurrentPolicyTransaction(),
											coverageConfigUIHelper,
											configUIHelper.getCurrentProductConfig(),
											policyUIHelper.getPolicyProcessing(),
											BrokerUI.getUserSession().getUserProfile());
	}
	
	protected BaseCoverageProcessor createNewCoverageSelectionsProcessor(ICoverageConfigParent covConfigParent,
																			IBusinessEntity covParent, 
																			List<CoverageConfigDto> coverageConfigList) {
		try {
			return getCoverageProcessor(UIConstants.UIACTION_Select)
												.withParameters(covConfigParent, covParent, coverageConfigList);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		} 
		return null;
	}
	
	protected BaseCoverageProcessor createBuildCoverageListProcessor(ICoverageConfigParent covConfigParent,
																		IBusinessEntity covParent) {
		try {
			return getCoverageProcessor(UIConstants.UIACTION_List)
										 		.withParameters(covConfigParent, covParent);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		} 
		return null;
	}
	
	protected BaseCoverageProcessor createRateCoverageProcessor(List<CoverageDto> coverageDtoList, ICoverageConfigParent covConfigParent,
																	IBusinessEntity covParent,
																	boolean loadPolicyTransactionFull) {
		try {
			return getCoverageProcessor(UIConstants.UIACTION_Rate)
										 		.withParameters(coverageDtoList, covConfigParent, covParent, loadPolicyTransactionFull);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		} 
		return null;
	}
	
	protected BaseCoverageProcessor createCoverageActionProcessor(String action, 
												ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto) {
		try {
			return getCoverageProcessor(action)
										 		.withParameters(covConfigParent, covParent, Arrays.asList(coverageDto), true);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		}
		return null;
	}
	
	protected BaseCoverageProcessor createMultipleCoverageProcessor(String action, 
																		PolicyTransaction<?> policyTransaction,
																		ICoverageConfigParent covConfigParent, 
																		IBusinessEntity covParent, 
																		List<CoverageDto> coverageDtoList) {
		try {
			return getCoverageProcessor(action)
										 		.withParameters(covConfigParent, covParent, coverageDtoList, false);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		}
		return null;
	}
	
	protected TaskProcessor createCoverageTaskProcessor() {
		return createCoverageTaskProcessor(null);
	}
	
	@SuppressWarnings("serial")
	protected TaskProcessor createCoverageTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BaseCoverageProcessor processor = (BaseCoverageProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_Rate:
						onRateComplete(dialog, processor.getCoverageDtoList());
						return true;
						
					default:
						if (dialog != null) {
							dialog.close();
						}
						
						reopenCoverageList(processor.getPolicyTransaction(),
								processor.getCovParent(), 
								processor.getCoverageDto(), 
								processor.getProcessAction());
						break;
					}
				}
				
				return true;
			}			
		};
	}
	
	// Update button
	public boolean canCoverageListUpdate(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			// Total lines
			if (!ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(dto.getCoverageType()) &&
				!ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(dto.getCoverageType())) {
				result = false;
			}
			else if (!BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable()) {
				result = false;
			}
			else {
				if ((!dto.isDetailRequired() && BooleanUtils.isNotTrue(dto.isReinsured())) || dto.isDeletedCoverage() || dto.wasDeletedCoverage()) {
					result = false;
				}
			}
		}
	
		return result;
	}
	
	// os updatable?
	public boolean canCoverageUpdate(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			// Total lines
			if (!ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(dto.getCoverageType()) &&
				!ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(dto.getCoverageType())) {
				result = false;
			}
			else if (!BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable()) {
				result = false;
			}
			else {
				if (!dto.isDetailRequired() || dto.isDeletedCoverage() || dto.wasDeletedCoverage()) {
					result = false;
				}
			}
		}
	
		return result;
	}
	
	public boolean canCoverageView(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			// Total lines
			if (!ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(dto.getCoverageType()) &&
				!ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(dto.getCoverageType())) {
				result = false;
			}
			else {
				if (!dto.isDetailRequired() && BooleanUtils.isNotTrue(dto.isReinsured())) {
					result = false;
				}
			}
		}
		
		return result;
	}
	
	public boolean canCoverageRemove(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			// Total lines
			if (!ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(dto.getCoverageType()) &&
				!ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(dto.getCoverageType())) {
				result = false;
			}
			else if (!BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable()) {
				result = false;
			}
			else {
				ICoverageConfig covConfig = covConfigParent.getCoverageConfig(dto.getCoverageType(), dto.getCoverageCode());
				result = !(covConfig != null && covConfig.isMandatory());
			}
		}
		
		return result;
	}
	
	public boolean canCoverageDelete(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			result = canCoverageRemove(covConfigParent, dto) && !dto.isDeletedCoverage() && !dto.wasDeletedCoverage();
		}
		
		return result;
	}
	
	public boolean canCoverageUndelete(ICoverageConfigParent covConfigParent, CoverageDto dto) {
		boolean result = dto != null;
		
		if (dto != null) {
			result = canCoverageRemove(covConfigParent, dto) && dto.isDeletedCoverage() && !dto.wasDeletedCoverage();
		}
		
		return result;
	}
	
	// For deleting coverage which is created from previous transaction 
	public boolean deleteCoverage(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto dto) {
		if (!dto.isNewCoverage() && !dto.isDeletedCoverage() && !dto.wasDeletedCoverage()) {
			try {
				createCoverageActionProcessor(UIConstants.UIACTION_Delete, covConfigParent, covParent, dto)
								.detachCoverage(covParent, dto);
			} catch (InsurancePolicyException e) {
				uiHelper.notificationException(e);
				return false;
			}
		}
		
		return true;
	}
	
	// For deleting coverage which is created from previous transaction 
	public boolean undeleteCoverage(ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto dto) {
		if (!dto.isNewCoverage() && dto.isDeletedCoverage() && !dto.wasDeletedCoverage()) {
			try {
				createCoverageActionProcessor(UIConstants.UIACTION_Undelete, covConfigParent, covParent, dto)
								.undeleteCoverage(covParent, dto);
			} catch (InsurancePolicyException e) {
				uiHelper.notificationException(e);
				return false;
			}
		}
		
		return true;
	}
	
	public void showWarnAndErrorOfCoverageSelction(IBusinessEntity covParent, boolean isSelected, List<CoverageConfigDto> selectionList) {
		List<String> warns = getWarnAndErrorOfCoverageSelction(covParent, isSelected, selectionList);
		if (!warns.isEmpty()) {
			uiHelper.notificationWarnings(warns);
		}
	}
	
	protected List<String> getWarnAndErrorOfCoverageSelction(IBusinessEntity covParent, boolean isSelected, List<CoverageConfigDto> selectionList) {
		return new ArrayList<String>();
	}
	
	public Double calculateUnitPremium(CoverageDto coverageDto) {
		Double unitPremium = null;
		
		if (coverageDto != null) {
			if (coverageDto.getCoverageNumOfUnits() != null && coverageDto.getCoverageNumOfUnits() != 0 &&
				coverageDto.getWrittenPremium() 	!= null) {
				unitPremium = new BigDecimal(
										coverageDto.getAnnualPremium() / coverageDto.getCoverageNumOfUnits() 
									).setScale(getDecimals(), BigDecimal.ROUND_HALF_UP).doubleValue();
			}
			
		}
	
		return unitPremium;
	}
	
	@SuppressWarnings("serial")
	protected void getRatringWarningsAndSave(DSUpdateDialog<?> dialog,
												ICoverageConfigParent covConfigParent, IBusinessEntity covParent, CoverageDto coverageDto, boolean isMultiple,
												CoverageEntryCallback callback,
												String action) {
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				BaseCoverageProcessor processor = (BaseCoverageProcessor) taskRun.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					List<String> warnMessages = policyUIHelper.buildRatingWarningMessagList(processor.getWarningMessageList());
					if (warnMessages != null && !warnMessages.isEmpty()) {
						uiHelper.notificationWarnings(warnMessages, new DSNotificatonUIHelper.NotificatonCallback() {
							
							@Override
							public void close() {
								continueOnSave();
							}
						});
					}
					else {
						continueOnSave();
					}
				}
				
				return true;
			}
			
			private void continueOnSave() {
				// Save is handled by orocessMultipleNewCoverages
				if (isMultiple) {
					dialog.setProcessStarted();
					dialog.close();
				}
				else {
					runSave(covConfigParent, covParent, coverageDto, dialog, action);
				}
				
				if (callback != null) {
					callback.saveEvent(coverageDto);
				}
				
				showEditMessages(Arrays.asList(coverageDto));
			}
			
		}).start(
				(getCoverageProcessor(UIConstants.UIACTION_RatingWarn)
					.withParameters(covParent, coverageDto))
				);
	}
	
	@SuppressWarnings("serial")
	protected void getRatringWarningsAndSaveGrid(DSUpdateDialog<?> dialog,
												 ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageDto> coverageDtoList) {
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				BaseCoverageProcessor processor = (BaseCoverageProcessor) taskRun.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					List<String> warnMessages = policyUIHelper.buildRatingWarningMessagList(processor.getWarningMessageList());
					if (warnMessages != null && !warnMessages.isEmpty()) {
						uiHelper.notificationWarnings(warnMessages, new DSNotificatonUIHelper.NotificatonCallback() {
							
							@Override
							public void close() {
								continueOnSave();
							}
						});
					}
					else {
						continueOnSave();
					}
				}
				
				return true;
			}
			
			private void continueOnSave() {
				// Save values
				createCoverageTaskProcessor(dialog).start(
						createMultipleCoverageProcessor(UIConstants.UIACTION_Save,
									policyUIHelper.getCurrentPolicyTransaction(), 
									covConfigParent, covParent, coverageDtoList)
						);
			}
			
		}).start(
				(getCoverageProcessor(UIConstants.UIACTION_RatingWarn)
					.withParameters(covParent, coverageDtoList))
				);
	}

	@SuppressWarnings("unchecked")
	protected void onReinsurance(ActionEvent event) {
		DSUpdateDialog<BaseCoverageListEdit> dialog = event.getSource();
		ICoverageConfigParent covConfigParent = (ICoverageConfigParent) dialog.getDataStore().getData1();
		IBusinessEntity covParent 			  = (IBusinessEntity) dialog.getDataStore().getData2();
		CoverageDto coverageDto				  = dialog.getContentComponent().getSelectedCoverageDto();
		
		covReinsuranceUIHelper.onOpenReinsuranceList(covConfigParent, covParent, coverageDto, parentObjectType);
	}

	protected boolean validReinsurances(CoverageDto coverageDto) {
		boolean ok = true;

		Boolean isReinsured = null;
		if (coverageDto.getEditIsReinsuredField() != null) {
			isReinsured = coverageDto.getEditIsReinsuredField().getValue();
		} else {
			isReinsured = coverageDto.isReinsured();
		}

		if (BooleanUtils.isTrue(isReinsured) && coverageDto.getReinsuranceDtos() != null
				&& !coverageDto.getReinsuranceDtos().isEmpty()) {
			ok = covReinsuranceUIHelper.validateReinsurances(coverageDto);
		}

		return ok;
	}
}
