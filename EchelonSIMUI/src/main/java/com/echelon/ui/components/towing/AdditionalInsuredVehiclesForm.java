package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class AdditionalInsuredVehiclesForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3864429283325533214L;
	private TextField 				descriptionField;
	private TextField				serialNoField;
	
	private Binder<AdditionalInsuredVehicles> binder;

	public AdditionalInsuredVehiclesForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForm();
	}
	
	private void buildFields() {
		descriptionField	= new TextField(labels.getString("AddlInsuredVehiclesFormDescriptionLabel"));
		serialNoField		= new TextField(labels.getString("AddlInsuredVehiclesFormSerialNoLabel"));
				
		binder = new Binder<AdditionalInsuredVehicles>(AdditionalInsuredVehicles.class);
		binder.forField(descriptionField).asRequired().bind(AdditionalInsuredVehicles::getDescription, AdditionalInsuredVehicles::setDescription);
		binder.forField(serialNoField).asRequired().bind(AdditionalInsuredVehicles::getSerialNum, AdditionalInsuredVehicles::setSerialNum);
		binder.setReadOnly(true);
	}
		
	private void buildForm() {
		this.removeAll();
		
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		
		form.add(descriptionField);
		form.add(serialNoField);
		
		this.add(form);
	}
	
	public void loadData(AdditionalInsuredVehicles addlInsuredVehicles) {
		loadDataConfigs(binder);
		
		binder.readBean(addlInsuredVehicles);
		
		enableDataView(binder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);

		setFocus();
	}
	
	public boolean saveEdit(AdditionalInsuredVehicles addlInsuredVehicles) {
		boolean ok1 = uiHelper.writeValues(binder, addlInsuredVehicles);
		
		return ok1;
	}
	
	public boolean validateEdit(boolean showMessage) {
		boolean valid = uiHelper.validateValues(showMessage, binder);
		
		return valid;
	}

	public void setFocus() {
		descriptionField.focus();
	}
}
