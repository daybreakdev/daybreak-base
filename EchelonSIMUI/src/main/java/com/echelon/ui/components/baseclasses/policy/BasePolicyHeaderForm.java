package com.echelon.ui.components.baseclasses.policy;

import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.Producer;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.ds.ins.uicommon.converters.DSLookupItemKeyToValueConverter;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.converters.BusTypeAndStatusFormat;
import com.echelon.ui.converters.QuoteNumberFormat;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyHeaderForm extends FormLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4475477347649060367L;
	private static final Logger log = Logger.getLogger(BasePolicyHeaderForm.class.getName());
	
	private ResourceBundle 		laebls;
	private UIHelper			uiHelper;
	private PolicyUIHelper		policyUIHelper;
	private TextField			insuredField			= new TextField();
	private TextField			brokerField				= new TextField();
	private TextField			policyNumberField		= new TextField();
	private TextField			quoteNumberField		= new TextField();
	private TextField			transTypeStatusField	= new TextField();
	private DatePicker			termEffDateField		= new DatePicker();
	private DatePicker			termExpDateField		= new DatePicker();
	private DatePicker			changeEffDateField		= new DatePicker();
	private TextField			provinceField			= new TextField();
	private TextField			extQuoteNumberField		= new TextField();
	
	private Binder<PolicyVersion> binder;
	private Binder<Producer> 	  prdBinder;
	
	public BasePolicyHeaderForm() {
		super();
		// TODO Auto-generated constructor stub
		
		this.uiHelper 		= BrokerUI.getHelperSession().getUiHelper();
		this.policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		buildFields();
		buildForm();
	}
	
	private void buildFields() {
		laebls = uiHelper.getSessionLabels();
		
		insuredField.setLabel(laebls.getString("PolicyHeaderFormInsuredLabel"));
		brokerField.setLabel(laebls.getString("PolicyHeaderFormBrokerLabel"));
		policyNumberField.setLabel(laebls.getString("PolicyHeaderFormPolicyNumberLabel"));
		quoteNumberField.setLabel(laebls.getString("PolicyHeaderFormQuoteNumberLabel"));
		transTypeStatusField.setLabel(laebls.getString("PolicyHeaderFormTransTypeStatusLabel"));
		termEffDateField.setLabel(laebls.getString("PolicyHeaderFormTermEffectiveDateLabel"));
		termExpDateField.setLabel(laebls.getString("PolicyHeaderFormTermExpiryDateLabel"));
		changeEffDateField.setLabel(laebls.getString("PolicyHeaderFormVerEffectiveDateLabel"));
		provinceField.setLabel(laebls.getString("PolicyHeaderFormProvinceLabel"));
		extQuoteNumberField.setLabel(laebls.getString("PolicyHeaderFormExternalQuoteNumberLabel"));
		
		insuredField.setWidthFull();
		brokerField.setWidthFull();
		
		binder = new Binder<PolicyVersion>(PolicyVersion.class);
		// Need to handle multiple customer types.
		binder.forField(insuredField).bind(pv -> {
			final Customer customer = pv.getInsurancePolicy().getPolicyCustomer();
			if (customer.getCommercialCustomer() != null) {
				return customer.getCommercialCustomer().getLegalName();
			} else if (customer.getPersonalCustomer() != null) {
				return customer.getPersonalCustomer().getFullName();
			} else {
				log.log(Level.SEVERE, "Unknown customer type {0}", customer.getCustomerType());
				return null;
			}
		}, null); // Read only
		//binder.forField(quoteNumberField).bind("insurancePolicy.quoteNum");
		binder.forField(policyNumberField).bind("insurancePolicy.basePolicyNum");
		binder.forField(transTypeStatusField).bind(PolicyVersion::getBusinessStatus, null);
		binder.forField(termEffDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("versionTerm.termEffDate");
		binder.forField(termExpDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("versionTerm.termExpDate");
		binder.forField(changeEffDateField).bind("versionDate");
		binder.forField(provinceField).withConverter(new DSLookupItemKeyToValueConverter<String>(String.class).setConfigLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions))
										.bind("insurancePolicy.controllingJurisdiction");
		binder.forField(extQuoteNumberField).bind("insurancePolicy.externalQuoteNum");
		
		binder.setReadOnly(true);
		quoteNumberField.setReadOnly(true);
		
		prdBinder = new Binder<Producer>(Producer.class);
		prdBinder.forField(brokerField).bind("legalName");
		prdBinder.setReadOnly(true);
	}
	
	private void buildForm() {
		this.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 7)
	        );
		this.add(insuredField, 4);
		this.add(brokerField, 3);
		this.add(policyNumberField, extQuoteNumberField, quoteNumberField, transTypeStatusField, termEffDateField, termExpDateField, changeEffDateField);
		
		this.getChildren().filter(o -> o instanceof TextField).collect(Collectors.toList())
							.forEach(o -> ((TextField)o).setReadOnly(true));
		
		this.getChildren().filter(o -> o instanceof DatePicker).collect(Collectors.toList())
							.forEach(o -> ((DatePicker)o).setReadOnly(true));
	}
	
	public void loadData(PolicyVersion policyVersion) {		
		binder.setBean(policyVersion);
		prdBinder.setBean(policyVersion.getInsurancePolicy().getPolicyProducer());
		
		String quoteNum = null;
		if (policyVersion != null && StringUtils.isNotBlank(policyVersion.getInsurancePolicy().getQuoteNum())) {
			quoteNum = QuoteNumberFormat.format(policyVersion.getInsurancePolicy().getQuoteNum(), 
													policyVersion.getPolicyVersionNum());
		}
		uiHelper.setFieldValue(quoteNumberField, quoteNum);
		
		uiHelper.setFieldValue(transTypeStatusField, 
				(new BusTypeAndStatusFormat()).format(policyUIHelper.getCurrentPolicyTransaction()));
	}
}
