package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.stream.Collectors;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.VehicleDriverUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.binder.Binder;

public class VehicleDriverForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7905658604970928794L;
	
	protected Label							nameInformationLabel;
	protected Label							selectNewDriverLabel;
	protected DSComboBox<String>			driverTypeField;
	protected Binder<VehicleDriver> 		binder;
	protected DriverList					driverList;
	protected SpecialtyVehicleRisk 			vehicle;
	protected VehicleDriver 				currDriver;
	protected VehicleDriverUIHelper			vehicleDriverUIHelper;

	public VehicleDriverForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.vehicleDriverUIHelper = new VehicleDriverUIHelper();
		this.setSizeFull();
		
		buildFields();
		buildForm();
	}
	

	protected void buildFields() {
		nameInformationLabel 				= new Label(labels.getString("VehicleDriverFormNameInformationTitle"));
		uiHelper.setLabelHeaderStyle(nameInformationLabel);
		driverTypeField 					= new DSComboBox<String>(labels.getString("VehicleDriverFormDriverTypeLabel"), String.class)
													.withLookupTableId(ConfigConstants.LOOKUPTABLE_AttachedDriverType);
		selectNewDriverLabel				= new Label(labels.getString("VehicleDriverFormSelectNewDriverTitle"));
		uiHelper.setLabelHeaderStyle(selectNewDriverLabel);
		
		binder = new Binder<VehicleDriver>(VehicleDriver.class);
		binder.forField(driverTypeField).asRequired().bind(VehicleDriver::getDriverType, VehicleDriver::setDriverType);
	}
	
	protected void buildForm() {
		FormLayout vehicleDriverForm = buildTypeForm();
		
		this.add(vehicleDriverForm);
		this.add(selectNewDriverLabel);
		
		if (driverList == null) {
			driverList = new DriverList(UIConstants.UIACTION_Select, this.uiContainerType);
			driverList.setSizeFull();
		}
		
		this.add(driverList);
	}
	
	protected FormLayout buildTypeForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );

		form.add(nameInformationLabel, 4);
		form.add(driverTypeField);
		return form;
	}
	
	public void loadData(SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		loadDataConfigs(binder);
		loadDriverList(vehicle, driver);
				
		this.vehicle = vehicle;
		this.currDriver = driver;
			
		binder.readBean(driver);
		
		enableDataView(binder);
	}
	
	private void loadDriverList(SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		List<Driver> drivers = vehicleDriverUIHelper.getAvailableDrivers(vehicle, driver);
		driverList.loadData(drivers.stream().collect(Collectors.toSet()),
								driver != null ? driver.getDriver() : null);
	}
	
	public void enableEdit() {
		if (vehicleDriverUIHelper.isPrincileDriverTypeSelected(vehicle, currDriver)) {
			driverTypeField.excludeDSItems(SpecialtyAutoConstants.ATTACHDRIVER_TYPE_PRINCIPAL);
		}
		
		enableDataEdit(binder);
		setFocus();
	}
	
	public boolean saveEdit(VehicleDriver driver) {
		boolean ok1 = uiHelper.writeValues(binder, driver);
		driver.setDriver(driverList.getSelectedDriver());
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);

		if (valid) {
			if (driverList.getSelectedDriver() == null) {
				uiHelper.showWaringRequiredFields();
				valid = false;
			}
		}
		
		return valid;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		driverTypeField.focus();
	}

}
