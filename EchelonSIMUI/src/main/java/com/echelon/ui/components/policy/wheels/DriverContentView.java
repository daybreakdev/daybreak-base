package com.echelon.ui.components.policy.wheels;

import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.helpers.policy.wheels.DriverUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="DriverContentView", layout = BasePolicyView.class)
public class DriverContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132388023659436469L;
	private 	DriverUIHelper				driverUIHelper;
	protected 	DriverForm					driverForm;
	protected 	DriverConvictionList		convictionList;
	protected 	DriverClaimList				claimList;
	protected 	DriverSuspensionList		suspensionList;

	public DriverContentView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DriverContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}
	
	protected DriverUIHelper getDriverUIHelper() {
		if (driverUIHelper == null) {
			driverUIHelper = new DriverUIHelper();
		}
		
		return driverUIHelper;
	}

	/* Title is not necessary
	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this,
												uiHelper.getSessionLabels().getString("VehicleDetailsTitle"),
												null);
		}
		
		super.buildLayout();
	}
	*/

	@Override
	protected String[] getTabLabels() {
		// TODO Auto-generated method stub
		return new String[] {
		                  labels.getString("DriverTabLabel"),
		                  labels.getString("DriverConvictionsTabLabel"),
		                  labels.getString("DriverClaimTabLabel"),
		                  labels.getString("DriverSuspensionTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {
				getDriverForm(),
				getConvictionForm(),
				getClaimForm(),
				getSuspensionForm()
				};
	}
	
	protected Component getDriverForm() {
		if (driverForm == null) {
			driverForm = getDriverUIHelper().createDriverForm(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return driverForm;
	}
	
	protected Component getConvictionForm() {
		if (convictionList == null) {
			convictionList = new DriverConvictionList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return convictionList;
	}
	
	protected Component getClaimForm() {
		if (claimList == null) {
			claimList = new DriverClaimList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return claimList;
	}
	
	protected Component getSuspensionForm() {
		if (suspensionList == null) {
			suspensionList = new DriverSuspensionList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return suspensionList;
	}

	protected void loadData(Driver driver) {
		if (driverForm != null) {
			getDriverUIHelper().loadView(driverForm, driver);
		}
		if (convictionList != null) {
			if (driver != null) {
				convictionList.loadData(driver, driver.getDriverConvictions());
			}
			else {
				convictionList.loadData(null, null);
			}
		}
		if (claimList != null) {
			if (driver != null) {
				claimList.loadData(driver, driver.getDriverClaims());
			}
			else {
				claimList.loadData(null, null);
			}
		}
		if (suspensionList != null) {
			if (driver != null) {
				suspensionList.loadData(driver, driver.getDriverSuspensions());
			}
			else {
				suspensionList.loadData(null, null);
			}
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		Driver driver = (Driver)nodeItem.getNodeObject();
		loadData(driver);
		refreshPage();
	}
}
