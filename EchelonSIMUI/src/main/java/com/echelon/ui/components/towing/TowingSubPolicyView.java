package com.echelon.ui.components.towing;

import com.ds.ins.utils.Constants;
import com.echelon.ui.components.baseclasses.policy.BaseSubPolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.button.Button;

public class TowingSubPolicyView extends BaseSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8192601968686575726L;

	public TowingSubPolicyView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TowingSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void refreshToolbar(IBusinessEntity subPolicy, boolean allowNew, boolean allowUpdate,
			boolean allowDelete) {
		// TODO Auto-generated method stub
		super.refreshToolbar(subPolicy, 
								(allowNew    && subPolicy == null), 
								(allowUpdate && subPolicy != null), 
								(allowDelete && subPolicy != null));
		
		if (toolbar != null) {
			Button btNew = toolbar.getButton(UIConstants.UIACTION_New);
			if (btNew != null) {
				if (allowNew) {
					// subpolicy is not currently deleted e.g. was deleted
					// try to enable new button
					if (subPolicy != null &&
						BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable() &&
						!btNew.isEnabled() && 
						BrokerUI.getUserSession().getCurrentPolicy() != null &&
						BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null &&
						Constants.VERS_TXN_TYPE_POLICYCHANGE.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType())
					) {
						SubPolicy<?> spPolicy = (SubPolicy<?>) subPolicy;
						// no active/pending subpolicy
						if (!BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getSubPolicies()
								.stream().filter(o -> spPolicy.getSubPolicyTypeCd().equalsIgnoreCase(o.getSubPolicyTypeCd())
														&& (o.isActive() || o.isPending()))
								.findFirst().isPresent()) {
							btNew.setEnabled(true);
							btNew.setVisible(true);
						}
	
					}
				}
				else {
					btNew.setVisible(false);
				}
			}
		}
	}

}
