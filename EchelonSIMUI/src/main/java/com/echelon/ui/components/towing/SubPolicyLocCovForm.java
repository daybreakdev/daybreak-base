package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class SubPolicyLocCovForm extends BaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8645898745867712879L;
	protected boolean						isLocationMandatory;
	protected boolean 						isUpdLocAttributes;
	protected DSComboBox<PolicyLocation>	locationField;
	protected DSNumberField<Integer>		maxNumOfVehiclesField;
	
	protected Binder<LocCoverageDto>		locBinder;
	
	protected IDSSubPolicyLocCovMultiForm 	parentForm;
	protected boolean						isBuildingLocation;

	public SubPolicyLocCovForm(String uiMode, UICONTAINERTYPES uiContainerType,
									UIConstants.OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}
	
	public void initialize(IDSSubPolicyLocCovMultiForm parentForm,
							boolean isLocationMandatory,
							boolean isUpdLocAttributes) {
		this.isLocationMandatory = isLocationMandatory;
		this.isUpdLocAttributes  = isUpdLocAttributes;
		this.parentForm 		 = parentForm;
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();
		
		locationField 			= new DSComboBox<PolicyLocation>(labels.getString("GarageSubPolicyCoverageLocationLabel"), PolicyLocation.class);
		maxNumOfVehiclesField 	= new DSNumberField<Integer>(labels.getString("GarageSubPolicyCoverageMaxNumOfVehiclesLabel"), Integer.class);
	}

	@Override
	protected void buildBinderForCoverage(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.buildBinderForCoverage(coverageDto);
	
		locBinder = new Binder<LocCoverageDto>(LocCoverageDto.class);
		
		if (isLocationMandatory) {
			locBinder.forField(locationField).asRequired().bind(LocCoverageDto::getPolicyLocation, LocCoverageDto::setPolicyLocation);
		}
		else {
			locBinder.forField(locationField).bind(LocCoverageDto::getPolicyLocation, LocCoverageDto::setPolicyLocation);
		}
		
		if (isUpdLocAttributes) {
			locBinder.forField(maxNumOfVehiclesField).withNullRepresentation("")
													.withConverter(maxNumOfVehiclesField.getConverter())
													.bind(LocCoverageDto::getNumVehicles, LocCoverageDto::setNumVehicles);
		}
		else {
			locBinder.forField(maxNumOfVehiclesField).withNullRepresentation("")
													.withConverter(maxNumOfVehiclesField.getConverter())
													.bind(LocCoverageDto::getNumVehicles, null);
		}
		
		locBinder.setReadOnly(true);
	}

	@Override
	protected void buildCoverageTopForm() {
		coverageTopForm.removeAll();
		
		coverageTopForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );
		
		coverageTopForm.add(locationField, 3);
	}
	
	@Override
	protected void buildPremiunmForm(CoverageDto coverageDto) {
		premiumForm.removeAll();
	}

	@Override
	protected void buildAddlFieldGroupsForm(CoverageDto coverageDto) {
		// No additional fields
	}

	public void refreshLocationSelection() {
		loadLocations();
	}
	
	protected void loadLocations(LocCoverageDto coverageDto) {
		loadLocations(coverageDto != null 
						? coverageDto.getPolicyLocation()
						: null);
	}
	
	protected void loadLocations() {
		loadLocations(locationField.getValue());
	}
	
	protected void loadLocations(PolicyLocation policyLocation) {
		isBuildingLocation = true;
		
		List<LookupTableItem> locLookupItems = new ArrayList<LookupTableItem>();
		List<LookupTableItem> availableLocations = parentForm.getAvailableLocations(policyLocation);
		if (availableLocations != null) {
			locLookupItems.addAll(availableLocations);
		}
		
		locationField.setDSItems(locLookupItems);
		
		isBuildingLocation = false;
	}
	
	protected void locationChanged() {
		if (!isBuildingLocation) {
			if (locationField.getValue() != null) {
				maxNumOfVehiclesField.setDSValue(locationField.getValue().getNumVehicles());
			}
			
			LocCoverageDto coverageDto = locBinder.getBean();
			if (coverageDto != null) {
				coverageDto.setPolicyLocation(locationField.getValue());
			}
			
			parentForm.locationChanged();
		}
	}

	public void loadData(LocCoverageDto coverageDto, List<LocCoverageDto> allDtos) {
		// TODO Auto-generated method stub
		loadLocations(coverageDto);
		
		super.loadData(coverageDto);
		
		locBinder.setBean(coverageDto);
		
		enableDataView(locBinder);
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		super.enableEdit(coverageDto);
		
		enableDataEdit(locBinder);
		
		// Update
		if (coverageDto != null && coverageDto.getCoveragePK() != null) {
			uiHelper.setEditFieldsReadonly(locationField);
		}
		else {
			locationField.addValueChangeListener(event -> locationChanged());
		}
		
		if (isUpdLocAttributes) {
			maxNumOfVehiclesField.setEnabled(true);
			
			if (coverageDto.isDeletedCoverage() || coverageDto.wasDeletedCoverage()) {
				uiHelper.setEditFieldsReadonly(maxNumOfVehiclesField);
			}
		}
		else {
			maxNumOfVehiclesField.setEnabled(false);
		}
		
		uiHelper.setEditFieldsReadonly(coverageUWAfjuestmentField);
	}

	@Override
	public boolean saveEdit(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		boolean ok1 = super.saveEdit(coverageDto);
		boolean ok2 = uiHelper.writeValues(locBinder, coverageDto);
		
		return ok1 && ok2;
	}

	@Override
	public boolean validateEdit(boolean showMessage) {
		// TODO Auto-generated method stub
		boolean ok1 = super.validateEdit(showMessage);
		boolean ok2 = uiHelper.validateValues(showMessage, locBinder);
		
		return ok1 && ok2;
	}

	@Override
	public boolean anyExtraFields() {
		return true;
	}
	
	public void setFocus() {
		if (locationField != null && 
			locationField.isEnabled() && 
			!locationField.isReadOnly()) {
			locationField.focus();
		}
		else if (maxNumOfVehiclesField != null && 
				 maxNumOfVehiclesField.isEnabled() && 
				 !maxNumOfVehiclesField.isReadOnly()){ 
			maxNumOfVehiclesField.focus();
		}
	}
}
