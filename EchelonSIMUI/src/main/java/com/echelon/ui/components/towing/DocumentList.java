package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.ds.ins.utils.Constants;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.DocumentUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.StreamResource;

@Route(value="DocumentsContentView", layout = BasePolicyView.class)
public class DocumentList extends BaseListLayout<PolicyDocuments> implements IPolicyViewContent{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7627254960234582198L;
	protected Grid<PolicyDocuments>	documentGrid;
	protected Anchor openAnchor;
	protected DocumentUIHelper documentUIHelper = new DocumentUIHelper();
	
	public DocumentList() {
		super();
		initializeLayouts(labels.getString("DocumentListTitle"));
		buildGrid();
	}
	
	public DocumentList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeLayouts(labels.getString("DocumentListTitle"));
		buildGrid();
	}
	
	public DocumentList(String uiMode, UICONTAINERTYPES uiContainerType, String title) {
		super(uiMode, uiContainerType);
		initializeLayouts(title);
		buildGrid();
	}
	
	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														title,
														UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar(); 
		openAnchor = new Anchor("","");
		openAnchor.setTarget("_blank");
		openAnchor.getElement().setAttribute("download","");
		Button openButton = new Button(uiHelper.getCommonLabels().getString("ButtonOpenLabel"));
		openAnchor.add(openButton);
		this.toolbar.buildButtons(new String[] { UIConstants.UIACTION_Generate});
		this.toolbar.add(openAnchor);
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupGenerateButtonEvent();
	}
	
	protected void buildGrid() {
		documentGrid = new Grid<PolicyDocuments>();
		uiHelper.setGridStyle(documentGrid);
		documentGrid.setSizeFull();
		documentGrid
				.addColumn(new DSLookupItemRenderer<PolicyDocuments>(PolicyDocuments::getDocumentId)
						.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_DocId))
				.setHeader(labels.getString("DocumentListDocumentIdLabel"));
		documentGrid.addColumn(PolicyDocuments::getDescription)
				.setHeader(labels.getString("DocumentListDescriptionLabel"));
		documentGrid
				.addColumn(new DSLookupItemRenderer<PolicyDocuments>(PolicyDocuments::getBusinessStatus)
						.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_BusinessStatus))
				.setHeader(labels.getString("DocumentListStatusLabel"));
		documentGrid.addColumn(PolicyDocuments::getCreatedBy).setHeader(labels.getString("DocumentListCreatedByLabel"));
		documentGrid.addColumn(new LocalDateTimeRenderer<PolicyDocuments>(PolicyDocuments::getCreatedDate,
				UIConstants.SYSDATETIMEFORMAT)).setHeader(labels.getString("DocumentListCreatedDateLabel"));
		documentGrid.addColumn(new LocalDateTimeRenderer<PolicyDocuments>(o -> {
			if ("G".equalsIgnoreCase(o.getBusinessStatus()))
				return o.getUpdatedDate();
			else
				return null;
		}, UIConstants.SYSDATETIMEFORMAT)).setHeader(labels.getString("DocumentListGeneratedDateLabel"));
		documentGrid
				.addColumn(new DSLookupItemRenderer<PolicyDocuments>(PolicyDocuments::getDistributionTarget)
						.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_DistTarget))
				.setHeader(labels.getString("DocumentListDistributionTargetLabel"));
		documentGrid
				.addColumn(new DSLookupItemRenderer<PolicyDocuments>(PolicyDocuments::getStorageType)
						.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_FileType))
				.setHeader(labels.getString("DocumentListStorageTypeLabel"));
		documentGrid.setSelectionMode(SelectionMode.SINGLE);
		documentGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(documentGrid);
		
		this.add(documentGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				documentUIHelper.newDocumentAction();
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!documentGrid.getSelectedItems().isEmpty()) {
					PolicyDocuments document = documentGrid.asSingleSelect().getValue();
					documentUIHelper.updateDocumentAction(document);
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!documentGrid.getSelectedItems().isEmpty()) {
					PolicyDocuments document = documentGrid.asSingleSelect().getValue();
					documentUIHelper.deleteDocumentAction(document);
				}
			});
		}
	}
	
	protected void setupGenerateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Generate) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Generate).addClickListener(event -> {
				if (!documentGrid.getSelectedItems().isEmpty()) {
					PolicyDocuments document = documentGrid.asSingleSelect().getValue();
					documentUIHelper.generateDocumentAction(document);
				}
			});
		}
	}
	
	protected void setupOpenButtonEvent() {
		if (!documentGrid.getSelectedItems().isEmpty()) {
			PolicyDocuments document = documentGrid.asSingleSelect().getValue();
			if (document.getStorageKey() != null && !document.getBusinessStatus().equalsIgnoreCase("P")) {
				StreamResource stream = documentUIHelper.getOpenDocumentStreamResource(document);
				if (stream != null) {
					openAnchor.setHref(documentUIHelper.getOpenDocumentStreamResource(document));
				} else {
					openAnchor.removeHref();
					openAnchor.setEnabled(false);
					if (document.getDocumentId().equalsIgnoreCase("UP") && this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
						this.toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
					}
				}
			} else {
				openAnchor.setEnabled(false);
			}
		}
	}
	
	protected void refreshToolbar() {
		toolbar.refreshButtons(null, documentGrid);
		String businessStatus = documentUIHelper.getBusinessStatus();
		String txnBusinessStatus = documentUIHelper.getTxnBusinessStatus();
		PolicyDocuments document = documentGrid.asSingleSelect().getValue();
		boolean isRecordSelected = document != null ? true : false;
		boolean isTxIssued       = Constants.BUSINESS_STATUS_ISSUED.equalsIgnoreCase(txnBusinessStatus);
		boolean isReinstatement  = Constants.VERSION_STATUS_ISSUEDREINSTATEMENT.equalsIgnoreCase(businessStatus)
				|| Constants.VERSION_STATUS_PENDINGREINSTATEMENT.equalsIgnoreCase(businessStatus);
		boolean isReissue = Constants.VERSION_STATUS_ISSUEDREISSUE.equalsIgnoreCase(businessStatus)
				|| Constants.VERSION_STATUS_PENDINGREISSUE.equalsIgnoreCase(businessStatus);
		boolean isExtension = Constants.VERSION_STATUS_PENDINGEXTENSTION.equalsIgnoreCase(businessStatus);
		boolean isCancellation = Constants.VERSION_STATUS_PENDINGCANCELLATION.equalsIgnoreCase(businessStatus);
		boolean isAdjustment = Constants.VERSION_STATUS_PENDINGADJUSTMENT.equalsIgnoreCase(businessStatus);
		boolean hasFullAccess    = BrokerUI.getUserSession() != null &&
									BrokerUI.getUserSession().getUserProfile() != null &&
									BrokerUI.getUserSession().getUserProfile().isHasFullAccess();
		
		if (toolbar.getButton(UIConstants.UIACTION_Generate) != null) {
			if (hasFullAccess && isRecordSelected) {
				if (isTxIssued) {
					toolbar.getButton(UIConstants.UIACTION_Generate).setEnabled(false);
				} else if (!document.getDocumentId().equalsIgnoreCase("UP")) {
					toolbar.getButton(UIConstants.UIACTION_Generate).setEnabled(true);
				} else {
					toolbar.getButton(UIConstants.UIACTION_Generate).setEnabled(false);
				}
			} else {
				toolbar.getButton(UIConstants.UIACTION_Generate).setEnabled(false);
			}
		}

		openAnchor.setEnabled(isRecordSelected);
		setupOpenButtonEvent();

		if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
			toolbar.getButton(UIConstants.UIACTION_View).setVisible(false);
		}

		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			if (hasFullAccess && (isTxIssued || isReinstatement || isReissue || isExtension || isCancellation || isAdjustment)) {
				this.toolbar.getButton(UIConstants.UIACTION_New).setEnabled(true);
			}
		}

		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			if (hasFullAccess && (document != null
					&& ((document.getDocumentId().equalsIgnoreCase("UP")
							&& (isTxIssued || isReinstatement || isReissue || isExtension || isCancellation || isAdjustment))
							|| (document.getDocumentId().equalsIgnoreCase("EP") && isExtension)
							|| (document.getDocumentId().equalsIgnoreCase("CA") && isCancellation)
							|| (document.getDocumentId().equalsIgnoreCase("ADJ") && isAdjustment))
					&& documentUIHelper.isDocumentEditable(document))) {
				this.toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(true);
			} else if (this.toolbar.getButton(UIConstants.UIACTION_Update).isEnabled()
					&& !documentUIHelper.isDocumentEditable(document)) {
				this.toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
			}
		}

		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			if (hasFullAccess && (document != null
					&& ((document.getDocumentId().equalsIgnoreCase("UP")
							&& (isTxIssued || isReinstatement || isReissue || isExtension || isCancellation || isAdjustment))
							|| (document.getDocumentId().equalsIgnoreCase("EP") && isExtension)
							|| (document.getDocumentId().equalsIgnoreCase("CA") && isCancellation)
							|| (document.getDocumentId().equalsIgnoreCase("ADJ") && isAdjustment))
					&& documentUIHelper.isDocumentEditable(document))) {
				this.toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(true);
			} else if (this.toolbar.getButton(UIConstants.UIACTION_Delete).isEnabled()
					&& !documentUIHelper.isDocumentEditable(document)) {
				this.toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
			}
		}
	}
	
	public boolean loadData(Set<PolicyDocuments> set) {
		List<PolicyDocuments> list = new ArrayList<PolicyDocuments>();
		if (set != null) {
			list.addAll((Collection<? extends PolicyDocuments>) set);
		}
		return loadData(list);
	}

	public boolean loadData(List<PolicyDocuments> list) {
		if (list == null) {
			list = new ArrayList<PolicyDocuments>();
		}
		documentGrid.setItems(list);
		refreshToolbar();
		return true;
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		documentUIHelper.loadView(this, nodeItem);
	}

}
