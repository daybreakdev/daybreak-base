package com.echelon.ui.components.towing;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.components.DSMultiSelectListBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class GarageSubPolicyForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7180329722716965085L;
	private final static String DELIMITER = " and "; 

	private Binder<GarageSubPolicy<?>> binder;
	private TextField				namedInsuredField;
	private IntegerField			numFTEmployeesField;
	private IntegerField			numPTEmployeesField;
	private DSMultiSelectListBox<String> businessDescField;

	public GarageSubPolicyForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildFields();
		buildForm();
	}

	public void loadData(GarageSubPolicy<?> garageSubPolicy) {
		businessDescField.loadDSItems();
		binder.readBean(garageSubPolicy);
		
		enableDataView(binder);
	}

	private void buildFields() {
		namedInsuredField   = new TextField(labels.getString("GarageSubPolicyNamedInsuredLabel"));
		numFTEmployeesField = new IntegerField(labels.getString("GarageSubPolicyNumFTEmployeesLabel"));
		numPTEmployeesField = new IntegerField(labels.getString("GarageSubPolicyNumPTEmployeesLabel"));
		businessDescField = new DSMultiSelectListBox<String>(DELIMITER, getDefaultBusinessDescription())
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_GarageBusinessDescList, false);
		businessDescField.setWidthFull();

		binder = new Binder<GarageSubPolicy<?>>();
		binder.forField(namedInsuredField)
			.bind(GarageSubPolicy::getSubPolicyNamedInsured, GarageSubPolicy::setSubPolicyNamedInsured);
		binder.forField(numFTEmployeesField)
			.bind(GarageSubPolicy::getNumFullTimeEmployees, GarageSubPolicy::setNumFullTimeEmployees);
		binder.forField(numPTEmployeesField)
			.bind(GarageSubPolicy::getNumPartTimeEmployees, GarageSubPolicy::setNumPartTimeEmployees);
		binder.forField(businessDescField)
			.withConverter(businessDescField.getConverter())
			.bind(GarageSubPolicy::getBusinessDescription, GarageSubPolicy::setBusinessDescription);
	}

	private void buildForm() {
		FormLayout form1 = new FormLayout();
		FormLayout form2 = new FormLayout();
		Label label = null;

		form1.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		);
		form2.setResponsiveSteps(
				new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 1)
		);

		label = new Label(labels.getString("GarageSubPolicyBusinessOperationsLabel"));
		setLabelStyle(label);
		form1.add(namedInsuredField);
		uiHelper.addFormEmptyColumn(form1);
		form1.add(numFTEmployeesField, numPTEmployeesField);
		form2.add(businessDescField);
		setFormStyle(form2);

		this.add(form1);
		this.add(label);
		this.add(form2);
	}

	public boolean validate() {
		return uiHelper.validateValues(binder);
	}

	public boolean save(GarageSubPolicy<?> subPolicy) {
		return uiHelper.writeValues(binder, subPolicy);
	}

	public void enableEdit() {
		enableDataEdit(binder);
		if (businessDescField.getValue().isEmpty()) {
			businessDescField.setDSValue(getDefaultBusinessDescription());
		}
		
		setFocus();
	}

	private void setLabelStyle(Label label) {
		label.getElement().getStyle().set("font-size", "15px");
		label.getElement().getStyle().set("margin-top", "10px");
		label.getElement().getStyle().set("margin-bottom", "10px");
		label.getElement().getStyle().set("font-weight", "bold");
	}

	private void setFormStyle(FormLayout form) {
		form.getElement().getStyle().set("overflow-y", "auto");
		form.getElement().getStyle().set("max-height", "450px");
	}

	@Override
	public void setFocus() {
		namedInsuredField.focus();
	}
	
	protected String getDefaultBusinessDescription() {
		switch (uiConfigHelper.getProductCode()) {
		case "TOWING":
			return "Towing Operations";
		case "LHT":
			return "Repair garage: excluding vehicle sales";
		}
		
		return StringUtils.EMPTY;
	}
}
