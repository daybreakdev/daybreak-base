package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.prodconf.baseclasses.CoverageConfigDto;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.constants.UIConstants;

public class BaseCoverageProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1014485104686736128L;
	protected UIConstants.OBJECTTYPES		parentObjectType;
	protected IGenericProductConfig			productConfig;
	protected ICoverageConfigParent 		covConfigParent;
	protected IBusinessEntity				covParent;
	protected CoverageDto					coverageDto;
	protected List<CoverageDto> 			coverageDtoList;
	protected BaseCoverageConfigUIHelper	coverageConfigUIHelper;
	protected List<CoverageConfigDto> 		coverageConfigList;
	protected boolean 						loadPolicyTransactionFull;
	protected List<String>					warningMessageList;
	
	protected BaseCoverageReinsuranceProcessor covReinsuranceProcessor;

	public BaseCoverageProcessor(String processAction, PolicyTransaction<?> policyTransaction,
									BaseCoverageConfigUIHelper coverageConfigUIHelper,
									IGenericProductConfig productConfig,
									UIConstants.OBJECTTYPES parentObjectType) {
		super(processAction, policyTransaction);
		this.coverageConfigUIHelper		= coverageConfigUIHelper;
		this.productConfig				= productConfig;
		this.parentObjectType			= parentObjectType;
		this.covReinsuranceProcessor	= createCovReinsuranceProcessor(parentObjectType);
		this.covReinsuranceProcessor.setProcessorValues(this);
	}
	
	public BaseCoverageReinsuranceProcessor createCovReinsuranceProcessor(UIConstants.OBJECTTYPES objectType) {
		return new BaseCoverageReinsuranceProcessor(processAction, policyTransaction);
	}

	public BaseCoverageProcessor withParameters(ICoverageConfigParent covConfigParent, IBusinessEntity covParent) {
		this.covConfigParent = covConfigParent;
		this.covParent = covParent;
		
		return this;
	}

	public BaseCoverageProcessor withParameters(List<CoverageDto> coverageDtoList, ICoverageConfigParent covConfigParent, IBusinessEntity covParent, boolean loadPolicyTransactionFull) {
		this.coverageDtoList = coverageDtoList;
		this.covConfigParent = covConfigParent;
		this.covParent = covParent;
		this.loadPolicyTransactionFull = loadPolicyTransactionFull;
		
		return this;
	}

	public BaseCoverageProcessor withParameters(ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
									List<CoverageConfigDto> coverageConfigList) {
		this.covConfigParent = covConfigParent;
		this.covParent = covParent;
		this.coverageConfigList = coverageConfigList;
		
		return this;
	}

	public BaseCoverageProcessor withParameters(ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
									List<CoverageDto> coverageDtoList, boolean isSingle) {
		this.covConfigParent = covConfigParent;
		this.covParent = covParent;
		
		if (isSingle) {
			this.coverageDto = coverageDtoList.get(0);
		}
		else {
			this.coverageDtoList = coverageDtoList;
		}
		
		return this;
	}

	public BaseCoverageProcessor withParameters(IBusinessEntity covParent, CoverageDto coverageDto) {
		this.covParent   = covParent;
		this.coverageDto = coverageDto;
		
		return this;
	}

	public BaseCoverageProcessor withParameters(IBusinessEntity covParent, List<CoverageDto> coverageDtoList) {
		this.covParent       = covParent;
		this.coverageDtoList = coverageDtoList;
		
		return this;
	}

	public IBusinessEntity getCovParent() {
		return covParent;
	}

	public CoverageDto getCoverageDto() {
		return coverageDto;
	}

	public List<CoverageDto> getCoverageDtoList() {
		return coverageDtoList;
	}

	public ICoverageConfigParent getCovConfigParent() {
		return covConfigParent;
	}

	public List<String> getWarningMessageList() {
		return warningMessageList;
	}

	protected boolean attachCoverage(CoverageDetails coverageDetails, CoverageDto coverageDto) {
		if (coverageDto.getCoveragePK() == null) {
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				coverageDetails.addCoverage((Coverage) coverageDto.getCoverage());
			}
			else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				coverageDetails.addEndorsement((Endorsement) coverageDto.getCoverage());
			}
		}
		
		return true;
	}

	protected boolean detachCoverage(IBusinessEntity parent, CoverageDetails coverageDetails, CoverageDto coverageDto) throws InsurancePolicyException {
		if (coverageDto.getCoveragePK() != null) {
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				Coverage coverage = coverageDto.getCoveragePK() == null 
									? coverageDetails.getCoverageByCode(coverageDto.getCoverageCode())
									: coverageDetails.getCoverageByPk(coverageDto.getCoveragePK());
				if (coverage != null) {
					if (parent instanceof PolicyTransaction) {
						PolicyTransaction polTransaction = getPolicyTransaction();
						policyProcessing.removePolicyCoverage(coverage, polTransaction);
					}
					else if (parent instanceof Risk) {
						Risk risk = (Risk) parent;
						if (risk.getSubPolicy() != null) {
							policyProcessing.removeSubPolicyRiskCoverage(coverage, policyTransaction, risk.getSubPolicy(), risk);
						}
						else {
							policyProcessing.removeRiskCoverage(coverage, policyTransaction, risk);
						}
					}
					else if (parent instanceof SubPolicy<?>) {
						SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
						policyProcessing.removeSubPolicyCoverage(coverage, policyTransaction, subPolicy);
					}
				}
			}
			else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				Endorsement endorsement = coverageDto.getCoveragePK() == null
											? coverageDetails.getEndorsementByCode(coverageDto.getCoverageCode())
											: coverageDetails.getEndorsementByPk(coverageDto.getCoveragePK());
				if (endorsement != null) {
					if (parent instanceof PolicyTransaction) {
						PolicyTransaction polTransaction = getPolicyTransaction();
						policyProcessing.removePolicyEndorsement(endorsement, polTransaction);
					}
					else if (parent instanceof Risk) {
						Risk risk = (Risk) parent;
						if (risk.getSubPolicy() != null) {
							policyProcessing.removeSubPolicyRiskEndorsement(endorsement, policyTransaction, risk.getSubPolicy(), risk);
						}
						else {
							policyProcessing.removeRiskEndorsement(endorsement, policyTransaction, risk);
						}
					}
					else if (parent instanceof SubPolicy<?>) {
						SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
						policyProcessing.removeSubPolicyEndorsement(endorsement, policyTransaction, subPolicy);
					}
				}
			}
		}
		
		return true;
	}

	protected boolean undeleteCoverage(IBusinessEntity parent, CoverageDetails coverageDetails, CoverageDto coverageDto) throws InsurancePolicyException {
		if (coverageDto.getCoveragePK() != null) {
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				Coverage coverage = coverageDetails.getCoverageByCode(coverageDto.getCoverageCode());
				if (coverage != null) {
					if (parent instanceof PolicyTransaction) {
						PolicyTransaction polTransaction = getPolicyTransaction();
						policyProcessing.undeletePolicyCoverage(coverage, polTransaction);
					}
					else if (parent instanceof Risk) {
						Risk risk = (Risk) parent;
						if (risk.getSubPolicy() != null) {
							policyProcessing.undeleteSubPolicyRiskCoverage(coverage, policyTransaction, risk.getSubPolicy(), risk);
						}
						else {
							policyProcessing.undeleteRiskCoverage(coverage, policyTransaction, risk);
						}
					}
					else if (parent instanceof SubPolicy<?>) {
						SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
						policyProcessing.undeleteSubPolicyCoverage(coverage, policyTransaction, subPolicy);
					}
				}
			}
			else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				Endorsement endorsement = coverageDetails.getEndorsementByCode(coverageDto.getCoverageCode());
				if (endorsement != null) {
					if (parent instanceof PolicyTransaction) {
						PolicyTransaction polTransaction = getPolicyTransaction();
						policyProcessing.undeletePolicyEndorsement(endorsement, polTransaction);
					}
					else if (parent instanceof Risk) {
						Risk risk = (Risk) parent;
						if (risk.getSubPolicy() != null) {
							policyProcessing.undeleteSubPolicyRiskEndorsement(endorsement, policyTransaction, risk.getSubPolicy(), risk);
						}
						else {
							policyProcessing.undeleteRiskEndorsement(endorsement, policyTransaction, risk);
						}
					}
					else if (parent instanceof SubPolicy<?>) {
						SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
						policyProcessing.undeleteSubPolicyEndorsement(endorsement, policyTransaction, subPolicy);
					}
				}
			}
		}
		
		return true;
	}
	
	// Called from edit grid
	public boolean attachCoverage(IBusinessEntity parent, CoverageDto coverageDto) throws InsurancePolicyException {
		if (parent instanceof PolicyTransaction) {
			PolicyTransaction polTransaction = (PolicyTransaction) parent;
			return attachCoverage(polTransaction.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof Risk) {
			Risk risk = (Risk) parent;
			return attachCoverage(risk.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof SubPolicy<?>) {
			SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
			return attachCoverage(subPolicy.getCoverageDetail(), coverageDto);
		}
		
		return false;
	}
	
	// Called from edit grid	
	public boolean detachCoverage(IBusinessEntity parent, CoverageDto coverageDto) throws InsurancePolicyException {
		if (parent instanceof PolicyTransaction) {
			PolicyTransaction polTransaction = (PolicyTransaction) parent;
			return detachCoverage(polTransaction, polTransaction.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof Risk) {
			Risk risk = (Risk) parent;
			return detachCoverage(risk, risk.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof SubPolicy<?>) {
			SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
			return detachCoverage(subPolicy, subPolicy.getCoverageDetail(), coverageDto);
		}
		
		return false;
	}
	
	// Called from edit grid
	public boolean undeleteCoverage(IBusinessEntity parent, CoverageDto coverageDto) throws InsurancePolicyException {
		if (parent instanceof PolicyTransaction) {
			PolicyTransaction polTransaction = (PolicyTransaction) parent;
			return undeleteCoverage(polTransaction, polTransaction.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof Risk) {
			Risk risk = (Risk) parent;
			return undeleteCoverage(risk, risk.getCoverageDetail(), coverageDto);
		}
		else if (parent instanceof SubPolicy<?>) {
			SubPolicy<?> subPolicy = (SubPolicy<?>) parent;
			return undeleteCoverage(subPolicy, subPolicy.getCoverageDetail(), coverageDto);
		}
		
		return false;
	}

	public boolean commitPolicy(PolicyTransaction parent) throws InsurancePolicyException {
		this.policyTransaction = parent;
		boolean ok = updatePolicyTransaction() && ratePolicyTransaction(null, null);
		return ok;
	}

	public boolean commitRisk(Risk parent) throws InsurancePolicyException {
		boolean ok = updatePolicyTransaction() && ratePolicyTransaction(null, parent);
		return ok;
	}

	public boolean commitSubPolicy(SubPolicy<?> parent) throws InsurancePolicyException {
		boolean ok = updatePolicyTransaction() && ratePolicyTransaction(parent, null);
		return ok;
	}
	
	protected boolean commit(Object parent) throws InsurancePolicyException {
		if (parent instanceof PolicyTransaction) {
			return commitPolicy(policyTransaction);
		}
		else if (parent instanceof Risk) {
			return commitRisk((Risk) parent);
		}
		else if (parent instanceof SubPolicy<?>) {
			return commitSubPolicy((SubPolicy<?>) parent);
		}
		
		return false;
	}
	
	/* causing issue
	protected IBusinessEntity loadCurrentParent(IBusinessEntity parent) {
		loadPolicyTransaction();
		parent = refreshParent(parent);
		return parent;
	}

	protected IBusinessEntity refreshParent(IBusinessEntity parent) {
		if (parent instanceof PolicyTransaction) {
			parent = getPolicyTransaction();
		}
		else if (parent instanceof Risk) {
			parent = findRisk(((Risk)parent).getPolicyRiskPK());
		}
		else if (parent instanceof SubPolicy<?>) {
			parent = policyTransaction.findSubPolicyByType(((SubPolicy)parent).getSubPolicyTypeCd());
		}
		
		return parent;
	}
	*/
	
	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		boolean save = false;
		
		switch (processAction) {
		case UIConstants.UIACTION_Rate:
			try {
				coverageDtoList = ratePolicyTransaction(policyTransaction, coverageDtoList, covConfigParent, covParent, loadPolicyTransactionFull);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
			} catch (SpecialtyAutoPolicyException e) {
				setProcessException(e);
			}
			break;
			
		case UIConstants.UIACTION_List: // Build list of coverages for grid
			try {
				coverageDtoList = buildFullCoverageList(covParent, covConfigParent);
			} catch (SpecialtyAutoPolicyException e1) {
				setProcessException(e1);
			}
			break;
		
		case UIConstants.UIACTION_Select: // For newly selected coverages
			coverageDtoList = createNewCoverageSelections(coverageConfigList, covParent, policyTransaction);
			break;
			
		case UIConstants.UIACTION_SaveNew: // For save new multiple and grid 
			//not for location coverages
			//covParent = loadCurrentParent(covParent);

			if (coverageDto != null) {
				if (saveNewCov(covParent, coverageDto)) {
					save = true;
				}
			}
			if (coverageDtoList != null && !coverageDtoList.isEmpty()) {
				if (coverageListAction(covParent, coverageDtoList)) {
					save = true;
				}
			}
			
			if (save) {
				commitChanges(covParent);
			}
			
			break;
			
		case UIConstants.UIACTION_Save:
			save = false;
			if (coverageDto != null) {
				if (saveCov(covParent, coverageDto)) {
					save = true;
				}
			}
			if (coverageDtoList != null && !coverageDtoList.isEmpty()) {
				if (coverageListAction(covParent, coverageDtoList)) {
					save = true;
				}
			}
			
			if (save) {
				commitChanges(covParent);
			}
			
			break;
			
		case UIConstants.UIACTION_Delete:
			if (deleteCov(covParent, coverageDto)) {
				commitChanges(covParent);
			}
			
			break;
			
		case UIConstants.UIACTION_Undelete:
			if (undeleteCov(covParent, coverageDto)) {
				commitChanges(covParent);
			}
			
			break;
			
		case UIConstants.UIACTION_RatingWarn:
			if (coverageDtoList != null && !coverageDtoList.isEmpty()) {
				try {
					warningMessageList = getRatingWarning(covParent, coverageDtoList);
				} catch (Exception e) {
					setProcessException(e);
				}
				
			}
			else if (coverageDto != null) {
				try {
					warningMessageList = getRatingWarning(covParent, coverageDto);
				} catch (Exception e) {
					setProcessException(e);
				}
			}
			break;

		default:
			break;
		}
	}
	
	// Called for grid and multiple creation
	protected boolean coverageListAction(IBusinessEntity covParent, List<CoverageDto> coverageDtoList) {
		if (coverageDtoList != null && !coverageDtoList.isEmpty()) {
			for(CoverageDto covDto: coverageDtoList) {
				if (covDto.isEditDeleted()) {
					if (!covDto.isNewCoverage()) {
						continue; // handled by the grid
					}
					
					deleteCov(covParent, covDto);
				}
				
				if (covDto.isEditUndeleted()) {
					this.covReinsuranceProcessor.preSaveCoverage(coverageDto);
					continue; // handled by the grid
				}
				
				// Handle multiple create 
				if (covDto.getCoveragePK() == null) {
					saveNewCov(covParent, covDto);
				}
				else {
					saveCov(covParent, covDto);
				}
			}
		}
		
		return true;
	}
	
	protected boolean deleteCov(IBusinessEntity parent, CoverageDto coverageDto) {
		try {
			if (!detachCoverage(parent, coverageDto)) {
				setProcessFail(true);
				return false;
			}
		} catch (Exception e) {
			setProcessException(e);
			return false;
		}
		
		return true;
	}
	
	protected boolean undeleteCov(IBusinessEntity parent, CoverageDto coverageDto) {
		try {
			if (!undeleteCoverage(parent, coverageDto)) {
				setProcessFail(true);
				return false;
			}
		} catch (Exception e) {
			setProcessException(e);
			return false;
		}

		return true;
	}
	
	protected boolean saveNewCov(IBusinessEntity parent, CoverageDto coverageDto) {
		try {
			if (!attachCoverage(parent, coverageDto)) {
				setProcessFail(true);
				return false;
			}
			
			this.covReinsuranceProcessor.preSaveCoverage(coverageDto);			
		} catch (Exception e) {
			setProcessException(e);
			return false;
		}
		
		return true;
	}
	
	protected boolean saveCov(IBusinessEntity parent, CoverageDto coverageDto) {
		this.covReinsuranceProcessor.preSaveCoverage(coverageDto);
		return true;
	}
	
	protected boolean commitChanges(IBusinessEntity parent) {
		try {
			if (!commit(parent)) {
				setProcessFail(true);
				return false;
			}
		} catch (Exception e1) {
			setProcessException(e1);
			return false;
		}
		
		return true;
	}
	
	protected List<CoverageDto> createNewCoverageSelections(List<CoverageConfigDto> coverageConfigs, IBusinessEntity covParent, PolicyTransaction polTransaction) {
		List<CoverageDto> newCoverages = new ArrayList<CoverageDto>();
		
		if (coverageConfigs != null && !coverageConfigs.isEmpty()) {
			coverageConfigs.stream().forEach(o -> {
				try {
					newCoverages.add(createCoverage(o.getType(), o.getCoverageConfig(), covParent, polTransaction));
				} catch (InsurancePolicyException e) {
					// TODO Auto-generated catch block
					throw new RuntimeException(e);
				}
			});
		}
		
		return newCoverages;
	}
	
	protected CoverageDto createCoverage(String coverageType, ICoverageConfig coverageConfig, IBusinessEntity covParent, PolicyTransaction polTransaction) throws InsurancePolicyException {
		CoverageDto coverageDto = null;
		
		if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageType)) {
			CoverageConfig covConfig = (CoverageConfig) coverageConfig;
			Coverage newCov = null;
			if (covParent != null && covParent instanceof Risk) {
				Risk risk = (Risk) covParent;
				if (risk.getSubPolicy() != null) {
					newCov = policyProcessing.createNewSubPolicyRiskCoverage(covConfig, polTransaction, risk.getSubPolicy(), (Risk)covParent);
				}
				else {
					newCov = policyProcessing.createNewRiskCoverage(covConfig, polTransaction, (Risk)covParent);
				}
			}
			else if (covParent != null && covParent instanceof SubPolicy<?>) {
				newCov = policyProcessing.createNewSubPolicyCoverage(covConfig, polTransaction, (SubPolicy<?>)covParent);
			}
			else {
				newCov = policyProcessing.createNewPolicyCoverage(covConfig, polTransaction);
			}
			coverageDto = createCoverageDto(coverageConfig, newCov, covParent);
		}
		else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageType)) {
			EndorsementConfig endConfig = (EndorsementConfig) coverageConfig;
			Endorsement newCov = null;
			if (covParent != null && covParent instanceof Risk) {
				Risk risk = (Risk) covParent;
				if (risk.getSubPolicy() != null) {
					newCov = policyProcessing.createNewSubPolicyRiskEndorsement(endConfig, polTransaction, risk.getSubPolicy(), risk);
				}
				else {
					newCov = policyProcessing.createNewRiskEndorsement(endConfig, polTransaction, risk);
				}
			}
			else if (covParent != null && covParent instanceof SubPolicy<?>) {
				newCov = policyProcessing.createNewSubPolicyEndorsement(endConfig, polTransaction, (SubPolicy<?>)covParent);
			}
			else {
				newCov = policyProcessing.createNewPolicyEndorsement(endConfig, polTransaction);
			}
			coverageDto = createCoverageDto(coverageConfig, newCov, covParent);
		}
		
		return coverageDto;
	}

	public CoverageDto createCoverageDto(ICoverageConfig covConfig, Coverage coverage, IBusinessEntity covParent) {
		CoverageDto dto = new CoverageDto(covConfig, coverage, covParent);
		initializeCoverageDto(dto);
		return dto;
	}

	public CoverageDto createCoverageDto(ICoverageConfig covConfig, Endorsement coverage, IBusinessEntity covParent) {
		CoverageDto dto = new CoverageDto(covConfig, coverage, covParent);
		initializeCoverageDto(dto);
		return dto;
	}
	
	protected List<CoverageDto> buildFullCoverageList(IBusinessEntity covParent, ICoverageConfigParent covConfigParent) throws SpecialtyAutoPolicyException {
		List<CoverageDto> allCoverages = new ArrayList<CoverageDto>();

		CoverageDetails coverageDetails = null;
		if (covParent != null) {
			if (covParent instanceof PolicyTransaction) {
				coverageDetails = ((PolicyTransaction)covParent).getCoverageDetail();
			}
			else if (covParent instanceof SubPolicy) {
				coverageDetails = ((SubPolicy)covParent).getCoverageDetail();
			}
			else if (covParent instanceof Risk) {
				coverageDetails = ((Risk)covParent).getCoverageDetail();
			}
			else if (covParent instanceof SpecialtyVehicleRiskAdjustment) {
				SpecialtyVehicleRiskAdjustment adjustment = (SpecialtyVehicleRiskAdjustment)covParent;
				adjustment = loadAdjustmentkData(adjustment);
				coverageDetails = adjustment.getCoverageDetail();
			}
		}

		if (coverageDetails != null) {
			if (coverageDetails.getCoverages() != null) {
				coverageDetails.getCoverages().stream().forEach(o -> {
					CoverageConfig covConfig = (CoverageConfig) covConfigParent.getCoverageConfig(ConfigConstants.COVERAGETYPE_COVERAGE, o.getCoverageCode());
					if (covConfig != null) {
						o.setDescription(covConfig.getCoverageDescription());
					}
					
					allCoverages.add(createCoverageDto(covConfig, o, covParent));
				});
			}
			
			if (coverageDetails.getEndorsements() != null) {
				coverageDetails.getEndorsements().stream().forEach(o -> {
					EndorsementConfig covConfig = (EndorsementConfig) covConfigParent.getCoverageConfig(ConfigConstants.COVERAGETYPE_ENDORSEMENT, o.getEndorsementCd());
					if (covConfig != null) {
						o.setDescription(covConfig.getEndorsementDescription());
					}
					
					allCoverages.add(createCoverageDto(covConfig, o, covParent));
				});
			}
		}
		
		return allCoverages;
	}
	
	protected void initializeCoverageDtos(List<CoverageDto> dtos) {
		if (dtos != null) {
			dtos.stream().forEach(o -> initializeCoverageDto(o));
		}
	}
	
	protected void initializeCoverageDto(CoverageDto dto) {
		if (dto != null && dto.getCoverageConfig() != null) {
			dto.setDetailRequired(coverageConfigUIHelper.isDetailRequired(dto.getCoverageConfig()));
			dto.setAllowPremiumOverride(dto.getCoverageConfig() != null && 
										 dto.getCoverageConfig().isAllowPremiumOverride());
			dto.setLimitInteger(coverageConfigUIHelper.useLimitIntegerField(dto) ||
								!coverageConfigUIHelper.isAllowLimitOverride(dto));
			dto.setLimitMandatory(coverageConfigUIHelper.isLimitMandatory(dto));
			dto.setAllowLimitOverride(coverageConfigUIHelper.isAllowLimitOverride(dto));
			dto.setDeductibleInteger(coverageConfigUIHelper.useDeductibleIntegerField(dto) ||
									!coverageConfigUIHelper.isAllowDeductibleOverride(dto));
			dto.setDeductibleMandatory(coverageConfigUIHelper.isDeductibleMandatort(dto));
			dto.setAllowDeductibleOverride(coverageConfigUIHelper.isAllowDeductibleOverride(dto));

			if (coverageConfigUIHelper.isAllowReinsurance(dto)) {
				dto.setAllowReinsurance(true);
				dto.setIsReinsured(true);
			}
		}
	}

	protected List<String> getRatingWarning(IBusinessEntity covParent, CoverageDto coverageDto) throws Exception {
		return null;
	}
	
	protected List<String> getRatingWarning(IBusinessEntity covParent, List<CoverageDto> coverageDtos) throws Exception {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	protected List<CoverageDto> ratePolicyTransaction(PolicyTransaction<?> policyTransaction, 
														List<CoverageDto> coverageDtoList,
														ICoverageConfigParent covConfigParent,
														IBusinessEntity covParent,
														boolean loadPolicyTransactionFull) throws InsurancePolicyException, SpecialtyAutoPolicyException {
		
		if (coverageDtoList != null && !coverageDtoList.isEmpty()) {
			for(CoverageDto dto : coverageDtoList) {
				if (!dto.isEditDeleted()) {
					this.covReinsuranceProcessor.preSaveCoverage(dto);
				}
			}
		}
		
		policyTransaction = getPolicyProcessing().ratePolicyTransaction(policyTransaction, loadPolicyTransactionFull, getUserProfile());
		coverageDtoList = buildFullCoverageList(covParent, covConfigParent);
		return coverageDtoList;
	}
	
	protected boolean ratePolicyTransaction(IBusinessEntity parent) {
		if (parent instanceof PolicyTransaction) {
			return ratePolicyTransaction(null, null);
		}
		else if (parent instanceof Risk) {
			Risk risk = (Risk) parent;
			return ratePolicyTransaction(risk.getSubPolicy(), risk);
		}
		else if (parent instanceof SubPolicy<?>) {
			return ratePolicyTransaction((SubPolicy<?>) parent, null);
		}
		
		return false;
	}
}
