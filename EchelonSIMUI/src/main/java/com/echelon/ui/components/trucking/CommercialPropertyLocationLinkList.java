package com.echelon.ui.components.trucking;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CommercialPropertyLocation;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertyLocationLinkUIHelper;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.provider.ListDataProvider;

public class CommercialPropertyLocationLinkList
	extends BaseListLayout<CommercialPropertyLocation> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8430915293895550203L;

	private CommercialPropertySubPolicy<?> parentCPSubPolicy;
	private Grid<CommercialPropertyLocation> locGrid;
	private CommercialPropertyLocationLinkUIHelper locLinkUIHelper;

	public CommercialPropertyLocationLinkList(String uiMode,
		UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		locLinkUIHelper = new CommercialPropertyLocationLinkUIHelper();

		initializeLayouts();
		buildGrid();
	}

	protected void initializeLayouts() {
		this.setSizeFull();

		if (UICONTAINERTYPES.Inplace.equals(uiContainerType)
			|| UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			String title = labels
				.getString("CommercialPropertyLocationDetailsTitle");
			DSHeaderComponents headComponents = uiLayoutHelper
				.buildHeaderLayout(this, title, UICOMPONENTTYPES.List);
			this.toolbar = headComponents.getToolbar();
			setupNewButtonEvent();
			setupUpdateButtonEvent();
			setupDeleteButtonEvent();
		}
	}

	protected void setupUpdateButtonEvent() {
		if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			toolbar.getButton(UIConstants.UIACTION_Update).setVisible(false);
		}
	}

	protected void setupNewButtonEvent() {
		if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
			toolbar.getButton(UIConstants.UIACTION_New).addClickListener(
				e -> (new CommercialPropertyLocationLinkUIHelper())
					.setupSelectionAction(parentCPSubPolicy,
						getLocationList()));
		}
	}

	protected void setupDeleteButtonEvent() {
		if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			toolbar.getButton(UIConstants.UIACTION_Delete)
				.addClickListener(e -> {
					if (!locGrid.getSelectedItems().isEmpty()) {
						(new CommercialPropertyLocationLinkUIHelper())
							.setupDeleteAction(parentCPSubPolicy,
								locGrid.getSelectedItems().iterator().next());
					}
				});
		}
	}

	protected void buildGrid() {
		locGrid = new Grid<>();
		locGrid.setSizeFull();
		uiHelper.setGridStyle(locGrid);
		addGridColumns();
		locGrid.setSelectionMode(SelectionMode.SINGLE);
		locGrid.addSelectionListener(e -> refreshToolbar());
		setUpGridDbClickEvent(locGrid);

		this.add(locGrid);
	}

	protected void addGridColumns() {
		createIndicatorColumn(locGrid);
		locGrid.addColumn(loc -> loc.getPolicyLocation().getLocationId())
			.setHeader(labels.getString("LocationNumLabel")).setSortable(true);
		locGrid
			.addColumn(loc -> loc.getPolicyLocation().getLocationAddress()
				.getAddressLine1())
			.setHeader(labels.getString("LocationAddressLabel"));
		locGrid
			.addColumn(
				loc -> loc.getPolicyLocation().getLocationAddress().getCity())
			.setHeader(labels.getString("LocationCityLabel"));
		locGrid.addColumn(locLinkUIHelper.getRenderer(uiLookupHelper))
			.setHeader(labels.getString("LocationProvinceLabel"));
		locGrid.addColumn(
			loc -> loc.getPolicyLocation().getLocationAddress().getPostalZip())
			.setHeader(labels.getString("LocationPostalCodeLabel"));
	}

	public boolean loadData(CommercialPropertySubPolicy<?> subPolicy) {
		this.parentCPSubPolicy = subPolicy;

		List<CommercialPropertyLocation> list = new ArrayList<>();
		if (this.parentCPSubPolicy != null) {
			if (subPolicy != null
				&& subPolicy.getCommercialPropertyLocations() != null) {
				list.addAll(subPolicy.getCommercialPropertyLocations());
			}
		}

		list = list.stream()
			.sorted((o1, o2) -> o1.getPolicyLocation().getLocationId()
				.compareTo(o2.getPolicyLocation().getLocationId()))
			.collect(Collectors.toList());

		locGrid.setItems(list);
		refreshToolbar();
		return true;
	}

	@Override
	protected void refreshToolbar() {
		if (parentCPSubPolicy == null) {
			if (toolbar != null) {
				toolbar.disableAllButtons();
			}
		} else {
			if (toolbar != null) {
				toolbar.refreshButtons(parentCPSubPolicy, locGrid);

				Button udButton = toolbar
					.getButton(UIConstants.UIACTION_Undelete);
				Button deButton = toolbar
					.getButton(UIConstants.UIACTION_Delete);

				// The policy location of policy can be soft-deleted
				// System does not support "soft delete" for CP location
				if (udButton != null) {
					udButton.setVisible(false);

					// The core code will disable delete and make undelete
					// available based
					// on the object's policy location
					if (udButton.isEnabled()) {
						if (deButton != null) {
							deButton.setEnabled(true);
							deButton.setVisible(true);
						}
					}
				}
			}
		}
	}

	protected void refreshDeleteButton() {
		// Delete allowed
	}

	protected List<CommercialPropertyLocation> getLocationList() {
		List<CommercialPropertyLocation> list = (List<CommercialPropertyLocation>) ((ListDataProvider<CommercialPropertyLocation>) locGrid
			.getDataProvider()).getItems();
		if (list == null) {
			list = new ArrayList<>();
		}

		return list;
	}
}
