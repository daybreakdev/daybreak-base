package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.shared.Registration;

public class CovPremiumField extends PremiumField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4099825694102473181L;
	
	protected Binder<CoverageDto> 	binder;
	protected CoverageDto			coverageDto;
	protected CovIsPremiumOvrField 	isPremiumOvrField;
	protected Registration			isPremiumOvrValueChangeListener;
	protected CovIsReinsuredField	isReinsuredField;
	protected Registration			isReinsuredValueChangeListener;	

	public CovPremiumField(String label) {
		super(label);
		// TODO Auto-generated constructor stub
	}
	
	public void initializeField(CoverageDto dto, CovIsPremiumOvrField isPremiumOvrField, CovIsReinsuredField isReinsuredField) {
		this.uiHelper    		= BrokerUI.getHelperSession().getUiHelper();
		this.coverageDto 		= dto;
		this.isPremiumOvrField  = isPremiumOvrField;
		this.isReinsuredField   = isReinsuredField;
	}
	
	protected boolean isReinsured() {
		return (isReinsuredField != null && BooleanUtils.isTrue(isReinsuredField.getValue()));
	}
	
	protected boolean isAllowPremiumOverride() {
		return coverageDto.isAllowPremiumOverride() && !isReinsured();
	}
		
	public void bindField(Binder<CoverageDto> binder) {
		this.binder = binder;
		binder.removeBinding(this);
		if ((isAllowPremiumOverride() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) || 
				(coverageDto.getAnnualPremium() != null && 
				 coverageDto.getAnnualPremium().doubleValue() != 0)) {
			binder.forField(this)
								.withNullRepresentation("")
								.withConverter(this.getConverter())
								.bind(CoverageDto::getAnnualPremium, CoverageDto::setAnnualPremium);
		}
		else {
			binder.forField(this)
								.withNullRepresentation("")
								.withConverter(this.getConverter())
								.bind(CoverageDto::getDispAnnualPremium, null);
		}
	}
	
	public void determineReadOnly() {
		if (coverageDto.isDeletedCoverage() || coverageDto.wasDeletedCoverage()) {
			uiHelper.setEditFieldsReadonly(this);
			
			if (this.isPremiumOvrValueChangeListener != null) {
				this.isPremiumOvrValueChangeListener.remove();
				this.isPremiumOvrValueChangeListener = null;
			}
			
			if (this.isReinsuredValueChangeListener != null) {
				this.isReinsuredValueChangeListener.remove();
				this.isReinsuredValueChangeListener = null;
			}
		}
		else if (coverageDto.isAllowPremiumOverride()) {
			uiHelper.setEditFieldsNotReadonly(this);
			
			if (this.isPremiumOvrValueChangeListener == null) {
				this.isPremiumOvrValueChangeListener = isPremiumOvrField.addValueChangeListener(this::coverageIsPremiumOverrideValueChanged);
			}

			if (this.isReinsuredValueChangeListener == null && isReinsuredField != null) {
				this.isReinsuredValueChangeListener = isReinsuredField.addValueChangeListener(this::coverageIsPremiumOverrideValueChanged);
			}
			
			coverageIsPremiumOverrideValueChanged(null);
		}
		else {
			uiHelper.setEditFieldsReadonly(this);
			
			if (this.isPremiumOvrValueChangeListener != null) {
				this.isPremiumOvrValueChangeListener.remove();
				this.isPremiumOvrValueChangeListener = null;
			}

			if (this.isReinsuredValueChangeListener != null) {
				this.isReinsuredValueChangeListener.remove();
				this.isReinsuredValueChangeListener = null;
			}
		}
	}
	
	protected void coverageIsPremiumOverrideValueChanged(ValueChangeEvent<?> event) {
		if (isReinsured()) {
			uiHelper.setFieldValue(this, DSNumericFormat.getPremiumInstance().format(coverageDto.getAnnualPremium()));
			uiHelper.setEditFieldsReadonly(this);
		}
		else if (BooleanUtils.isTrue(isPremiumOvrField.getValue())) {
			uiHelper.setEditFieldsNotReadonly(this);
		}
		else {
			uiHelper.setFieldValue(this, DSNumericFormat.getPremiumInstance().format(coverageDto.getOriginalPremium()));
			uiHelper.setEditFieldsReadonly(this);
		}
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && isAllowPremiumOverride() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) {
			this.setTabIndex(value);
			return true;
		}
		else {
			this.setTabIndex(-1);
			return false;
		}
	}
}
