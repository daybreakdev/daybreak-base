package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.interfaces.ILifecycleStatusConfig;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.ActionEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper.NotificatonCallback;
import com.ds.ins.utils.Configurations;
import com.ds.ins.utils.Constants;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyCancelConfirmForm;
import com.echelon.ui.components.baseclasses.policy.BasePolicyConfirmForm;
import com.echelon.ui.components.baseclasses.policy.IBasePolicyConfirmForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyActionsProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.policy.NewPolicyWizardHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.component.html.Hr;
import com.vaadin.flow.component.html.Label;

public class BasePolicyActionsUIHelper extends ContentUIHelper implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8566796123482788990L;
	private static final String MENUITEM_NEWQUOTE 		= "NEWQUOTE";
	private static final String MENUITEM_NEWRENEWAL		= "NEWRENEWAL";
	private static final String MENUITEM_SEP 	  		= "SEP";

	public BasePolicyActionsUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void newQuoteNewClient() {
		(new NewPolicyWizardHelper()).newQuoteWizard(true);
	}

	public void newRenewal() {
		(new NewPolicyWizardHelper()).newRenewalWizard();
	}

	protected BasePolicyConfirmForm createConfirmForm(String policyAction) {
		String policyVersTxnType = Constants.VERS_TXN_TYPE_NEWBUSINESS;
		String policyVerStatus	 = Constants.VERSION_STATUS_PENDING;
		if (BrokerUI.getUserSession() != null &&
			BrokerUI.getUserSession().getCurrentPolicy() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null) {
			policyVersTxnType = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType();
			policyVerStatus   = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getBusinessStatus();
		}


		return new BasePolicyConfirmForm(UIConstants.UIACTION_Confirm, UICONTAINERTYPES.Dialog, policyAction, policyVersTxnType, policyVerStatus);
	}

	protected void openConfirmDialog(String policyAction, LocalDate effectiveDate) {
		openConfirmDialog(policyAction, effectiveDate, null);
	}

	protected void openConfirmDialog(String policyAction, LocalDate effectiveDate, List<LookupTableItem> renewalTermLengths) {
		PolicyConfirmDto dto = createPolicyConfirmDto(policyAction);
		dto.setChangeEffectiveDate(effectiveDate);

		DSUpdateDialog<BasePolicyConfirmForm> dialog = createConfirmDialog(policyAction);

		final BasePolicyConfirmForm contentComponent = dialog.getContentComponent();
		final PolicyTransaction<?> currentPolicyTransaction = policyUIHelper.getCurrentPolicyTransaction();
		if (renewalTermLengths != null) {
			contentComponent.loadData(currentPolicyTransaction, dto, renewalTermLengths);
		} else {
			contentComponent.loadData(currentPolicyTransaction, dto);
		}

		contentComponent.enableEdit();
		dialog.open();
	}

	protected DSUpdateDialog<BasePolicyConfirmForm> createConfirmDialog(String policyAction) {
		String titleId = "MainViewMenuItem"+policyAction+"Label";
		DSUpdateDialog<BasePolicyConfirmForm> dialog = (DSUpdateDialog<BasePolicyConfirmForm>)
				createDialogWithActions(uiHelper.getSessionLabels().getString(titleId),
									    UIConstants.UIACTION_Confirm,
										createConfirmForm(policyAction),
										UIConstants.UIACTION_Confirm, UIConstants.UIACTION_Cancel);
		dialog.setWidth("600px");
		return dialog;
	}


	protected BasePolicyCancelConfirmForm createConfirmCancelForm(String policyAction) {
		String policyVersTxnType = Constants.VERS_TXN_TYPE_CANCELLATION;
		String policyVerStatus	 = Constants.VERSION_STATUS_PENDING;
		if (BrokerUI.getUserSession() != null &&
			BrokerUI.getUserSession().getCurrentPolicy() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null) {
			policyVersTxnType = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType();
			policyVerStatus   = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getBusinessStatus();
		}


		return UIFactory.getInstance().getPolicyCancelConfirmForm(UIConstants.UIACTION_Confirm, UICONTAINERTYPES.Dialog, policyAction, policyVersTxnType, policyVerStatus);
	}

	protected void openConfirmCancelDialog(String policyAction, LocalDate effectiveDate) {
		PolicyConfirmDto dto = createPolicyConfirmDto(policyAction);
		dto.setChangeEffectiveDate(effectiveDate);

		DSUpdateDialog<BasePolicyCancelConfirmForm> dialog = createConfirmCancelDialog(policyAction);
		dialog.getContentComponent().loadData(policyUIHelper.getCurrentPolicyTransaction(), dto, dialog);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}

	protected DSUpdateDialog<BasePolicyCancelConfirmForm> createConfirmCancelDialog(String policyAction) {
		String titleId = "MainViewMenuItem"+policyAction+"Label";
		DSUpdateDialog<BasePolicyCancelConfirmForm> dialog = (DSUpdateDialog<BasePolicyCancelConfirmForm>)
				createDialogWithActions(uiHelper.getSessionLabels().getString(titleId),
									    UIConstants.UIACTION_Confirm,
									    createConfirmCancelForm(policyAction),
										UIConstants.UIACTION_Confirm, UIConstants.UIACTION_Cancel);
		dialog.setWidth("600px");
		return dialog;
	}

	@Override
	protected void onAction(ActionEvent event) {
		// for openConfirmDialog
		if (UIConstants.UIACTION_Confirm.equals(event.getAction())) {
			DSUpdateDialog<?> dialog = event.getSource();
			IBasePolicyConfirmForm confirmForm = (IBasePolicyConfirmForm)dialog.getContentComponent();
			if (confirmForm.validateEdit()) {
				PolicyConfirmDto confirmDto = confirmForm.getConfirmDto();
				if (confirmDto != null) {
					runPolicyAction(confirmDto, dialog);
				}
			}
		}
	}

	protected void confirmAction(String action, boolean needConfirmToBeRated) {
		List<String> warnMessages = null;
		try {
			warnMessages = getActionWarnMessages(action);
		} catch (Exception e) {
			return;
		} 
		
		if (needConfirmToBeRated) {
			List<String> ratedMessages = null;
			try {
				ratedMessages = getToBeRatedMessages();
			} catch (Exception e) {
				return;
			}
			openPolicyActionConfirmDialog(action, ratedMessages, warnMessages);
		}
		else {
			openPolicyActionConfirmDialog(action, null, warnMessages);
		}
	}
	
	protected List<String> getToBeRatedMessages() throws Exception {
		List<String> results = new ArrayList<String>();
		
		String toBeRatedMessage = uiHelper.getSessionLabels().getString("PolicyConfirmDialogPolicyToBeRatedMessage");
		if (StringUtils.isNotBlank(toBeRatedMessage)) {
			results.add(toBeRatedMessage);
		}
		
		List<String> warns = getActionWarnMessages(ConfigConstants.LIFECYCLEACTION_Rating);
		if (warns != null && !warns.isEmpty()) {
			results.addAll(warns);
		}
		
		return results;
	}
	
	protected List<String> getActionWarnMessages(String action) throws Exception {
		return new ArrayList<String>();
	}
	
	protected void confirmRatingAction(String action) {
		List<String> warnMessages = null;
		try {
			warnMessages = getActionWarnMessages(action);
		} catch (Exception e) {
			return;
		} 
		
		if (warnMessages != null && !warnMessages.isEmpty()) {
			uiHelper.notificationWarnings(warnMessages, new DSNotificatonUIHelper.NotificatonCallback() {

				@Override
				public void close() {
					runPolicyAction(new PolicyConfirmDto(action), null);
				}
			});
		}
		else {
			runPolicyAction(new PolicyConfirmDto(action), null);
		}
	}

	protected void openPolicyActionConfirmDialog(String action, List<String> ratedMessages, List<String> warnMessages) {
		List<String> messages = new ArrayList<String>();
		if ((ratedMessages != null && !ratedMessages.isEmpty()) || (warnMessages != null && !warnMessages.isEmpty())) {
			messages.add("");

			if (ratedMessages != null && !ratedMessages.isEmpty()) {
				messages.addAll(ratedMessages);
			}

			if (warnMessages != null && !warnMessages.isEmpty()) {
				messages.addAll(warnMessages);
			}

			messages.add("");
		}

		String[] confirmMessages = new String[2 + (messages != null ? messages.size() : 0)];
		confirmMessages[0] = uiHelper.getSessionLabels().getString("PolicyConfirmDialog"+action+"Message");
		if (messages != null && !messages.isEmpty()) {
			int msgIdx = 1;
			for(String msg : messages) {
				confirmMessages[msgIdx] = msg;
				msgIdx++;
			}
		}
		confirmMessages[confirmMessages.length-1] = uiHelper.getSessionLabels().getString("PolicyConfirmDialogConfirmMessage");

		DSConfirmDialog dialog = new DSConfirmDialog(
									uiHelper.getSessionLabels().getString("MainViewMenuItem"+action+"Label"),
									uiHelper.getButtonLabel(UIConstants.UIACTION_Confirm),
									this::onActionConfirm,
									uiHelper.getButtonLabel(UIConstants.UIACTION_Cancel),
									confirmMessages);
		PolicyConfirmDto confirmDto = createPolicyConfirmDto(action);
		dialog.setDataStore(new DSDialogDataStore(confirmDto));
		dialog.open();
	}

	protected void onActionConfirm(DSConfirmDialog.ConfirmEvent event) {
		PolicyConfirmDto 	polActionConfirmDto 	= (PolicyConfirmDto) event.getSource().getDataStore().getData1();
		DSConfirmDialog 	polActionConfirmDialog 	= event.getSource();

		runPolicyAction(polActionConfirmDto, polActionConfirmDialog);
	}

	protected BasePolicyActionsProcessor createPolicyActionsProcessor(PolicyConfirmDto confirmDto) {
		return UIFactory.getInstance().getPolicyActionsProcessor(UIConstants.UIACTION_Confirm, policyUIHelper.getCurrentPolicyTransaction())
							.withParameters(confirmDto);
	}

	protected TaskProcessor createActionsTaskProcessor(IDSDialog dialog) {
		return (TaskProcessor) (new TaskProcessor(UI.getCurrent(), dialog)
				{
					@Override
					protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
						BasePolicyActionsProcessor processor = (BasePolicyActionsProcessor)taskRun.getProcessor();
						if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
							if (dialog != null) {
								dialog.close();
							}

							onActionsTaskCompleteSucceed(dialog, processor);

							if (processor.getConfirmDto() != null) {
								reopen(processor.getPolicyTransaction(), processor.getConfirmDto().getPolicyAction());
							}
							else {
								reopen(processor.getPolicyTransaction(), null);
							}
						}
						else if (!VALIDATIONSTATUSES.Confirm.equals(processStatus)) {
							if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(processor.getConfirmDto().getPolicyAction())) {
								if (dialog != null) {
									dialog.close();
								}
							}
						}

						return true;
					}
				})
				.withProgressDuration(UIConstants.PROGRESSDURATIONS.Medium);
	}

	protected void onActionsTaskCompleteSucceed(IDSDialog dialog, BasePolicyActionsProcessor processor) {

	}

	protected void runPolicyAction(PolicyConfirmDto confirmDto, IDSDialog dialog) {
		// if not renewal then run standard action
		if (!runCreatePolicyRenewalAction(confirmDto, dialog)) {
			createActionsTaskProcessor(dialog).start(createPolicyActionsProcessor(confirmDto));
		}
	}

	protected boolean runCreatePolicyRenewalAction(PolicyConfirmDto confirmDto, IDSDialog dialog) {
		if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(confirmDto.getPolicyAction())) {
			if (dialog != null) {
				dialog.close();
			}
	
			DSConfirmDialog confirmDialog = new DSConfirmDialog(
										uiHelper.getSessionLabels().getString("MainViewMenuItem"+ConfigConstants.LIFECYCLEACTION_Renewal+"Label"),
										uiHelper.getButtonLabel(UIConstants.UIACTION_Yes),
										this::onRenewalResetYes,
										uiHelper.getButtonLabel(UIConstants.UIACTION_No),
										this::onRenewalResetNo,
										uiHelper.getSessionLabels().getString("PolicyConfirmDialogRenewalConfirmResetUWAMessage"));
			confirmDialog.setDataStore(new DSDialogDataStore(confirmDto));
			confirmDialog.open();
			return true;
		}
		
		return false;
	}

	protected void onRenewalResetYes(DSConfirmDialog.ConfirmEvent event) {
		PolicyConfirmDto 	confirmDto 	  = (PolicyConfirmDto) event.getSource().getDataStore().getData1();
		DSConfirmDialog 	confirmDialog = event.getSource();

		confirmDialog.close();
		confirmDto.setIsRenewalResetUWA(false);
		preRenewalValidaton(confirmDto);
	}

	protected void onRenewalResetNo(DSConfirmDialog.CancelEvent event) {
		PolicyConfirmDto 	confirmDto 	  = (PolicyConfirmDto) event.getSource().getDataStore().getData1();
		DSConfirmDialog 	confirmDialog = event.getSource();

		confirmDialog.close();
		confirmDto.setIsRenewalResetUWA(true);
		preRenewalValidaton(confirmDto);
	}

	protected void preRenewalValidaton(PolicyConfirmDto confirmDto) {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			try {
				policyUIHelper.getPolicyProcessing().preCreatePolicyRenewal(BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(),
														Boolean.TRUE,
														BrokerUI.getUserSession().getUserProfile());
			} catch (InsurancePolicyException e) {
				NotificatonCallback callback = new DSNotificatonUIHelper.NotificatonCallback() {

					@Override
					public void close() {
						createActionsTaskProcessor(null).start(createPolicyActionsProcessor(confirmDto));
					}
				};
				if (Constants.EXCEPTION_RENEWALCHECKEXPIRY.equalsIgnoreCase(e.getMessage())) {
					uiHelper.notificationWarning(Constants.EXCEPTION_RENEWALCHECKEXPIRY, callback);
					return;
				}
			}
		}

		createActionsTaskProcessor(null).start(createPolicyActionsProcessor(confirmDto));
	}

	protected BasePolicyActionsProcessor createCheckPolicyActionAllowProcessor(HashMap<String, Boolean> actionAllowMaps) {
		BasePolicyActionsProcessor processor = UIFactory.getInstance().getPolicyActionsProcessor(UIConstants.UIACTION_List, policyUIHelper.getCurrentPolicyTransaction())
													.withParameters(actionAllowMaps);
		(new TaskProcessor(UI.getCurrent())).initializeProcessor(processor);
		return processor;
	}

	public void refreshActionMenuItems(MenuItem actionMItem, Label ratingIndicatorLabel) {
		actionMItem.getSubMenu().removeAll();
		ratingIndicatorLabel.setVisible(false);

		if (BrokerUI.getUserSession() != null &&
			BrokerUI.getUserSession().getUserProfile() != null &&
			BrokerUI.getUserSession().getUserProfile().isHasFullAccess()) {
			// New Quote/Policy menu
			MenuItem newQuoteMenuItem = actionMItem.getSubMenu().addItem(uiHelper.getSessionLabels().getString("MainViewMenuItemNewQuoteNewClientLabel"));
			newQuoteMenuItem.setId(MENUITEM_NEWQUOTE);
			newQuoteMenuItem.addClickListener(event -> newQuoteNewClient());

			// Manual Renewal
			MenuItem newRenewalMenuItem = actionMItem.getSubMenu().addItem(uiHelper.getSessionLabels().getString("MainViewMenuItemNewRenewalLabel"));
			newRenewalMenuItem.setId(MENUITEM_NEWRENEWAL);
			newRenewalMenuItem.addClickListener(event -> newRenewal());

			MenuItem sepMenuItem = actionMItem.getSubMenu().addItem(new Hr());
			sepMenuItem.setId(MENUITEM_SEP);
			sepMenuItem.setVisible(false);

			if (BrokerUI.getHelperSession().getPolicyUIHelper().getCurrentPolicyTransaction() != null) {
				boolean anyVisible = false;
				List<String> lifecycleActions = getLifecycleActions();
				List<String> additionalActions = getAdditionalActions();
				filterAllowedActions(lifecycleActions, additionalActions);

				if (lifecycleActions != null && !lifecycleActions.isEmpty()) {
					lifecycleActions.stream().forEach(o -> buildActionMenuItem(actionMItem, o));
					anyVisible = !lifecycleActions.isEmpty();
				}

				sepMenuItem.setVisible(anyVisible);

				if (BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable() &&
					additionalActions != null && !additionalActions.isEmpty()) {
					MenuItem sepMenuItem2 = actionMItem.getSubMenu().addItem(new Hr());
					sepMenuItem2.setId(MENUITEM_SEP);

					additionalActions.stream().forEach(o -> buildActionMenuItem(actionMItem, o));
				} else {
					/*
					 * ESL-2093: enable rate action during pending adjustments
					 * 
					 * Pending adjustment transactions are not editable (creating adjustments has
					 * special handling), but we need to allow the user to manually cause the
					 * transaction to be rated prior to issuing the adjustment transaction.
					 * 
					 * If the current policy version's business status is pending adjustment and
					 * additional actions contains an entry for rating, then we add the rate action
					 * (and a separator).
					 */
					final String businessStatus = policyUIHelper.getCurrentPolicyVersion().getBusinessStatus();
					if (StringUtils.equalsIgnoreCase(businessStatus, Constants.VERSION_STATUS_PENDINGADJUSTMENT)
							&& additionalActions.contains(ConfigConstants.LIFECYCLEACTION_Rating)) {
						final MenuItem sepMenuItem2 = actionMItem.getSubMenu().addItem(new Hr());
						sepMenuItem2.setId(MENUITEM_SEP);

						buildActionMenuItem(actionMItem, ConfigConstants.LIFECYCLEACTION_Rating);
					}
				}
			}

			if (BrokerUI.getUserSession().getCurrentPolicy() != null &&
					BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null &&
					BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getIsReRateReqd() != null &&
					BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getIsReRateReqd()) {
				ratingIndicatorLabel.setVisible(true);
			}
		}

		if (actionMItem.getSubMenu().getItems().isEmpty()) {
			actionMItem.setEnabled(false);
		}
		else {
			actionMItem.setEnabled(true);
		}
	}

	protected void filterAllowedActions(List<String> lifecycleActions, List<String> additionalActions) {
		List<String> allActions = new ArrayList<String>();
		allActions.addAll(lifecycleActions);
		allActions.addAll(additionalActions);

		if (!allActions.isEmpty()) {
			final HashMap<String, Boolean> areActionsAllowMaps = new HashMap<String, Boolean>();
			allActions.stream().forEach(o -> areActionsAllowMaps.put(o, Boolean.FALSE));
			BasePolicyActionsProcessor processor = createCheckPolicyActionAllowProcessor(areActionsAllowMaps);
			processor.checkActionsAllow();
			HashMap<String, Boolean> actionAllowedMaps = processor.getActionAllowMaps();
			if (actionAllowedMaps != null && !actionAllowedMaps.isEmpty()) {
				lifecycleActions.removeIf(o -> BooleanUtils.isNotTrue(actionAllowedMaps.get(o)));
				additionalActions.removeIf(o -> BooleanUtils.isNotTrue(actionAllowedMaps.get(o)));
			}
		}
	}

	protected List<String> getLifecycleActions() {
		List<String> lifecycleActions = new ArrayList<String>();

		ILifecycleStatusConfig lifecycleStatusConfig = BrokerUI.getUserSession().getLifecycleStatusConfig();
		if (lifecycleStatusConfig != null) {
			lifecycleActions = lifecycleStatusConfig.getLifecycleActionEnums();
		}

		return lifecycleActions;
	}

	protected List<String> getAdditionalActions() {
		List<String> actionList = new ArrayList<String>();

		actionList.add(ConfigConstants.LIFECYCLEACTION_Rating);
		actionList.add(DSUIConstants.UIACTION_DoNotRenew);
		actionList.add(DSUIConstants.UIACTION_RmDoNotRenew);

		return actionList;
	}

	protected void reopen(PolicyTransaction policyTransaction, String policyAction) {
		if (policyTransaction != null &&
				policyTransaction.getPolicyMessages() != null && !policyTransaction.getPolicyMessages().isEmpty()) {
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Messages, null);
		}
		else if (ConfigConstants.LIFECYCLEACTION_Rating.equals(policyAction)) {
			policyUIHelper.reopenPolicy(policyTransaction);
		}
		else {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Policy, null);
		}
	}

	public void buildActionMenuItem(MenuItem actionMItem, String action) {
		MenuItem menuItem = actionMItem.getSubMenu().addItem(uiHelper.getSessionLabels().getString("MainViewMenuItem"+action+"Label"));
		menuItem.setId(action);
		if (ConfigConstants.LIFECYCLEACTION_BindQuote.equals(action)) {
			menuItem.addClickListener(event -> bindQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_CopyQuote.equals(action)) {
			menuItem.addClickListener(event -> copyQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineQuote.equals(action)) {
			menuItem.addClickListener(event -> declineQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssuePolicy.equals(action)) {
			menuItem.addClickListener(event -> issuePolicy());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueQuote.equals(action)) {
			menuItem.addClickListener(event -> issueQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_NotRequiredQuote.equals(action)) {
			menuItem.addClickListener(event -> notRequiredQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_WithdrawQuote.equals(action)) {
			menuItem.addClickListener(event -> withdrawQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineQuote.equals(action)) {
			menuItem.addClickListener(event -> undoDeclineQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_UndoNotRequiredQuote.equals(action)) {
			menuItem.addClickListener(event -> undoNotRequiredQuote());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Rating.equals(action)) {
			menuItem.addClickListener(event -> rating());
		}
		else if (ConfigConstants.LIFECYCLEACTION_PolicyChange.equals(action)) {
			menuItem.addClickListener(event -> policyChange());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclinePolicyChange.equals(action)) {
			menuItem.addClickListener(event -> declinePolicyChange());
		}
		else if (ConfigConstants.LIFECYCLEACTION_UndoDeclinePolicyChange.equals(action)) {
			menuItem.addClickListener(event -> undoDeclinePolicyChange());
		}
		else if (ConfigConstants.LIFECYCLEACTION_SpoilPolicyChange.equals(action)) {
			menuItem.addClickListener(event -> spoilPolicyChange());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssuePolicyChange.equals(action)) {
			menuItem.addClickListener(event -> issuePolicyChange());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Reversal.equals(action)) {
			menuItem.addClickListener(event -> issueReversal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(action)) {
			menuItem.addClickListener(event -> renewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_QuoteRenewal.equals(action)) {
			menuItem.addClickListener(event -> quoteRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_BindRenewal.equals(action)) {
			menuItem.addClickListener(event -> bindRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueRenewal.equals(action)) {
			menuItem.addClickListener(event -> issueRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineRenewal.equals(action)) {
			menuItem.addClickListener(event -> declineRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineRenewal.equals(action)) {
			menuItem.addClickListener(event -> undoDeclineRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_SpoilRenewal.equals(action)) {
			menuItem.addClickListener(event -> spoilRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_CopyQuoteRenewal.equals(action)) {
			menuItem.addClickListener(event -> copyQuoteRenewal());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Cancel.equals(action)) {
			menuItem.addClickListener(event -> cancel());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineCancel.equals(action)) {
			menuItem.addClickListener(event -> declineCancel());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueCancel.equals(action)) {
			menuItem.addClickListener(event -> issueCancel());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Reinstate.equals(action)) {
			menuItem.addClickListener(event -> reinstate());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueReinstate.equals(action)) {
			menuItem.addClickListener(event -> issueReinstate());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineReinstate.equals(action)) {
			menuItem.addClickListener(event -> declineReinstate());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Reissue.equals(action)) {
			menuItem.addClickListener(event -> reissue());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueReissue.equals(action)) {
			menuItem.addClickListener(event -> issueReissue());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineReissue.equals(action)) {
			menuItem.addClickListener(event -> declineReissue());
		}
		else if (DSUIConstants.UIACTION_DoNotRenew.equals(action)) {
			menuItem.addClickListener(event -> doNotRenew());
		}
		else if (DSUIConstants.UIACTION_RmDoNotRenew.equals(action)) {
			menuItem.addClickListener(event -> rmDoNotRenew());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Extension.equals(action)) {
			menuItem.addClickListener(event -> extension());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueExtension.equals(action)) {
			menuItem.addClickListener(event -> issueExtension());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineExtension.equals(action)) {
			menuItem.addClickListener(event -> declineExtension());
		}
		else if (ConfigConstants.LIFECYCLEACTION_SpoilExtension.equals(action)) {
			menuItem.addClickListener(event -> spoilExtension());
		}
		else if (ConfigConstants.LIFECYCLEACTION_Adjustment.equals(action)) {
			menuItem.addClickListener(event -> adjustment());
		}
		else if (ConfigConstants.LIFECYCLEACTION_IssueAdjustment.equals(action)) {
			menuItem.addClickListener(event -> issueAdjustment());
		}
		else if (ConfigConstants.LIFECYCLEACTION_DeclineAdjustment.equals(action)) {
			menuItem.addClickListener(event -> declineAdjustment());
		}

		try {
			String disableActionPp = Configurations.getInstance().getProperty("DisableAction"+action);
			if ("TRUE".equalsIgnoreCase(disableActionPp)) {
				menuItem.setEnabled(false);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void rating() {
		confirmRatingAction(ConfigConstants.LIFECYCLEACTION_Rating);
	}

	protected void bindQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_BindQuote, true);
	}

	protected void issuePolicy() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssuePolicy, true);
	}

	protected void issueQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueQuote, true);
	}

	protected void copyQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_CopyQuote, false);
	}

	protected void withdrawQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_WithdrawQuote, false);
	}

	protected void undoDeclineQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_UndoDeclineQuote, false);
	}

	protected void undoNotRequiredQuote() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_UndoNotRequiredQuote, false);
	}

	protected void declineQuote() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineQuote, null);
	}

	protected void notRequiredQuote() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_NotRequiredQuote, null);
	}

	protected void policyChange() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_PolicyChange,
							policyUIHelper.getCurrentPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
	}

	protected PolicyConfirmDto createPolicyConfirmDto(String policyAction) {
		PolicyConfirmDto dto = new PolicyConfirmDto(policyAction);
		return dto;
	}

	protected void declinePolicyChange() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclinePolicyChange, null);
	}

	protected void undoDeclinePolicyChange() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_UndoDeclinePolicyChange, false);
	}

	protected void spoilPolicyChange() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_SpoilPolicyChange, false);
	}

	protected void issuePolicyChange() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssuePolicyChange, true);
	}

	protected void issueReversal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_Reversal, false);
	}

	protected boolean preRenewal() {
		if (policyUIHelper.getCurrentPolicyTransaction() != null &&
			policyUIHelper.getCurrentPolicyTransaction().getPolicyTerm().isDoNotRenew()) {
			uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("ErrorPolicyMakedDoNotRenew"));
			return false;
		}

		return true;
	}

	protected void renewal() {
		if (preRenewal()) {
			// openConfirmDialog(ConfigConstants.LIFECYCLEACTION_Renewal, null);
			final IGenericProductConfig prodConfig = configUIHelper.getCurrentProductConfig();
			final List<LookupTableItem> renewalTermLengths = prodConfig
				.getFieldLookup(ConfigConstants.LOOKUPTABLE_TermLengthsRenewal);
			openConfirmDialog(ConfigConstants.LIFECYCLEACTION_Renewal, null, renewalTermLengths);
		}
	}

	protected void quoteRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_QuoteRenewal, true);
	}

	protected void bindRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_BindRenewal, true);
	}

	protected void issueRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueRenewal, true);
	}

	protected void declineRenewal() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineRenewal, null);
	}

	protected void undoDeclineRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_UndoDeclineRenewal, false);
	}

	protected void spoilRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_SpoilRenewal, false);
	}

	protected void copyQuoteRenewal() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_CopyQuoteRenewal, false);
	}

	protected void cancel() {
		openConfirmCancelDialog(ConfigConstants.LIFECYCLEACTION_Cancel, null);
	}

	protected void declineCancel() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineCancel, null);
	}

	protected void issueCancel() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueCancel, true);
	}

	protected void reinstate() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_Reinstate, false);
	}

	protected void issueReinstate() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueReinstate, false);
	}

	protected void declineReinstate() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineReinstate, null);
	}

	protected void reissue() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_Reissue, false);
	}

	protected void issueReissue() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueReissue, false);
	}

	protected void declineReissue() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineReissue, null);
	}

	protected void doNotRenew() {
		openConfirmDialog(DSUIConstants.UIACTION_DoNotRenew, null);
	}

	protected void rmDoNotRenew() {
		confirmAction(DSUIConstants.UIACTION_RmDoNotRenew, false);
	}

	protected boolean preExtension() {
		if (policyUIHelper.getCurrentPolicyTransaction() != null &&
			policyUIHelper.getCurrentPolicyTransaction().getPolicyTerm().isDoNotRenew()) {
			uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("ErrorPolicyMakedDoNotRenew"));
			return false;
		}

		return true;
	}

	protected void extension() {
		if (preExtension()) {
			openConfirmDialog(ConfigConstants.LIFECYCLEACTION_Extension, null);
		}
	}

	protected void issueExtension() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueExtension, false);
	}

	protected void declineExtension() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineExtension, null);
	}

	protected void spoilExtension() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_SpoilExtension, null);
	}
	
	protected void adjustment() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_Adjustment,
				policyUIHelper.getCurrentPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
	}
	
	protected void issueAdjustment() {
		confirmAction(ConfigConstants.LIFECYCLEACTION_IssueAdjustment, false);
	}
	
	protected void declineAdjustment() {
		openConfirmDialog(ConfigConstants.LIFECYCLEACTION_DeclineAdjustment, null);
	}
}
