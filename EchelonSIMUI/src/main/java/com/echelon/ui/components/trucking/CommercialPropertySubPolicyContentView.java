package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.towing.SubPolicyCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value = "CommercialPropertySubPolicyNodeContentView", layout = BasePolicyView.class)
public class CommercialPropertySubPolicyContentView extends BaseTabLayout
	implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3643981163446150920L;

	private CommercialPropertySubPolicyUIHelper subPolicyUIHelper = new CommercialPropertySubPolicyUIHelper();
	private CommercialPropertySubPolicyView subPolicyView;
	private SubPolicyCoverageList covList;
	private CommercialPropertyLocationLinkList locationList;

	public CommercialPropertySubPolicyContentView() {
		super();
	}

	public CommercialPropertySubPolicyContentView(String uiMode,
		UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected void buildLayout() {
		if (UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this, null, null);
		}

		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
				labels.getString("CommercialPropertySubPolicyDetailsTitle"),
				labels.getString("LocationLinkTabLabel"),
				labels.getString("CoverageTabLabel") };
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] { getSubPolicyView(), getLocationList(),
				getSubPolicyCoverageList() };
	}

	protected Component getSubPolicyView() {
		if (subPolicyView == null) {
			subPolicyView = new CommercialPropertySubPolicyView(uiMode,
				UICONTAINERTYPES.Tab);
		}

		return subPolicyView;
	}

	protected Component getSubPolicyCoverageList() {
		if (covList == null) {
			covList = (SubPolicyCoverageList) CoverageUIFactory.getInstance()
				.createCoverageList(OBJECTTYPES.CPSubPolicy, uiMode,
					UICONTAINERTYPES.Tab);
		}

		return covList;
	}

	protected Component getLocationList() {
		if (locationList == null) {
			locationList = new CommercialPropertyLocationLinkList(uiMode,
				UICONTAINERTYPES.Tab);
		}

		return locationList;
	}

	public void loadData(CommercialPropertySubPolicy<?> subPolicy) {
		if (subPolicyView != null) {
			subPolicyUIHelper.loadView(subPolicyView, subPolicy);
		}
		if (covList != null) {
			((CommercialPropertySubPolicyCoverageUIHelper) CoverageUIFactory
				.getInstance().createCoverageHelper(OBJECTTYPES.CPSubPolicy))
					.loadView(covList, subPolicy);
		}
		if (locationList != null) {
			subPolicyUIHelper.loadView(locationList, subPolicy);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		if (nodeItem == null) {
			return;
		}

		final CommercialPropertySubPolicy<?> subPolicy = (CommercialPropertySubPolicy<?>) nodeItem
			.getNodeObject();

		subPolicyUIHelper.loadSubpolicyData(() -> {
			loadData(subPolicy);
			refreshPage();

			tab.setEnabled(subPolicy != null);
		}, subPolicy);
	}
}
