package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.converters.SIMNumericFormat;
import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyValueUIHelper;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.grid.ColumnTextAlign;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.renderer.ComponentRenderer;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class CargoSubPolicyValueList extends BaseListLayout<CargoDetails> {
	private static final long serialVersionUID = 5458578230297085693L;
	private Grid<CargoDetails>				valueGrid;
	private	CargoSubPolicy<?> 				parentSubPolicy;
	private CargoDetails					lastCargoDetails;
	private	CargoSubPolicyValueUIHelper 	cargoValueHelper = new CargoSubPolicyValueUIHelper();
	
	public CargoSubPolicyValueList(String uiMode, UICONTAINERTYPES uiContainerType) {
		initializeLayouts(labels.getString("CargoSubPolicyValueTitle"));
		buildGrid();
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														title,
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar(); 
		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	// Use last row details
	protected Component setupTotalLine(CargoDetails cargoDetails) {
		if (cargoDetails != null &&
				lastCargoDetails != null && 
				lastCargoDetails.equals(cargoDetails)) {
			HorizontalLayout layout = new HorizontalLayout();
			uiHelper.formatFullLayout(layout);
			layout.getElement().getStyle().set("border-bottom","1px solid var(--lumo-contrast-20pct)");
			return layout;
		}

		return new Div();
	}

	public boolean loadData(CargoSubPolicy<?> subPolicy) {
		this.parentSubPolicy  = subPolicy;
		this.lastCargoDetails = null;
		
		if (subPolicy != null) {
			List<CargoDetails> list = new ArrayList<CargoDetails>();
			list.addAll(subPolicy.getCargoDetails());

			if (list != null && !list.isEmpty()) {
				this.lastCargoDetails = list.get(list.size() - 1);
				valueGrid.setDetailsVisible(this.lastCargoDetails, true);
				CargoDetails totRow = cargoValueHelper.createTotalRow(subPolicy);
				if (totRow != null) {
					list.add(totRow);
				}
			}
			
			this.valueGrid.setItems(list);
		}
		else {
			this.valueGrid.setItems(new ArrayList<CargoDetails>());
		}		
		refreshToolbar();
		
		return true;
	}
	
	protected void buildGrid() {
		valueGrid = new Grid<CargoDetails>();
		uiHelper.setGridStyle(valueGrid);
		valueGrid.setSizeFull();
		
		createIndicatorColumn(valueGrid);
		valueGrid.addColumn(CargoDetails::getCargoDescription).setHeader(labels.getString("CargoSubPolicyValueDescriptionLabel"))
																.setAutoWidth(true);
		valueGrid.addColumn(new NumberRenderer<>(CargoDetails::getAverage, DSNumericFormat.getIntegerInstance()))
								.setHeader(labels.getString("CargoSubPolicyValueAverageLabel"))
								.setTextAlign(ColumnTextAlign.END);
		valueGrid.addColumn(new NumberRenderer<>(CargoDetails::getuWManual, DSNumericFormat.getIntegerInstance()))
								.setHeader(labels.getString("CargoSubPolicyValueUWManualLabel"))
								.setTextAlign(ColumnTextAlign.END);
		valueGrid.addColumn(new NumberRenderer<>(CargoDetails::getCargoValue, DSNumericFormat.getAmountInstance()))
								.setHeader(labels.getString("CargoSubPolicyValueRateLabel"))
								.setTextAlign(ColumnTextAlign.END);
		valueGrid.addColumn(new NumberRenderer<>(CargoDetails::getCargoExposure, DSNumericFormat.getAmountInstance()))
								.setHeader(labels.getString("CargoSubPolicyValueExposureLabel"))
								.setTextAlign(ColumnTextAlign.END);
		valueGrid.addColumn(createPartialPremiumNumberRenderer())
								.setHeader(labels.getString("CargoSubPolicyValuePartialPremiumLabel"))
								.setTextAlign(ColumnTextAlign.END);
		
		valueGrid.setDetailsVisibleOnClick(false);
		valueGrid.setSelectionMode(SelectionMode.SINGLE);
		valueGrid.setDetailsVisibleOnClick(false);
		valueGrid.addSelectionListener(event -> refreshToolbar());
		valueGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		valueGrid.setItemDetailsRenderer(new ComponentRenderer<>(o -> {
			return setupTotalLine(o);
		}));
		setUpGridDbClickEvent(valueGrid);
		
		this.add(valueGrid);
	}
	
	protected NumberRenderer<CargoDetails> createPartialPremiumNumberRenderer() {
		return new NumberRenderer<CargoDetails>(CargoDetails::getCargoPartialPremiums, SIMNumericFormat.getAmountInstance());
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				cargoValueHelper.newCargoSubPolicyValueAction(parentSubPolicy);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!valueGrid.getSelectedItems().isEmpty()) {
					CargoDetails selected = valueGrid.getSelectionModel().getFirstSelectedItem().orElse(null);
					cargoValueHelper.updateCargoSubPolicyAction(parentSubPolicy, selected);
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!valueGrid.getSelectedItems().isEmpty()) {
					CargoDetails selected = valueGrid.getSelectionModel().getFirstSelectedItem().orElse(null);
					cargoValueHelper.deleteCargoSubPolicyAction(parentSubPolicy, selected);
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (!valueGrid.getSelectedItems().isEmpty()) {
					CargoDetails selected = valueGrid.getSelectionModel().getFirstSelectedItem().orElse(null);
					cargoValueHelper.undeleteCargoSubPolicyAction(parentSubPolicy, selected);
				}
			});
		}
	}

	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!valueGrid.getSelectedItems().isEmpty()) {
					CargoDetails selected = valueGrid.getSelectionModel().getFirstSelectedItem().orElse(null);
					cargoValueHelper.viewCargoSubPolicyAction(parentSubPolicy, selected);
				}
			});
		}
	}
	
	protected void refreshToolbar() {
		if (parentSubPolicy == null) {
			toolbar.disableAllButtons();
		}
		else {
			toolbar.refreshButtons(parentSubPolicy, valueGrid);
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null && 
				toolbar.getButton(UIConstants.UIACTION_Delete).isEnabled()) {
				CargoDetails selected = null;
				if (!valueGrid.getSelectedItems().isEmpty()) {
					selected = valueGrid.getSelectedItems().iterator().next();
				}
				if (selected != null) {
					 if (UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(selected.getCargoDescription())) {
						 toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
					 }
					 else if (UIConstants.CARGOVALUE_TOTAL.equalsIgnoreCase(selected.getDescription())) {
						 toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(false);
						 toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
					 }
				}
			}
		}
	}
}
