package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class VehicleCoverageList extends BaseCoverageList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1113333838073057026L;
	
	public VehicleCoverageList(String uiMode, UICONTAINERTYPES uiContainerType, OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildGridDataColumns() {
		super.buildGridDataColumns();

		coverageGrid.getColumnByKey(COLUMNKEY_UNITPREMIUM).setVisible(true);
	}
}
