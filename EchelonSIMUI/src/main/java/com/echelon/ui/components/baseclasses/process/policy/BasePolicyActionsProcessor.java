package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.uicommon.constants.DSUIConstants;
import com.ds.ins.utils.Constants;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;
import com.echelon.utils.SpecialtyAutoConstants;

public class BasePolicyActionsProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5606603842158904300L;
	private static final Logger log	= Logger.getLogger(BasePolicyActionsProcessor.class.getName());
			
	private PolicyConfirmDto 		 confirmDto;
	private HashMap<String, Boolean> actionAllowMaps;

	@SuppressWarnings("rawtypes")
	public BasePolicyActionsProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
	}

	public BasePolicyActionsProcessor withParameters(PolicyConfirmDto confirmDto) {
		this.confirmDto = confirmDto;
		
		return this;
	}

	public BasePolicyActionsProcessor withParameters(HashMap<String, Boolean> actionAllowMaps) {
		this.actionAllowMaps = actionAllowMaps;
		
		return this;
	}
	
	public PolicyConfirmDto getConfirmDto() {
		return confirmDto;
	}

	public HashMap<String, Boolean> getActionAllowMaps() {
		return actionAllowMaps;
	}

	@Override
	public void run() {
		if (confirmDto != null) {
			runProcessing();
		}
		else if (actionAllowMaps != null) {
			checkActionsAllow();
		}
	}
	
	@SuppressWarnings("unchecked")
	protected void runProcessing() {
		String action = confirmDto.getPolicyAction();
		
		if (!ConfigConstants.LIFECYCLEACTION_Rating.equals(action)) {
			try {
				loadPolicyTransaction();
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
		}
		
		if (this.policyTransaction == null) {
			setProcessErrorResourceCode("ErrorPolicyTransactionNotAvailable");
			return;
		}
		
		log.log(Level.INFO, "UI Request "+action+" Begins");

		try {
			if (!preRunActionValidations(action)) {
				this.policyTransaction = loadFullPolicyTransactionData(this.policyTransaction);
				return;
			}
			
			if (ConfigConstants.LIFECYCLEACTION_BindQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.bindQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_CopyQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.copyQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.declineQuote(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssuePolicy.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicy(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.issueQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_NotRequiredQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.notRequireQuote(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_WithdrawQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.withdrawQuotedQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.undoDeclineQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_UndoNotRequiredQuote.equals(action)) {
				this.policyTransaction = this.policyProcessing.undoNotRequireQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Rating.equals(action)) {
				this.policyTransaction = this.policyProcessing.ratePolicyTransaction(this.policyTransaction.getPolicyTransactionPK(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_PolicyChange.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyChange(this.policyTransaction, 
																		confirmDto.getChangeEffectiveDate(),
																		confirmDto.getChangeDescription(),
																		getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclinePolicyChange.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyChange(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_UndoDeclinePolicyChange.equals(action)) {
				this.policyTransaction = this.policyProcessing.undoDeclinePolicyChange(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_SpoilPolicyChange.equals(action)) {
				this.policyTransaction = this.policyProcessing.spoilPolicyChange(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssuePolicyChange.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyChange(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Reversal.equals(action)) {
				this.policyTransaction = this.policyProcessing.reversePolicyChange(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyRenewal(this.policyTransaction, 
																					confirmDto.getChangeDescription(),
																					confirmDto.getIsRenewalResetUWA(),
																					Boolean.FALSE,
																					null,
																					confirmDto.getRenewalTermLength(),
																					getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_QuoteRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.quotePolicyRenewal(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_BindRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.bindPolicyRenewal(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyRenewal(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyRenewal(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.undoDeclinePolicyRenewal(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_SpoilRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.spoilPolicyRenewal(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_CopyQuoteRenewal.equals(action)) {
				this.policyTransaction = this.policyProcessing.copyRenewalQuote(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Cancel.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyCancellation(this.policyTransaction, confirmDto.getChangeEffectiveDate(), confirmDto.getChangeMethod(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineCancel.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyCancellation(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueCancel.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyCancellation(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Reinstate.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyReinstatement(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueReinstate.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyReinstatement(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineReinstate.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyReinstatement(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Reissue.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyReissue(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueReissue.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyReissue(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineReissue.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyReissue(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (DSUIConstants.UIACTION_DoNotRenew.equalsIgnoreCase(action)) {
				this.policyTransaction.getPolicyTerm().setDoNotRenew(true);
				this.policyTransaction.getPolicyTerm().setDoNotRenewReason(confirmDto.getChangeReasonDetails());
				this.policyProcessing.updatePolicyTransaction(this.policyTransaction, getUserProfile());
			}
			else if (DSUIConstants.UIACTION_RmDoNotRenew.equalsIgnoreCase(action)) {
				this.policyTransaction.getPolicyTerm().setDoNotRenew(false);
				this.policyTransaction.getPolicyTerm().setDoNotRenewReason(null);
				this.policyProcessing.updatePolicyTransaction(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Extension.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyExtension(this.policyTransaction, confirmDto.getChangeDescription(), confirmDto.getChangeDays(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueExtension.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyExtension(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineExtension.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyExtension(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_SpoilExtension.equals(action)) {
				this.policyTransaction = this.policyProcessing.spoilPolicyExtension(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_Adjustment.equals(action)) {
				this.policyTransaction = this.policyProcessing.createPolicyAdjustment(this.policyTransaction, 
																		confirmDto.getChangeEffectiveDate(),
																		confirmDto.getChangeDescription(),
																		getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_IssueAdjustment.equals(action)) {
				this.policyTransaction = this.policyProcessing.issuePolicyAdjustment(this.policyTransaction, getUserProfile());
			}
			else if (ConfigConstants.LIFECYCLEACTION_DeclineAdjustment.equals(action)) {
				this.policyTransaction = this.policyProcessing.declinePolicyAdjustment(this.policyTransaction, confirmDto.getChangeReasonCd(), confirmDto.getChangeReasonDetails(), getUserProfile());
			}
		} catch (Exception e) {
			setProcessException(e);
			return;
		} finally {
			log.log(Level.INFO, "UI Request "+action+" End");
		}
		
		// Do not refresh the policy transaction when validation failed
		if (this.policyTransaction != null && (
			this.policyTransaction.getPolicyMessages() == null || 
			this.policyTransaction.getPolicyMessages().isEmpty())) {
			loadPolicyTransaction();
		}
	}
	
	@SuppressWarnings("unchecked")
	public void checkActionsAllow() {
		if (actionAllowMaps != null && !actionAllowMaps.isEmpty()) {
			for(Map.Entry<String, Boolean> entry : actionAllowMaps.entrySet()) {
				String action = entry.getKey();
				
				boolean isAllowed = true;
				
				log.log(Level.INFO, "UI Request pre-"+action+" Begins");
				
				try {
					if (ConfigConstants.LIFECYCLEACTION_BindQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preBindQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_CopyQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preCopyQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preDeclineQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssuePolicy.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preIssuePolicy(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preIssueQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_NotRequiredQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preNotRequireQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_WithdrawQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preWithdrawQuotedQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preUndoDeclineQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_UndoNotRequiredQuote.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preUndoNotRequireQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Rating.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preRatePolicyTransaction(this.policyTransaction.getPolicyTransactionPK());
					}
					else if (ConfigConstants.LIFECYCLEACTION_PolicyChange.equals(action)) {
						this.policyProcessing.preCreatePolicyChange(this.policyTransaction, 
																	this.policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate(),
																	getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclinePolicyChange.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preDeclinePolicyChange(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_UndoDeclinePolicyChange.equals(action)) {
						this.policyTransaction = this.policyProcessing.preUndoDeclinePolicyChange(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_SpoilPolicyChange.equals(action)) {
						this.policyTransaction = this.policyProcessing.preSpoilPolicyChange(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssuePolicyChange.equals(action)) {
						//this.policyTransaction = this.policyProcessing.preIssuePolicyChange(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Reversal.equals(action)) {
						this.policyProcessing.preReversePolicyChange(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(action)) {
						this.policyProcessing.preCreatePolicyRenewal(this.policyTransaction, Boolean.FALSE, getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_QuoteRenewal.equals(action)) {
						this.policyProcessing.preQuotePolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_BindRenewal.equals(action)) {
						this.policyProcessing.preBindPolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueRenewal.equals(action)) {
						this.policyProcessing.preIssuePolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineRenewal.equals(action)) {
						this.policyProcessing.preDeclinePolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_UndoDeclineRenewal.equals(action)) {
						this.policyProcessing.preUndoDeclinePolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_SpoilRenewal.equals(action)) {
						this.policyTransaction = this.policyProcessing.preSpoilPolicyRenewal(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_CopyQuoteRenewal.equals(action)) {
						this.policyTransaction = this.policyProcessing.preCopyRenewalQuote(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Cancel.equals(action)) {
						this.policyTransaction = this.policyProcessing.preCreatePolicyCancellation(this.policyTransaction, LocalDate.now(), getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineCancel.equals(action)) {
						this.policyTransaction = this.policyProcessing.preDeclinePolicyCancellation(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueCancel.equals(action)) {
						this.policyTransaction = this.policyProcessing.preIssuePolicyCancellation(this.policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Reinstate.equals(action)) {
						this.policyTransaction = this.policyProcessing.preCreatePolicyReinstatement(policyTransaction, getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueReinstate.equals(action)) {
						this.policyTransaction = this.policyProcessing.preIssuePolicyReinstatement(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineReinstate.equals(action)) {
						this.policyTransaction = this.policyProcessing.preDeclinePolicyReinstatement(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Reissue.equals(action)) {
						this.policyTransaction = this.policyProcessing.preCreatePolicyReissue(policyTransaction, getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueReissue.equals(action)) {
						this.policyTransaction = this.policyProcessing.preIssuePolicyReissue(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineReissue.equals(action)) {
						this.policyTransaction = this.policyProcessing.preDeclinePolicyReissue(policyTransaction);
					}
					else if (DSUIConstants.UIACTION_DoNotRenew.equalsIgnoreCase(action)) {
						if (this.policyTransaction.getPolicyTerm().isDoNotRenew()) {
							isAllowed = false;
						}
						else if (!(Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(this.policyTransaction.getPolicyVersion().getPolicyTxnType()) &&
								   Constants.BUSINESS_STATUS_PENDING.equalsIgnoreCase(this.policyTransaction.getPolicyVersion().getBusinessStatus()))) {
							this.policyProcessing.preCreatePolicyRenewal(this.policyTransaction, Boolean.FALSE, getUserProfile());
						}
					}
					else if (DSUIConstants.UIACTION_RmDoNotRenew.equalsIgnoreCase(action)) {
						if (!this.policyTransaction.getPolicyTerm().isDoNotRenew()) {
							isAllowed = false;	
						}
						else if (!(Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(this.policyTransaction.getPolicyVersion().getPolicyTxnType()) &&
								   Constants.BUSINESS_STATUS_PENDING.equalsIgnoreCase(this.policyTransaction.getPolicyVersion().getBusinessStatus()))) {
							this.policyProcessing.preCreatePolicyRenewal(this.policyTransaction, Boolean.FALSE, getUserProfile());
						}
					}
					else if (ConfigConstants.LIFECYCLEACTION_Extension.equals(action)) {
						this.policyTransaction = this.policyProcessing.preCreatePolicyExtension(policyTransaction, getUserProfile());
					}
					else if (ConfigConstants.LIFECYCLEACTION_IssueExtension.equals(action)) {
						this.policyTransaction = this.policyProcessing.preIssuePolicyExtension(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_DeclineExtension.equals(action)) {
						this.policyTransaction = this.policyProcessing.preDeclinePolicyExtension(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_SpoilExtension.equals(action)) {
						this.policyTransaction = this.policyProcessing.preSpoilPolicyExtension(policyTransaction);
					}
					else if (ConfigConstants.LIFECYCLEACTION_Adjustment.equals(action)) {
						// ESL-2058 do not allow adjustments for scheduled, mileage, receipts policies
						// Moved to service side
						/*
						final SubPolicy<?> autoSubPolicy = policyTransaction
								.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
						final String fleetBasis = Optional.ofNullable(autoSubPolicy)
								.map(SpecialityAutoSubPolicy.class::cast).map(SpecialityAutoSubPolicy::getFleetBasis)
								.orElse(StringUtils.EMPTY);
						switch (fleetBasis) {
						case "SCHD": // Scheduled
						case "21AM": // 21A-Mileage
						case "21AR": // 21A-Receipts
							isAllowed = false;
							break;
						}
						*/
						this.policyTransaction = this.policyProcessing.preCreatePolicyAdjustment(policyTransaction, LocalDate.now(), getUserProfile());
					}
				} catch (Exception e) {
					isAllowed = false;
				} finally {
					log.log(Level.INFO, "UI Request pre-"+action+" Ends");
				}
				
				actionAllowMaps.put(entry.getKey(), isAllowed);
			}
		}
	}
	
	protected boolean isRequirePreRunActionValidations(String action) {
		return false;
	}
	
	@SuppressWarnings("unchecked")
	protected boolean preRunActionValidations(String action) throws InsurancePolicyException {
		if (isRequirePreRunActionValidations(action)) {
			policyTransaction = loadFullPolicyTransactionData(policyTransaction);
			policyTransaction = policyProcessing.runPolicyRules(policyTransaction, SpecialtyAutoConstants.RULE_GROUP_RATING);
			if (policyTransaction.getPolicyMessages() != null && !policyTransaction.getPolicyMessages().isEmpty()) {
				return false;
			}
		}
		
		return true;
	}
}
