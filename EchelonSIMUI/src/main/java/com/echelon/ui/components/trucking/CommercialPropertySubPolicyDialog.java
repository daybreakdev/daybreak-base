package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.vaadin.flow.component.Unit;

public class CommercialPropertySubPolicyDialog extends DSUpdateDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5421220892363541589L;

	private CommercialPropertySubPolicyForm cpSubPolicyForm;

	public CommercialPropertySubPolicyDialog(String title, String action) {
		super();
		this.cpSubPolicyForm = new CommercialPropertySubPolicyForm(action,
			UICONTAINERTYPES.Dialog);
		initDialog(title, cpSubPolicyForm, action);

		// Adjust dialog width, as there are no additional fields.
		// Without doing this the dialog is excessively wide.
		this.setMaxWidth(550, Unit.PIXELS);
	}

	public void loadData(CommercialPropertySubPolicy<?> subPolicy) {
		cpSubPolicyForm.loadData(subPolicy);
		cpSubPolicyForm.enableEdit();
	}

	public boolean validateEdit() {
		return cpSubPolicyForm.validateEdit();
	}

	public boolean saveEdit(CommercialPropertySubPolicy<?> subPolicy) {
		return cpSubPolicyForm.saveEdit(subPolicy);
	}
}
