package com.echelon.ui.components.towing;

import java.util.Comparator;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.policy.BasePolicyUpdateForm;
import com.echelon.ui.components.baseclasses.policy.PolicyLabilityLimitField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class TowingPolicyUpdateForm extends BasePolicyUpdateForm implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 712385890644425747L;
	private PolicyLabilityLimitField liabilityLimitField;
	
	private Binder<SpecialtyAutoPackage> spBinder;

	public TowingPolicyUpdateForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();
		
		quotePreparedByField.withLookupTableId(ConfigConstants.LOOKUPTABLE_QuotePreparedByList);
		
		liabilityLimitField	= new PolicyLabilityLimitField(labels.getString("PolicyInfoFormLiabilityLimitLabel"));
		
		spBinder = new Binder<SpecialtyAutoPackage>(SpecialtyAutoPackage.class);
		spBinder.forField(liabilityLimitField).asRequired()
												.bind(SpecialtyAutoPackage::getLiabilityLimit, SpecialtyAutoPackage::setLiabilityLimit);
		spBinder.setReadOnly(true);
	}

	@Override
	protected void buildFormLines(FormLayout form) {
		form.add(extQuoteNumField);
		form.add(liabilityLimitField);
		form.add(termEffectiveDateField);
		form.add(quotePreparedByField);
	}

	@Override
	public void loadData(PolicyTransaction policyTransaction) {
		// TODO Auto-generated method stub
		super.loadData(policyTransaction);
		
		loadDataConfigs(spBinder);
		
		if (policyTransaction.getPolicyVersion().getInsurancePolicy() instanceof SpecialtyAutoPackage) {
			uiHelper.readValues(spBinder, policyTransaction.getPolicyVersion().getInsurancePolicy());
		}
		
		enableDataView(spBinder);
	}

	@Override
	public void enabledEdit(PolicyTransaction policyTransaction) {
		// TODO Auto-generated method stub
		super.enabledEdit(policyTransaction);
		
		enableDataEdit(spBinder);
		
		// By default, the items are sorted as Strings (1 < 10 < 2); we need to sort as numbers, so that 1 < 2 < 10
		liabilityLimitField.getDSItems().sort(Comparator.comparingInt(item -> Integer.parseInt(item.toString())));
		
		setFocus();
	}

	@Override
	public boolean validateEdit() {
		// TODO Auto-generated method stub
		boolean ok1 = super.validateEdit();
		boolean ok2 = uiHelper.validateValues(spBinder);
		
		return ok1 && ok2;
	}

	public Integer getLiabilityLimit() {
		return liabilityLimitField.getValue();
	}
	
}
