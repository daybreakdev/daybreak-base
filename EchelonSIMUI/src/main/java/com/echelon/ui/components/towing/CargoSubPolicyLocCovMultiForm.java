package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants;

public class CargoSubPolicyLocCovMultiForm extends SubPolicyLocCovMultiForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1655825922192086877L;

	public CargoSubPolicyLocCovMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, UIConstants.OBJECTTYPES.CGOSubPolicy, false);
	}

	@Override
	protected IBaseCovAndEndorseForm createCoverageForm() {
		// TODO Auto-generated method stub
		return new CargoSubPolicyCoverageForm(uiMode, uiContainerType);
	}

}
