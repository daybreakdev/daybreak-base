package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyUIHelper;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class CargoSubPolicyView extends TowingSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2029418067865835124L;
	private CargoSubPolicyUIHelper	cargoHelper =  (CargoSubPolicyUIHelper) UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
	private CargoSubPolicyForm		cargoSubPolicyForm;

	public CargoSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeSubPolicyView(labels.getString("CargoSubPolicyCargoDetailsTitle"));
		buildComponents();
	}

	private void buildComponents() {
		cargoSubPolicyForm = new CargoSubPolicyForm(this.uiMode, this.uiContainerType);
		this.add(cargoSubPolicyForm);
	}

	public boolean loadData(CargoSubPolicy<?> cargoSubPolicy) {
		boolean selectedSubPolicy = cargoSubPolicy != null;
		cargoSubPolicyForm.loadData(cargoSubPolicy);

		refreshToolbar(cargoSubPolicy, true, true, true);
		return true;
	}

	@Override
	protected void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).addClickListener(e -> {
					cargoHelper.newSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
					cargoHelper.updateSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(e -> {
					cargoHelper.deleteSubPolicyAction();
				});
			}
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(e -> {
					cargoHelper.undeleteSubPolicyAction();
				});
			}
		}
	}

}
