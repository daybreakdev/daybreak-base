package com.echelon.ui.components.baseclasses.process.policy;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.RatingException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.system.UserProfile;
import com.ds.ins.uicommon.processor.DSBaseProcessor;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.utils.SpecialtyAutoConstants;

public class BasePolicyProcessor extends DSBaseProcessor {
	private static final Logger log	= Logger.getLogger(BasePolicyProcessor.class.getName());
			
	protected IGenericProductConfig		policyProductConfig;
	protected IGenericPolicyProcessing 	policyProcessing;
	protected PolicyTransaction			policyTransaction;
	
	public BasePolicyProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction);
		
		this.policyTransaction = policyTransaction;
	}
	
	public void setProcessorValues(UserProfile userProfile,
									IGenericProductConfig policyProductConfig,
									IGenericPolicyProcessing policyProcessing 
									) {
		super.setProcessorValues(userProfile);
		
		this.policyProductConfig = policyProductConfig;
		this.policyProcessing 	 = policyProcessing;
	}
	
	public void setProcessorValues(BasePolicyProcessor sourceProcessor) {
		setProcessorValues(sourceProcessor.getUserProfile(), sourceProcessor.getPolicyProductConfig(), sourceProcessor.getPolicyProcessing());
	}

	public IGenericProductConfig getPolicyProductConfig() {
		return policyProductConfig;
	}

	public IGenericPolicyProcessing getPolicyProcessing() {
		return policyProcessing;
	}

	public PolicyTransaction getPolicyTransaction() {
		return policyTransaction;
	}
	
	protected boolean handledProcessException(Throwable throwable) {
		if (throwable != null && 
			throwable.getCause() != null && 
			throwable.getCause() instanceof RatingException) {
			RatingException ratingException = (RatingException) throwable.getCause();
			if (ratingException.getResourceCode() != null) {
				setProcessErrorResourceCode(ratingException.getResourceCode());
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public void setProcessException(Exception processException) {
		if (!handledProcessException(processException)) {
			super.setProcessException(processException);
		}
	}

	@Override
	public void setProcessThrowable(Throwable processThrowable) {
		if (!handledProcessException(processThrowable)) {
			super.setProcessThrowable(processThrowable);
		}
	}

	protected boolean updatePolicyTransaction() {
		PolicyTransaction polTransaction = updatePolicyTransaction(this.policyTransaction);
		if (polTransaction != null) {
			this.policyTransaction = polTransaction;
			return true;
		}
		
		return false;
	}
	
	protected PolicyTransaction updatePolicyTransaction(PolicyTransaction polTransaction) {
		try {
			polTransaction = policyProcessing.updatePolicyTransaction(polTransaction, getUserProfile());
		} catch (Exception e) {
			setProcessException(e);
			return null;
		}
		
		return polTransaction;
	}
/*
	protected boolean ratePolicyTransaction() {
		try {
			policyTransaction = policyProcessing.rollupPolicy(policyTransaction.getPolicyTransactionPK(), getUserProfile());
		} catch (Exception e) {
			if (e.getCause() != null && e.getCause() instanceof RatingException) {
				RatingException ratingException = (RatingException) e.getCause();
				if (ratingException.getResourceCode() != null) {
					setProcessErrorResourceCode(ratingException.getResourceCode());
					return false;
				}
			}
			setProcessException(e);
			return false;
		}
		
		return true;
	}
*/
	// subPolicy and risk must be saved
	protected boolean ratePolicyTransaction(SubPolicy<?> subPolicy, Risk risk) {
		try {
			policyTransaction = policyProcessing.rollupPolicy(policyTransaction.getPolicyTransactionPK(), subPolicy, risk, true, getUserProfile());
		} catch (Exception e) {
			if (e.getCause() != null && e.getCause() instanceof RatingException) {
				RatingException ratingException = (RatingException) e.getCause();
				if (ratingException.getResourceCode() != null) {
					setProcessErrorResourceCode(ratingException.getResourceCode());
					return false;
				}
			}
			setProcessException(e);
			return false;
		}

		loadPolicyTransaction();
		return true;
	}
	
	protected boolean loadPolicyTransaction() {
		InsurancePolicy policy = policyTransaction.getPolicyVersion().getInsurancePolicy();
		try {
			if (policy.getBasePolicyNum() != null && !policy.getBasePolicyNum().trim().isEmpty()) {
				PolicyTransaction polTrans = policyProcessing.findLatestPolicyTransaction(policy.getBasePolicyNum(),
																	policyTransaction.getPolicyTerm().getTermNumber(),
																	policyTransaction.getPolicyVersion().getPolicyVersionNum());
				if (polTrans != null) {
					polTrans = loadPolicyData(polTrans);
					//polTrans = loadPolicyTransactionData(polTrans);
					policyTransaction = polTrans;
				}
				return true;
			}
			else if (policy.getQuoteNum() != null && !policy.getQuoteNum().trim().isEmpty()) {
				PolicyTransaction polTrans = policyProcessing.findLatestPolicyTransactionByQuoteNumber(policy.getQuoteNum(),
																	policyTransaction.getPolicyTerm().getTermNumber(),
																	policyTransaction.getPolicyVersion().getPolicyVersionNum());

				if (polTrans != null) {
					polTrans = loadPolicyData(polTrans);
					policyTransaction = polTrans;
				}
				return true;
			}
		} catch (Exception e) {
			setProcessException(e);
		}
		
		return false;
	}
	
	protected PolicyTransaction loadPolicyTransaction(String policyNumber, String quoteNumber, Integer termNumber, Integer versionNumber) throws InsurancePolicyException {
		PolicyTransaction polTransaction = null;
		
		if (policyNumber != null && !policyNumber.isEmpty()) {
			log.log(Level.INFO, "UI loadPolicyTransaction - findLatestPolicyTransaction "+policyNumber+" Begins");
			polTransaction = policyProcessing.findLatestPolicyTransaction(policyNumber, termNumber, versionNumber);
			//polTransaction = policyProcessing.loadPolicyTransaction(polTransaction, true);
			log.log(Level.INFO, "UI loadPolicyTransaction - findLatestPolicyTransaction "+policyNumber+" End");
			policyTransaction = polTransaction;
		}
		else if (quoteNumber != null && !quoteNumber.isEmpty()) {
			log.log(Level.INFO, "UI loadPolicyTransaction - findLatestPolicyTransactionByQuoteNumber "+quoteNumber+" Begins");
			polTransaction = policyProcessing.findLatestPolicyTransactionByQuoteNumber(quoteNumber, termNumber, versionNumber);
			log.log(Level.INFO, "UI loadPolicyTransaction - findLatestPolicyTransactionByQuoteNumber "+quoteNumber+" End");
		}
		
		loadPolicyData(polTransaction);
		
		return polTransaction;
	}
	
	protected PolicyTransaction<?> loadPolicyData(PolicyTransaction<?> polTransaction) {
		log.log(Level.INFO, "UI BasePolicyDetailProcessor - loadPolicyData "+polTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum()+" Begins");
		try {
			polTransaction = loadPolicyTransactionData(polTransaction);
		} catch (Exception e) {
		}
		
		polTransaction.getSubPolicies().stream().filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(o.getSubPolicyTypeCd()))
			.forEach(o -> {
			try {
				loadSubPolicyData(o);
			} catch (Exception e) {
			}
		});
		log.log(Level.INFO, "UI BasePolicyDetailProcessor - loadPolicyData "+polTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum()+" End");
		
		return polTransaction;
	}

	protected SubPolicy<?> loadSubPolicy(String subPolicyTypeCd) throws InsurancePolicyException {
		//loadPolicyTransaction();
		SubPolicy<?> sp = policyTransaction.findSubPolicyByType(subPolicyTypeCd);
		if (sp != null) {
			sp = loadSubPolicyData(sp);
		}
		return sp;
	}
	
	protected Risk loadPolicyRisk(Long policyRiskPK) throws InsurancePolicyException {
		//loadPolicyTransaction();
		Risk risk = findRisk(policyRiskPK);
		if (risk != null) {
			risk = loadPolicyRiskData(risk);
		}
		return risk;
	}
	
	protected Risk loadSubPolicyRisk(Long policyRiskPK) throws InsurancePolicyException {
		//loadPolicyTransaction();
		Risk risk = findRisk(policyRiskPK);
		if (risk != null) {
			risk = loadSubPolicyRiskData(risk);
		}
		return risk;
	}
	
	protected Risk findRisk(Long policyRiskPK) {
		if (policyRiskPK != null) {
			// Try Policy level
			Risk risk = ((PolicyTransaction<Risk>)policyTransaction).getPolicyRisks().stream().filter(
								o -> o.getPolicyRiskPK() != null && 
									 o.getPolicyRiskPK().longValue() == policyRiskPK.longValue()
							).findFirst().orElse(null);
			if (risk != null) {
				return risk;
			}
			
			// Try subpolicy level
			for(SubPolicy<Risk> subPolicy : ((PolicyTransaction<Risk>)policyTransaction).getSubPolicies()) {
				if (subPolicy.getSubPolicyRisks() != null && !subPolicy.getSubPolicyRisks().isEmpty()) {
					for(Risk rsk : subPolicy.getSubPolicyRisks()) {
						if (rsk.getPolicyRiskPK() != null &&
							rsk.getPolicyRiskPK().longValue() == policyRiskPK.longValue()) {
							return rsk;
						}
					}
				}
			}
		}
		
		return null;
	}
	
	public boolean needToLoadData(CoverageDetails coverageDetails) {
		boolean toLoad = false;
		try {
			coverageDetails.getCoverages().stream().count();
			coverageDetails.getEndorsements().stream().count();
		} catch (Exception e) {
			toLoad = true;
		}
		
		return toLoad;
	}
	
	protected PolicyTransaction<?> loadFullPolicyTransactionData(PolicyTransaction<?> polTransaction) throws InsurancePolicyException {
		try {
			polTransaction = policyProcessing.loadPolicyTransactionFull(polTransaction, false);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return polTransaction;
	}
	
	protected PolicyTransaction<?> loadPolicyTransactionData(PolicyTransaction<?> polTransaction) throws InsurancePolicyException {
		if (needToLoadData(polTransaction.getCoverageDetail())) {
			polTransaction = policyProcessing.loadPolicyTransaction(polTransaction, true);
		}
		return polTransaction;
	}
	
	public Risk loadPolicyRiskData(Risk risk) throws InsurancePolicyException {
		if (needToLoadData(risk.getCoverageDetail())) {
			risk = policyProcessing.loadPolicyRisk(risk, true);
		}
		return risk;
	}
	
	protected SubPolicy<?> loadSubPolicyData(SubPolicy<?> subPolicy) throws InsurancePolicyException {
		if (needToLoadData(subPolicy.getCoverageDetail())) {
			subPolicy = policyProcessing.loadSubPolicy(subPolicy, true);
		}
		return subPolicy;
	}
	
	protected Risk loadSubPolicyRiskData(Risk risk) throws InsurancePolicyException {
		if (needToLoadData(risk.getCoverageDetail())) {
			risk = policyProcessing.loadSubPolicyRisk(risk, true);
		}
		return risk;
	}
	
	protected SpecialtyVehicleRiskAdjustment loadAdjustmentkData(SpecialtyVehicleRiskAdjustment adjustment) throws SpecialtyAutoPolicyException {
		if (needToLoadData(adjustment.getCoverageDetail())) {
			ISpecialtyAutoPolicyProcessing saPolicyProcessing = (ISpecialtyAutoPolicyProcessing) policyProcessing;
			adjustment = saPolicyProcessing.loadSpecialtyVehicleRiskAdjustment(adjustment);
		}
		return adjustment;
	}
	
	public BasePolicyProcessor withProcessorValues(UserProfile userProfile,
			IGenericProductConfig policyProductConfig,
			IGenericPolicyProcessing policyProcessing 
			) {
		setProcessorValues(userProfile, policyProductConfig, policyProcessing);
		return this;
	}
}