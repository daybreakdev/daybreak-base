package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.constants.UIConstants;

public class LocationDialog extends DSUpdateDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1956498760794986480L;
	private LocationMultiForm	multiForm;

	public LocationDialog(String title, String action, GarageSubPolicy<?> garageSubPolicy) {
		this(title, action, garageSubPolicy, null);
	}

	public LocationDialog(String title, String action, GarageSubPolicy<?> garageSubPolicy, PolicyLocation location) {
		super();
		this.multiForm = new LocationMultiForm(action, garageSubPolicy, location);
		
		if (UIConstants.UIACTION_New.equals(action)) {
			initDialog(title, action, multiForm, UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel);
		}
		else {
			initDialog(title, multiForm, action);
		}
	}

	public boolean validate() {
		return multiForm.validate();
	}

	public boolean save(PolicyLocation location, LocationForm form) {
		return multiForm.save(location, form);
	}

	public boolean update(PolicyLocation location) {
		return multiForm.update(location);
	}
/*
	@Override
	protected DSUIConstants.UIACTIONS[] getButtonActions() {
		boolean isUpdate = action.equals(UIConstants.UIACTION_Update);
		UIACTIONS[] actions = new UIACTIONS[isUpdate ? 2 : 3];
		if (!isUpdate) {
			actions[0] = UIConstants.UIACTION_Add;
		}
		actions[isUpdate ? 0 : 1] = UIConstants.UIACTION_Save;
		actions[isUpdate ? 1 : 2] = UIConstants.UIACTION_Cancel;
		return actions;
	}
*/
	public void addNewForm (boolean autoRemovable) {
		multiForm.addNewForm(autoRemovable);
	}

	public LocationMultiForm getMultiForm() {
		return multiForm;
	}

}
