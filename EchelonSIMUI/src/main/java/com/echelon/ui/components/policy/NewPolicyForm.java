package com.echelon.ui.components.policy;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.baseclasses.policy.PolicyLabilityLimitField;
import com.echelon.ui.components.towing.TowingNewPolicySubpolicyForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.NewPolicyUIHelper;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.DomEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.Binding;

public class NewPolicyForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5367587375096694668L;
	private RadioButtonGroup<String> 	customerTypeField;
	private DSComboBox<String>			productCodeField;
	private DSComboBox<String> 			provinceField;
	private DatePicker					effectiveDateField;
	private DSComboBox<Integer>			termLengthField;
	private Checkbox					hasAutoPolicyField;
	private Checkbox					hasMotorTruckCargoField;
	private Checkbox					hasCommGeneralLiabilityField;
	private Checkbox					hasGarageField;
	private Checkbox					hasCommercialPropertyField;
	private PolicyLabilityLimitField	liabilityLimitField;
	private DSComboBox<String>			quotePreparedByField;
	private INewPolicySubpolicyForm		subpolicySection;
	private FormLayout 					mainForm;		
	
	// Renewal
	private TextField					policyNumberField;
	private TextField					quoteNumberField;
	private DSNumberField<Integer>  	policyTermField;
	
	private Binder<NewPolicyDto>		binder;
	private Binding<?, ?> 				hasAutomobileSubPolicyBinding;
	
	private NewPolicyDto 				newPolicyDto;
	@SuppressWarnings("rawtypes")
	private NewPolicyUIHelper 			newPolicyUIHelper;
	
	private TextField					extQuoteNumField;
		
    @SuppressWarnings("serial")
	@DomEvent("change")
    public static class ProductChangeEvent extends ComponentEvent<DSComboBox<String>> {
        public ProductChangeEvent(DSComboBox<String> source, boolean fromClient) {
            super(source, fromClient);
        }
    }

    @SuppressWarnings("serial")
	@DomEvent("change")
    public static class CustomerTypeChangeEvent extends ComponentEvent<RadioButtonGroup<String>> {
        public CustomerTypeChangeEvent(RadioButtonGroup<String> source, boolean fromClient) {
            super(source, fromClient);
        }
    }
    
	public NewPolicyForm(String uiMode, UICONTAINERTYPES uiContainerType,
							NewPolicyDto newPolicyDto,
							ComponentEventListener<ProductChangeEvent> productChangeListerner,
							ComponentEventListener<CustomerTypeChangeEvent> customerTypeChangeListerner) {
		super(uiMode, uiContainerType);
		
		this.newPolicyDto = newPolicyDto;
		this.newPolicyUIHelper = UIFactory.getInstance().getNewPolicyUIHelper(null);
		
		buildFields(productChangeListerner, customerTypeChangeListerner);
		buildForm();
	}

	@SuppressWarnings("rawtypes")
	public NewPolicyUIHelper getNewPolicyUIHelper() {
		return newPolicyUIHelper;
	}

	private void buildFields(ComponentEventListener<ProductChangeEvent> productChangeListerner,
								ComponentEventListener<CustomerTypeChangeEvent> customerTypeChangeListerner) {
		labels = uiHelper.getSessionLabels();
		
		customerTypeField				= new RadioButtonGroup<String>();
		customerTypeField.setLabel(labels.getString("NewPolicyDialogCustomerTypeLabel"));
		customerTypeField.setItems(UIConstants.CUST_TYPE_COMM, UIConstants.CUST_TYPE_PERS);

		productCodeField 				= new DSComboBox<String>(labels.getString("NewPolicyDialogProductLabel"), String.class);
		provinceField 					= new DSComboBox<String>(labels.getString("NewPolicyDialogProvinceLabel"), String.class)
												.withProductConfigTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		effectiveDateField 				= new DatePicker(labels.getString("NewPolicyDialogEffectiveDateLabel"));
		termLengthField 				= new DSComboBox<Integer>(labels.getString("NewPolicyDialogTermLengthLabel"), Integer.class)
												.withProductConfigTableId(ConfigConstants.LOOKUPTABLE_TermLengths)
												.withSortList(false);
		hasAutoPolicyField 				= new Checkbox(labels.getString("NewPolicyDialogAutomobilePolicyLabel"));
		hasCommGeneralLiabilityField 	= new Checkbox(labels.getString("NewPolicyDialogCommercialGenLiabPolicyLabel"));
		hasMotorTruckCargoField 		= new Checkbox(labels.getString("NewPolicyDialogMotorTruckCargoPolicyLabel"));
		hasGarageField 					= new Checkbox(labels.getString("NewPolicyDialogOntarioGaragePolicyLabel"));
		hasCommercialPropertyField		= new Checkbox(labels.getString("NewPolicyDialogCommercialPropertyPolicyLabel"));
		liabilityLimitField				= new PolicyLabilityLimitField(labels.getString("NewPolicyDialogFormLiabilityLimitLabel"), true);
		quotePreparedByField			= new DSComboBox<String>(labels.getString("PolicyInfoFormQuotePreparedBy"), String.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_QuotePreparedByList);
		policyNumberField				= new TextField(labels.getString("NewPolicyDialogPolicyNumberLabel"));
		quoteNumberField				= new TextField(labels.getString("NewPolicyDialogQuoteNumberLabel"));
		policyTermField					= new DSNumberField<Integer>(labels.getString("NewPolicyDialogPolicyTermNumberLabel"), Integer.class);
		extQuoteNumField				= new TextField(labels.getString("NewPolicyDialogExternalQuoteNumberLabel"));

		uiHelper.fieldTextAlightLeft(termLengthField);
		effectiveDateField.addValueChangeListener(o -> loadProductLookup(false));
		productCodeField.addValueChangeListener(o -> onProductCodeChange(productChangeListerner));
		customerTypeField.addValueChangeListener(o -> onCustomerTypeChange(customerTypeChangeListerner));
		
		binder = new Binder<NewPolicyDto>(NewPolicyDto.class);
		/* Cannot make this working
		binder.bindInstanceFields(this);
		productCodeField.setRequired(true);
		productCodeField.setRequiredIndicatorVisible(true);
		*/
		//binder.forField(productCodeField).asRequired().bind(WrkNewPolicyDto::getProductItem, WrkNewPolicyDto::setProductItem);
		
		binder.forField(customerTypeField).asRequired().bind(NewPolicyDto::getCustomerType, NewPolicyDto::setCustomerType);
		binder.forField(productCodeField).asRequired().bind(NewPolicyDto::getProductCd, NewPolicyDto::setProductCd);
		binder.forField(provinceField).asRequired().bind(NewPolicyDto::getProvince, NewPolicyDto::setProvince);
		binder.forField(effectiveDateField).asRequired().bind(NewPolicyDto::getEffectiveDate, NewPolicyDto::setEffectiveDate);
		binder.forField(termLengthField).asRequired().bind(NewPolicyDto::getTermLength, NewPolicyDto::setTermLength);
		hasAutomobileSubPolicyBinding = binder.forField(hasAutoPolicyField).bind(NewPolicyDto::getHasAutomobileSubPolicy, NewPolicyDto::setHasAutomobileSubPolicy);
		binder.forField(hasCommGeneralLiabilityField).bind(NewPolicyDto::getHasCGLSubPolicy, NewPolicyDto::setHasCGLSubPolicy);
		binder.forField(hasMotorTruckCargoField).bind(NewPolicyDto::getHasCargoSubPolicy, NewPolicyDto::setHasCargoSubPolicy);
		binder.forField(hasGarageField).bind(NewPolicyDto::getHasGarageSubPolicy, NewPolicyDto::setHasGarageSubPolicy);
		binder.forField(hasCommercialPropertyField).bind(NewPolicyDto::getHasCommercialPropertySubPolicy, NewPolicyDto::setHasCommercialPropertySubPolicy);
		binder.forField(liabilityLimitField).asRequired().bind(NewPolicyDto::getLiabilityLimit, NewPolicyDto::setLiabilityLimit);
		binder.forField(quotePreparedByField).asRequired().bind(NewPolicyDto::getQuotePreparedBy, NewPolicyDto::setQuotePreparedBy);
		binder.forField(extQuoteNumField).bind(NewPolicyDto::getExternalQuoteNumber, NewPolicyDto::setExternalQuoteNumber);
		
		if (newPolicyDto.isRenewal()) {
			binder.forField(policyNumberField).asRequired()
											.withValidator(o -> newPolicyUIHelper.validatePolicyNumberPrefix(productCodeField.getValue(), o), labels.getString("NewPolicyDialogPolicyNumberPrefixError"))
											.withValidator(o -> newPolicyUIHelper.validatePolicyNumberExist(productCodeField.getValue(), o), labels.getString("NewPolicyDialogPolicyNumberExistError"))
											.bind(NewPolicyDto::getPolicyNumber, NewPolicyDto::setPolicyNumber);
			binder.forField(quoteNumberField)
											.withValidator(o -> newPolicyUIHelper.validateQuoteNumberPrefix(productCodeField.getValue(), o), labels.getString("NewPolicyDialogQuoteNumberPrefixError"))
											.withValidator(o -> newPolicyUIHelper.validateQuoteNumberExist(productCodeField.getValue(), o), labels.getString("NewPolicyDialogQuoteNumberExistError"))
											.bind(NewPolicyDto::getQuoteNumber, NewPolicyDto::setQuoteNumber);
			binder.forField(policyTermField).asRequired()
											.withConverter(policyTermField.getConverter())
											.withNullRepresentation(0)
											.withValidator(o -> o != null && o > 0, labels.getString("NewPolicyDialogPolicyTermNumberError"))
											.bind(NewPolicyDto::getPolicyTermNumber, NewPolicyDto::setPolicyTermNumber);
		}
		
		binder.setReadOnly(true);
		
		this.newPolicyDto.setHasAutoPolicyField(hasAutoPolicyField);
		this.newPolicyDto.setHasCommGeneralLiabilityField(hasCommGeneralLiabilityField);
		this.newPolicyDto.setHasMotorTruckCargoField(hasMotorTruckCargoField);
		this.newPolicyDto.setHasGarageField(hasGarageField);
		this.newPolicyDto.setHasCommercialPropertyField(hasCommercialPropertyField);
		
		this.newPolicyDto.setBinder(binder);
		this.newPolicyDto.setHasAutomobileSubPolicyBinding(hasAutomobileSubPolicyBinding);		
	}
	
	private void buildForm() {
		mainForm = new FormLayout();
		uiHelper.formatFullLayout(mainForm);
		
		mainForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		
		mainForm.add(customerTypeField);
		uiHelper.addFormEmptyColumn(mainForm);
		
		if (newPolicyDto.isRenewal()) {
			mainForm.add(extQuoteNumField, quoteNumberField);
			mainForm.add(policyNumberField, policyTermField);
		}
		else {
			mainForm.add(extQuoteNumField);
			uiHelper.addFormEmptyColumn(mainForm);
		}
		
		mainForm.add(effectiveDateField, quotePreparedByField);
		mainForm.add(productCodeField, provinceField);
		mainForm.add(liabilityLimitField, termLengthField);
		uiHelper.addFormEmptyColumn(mainForm);
		
		this.add(mainForm);
	}
	
	private void loadProductLookup(boolean onLoadData) {
		List<LookupTableItem> items = uiConfigHelper.getActiveProductLookups(effectiveDateField.getValue());
		productCodeField.setDSItems(items);
		if (onLoadData) {
			productCodeField.setDSValue(newPolicyDto.getProductCd());
		}
		else {
			if (productCodeField.getDSItemCount() == 1 && productCodeField.getValue() == null) {
				productCodeField.setDSFirstValue();
			}
		}
	}
	
	private void onProductCodeChange(ComponentEventListener<ProductChangeEvent> productChangeListerner) {
		loadSelectedProductLookups(false);
		loadSubpolicySection();
		productChangeListerner.onComponentEvent(new ProductChangeEvent(productCodeField, true));
		
		// Enable only when the selected product is LHT
		hasCommercialPropertyField.setEnabled(ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(productCodeField.getValue()));

		final SpecialityAutoSubPolicy<?> autoSubPolicy = newPolicyDto.getAutoSubPolicy();
		if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(productCodeField.getValue())) {
			customerTypeField.setEnabled(true);
			if (autoSubPolicy != null) {
				autoSubPolicy.setIsFleet(false);
			}
		} else {
			customerTypeField.setValue(UIConstants.CUST_TYPE_COMM);
			customerTypeField.setEnabled(false);
			if (autoSubPolicy != null) {
				autoSubPolicy.setIsFleet(true);
			}
		}
	}
	
	private void onCustomerTypeChange(ComponentEventListener<CustomerTypeChangeEvent> customerTypeChangeListerner) {
		customerTypeChangeListerner.onComponentEvent(new CustomerTypeChangeEvent(customerTypeField, true));
	}
	
	private void loadSelectedProductLookups(boolean onLoadData) {
		if (productCodeField.getLookupTableItemValue() != null) {
			newPolicyUIHelper = UIFactory.getInstance().getNewPolicyUIHelper(productCodeField.getLookupTableItemValue().getItemKey());

			IGenericProductConfig config = (IGenericProductConfig) productCodeField.getLookupTableItemValue().getItemObject();
			uiPolicyHelper.updateSessionProductConfig(config, effectiveDateField.getValue());
		}
		else {
			newPolicyUIHelper = UIFactory.getInstance().getNewPolicyUIHelper(null);
		}
		
		provinceField.loadDSItems();
		termLengthField.loadDSItems();
		liabilityLimitField.loadDSItems();
		// By default, the items are sorted as Strings (1 < 10 < 2); we need to sort as numbers, so that 1 < 2 < 10
		liabilityLimitField.getDSItems().sort(Comparator.comparingInt(item -> Integer.parseInt(item.toString())));

		quotePreparedByField.loadDSItems();

		if (onLoadData) {
			provinceField.setDSValue(newPolicyDto.getProvince());
			termLengthField.setDSValue(newPolicyDto.getTermLength());
			quotePreparedByField.setDSValue(newPolicyDto.getQuotePreparedBy());
		}
		else {
			if (provinceField.getDSItemCount() == 1 && provinceField.getValue() == null) {
				provinceField.setDSFirstValue();
			}

			// Set default term length value: 12 months if present, otherwise "first" value (a.k.a shortest).
			if (termLengthField.getValue() == null && termLengthField.getDSItemCount() > 0) {
				List<Integer> items = (List<Integer>) termLengthField.getDSItems();
				Optional<Integer> twelveMonths = items.stream()
					.filter(length -> length.equals(12))
					.findFirst();

				if (twelveMonths.isPresent()) {
					termLengthField.setDSValue(twelveMonths.get());
				} else {
					termLengthField.setDSFirstValue();
				}
			}
		}
		
		try {
			newPolicyUIHelper.onProductCodeChange(newPolicyDto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			uiHelper.notificationException(e);
		}
	}
	
	private void loadSubpolicySection() {
		if (mainForm != null) {
			if (subpolicySection != null) {
				mainForm.remove((Component)subpolicySection);
			}
			
			subpolicySection = new TowingNewPolicySubpolicyForm(uiMode, uiContainerType, newPolicyDto);
			if (subpolicySection != null) {
				mainForm.add((Component) subpolicySection, 2);
				subpolicySection.loadData(newPolicyDto);
				subpolicySection.enableEdit();
			}
		}
	}
	
	public boolean saveEdit(NewPolicyDto newPolicyDto) {
		boolean ok = uiHelper.writeValues(binder, newPolicyDto) &&
						(subpolicySection != null && subpolicySection.saveEdit(newPolicyDto));
		
		if (productCodeField.getLookupTableItemValue() != null) {
			IGenericProductConfig config = (IGenericProductConfig) productCodeField.getLookupTableItemValue().getItemObject();
			if (config != null) {
				newPolicyDto.setProductCd(config.getProductCd());
				newPolicyDto.setProductVersion(config.getVersionNum());
				newPolicyDto.setProductProgram(config.getProgramCd());
			}
		}
		
	    return ok;
	}
	
	public NewPolicyDto getNewPolicyDto() {
		NewPolicyDto dto = binder.getBean();
		return dto;
	}
	
	public boolean validateEdit() {
		extQuoteNumField.setInvalid(false);
		extQuoteNumField.setErrorMessage(null);
		
		boolean valid1 = uiHelper.validateValues(binder);
		boolean valid2 = UIFactory.getInstance().getPolicyDetailsUIHelper().validateExternalQuoteNumber(extQuoteNumField.getValue(), true);
		
		if (!valid2) {
			extQuoteNumField.setInvalid(true);
			extQuoteNumField.setErrorMessage(labels.getString("NewPolicyDialogExternalQuoteNumberExistMessage"));
		}
		
		return valid1 && valid2;
	}
	
	public boolean loadData() {
		binder.readBean(newPolicyDto);
		
		if (newPolicyDto.getProductCd() != null) {
			loadProductLookup(true);
			loadSelectedProductLookups(true);
		}
		return true;
	}
	
	public boolean enableEdit() {
		enableDataEdit(binder);
		customerTypeField.setEnabled(false); // Only enabled for certain products
		
		setFocus();
		
		return true;
	}
	
	@Override
	public void setFocus() {
		extQuoteNumField.focus();
	}
}
