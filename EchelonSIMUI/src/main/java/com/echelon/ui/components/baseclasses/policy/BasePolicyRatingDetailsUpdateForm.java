package com.echelon.ui.components.baseclasses.policy;

import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.ds.ins.prodconf.config.rating.RatebookVersionConfiguration;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyRatingDetailsUpdateForm extends BaseLayout implements IDSFocusAble {
	private static final long serialVersionUID = 1L;
	private static final Logger log = Logger.getLogger(BasePolicyRatingDetailsUpdateForm.class.getName());

	private final Binder<RatebookVersionConfiguration> binder = new Binder<>();

	private ComboBox<Integer> version;
	private TextField status;
	private DatePicker newBusinessEffDate;
	private DatePicker newBusinessExpDate;
	private DatePicker renewalEffDate;
	private DatePicker renewalExpDate;

	private List<RatebookVersionConfiguration> ratebookVersions;

	public BasePolicyRatingDetailsUpdateForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);

		buildFields();
		bindFields();
		setupFieldListeners();
		buildLayout();
	}

	public RatebookVersionConfiguration getSelectedRatebookVersion() {
		return binder.getBean();
	}

	@Override
	public void setFocus() {
		version.focus();
	}

	public void loadData(List<RatebookVersionConfiguration> ratebookVersions, Integer currentRatebookVersion) {
		this.ratebookVersions = Objects.requireNonNull(ratebookVersions);

		final List<Integer> ratebookVersionNumbers = ratebookVersions.stream()
				.map(RatebookVersionConfiguration::getVersionNum).collect(Collectors.toList());
		version.setItems(ratebookVersionNumbers);

		if (ratebookVersionNumbers.contains(currentRatebookVersion)) {
			version.setValue(currentRatebookVersion);
			updateSelectedRatebookVersion(currentRatebookVersion);
		} else {
			log.log(Level.WARNING, "Unknown ratebook version: {0}", currentRatebookVersion);
		}
	}

	protected void buildFields() {
		version = new ComboBox<>(labels.getString("PolicyRatingDetailsRatebookVerLabel"));
		version.setRequired(true);

		status = new TextField(labels.getString("PolicyRatingDetailsRatebookStatusLabel"));
		newBusinessEffDate = new DatePicker(labels.getString("PolicyRatingDetailsRatebookNewBusEffDateLabel"));
		newBusinessExpDate = new DatePicker(labels.getString("PolicyRatingDetailsRatebookNewBusExpDateLabel"));
		renewalEffDate = new DatePicker(labels.getString("PolicyRatingDetailsRatebookRenewalEffDateLabel"));
		renewalExpDate = new DatePicker(labels.getString("PolicyRatingDetailsRatebookRenewalExpDateLabel"));
	}

	protected void bindFields() {
		binder.forField(status).bind(RatebookVersionConfiguration::getStatus, null);
		binder.forField(newBusinessEffDate).withConverter(new DSLocaldateTimeToLocalDateConverter())
				.bind(RatebookVersionConfiguration::getNewBusinessEffDate, null);
		binder.forField(newBusinessExpDate).withConverter(new DSLocaldateTimeToLocalDateConverter())
				.bind(RatebookVersionConfiguration::getNewBusinessExpDate, null);
		binder.forField(renewalEffDate).withConverter(new DSLocaldateTimeToLocalDateConverter())
				.bind(RatebookVersionConfiguration::getRenewalEffDate, null);
		binder.forField(renewalExpDate).withConverter(new DSLocaldateTimeToLocalDateConverter())
				.bind(RatebookVersionConfiguration::getRenewalExpDate, null);
	}

	protected void setupFieldListeners() {
		version.addValueChangeListener(event -> updateSelectedRatebookVersion(event.getValue()));
	}

	protected void buildLayout() {
		setWidthFull();

		final FormLayout form = new FormLayout();
		form.setWidthFull();
		form.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2));

		form.add(version, status);
		form.add(newBusinessEffDate, newBusinessExpDate);
		form.add(renewalEffDate, renewalExpDate);

		add(form);
	}

	private void updateSelectedRatebookVersion(Integer versionNum) {
		binder.setBean(null);
		if (version != null) {
			ratebookVersions.stream()
					.filter(ratebookVersionConfig -> ratebookVersionConfig.getVersionNum() == versionNum).findFirst()
					.ifPresent(binder::setBean);
		}
	}
}
