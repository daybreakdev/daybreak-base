package com.echelon.ui.components.entities;

import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.towing.AdditionalInsuredVehiclesList;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout.Orientation;

public class AdditionalInsuredAndVehiclesView extends BaseLayout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6291822039188274172L;
	private SplitLayout 					groupLayout;
	private AdditionalInsuredList			addInsuredList;
	private AdditionalInsuredVehiclesList	vehiclesList;

	public AdditionalInsuredAndVehiclesView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		buildLayout();
	}
	
	private void buildLayout() {
		this.setSizeFull();
		
		buildGroupLayout();
	}

	private void buildGroupLayout() {
		groupLayout = new SplitLayout();
		uiHelper.formatFullLayout(groupLayout);
		groupLayout.setSizeFull();
		groupLayout.setOrientation(Orientation.VERTICAL);

		addInsuredList = new AdditionalInsuredList(uiMode, uiContainerType);
		vehiclesList = new AdditionalInsuredVehiclesList(uiMode, uiContainerType);

		groupLayout.addToPrimary(addInsuredList);
		groupLayout.addToSecondary(vehiclesList);
		groupLayout.setSplitterPosition(35);
		addInsuredList.setSelectionChangeListener(vehiclesList::lienholderLessorChanged);
		
		groupLayout.setVisible(false);
		this.add(groupLayout);
	}
	
	public void loadData(SpecialityAutoSubPolicy<Risk> parentSubPolicy) {
		groupLayout.setVisible(true);
		addInsuredList.loadData(parentSubPolicy);
	}

}
