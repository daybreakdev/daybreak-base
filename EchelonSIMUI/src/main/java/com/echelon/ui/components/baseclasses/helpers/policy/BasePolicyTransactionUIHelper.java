package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyTransactionProcessor;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.UI;

public class BasePolicyTransactionUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8622562552387653280L;
	
	public interface LoadPolicyTransactionDataCallback{
				public void loadComplete();
			};

	public BasePolicyTransactionUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public void loadPolicytransactionData(LoadPolicyTransactionDataCallback callback) {
		if (policyUIHelper.needToLoadTransactionData()) {
			loadtransactionData(UIConstants.UIACTION_LoadData, callback);
		}
		else {
			callback.loadComplete();
		}
	}
	
	public void loadFullPolicytransactionData(LoadPolicyTransactionDataCallback callback) {
		if (policyUIHelper.getCurrentPolicy().isFullTransDataLoaded() && 
			(!configUIHelper.isCurrentAllowProductReinsurance() || policyUIHelper.getCurrentPolicy().getReinsuranceReportDTO() != null)) {
			callback.loadComplete();
		}
		else {
			loadtransactionData(UIConstants.UIACTION_LoadFull, callback);
		}
	}
	
	protected void loadtransactionData(String action, LoadPolicyTransactionDataCallback callback) {
		(new TaskProcessor(UI.getCurrent(), null) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyTransactionProcessor processor = (BasePolicyTransactionProcessor) taskRun.getProcessor();
					if (callback != null) {
						if (processor.getPolicyTransaction() != null) {
							if (UIConstants.UIACTION_LoadFull.equals(action)) {
								policyUIHelper.updateWithPolicyVersion(processor.getPolicyTransaction().getPolicyVersion(), processor.getPolicyTransaction(), 
										policyUIHelper.getCurrentPolicy().getTransactionHistores(), 
										null);
								policyUIHelper.getCurrentPolicy().setReinsuranceReportDTO(processor.getReinsuranceReportDTO());
								policyUIHelper.getCurrentPolicy().setFullTransDataLoaded(true);
							}
						}
						callback.loadComplete();
					}
				}
				
				return true;
			}
				
		})
		.start(
				new BasePolicyTransactionProcessor(action, 
								policyUIHelper.getCurrentPolicyTransaction()
								).withReinsurable(configUIHelper.isCurrentAllowProductReinsurance())
			);
	}
}
