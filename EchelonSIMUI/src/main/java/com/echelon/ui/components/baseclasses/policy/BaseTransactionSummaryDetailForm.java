package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.PolicyCoverageUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class BaseTransactionSummaryDetailForm extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8676468607948060087L;
	
	public BaseTransactionSummaryDetailForm() {
		super();
		// TODO Auto-generated constructor stub
		
		initlaizeLayout();
	}

	public BaseTransactionSummaryDetailForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initlaizeLayout();
	}
	
	private void initlaizeLayout() {
		// Remove gaps between details
		//this.setSizeFull();
	}

	protected void createDetails(IBusinessEntity busEntity, String title, BaseCoverageList coverageList) {
		VerticalLayout layout = new VerticalLayout();
		uiHelper.formatFullLayout(layout);
		layout.setMinWidth(BaseTransactionSummaryContentView.SUMMARYWIDTH);
		Label titleLabel = new Label(title);
		uiHelper.setLabelFooterStyle(titleLabel);
		
		Icon icon = uiPolicyHelper.getBusinessStatusIcon(busEntity);
		if (icon != null) {
			HorizontalLayout hLayout = new HorizontalLayout();
			uiHelper.formatFullLayout(hLayout);
			hLayout.add(icon, titleLabel);
			layout.add(hLayout);
		}
		else {
			layout.add(titleLabel);
		}
		
		Details details = new Details();
		details.setSummary(layout);
		details.setContent(coverageList);
		details.setOpened(true);
		details.addThemeVariants(DetailsVariant.FILLED, DetailsVariant.REVERSE);
		
		this.add(details);
	}
	
	protected void createPolicyInformation(PolicyTransaction policyTransaction) {
		BaseCoverageList coverageList = (BaseCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.Policy, uiMode, uiContainerType);
		createDetails(policyTransaction, labels.getString("TransactionInformationPolicyLabel"), coverageList);
		
		((PolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Policy)).loadView(coverageList, policyTransaction);
	}
	
	public void loadData() {
		PolicyTransaction<?> policyTransaction = uiPolicyHelper.getCurrentPolicyTransaction();
		
		if (policyTransaction != null) {
			//createPolicyInformation(policyTransaction);
		}
	}
}
