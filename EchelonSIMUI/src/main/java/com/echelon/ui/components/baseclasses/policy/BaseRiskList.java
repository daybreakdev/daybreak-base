package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.List;

import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.provider.ListDataProvider;

public class BaseRiskList<RISK extends Risk> extends BaseListLayout<RISK> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1671991658462030356L;
	protected Grid<RISK>			riskGrid;
	
	public BaseRiskList() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseRiskList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}
	
	public BaseRiskList(String uiMode, UICONTAINERTYPES uiContainerType, String title) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		initializeLayouts(title);
		buildGrid();
	}
	
	protected String[] getListActions() {
		return null;
	}

	protected void initializeLayouts(String title) {
		this.setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType) ||
			UIConstants.UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													title,
													UIConstants.UICOMPONENTTYPES.List,
													getListActions());
			this.toolbar = headComponents.getToolbar();			
			setupNewButtonEvent();
			setupUpdateButtonEvent();
			setupDeleteButtonEvent();
			setupViewButtonEvent();
			setupCopyButtonEvent();
		}		
	}
	
	protected void buildGrid() {
		riskGrid = new Grid<RISK>();
		riskGrid.setSizeFull();
		uiHelper.setGridStyle(riskGrid);
		addGridColumns();
		riskGrid.setSelectionMode(SelectionMode.SINGLE);
		riskGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(riskGrid);
		
		this.add(riskGrid);
	}
	
	protected void addGridColumns() {
		riskGrid.addColumn(Risk::getDisplayString).setHeader("");		
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					
				}
			});
		}
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					
				}
			});
		}
	}
	
	protected void setupCopyButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Copy) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Copy).addClickListener(event -> {
				if (!riskGrid.getSelectedItems().isEmpty()) {
					
				}
			});
		}
	}

	public boolean loadData(String riskType) {
		// TODO Auto-generated method stub
		List<RISK> risks = new ArrayList<RISK>();
		PolicyTransaction<?> polTransaction = uiPolicyHelper.getCurrentPolicyTransaction();
		// failed to lazily initialize a collection
		//risks.addAll(polTransaction.getPolicyRisks());
		
		this.riskGrid.setItems(risks);
		refreshToolbar();
		
		return true;
	}
	
	protected void refreshToolbar(IBusinessEntity parentEntity) {
		if (toolbar != null) {
			toolbar.refreshButtons(parentEntity, riskGrid);
		}
	}
	
	
	public List<RISK> getCurrentList() {
		List<RISK> list = (List<RISK>) ((ListDataProvider<RISK>)this.riskGrid.getDataProvider()).getItems();
		if (list == null) {
			list = new ArrayList<RISK>();
		}
		return list;
	}

}
