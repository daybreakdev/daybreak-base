package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;

public interface IBaseCovAndEndorseForm {

	public void 	setUiMode(String uiMode);
	public void 	loadData(CoverageDto coverageDto);
	public void 	enableEdit(CoverageDto coverageDto);
	public boolean 	validateEdit(boolean showMessage);
	public boolean 	saveEdit(CoverageDto coverageDto);
	public boolean 	anyExtraFields();
	public void 	setFormVisible(boolean isVisible);
	public void 	setFocus();
	public boolean  isDataInstanceModified();
	public void	    setIsReinsuredValueChangeListener(HasValue.ValueChangeListener<? super ComponentValueChangeEvent<?, ?>> listener);
}
