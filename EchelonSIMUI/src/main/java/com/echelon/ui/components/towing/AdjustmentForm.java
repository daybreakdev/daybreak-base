package com.echelon.ui.components.towing;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.AdjustmentUIHelper;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class AdjustmentForm extends BaseLayout implements IDSFocusAble {
	private static final long serialVersionUID = 7066167645152712080L;

	private AdjustmentUIHelper    adjustmentUIHelper;
	private Binder<SpecialtyVehicleRiskAdjustment> binder;

	private IntegerField vehicleNumber;
	private IntegerField numberOfUnits;
	private TextField vehicleDescription;
	private DatePicker adjustmentDate;
	private IntegerField adjustedUnits;
	private IntegerField difference;
	private NumberField ratePerUnit;
	private DSNumberField<Double> prorataFactor;
	private NumberField premiumChange;

	public AdjustmentForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.adjustmentUIHelper = new AdjustmentUIHelper();
		
		buildFields();
		bindFields();
		buildForm();
	}

	public void loadData(SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		loadDataConfigs(binder);
		binder.readBean(adjustment);
		setValidation(parentVehicle, adjustment);
	}

	public boolean validate() {
		return uiHelper.validateValues(binder);
	}

	public boolean save(SpecialtyVehicleRiskAdjustment adjustment) {
		return uiHelper.writeValues(binder, adjustment);
	}

	@Override
	public void setFocus() {
		vehicleDescription.focus();
	}

	private void buildFields() {
		vehicleNumber = new IntegerField(labels.getString("AdjustmentVehicleNumberLabel"));
		numberOfUnits = new IntegerField(labels.getString("AdjustmentNumberOfUnitsLabel"));
		vehicleDescription = new TextField(labels.getString("AdjustmentVehicleDescriptionLabel"));
		adjustmentDate = new DatePicker(labels.getString("AdjustmentAdjustmentDateLabel"));
		adjustedUnits = new IntegerField(labels.getString("AdjustmentAdjustedUnitsLabel"));
		difference = new IntegerField(labels.getString("AdjustmentDifferenceLabel"));
		ratePerUnit = new NumberField(labels.getString("AdjustmentRatePerUnitLabel"));
		prorataFactor = new DSNumberField<>(labels.getString("AdjustmentProRataFactorLabel"), Double.class, 3);
		premiumChange = new NumberField(labels.getString("AdjustmentPremiumChangeLabel"));

		adjustedUnits.setHasControls(true);

		difference.setReadOnly(true);
		prorataFactor.setReadOnly(true);
		premiumChange.setReadOnly(true);

		/*
		 * Difference is not persisted anywhere; we show it for the user's convenience.
		 * It reflects the change in # of units, whereas adjustedUnits shows the final
		 * result.
		 */
		adjustedUnits.addValueChangeListener(event -> {
			final Integer originalNumUnits = numberOfUnits.getValue();
			final Integer currentNumUnits = event.getValue();
			final int diff = currentNumUnits - originalNumUnits;
			difference.setValue(diff);
		});
	}

	private void bindFields() {
		binder = new Binder<SpecialtyVehicleRiskAdjustment>();

		binder.forField(vehicleNumber)
			.bind(adjustment -> adjustment.getSpecialtyVehicleRisk().getVehicleSequence(), null);
		binder.forField(numberOfUnits)
			.bind(adjustment -> adjustmentUIHelper.getPrevAdjustmentNumOfUnits(adjustment), null);
		binder.forField(vehicleDescription)
			.asRequired()
			.bind(SpecialtyVehicleRiskAdjustment::getVehicleDescription,
					SpecialtyVehicleRiskAdjustment::setVehicleDescription);
		binder.forField(adjustmentDate)
			.asRequired()
			.bind(SpecialtyVehicleRiskAdjustment::getAdjustmentDate, SpecialtyVehicleRiskAdjustment::setAdjustmentDate);
		binder.forField(adjustedUnits)
			.asRequired()
			.bind(SpecialtyVehicleRiskAdjustment::getNumOfUnits, SpecialtyVehicleRiskAdjustment::setNumOfUnits);
		binder.forField(ratePerUnit)
			.bind(adjustmentUIHelper::getOriginatingAdjustmentUnitRate, null); // Read-only
		binder.forField(prorataFactor)
			.bind(adjustment -> String.valueOf(adjustment.getProRataFactor()), null); // Read-only: set by business tier
		binder.forField(premiumChange)
			.bind(SpecialtyVehicleRiskAdjustment::getPremiumChange, null); // Read-only: set by business tier
		
		uiHelper.setEditFieldsReadonly(numberOfUnits);
	}

	private void buildForm() {
		FormLayout layout = new FormLayout();
		layout.setResponsiveSteps(new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 5));

		layout.add(vehicleNumber, numberOfUnits);
		layout.add(vehicleDescription, 2);
		layout.add(adjustmentDate);

		layout.add(adjustedUnits, difference, ratePerUnit, prorataFactor, premiumChange);

		add(layout);
	}

	private void setValidation(SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		// Adjustment date validation. Depends on term effective/expiry dates
		final PolicyTerm policyTerm = uiPolicyHelper.getCurrentPolicyTransaction().getPolicyTerm();
		final PolicyVersion policyVer = uiPolicyHelper.getCurrentPolicyVersion();
		final SpecialtyVehicleRiskAdjustment prevAdj = adjustment.findPrevAdjustment();
		final SpecialtyVehicleRiskAdjustment nextAdj = adjustment.findNextAdjustment();
		if (prevAdj != null) {
			adjustmentDate.setMin(prevAdj.getAdjustmentDate());
		}
		else {
			adjustmentDate.setMin(policyTerm.getTermEffDate().toLocalDate());
		}
		if (nextAdj != null) {
			adjustmentDate.setMax(nextAdj.getAdjustmentDate());
		}
		else {
			adjustmentDate.setMax(policyVer.getVersionDate());
		}
		
		// Adjusted units validation (depends on fleet basis)
		final String fleetBasis = Optional.ofNullable(parentVehicle)
			.map(SpecialtyVehicleRisk::getSubPolicy)
			.filter(subPolicy -> subPolicy instanceof SpecialityAutoSubPolicy ? true : false)
			.map(SpecialityAutoSubPolicy.class::cast)
			.map(SpecialityAutoSubPolicy::getFleetBasis)
			.orElse(StringUtils.EMPTY);
		final int numUnits = parentVehicle.getNumberOfUnits();
		switch (fleetBasis) {
		case "21BQ": // Pro rata (quarterly)
		case "21BS": // Pro rata (semi-annual)
		case "21BA": // Pro rata (annual)
			/*
			 * Pro rata policies make adjustments one vehicle at a time. Only allow the user
			 * to set the adjustment number of units to one more or less than the current
			 * number of units.
			 */
			adjustedUnits.setMin(Math.max(0, numUnits - 1)); // Guard against negative # of vehicles
			adjustedUnits.setMax(Math.max(0, numUnits + 1)); // Guard against negative # of vehicles
			break;
		case "21B5": // 50/50
			// 50/50 policies make adjustments in bulk. No special handling needed.
			adjustedUnits.setMin(0);
			break;
		}
	}
}
