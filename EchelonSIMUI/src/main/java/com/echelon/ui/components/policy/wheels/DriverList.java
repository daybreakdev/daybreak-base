package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.echelon.ui.helpers.policy.wheels.DriverUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.router.Route;

@Route(value="DriverListNodeContentView", layout = BasePolicyView.class)
public class DriverList extends BaseListLayout<Driver> implements IPolicyViewContent {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1500726241628756970L;
	private DriverUIHelper 		driverUIHelper = new DriverUIHelper();	
	private Grid<Driver> 		driverGrid;

	private PersonalCustomerUIHelper	personalCustomerUIHelper;
	
	public DriverList() {
		super();
		// TODO Auto-generated constructor stub
		this.personalCustomerUIHelper = new PersonalCustomerUIHelper();
		
		initializeLayout();
		buildGrid();
	}

	public DriverList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		this.personalCustomerUIHelper = new PersonalCustomerUIHelper();
		
		initializeLayout();
		buildGrid();
	}

	private void initializeLayout() {
		this.setSizeFull();
		
		if (!UIConstants.UIACTION_Select.equalsIgnoreCase(this.uiMode)) {
			DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
															labels.getString("DriverListTitle"),
															UIConstants.UICOMPONENTTYPES.List);
			this.toolbar = headerComponents.getToolbar();		
			setupNewButtonEvent();
			setupUpdateButtonEvent();
			setupDeleteButtonEvent();
			setupUndeleteButtonEvent();
		}
	}
	
	private void buildGrid() {
		driverGrid = new Grid<Driver>();
		driverGrid.setSizeFull();
		uiHelper.setGridStyle(driverGrid);
		
		createIndicatorColumn(driverGrid);

		driverGrid.addColumn(Driver::getDriverSequence).setWidth("10px");
		driverGrid.addColumn(driver -> personalCustomerUIHelper.getCustomerName(driver))
							.setHeader(labels.getString("DriverListColumnNameLabel"))
							.setAutoWidth(true);
		driverGrid.addColumn(driver -> driverUIHelper.getDriverAge(driver.getBirthDate()))
							.setHeader(labels.getString("DriverListColumnAgeLabel"));
		driverGrid.addColumn(new DSLookupItemRenderer<Driver>(Driver::getMaritalStatus)
							.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_MaritalStatuses))
							.setHeader(labels.getString("DriverListColumnMaritalStatusLabel"));
		driverGrid.addColumn(new DSLookupItemRenderer<Driver>(Driver::getGender)
							.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_Genders))
							.setHeader(labels.getString("DriverListColumnSexLabel"));
		driverGrid.addColumn(driver -> driverUIHelper.getYearsLicensed(driver.getDateLicensed()))
							.setHeader(labels.getString("DriverListColumnYearsLicensedLabel"));
		
		driverGrid.setSelectionMode(SelectionMode.SINGLE);
		driverGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		if (!UIConstants.UIACTION_Select.equalsIgnoreCase(this.uiMode)) {
			driverGrid.addSelectionListener(o -> {
				if (driverGrid.asSingleSelect() != null) {
					uiPolicyHelper.selectPolicyItem(UIConstants.POLICYTREENODES.Driver, 
													o.getSource().getSelectedItems().iterator().next());
				}
			});
			setUpGridDbClickEvent(driverGrid);
		}
				
		this.add(driverGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				driverUIHelper.newDriverAction();
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (driverGrid.asSingleSelect() != null) {
					driverUIHelper.updateDriverAction(driverGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (driverGrid.asSingleSelect() != null) {
					driverUIHelper.deleteDriverAction(driverGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (driverGrid.asSingleSelect() != null) {
					driverUIHelper.undeleteDriverAction(driverGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(Set<Driver> set, Driver driver) {
		List<Driver> list = driverUIHelper.sortDriverList(set);

		this.driverGrid.setItems(list);
		
		if (driver != null) {
			this.driverGrid.select(driver);	
		}
		
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		if (toolbar != null) {
			toolbar.refreshButtons(uiPolicyHelper.getCurrentPolicyTransaction(), driverGrid);
		}
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		driverUIHelper.loadView(this, nodeItem);
	}
	
	public Driver getSelectedDriver() {
		return driverGrid.asSingleSelect() != null ? driverGrid.asSingleSelect().getValue() : null; 
	}
}
