package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.SubPolicy;

public class BaseSubPolicyView extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -135324468438214961L;
	protected ActionToolbar				toolbar;

	public BaseSubPolicyView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	protected void initializeSubPolicyView(String title) {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType) ||
			UIConstants.UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													title,
													UIConstants.UICOMPONENTTYPES.Form);
			this.toolbar = (ActionToolbar) headComponents.getToolbar();
			buildButtonEvents();
		}
	}

	protected void buildButtonEvents() {
		
	}

	protected void refreshToolbar(IBusinessEntity subPolicy, boolean allowNew, boolean allowUpdate, boolean allowDelete) {
		if (toolbar != null) {
			toolbar.refreshButtons(null, subPolicy);
			if (!allowNew) {
				if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
					toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
				}
			}
			if (!allowUpdate) {
				if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
					toolbar.getButton(UIConstants.UIACTION_Update).setVisible(false);
				}
			}
			if (!allowDelete) {
				if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
					toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
				}
			}
		}
	}
	
	public void loadData(SubPolicy subPolicy) {
		refreshToolbar(subPolicy, true, true, true);
	}
}
