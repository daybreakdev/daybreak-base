package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.vaadin.flow.data.binder.Binder;

public class CovDeductibleField extends BaseCovLimitAndDedField {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6267001730315531349L;

	public CovDeductibleField(BaseCoverageConfigUIHelper coverageConfigUIHelper, BaseCoverageUIHelper coverageUIHelper) {
		super(coverageConfigUIHelper, coverageUIHelper);
	}
	
	public void bindField(Binder<CoverageDto> binder) {
		super.setBinder(binder);
		
		removeBinding();
		
		if (!isAllowOverride() || isDeletedCoverage()) {
			binder.forField(integerField)
						.withNullRepresentation("")
						.withConverter(integerField.getConverter())
						.bind(CoverageDto::getDeductible1, null);
		}
		else if (isInteger()) {
			if (isMandatory()) {
				binder.forField(integerField).asRequired()
						.withNullRepresentation("")
						.withConverter(integerField.getConverter())
						.bind(CoverageDto::getDeductible1, CoverageDto::setDeductible1);
			}
			else {
				binder.forField(integerField)
						.withNullRepresentation("")
						.withConverter(integerField.getConverter())
						.bind(CoverageDto::getDeductible1, CoverageDto::setDeductible1);
			}
		}
		else {
			if (isMandatory()) {
				binder.forField(lookupField).asRequired()
						.bind(CoverageDto::getDeductible1, CoverageDto::setDeductible1);
			}
			else {
				binder.forField(lookupField)
						.bind(CoverageDto::getDeductible1, CoverageDto::setDeductible1);
			}
		}
	}
	
	protected boolean isInteger() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isInteger = this.coverageDto.isDeductibleInteger();

		return isInteger;
	}
	
	protected boolean isMandatory() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isMandatory = this.coverageDto.isDeductibleMandatory();

		return isMandatory;
	}

	protected boolean isAllowOverride() {
		if (this.coverageDto == null) {
			return false;
		}
		
		boolean isAllowOverride = coverageDto.isAllowDeductibleOverride();

		return isAllowOverride;
	}
	
	protected void setFieldItems(CoverageDto dto) {
		coverageConfigUIHelper.setDeductibleFieldItems(lookupField, dto);
	}

	public void validate() {
		if (isAllowOverride() && !isDeletedCoverage()) {
			if (isInteger()) {
				integerField.validate();
				integerField.setRequiredIndicatorVisible(true);
			}
			else {
				lookupField.isInvalid();
				lookupField.setRequiredIndicatorVisible(true);
			}
		}
	}
}
