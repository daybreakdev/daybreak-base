package com.echelon.ui.components.baseclasses.policy;

import java.util.List;

import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyNoteUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.PolicyNotes;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.data.renderer.LocalDateTimeRenderer;

public class BasePolicyNoteList extends BaseListLayout<PolicyNotes> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6036019880969479338L;
	protected Grid<PolicyNotes> 		notesGrid;
	protected boolean					isInternal;
	protected BasePolicyNoteUIHelper	noteUIHelper;
	
	public BasePolicyNoteList(boolean isInternal) {
		super();
		
		this.isInternal   = isInternal;
		initializeLayouts();
	}

	public BasePolicyNoteList(boolean isInternal, String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.isInternal   = isInternal;
		initializeLayouts();
	}
	
	protected void initializeLayouts() {
		this.noteUIHelper = new BasePolicyNoteUIHelper(isInternal);
		this.setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			// Note Update button is not visible; just for setting enabled
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													null,
													UIConstants.UICOMPONENTTYPES.List);
			toolbar = headComponents.getToolbar();
			
			toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				noteUIHelper.newVehicleAction();
			});
			
			toolbar.getButton(UIConstants.UIACTION_Update).setText(uiHelper.getButtonLabel(UIConstants.UIACTION_Open));
			toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (notesGrid.getSelectedItems().size() > 0) {
					PolicyNotes policyNotes = notesGrid.getSelectedItems().iterator().next();
					if (isOpenView(policyNotes)) {
						noteUIHelper.viewNoteAction(policyNotes);
					}
					else {
						noteUIHelper.updateVehicleAction(policyNotes);
					}
				}
			});
			
			// No View
			if (toolbar.getButton(UIConstants.UIACTION_View) != null) {
				toolbar.getButton(UIConstants.UIACTION_View).setVisible(false);
			}
			
			toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				noteUIHelper.deleteAction(notesGrid.getSelectedItems().iterator().next());;
			});
		}
		
		buildGrid();
	}

	protected void buildGrid() {
		notesGrid = new Grid<PolicyNotes>();
		notesGrid.setSizeFull();
		uiHelper.setGridStyle(notesGrid);
		
		notesGrid.addColumn(PolicyNotes::getDescription).setHeader(labels.getString("PolicyNoteColumnSubjectLabel"))
							.setWidth("60%");
		notesGrid.addColumn(new LocalDateTimeRenderer<>(PolicyNotes::getOriginatingDate, UIConstants.SYSDATETIMEFORMAT))
							.setHeader(labels.getString("PolicyNoteColumnCreatedDateLabel"));
		notesGrid.addColumn(PolicyNotes::getOriginatingAuthor).setHeader(labels.getString("PolicyNoteColumnCreatedByLabel"));
		
		notesGrid.setSelectionMode(SelectionMode.SINGLE);
		notesGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		notesGrid.addSelectionListener(event -> refreshToolbar());
		setUpGridDbClickEvent(notesGrid);
		
		this.add(notesGrid);
	}
	
	public void loadData() {
		List<PolicyNotes> list = noteUIHelper.buildNoteList(uiPolicyHelper.getCurrentPolicyVersion());
		notesGrid.setItems(list);
		refreshToolbar();
	}
	
	protected boolean isOpenView(PolicyNotes policyNotes) {
		boolean isView = true;
		
		if (toolbar.getButton(UIConstants.UIACTION_New).isEnabled() &&
				noteUIHelper.isNoteEditable(policyNotes)) {
			isView = false;
		}
		
		return isView;
	}
	
	protected boolean isDeleteOk(PolicyNotes policyNotes) {
		boolean result = false;
		
		if (toolbar.getButton(UIConstants.UIACTION_New).isEnabled() &&
				noteUIHelper.isNoteEditable(policyNotes)) {
			result = true;
		}
		
		return result;
	}
	
	protected void refreshToolbar() {
		if (toolbar != null) {
			toolbar.refreshButtons(null, notesGrid);
						
			if (toolbar.getButton(UIConstants.UIACTION_New) != null &&
				!toolbar.getButton(UIConstants.UIACTION_New).isEnabled() &&
				noteUIHelper.canCreateNewNote()) {
				toolbar.getButton(UIConstants.UIACTION_New).setEnabled(true);
			}
									
			// Always enable Update(Open) when row is selected (controlled by isOpenView)
			if (!notesGrid.getSelectedItems().isEmpty()) {
				PolicyNotes policyNotes = notesGrid.getSelectedItems().iterator().next();
				
				if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
					toolbar.getButton(UIConstants.UIACTION_Update).setEnabled(true);
				}
				
				if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
					toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(isDeleteOk(policyNotes));
				}
			}
		}
	}
}
