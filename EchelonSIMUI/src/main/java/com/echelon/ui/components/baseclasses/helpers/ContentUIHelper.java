package com.echelon.ui.components.baseclasses.helpers;

import com.ds.ins.uicommon.helpers.DSBaseContentUIHelper;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;

public class ContentUIHelper extends DSBaseContentUIHelper<UIHelper, ProductConfigUIHelper, LookupUIHelper> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1514915986729308637L;
	protected PolicyUIHelper			policyUIHelper;
	protected POLICYTREENODES			policyTreeNode;

	public ContentUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void postConstruct() {
		// TODO Auto-generated method stub
		super.postConstruct();
		
		policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
	}

}
