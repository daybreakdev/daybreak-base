package com.echelon.ui.components.baseclasses;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.uicommon.components.DSMultiNewEntryForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.icon.Icon;

public abstract class MultiNewEntryForm<FORM, TYPE> extends DSMultiNewEntryForm<FORM, TYPE> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1862978656901119666L;

	public MultiNewEntryForm(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType, int maxVisibleRow) {
		super(uiMode, uiContainerType, maxVisibleRow);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isIndicatorColumnRequired() {
		return BrokerUI.getHelperSession().getPolicyUIHelper().isBusinesEntityIndicatorColumnRequired();
	}

	@Override
	protected Icon getColumnIcon(IBusinessEntity businessEntity) {
		Icon icon = BrokerUI.getHelperSession().getPolicyUIHelper().getBusinessStatusIcon(businessEntity);
		return icon;
	}

	@Override
	public void enableEdit() {
		if (BrokerUI.getHelperSession().getPolicyUIHelper().isCurrentTransactionEditable()) {
			entryGrid.getColumnByKey(COLUMNKEY_TOOLBAR).setVisible(true);
			return;
		}
		
		super.enableEdit();
	}
	
}
