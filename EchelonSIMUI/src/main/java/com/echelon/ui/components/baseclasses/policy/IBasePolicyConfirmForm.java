package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;

public interface IBasePolicyConfirmForm {

	public boolean 				validateEdit();
	public PolicyConfirmDto 	getConfirmDto();
	
}
