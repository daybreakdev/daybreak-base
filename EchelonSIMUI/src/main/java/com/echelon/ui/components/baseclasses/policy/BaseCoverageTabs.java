package com.echelon.ui.components.baseclasses.policy;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.uicommon.components.DSBaseTabLayout;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;

@SuppressWarnings("rawtypes")
public class BaseCoverageTabs extends DSBaseTabLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5036532968809573773L;
	protected DSUpdateDialog<?>				parentDialog;
	protected IBaseCovAndEndorseForm 		covAndEndorseForm;
	protected BaseRatefactorList			ratefactorList;
	protected BaseCoverageReinsuranceList	covReinsuranceList;
	protected UIConstants.OBJECTTYPES		parentObjectType;
	protected String						coverageType;
	protected String						coverageCode;
	protected Boolean						isAllowReinsurance;
	
	public BaseCoverageTabs() {
		super();
	}

	public BaseCoverageTabs(UIConstants.OBJECTTYPES objectType, String coverageType, String coverageCode, Boolean isAllowReinsurance, String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.parentObjectType 	= objectType;
		this.coverageType     	= coverageType;
		this.coverageCode	  	= coverageCode;
		this.isAllowReinsurance = isAllowReinsurance;
		
		buildLayout();
	}

	public void setParentDialog(DSUpdateDialog<?> parentDialog) {
		this.parentDialog = parentDialog;
	}

	@Override
	protected void buildLayout() {
		// in case being called from super() 
		if (this.parentObjectType != null) {
			super.buildLayout();
		}
	}

	@Override
	protected String[] getTabLabels() {
		if (BooleanUtils.isTrue(isAllowReinsurance)) {
			return new String[] {
	                	labels.getString("CoverageListTitle"),
	                	labels.getString("RatefactorListTitle"),
	                	labels.getString("ReinsuranceListTitle")
					};
			
		}
		else {
			return new String[] {
	                	labels.getString("CoverageListTitle"),
	                	labels.getString("RatefactorListTitle")
					};
		}
	}

	@Override
	protected Component[] getTabComponents() {
		if (BooleanUtils.isTrue(isAllowReinsurance)) {
			return new Component[] {
						getCovAndEndorseTab(),
						getRatefactorList(),
						getCoverageReinsuranceList()
					};
		}
		else {
			return new Component[] {
						getCovAndEndorseTab(),
						getRatefactorList()
					};
		}
	}

	public IBaseCovAndEndorseForm getCovAndEndorseForm() {
		return covAndEndorseForm;
	}

	protected Component getCovAndEndorseTab() {
		if (covAndEndorseForm == null) {
			covAndEndorseForm = CoverageUIFactory.getInstance().createCoverageForm(parentObjectType, coverageType, coverageCode, uiMode, UICONTAINERTYPES.Dialog);
			if (BooleanUtils.isTrue(isAllowReinsurance)) {
				covAndEndorseForm.setIsReinsuredValueChangeListener(this::isReinsuredFieldValueChanged);
			}
		}
		
		return (Component) covAndEndorseForm;
	}
	
	protected Component getRatefactorList() {
		if (ratefactorList == null) {
			ratefactorList = new BaseRatefactorList(uiMode, uiContainerType);
		}
		
		return ratefactorList;
	}
	
	public BaseCoverageReinsuranceList getCovReinsuranceList() {
		return covReinsuranceList;
	}

	protected Component getCoverageReinsuranceList() {
		if (covReinsuranceList == null) {
			covReinsuranceList = CoverageUIFactory.getInstance().createCoverageReinsuranceList(parentObjectType, ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageType), uiMode, UICONTAINERTYPES.Tab);
		}
		
		return covReinsuranceList;
	}
	
	public void loadData(CoverageDto coverageDto) {
		covAndEndorseForm.loadData(coverageDto);
		ratefactorList.loadData(coverageDto.getCoverage());
		
		if (covReinsuranceList != null) {
			covReinsuranceList.loadData(null, coverageDto);
			if (BooleanUtils.isTrue(coverageDto.isReinsured())) {
				setReinsuranceTabVisible(true);
			}
			else {
				setReinsuranceTabVisible(false);
			}
		}
		
		refreshPage();
	}

	@Override
	protected void refreshPageAttachComponent(int page, Component component) {
		// TODO Auto-generated method stub
		super.refreshPageAttachComponent(page, component);
		
		if (parentDialog != null) {
			switch (page) {
			case 1:
				parentDialog.setMinHeight("600px");
				break;
				
			case 2:
				parentDialog.setMinWidth("1500px");
				parentDialog.setMinHeight("600px");
				break;

			default:
				break;
			}
		}
	}
	
	public void isReinsuredFieldValueChanged(ValueChangeEvent<?> event) {
		if (BooleanUtils.isTrue((Boolean)event.getValue())) {
			setReinsuranceTabVisible(true);
		}
		else {
			setReinsuranceTabVisible(false);
		}
	}
	
	protected void setReinsuranceTabVisible(boolean isVisible) {
		tabs[2].setVisible(isVisible);
	}
}
