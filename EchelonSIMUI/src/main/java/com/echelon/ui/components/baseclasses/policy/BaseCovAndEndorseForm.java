package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.domain.policy.Premium;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.CoverageUIFactory;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.validator.DateRangeValidator;

public class BaseCovAndEndorseForm extends BaseLayout implements IBaseCovAndEndorseForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5793065407670117955L;
	protected BaseCoverageConfigUIHelper	coverageConfigUIHelper;
	protected TextField						coverageTypeField;
	protected DatePicker					coverageEffectiveField;
	protected DatePicker					coverageExpiryField;
	protected CovLimitField					coverageLimitField;
	protected CovDeductibleField	 		coverageDeductibleField;
	protected CovIsPremiumOvrField			coverageIsPremiumOverrideField;
	protected CovIsReinsuredField			coverageIsReinsuredField;
	protected PremiumField					coverageRatedPremiumField;
	protected CovPremiumField				coverageAnnualPremiumField;
	protected PremiumField					coverageWrittenPremiumField;
	protected PremiumField					coverageNetPremiumChangeField;
	protected CovUWAdjustmentField 			coverageUWAfjuestmentField;
	protected Binder<CoverageDto> 			covBinder;
	protected Binder<Premium> 				premBinder;
	protected DateRangeValidator 			expiryDateValidator;
	protected FormLayout					coverageTopForm;
	protected FormLayout					coverageMiddleForm;
	protected FormLayout					premiumForm;
	protected Div							addlFieldsDiv;
	protected BaseCovAddlFieldsForm			covAddlFieldsForm;
	protected BaseCoverageUIHelper			coverageUIHelper;
	protected CoverageDto 					coverageDto;
	protected UIConstants.OBJECTTYPES 		parentObjectType;

	public BaseCovAndEndorseForm(String uiMode, UICONTAINERTYPES uiContainerType, 
									UIConstants.OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.parentObjectType		= parentObjectType;
		this.coverageUIHelper 		= CoverageUIFactory.getInstance().createCoverageHelper(this.parentObjectType);
		this.coverageConfigUIHelper = CoverageUIFactory.getInstance().createCoverageConfigHelper(this.parentObjectType);
		
		buildLayout();
		buildFields();
		buildForms();
	}
	
	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}
	
	protected void buildFields() {
		expiryDateValidator = new DateRangeValidator(labels.getString("CoverageFormExpiryDateRangeError"), null, null);
		
		coverageTypeField 				= new TextField(labels.getString("CoverageFormCoverageTypeLabel"));
		coverageEffectiveField 			= new DatePicker(labels.getString("CoverageFormEffectiveLabel"));
		coverageExpiryField 			= new DatePicker(labels.getString("CoverageFormExpiryLabel"));
		coverageIsPremiumOverrideField  = new CovIsPremiumOvrField(labels.getString("CoverageFormIsPremiumOverrideLabel"));
		coverageIsReinsuredField		= new CovIsReinsuredField(labels.getString("CoverageFormIsReinsuredLabel"));
		coverageRatedPremiumField 		= new PremiumField(labels.getString("CoverageFormRatedPremiumLabel"));
		coverageAnnualPremiumField 		= new CovPremiumField(labels.getString("CoverageFormAnnualPremiumLabel"));
		coverageWrittenPremiumField 	= new PremiumField(labels.getString("CoverageFormWrittenPremiumLabel"));
		coverageNetPremiumChangeField 	= new PremiumField(labels.getString("CoverageFormNeTPremiumChangeLabel"));
		coverageLimitField 				= new CovLimitField(coverageConfigUIHelper, coverageUIHelper);
		coverageDeductibleField 		= new CovDeductibleField(coverageConfigUIHelper, coverageUIHelper);
		coverageUWAfjuestmentField		= new CovUWAdjustmentField(labels.getString("CoverageFormUWAdjustmentLabel"));
		
		coverageLimitField.setLabel(labels.getString("CoverageFormLimitLabel"));
		coverageDeductibleField.setLabel(labels.getString("CoverageFormDeductibleLabel"));
	}
	
	protected void buildForms() {
		coverageTopForm 	= new FormLayout();
		coverageMiddleForm	= new FormLayout();
		premiumForm  		= new FormLayout();
		addlFieldsDiv		= new Div();
		
		addlFieldsDiv.setSizeFull();
				
		this.add(coverageTopForm, coverageMiddleForm, premiumForm, addlFieldsDiv);
	}
	
	protected void buildCoverageTopForm() {
		coverageTopForm.removeAll();
		
		coverageTopForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 2)
		        );
		
		coverageTopForm.add(coverageTypeField);
		coverageTopForm.setColspan(coverageTypeField, 2);
		coverageTopForm.add(coverageEffectiveField, coverageExpiryField);
		coverageTopForm.add(coverageLimitField, coverageDeductibleField);
		
		if (UICONTAINERTYPES.Grid.equals(this.uiContainerType)) {
			coverageTopForm.setVisible(false);
		}
	}
	
	protected void buildCoverageMiddleForm() {
		coverageMiddleForm.removeAll();
	}
	
	protected void buildPremiunmForm(CoverageDto coverageDto) {
		premiumForm.removeAll();
		
		premiumForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 6)
		        );
		
		//First half
		premiumForm.add(coverageUWAfjuestmentField);
		attachIsPremOvrAndIsReinsuredToForm(premiumForm, 2);
		//Second half
		premiumForm.add(coverageRatedPremiumField, coverageAnnualPremiumField, coverageWrittenPremiumField);
		
		if (UICONTAINERTYPES.Grid.equals(this.uiContainerType)) {
			premiumForm.setVisible(false);
		}
	}
	
	protected IBaseCovAdditionalForm createCoverageAddlFieldsForm(String covCode) {
		return CoverageUIFactory.getInstance().createCoverageAddlFieldsForm(parentObjectType, covCode, uiMode, uiContainerType);
	}

	protected void buildAddlFieldGroupsForm(CoverageDto coverageDto) {
		addlFieldsDiv.removeAll();
		
		covAddlFieldsForm = (BaseCovAddlFieldsForm) createCoverageAddlFieldsForm(coverageDto.getCoverageCode());
		addlFieldsDiv.add(covAddlFieldsForm);

		if (coverageMiddleForm.getChildren().iterator().hasNext()) {
			covAddlFieldsForm.setFollowFormLayout(coverageMiddleForm);
		}
		else {
			covAddlFieldsForm.setFollowFormLayout(coverageTopForm);
		}
	}

	protected void initialzieFields(CoverageDto coverageDto) {
		coverageLimitField.initializeField(coverageDto);
		coverageDeductibleField.initializeField(coverageDto);
		coverageIsReinsuredField.initializeField(coverageDto);
		coverageIsPremiumOverrideField.initializeField(coverageDto, coverageIsReinsuredField);
		coverageAnnualPremiumField.initializeField(coverageDto, coverageIsPremiumOverrideField, coverageIsReinsuredField);
		coverageUWAfjuestmentField.initializeField(this.parentObjectType, coverageDto, coverageIsPremiumOverrideField);
		
		coverageDto.setEditLimitField(coverageLimitField);
		coverageDto.setEditDeductibleField(coverageDeductibleField);
		coverageDto.setEditPremiumField(coverageAnnualPremiumField);
		coverageDto.setEditIsPremiumOverrideField(coverageIsPremiumOverrideField);
		coverageDto.setEditIsReinsuredField(coverageIsReinsuredField);
		coverageDto.setEditUwAdjustmentField(coverageUWAfjuestmentField);
	}
	
	private void buildBinder(CoverageDto coverageDto) {
		buildBinderForCoverage(coverageDto);
		buildBinderForPremium(coverageDto);
	}
	
	protected void buildBinderForCoverage(CoverageDto coverageDto) {
		covBinder = new Binder<CoverageDto>(CoverageDto.class);
		
		covBinder.forField(coverageTypeField).bind(CoverageDto::getDescription, null);
		covBinder.forField(coverageEffectiveField).bind(CoverageDto::getCoverageEffDate, CoverageDto::setCoverageEffDate);
		covBinder.forField(coverageExpiryField)
							.withValidator(expiryDateValidator)
							.bind(CoverageDto::getCoverageExpDate, CoverageDto::setCoverageExpDate);

		coverageUWAfjuestmentField.bindField(covBinder); // .bind("coveragePremium.premiumModifier1");
		coverageLimitField.bindField(covBinder);
		coverageDeductibleField.bindField(covBinder);
		
		covBinder.setReadOnly(true);
	}
	
	protected void buildBinderForPremium(CoverageDto coverageDto) {
		premBinder = new Binder<Premium>(Premium.class);
		
		coverageIsPremiumOverrideField.bindField(covBinder);
		coverageIsReinsuredField.bindField(covBinder);
		coverageAnnualPremiumField.bindField(covBinder);

		premBinder.forField(coverageRatedPremiumField)
							.withNullRepresentation("")
							.withConverter(coverageRatedPremiumField.getConverter())
							.bind(Premium::getOriginalPremium, null);
		premBinder.forField(coverageWrittenPremiumField)
							.withNullRepresentation("")
							.withConverter(coverageWrittenPremiumField.getConverter())
							.bind(Premium::getWrittenPremium, null);
		premBinder.forField(coverageNetPremiumChangeField)
							.withNullRepresentation("")
							.withConverter(coverageNetPremiumChangeField.getConverter())
							.bind(Premium::getNetPremiumChange, null);
		premBinder.setReadOnly(true);
	}
	
	@Override
	public void loadData(CoverageDto coverageDto) {
		this.coverageDto = coverageDto;

		initializeReinsurance();
		initialzieFields(coverageDto);
		buildCoverageTopForm();
		buildCoverageMiddleForm();
		buildPremiunmForm(coverageDto);
		buildAddlFieldGroupsForm(coverageDto);
		buildBinder(coverageDto);
		
		if (coverageDto.getCoverage() != null) {
			covBinder.readBean(coverageDto);
			premBinder.readBean(coverageDto.getCoveragePremium());
			
			if (coverageDto != null) {
				if (coverageDto.getCoverageConfig() != null
						&& !coverageDto.getCoverageConfig().isAllowPremiumOverride()) {
					if (coverageDto.getCoveragePremium() == null
							|| coverageDto.getCoveragePremium().getOriginalPremium() == null
							|| coverageDto.getCoveragePremium().getOriginalPremium().doubleValue() == 0) {
						uiHelper.setFieldValue(coverageRatedPremiumField, null);
					}
					if (coverageDto.getCoveragePremium() == null
							|| coverageDto.getCoveragePremium().getAnnualPremium() == null
							|| coverageDto.getCoveragePremium().getAnnualPremium().doubleValue() == 0) {
						uiHelper.setFieldValue(coverageAnnualPremiumField, null);
					}
					if (coverageDto.getCoveragePremium() == null
							|| coverageDto.getCoveragePremium().getWrittenPremium() == null
							|| coverageDto.getCoveragePremium().getWrittenPremium().doubleValue() == 0) {
						uiHelper.setFieldValue(coverageWrittenPremiumField, null);
					}
					if (coverageDto.getNetPremiumChange() == null
							|| coverageDto.getCoveragePremium().getNetPremiumChange() == null
							|| coverageDto.getCoveragePremium().getNetPremiumChange().doubleValue() == 0) {
						uiHelper.setFieldValue(coverageNetPremiumChangeField, null);
					}
				}
			}
		}
		
		enableDataView(covBinder, premBinder);
		
		if (covAddlFieldsForm != null) {
			covAddlFieldsForm.loadData(coverageDto);
		}
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		this.coverageDto = coverageDto;
		
		enableDataEdit(covBinder, premBinder);
		
		uiHelper.setEditFieldsReadonly(coverageTypeField, coverageEffectiveField, coverageExpiryField,
										coverageWrittenPremiumField);
		
		coverageLimitField.determineReadOnly();
		coverageDeductibleField.determineReadOnly();
		coverageIsPremiumOverrideField.determineReadOnly();
		coverageIsReinsuredField.determineReadOnly();
		coverageAnnualPremiumField.determineReadOnly();
		coverageUWAfjuestmentField.determineReadOnly();
		
		coverageEffectiveField.addValueChangeListener(event -> {
			expiryDateValidator.setMinValue(coverageEffectiveField.getValue());
			covBinder.validate();
		});
		
		if (covAddlFieldsForm != null) {
			covAddlFieldsForm.enableEdit(coverageDto);
		}
	}

	@Override
	public boolean saveEdit(CoverageDto coverageDto) {
		boolean ok1 = uiHelper.writeValues(covBinder, coverageDto);
		boolean ok2 = uiHelper.writeValues(premBinder, coverageDto.getCoveragePremium());
		boolean ok3 = true;
		if (covAddlFieldsForm != null) {
			ok3 = covAddlFieldsForm.saveEdit(coverageDto);
		}
		
		return ok1 && ok2 && ok3;
	}

	public boolean validateEdit() {
		return validateEdit(true);
	}

	@Override
	public boolean validateEdit(boolean showMessage) {
		boolean valid1 = uiHelper.validateValues(showMessage, covBinder, premBinder);
		boolean valid2 = true;
		if (covAddlFieldsForm != null) {
			valid2 = covAddlFieldsForm.validateEdit(showMessage);
		}
		
		return valid1 && valid2;
	}

	@Override
	public boolean anyExtraFields() {
		return (coverageMiddleForm.getChildren().iterator().hasNext()) ||
			   (covAddlFieldsForm != null && covAddlFieldsForm.getChildren().iterator().hasNext());
	}

	@Override
	public void setFormVisible(boolean isVisible) {
		if (covAddlFieldsForm != null) {
			covAddlFieldsForm.setFormVisible(isVisible);
		}
	}

	@Override
	public void setFocus() {
		if (!uiHelper.setFocus(coverageTopForm, coverageMiddleForm, premiumForm)) {
			if (covAddlFieldsForm != null) {
				covAddlFieldsForm.setFocus();
			}
		}
	}

	@Override
	public boolean isDataInstanceModified() {
		return false;
	}

	@Override
	public void setIsReinsuredValueChangeListener(
			ValueChangeListener<? super ComponentValueChangeEvent<?, ?>> listener) {
		coverageIsReinsuredField.addValueChangeListener(listener);
	}

	protected void initializeReinsurance() {
		if (uiConfigHelper.isCurrentAllowProductReinsurance()) {
			coverageIsReinsuredField.setVisible(true);
		}
		else {
			coverageIsReinsuredField.setVisible(false);
		}
	}

	// assignedColumns = available columns for these 2 fields
	protected VerticalLayout attachIsPremOvrAndIsReinsuredToForm(FormLayout form, int assignedColumns) {
		VerticalLayout fieldLayout = null;
		
		if (coverageIsReinsuredField.isVisible()) {
			FormLayout attForm = new FormLayout();
			// Alien the check-boxes to left
			fieldLayout = new VerticalLayout();
			uiHelper.formatFullLayout(fieldLayout);
			if (assignedColumns == 1) {
				fieldLayout.getElement().getStyle().set("margin-top","27px");	
			}
			else {
				fieldLayout.getElement().getStyle().set("margin-top","15px");
			}
			fieldLayout.setAlignItems(Alignment.START);
			fieldLayout.add(coverageIsPremiumOverrideField, coverageIsReinsuredField);
			attForm.add(fieldLayout);
			form.add(attForm, assignedColumns);
		}
		else {
			form.add(coverageIsPremiumOverrideField, assignedColumns);
		}
		
		return fieldLayout;
	}
}
