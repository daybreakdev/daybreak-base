package com.echelon.ui.components.entities;

import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.ds.ins.domain.entities.PersonalCustomer;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;

public class PersonalCustomerView extends CustomerView<PersonalCustomer> {
	private static final long serialVersionUID = 8422624399424545446L;

	public PersonalCustomerView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected CustomerUIHelper<PersonalCustomer> createCustomerUIHelper() {
		return new PersonalCustomerUIHelper();
	}

}
