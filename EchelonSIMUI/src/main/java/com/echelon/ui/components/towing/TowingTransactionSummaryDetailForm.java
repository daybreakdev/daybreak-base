package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLocalDateFormat;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.policy.BaseTransactionSummaryDetailForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyCoverageUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class TowingTransactionSummaryDetailForm extends BaseTransactionSummaryDetailForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1586260508560701346L;

	public TowingTransactionSummaryDetailForm() {
		super(UIConstants.UIACTION_View, UICONTAINERTYPES.Information);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void loadData() {
		super.loadData();
		
		PolicyTransaction<?> policyTransaction = uiPolicyHelper.getCurrentPolicyTransaction();
		if (policyTransaction != null) {
			SubPolicy<?> autoSubPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
			if (autoSubPolicy != null) {
				createAutoInformation((SpecialityAutoSubPolicy<?>) autoSubPolicy);
				createVehiclesInformation((SpecialityAutoSubPolicy<?>) autoSubPolicy);
			}
			
			List<SubPolicy<?>> subPolicies = UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)
													.getSortedSubPolicies(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
			if (subPolicies != null && !subPolicies.isEmpty()) {
				for(SubPolicy<?> subPolicy : subPolicies) {
					createCGLInformation((CGLSubPolicy<?>) subPolicy, (subPolicies.size() > 1));
				}
			}
					
			subPolicies = UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)
													.getSortedSubPolicies(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
			if (subPolicies != null && !subPolicies.isEmpty()) {
				for(SubPolicy<?> subPolicy : subPolicies) {
					createCGOInformation((CargoSubPolicy<?>) subPolicy, (subPolicies.size() > 1));
				}
			}
			
			subPolicies = UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE)
													.getSortedSubPolicies(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
			if (subPolicies != null && !subPolicies.isEmpty()) {
				for(SubPolicy<?> subPolicy : subPolicies) {
					createGARInformation((GarageSubPolicy<?>) subPolicy, (subPolicies.size() > 1));
				}
			}
			
			subPolicies = UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY)
													.getSortedSubPolicies(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
			if (subPolicies != null && !subPolicies.isEmpty()) {
				for (SubPolicy<?> subPolicy : subPolicies) {
					createCommercialPropertyInformation((CommercialPropertySubPolicy<?>) subPolicy, (subPolicies.size() > 1));
				}
			}
		}
	}

	protected void createAutoInformation(SpecialityAutoSubPolicy subPolicy) {
		SubPolicyCoverageList coverageList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.AUTOSubPolicy, uiMode, uiContainerType);
		createDetails(subPolicy, labels.getString("TransactionInformationAutoLabel"), coverageList);
		
		((AutoSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.AUTOSubPolicy)).loadView(coverageList, subPolicy);
	}
	
	protected void createVehiclesInformation(SpecialityAutoSubPolicy subPolicy) {
		if (subPolicy != null && subPolicy.getSubPolicyRisks() != null) {
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy = (SpecialityAutoSubPolicy) subPolicy;
			List<SpecialtyVehicleRisk> vehicles = (new VehicleUIHelper()).sortVehicleList(autoSubPolicy.getSubPolicyRisks());	
			for(Risk risk : vehicles) {
				VehicleCoverageList     vehicleCoverageList	  = (VehicleCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.Vehicle, uiMode, uiContainerType);
				VehicleCoverageUIHelper vehicleCoverageUIHelper = (VehicleCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Vehicle);
				SpecialtyVehicleRisk veh = (SpecialtyVehicleRisk) risk;
				createDetails(risk, labels.getString("TransactionInformationAutoLabel")+" : "+veh.getDisplayString(), vehicleCoverageList);
				
				vehicleCoverageUIHelper.loadView(vehicleCoverageList, veh);			
			}
		}
	}

	protected void createCGLInformation(CGLSubPolicy<?> subPolicy, boolean isMultipleRecords) {
		SubPolicyCoverageList coverageList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.CGLSubPolicy, uiMode, uiContainerType);
		createDetails(subPolicy, getSubPolicyLabel(subPolicy, labels.getString("TransactionInformationCGLLabel"), isMultipleRecords), coverageList);
		
		((CGLSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.CGLSubPolicy)).loadView(coverageList, subPolicy);
	}

	protected void createCGOInformation(CargoSubPolicy<?> subPolicy, boolean isMultipleRecords) {
		SubPolicyCoverageList coverageList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.CGOSubPolicy, uiMode, uiContainerType);
		createDetails(subPolicy, getSubPolicyLabel(subPolicy, labels.getString("TransactionInformationCGOLabel"), isMultipleRecords), coverageList);
		
		((CargoSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.CGOSubPolicy)).loadView(coverageList, subPolicy);
	}

	protected void createGARInformation(GarageSubPolicy<?> subPolicy, boolean isMultipleRecords) {
		SubPolicyCoverageList coverageList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.GARSubPolicy, uiMode, uiContainerType);
		createDetails(subPolicy, getSubPolicyLabel(subPolicy, labels.getString("TransactionInformationGARLabel"), isMultipleRecords), coverageList);
		
		((GarageSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.GARSubPolicy)).loadView(coverageList, subPolicy);
	}

	protected void createCommercialPropertyInformation(CommercialPropertySubPolicy<?> subPolicy, boolean isMultipleRecords) {
		SubPolicyCoverageList coverageList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(OBJECTTYPES.CPSubPolicy, uiMode, uiContainerType);
		createDetails(subPolicy, getSubPolicyLabel(subPolicy, labels.getString("TransactionInformationCPLabel"), isMultipleRecords), coverageList);
		
		((CommercialPropertySubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(OBJECTTYPES.CPSubPolicy)).loadView(coverageList, subPolicy);
	}
	
	protected String getSubPolicyLabel(SubPolicy<?> subPolicy, String label, boolean isMultipleRecords) {
		String text = "";
		if (isMultipleRecords) {
			DSLocalDateFormat dateFormat = new DSLocalDateFormat();
			text = dateFormat.format(subPolicy.getEffectiveDate())+" / "+dateFormat.format(subPolicy.getExpiryDate());
		}
		
		return label+" "+text; 
	}
}
