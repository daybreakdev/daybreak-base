package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.GenericFieldGroupsForm;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.formlayout.FormLayout;

public class BaseCovAddlFieldsForm extends BaseLayout implements IBaseCovAdditionalForm {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -777891192643464313L;

	protected UIConstants.OBJECTTYPES parentObjectType;
	protected GenericFieldGroupsForm	addlFieldGroupsForm;
	
	public BaseCovAddlFieldsForm(String uiMode, UICONTAINERTYPES uiContainerType, UIConstants.OBJECTTYPES parentObjectType) {
		super(uiMode, uiContainerType);

		this.parentObjectType = parentObjectType;
		buildLayout();
		buildForms();
	}
	
	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}
	
	protected void buildForms() {
		addlFieldGroupsForm = createCovAddlFieldsForm();
		this.add(addlFieldGroupsForm);
	}
	
	protected GenericFieldGroupsForm createCovAddlFieldsForm() {
		return new GenericFieldGroupsForm(uiMode, uiContainerType);
	}

	public void setFollowFormLayout(FormLayout followFormLayout) {
		addlFieldGroupsForm.setFollowFormLayout(followFormLayout);		
	}
	
	@Override
	public void loadData(CoverageDto coverageDto) {
		addlFieldGroupsForm.loadData(coverageDto.getCoverageConfig(), coverageDto.getDataExtension(), uiConfigHelper.getCurrentUIConfig(), coverageDto);
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		addlFieldGroupsForm.enableEdit();
	}

	@Override
	public boolean validateEdit(boolean showMessage) {
		boolean valid = addlFieldGroupsForm.validateEdit(showMessage);
		return valid;
	}

	@Override
	public boolean saveEdit(CoverageDto coverageDto) {
		boolean ok = addlFieldGroupsForm.saveEdit(coverageDto.getDataExtension());
		return ok;
	}

	@Override
	public boolean anyExtraFields() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setFormVisible(boolean isVisible) {
		addlFieldGroupsForm.setFormVisible(isVisible);
	}

	@Override
	public void setFocus() {
		addlFieldGroupsForm.setFocus();
	}
	
	@Override
	public boolean hasAnyAdditionalFields() {
		// TODO Auto-generated method stub
		return addlFieldGroupsForm.getChildren().iterator().hasNext();
	}

	@Override
	public boolean isDataInstanceModified() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void setIsReinsuredValueChangeListener(
			ValueChangeListener<? super ComponentValueChangeEvent<?, ?>> listener) {
	}
	
}
