package com.echelon.ui.components.baseclasses;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.uicommon.components.DSBaseListLayout;
import com.echelon.ui.components.baseclasses.helpers.LayoutUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.icon.Icon;

@SuppressWarnings("serial")
public abstract class BaseListLayout<MODEL> extends DSBaseListLayout<MODEL, UIHelper, ProductConfigUIHelper, LookupUIHelper, LayoutUIHelper> {
	
	protected PolicyUIHelper uiPolicyHelper;
	
	public BaseListLayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public BaseListLayout(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void postConstruct() {
		uiPolicyHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		
		super.postConstruct();
	}
	
	protected boolean isIndicatorColumnRequired() {
		return uiPolicyHelper.isBusinesEntityIndicatorColumnRequired();
	}
	
	protected Icon getColumnIcon(IBusinessEntity businessEntity) {
		Icon icon = uiPolicyHelper.getBusinessStatusIcon(businessEntity);
		return icon;
	}
}
