package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.towing.dto.AdjustmentDto;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class CargoSubPolicyAdjustmentList extends AdjustmentList {

	/**
	 * 
	 */
	private static final long serialVersionUID = -695916093695958633L;

	public CargoSubPolicyAdjustmentList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, OBJECTTYPES.CGOSubPolicy);
		// TODO Auto-generated constructor stub
	}

	public boolean loadData(CargoSubPolicy<?> parent, List<SpecialtyVehicleRiskAdjustment> adjustments) {
		// TODO Auto-generated method stub
		return super.loadData(adjustments);
	}

	@Override
	protected void refreshToolbar() {
		// TODO Auto-generated method stub
		if (toolbar != null) {
			toolbar.disableAllButtons();
			toolbar.setVisible(false);
		}
	}

	@Override
	protected void loadCoverageList(AdjustmentCoverageList adjCoverageList, AdjustmentDto dto) {
		// TODO Auto-generated method stub
		adjCoverageUIHelper.loadView(adjCoverageList, dto.getAdjustment().getCargoSubPolicy(), dto.getAdjustment());
	}
	
}
