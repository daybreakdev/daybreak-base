package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.echelon.ui.components.baseclasses.dto.PolicySearchCriteria;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.dto.PolicySearchResults;

public class BasePolicySearchProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3467032470702945089L;
	private PolicySearchCriteria criteria;
	private String extQuoteNum;
	private String quoteNumber;
	private String policyNumber;
	private Boolean foundPolicy;
	
	public BasePolicySearchProcessor(PolicySearchCriteria criteria) {
		super(UIConstants.UIACTION_Search, null);
		this.criteria = criteria;
	}
	
	public BasePolicySearchProcessor(String extQuoteNum, String quoteNumber, String policyNumber) {
		super(UIConstants.UIACTION_Search, null);
		this.extQuoteNum = extQuoteNum;
		this.quoteNumber = quoteNumber;
		this.policyNumber = policyNumber;
	}
	
	public PolicySearchCriteria getCriteria() {
		return criteria;
	}

	public Boolean getFoundPolicy() {
		return foundPolicy;
	}

	public void run() {
		if (criteria != null) {
			criteria = runSearch(criteria);
		}
		else if (StringUtils.isNotBlank(extQuoteNum)) {
			foundPolicy = searchTheSameExternalQuoteNumber(extQuoteNum, quoteNumber, policyNumber);
		}
	}
	
	public PolicySearchCriteria runSearch(PolicySearchCriteria criteria) {
		return runSearch(criteria, true);
	}
	
	protected PolicySearchCriteria runSearch(PolicySearchCriteria criteria, boolean showMsg) {
		criteria.setSearchResults(new ArrayList<PolicySearchResults>());
		
		List<PolicySearchResults> results = null;
		try {
			results = policyProcessing.findPolicies(criteria.getPolicyNum(), criteria.getName(), criteria.getBroker(), criteria.getQuoteNum(), criteria.getExtQuoteNum());
		} catch (Exception e) {
			setProcessException(e);
		} catch (Throwable e) {
			setProcessThrowable(e);
		}
				
		if (results == null) {
			results = new ArrayList<PolicySearchResults>();
		}
		
		if (showMsg && results.isEmpty()) {
			setProcessWarnResourceCode("PolicySearchResultNoResultsFoundMessage");
		}
		
		criteria.setSearchResults(results);
		
		/*
		resultGrid.sort(Arrays.asList(
						new GridSortOrder(resultGrid.getColumnByKey("PolicyNum"), SortDirection.ASCENDING)
						));
						*/
		
		return criteria;
	}

	public boolean searchTheSameExternalQuoteNumber(String extQuoteNum, String quoteNumber, String policyNumber) {
		boolean anyResult = false;
		
		PolicySearchCriteria criteria = new PolicySearchCriteria();
		criteria.setExtQuoteNum(extQuoteNum);
		criteria = runSearch(criteria, false);
		if (criteria != null && criteria.getSearchResults() != null && !criteria.getSearchResults().isEmpty()) {
			if (StringUtils.isNotBlank(quoteNumber)) {
				criteria.getSearchResults().removeIf(o -> quoteNumber.equalsIgnoreCase(o.getQuoteNum()));
			}
			if (StringUtils.isNotBlank(policyNumber)) {
				criteria.getSearchResults().removeIf(o -> policyNumber.equalsIgnoreCase(o.getPolicyNum()));
			}
			
			anyResult = !criteria.getSearchResults().isEmpty();
		}
		
		return anyResult;
	}
}
