package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyExposureRatingForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyExposureRatingProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.UI;

public class BasePolicyExposureRatingUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4804103261494757193L;

	public BasePolicyExposureRatingUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected BasePolicyExposureRatingForm createPolicyExposureRatingForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new BasePolicyExposureRatingForm(action, containerType);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected DSUpdateDialog<BasePolicyExposureRatingForm> createPolicyExposureRatingDialog(String title, String action) {
		DSUpdateDialog dialog = (DSUpdateDialog<BasePolicyExposureRatingForm>) createDialog(title, 
															createPolicyExposureRatingForm(action, UICONTAINERTYPES.Dialog), 
															action);
		return dialog;
	}
	
	public void setupPolicyExposureRatingUpdateAction() {
		DSUpdateDialog<BasePolicyExposureRatingForm> dialog = createPolicyExposureRatingDialog(uiHelper.getSessionLabels().getString("PolicyExposureRatingFactorsUpdateTitle"), UIConstants.UIACTION_Update);
		dialog.getContentComponent().loadData(getCurrentSpecialityAutoExpRating());
		dialog.getContentComponent().enabledEdit();
		dialog.open();
	}
	
	public SpecialityAutoExpRating getCurrentSpecialityAutoExpRating() {
		if (policyUIHelper.getCurrentPolicyTransaction() != null && 
				policyUIHelper.getCurrentPolicyTransaction().getPolicyTransactionDatas() != null &&
				!policyUIHelper.getCurrentPolicyTransaction().getPolicyTransactionDatas().isEmpty()) {
			SpecialityAutoExpRating specialityAutoExpRating = (SpecialityAutoExpRating) policyUIHelper.getCurrentPolicyTransaction().getPolicyTransactionDatas()
																.stream().filter(o -> SpecialtyAutoConstants.SPEC_TRANSACTIONDATA_EXPOSURE_RATING.equalsIgnoreCase(o.getDescription()))
																.findFirst().orElse(null);
			return specialityAutoExpRating;
		}	
		
		return null;
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<BasePolicyExposureRatingForm> dialog = event.getSource();
		if (dialog.getContentComponent().validateEdit()) {
			SpecialityAutoExpRating specialityAutoExpRating = getCurrentSpecialityAutoExpRating();
			if (specialityAutoExpRating == null) {
				specialityAutoExpRating = new SpecialityAutoExpRating();
				specialityAutoExpRating.setDescription(SpecialtyAutoConstants.SPEC_TRANSACTIONDATA_EXPOSURE_RATING);
			}
			
			if (dialog.getContentComponent().saveEdit(specialityAutoExpRating)) {
				save(dialog, specialityAutoExpRating);
			}
		}
	}
	
	protected void save(DSUpdateDialog<?> dialog, SpecialityAutoExpRating specialityAutoExpRating) {
		(new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyExposureRatingProcessor processor = (BasePolicyExposureRatingProcessor) taskRun.getProcessor();
					dialog.close();
					policyUIHelper.reopenPolicy(processor.getPolicyTransaction(), UIConstants.POLICYTREENODES.Policy, null);
				}
				
				return true;
			}
				
		}).start(new BasePolicyExposureRatingProcessor(UIConstants.UIACTION_Save, policyUIHelper.getCurrentPolicyTransaction()).withParameter(specialityAutoExpRating));
	}
	
	
}
