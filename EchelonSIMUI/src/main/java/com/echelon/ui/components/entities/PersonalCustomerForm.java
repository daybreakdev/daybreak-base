package com.echelon.ui.components.entities;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.PersonalCustomer;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSPhoneField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;

public class PersonalCustomerForm extends CustomerForm<PersonalCustomer> {
	private static final long serialVersionUID = 2758884768806653013L;

	// PolicyHolder section
	private TextField givenName;
	private TextField middleName;
	private TextField familyName;
	private DSComboBox<String> gender;
	private DSPhoneField phoneNumber;
	private EmailField email;
	private DSComboBox<String> preferredLanguage;

	public PersonalCustomerForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	public void setFocus() {
		givenName.setAutofocus(true);
	}

	@Override
	protected CustomerUIHelper<PersonalCustomer> createCustomerUIHelper() {
		return new PersonalCustomerUIHelper();
	}

	@Override
	protected PersonalCustomer getCustomer(Customer customer) {
		return customer.getPersonalCustomer();
	}

	@Override
	protected void buildPolicyHolderFields() {
		super.buildPolicyHolderFields();
		givenName = new TextField(labels.getString("PersonalCustomerGivenNameLabel"));
		middleName = new TextField(labels.getString("PersonalCustomerMiddleNameLabel"));
		familyName = new TextField(labels.getString("PersonalCustomerFamilyNameLabel"));
		gender = new DSComboBox<String>(labels.getString("PersonalCustomerGenderLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_Genders);
		phoneNumber = new DSPhoneField(labels.getString("PersonalCustomerPhoneNumberLabel"));
		email = new EmailField(labels.getString("PersonalCustomerEmailLabel"));
		preferredLanguage = new DSComboBox<String>(labels.getString("CommercialCustomerLanguageLabel"), String.class)
				.withLookupTableId(ConfigConstants.LOOKUPTABLE_Languages);
	}

	@Override
	protected void bindCustomer() {
		customerBinder.forField(givenName).asRequired().bind(PersonalCustomer::getGivenName,
				PersonalCustomer::setGivenName);
		customerBinder.forField(middleName).bind(PersonalCustomer::getMiddleName, PersonalCustomer::setMiddleName);
		customerBinder.forField(familyName).asRequired().bind(PersonalCustomer::getFamilyName,
				PersonalCustomer::setFamilyName);
		customerBinder.forField(gender).asRequired().bind(PersonalCustomer::getGender, PersonalCustomer::setGender);
		customerBinder.forField(phoneNumber).bind(PersonalCustomer::getHomePhone, PersonalCustomer::setHomePhone);
		customerBinder.forField(email).bind(PersonalCustomer::getMainEmail, PersonalCustomer::setMainEmail);
		customerBinder.forField(preferredLanguage).asRequired().bind(
				customer -> customer.getCustomer().getPreferredLanguage(),
				(customer, lang) -> customer.getCustomer().setPreferredLanguage(lang));

		customerBinder.setReadOnly(true);
	}

	@Override
	protected FormLayout buildPolicyHolderLayout() {
		final FormLayout form = super.buildPolicyHolderLayout();

		final FormLayout nameRow = createFormLayout(5);
		nameRow.add(givenName, 2);
		nameRow.add(middleName, 1);
		nameRow.add(familyName, 2);
		form.add(nameRow, 4);

		form.add(gender, phoneNumber, email, preferredLanguage);

		return form;
	}
}
