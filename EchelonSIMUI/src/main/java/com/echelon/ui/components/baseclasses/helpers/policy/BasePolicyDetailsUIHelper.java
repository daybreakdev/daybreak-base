package com.echelon.ui.components.baseclasses.helpers.policy;

import java.io.Serializable;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.dto.PolicySearchCriteria;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyInformationForm;
import com.echelon.ui.components.baseclasses.policy.BasePolicyUpdateForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyDetailProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicySearchProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.vaadin.flow.component.UI;

public class BasePolicyDetailsUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8083715320540389140L;
	
	public interface PolicySearchCallback {
		public void resultIsReady(PolicySearchCriteria criteria);
	}
	
	public BasePolicyDetailsUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected BasePolicyUpdateForm createPolicyUpdateForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new BasePolicyUpdateForm(action, containerType);
	}
	
	protected DSUpdateDialog<BasePolicyUpdateForm> createPolicyUpdateDialog(String title, String action) {
		DSUpdateDialog dialog = (DSUpdateDialog<BasePolicyUpdateForm>) createDialog(title, 
															createPolicyUpdateForm(action, UICONTAINERTYPES.Dialog), 
															action);
		dialog.setWidth("500px");
		return dialog;
	}
	
	public void loadView(BasePolicyInformationForm policyUI) {
		PolicyTransaction policyTransaction = policyUIHelper.getCurrentPolicyTransaction();
		policyUI.loadData(policyTransaction);
	}

	
	public void setupPolicyUpdateAction() {
		DSUpdateDialog<BasePolicyUpdateForm> dialog = createPolicyUpdateDialog(uiHelper.getSessionLabels().getString("PolicyInfoUpdateTitle"), 
																				 UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(policyUIHelper.getCurrentPolicyTransaction()));
		dialog.getContentComponent().loadData(policyUIHelper.getCurrentPolicyTransaction());
		dialog.getContentComponent().enabledEdit(policyUIHelper.getCurrentPolicyTransaction());
		dialog.open();		
	}
	
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<BasePolicyUpdateForm> dialog = (DSUpdateDialog<BasePolicyUpdateForm>) event.getSource();
		PolicyTransaction polTransaction  = (PolicyTransaction) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit()) {
			
			(new TaskProcessor(UI.getCurrent(), dialog) {

				@Override
				protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
					BasePolicyDetailProcessor processor = (BasePolicyDetailProcessor) taskRun.getProcessor();
					if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
						dialog.close();
						policyUIHelper.reopenPolicy(processor.getPolicyTransaction(), POLICYTREENODES.Policy, null);
					}
				
					return true;
				}
				
			})
			.start(createUpdatePolicyProcessor((PolicyTransaction) dialog.getDataStore().getData1(), dialog));
		}
	}
	
	public void openPolicy(String productCd, String policyNumber, String quoteNumber, Integer termNumber, Integer versinNumber) {
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				BasePolicyDetailProcessor processor = (BasePolicyDetailProcessor) taskRun.getProcessor();
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					if (processor.getPolicyTransaction() != null) {
						policyUIHelper.openPolicy(processor.getPolicyTransaction());
					}
				}
				return true;
			}
			
		})
		.start(createSelectPolicyProcessor(productCd, policyNumber, quoteNumber, termNumber, versinNumber));
	}

	protected BasePolicyDetailProcessor createUpdatePolicyProcessor(PolicyTransaction polTransaction, DSUpdateDialog<BasePolicyUpdateForm> dialog) {
		return null;
	}
	
	public void policySearch(PolicySearchCriteria criteria, PolicySearchCallback callback) {
		(new TaskProcessor(UI.getCurrent()) {
			
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				BasePolicySearchProcessor processor = (BasePolicySearchProcessor) taskRun.getProcessor();
				callback.resultIsReady(processor.getCriteria());
				
				return true;
			}
			
		})
		.start(createPolicySearchProcessor(criteria));
	}
	
	protected BasePolicyDetailProcessor createSelectPolicyProcessor(String productCd, String policyNumber, String quoteNumber, Integer termNumber, Integer versinNumber) {
		return new BasePolicyDetailProcessor(UIConstants.UIACTION_Open, policyUIHelper.getCurrentPolicyTransaction(),
								policyNumber, quoteNumber, termNumber, versinNumber);
	}

	protected BasePolicySearchProcessor createPolicySearchProcessor(PolicySearchCriteria criteria) {
		return new BasePolicySearchProcessor(criteria);
	}
	
	public boolean validateExternalQuoteNumber(String extQuoteNumber, boolean forNewPolicy) {
		if (forNewPolicy ||
				BrokerUI.getUserSession() == null ||
				BrokerUI.getUserSession().getCurrentPolicy() == null ||
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() == null) {
			return validateExternalQuoteNumber(extQuoteNumber, null, null);
		}
		else {
			String quoteNumber = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getQuoteNum(); 
			String policyNumber = BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
			return validateExternalQuoteNumber(extQuoteNumber, quoteNumber, policyNumber);
		}
	}
	
	protected boolean validateExternalQuoteNumber(String extQuoteNumber, String quoteNumber, String policyNumber) {
		boolean isOk = true;
		
		if (StringUtils.isNotBlank(extQuoteNumber)) {
			BasePolicyDetailsUIHelper policyDetailsUIHelper = UIFactory.getInstance().getPolicyDetailsUIHelper();
			isOk = !policyDetailsUIHelper.searchTheSameExternalQuoteNumber(extQuoteNumber, quoteNumber, policyNumber);
		}
		
		return isOk;
	}
	
	protected boolean searchTheSameExternalQuoteNumber(String extQuoteNum, String quoteNumber, String policyNumber) {
		BasePolicySearchProcessor searchProcessor = new BasePolicySearchProcessor(extQuoteNum, quoteNumber, policyNumber);
		(new TaskProcessor()).manualStart(searchProcessor);
		return BooleanUtils.isTrue(searchProcessor.getFoundPolicy());
	}
	
}
