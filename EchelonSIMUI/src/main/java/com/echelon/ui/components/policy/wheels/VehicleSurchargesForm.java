package com.echelon.ui.components.policy.wheels;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.VehicleSurchargesUIHelper;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.binder.Binder;

public class VehicleSurchargesForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4522013197756844765L;
	private Label claimsSurchargeLabel;
	private Label minorConvictionSurchargeLabel;
	private Label majorConvictionSurchargeLabel;
	private Label criminalConvictionSurchargeLabel;
	private Label principalYoungDriverSurchargeLabel;
	private Label secondaryYoungDriverSurchargeLabel;
	private Label totalSurchargeLabel;

	private DSNumberField<Double> 			claimsSurchargeField;
	private DSNumberField<Double> 			claimsSurchargeOverrideField;
	private DSNumberField<Double> 			minorConvictionSurchargeField;
	private DSNumberField<Double> 			minorConvictionSurchargeOverrideField;
	private DSNumberField<Double> 			majorConvictionSurchargeField;
	private DSNumberField<Double> 			majorConvictionSurchargeOverrideField;
	private DSNumberField<Double> 			criminalConvictionSurchargeField;
	private DSNumberField<Double> 			criminalConvictionSurchargeOverrideField;
	private DSNumberField<Double> 			principleYoungDriverSurchargeField;
	private DSNumberField<Double> 			principleYoungDriverSurchargeOverrideField;
	private DSNumberField<Double> 			secondaryYoungDriverSurchargeField;
	private DSNumberField<Double> 			secondaryYoungDriverSurchargeOverrideField;
	private DSNumberField<Double> 			totalSurchargeField;
	private DSNumberField<Double> 			totalSurchargeOverrideField;
	private List<DSNumberField<Double>> 	calcSurchargeOverrideFieldList;
	
	private Binder<SpecialtyVehicleRisk> 	binder;
	private VehicleSurchargesUIHelper		surchargesUIHelper;
	private ActionToolbar					toolbar;
	private SpecialtyVehicleRisk			currVehicle;

	public VehicleSurchargesForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		
		this.surchargesUIHelper = new VehicleSurchargesUIHelper();
		
		if (!UICONTAINERTYPES.Dialog.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
														labels.getString("VehicleSurchargesFormTitle"),
														UIConstants.UICOMPONENTTYPES.Form);
			this.toolbar = (ActionToolbar) headComponents.getToolbar();
			buildButtonEvents();
		}
		
		buildFields();
		buildForm();
	}
	
	private void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
					surchargesUIHelper.editSurchargesAction(currVehicle);
				});
			}
		}		
	}

	protected void buildFields() {
		claimsSurchargeLabel						= new Label(labels.getString("VehicleSurchargesClaimsFormLabel"));
		minorConvictionSurchargeLabel				= new Label(labels.getString("VehicleSurchargesMinorConvictionsFormLabel"));
		majorConvictionSurchargeLabel				= new Label(labels.getString("VehicleSurchargesMajorConvictionsFormLabel"));
		criminalConvictionSurchargeLabel			= new Label(labels.getString("VehicleSurchargesCriminalConvictionsFormLabel"));
		principalYoungDriverSurchargeLabel			= new Label(labels.getString("VehicleSurchargesPrincipalYoungDriverFormLabel"));
		secondaryYoungDriverSurchargeLabel			= new Label(labels.getString("VehicleSurchargesSecondaryYoungDriverFormLabel"));
		totalSurchargeLabel							= new Label(labels.getString("VehicleSurchargesTotalFormLabel"));
		
		claimsSurchargeField 						= new DSNumberField<Double>("", Double.class, 2);
		claimsSurchargeOverrideField 				= new DSNumberField<Double>("", Double.class, 2);
		minorConvictionSurchargeField 				= new DSNumberField<Double>("", Double.class, 2);
		minorConvictionSurchargeOverrideField 		= new DSNumberField<Double>("", Double.class, 2);
		majorConvictionSurchargeField 				= new DSNumberField<Double>("", Double.class, 2);
		majorConvictionSurchargeOverrideField 		= new DSNumberField<Double>("", Double.class, 2);
		criminalConvictionSurchargeField 			= new DSNumberField<Double>("", Double.class, 2);
		criminalConvictionSurchargeOverrideField 	= new DSNumberField<Double>("", Double.class, 2);
		principleYoungDriverSurchargeField 			= new DSNumberField<Double>("", Double.class, 2);
		principleYoungDriverSurchargeOverrideField 	= new DSNumberField<Double>("", Double.class, 2);
		secondaryYoungDriverSurchargeField 			= new DSNumberField<Double>("", Double.class, 2);
		secondaryYoungDriverSurchargeOverrideField 	= new DSNumberField<Double>("", Double.class, 2);
		totalSurchargeField 						= new DSNumberField<Double>("", Double.class, 2);
		totalSurchargeOverrideField 				= new DSNumberField<Double>("", Double.class, 2);
		
		binder = new Binder<SpecialtyVehicleRisk>();
		
		binder.forField(claimsSurchargeField).withNullRepresentation("")
						.withConverter(claimsSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getClaimsSurcharge, null);
		binder.forField(claimsSurchargeOverrideField).withNullRepresentation("")
						.withConverter(claimsSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getClaimsSurchargeOverride, SpecialtyVehicleRisk::setClaimsSurchargeOverride);
		
		binder.forField(minorConvictionSurchargeField).withNullRepresentation("")
						.withConverter(minorConvictionSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getMinorConvictionSurcharge, null);
		binder.forField(minorConvictionSurchargeOverrideField).withNullRepresentation("")
						.withConverter(minorConvictionSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getMinorConvictionSurchargeOverride, SpecialtyVehicleRisk::setMinorConvictionSurchargeOverride);
		
		binder.forField(majorConvictionSurchargeField).withNullRepresentation("")
						.withConverter(majorConvictionSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getMajorConvictionSurcharge, null);
		binder.forField(majorConvictionSurchargeOverrideField).withNullRepresentation("")
						.withConverter(majorConvictionSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getMajorConvictionSurchargeOverride, SpecialtyVehicleRisk::setMajorConvictionSurchargeOverride);
		
		binder.forField(criminalConvictionSurchargeField).withNullRepresentation("")
						.withConverter(criminalConvictionSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getCriminalConvictionSurcharge, null);
		binder.forField(criminalConvictionSurchargeOverrideField).withNullRepresentation("")
						.withConverter(criminalConvictionSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getCriminalConvictionSurchargeOverride, SpecialtyVehicleRisk::setCriminalConvictionSurchargeOverride);
		
		binder.forField(principleYoungDriverSurchargeField).withNullRepresentation("")
						.withConverter(principleYoungDriverSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getPrincipleYoungDriverSurcharge, null);
		binder.forField(principleYoungDriverSurchargeOverrideField).withNullRepresentation("")
						.withConverter(principleYoungDriverSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getPrincipleYoungDriverSurchargeOverride, SpecialtyVehicleRisk::setPrincipleYoungDriverSurchargeOverride);
		
		binder.forField(secondaryYoungDriverSurchargeField).withNullRepresentation("")
						.withConverter(secondaryYoungDriverSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getSecondaryYoungDriverSurcharge, null);
		binder.forField(secondaryYoungDriverSurchargeOverrideField).withNullRepresentation("")
						.withConverter(secondaryYoungDriverSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getSecondaryYoungDriverSurchargeOverride, SpecialtyVehicleRisk::setSecondaryYoungDriverSurchargeOverride);
		
		binder.forField(totalSurchargeField).withNullRepresentation("")
						.withConverter(totalSurchargeField.getConverter())
						.bind(SpecialtyVehicleRisk::getTotalSurcharge, null);
		binder.forField(totalSurchargeOverrideField).withNullRepresentation("")
						.withConverter(totalSurchargeOverrideField.getConverter())
						.bind(SpecialtyVehicleRisk::getTotalSurchargeOverride, SpecialtyVehicleRisk::setTotalSurchargeOverride);
		binder.setReadOnly(true);
		
		calcSurchargeOverrideFieldList = Arrays.asList(claimsSurchargeOverrideField, minorConvictionSurchargeOverrideField, majorConvictionSurchargeOverrideField,
													criminalConvictionSurchargeOverrideField, principleYoungDriverSurchargeOverrideField, secondaryYoungDriverSurchargeOverrideField);
	}
	
	protected void buildForm() {
		FormLayout form = createFormLayout(3);
		
		form.add(claimsSurchargeLabel, claimsSurchargeField, claimsSurchargeOverrideField);
		form.add(minorConvictionSurchargeLabel, minorConvictionSurchargeField, minorConvictionSurchargeOverrideField);
		form.add(majorConvictionSurchargeLabel, majorConvictionSurchargeField, majorConvictionSurchargeOverrideField);
		form.add(criminalConvictionSurchargeLabel, criminalConvictionSurchargeField, criminalConvictionSurchargeOverrideField);
		form.add(principalYoungDriverSurchargeLabel, principleYoungDriverSurchargeField, principleYoungDriverSurchargeOverrideField);
		form.add(secondaryYoungDriverSurchargeLabel, secondaryYoungDriverSurchargeField, secondaryYoungDriverSurchargeOverrideField);
		form.add(totalSurchargeLabel, totalSurchargeField, totalSurchargeOverrideField);
		
		this.add(form);
	}	
	
	public void loadData(SpecialtyVehicleRisk vehicle) {
		this.currVehicle = vehicle;
		
		loadDataConfigs(binder);
		
		binder.readBean(vehicle);
		
		enableDataView(binder);
		refreshToolbar();
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		uiHelper.setEditFieldsReadonly(totalSurchargeOverrideField);
		
		setupFieldsEvents();
		
		setFocus();
	}
	
	public boolean saveEdit(SpecialtyVehicleRisk vehicle) {
		boolean ok1 = uiHelper.writeValues(binder, vehicle);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	private void refreshToolbar() {
		if (toolbar != null) {
			this.toolbar.refreshButtons(currVehicle, currVehicle);
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New).setEnabled(false);
				toolbar.getButton(UIConstants.UIACTION_New).setVisible(false);
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).setEnabled(false);
				toolbar.getButton(UIConstants.UIACTION_Delete).setVisible(false);
			}
		}
	}
	
	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
		claimsSurchargeOverrideField.focus();
	}
	
	private void setupFieldsEvents() {
		claimsSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
		minorConvictionSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
		majorConvictionSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
		criminalConvictionSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
		principleYoungDriverSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
		secondaryYoungDriverSurchargeOverrideField.addValueChangeListener(e -> calculateTotal());
	}
	
	private void calculateTotal() {
		Double tot = null;
		
		List<DSNumberField<Double>> fields = calcSurchargeOverrideFieldList.stream()
									.filter(o -> o.getModelValue() != null)
									.collect(Collectors.toList());
		if (!fields.isEmpty()) {
			tot = fields.stream().mapToDouble(DSNumberField::getModelValue).sum();
		}
		
		totalSurchargeOverrideField.setDSValue(tot);
	}
}
