package com.echelon.ui.components;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.processes.policy.PolicyProcessesFactory;
import com.echelon.prodconf.ConfigConstants;

@WebListener
public class BrokerUIStartupServlet implements ServletContextListener {
	private static final Logger	log = Logger.getLogger(BrokerUIStartupServlet.class.getName());

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		try {
			PolicyProcessesFactory.getInstance().newPolicyProcesses(ConfigConstants.PRODUCTCODE_TOWING)
						.findPolicies("-1", null, null, "-1");
		} catch (SpecialtyAutoPolicyException e) {
			log.log(Level.WARNING, "Failed to initialize hibernate components");
			e.printStackTrace();
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

}
