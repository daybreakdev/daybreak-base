package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

/**
 * Base class for Vehicle endorsement forms.
 */
public class VehicleEndorsementForm extends BaseCovAndEndorseForm {
	private static final long serialVersionUID = 5786432609272828084L;

	public VehicleEndorsementForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, OBJECTTYPES.Vehicle);
	}
}
