package com.echelon.ui.components.towing;

import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.MultiNewEntryForm;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyValueUIHelper;
import com.echelon.ui.session.UIFactory;

public class CargoSubPolicyValueMultiForm extends MultiNewEntryForm<CargoSubPolicyValueForm, CargoDetails> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6220650535093857865L;
	private CargoSubPolicyValueUIHelper cargoValueUIHelper = new CargoSubPolicyValueUIHelper();
	private CargoSubPolicy<?> parentSubPolicy;

	public CargoSubPolicyValueMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, 7);
	}

	@Override
	protected DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails> createMultiNewEntryDto() {
		DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails> newDto = super.createMultiNewEntryDto();
		newDto.setData(cargoValueUIHelper.createEmptyCargoDetails());
		
		CargoSubPolicyValueForm form = UIFactory.getInstance().getCargoSubPolicyValueForm(uiMode, uiContainerType, this);
		form.loadData(parentSubPolicy, newDto.getData());
		form.enableEdit();
		newDto.setForm(form);
		
		return newDto;
	}

	@Override
	protected boolean formValidateEdit(DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().validateEdit(false);
	}

	@Override
	protected boolean formSaveEdit(DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().save(dto.getData());
	}
	
	public void valueDescriptionSelectionChanged() {
		List<DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails>> list = getAllEntries();
		if (list != null && !list.isEmpty()) {
			list.stream().forEach(o -> o.getForm().refreshValueDescriptionSelection());
		}		
	}

	public void enableEdit() {
		super.enableEdit();
		this.initialzieRow();
	}

	public void loadData(CargoSubPolicy<?> parentSubPolicy) {
		this.parentSubPolicy = parentSubPolicy;
	}
}
