package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.prodconf.baseclasses.CoverageConfigDto;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.data.provider.ListDataProvider;

public class BaseCoveragePicker extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4487764285949525881L;
	private Grid<CoverageConfigDto>	coverageGrid = new Grid<CoverageConfigDto>();
	private IBusinessEntity			covParent;
	private BaseCoverageUIHelper  coverageUIHelper;	
	
	public BaseCoveragePicker(String uiMode, UICONTAINERTYPES uiContainerType, BaseCoverageUIHelper coverageUIHelper) {
		super(uiMode, uiContainerType);
		
		// TODO Auto-generated constructor stub
		this.coverageUIHelper = coverageUIHelper;
		initializeLayouts();
		buildGrid();
	}
	
	protected void initializeLayouts() {
		this.setSizeFull();
		coverageGrid.setSizeFull();
		
		Label label = new Label(labels.getString("CoveragePickerLabel"));
		this.add(label);
		this.setHorizontalComponentAlignment(Alignment.START, label);
	}
	
	protected void buildGrid() {
		uiHelper.setGridStyle(coverageGrid);
		coverageGrid.addComponentColumn(o -> createColumnCheckBox(o))
							.setHeader(createHeaderColumnCheckBox());
		coverageGrid.addColumn(o -> o.getCoverageConfig().getDescription())
							.setHeader(labels.getString("CoveragePickerCoverageLabel"))
							.setWidth("85%");
		
		coverageGrid.setSelectionMode(SelectionMode.NONE);
		coverageGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		this.add(coverageGrid);
	}
	
	protected Checkbox createHeaderColumnCheckBox() {
		Checkbox checkbox = new Checkbox();
		checkbox.addValueChangeListener(new HasValue.ValueChangeListener<ValueChangeEvent<?>>() {
			@Override
			public void valueChanged(ValueChangeEvent<?> event) {
				boolean isChecked = (event.getValue() != null && (Boolean)event.getValue());
				ListDataProvider<CoverageConfigDto> dataProvider = (ListDataProvider<CoverageConfigDto>) coverageGrid.getDataProvider();
				if (dataProvider.getItems() != null) {
					List<CoverageConfigDto> selection = dataProvider.getItems().stream().filter(o -> o.isSelected() != isChecked).collect(Collectors.toList());
					if (!selection.isEmpty()) {
						selection.stream().forEach(o -> o.setSelected(isChecked));
						
						if (isChecked) {
							coverageUIHelper.showWarnAndErrorOfCoverageSelction(covParent, isChecked, selection);
						}
					}
					coverageGrid.getDataProvider().refreshAll();
				}
			}
		});
		return checkbox;
	}
	
	protected Checkbox createColumnCheckBox(CoverageConfigDto dto) {
		Checkbox checkbox = new Checkbox();
		checkbox.setValue(dto.isSelected());
		
		checkbox.addValueChangeListener(new HasValue.ValueChangeListener<ValueChangeEvent<?>>() {
			@Override
			public void valueChanged(ValueChangeEvent<?> event) {
				boolean isChecked = (event.getValue() != null && (Boolean)event.getValue());
				dto.setSelected(isChecked);
				coverageUIHelper.showWarnAndErrorOfCoverageSelction(covParent, isChecked, Arrays.asList(dto));
			}
		});
		return checkbox;
	}

	public boolean loadData(IBusinessEntity	covParent, List<CoverageConfigDto> coverageConfigs) {
		this.covParent = covParent;
		
		if (coverageConfigs != null) {
			Collections.sort(coverageConfigs);
		}
		
		coverageGrid.setItems(coverageConfigs);
		return true;
	}
	
	public boolean validateSelection() {
		boolean ok = false;
		
		if (!getSelectedCoverageConfigs().isEmpty()) {
			ok = true;
		}
		else {
			uiHelper.notificationWarning(labels.getString("CoveragePickerNoSelectionError"));
		}
		
		return ok;
	}
	
	public List<CoverageConfigDto> getSelectedCoverageConfigs() {
		List<CoverageConfigDto> results = new ArrayList<CoverageConfigDto>();
		
		ListDataProvider<CoverageConfigDto> dataProvider = (ListDataProvider<CoverageConfigDto>) coverageGrid.getDataProvider();
		if (dataProvider.getItems() != null) {
			dataProvider.getItems().stream().forEach(o -> {
				if (o.isSelected()) {
					results.add(o);
				}
			});
		}
		
		return results;
	}
}
