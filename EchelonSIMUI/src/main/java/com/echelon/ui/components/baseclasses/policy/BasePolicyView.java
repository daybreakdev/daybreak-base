package com.echelon.ui.components.baseclasses.policy;

import java.util.ResourceBundle;

import com.ds.ins.uicommon.components.DSBaseTabLayout;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.echelon.ui.components.MainView;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.DefaultContentView;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.vaadin.flow.component.HasElement;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.selection.SelectionEvent;
import com.vaadin.flow.data.selection.SelectionListener;
import com.vaadin.flow.router.AfterNavigationEvent;
import com.vaadin.flow.router.AfterNavigationObserver;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.RouteConfiguration;
import com.vaadin.flow.router.RouterLayout;

@ParentLayout(value = MainView.class)
public class BasePolicyView extends BaseLayout implements AfterNavigationObserver, BeforeEnterObserver, RouterLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1789825626323758831L;
	private ResourceBundle				labels;
	private BasePolicyHeaderForm		headerForm			= new BasePolicyHeaderForm();
	private VerticalLayout				headerLayout		= new VerticalLayout();
	private SplitLayout					detailSplit			= new SplitLayout();
	private Label						treeTitle		 	= new Label();
	private VerticalLayout				treeTitleLayout  	= new VerticalLayout();
	private BasePolicyTreeNavigator 	treeNavigator;
	private VerticalLayout				treeLayout			= new VerticalLayout();
	private VerticalLayout				contentLayout		= new VerticalLayout();
	private PolicyUIHelper				policyUIHelper;
	
	public BasePolicyView() {
		super();
		// TODO Auto-generated constructor stub
		
		policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		
		buildLayouts();
	}

	private void buildLayouts() {
		initializeLayouts();
		
		labels = uiHelper.getSessionLabels();
		
		buildHeaderLayout();
		buildDetailSplit();
		
		headerLayout.setWidth("99%");
		detailSplit.setWidth("99%");
		this.add(headerLayout, detailSplit);
		this.setHorizontalComponentAlignment(Alignment.CENTER, headerLayout, detailSplit);
	}
	
	private void initializeLayouts() {
		this.setHeightFull();
		//this.setPadding(true);
		uiHelper.formatFullLayout(headerLayout);
		uiHelper.formatFullLayout(treeLayout);
		treeLayout.setHeightFull();
		//treeLayout.setPadding(true);
		uiHelper.setBorder(treeLayout);
		uiHelper.formatFullLayout(treeTitleLayout);
		treeTitleLayout.setPadding(true);
		uiHelper.formatFullLayout(contentLayout);
		contentLayout.setHeightFull();
		contentLayout.setPadding(true);
		uiHelper.setBorder(contentLayout);
		
		uiHelper.setLabelHeaderStyle(treeTitle);
	}
	
	private void buildHeaderLayout() {
		headerLayout.add(headerForm);
	}
	
	private void buildDetailSplit() {
		treeTitle.setText(labels.getString("PolicyViewTreeLabel"));
		treeTitleLayout.add(treeTitle);
				
		detailSplit.setSizeFull();
		detailSplit.addToPrimary(treeLayout);
		detailSplit.addToSecondary(contentLayout);
		detailSplit.setSplitterPosition(25);
	}
	
	@Override
	public void beforeEnter(BeforeEnterEvent event) {
		//uiHelper.handleBeforeEnter();
	}

	@Override
	public void afterNavigation(AfterNavigationEvent event) {
		// TODO Auto-generated method stub
		//warning - Called for every view changes
		try {
			resetPolicyView();
		} catch (Throwable e) {
			uiHelper.notificationException(e, new DSNotificatonUIHelper.NotificatonCallback() {
				
				@Override
				public void close() {
					//policyUIHelper.openPolicySearchView();
				}
			});
		}
	}
	
	private void resetPolicyView() {
		
		// This block of codes for opening a policy
		if (BrokerUI.getUserSession() != null) {
			if (BrokerUI.getUserSession().getPolicyView() == null) {
				PolicyVersion 	  	 policyVersion 	   = uiPolicyHelper.getCurrentPolicyVersion();
				PolicyTransaction<?> policyTransaction = uiPolicyHelper.getCurrentPolicyTransaction();
				
				uiPolicyHelper.refreshPolicyMenuItems();
				BrokerUI.getUserSession().setPolicyView(this);
				loadData(policyVersion, policyTransaction);
			}
		}
	}

	public void loadData(PolicyVersion policyVersion, PolicyTransaction policyTransaction) {
		headerForm.loadData(policyVersion);
		
		buildPolicyTree(policyVersion, policyTransaction);

		updateDetailSplitTree();
	}

	protected void updateDetailSplitTree() {
		//contentLayout.removeAll();
		treeLayout.removeAll();
		if (treeNavigator != null) {
			treeLayout.add(treeTitleLayout, treeNavigator.getTreeGrid());
			
			/* no need to preselect the root node Policy
			 * e.g. reopen vehicle when saving vehicle
			if (treeNavigator.getRootItems() != null && !treeNavigator.getRootItems().isEmpty()) {
				treeNavigator.getTreeGrid().select(treeNavigator.getRootItems().get(0));
			}
			*/
		}
	}	
	
	protected void updateDetailSplitInfo(DSTreeGridItem item) {
		if (item != null) {
			
			// Open different view when node type not the same
			if (/* does not work for vehicle coverage grid; list cound be shortened
				DSBrokerUI.getUserSession().getPolicyNodeItem() == null ||
				DSBrokerUI.getUserSession().getPolicyNodeContent() == null ||
				!item.getNodeType().equalsIgnoreCase(DSBrokerUI.getUserSession().getPolicyNodeItem().getNodeType())
				*/
				true) {
				
				Integer contentViewPage = null;
				boolean isSameNodeType  = false;
				if (BrokerUI.getUserSession().getPolicyNodeItem() == null ||
					BrokerUI.getUserSession().getPolicyNodeContent() == null ||
					item.getNodeType().equalsIgnoreCase(BrokerUI.getUserSession().getPolicyNodeItem().getNodeType())) {
					isSameNodeType = true;
					
					if (isSameNodeType &&
						BrokerUI.getUserSession().getPolicyNodeContent() != null && 
						BrokerUI.getUserSession().getPolicyNodeContent() instanceof DSBaseTabLayout) {
						DSBaseTabLayout tabLayout = (DSBaseTabLayout)BrokerUI.getUserSession().getPolicyNodeContent();
						contentViewPage = tabLayout.getCurrentPage();
					}
				}
				
				UI.getCurrent().navigate(UIConstants.CONTENTVIEW_DEFAULT);
				
				String routeName = item.getNodeType()+UIConstants.CONTENTVIEW_POSTFIX;;
				
				// check if Route defined; try Product specific version first
				boolean routeFound = false;
				if (uiPolicyHelper.getCurrentPolicyVersion() != null) {
					String productCd = uiPolicyHelper.getCurrentPolicyVersion().getInsurancePolicy().getProductCd();
					String productRouteName = productCd+routeName;
					if (findRouteByName(productRouteName)) {
						routeFound = true;
						routeName = productRouteName;
					}
				}
				
				if (!routeFound) {
					if (findRouteByName(routeName)) {
						routeFound = true;
					}
				}
				
				if (routeFound) {
					UI.getCurrent().navigate(routeName);					
					
					// Same type, open the same tab
					if (isSameNodeType && contentViewPage != null &&
						BrokerUI.getUserSession().getPolicyNodeContent() instanceof DSBaseTabLayout) {
						DSBaseTabLayout tabLayout = (DSBaseTabLayout)BrokerUI.getUserSession().getPolicyNodeContent();
						tabLayout.openPage(contentViewPage);
					}
				}
				else if (BrokerUI.getUserSession().getPolicyNodeContent() != null &&
						 BrokerUI.getUserSession().getPolicyNodeContent() instanceof DefaultContentView) {
					((DefaultContentView)BrokerUI.getUserSession().getPolicyNodeContent())
						.loadContentData(item);
					
					// Same type, open the same tab
					if (isSameNodeType && contentViewPage != null &&
						BrokerUI.getUserSession().getPolicyNodeContent() instanceof DSBaseTabLayout) {
						DSBaseTabLayout tabLayout = (DSBaseTabLayout)BrokerUI.getUserSession().getPolicyNodeContent();
						tabLayout.openPage(contentViewPage);
					}
				}
				else {
					//UI.getCurrent().navigate(DSUIConstants.CONTENTVIEW_DEFAULT);
				}
			}
			else {
				// Load data
				BrokerUI.getUserSession().getPolicyNodeContent().loadContentData(item);
			}
			
		}
	}
	
	protected boolean findRouteByName(String routeName) {
		if (RouteConfiguration.forSessionScope().getAvailableRoutes().stream()
				.filter(o -> routeName.equalsIgnoreCase(o.getUrl()))
				.findFirst().orElse(null) != null) {
			return true;
		}
		
		return false;
	}
	
	protected void buildPolicyTree(PolicyVersion policyVersion, PolicyTransaction policyTransaction) {
		treeNavigator = null;
		if (BrokerUI.getUserSession() != null) {
			treeNavigator = BrokerUI.getUserSession().getPolicyTreeNavigator();
		}
		
		if (treeNavigator == null) {
			uiHelper.notificationError(labels.getString("ErrorPolicyViewTreeNavigatorNotAvailable"));
			return;
		}
		
		treeNavigator.loadData(policyVersion, policyTransaction);
		TreeGrid<DSTreeGridItem> treeGrid = treeNavigator.getTreeGrid();
		treeGrid.addSelectionListener(new SelectionListener<Grid<DSTreeGridItem>, DSTreeGridItem>() {
			
			@Override
			public void selectionChange(SelectionEvent<Grid<DSTreeGridItem>, DSTreeGridItem> event) {
				DSTreeGridItem item = event.getFirstSelectedItem().orElse(null);
				if (item == null || !item.isManualSelection()) {
					updateDetailSplitInfo(item);
				}
			}
		});		
		treeGrid.addCollapseListener(event -> {
				if (event.getItems().size() > 0) {
					DSTreeGridItem item = event.getItems().iterator().next();
					treeGrid.select(item);
				}
		});
		treeGrid.addExpandListener(event -> {
			if (event.getItems().size() > 0) {
				DSTreeGridItem item = event.getItems().iterator().next();
				treeGrid.select(item);
			}
	});
	}
	
	public void selectTreeNode(String itemNodeType, Object itemNodeObject) {
		DSTreeGridItem item = treeNavigator.searchTreeItem(itemNodeType, itemNodeObject);
		if (item != null) {
			item.setManualSelection(true);
			try {
				treeNavigator.getTreeGrid().select(item);
				updateDetailSplitInfo(item);
			} catch (Exception e) {
				
			} finally {
				item.setManualSelection(false);
			}
		}
	}

	@Override
	public void showRouterLayoutContent(HasElement content) {
		// TODO Auto-generated method stub
		if (uiHelper.handleBeforeEnter()) {
			contentLayout.removeAll();
			
			if (content instanceof IPolicyViewContent) {			
				contentLayout.getElement().appendChild(content.getElement());
				
				DSTreeGridItem treeItem = null;
				if (treeNavigator != null &&
					treeNavigator.getTreeGrid().getSelectedItems() != null && 
					!treeNavigator.getTreeGrid().getSelectedItems().isEmpty()) {
					treeItem = treeNavigator.getTreeGrid().getSelectedItems().iterator().next();
				}
				
				BrokerUI.getUserSession().setPolicyNodeItem(treeItem);
				BrokerUI.getUserSession().setPolicyNodeContent(((IPolicyViewContent)content));
				BrokerUI.getUserSession().getPolicyNodeContent().loadContentData(treeItem);
			}
		}
	}
	
}
