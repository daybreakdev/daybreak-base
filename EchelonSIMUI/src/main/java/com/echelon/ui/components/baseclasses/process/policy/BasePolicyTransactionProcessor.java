package com.echelon.ui.components.baseclasses.process.policy;

import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.conversion.DataFixesProcesses;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyTransactionProcessor extends BasePolicyProcessor {

	private List<String> 					processingResults;
	@SuppressWarnings("rawtypes")
	private IGenericPolicyProcessing		processPolicyProcessing;
	private Boolean							processResult;
	private List<String>					warningMessageList;
	private boolean 						isReinsurable;
	private ReinsuranceReportDTO			reinsuranceReportDTO;
	
	@SuppressWarnings("rawtypes")
	public BasePolicyTransactionProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	public BasePolicyTransactionProcessor(String processAction, IGenericPolicyProcessing policyProcessing) {
		super(processAction, null);
		
		this.processPolicyProcessing = policyProcessing;
	}
	
	public BasePolicyTransactionProcessor withReinsurable(boolean isReinsurable) {
		this.isReinsurable = isReinsurable;
		return this;
	}

	public List<String> getProcessingResults() {
		return processingResults;
	}

	public Boolean getProcessResult() {
		return processResult;
	}

	public List<String> getWarningMessageList() {
		return warningMessageList;
	}

	public ReinsuranceReportDTO getReinsuranceReportDTO() {
		return reinsuranceReportDTO;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_RunDataFixes:
			try {
				//processingResults = processPolicyProcessing.rateAllPolicyTransactions(getUserProfile());
				//processResult = (new DataFixesProcesses()).runMidtermChangeDataFixes(processPolicyProcessing);
				processResult = (new DataFixesProcesses()).runMailingAddressLocationFixes((ISpecialtyAutoPolicyProcessing)processPolicyProcessing);
			} catch (Exception e) {
				setProcessException(e);
			}
			
			break;

		case UIConstants.UIACTION_Open:
			loadPolicyTransaction();
			break;
			
		case UIConstants.UIACTION_LoadData:
			try {
				policyTransaction = loadPolicyTransactionData(policyTransaction);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
			}
			break;
			
		case UIConstants.UIACTION_LoadFull:
			if (isReinsurable) {
				try {
					reinsuranceReportDTO = getReinsuranceReport(policyTransaction);
				} catch (SpecialtyAutoPolicyException e) {
					setProcessException(e);
				}
			}
			try {
				policyTransaction = loadFullPolicyTransactionData(policyTransaction);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
			}
			break;
			
		default:
			break;
		}
	}

	protected ReinsuranceReportDTO getReinsuranceReport(PolicyTransaction<?> polTransaction) throws SpecialtyAutoPolicyException {
		ReinsuranceReportDTO result = ((ISpecialtyAutoPolicyProcessing) policyProcessing)
				.getReinsuranceReportDTO(polTransaction);
		return result;
	}
}
