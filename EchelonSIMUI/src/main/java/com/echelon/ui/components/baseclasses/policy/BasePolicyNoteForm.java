package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.DSTextArea;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyNotes;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyNoteForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2035159981784693602L;
	private TextField			subjectField;
	private DSTextArea			detailField;
	private Binder<PolicyNotes>	binder;

	public BasePolicyNoteForm() {
		super();
		// TODO Auto-generated constructor stub
		
		buildLayout();
		buildFields();
		buildForms();
	}

	public BasePolicyNoteForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildLayout();
		buildFields();
		buildForms();
	}

	protected void buildLayout() {
		
	}
	
	protected void buildFields() {
		subjectField 	= new TextField(labels.getString("PolicyNoteFormSubjectLabel"));
		detailField		= new DSTextArea(labels.getString("PolicyNoteFormDetailLabel"), 2000);
		
		detailField.setHeight("300px");
		
		binder = new Binder<PolicyNotes>(PolicyNotes.class);
		binder.forField(subjectField).asRequired().bind(PolicyNotes::getDescription, PolicyNotes::setDescription);
		binder.forField(detailField).asRequired().bind(PolicyNotes::getNoteDetails, PolicyNotes::setNoteDetails);
		binder.setReadOnly(true);
	}
	
	protected void buildForms() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 1)
		        );
		
		form.add(subjectField, detailField);
		
		this.add(form);
	}
	
	public void loadData(PolicyNotes policyNotes) {
		binder.readBean(policyNotes);
		
		enableDataView(binder);
	}
	
	public void enableEdit(PolicyNotes policyNotes) {
		enableDataEdit(binder);
		
		setFocus();
	}

	public boolean saveEdit(PolicyNotes policyNotes) {
		boolean ok1 = uiHelper.writeValues(binder, policyNotes);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid1 = uiHelper.validateValues(binder);
		
		return valid1;
	}
	
	public void setFocus() {
		subjectField.focus();
	}
	
	public void setViewMode() {
		uiHelper.setEditFieldsReadonly(binder);
	}
}
