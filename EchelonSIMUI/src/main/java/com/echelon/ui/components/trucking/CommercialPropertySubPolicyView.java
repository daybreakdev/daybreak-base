package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.ui.components.towing.TowingSubPolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyUIHelper;

public class CommercialPropertySubPolicyView extends TowingSubPolicyView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7242246852381823588L;

	private CommercialPropertySubPolicyUIHelper cpHelper = new CommercialPropertySubPolicyUIHelper();
	private CommercialPropertySubPolicyForm cpSubPolicyForm;

	public CommercialPropertySubPolicyView(String uiMode,
		UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeSubPolicyView(null);
		buildComponents();
	}

	private void buildComponents() {
		cpSubPolicyForm = new CommercialPropertySubPolicyForm(uiMode,
			uiContainerType);
		this.add(cpSubPolicyForm);
	}

	public boolean loadData(CommercialPropertySubPolicy<?> subPolicy) {
		boolean selectedSubPolicy = subPolicy != null;
		cpSubPolicyForm.loadData(subPolicy);

		// Don't need the update button
		refreshToolbar(subPolicy, true, false, true);
		return true;
	}

	@Override
	protected void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_New) != null) {
				toolbar.getButton(UIConstants.UIACTION_New)
					.addClickListener(e -> cpHelper.newSubPolicyAction());
			}
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update)
					.addClickListener(e -> cpHelper.updateSubPolicyAction());
			}
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete)
					.addClickListener(e -> cpHelper.deleteSubPolicyAction());
			}
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete)
					.addClickListener(e -> cpHelper.undeleteSubPolicyAction());
			}
		}
	}
}
