package com.echelon.ui.components.baseclasses.helpers.policy;

import java.util.List;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;

public class BasePolicyCoverageUIHelper extends BaseCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3167104847508769047L;

	public BasePolicyCoverageUIHelper(OBJECTTYPES parentObjectType, POLICYTREENODES policyTreeNode) {
		super(parentObjectType, policyTreeNode);
	}

	public void loadView(BaseCoverageList component, PolicyTransaction policyTransaction) {
		IGenericProductConfig productConfig = configUIHelper.getCurrentProductConfig();
		
		List<CoverageDto> coverageDtoList = buildFullCoverageList((ICoverageConfigParent) productConfig, policyTransaction);
		component.loadData((ICoverageConfigParent) productConfig, policyTransaction, coverageDtoList);
	}

	@Override
	public List<TotalPremiumDto> createTotalPremiumDtos(IBusinessEntity covParent) {
		PolicyTransaction policyTransaction = (PolicyTransaction) covParent;
		List<TotalPremiumDto> dtos = super.createSingleTotalPremium(covParent, policyTransaction.getTransactionPremium(), policyTransaction.getCoverageDetail());
		return dtos;
	}
}
