package com.echelon.ui.components.policy.wheels;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverConvictionUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverConviction;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class DriverConvictionForm extends BaseLayout implements IDSFocusAble {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4206806551187533705L;
	protected DatePicker					MVRDateField;
	protected DatePicker					convictionDateField;
	protected DSComboBox<String>			convictionTypeField;
	protected DSComboBox<String>			convictionCodeField;
	
	protected DriverConvictionUIHelper		convictionUIHelper;
	protected Binder<DriverConviction>		binder;

	public DriverConvictionForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.convictionUIHelper = new DriverConvictionUIHelper();
		this.setSizeFull();
		
		buildFields();
		buildForm();
	}
	

	protected void buildFields() {
		MVRDateField 					= new DatePicker(labels.getString("DriverConvictionFormMVRDate"));
		convictionDateField				= new DatePicker(labels.getString("DriverConvictionFormConvDate"));
		convictionTypeField				= new DSComboBox<String>(labels.getString("DriverConvictionFormConvType"), String.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_ConvictionType);
		convictionCodeField				= new DSComboBox<String>(labels.getString("DriverConvictionFormConvCode"), String.class)
												.withLookupTableId(ConfigConstants.LOOKUPTABLE_ConvictionCode)
												.withIncludeCodeItemLabel(true);
		
		binder = new Binder<DriverConviction>(DriverConviction.class);
		binder.forField(MVRDateField).asRequired().withConverter(new DSLocaldateTimeToLocalDateConverter())
											.bind(DriverConviction::getMvrDate, DriverConviction::setMvrDate);
		binder.forField(convictionDateField).asRequired()
											.bind(DriverConviction::getConvictionDate, DriverConviction::setConvictionDate);
		binder.forField(convictionTypeField).asRequired()
											.bind(DriverConviction::getConvictionType, DriverConviction::setConvictionType);
		binder.forField(convictionCodeField).asRequired()
											.bind(DriverConviction::getConvictionCode, DriverConviction::setConvictionCode);
		
		binder.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout convictionForm = new FormLayout();
		
		convictionForm.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );
		
		convictionForm.add(MVRDateField, convictionDateField, convictionTypeField);
		convictionForm.add(convictionCodeField, 2);
		uiHelper.addFormEmptyColumn(convictionForm);
		uiHelper.addFormEmptyColumn(convictionForm);
		
		this.add(convictionForm);
		
	}
		
	public void loadData(Driver driver, DriverConviction conviction) {
		loadDataConfigs(binder);
				
		binder.readBean(conviction);
		
		enableDataView(binder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		convictionTypeValueChanged(null);
		
		setupFieldValueChanges();
		setFocus();
	}
	
	public boolean saveEdit(DriverConviction conviction) {
		boolean ok1 = uiHelper.writeValues(binder, conviction);
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}

	@Override
	public void setFocus() {
		// TODO Auto-generated method stub
	}

	protected void setupFieldValueChanges() {
		convictionTypeField.addValueChangeListener(this::convictionTypeValueChanged);
	}

	private void convictionTypeValueChanged(ValueChangeEvent<?> event) {
		convictionCodeField.loadDSItems(convictionTypeField.getValue());
	}

}
