package com.echelon.ui.components.nonfleet;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.policy.wheels.VehicleDriverClaimList;
import com.echelon.ui.components.policy.wheels.VehicleDriverList;
import com.echelon.ui.components.towing.VehicleContentView;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="NONFLEETVehicleContentView", layout = BasePolicyView.class)
public class NonFleetVehicleContentView extends VehicleContentView {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2793981066483763778L;
	private VehicleDriverList 		vehicleDriverList;
	private VehicleDriverClaimList	vehicleDriverClaimList;
	
	public NonFleetVehicleContentView() {
		super();
		// TODO Auto-generated constructor stub
	}

	public NonFleetVehicleContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
                labels.getString("VehicleTabLabel"),
                labels.getString("CoverageTabLabel"),
                labels.getString("VehicleDriverTabLabel"),
                labels.getString("VehicleDriverClaimTabLabel")
				};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {getVehicleForm(), getVehicleCoverageList(), getVehicleDriverList(), getVehicleDriverClaimList()};
	}

	private Component getVehicleDriverList() {
		if (vehicleDriverList == null) {
			vehicleDriverList = new VehicleDriverList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return vehicleDriverList;
	}
	
	private Component getVehicleDriverClaimList() {
		if (vehicleDriverClaimList == null) {
			vehicleDriverClaimList = new VehicleDriverClaimList(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return vehicleDriverClaimList;
	}


	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		super.loadContentData(nodeItem);

		SpecialtyVehicleRisk vehicleRisk = (SpecialtyVehicleRisk)nodeItem.getNodeObject();

		if (vehicleDriverList != null) {
			vehicleDriverList.loadData(vehicleRisk, vehicleRisk.getVehicleDrivers());
		}
		
		if (vehicleDriverClaimList != null) {
			vehicleDriverClaimList.loadData(vehicleRisk, vehicleRisk.getVehicleDriverClaims());
		}
	}

}
