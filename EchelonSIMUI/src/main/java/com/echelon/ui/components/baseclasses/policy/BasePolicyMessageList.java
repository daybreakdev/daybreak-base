package com.echelon.ui.components.baseclasses.policy;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLabelRenderder;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.renderers.PolicyMessageLabelRenderder;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.domain.policy.PolicyMessage;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.treegrid.TreeGrid;
import com.vaadin.flow.data.provider.hierarchy.AbstractBackEndHierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalDataProvider;
import com.vaadin.flow.data.provider.hierarchy.HierarchicalQuery;
import com.vaadin.flow.router.Route;

@Route(value="MessagesContentView", layout = BasePolicyView.class)
public class BasePolicyMessageList extends BaseLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3714891823315411037L;
	protected TreeGrid<PolicyMessage> messagesGrid;
	protected PolicyMessageLabelRenderder<PolicyMessage> messageLabelRenderder;
	
	public BasePolicyMessageList() {
		super();
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
	}

	public BasePolicyMessageList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
	}
	
	protected void initializeLayouts() {
		this.setSizeFull();
		
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
													labels.getString("PolicyMessageListTitle"),
													null);
		}
		
		buildGrid();
	}

	protected void buildGrid() {
		messageLabelRenderder = new PolicyMessageLabelRenderder<PolicyMessage>(PolicyMessage::getDescription, "PolicyMessage", null);
		
		messagesGrid = new TreeGrid<PolicyMessage>();
		messagesGrid.setSizeFull();
		uiHelper.setGridStyle(messagesGrid);
		
		// setSortable not working for lazy loading
		messagesGrid.addHierarchyColumn(o -> new DSLabelRenderder<>(PolicyMessage::getSeverity, "PolicyMessageSeverity", null).getFormattedLabelValue(o.getSeverity()))
								.setHeader(labels.getString("PolicyMessageListColumnSeverityLabel"))
								.setWidth("5%")
								//.setSortable(true)
								;
		messagesGrid.addColumn(messageLabelRenderder)
								.setHeader(labels.getString("PolicyMessageListColumnMessageLabel"))
								.setWidth("70%")
								//.setSortable(true)
								;
		messagesGrid.addColumn(PolicyMessage::getRuleId).setHeader(labels.getString("PolicyMessageListColumnRuleIDLabel"))
								//.setSortable(true)
								;
		
		messagesGrid.setSelectionMode(SelectionMode.SINGLE);
		messagesGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		this.add(messagesGrid);
	}
	
	public void loadData() {
		List<PolicyMessage> list = new ArrayList<PolicyMessage>();
		
		if (uiPolicyHelper.getCurrentPolicyTransaction() != null &&
				uiPolicyHelper.getCurrentPolicyTransaction().getPolicyMessages() != null) {
			list.addAll(uiPolicyHelper.getCurrentPolicyTransaction().getPolicyMessages());
			
			int idx = 1;
			for(PolicyMessage msg : list) {
				msg.setDescription("MESSAGE"+(idx++));
			}
		}
		
		messageLabelRenderder.setMessages(list);
		
		HierarchicalDataProvider<PolicyMessage, Void> dataProvider =
		        new AbstractBackEndHierarchicalDataProvider<PolicyMessage, Void>() {

					@Override
					public int getChildCount(HierarchicalQuery<PolicyMessage, Void> query) {
						PolicyMessage policyMessage = query.getParent();
						if (policyMessage == null) {
							return list.size();
						}
						
						return (policyMessage.getRelatedEntities() != null && !policyMessage.getRelatedEntities().isEmpty()
								? 1
							    : 0);
					}

					@Override
					public boolean hasChildren(PolicyMessage item) {
						// TODO Auto-generated method stub
						return item.getRelatedEntities() != null && !item.getRelatedEntities().isEmpty();
					}

					@Override
					protected Stream<PolicyMessage> fetchChildrenFromBackEnd(
							HierarchicalQuery<PolicyMessage, Void> query) {
						if (query.getParent() == null) {
							return list.stream();
						}
						
						List<PolicyMessage> entityMessages = new ArrayList<PolicyMessage>();
						PolicyMessage policyMessage = query.getParent();
						if (policyMessage.getRelatedEntities() != null && !policyMessage.getRelatedEntities().isEmpty()) {
							String vehsLabel = (new VehicleUIHelper()).buildVehiclesLabel(policyMessage.getRelatedEntities());
							if (vehsLabel != null && vehsLabel.trim().length() > 0) {
								PolicyMessage entityMessage = new PolicyMessage();
								entityMessage.setDescription(vehsLabel);
								entityMessages.add(entityMessage);
							}
						}
						return entityMessages.stream();
					}
			
		};
		
		messagesGrid.setDataProvider(dataProvider);
		messagesGrid.expand(list);
	}
	
	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		// TODO Auto-generated method stub
		loadData();
	}

}
