package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageReinsurance;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.EndorsementReinsurance;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.CoverageReinsuranceDto;

public class BaseCoverageReinsuranceProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4360143790298371314L;

	public BaseCoverageReinsuranceProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	public CoverageDto preSaveCoverage(CoverageDto coverageDto) {
		
		if (BooleanUtils.isTrue(coverageDto.isReinsured())) {
			saveIsReinsured(coverageDto);
		}
		
		// Maybe change to not reinsured
		else {
			saveIsNotReinsured(coverageDto);
		}
		
		return coverageDto;
	}
	
	protected void saveIsReinsured(CoverageDto coverageDto) {
		if (coverageDto.getReinsuranceDtos() != null && !coverageDto.getReinsuranceDtos().isEmpty()) {
			for(CoverageReinsuranceDto coverageReinsuranceDto : coverageDto.getReinsuranceDtos()) {
				// is removed ? 
				if (coverageReinsuranceDto.isDeleted()) {
					if (!coverageReinsuranceDto.isNew()) {
						if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
							Coverage coverage = (Coverage) coverageDto.getCoverage();
							coverage.removeCoverageReinsurance(coverageReinsuranceDto.getCovReinsurance());
						}
						else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
							Endorsement endorsement = (Endorsement) coverageDto.getCoverage();
							endorsement.removeEndorsementReinsurance(coverageReinsuranceDto.getEndorsementReinsurance());
						}
					}
				}
				else if (coverageReinsuranceDto.isNew()) {
					if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Coverage coverage = (Coverage) coverageDto.getCoverage();
						coverage.addCoverageReinsurance(coverageReinsuranceDto.getCovReinsurance());
					}
					else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Endorsement endorsement = (Endorsement) coverageDto.getCoverage();
						endorsement.addEndorsementReinsurance(coverageReinsuranceDto.getEndorsementReinsurance());
					}
				}
				else if (coverageReinsuranceDto.isUndeleted()) {
					if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Coverage coverage = (Coverage) coverageDto.getCoverage();
						coverage.undeleteCoverageReinsurance(coverageReinsuranceDto.getCovReinsurance());
					}
					else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Endorsement endorsement = (Endorsement) coverageDto.getCoverage();
						endorsement.undeleteEndorsementReinsurance(coverageReinsuranceDto.getEndorsementReinsurance());
					}
				}
				else if (coverageReinsuranceDto.isUpdated()) {
				}
			}
		}
	}
	
	protected void saveIsNotReinsured(CoverageDto coverageDto) {
		if (coverageDto.getReinsuranceDtos() != null && !coverageDto.getReinsuranceDtos().isEmpty()) {
			for(CoverageReinsuranceDto coverageReinsuranceDto : coverageDto.getReinsuranceDtos()) {
				if (!coverageReinsuranceDto.isNew()) {
					if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Coverage coverage = (Coverage) coverageDto.getCoverage();
						coverage.removeCoverageReinsurance(coverageReinsuranceDto.getCovReinsurance());
					}
					else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
						Endorsement endorsement = (Endorsement) coverageDto.getCoverage();
						endorsement.removeEndorsementReinsurance(coverageReinsuranceDto.getEndorsementReinsurance());
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public CoverageReinsurance createEmptyCoverageReinsurance(CoverageDto coverageDto) throws InsurancePolicyException {
		return policyProcessing.createNewCoverageReinsurance(policyTransaction, (PolicyReinsurer)null, (Coverage) coverageDto.getCoverage());
	}
	
	@SuppressWarnings("unchecked")
	public EndorsementReinsurance createEmptyEndorsementReinsurance(CoverageDto coverageDto) throws InsurancePolicyException {
		return policyProcessing.createNewEndorsementReinsurance(policyTransaction, (PolicyReinsurer)null, (Endorsement) coverageDto.getCoverage());
	}
}
