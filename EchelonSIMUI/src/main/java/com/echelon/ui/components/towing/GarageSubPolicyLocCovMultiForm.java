package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants;

public class GarageSubPolicyLocCovMultiForm extends SubPolicyLocCovMultiForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7810049949825217004L;
	
	public GarageSubPolicyLocCovMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, UIConstants.OBJECTTYPES.GARSubPolicy, true);   
	}
	
	@Override
	protected IBaseCovAndEndorseForm createCoverageForm() {
		return new GarageSubPolicyLocCoverageForm(uiMode, uiContainerType);
	}

}
