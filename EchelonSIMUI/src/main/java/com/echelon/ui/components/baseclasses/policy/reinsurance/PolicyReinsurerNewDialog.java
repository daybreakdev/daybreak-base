package com.echelon.ui.components.baseclasses.policy.reinsurance;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.ds.ins.domain.entities.Reinsurer;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.ui.constants.UIConstants;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.checkbox.CheckboxGroup;
import com.vaadin.flow.component.checkbox.CheckboxGroupVariant;

/**
 * Dialog for selecting reinsurers to add to the policy.
 */
public class PolicyReinsurerNewDialog extends DSUpdateDialog<CheckboxGroup<Reinsurer>> {
	private static final long serialVersionUID = 9156368184264895970L;

	private CheckboxGroup<Reinsurer> checkboxGroup;

	/**
	 * Initializes a dialog allowing the user to select reinsurers that should be
	 * added to the policy.
	 * 
	 * @param title               the dialog title.
	 * @param availableReinsurers a list of available reinsurers.
	 */
	public PolicyReinsurerNewDialog(String title, Collection<Reinsurer> availableReinsurers) {
		super();

		List<Reinsurer> reinsurers = availableReinsurers.stream()
			.sorted(Comparator.comparing(reinsurer -> reinsurer.getLegalName(), Comparator.nullsLast(Comparator.naturalOrder())))
			.collect(Collectors.toList());

		checkboxGroup = new CheckboxGroup<>();
		checkboxGroup.addThemeVariants(CheckboxGroupVariant.LUMO_VERTICAL);
		checkboxGroup.setItems(reinsurers);
		checkboxGroup.setItemLabelGenerator(Reinsurer::getLegalName);
		checkboxGroup.addValueChangeListener(this::valueChanged);

		initDialog(title, UIConstants.UIACTION_New, checkboxGroup, UIConstants.UIACTION_Save,
			UIConstants.UIACTION_Cancel);

		// Save button is disabled until values are selected
		this.getButton(UIConstants.UIACTION_Save).setEnabled(false);
	}

	/**
	 * Retrieves the reinsurers that are to be added to the policy.
	 * 
	 * @return the selected reinsurers.
	 */
	public Set<Reinsurer> getSelectedReinsurers() {
		return checkboxGroup.getSelectedItems();
	}

	/**
	 * Listen for changes in the selected checkboxes. Disables the save button when
	 * no reinsurers are selected.
	 * 
	 * @param event the value change event.
	 */
	private void valueChanged(
		ComponentValueChangeEvent<CheckboxGroup<Reinsurer>, Set<Reinsurer>> event) {
		getButton(UIConstants.UIACTION_Save)
			.setEnabled(!checkboxGroup.getSelectedItems().isEmpty());
	}
}
