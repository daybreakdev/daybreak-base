package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.constants.UIConstants;

public class CGLSubPolicyLocCovMultiForm extends SubPolicyLocCovMultiForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2887869756909275207L;
	
	public CGLSubPolicyLocCovMultiForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType, UIConstants.OBJECTTYPES.CGLSubPolicy, false);
	}
	
	@Override
	protected IBaseCovAndEndorseForm createCoverageForm() {
		return new CGLSubPolicyCoverageForm(uiMode, uiContainerType);
	}

}
