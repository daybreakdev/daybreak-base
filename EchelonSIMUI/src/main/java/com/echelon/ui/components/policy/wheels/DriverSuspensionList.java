package com.echelon.ui.components.policy.wheels;

import java.util.List;
import java.util.Set;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.wheels.DriverSuspensionUIHelper;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverSuspension;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public class DriverSuspensionList extends BaseListLayout<DriverSuspension> {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4596447800780978467L;
	private DriverSuspensionUIHelper 		suspensionUIHelper = new DriverSuspensionUIHelper();	
	private Grid<DriverSuspension> 			suspensionGrid;
	private Driver							driver;

	public DriverSuspensionList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}
	
	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("DriverSuspensionListTitle"
																+ ""),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
	}
	
	private void buildGrid() {
		suspensionGrid = new Grid<DriverSuspension>();
		suspensionGrid.setSizeFull();
		uiHelper.setGridStyle(suspensionGrid);
		
		createIndicatorColumn(suspensionGrid);
		suspensionGrid.addColumn(new DSLookupItemRenderer<DriverSuspension>(DriverSuspension::getSuspensionType) 
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_SuspensionType))
								.setHeader(labels.getString("DriverSuspensionListColumnType"))
								.setAutoWidth(true);
		suspensionGrid.addColumn(new DSLookupItemRenderer<DriverSuspension>(DriverSuspension::getSuspensionReason)
								.setLookupConfigTableId(ConfigConstants.LOOKUPTABLE_SuspensionReason))
								.setHeader(labels.getString("DriverSuspensionListColumnReason"))
								.setAutoWidth(true);
		suspensionGrid.addColumn(DriverSuspension::getSuspensionStartDate)
								.setHeader(labels.getString("DriverSuspensionListColumnStartDate"))
								.setAutoWidth(true);
		suspensionGrid.addColumn(DriverSuspension::getSuspensionEndDate)
								.setHeader(labels.getString("DriverSuspensionListColumnEndDate"))
								.setAutoWidth(true);
		suspensionGrid.addColumn(suspension -> suspensionUIHelper.getSuspensionLength(suspension.getSuspensionStartDate(), 
																						suspension.getSuspensionEndDate()))
								.setHeader(labels.getString("DriverSuspensionListColumnLength"))
								.setAutoWidth(true);
		suspensionGrid.setSelectionMode(SelectionMode.SINGLE);
		suspensionGrid.addSelectionListener(event -> refreshToolbar());
		suspensionGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		
		setUpGridDbClickEvent(suspensionGrid);
		
		this.add(suspensionGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				suspensionUIHelper.newSuspensionAction(driver);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (suspensionGrid.asSingleSelect() != null) {
					suspensionUIHelper.updateSuspensionAction(driver, suspensionGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (suspensionGrid.asSingleSelect() != null) {
					suspensionUIHelper.deleteSuspensionAction(driver, suspensionGrid.asSingleSelect().getValue());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (suspensionGrid.asSingleSelect() != null) {
					suspensionUIHelper.undeleteSuspensionAction(driver, suspensionGrid.asSingleSelect().getValue());
				}
			});
		}
	}

	public boolean loadData(Driver driver, Set<DriverSuspension> set) {
		this.driver = driver;
		List<DriverSuspension> list = suspensionUIHelper.sortSuspensionList(set);

		this.suspensionGrid.setItems(list);
		refreshToolbar();
		
		return true;
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(driver, suspensionGrid);
	}

}