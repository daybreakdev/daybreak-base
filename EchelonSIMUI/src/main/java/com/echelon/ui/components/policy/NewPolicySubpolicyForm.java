package com.echelon.ui.components.policy;

import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;

public class NewPolicySubpolicyForm extends BaseLayout implements INewPolicySubpolicyForm {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7141795240486719405L;
	protected NewPolicyDto newPolicyDto;

	public NewPolicySubpolicyForm(String uiMode, UICONTAINERTYPES uiContainerType, NewPolicyDto newPolicyDto) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.newPolicyDto = newPolicyDto;
	}

	@Override
	public boolean loadData(NewPolicyDto newPolicyDto) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean enableEdit() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean saveEdit(NewPolicyDto newPolicyDto) {
		// TODO Auto-generated method stub
		return true;
	}
}
