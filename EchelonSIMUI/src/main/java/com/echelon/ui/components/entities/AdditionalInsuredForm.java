package com.echelon.ui.components.entities;

import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSPostalZipCodeField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class AdditionalInsuredForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6417300826229883330L;
	private TextField 				nameField;
	private TextField				addressField;
	private	TextField				cityField;
	private	DSComboBox<String>		provinceField;
	private	DSPostalZipCodeField	postalzipField;
	private TextField				relationshipField;
	private TextField				relationshipDisplayField;
	
	private Binder<AdditionalInsured> 	binder;
	private Binder<Address>				addBinder;

	public AdditionalInsuredForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildFields();
		buildForm();
	}
	
	private void buildFields() {
		nameField 					= new TextField(labels.getString("AdditionalInsuredFormNameLabel"));
		addressField				= new TextField(labels.getString("AdditionalInsuredFormAddressLabel"));
		cityField					= new TextField(labels.getString("AdditionalInsuredFormCityLabel"));
		provinceField				= new DSComboBox<String>(labels.getString("AdditionalInsuredFormProvinceLabel"), String.class)
											.withLookupTableId(ConfigConstants.LOOKUPTABLE_Jurisdictions);
		postalzipField				= new DSPostalZipCodeField(labels.getString("AdditionalInsuredFormPostalCodeLabel"));
		relationshipField			= new TextField(labels.getString("AdditionalInsuredFormRelationshipLabel"));
		relationshipDisplayField	= new TextField(labels.getString("AdditionalInsuredFormRelationshipLabel"));

		binder = new Binder<AdditionalInsured>(AdditionalInsured.class);
		binder.forField(nameField).asRequired().bind(AdditionalInsured::getAdditionalInsuredName, AdditionalInsured::setAdditionalInsuredName);
		binder.forField(relationshipField).bind(AdditionalInsured::getRelationship, AdditionalInsured::setRelationship);
		binder.setReadOnly(true);
		
		addBinder = new Binder<Address>(Address.class);
		addBinder.forField(addressField).asRequired()
										.bind(Address::getAddressLine1, Address::setAddressLine1);
		addBinder.forField(cityField).asRequired()
										.bind(Address::getCity, Address::setCity);
		addBinder.forField(provinceField).asRequired()
										.bind(Address::getProvState, Address::setProvState);
		addBinder.forField(postalzipField).asRequired()
										.withNullRepresentation("")
										.withConverter(postalzipField.getConverter())
										.withValidator(postalzipField.getValidator())
										.bind(Address::getPostalZip, Address::setPostalZip);
		addBinder.setReadOnly(true);
		
		relationshipDisplayField.setValue(labels.getString("AdditionalInsuredRelationshipDisplayValue"));
		relationshipDisplayField.setReadOnly(true);
	}
	
	private void buildForm() {
		FormLayout form = new FormLayout();
		/* 1 Line 
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 8)
		        );
		        */
		
		/* 2 Lines */
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		form.add(nameField, 2);
		form.add(addressField, 2);
		form.add(cityField, provinceField, postalzipField, relationshipDisplayField);
		
		this.add(form);
	}
	
	public void loadData(SubPolicy subPolicy, AdditionalInsured additionalInsured) {
		loadDataConfigs(binder, addBinder);

		relationshipDisplayField.setVisible(false);
		
		binder.setBean(additionalInsured);
		addBinder.setBean(additionalInsured.getAdditionalInsuredAddress());
		
		enableDataView(binder, addBinder);
	}
	
	public void enableEdit() {
		enableDataEdit(binder, addBinder);
		
		uiHelper.setEditFieldsReadonly(relationshipDisplayField);
		
		setFocus();
	}
	
	public boolean saveEdit(AdditionalInsured additionalInsured) {
		boolean ok1 = uiHelper.writeValues(binder, additionalInsured);
		boolean ok2 = uiHelper.writeValues(addBinder, additionalInsured.getAdditionalInsuredAddress());
		
		return ok1 && ok2;
	}
	
	public boolean validateEdit(boolean showMessage) {
		boolean valid = uiHelper.validateValues(showMessage, binder, addBinder);
		
		return valid;
	}
	
	public void setFocus() {
		nameField.focus();
	}
}
