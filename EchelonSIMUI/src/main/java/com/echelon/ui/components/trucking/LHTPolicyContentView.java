package com.echelon.ui.components.trucking;

import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyExposureRatingUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyExposureRatingForm;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.towing.TowingPolicyContentView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="LHTPolicyContentView", layout = BasePolicyView.class)
public class LHTPolicyContentView extends TowingPolicyContentView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9072874897636137438L;
	protected BasePolicyExposureRatingForm 		policyExposureRating;
	protected BasePolicyExposureRatingUIHelper	policyExposureRatingUIHelper;

	public LHTPolicyContentView() {
		super();
		// TODO Auto-generated constructor stub
		
		this.policyExposureRatingUIHelper = createPolicyExposureRatingFormUIHelper();
	}

	@Override
	protected String[] getTabLabels() {
		// TODO Auto-generated method stub
		String[] baseValues = super.getTabLabels();
		
		return new String[] {
			baseValues[0],
			baseValues[1],
			baseValues[2],
			labels.getString("PolicyDetailsTabPolicyExposureRating"),
			baseValues[3]
		};
	}

	@Override
	protected Component[] getTabComponents() {
		// TODO Auto-generated method stub
		Component[] baseValues = super.getTabComponents();
		
		return new Component[] {
				baseValues[0],
				baseValues[1],
				baseValues[2],
				getPolicyExposureRating(),
				baseValues[3]
		};
	}
	
	protected Component getPolicyExposureRating() {
		if (policyExposureRating == null) {
			policyExposureRating = new BasePolicyExposureRatingForm(uiMode, uiContainerType);
		}
		
		return policyExposureRating;
	}
	
	protected BasePolicyExposureRatingUIHelper createPolicyExposureRatingFormUIHelper() {
		return new BasePolicyExposureRatingUIHelper();
	}

	@Override
	public void loadData() {
		// TODO Auto-generated method stub
		super.loadData();
		
		if (policyExposureRating != null) {
			policyExposureRating.loadData(policyExposureRatingUIHelper.getCurrentSpecialityAutoExpRating());
		}
	}

}
