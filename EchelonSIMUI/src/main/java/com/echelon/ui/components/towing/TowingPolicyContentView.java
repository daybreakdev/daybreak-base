package com.echelon.ui.components.towing;

import com.echelon.ui.components.baseclasses.policy.BasePolicyContentView;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="PolicyContentView", layout = BasePolicyView.class)
public class TowingPolicyContentView extends BasePolicyContentView {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6801555630251226596L;

	@Override
	protected Component getPolicyInformationForm() {
		if (policyInformationForm == null) {
			policyInformationForm = new TowingPolicyInformationForm(uiMode, uiContainerType);
		}
		
		return policyInformationForm;
	}
}
