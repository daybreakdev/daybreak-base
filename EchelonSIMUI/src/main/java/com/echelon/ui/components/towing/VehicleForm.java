package com.echelon.ui.components.towing;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.ActionToolbar;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.HasValue.ValueChangeEvent;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class VehicleForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2539551764517128628L;
	
	private static final String DEFAULT_VEHICLECLASS = "43";
	
	private VehicleUIHelper					vehicleUIHelper;
	private ActionToolbar					toolbar;
	
	protected IntegerField					numOfUnitsField;
	protected TextField						descriptionField;
	protected DSComboBox<String>			vehicleTypeField;
	protected DSComboBox<String>			trailerTypeField;
	protected DSComboBox<String>			vehicleDescField;
	protected DSComboBox<String>			vehicleUseField;
	protected DSComboBox<String>			vehicleClassField;
	protected DSComboBox<String>			drivingRecordField;
	protected DSNumberField<Double>			unitLPNField;
	protected DSComboBox<String>			unitExposureField;
	protected TextField						unitSerialNumField;
	protected DSComboBox<String>			unitMakeField;
	protected ComboBox<Integer>				unitYearField;
	protected TextField						impliedRateGroupField;
	protected DSComboBox<String>			selectedRateGroupField;
	protected DSNumberField<Integer>  		avgKmsPerYearField;

	protected Binder<SpecialtyVehicleRisk> 	binder;
	
	protected SpecialityAutoSubPolicy		parentSubPolicy;
	protected SpecialtyVehicleRisk			currVehicle;
	
	public VehicleForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		this.vehicleUIHelper = createVehicleUIHelper();
		
		if (UIConstants.UICONTAINERTYPES.Tab.equals(uiContainerType)) {
			DSHeaderComponents headComponents = uiLayoutHelper.buildHeaderLayout(this,
														null,
														UIConstants.UICOMPONENTTYPES.Form,
														UIConstants.UIACTION_Update, UIConstants.UIACTION_Copy, UIConstants.UIACTION_Delete);
			this.toolbar = (ActionToolbar) headComponents.getToolbar();
			buildButtonEvents();
		}

		buildFields();
		buildForm();
	}

	protected VehicleUIHelper createVehicleUIHelper() {
		return new VehicleUIHelper();
	}
	
	protected VehicleUIHelper getVehicleUIHelper() {
		return vehicleUIHelper;
	}
	
	protected void buildFields() {
		numOfUnitsField 		= new IntegerField(labels.getString("VehicleFormNumOfUnitsLabel"));
		descriptionField		= new TextField(labels.getString("VehicleFormDescriptionLabel"));
		vehicleTypeField 		= new DSComboBox<String>(labels.getString("VehicleFormVehicleTypeLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehType);
		trailerTypeField 		= new DSComboBox<String>(labels.getString("VehicleFormTrailerTypeLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_TrailerType);
		vehicleDescField 		= new DSComboBox<String>(labels.getString("VehicleFormVehicleDescLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehDesc);
		vehicleUseField 		= new DSComboBox<String>(labels.getString("VehicleFormVehicleUseLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehUseList);
		vehicleClassField 		= new DSComboBox<String>(labels.getString("VehicleFormVehicleClassLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehClassList)
										.withSortList(false);
		drivingRecordField 		= new DSComboBox<String>(labels.getString("VehicleFormDrivingRecordLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_DrivingRecord);
		unitLPNField 			= new DSNumberField<Double>(labels.getString("VehicleFormUnitLPNLabel"), Double.class, false);
		unitExposureField 		= new DSComboBox<String>(labels.getString("VehicleFormUnitExposureLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_UnitExposure);
		unitSerialNumField 		= new TextField(labels.getString("VehicleFormUnitSerialNumLabel"));
		unitMakeField 			= new DSComboBox<String>(labels.getString("VehicleFormUnitMakeLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehMakeList);
		unitYearField 			= new ComboBox<Integer>(labels.getString("VehicleFormUnitYearLabel"));
		impliedRateGroupField 	= new TextField(labels.getString("VehicleFormImpliedRateGroupLabel"));
		selectedRateGroupField 	= new DSComboBox<String>(labels.getString("VehicleFormSelectedRateGroupLabel"), String.class)
										.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehicheRateGroupList)
										.withSortList(false);
		avgKmsPerYearField		= new DSNumberField<Integer>(labels.getString("VehicleFormAvgKmsPerYearLabel"), Integer.class);
		
		numOfUnitsField.setHasControls(true);
		numOfUnitsField.setMin(1);
		
		binder = new Binder<SpecialtyVehicleRisk>(SpecialtyVehicleRisk.class);
		binder.forField(numOfUnitsField).asRequired().bind(SpecialtyVehicleRisk::getNumberOfUnits, SpecialtyVehicleRisk::setNumberOfUnits);
		binder.forField(descriptionField).asRequired().bind(SpecialtyVehicleRisk::getDisplayString, SpecialtyVehicleRisk::setDisplayString);
		binder.forField(vehicleTypeField).asRequired().bind(SpecialtyVehicleRisk::getVehicleType, SpecialtyVehicleRisk::setVehicleType);
		binder.forField(trailerTypeField).asRequired().bind(SpecialtyVehicleRisk::getTrailerType, SpecialtyVehicleRisk::setTrailerType);
		binder.forField(vehicleDescField).asRequired().bind(SpecialtyVehicleRisk::getVehicleDescription, SpecialtyVehicleRisk::setVehicleDescription);
		binder.forField(vehicleUseField).asRequired().bind(SpecialtyVehicleRisk::getVehicleUse, SpecialtyVehicleRisk::setVehicleUse);
		binder.forField(vehicleClassField).asRequired().bind(SpecialtyVehicleRisk::getVehicleClass, SpecialtyVehicleRisk::setVehicleClass);
		binder.forField(drivingRecordField).asRequired().bind(SpecialtyVehicleRisk::getDrivingRecord, SpecialtyVehicleRisk::setDrivingRecord);
		binder.forField(unitLPNField) //.asRequired()
									 .withNullRepresentation("")
									 .withConverter(unitLPNField.getConverter())
									 .bind(SpecialtyVehicleRisk::getUnitLPN, SpecialtyVehicleRisk::setUnitLPN);
		binder.forField(unitExposureField).asRequired().bind(SpecialtyVehicleRisk::getUnitExposure, SpecialtyVehicleRisk::setUnitExposure);
		binder.forField(selectedRateGroupField).asRequired().bind(SpecialtyVehicleRisk::getSelectedRateGroup, SpecialtyVehicleRisk::setSelectedRateGroup);
		
		// Also fix vehicleRequiredChanges
		binder.forField(unitSerialNumField).bind(SpecialtyVehicleRisk::getUnitSerialNumberorVin, SpecialtyVehicleRisk::setUnitSerialNumberorVin);
		binder.forField(unitMakeField).bind(SpecialtyVehicleRisk::getUnitMake, SpecialtyVehicleRisk::setUnitMake);
		binder.forField(unitYearField).bind(SpecialtyVehicleRisk::getUnitYear, SpecialtyVehicleRisk::setUnitYear);
		binder.forField(impliedRateGroupField) //.asRequired()
									.bind(SpecialtyVehicleRisk::getImpliedRateGroup, SpecialtyVehicleRisk::setImpliedRateGroup);
		binder.forField(avgKmsPerYearField) //.asRequired()
									.withNullRepresentation("")
									.withConverter(avgKmsPerYearField.getConverter())
									.bind(SpecialtyVehicleRisk::getAvgKmsPerYear, SpecialtyVehicleRisk::setAvgKmsPerYear);
		
		binder.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout form1 = buildTopForm(); 
		FormLayout form2 = buildBottomForm();
		
		form1.add(form2, 3);
		this.add(form1);
	}
	
	protected FormLayout buildTopForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 3)
		        );

		buildFirstLine(form);
		form.add(vehicleTypeField, vehicleDescField, trailerTypeField);
		form.add(vehicleUseField, vehicleClassField, drivingRecordField);
		
		return form;
	}
	
	protected void buildFirstLine(FormLayout form) {
		form.add(numOfUnitsField, 1);
		form.add(descriptionField, 2);
	}
	
	protected FormLayout buildBottomForm() {
		FormLayout form2 = new FormLayout();
		form2.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 4)
		        );
		form2.add(unitSerialNumField, unitMakeField, unitYearField, unitLPNField);
		form2.add(avgKmsPerYearField, impliedRateGroupField, selectedRateGroupField, unitExposureField);
		
		return form2;
	}
	
	private void enabledPPVehicleFields() {
		boolean isValueChanged = false;
		boolean isReadOnly = true;
		// Is Vehicle or Fleet
		if (!vehicleUIHelper.isVehicleGroup(numOfUnitsField.getValue()) ||
			(parentSubPolicy.getIsFleet() != null && parentSubPolicy.getIsFleet())) {
			isReadOnly = false;
		}
		
		if (isReadOnly) {
			unitSerialNumField.clear();
			unitMakeField.clear();
		}
		
		if ((!numOfUnitsField.isReadOnly() && numOfUnitsField.getValue() == null) || 
			unitSerialNumField.isReadOnly() != isReadOnly) {
			if (isReadOnly) {
				uiHelper.setEditFieldsReadonly(unitSerialNumField, unitMakeField);
			}
			else {
				uiHelper.setEditFieldsNotReadonly(unitSerialNumField, unitMakeField);
			}
			isValueChanged = true;
		}
		
		if (isValueChanged) {
			vehicleRequiredChanges();
		}
		
		if (vehicleUIHelper.isVehicleGroup(numOfUnitsField.getValue())) {
			uiHelper.setEditFieldsNotReadonly(avgKmsPerYearField);
		}
		else {
			avgKmsPerYearField.clear();
			uiHelper.setEditFieldsReadonly(avgKmsPerYearField);
		}
	}

	private void vehicleRequiredChanges() {
		if (isFleetbasisScheduled()) {
			uiHelper.setEditFieldsReadonly(numOfUnitsField);
			
			if (UIConstants.VEHICLEDESC_DEALERPLATE.equalsIgnoreCase(vehicleDescField.getValue()) ||
				UIConstants.VEHICLEDESC_OPCF27B.equalsIgnoreCase(vehicleDescField.getValue())) {
				setMakeYearSerialNumNotRequired();
			}
			else {
				setMakeYearSerialNumRequired();
			}
		}
		else if (!numOfUnitsField.isReadOnly()) {
			// Fleet is optional
			if (unitSerialNumField.isReadOnly() ||
				(parentSubPolicy.getIsFleet() != null && parentSubPolicy.getIsFleet())) {
				setMakeYearSerialNumNotRequired();
			}
			else {
				setMakeYearSerialNumRequired();
			}
		}
	}
	
	private void setMakeYearSerialNumRequired() {
		binder.removeBinding(unitSerialNumField);
		binder.removeBinding(unitMakeField);
		binder.removeBinding(unitYearField);
		
		binder.forField(unitSerialNumField).asRequired().bind(SpecialtyVehicleRisk::getUnitSerialNumberorVin, SpecialtyVehicleRisk::setUnitSerialNumberorVin);
		binder.forField(unitMakeField).asRequired().bind(SpecialtyVehicleRisk::getUnitMake, SpecialtyVehicleRisk::setUnitMake);
		binder.forField(unitYearField).asRequired().bind(SpecialtyVehicleRisk::getUnitYear, SpecialtyVehicleRisk::setUnitYear);
	}
	
	private void setMakeYearSerialNumNotRequired() {
		binder.removeBinding(unitSerialNumField);
		binder.removeBinding(unitMakeField);
		binder.removeBinding(unitYearField);
		
		binder.forField(unitSerialNumField).bind(SpecialtyVehicleRisk::getUnitSerialNumberorVin, SpecialtyVehicleRisk::setUnitSerialNumberorVin);
		binder.forField(unitMakeField).bind(SpecialtyVehicleRisk::getUnitMake, SpecialtyVehicleRisk::setUnitMake);
		binder.forField(unitYearField).bind(SpecialtyVehicleRisk::getUnitYear, SpecialtyVehicleRisk::setUnitYear);
		
		unitSerialNumField.setRequiredIndicatorVisible(false);
		unitMakeField.setRequiredIndicatorVisible(false);
		unitYearField.setRequiredIndicatorVisible(false);
	}
	
	private boolean isFleetbasisScheduled() {
		return parentSubPolicy != null &&
				SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED.equalsIgnoreCase(parentSubPolicy.getFleetBasis());
	}

	private void numOfUnitsFieldValueChanged(ValueChangeEvent<?> event) {
		// no need to initialize year for vehicle group
		/*
		if (vehicleUIHelper.isVehicleGroup(numOfUnitsField.getValue())) {
			if (unitYearField.getValue() == null) {
				unitYearField.setDSValue(LocalDate.now().getYear());
			}
		}
		*/
		
		enabledPPVehicleFields();
	}

	private void vehicleTypeFieldValueChanged(ValueChangeEvent<?> event) {
		vehicleDescField.loadDSItems(vehicleTypeField.getValue());
		trailerTypeField.loadDSItems(vehicleTypeField.getValue(), vehicleDescField.getValue());
	}

	protected void vehicleDescFieldValueChanged(ValueChangeEvent<?> event) {
		trailerTypeField.loadDSItems(vehicleTypeField.getValue(), vehicleDescField.getValue());
		vehicleClassField.loadDSItems(vehicleTypeField.getValue(), vehicleDescField.getValue());
		
		setDefaultVehicleClass();
		
		if (!UIConstants.VEHICLEDESC_TRAILER.equalsIgnoreCase(vehicleDescField.getValue()) && 
				!UIConstants.VEHICLEDESC_EXCESSTRAILER.equalsIgnoreCase(vehicleDescField.getValue())) {
			trailerTypeField.setDSValue(UIConstants.TRAILERTYPE_NOTTRAILER);
		} else if ((trailerTypeField.getValue() == null && 
				ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(BrokerUI.getUserSession().getPolicyProductConfig().getProductCd())) && 
				(UIConstants.VEHICLEDESC_TRAILER.equalsIgnoreCase(vehicleDescField.getValue()) || UIConstants.VEHICLEDESC_EXCESSTRAILER.equalsIgnoreCase(vehicleDescField.getValue()))) {
			trailerTypeField.setDSValue(UIConstants.TRAILERTYPE_CARGO);
		}
			
		
		vehicleRequiredChanges();
	}
	
	protected String getDefaultVehicleClass() {
		return DEFAULT_VEHICLECLASS;
	}
	
	protected void setDefaultVehicleClass() {
		String defaultValue = getDefaultVehicleClass();
		
		// try to keep the selected value
		String currValue = vehicleClassField.getValue();
		if (StringUtils.isNotBlank(currValue)) {
			if (uiLookupHelper.findLookupItemByKey(currValue, vehicleClassField.getDSLookupItems(), true) != null) {
				defaultValue = currValue;
			}
		}
		
		vehicleClassField.setDSValue(defaultValue);
	}
	
	private void trailerTypeFieldValueChanged(ValueChangeEvent<?> event) {
		trailerTypeFieldTableIdChanged(trailerTypeField.getValue());	
		unitMakeField.loadDSItems();
	}
	
	private void trailerTypeFieldTableIdChanged(String value) {
		if (value != null && UIConstants.TRAILERTYPE_NOTTRAILER.equalsIgnoreCase(value)) {
			unitMakeField.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehMakeList);
		}
		else {
			unitMakeField.withLookupTableId(ConfigConstants.LOOKUPTABLE_VehMakeListTrailers);
		}
	}
	
	protected void unitYearFieldValueChanged(ValueChangeEvent<?> event) {
		getVehicleRateGroup();
	}
	
	protected void unitLPNFieldValueChanged(ValueChangeEvent<?> event) {
		getVehicleRateGroup();
	}
	
	protected void getVehicleRateGroup() {
		vehicleUIHelper.setVehicleRateGroupFields(unitYearField, unitLPNField, impliedRateGroupField, selectedRateGroupField);
	}

	@Override
	protected boolean loadDataConfigs(Binder... binders) {
		boolean ok = super.loadDataConfigs(binders);
		
		unitYearField.setItems(uiLookupHelper.getVehilceYears());
		
		return ok;
	}

	public void loadData(SpecialityAutoSubPolicy parentSubPolicy, SpecialtyVehicleRisk vehicleRisk) {
		this.parentSubPolicy = parentSubPolicy;
		this.currVehicle 	 = vehicleRisk;
		if (vehicleRisk != null) {
			trailerTypeFieldTableIdChanged(vehicleRisk.getTrailerType());
		}
		loadDataConfigs(binder);
		
		binder.readBean(vehicleRisk);
		
		enableDataView(binder);
		
		refreshToolbar(vehicleRisk);
	}
	
	public void enableEdit() {
		enableDataEdit(binder);
		
		numOfUnitsFieldValueChanged(null);
		uiHelper.setEditFieldsReadonly(impliedRateGroupField);
		vehicleTypeFieldValueChanged(null);
		vehicleDescFieldValueChanged(null);
		vehicleRequiredChanges();
		
		setupFieldValueChanges();
		
		setFocus();
	}
	
	public boolean saveEdit(SpecialtyVehicleRisk vehicleRisk) {
		boolean ok1 = uiHelper.writeValues(binder, vehicleRisk);
		
		if (ok1) {
			vehicleRisk.setIsIndividual(!vehicleUIHelper.isVehicleGroup(vehicleRisk.getNumberOfUnits()));
		}
		
		return ok1;
	}
	
	public boolean validateEdit() {
		boolean valid = uiHelper.validateValues(binder);
		
		return valid;
	}
	
	private void buildButtonEvents() {
		if (toolbar != null) {
			if (toolbar.getButton(UIConstants.UIACTION_Update) != null) {
				toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(e -> {
						getVehicleUIHelper().updateVehicleAction(parentSubPolicy, currVehicle);
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Copy) != null) {
				toolbar.getButton(UIConstants.UIACTION_Copy).addClickListener(e -> {
						getVehicleUIHelper().copyVehicleAction(parentSubPolicy, currVehicle);
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(e -> {
						getVehicleUIHelper().deleteVehicleAction(parentSubPolicy, currVehicle);
				});
			}
			
			if (toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
				toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(e -> {
						getVehicleUIHelper().undeleteVehicleAction(parentSubPolicy, currVehicle);
				});
			}
		}
	}
	
	private void refreshToolbar(SpecialtyVehicleRisk risk) {
		if (toolbar != null) {
			toolbar.refreshButtons(null, risk);
		}
	}

	public void setFocus() {
		numOfUnitsField.focus();
	}
	
	protected void setupFieldValueChanges() {
		numOfUnitsField.addValueChangeListener(this::numOfUnitsFieldValueChanged);
		vehicleTypeField.addValueChangeListener(this::vehicleTypeFieldValueChanged);
		vehicleDescField.addValueChangeListener(this::vehicleDescFieldValueChanged);
		trailerTypeField.addValueChangeListener(this::trailerTypeFieldValueChanged);
		unitYearField.addValueChangeListener(this::unitYearFieldValueChanged);
		unitLPNField.addValueChangeListener(this::unitLPNFieldValueChanged);
	}
}
