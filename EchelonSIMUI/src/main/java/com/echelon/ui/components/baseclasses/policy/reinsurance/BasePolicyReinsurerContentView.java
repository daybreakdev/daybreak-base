package com.echelon.ui.components.baseclasses.policy.reinsurance;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.reinsurance.PolicyReinsurerUIHelper;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.router.Route;

/**
 * Content view for mapping reinsurers to policies.
 */
@Route(value = "ReinsurersContentView", layout = BasePolicyView.class)
public class BasePolicyReinsurerContentView extends BaseListLayout<PolicyReinsurer>
	implements IPolicyViewContent {
	private static final long serialVersionUID = -4882336871977839082L;

	private Grid<PolicyReinsurer> grid;
	private PolicyReinsurerUIHelper policyReinsurerUIHelper = new PolicyReinsurerUIHelper();

	public BasePolicyReinsurerContentView() {
		super();
		initializeLayouts();
		buildGrid();
	}

	public BasePolicyReinsurerContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		initializeLayouts();
		buildGrid();
	}

	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		policyReinsurerUIHelper.loadView(this, nodeItem);
	}

	public boolean loadData(Set<PolicyReinsurer> policyReinsurers) {
		List<PolicyReinsurer> list = new ArrayList<>();
		if (policyReinsurers != null) {
			list.addAll(policyReinsurers);
		}

		return loadData(list);
	}

	public boolean loadData(List<PolicyReinsurer> policyReinsurers) {
		if (policyReinsurers == null) {
			policyReinsurers = new ArrayList<>();
		}

		policyReinsurers.sort(Comparator.comparing(polRe -> polRe.getReinsurer().getLegalName(), Comparator.nullsLast(Comparator.naturalOrder())));

		grid.setItems(policyReinsurers);
		refreshToolbar();

		return true;
	}

	protected void initializeLayouts() {
		this.setSizeFull();
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this,
			labels.getString("PolicyReinsurerListTitle"), UICOMPONENTTYPES.List,
			new String[] { UIConstants.UIACTION_New, UIConstants.UIACTION_View,
					UIConstants.UIACTION_Delete, });
		this.toolbar = headerComponents.getToolbar();

		toolbar.getButton(UIConstants.UIACTION_New)
			.addClickListener(event -> policyReinsurerUIHelper.newAction());
		toolbar.getButton(UIConstants.UIACTION_View)
			.addClickListener(event -> policyReinsurerUIHelper.viewAction(getSelected()));
		toolbar.getButton(UIConstants.UIACTION_Delete)
			.addClickListener(event -> policyReinsurerUIHelper.deleteAction(getSelected()));
	}

	protected void buildGrid() {
		grid = new Grid<>();
		uiHelper.setGridStyle(grid);
		grid.setSizeFull();
		grid.addSelectionListener(event -> refreshToolbar());

		grid.addColumn(polRe -> polRe.getReinsurer().getLegalName())
			.setHeader(labels.getString("PolicyReinsurerLegalNameLabel"));

		this.add(grid);
	}

	@Override
	protected void refreshToolbar() {
		toolbar.refreshButtons(null, grid);
		
		boolean isReinsuranceAllowed = policyReinsurerUIHelper.isReinsuranceAllowed();
		boolean hasSelection = !grid.getSelectedItems().isEmpty();
		
		Button newButton = toolbar.getButton(UIConstants.UIACTION_New);
		newButton.setEnabled(isReinsuranceAllowed);

		Button viewButton = toolbar.getButton(UIConstants.UIACTION_View);
		viewButton.setEnabled(isReinsuranceAllowed && hasSelection);

		Button deleteButton = toolbar.getButton(UIConstants.UIACTION_Delete);
		deleteButton.setEnabled(isReinsuranceAllowed && hasSelection);
	}

	private PolicyReinsurer getSelected() {
		return grid.asSingleSelect().getValue();
	}
}
