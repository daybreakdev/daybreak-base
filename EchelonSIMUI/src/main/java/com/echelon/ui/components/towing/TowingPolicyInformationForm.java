package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSNumberField;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.ui.components.baseclasses.policy.BasePolicyInformationForm;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class TowingPolicyInformationForm extends BasePolicyInformationForm {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1886090500734616407L;
	private DSNumberField<Integer> liabilityLimitField;
	
	private Binder<SpecialtyAutoPackage> spBinder;

	public TowingPolicyInformationForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}
		
	@Override
	protected void buildFields() {
		// TODO Auto-generated method stub
		super.buildFields();
		
		liabilityLimitField	= new DSNumberField<Integer>(labels.getString("PolicyInfoFormLiabilityLimitLabel"), Integer.class);
		
		spBinder = new Binder<SpecialtyAutoPackage>(SpecialtyAutoPackage.class);
		spBinder.forField(liabilityLimitField).withNullRepresentation("")
												.withConverter(liabilityLimitField.getConverter())
												.bind(SpecialtyAutoPackage::getLiabilityLimit, SpecialtyAutoPackage::setLiabilityLimit);
		spBinder.setReadOnly(true);
	}

	@Override
	protected void buildFormLine1(FormLayout form) {
		form.add(productField, liabilityLimitField, issueDateField);
	}

	@Override
	public void loadData(PolicyTransaction policyTransaction) {
		// TODO Auto-generated method stub
		super.loadData(policyTransaction);
		
		if (policyTransaction.getPolicyVersion().getInsurancePolicy() instanceof SpecialtyAutoPackage) {
			spBinder.readBean((SpecialtyAutoPackage) policyTransaction.getPolicyVersion().getInsurancePolicy());
		}
		
		enableDataView(spBinder);
	}

}
