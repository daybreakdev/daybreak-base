package com.echelon.ui.components.trucking;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.ui.components.towing.CargoSubPolicyValueList;
import com.echelon.ui.converters.SIMNumericFormat;
import com.vaadin.flow.data.renderer.NumberRenderer;

public class LHTCargoSubPolicyValueList extends CargoSubPolicyValueList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6424243997302451183L;

	public LHTCargoSubPolicyValueList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected NumberRenderer<CargoDetails> createPartialPremiumNumberRenderer() {
		return new NumberRenderer<CargoDetails>(CargoDetails::getCargoPartialPremiums, SIMNumericFormat.getFactorInstance());
	}

}
