package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyTransactionData;
import com.ds.ins.domain.policy.Risk;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyExposureRatingProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2197606586401728957L;
	private PolicyTransactionData policyTransactionData;

	public BasePolicyExposureRatingProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public BasePolicyExposureRatingProcessor withParameter(PolicyTransactionData policyTransactionData) {
		this.policyTransactionData = policyTransactionData;
		
		return this;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
		case UIConstants.UIACTION_Save:
			savePolicyExposureRating(policyTransaction, policyTransactionData);
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}
	
	protected void savePolicyExposureRating(PolicyTransaction<Risk> polTransaction, PolicyTransactionData polTransactionData) {
		if (!polTransaction.getPolicyTransactionDatas()
							.stream().filter(o -> StringUtils.isNotBlank(o.getDescription()) && o.getDescription().equalsIgnoreCase(polTransactionData.getDescription()))
							.findFirst().isPresent()) {
			polTransaction.addPolicyTransactionData(polTransactionData);
		}
		
		updatePolicyTransaction(polTransaction);
	}

}
