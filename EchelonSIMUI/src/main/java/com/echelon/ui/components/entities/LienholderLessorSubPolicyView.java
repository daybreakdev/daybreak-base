package com.echelon.ui.components.entities;

import com.ds.ins.domain.policy.Risk;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.towing.LienLessorSubPolicyVehicleDetailsList;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.component.splitlayout.SplitLayout.Orientation;

public class LienholderLessorSubPolicyView extends BaseLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6886008660992935523L;
	private SplitLayout 							groupLayout;
	private LienholderLessorSubPolicyList			lienLessorList;
	private LienLessorSubPolicyVehicleDetailsList   vehDetailsList;
	
	public LienholderLessorSubPolicyView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		buildLayout();
	}
	
	private void buildLayout() {
		this.setSizeFull();
		
		buildGroupLayout();
	}

	private void buildGroupLayout() {
		groupLayout = new SplitLayout();
		uiHelper.formatFullLayout(groupLayout);
		groupLayout.setSizeFull();
		groupLayout.setOrientation(Orientation.VERTICAL);

		lienLessorList = new LienholderLessorSubPolicyList(uiMode, uiContainerType);
		vehDetailsList = new LienLessorSubPolicyVehicleDetailsList(uiMode, uiContainerType);

		groupLayout.addToPrimary(lienLessorList);
		groupLayout.addToSecondary(vehDetailsList);
		groupLayout.setSplitterPosition(35);
		lienLessorList.setSelectionChangeListener(vehDetailsList::lienholderLessorChanged);
		
		groupLayout.setVisible(false);
		this.add(groupLayout);
	}
	
	public void loadData(SpecialityAutoSubPolicy<Risk> parentSubPolicy) {
		groupLayout.setVisible(true);
		lienLessorList.loadData(parentSubPolicy);
	}
}
