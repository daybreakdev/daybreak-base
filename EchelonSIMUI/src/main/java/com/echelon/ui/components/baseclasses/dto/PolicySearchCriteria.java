package com.echelon.ui.components.baseclasses.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.dto.PolicySearchResults;
import com.vaadin.flow.component.grid.GridSortOrder;

public class PolicySearchCriteria implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5838424384668687457L;
	private String name;
	private String policyNum;
	private String broker;
	private String quoteNum;
	private String extQuoteNum;
	private List<PolicySearchResults> searchResults;
	private List<GridSortOrder<PolicySearchResults>> sortOrders;
	
	public PolicySearchCriteria() {
		super();
		// TODO Auto-generated constructor stub
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPolicyNum() {
		return policyNum;
	}
	public void setPolicyNum(String policyNum) {
		this.policyNum = policyNum;
	}
	public String getBroker() {
		return broker;
	}
	public void setBroker(String broker) {
		this.broker = broker;
	}
	public String getQuoteNum() {
		return quoteNum;
	}
	public void setQuoteNum(String quoteNum) {
		this.quoteNum = quoteNum;
	}
	public List<PolicySearchResults> getSearchResults() {
		if (searchResults == null) {
			searchResults = new ArrayList<PolicySearchResults>();
		}
		return searchResults;
	}
	public void setSearchResults(List<PolicySearchResults> searchResults) {
		this.searchResults = searchResults;
	}
	public List<GridSortOrder<PolicySearchResults>> getSortOrders() {
		return sortOrders;
	}
	public void setSortOrders(List<GridSortOrder<PolicySearchResults>> sortOrders) {
		this.sortOrders = sortOrders;
	}
	public String getExtQuoteNum() {
		return extQuoteNum;
	}
	public void setExtQuoteNum(String extQuoteNum) {
		this.extQuoteNum = extQuoteNum;
	}
	
}