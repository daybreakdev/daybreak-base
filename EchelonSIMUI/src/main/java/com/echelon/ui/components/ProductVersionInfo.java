package com.echelon.ui.components;

import java.io.Serializable;

public class ProductVersionInfo implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1681948132376001036L;
	private static final String VersionNumber = "4.0.24";

	public ProductVersionInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static String getVersionNumber() {
		return VersionNumber;
	}
}
