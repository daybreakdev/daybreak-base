package com.echelon.ui.components.baseclasses.policy;

import java.time.LocalDate;

import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.IDSFocusAble;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.uicommon.converters.DSLocaldateTimeToLocalDateConverter;
import com.ds.ins.utils.Constants;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;

public class BasePolicyUpdateForm extends BaseLayout implements IDSFocusAble {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4268502503865342800L;
	protected TextField				extQuoteNumField;
	protected DatePicker			termEffectiveDateField;
	protected DatePicker			quotePreparedDateField;
	protected DSComboBox<String>	quotePreparedByField;
	
	protected Binder<PolicyTransaction> binder;
	protected Binder<InsurancePolicy>   ipBinder;

	public BasePolicyUpdateForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		
		initializeLayouts();
		buildFields();
		buildForm();
	}
	
	protected void initializeLayouts() {
		
	}

	protected void buildFields() {
		termEffectiveDateField 	= new DatePicker(labels.getString("PolicyInfoFormTermEffectiveDateLabel"));
		
		binder = new Binder<PolicyTransaction>(PolicyTransaction.class);
		binder.forField(termEffectiveDateField).withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("policyVersion.versionTerm.termEffDate");
		binder.setReadOnly(true);
		
		ipBinder = new Binder<InsurancePolicy>(InsurancePolicy.class);
		extQuoteNumField		= new TextField(labels.getString("PolicyInfoFormExternalQuoteNumberLabel"));
		quotePreparedByField	= new DSComboBox<String>(labels.getString("PolicyInfoFormQuotePreparedBy"), String.class);
		quotePreparedDateField	= new DatePicker(labels.getString("PolicyInfoFormQuotePreparedDate"));
		
		ipBinder.forField(quotePreparedByField).asRequired().bind("quotePreparedBy");
		//ipBinder.forField(quotePreparedDateField).asRequired().withConverter(new DSLocaldateTimeToLocalDateConverter()).bind("quotePreparedDate");
		ipBinder.forField(extQuoteNumField).bind("externalQuoteNum");
		
		ipBinder.setReadOnly(true);
	}
	
	protected void buildForm() {
		FormLayout form = new FormLayout();
		
		form.setResponsiveSteps(
		        new FormLayout.ResponsiveStep(UIConstants.MINFIELDWIDTH, 1)
		        );
		
		buildFormLines(form);
		
		this.add(form);
	}
	
	protected void buildFormLines(FormLayout form) {
		form.add(extQuoteNumField);
		form.add(termEffectiveDateField);
		form.add(quotePreparedByField);
	}
	
	public void loadData(PolicyTransaction policyTransaction) {
		loadDataConfigs(binder, ipBinder);
		
		binder.readBean(policyTransaction);
		
		if (policyTransaction != null) {
			uiHelper.readValues(ipBinder, policyTransaction.getPolicyVersion().getInsurancePolicy());
		}
		
		enableDataView(binder, ipBinder);
	}			

	public void enabledEdit(PolicyTransaction policyTransaction) {
		enableDataEdit(binder, ipBinder);
		
		if (policyTransaction != null &&
			!Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(policyTransaction.getPolicyVersion().getPolicyTxnType())) {
			uiHelper.setEditFieldsReadonly(termEffectiveDateField);
		}
		
		setFocus();
	}
	
	public boolean validateEdit() {
		extQuoteNumField.setInvalid(false);
		extQuoteNumField.setErrorMessage(null);
		
		boolean ok1 = uiHelper.validateValues(binder);
		boolean ok2 = uiHelper.validateValues(ipBinder);
		boolean ok3 = UIFactory.getInstance().getPolicyDetailsUIHelper().validateExternalQuoteNumber(extQuoteNumField.getValue(), false);
		
		if (!ok3) {
			extQuoteNumField.setInvalid(true);
			extQuoteNumField.setErrorMessage(labels.getString("NewPolicyDialogExternalQuoteNumberExistMessage"));
		}
		
		return ok1 && ok2 && ok3;
	}

	public LocalDate getTermEffectiveDate() {
		return termEffectiveDateField.getValue();
	}

	public LocalDate getQuotePreparedDate() {
		return quotePreparedDateField.getValue();
	}
	
	public String getQuotePreparedBy() {
		return quotePreparedByField.getValue();
	}
	
	public String getExternalQuoteNumber() {
		return extQuoteNumField.getValue();
	}

	public void setFocus() {
		extQuoteNumField.focus();
	}
}
