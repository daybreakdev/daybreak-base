package com.echelon.ui.components.baseclasses.process.policy;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.logging.Logger;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.constants.UIConstants;

public class BasePolicyDetailProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8898916316502050463L;
	private static final Logger log	= Logger.getLogger(BasePolicyDetailProcessor.class.getName());
	private String 		policyNumber;
	private String 		quoteNumber;
	private Integer 	termNumber;
	private Integer 	versionNumber;
	
	protected LocalDateTime	termEffectivate;
	protected String		quotePreparedBy;
	protected String 		extQuoteNumber;

	public BasePolicyDetailProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			LocalDateTime termEffectivate, String quotePreparedBy, String extQuoteNumber) {
		super(processAction, policyTransaction);
		this.termEffectivate = termEffectivate;
		this.quotePreparedBy = quotePreparedBy;
		this.extQuoteNumber = extQuoteNumber;
	}

	public BasePolicyDetailProcessor(String processAction, PolicyTransaction<?> policyTransaction, 
			String policyNumber, String quoteNumber, Integer termNumber, Integer versionNumber) {
		super(processAction, policyTransaction);
		this.policyNumber = policyNumber;
		this.quoteNumber = quoteNumber;
		this.termNumber = termNumber;
		this.versionNumber = versionNumber;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Open:
			try {
				this.policyTransaction = loadPolicyTransaction(policyNumber, quoteNumber, termNumber, versionNumber);
				this.policyTransaction = loadPolicyData(this.policyTransaction);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;
			
		default:
			break;
		}
	}
	
	protected boolean ratePolicyTransaction() {
		return ratePolicyTransaction(null, null);
	}
}
