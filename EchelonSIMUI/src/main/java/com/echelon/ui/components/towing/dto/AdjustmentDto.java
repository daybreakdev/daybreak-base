package com.echelon.ui.components.towing.dto;

import java.io.Serializable;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageList;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class AdjustmentDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4411877098382426397L;
	private SpecialtyVehicleRiskAdjustment 	adjustment;
	private BaseCoverageList				coverageList;
	private HorizontalLayout				covListContainer;
	private boolean							isVisible;
	
	public AdjustmentDto(SpecialtyVehicleRiskAdjustment adjustment) {
		super();
		this.adjustment = adjustment;
		this.isVisible = false;
	}

	public SpecialtyVehicleRiskAdjustment getAdjustment() {
		return adjustment;
	}

	public void setAdjustment(SpecialtyVehicleRiskAdjustment adjustment) {
		this.adjustment = adjustment;
	}

	public BaseCoverageList getCoverageList() {
		return coverageList;
	}

	public void setCoverageList(BaseCoverageList coverageList) {
		this.coverageList = coverageList;
	}

	public HorizontalLayout getCovListContainer() {
		return covListContainer;
	}

	public void setCovListContainer(HorizontalLayout covListContainer) {
		this.covListContainer = covListContainer;
	}

	public boolean isVisible() {
		return isVisible;
	}

	public void setVisible(boolean isVisible) {
		this.isVisible = isVisible;
	}
	
}
