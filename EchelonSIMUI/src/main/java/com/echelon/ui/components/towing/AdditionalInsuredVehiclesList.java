package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSHeaderComponents;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseListLayout;
import com.echelon.ui.components.entities.AdditionalInsuredList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.AdditionalInsuredVehiclesUIHelper;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;

public class AdditionalInsuredVehiclesList extends BaseListLayout<AdditionalInsuredVehicles> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8722696355587481753L;
	private AdditionalInsuredVehiclesUIHelper	addlInsuredVehiclesUIHelper = new AdditionalInsuredVehiclesUIHelper();
	private Grid<AdditionalInsuredVehicles> 	addlInsuredVehiclesGrid;
	private SpecialityAutoSubPolicy<Risk>		parentSubPolicy;
	private AdditionalInsured					parentAdditionalInsured;
	
	public AdditionalInsuredVehiclesList(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
		initializeLayout();
		buildGrid();
	}

	private void initializeLayout() {
		this.setSizeFull();
		
		DSHeaderComponents headerComponents = uiLayoutHelper.buildHeaderLayout(this, 
														labels.getString("AddlInsuredVehiclesListTitle"),
														UIConstants.UICOMPONENTTYPES.List);
		this.toolbar = headerComponents.getToolbar();		
		setupNewButtonEvent();
		setupUpdateButtonEvent();
		setupDeleteButtonEvent();
		setupUndeleteButtonEvent();
		setupViewButtonEvent();
	}
	
	private void buildGrid() {
		addlInsuredVehiclesGrid = new Grid<AdditionalInsuredVehicles>();
		addlInsuredVehiclesGrid.setSizeFull();
		uiHelper.setGridStyle(addlInsuredVehiclesGrid);
		
		createIndicatorColumn(addlInsuredVehiclesGrid);
		addlInsuredVehiclesGrid.addColumn(AdditionalInsuredVehicles::getDescription).setHeader(labels.getString("AddlInsuredVehiclesListDescriptionLabel"))
									.setAutoWidth(true);
		addlInsuredVehiclesGrid.addColumn(AdditionalInsuredVehicles::getSerialNum).setHeader(labels.getString("AddlInsuredVehiclesListSerialNoLabel"))
									.setAutoWidth(true);
		
		addlInsuredVehiclesGrid.setDetailsVisibleOnClick(false);
		addlInsuredVehiclesGrid.setSelectionMode(SelectionMode.SINGLE);
		addlInsuredVehiclesGrid.addSelectionListener(event -> refreshToolbar());
		addlInsuredVehiclesGrid.getColumns().stream().forEach(o -> o.setResizable(true));
		setUpGridDbClickEvent(addlInsuredVehiclesGrid);
		
		this.add(addlInsuredVehiclesGrid);
	}
	
	protected void setupNewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_New) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_New).addClickListener(event -> {
				addlInsuredVehiclesUIHelper.newAction(parentSubPolicy, parentAdditionalInsured);
			});
		}
	}
	
	protected void setupUpdateButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Update) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Update).addClickListener(event -> {
				if (!addlInsuredVehiclesGrid.getSelectedItems().isEmpty()) {
					addlInsuredVehiclesUIHelper.updateAction(parentSubPolicy, parentAdditionalInsured, addlInsuredVehiclesGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupDeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Delete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Delete).addClickListener(event -> {
				if (!addlInsuredVehiclesGrid.getSelectedItems().isEmpty()) {
					addlInsuredVehiclesUIHelper.deleteAction(parentSubPolicy, parentAdditionalInsured, addlInsuredVehiclesGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupUndeleteButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_Undelete) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_Undelete).addClickListener(event -> {
				if (!addlInsuredVehiclesGrid.getSelectedItems().isEmpty()) {
					addlInsuredVehiclesUIHelper.undeleteAction(parentSubPolicy, parentAdditionalInsured, addlInsuredVehiclesGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	protected void setupViewButtonEvent() {
		if (this.toolbar.getButton(UIConstants.UIACTION_View) != null) {
			this.toolbar.getButton(UIConstants.UIACTION_View).addClickListener(event -> {
				if (!addlInsuredVehiclesGrid.getSelectedItems().isEmpty()) {
					addlInsuredVehiclesUIHelper.viewAction(parentSubPolicy, parentAdditionalInsured, addlInsuredVehiclesGrid.getSelectedItems().iterator().next());
				}
			});
		}
	}
	
	public void lienholderLessorChanged(AdditionalInsuredList.SelectionChangeEvent event) {
		if (event.getSource() instanceof AdditionalInsuredList) {
			AdditionalInsuredList additionalInsuredList = (AdditionalInsuredList) event.getSource();
			loadData((additionalInsuredList.getParentSubPolicy() != null && additionalInsuredList.getParentSubPolicy() instanceof SpecialityAutoSubPolicy
						? (SpecialityAutoSubPolicy<Risk>)additionalInsuredList.getParentSubPolicy()
						: null), 
						additionalInsuredList.getCurrentAdditionalInsured());
		}
	}
		
	protected void loadData(SpecialityAutoSubPolicy<Risk> parentSubPolicy, AdditionalInsured parentAdditionalInsured) {
		this.parentSubPolicy 		 = parentSubPolicy;
		this.parentAdditionalInsured = parentAdditionalInsured;
		
		List<AdditionalInsuredVehicles> list = new ArrayList<AdditionalInsuredVehicles>();
		if (parentSubPolicy != null && parentSubPolicy.getAdditionalInsuredVehicles() != null &&
				parentAdditionalInsured != null) {
			list.addAll(parentSubPolicy.getAdditionalInsuredVehicles().stream()
								.filter(o -> (o.getAdditionalInsured() != null && parentAdditionalInsured.equals(o.getAdditionalInsured())))
								.collect(Collectors.toList()));
		}
		
		addlInsuredVehiclesGrid.setItems(list);
		
		refreshToolbar();
	}

	protected void refreshToolbar() {
		if (parentSubPolicy == null || parentAdditionalInsured == null) {
			toolbar.disableAllButtons();
		}
		else {
			toolbar.refreshButtons(parentAdditionalInsured, addlInsuredVehiclesGrid);
		}
	}
}
