package com.echelon.ui.components.baseclasses.policy;

import com.ds.ins.domain.entities.HasCustomer;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyTransactionUIHelper;
import com.echelon.ui.components.entities.CustomerView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.helpers.policy.PolicyCoverageUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.echelon.ui.session.UIFactory;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

//@Route(value="PolicyContentView", layout = BasePolicyView.class)
public class BasePolicyContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7723572182764237691L;
	protected VerticalLayout					customerLayout;
	protected CustomerView<HasCustomer>			customerView;
	protected BasePolicyInformationForm			policyInformationForm;
	protected BaseCoverageList					covList;
	protected BasePolicyRatingDetails			ratingDetails;
	protected BasePolicyTransactionUIHelper		policyTransactionUIHelper;

	public BasePolicyContentView() {
		super();
		// TODO Auto-generated constructor stub
		
		this.policyTransactionUIHelper = new BasePolicyTransactionUIHelper();
	}

	public BasePolicyContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this,
												null,
												null);
		}

		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		// TODO Auto-generated method stub
		return new String[] {
		                  labels.getString("PolicyDetailsTabPolicyHolderLabel"),
		                  labels.getString("PolicyDetailsTabPolicyInfoLabel"),
		                  labels.getString("PolicyDetailsTabPolicyCoverageLabel"),
		                  //labels.getString("PolicyDetailsTabRelatedPolicyLabel")
		                  labels.getString("PolicyDetailsTabPolicyRatingDetailsLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		// TODO Auto-generated method stub
		return new Component[] {
				getPolicyHolderView(),
				getPolicyInformationForm(),
				getPolicyCoverageList(),
				getPolicyRatingDetails()
			};
	}
	
	protected Component getPolicyHolderView() {
		if (customerLayout == null) {
			customerLayout = new VerticalLayout();
			customerLayout.setSizeFull();
		}
		
		return customerLayout;
	}
	
	protected Component getPolicyInformationForm() {
		if (policyInformationForm == null) {
			policyInformationForm = new BasePolicyInformationForm(uiMode, uiContainerType);
		}
		
		return policyInformationForm;
	}
	
	protected Component getPolicyCoverageList() {
		if (covList == null) {
			covList = CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.Policy, uiMode, uiContainerType);
		}
		
		return covList;
	}
	
	protected Component getPolicyRatingDetails() {
		if (ratingDetails == null) {
			ratingDetails = new BasePolicyRatingDetails(uiMode, uiContainerType);
		}
		
		return ratingDetails;
	}

	public void loadData() {
		// Need to determine which type of customer (commercial or personal) we're dealing with.
		final String customerType = this.uiPolicyHelper.getCurrentPolicyVersion().getInsurancePolicy()
				.getPolicyCustomer().getCustomerType();
		@SuppressWarnings("unchecked")
		final CustomerUIHelper<HasCustomer> customerUIHelper = (CustomerUIHelper<HasCustomer>) UIFactory.getInstance()
				.getCustomerUIHelper(customerType);
		customerView = customerUIHelper.loadView(customerUIHelper.createCustomerView(uiMode, uiContainerType));
		customerLayout.removeAll();
		customerLayout.add(customerView);
		
		if (policyInformationForm != null) {
			UIFactory.getInstance().getPolicyDetailsUIHelper().loadView(policyInformationForm);
		}
		
		if (covList != null) {
			((PolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Policy))
						.loadView(covList, uiPolicyHelper.getCurrentPolicyTransaction());
		}
		
		if (ratingDetails != null) {
			ratingDetails.loadData();
		}
	}
	
	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		
		policyTransactionUIHelper.loadPolicytransactionData(new BasePolicyTransactionUIHelper.LoadPolicyTransactionDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData();
				refreshPage();
			}
		});
	}	
}
