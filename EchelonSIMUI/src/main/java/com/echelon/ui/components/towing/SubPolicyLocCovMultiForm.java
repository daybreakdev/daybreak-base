package com.echelon.ui.components.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.ds.ins.uicommon.components.DSToolbar;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.components.baseclasses.MultiNewEntryForm;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAdditionalForm;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.BaseTowingSubPolicyCoverageUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.vaadin.flow.component.AbstractField.ComponentValueChangeEvent;
import com.vaadin.flow.component.HasValue.ValueChangeListener;
import com.vaadin.flow.component.grid.HeaderRow;

@SuppressWarnings("serial")
public abstract class SubPolicyLocCovMultiForm extends MultiNewEntryForm<SubPolicyLocCovForm, LocCoverageDto> 
												implements IBaseCovAdditionalForm, IDSSubPolicyLocCovMultiForm {

	private boolean						isLocationMandatory;
	@SuppressWarnings("unused")
	private boolean						isUpdLocAttributes;
	private boolean						isDataInstanceModified;
	
	protected UIConstants.OBJECTTYPES	parentObjectType;
	protected String					mainCoverageType;
	protected String					mainCoverageCode;
	protected IBaseCovAndEndorseForm	coverageForm;

	@SuppressWarnings("rawtypes")
	protected BaseTowingSubPolicyCoverageUIHelper	coverageUIHelper;

	public SubPolicyLocCovMultiForm(String uiMode, UICONTAINERTYPES uiContainerType,
										UIConstants.OBJECTTYPES parentObjectType,
										boolean isLocationMandatory) {
		super(uiMode, uiContainerType, 2); // 2 rows in main form and 3 rows in edit form
		
		this.parentObjectType = parentObjectType;
		this.isDataInstanceModified = false;
		this.coverageUIHelper = (BaseTowingSubPolicyCoverageUIHelper) CoverageUIFactory.getInstance().createCoverageHelper(this.parentObjectType);
	}

	protected IBaseCovAndEndorseForm createCoverageForm() {
		return null;
	}
	
	@Override
	protected void buildGridHeaderForm() {
		if (!UICONTAINERTYPES.Grid.equals(this.uiContainerType)) {
			coverageForm = createCoverageForm();
			
			if (coverageForm != null) {
				BaseLayout form = (BaseLayout) coverageForm;
				form.setSizeFull();
				
				addGridHeaderForm(form);
				//this.add(coverageForm);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected IBusinessEntity convertBusinessEntity(Object object) {
		if (object != null) {
			DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto  = (DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>) object;
			return (IBusinessEntity) dto.getData().getCoverage();
		}
		return super.convertBusinessEntity(object);
	}

	@SuppressWarnings("unchecked")
	public List<LookupTableItem> getAvailableLocations(PolicyLocation include) {
		List<LocCoverageDto> allDtos = getAllData();
		return coverageUIHelper.getAvailableLocations(mainCoverageCode, include, allDtos);
	}

	@Override
	protected DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> createMultiNewEntryDto() {			
		return createMultiNewEntryDto(coverageUIHelper.createNewLocCoverageDto(mainCoverageType, mainCoverageCode), false);
	}
	
	protected DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> createMultiNewEntryDto(LocCoverageDto garLocCoverageDto, boolean fromLoadData) {
		DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto = super.createMultiNewEntryDto();
		dto.setData(garLocCoverageDto);
		
		SubPolicyLocCovForm form = createLocCovForm(garLocCoverageDto, fromLoadData);
		
		dto.setForm(form);
		return dto;		
	}
	
	protected SubPolicyLocCovForm createLocCovForm(LocCoverageDto garLocCoverageDto, boolean fromLoadData) {
		String formUIMode = uiMode;
		if (garLocCoverageDto.isDeletedCoverage() || garLocCoverageDto.wasDeletedCoverage()) {
			formUIMode = UIConstants.UIACTION_View;
		}
		
		SubPolicyLocCovForm form = CoverageUIFactory.getInstance().createSubPolicyLocCovForm(parentObjectType, formUIMode, uiContainerType);
		form.initialize(this, fromLoadData, fromLoadData);
		form.loadData(garLocCoverageDto, getAllData());
		if (!fromLoadData && !UIConstants.UIACTION_View.equals(formUIMode)) {
			form.enableEdit(garLocCoverageDto);
		}
		
		return form;
	}
	
	protected List<DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>> buildAllEntries(List<LocCoverageDto> locCoverageDtos) {
		List<DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>> results = new ArrayList<DSMultiNewEntryDto<SubPolicyLocCovForm,LocCoverageDto>>();
		
		if (locCoverageDtos != null) {
			locCoverageDtos.stream().forEach(o -> results.add(createMultiNewEntryDto(o, true)));
		}
		
		return results.stream().sorted((o1, o2) -> o1.getData().getPolicyLocation().getLocationId().compareTo(o2.getData().getPolicyLocation().getLocationId()))
						.collect(Collectors.toList());
	}
	
	public void locationChanged() {
		getAllEntries().stream().forEach(o -> o.getForm().refreshLocationSelection());
	}

	@Override
	protected HeaderRow addGridHeaderRow() {
		// TODO Auto-generated method stub
		HeaderRow hRow = super.addGridHeaderRow();
		hRow.getCell(entryGrid.getColumnByKey(COLUMNKEY_TOOLBAR))
					.setComponent(createAddToolbar());
		return hRow;
	}
	
	protected DSToolbar createAddToolbar() {
		DSToolbar addToolbar = new DSToolbar();
		
		addToolbar.createButtonWithAction(UIConstants.UIACTION_Add).addClickListener(event -> {
			if (validateEdit()) {
				addAction(true);
			}
		});
		
		return addToolbar;
	}

	@Override
	protected boolean deleteEntry(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		if (dto.getData().isNewCoverage()) {
			return super.deleteEntry(dto);
		}
		else if (CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Location)
					.deleteCoverage(uiConfigHelper.getCurrentProductConfig().getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION), 
									dto.getData().getPolicyLocation(), 
									dto.getData())) {
			isDataInstanceModified = true;
			dto.setForm(createLocCovForm(dto.getData(), false));
			return true;
		}
		
		return false;
	}

	@Override
	public boolean deleteAction(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		boolean ok = super.deleteAction(dto);
		
		if (ok) {
			locationChanged();
		}
		
		return ok;
	}

	@Override
	protected boolean undeleteEntry(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		if (dto.getData().isDeletedCoverage() && !dto.getData().wasDeletedCoverage()) {
			if (CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.Location)
					.undeleteCoverage(uiConfigHelper.getCurrentProductConfig().getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION), 
										dto.getData().getPolicyLocation(), 
										dto.getData())) {
				isDataInstanceModified = true;
				dto.setForm(createLocCovForm(dto.getData(), false));
				return true;
			}
		}
		
		return false;
	}

	@Override
	public boolean undeleteAction(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		boolean ok = super.undeleteAction(dto);
		
		if (ok) {
			locationChanged();
		}
		
		return ok;
	}

	@Override
	protected boolean formValidateEdit(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().validateEdit(false);
	}

	@Override
	protected boolean formSaveEdit(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		// TODO Auto-generated method stub
		return dto.getForm().saveEdit(dto.getData());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void loadData(CoverageDto coverageDto) {
		if (coverageForm != null) {
			coverageForm.loadData(coverageDto);
		}
		
		this.mainCoverageCode   = coverageDto.getCoverageCode();
		this.mainCoverageType   = coverageDto.getCoverageType();
		this.isUpdLocAttributes = SpecialtyAutoConstants.SUBPCY_COV_CODE_SP64.equalsIgnoreCase(this.mainCoverageCode);

		List<LocCoverageDto> locCoverageDtos = coverageUIHelper.buildLocCoverageList(mainCoverageType, mainCoverageCode);
		setAllEntries(buildAllEntries(locCoverageDtos));
	}

	@Override
	public void enableEdit(CoverageDto coverageDto) {
		if (coverageForm != null) {
			coverageForm.enableEdit(coverageDto);
		}
		
		super.enableEdit();
		
		List<DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>> list = getAllEntries();
		if (list != null && !list.isEmpty()) {
			list.stream().forEach(o -> {
				o.getForm().enableEdit(o.getData());
				refreshButtons();
				o.getDeleteButton().setVisible(!o.getData().isDeletedCoverage() && !o.getData().wasDeletedCoverage());
				o.getUndeleteButton().setVisible(o.getData().isDeletedCoverage() && !o.getData().wasDeletedCoverage());
			});
		}
		
		this.initialzieRow();
	}

	@Override
	public boolean validateEdit(boolean showMessage) {
		// TODO Auto-generated method stub
		boolean ok = true; 
		if (coverageForm != null) {
			ok = coverageForm.validateEdit(true); // validateEdit() -> validateEdit(true)
		}
		return ok && super.validateEdit(showMessage);
	}
	
	@Override
	public boolean saveEdit(CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		boolean ok = true; 
		if (coverageForm != null) {
			ok = coverageForm.saveEdit(coverageDto);
		}
		return ok && super.saveEdit();
	}

	@Override
	public boolean anyExtraFields() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void setFormVisible(boolean isVisible) {
		super.setFormVisible(isVisible);
	}

	@Override
	protected void refreshButtons() {
		// TODO Auto-generated method stub
		super.refreshButtons();
		
		if (!UIConstants.UIACTION_View.equals(uiMode)) {
			if (!isLocationMandatory) {
				List<DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>> items = getAllEntries();
				items.stream().forEach(o -> {
					refreshDeleteButton(o, true);
					refreshUndeleteButton(o, false);
				});
				initialzieRow();
			}
		}
	}
	
	@Override
	public void setFocus() {
		if (coverageForm != null) {
			coverageForm.setFocus();
		}
		else {
			List<DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto>> list = getAllEntries();
			if (list != null && !list.isEmpty()) {
				list.get(0).getForm().setFocus();
			}
		}
	}

	@Override
	protected void setFocus(
		DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto) {
		dto.getForm().setFocus();
	}

	@Override
	public boolean hasAnyAdditionalFields() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	protected void refreshDeleteButton(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto,
			boolean isEnabled) {
		if (dto.getData().isDeletedCoverage() || dto.getData().wasDeletedCoverage()) {
			super.refreshDeleteButton(dto, false);
		}
		else {
			super.refreshDeleteButton(dto, isEnabled); // default
		}
	}

	@Override
	protected void refreshUndeleteButton(DSMultiNewEntryDto<SubPolicyLocCovForm, LocCoverageDto> dto,
			boolean isEnabled) {
		if (dto.getData().isDeletedCoverage() &&
			!dto.getData().wasDeletedCoverage() &&
			dto.getData().getPolicyLocation() != null &&
			!dto.getData().getPolicyLocation().isDeleted() &&
			!dto.getData().getPolicyLocation().wasDeleted()) {
			super.refreshUndeleteButton(dto, true);
		}
		else {
			super.refreshUndeleteButton(dto, false);
		}
	}

	@Override
	public boolean isDataInstanceModified() {
		// TODO Auto-generated method stub
		return isDataInstanceModified;
	}

	@Override
	public void setIsReinsuredValueChangeListener(
			ValueChangeListener<? super ComponentValueChangeEvent<?, ?>> listener) {
		if (coverageForm != null) {
			coverageForm.setIsReinsuredValueChangeListener(listener);
		}
	}
}
