package com.echelon.ui.components.baseclasses.policy;

import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.checkbox.Checkbox;
import com.vaadin.flow.data.binder.Binder;

public class CovIsReinsuredField extends Checkbox {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3326188879503480042L;
	protected UIHelper					uiHelper;
	protected Binder<CoverageDto> 		binder;
	protected CoverageDto				coverageDto;

	public CovIsReinsuredField(String label) {
		super(label);
		
		uiHelper = BrokerUI.getHelperSession().getUiHelper();
	}
	
	public void initializeField(CoverageDto dto) {
		this.coverageDto = dto;
		this.coverageDto.setEditIsReinsuredField(this);
	}
	
	public void bindField(Binder<CoverageDto> binder) {
		this.binder = binder;
		binder.removeBinding(this);
		if (coverageDto.isAllowReinsurance() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) {
			binder.forField(this)
								.bind(CoverageDto::isReinsured, 
									  CoverageDto::setIsReinsured);
		}
		else {
			binder.forField(this)
								.bind(CoverageDto::isReinsured, null);
		}
	}
	
	public void determineReadOnly() {
		// The is reinsured checkbox is not user-editable
		uiHelper.setEditFieldsReadonly(this);
		/*
		if (coverageDto.isDeletedCoverage() || coverageDto.wasDeletedCoverage()) {
			uiHelper.setEditFieldsReadonly(this);
		}
		else if (coverageDto.isAllowReinsurance()) {
			uiHelper.setEditFieldsNotReadonly(this);
		}
		else {
			if (this.getValue() != null && this.getValue()) {
				uiHelper.setFieldValue(this, Boolean.FALSE);
			}
			uiHelper.setEditFieldsReadonly(this);
		}
		*/
	}
	
	public boolean setTabindexIfEditiable(int value) {
		if (this.isEnabled() && coverageDto.isAllowReinsurance() && !coverageDto.isDeletedCoverage() && !coverageDto.wasDeletedCoverage()) {
			this.setTabIndex(value);
			return true;
		}
		else {
			this.setTabIndex(-1);
			return false;
		}
	}
}
