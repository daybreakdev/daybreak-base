package com.echelon.ui.components.towing;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.echelon.ui.components.entities.AdditionalInsuredList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.AdditionalInsuredUIHelper;
import com.echelon.ui.helpers.policy.towing.AdjustmentUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyValueUIHelper;
import com.echelon.ui.session.CoverageUIFactory;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.router.Route;

@Route(value="CargoSubPolicyNodeContentView", layout = BasePolicyView.class)
public class CargoSubPolicyContentView extends BaseTabLayout implements IPolicyViewContent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4132388023659436469L;
	private CargoSubPolicyUIHelper 			subPolicyUIHelper =	 (CargoSubPolicyUIHelper) UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
	private CargoSubPolicyView				subPolicyView;
	@SuppressWarnings("unused")
	private	CargoSubPolicyValueUIHelper		cargoValueHelper	= new CargoSubPolicyValueUIHelper();
	private CargoSubPolicyValueList			cargoValueList;
	private SubPolicyCoverageList			covList;
	private AdditionalInsuredList			addInsuredList;
	private AdditionalInsuredUIHelper		addInsuredUIHelper  = new AdditionalInsuredUIHelper();
	private CargoSubPolicyAdjustmentList	adjustmentList;		

	public CargoSubPolicyContentView() {
		super();
	}

	public CargoSubPolicyContentView(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
	}

	@Override
	protected void buildLayout() {
		if (UIConstants.UICONTAINERTYPES.Inplace.equals(uiContainerType)) {
			uiLayoutHelper.buildHeaderLayout(this, null, null);
		}
		super.buildLayout();
	}

	@Override
	protected String[] getTabLabels() {
		return new String[] {
		                  labels.getString("CargoSubPolicyDetailsTitle"),
		                  labels.getString("CargoSubPolicyValueTitle"),
		                  labels.getString("AdditionalInsuredTabLabel"),
		                  labels.getString("CoverageTabLabel"),
		                  labels.getString("AdjustmentTabLabel")
						};
	}

	@Override
	protected Component[] getTabComponents() {
		return new Component[] {
				getSubPolicyView(),
				getValueList(),
				getAdditionalInsuredList(),
				getSubPolicyCoverageList(),
				getAdjustmentList()
		};
	}
	
	protected Component getSubPolicyView() {
		if (subPolicyView == null) {
			subPolicyView = new CargoSubPolicyView(uiMode, UICONTAINERTYPES.Tab);
		}
		
		return subPolicyView;
	}
	
	protected Component getAdditionalInsuredList() {
		if (addInsuredList == null) {
			addInsuredList = new AdditionalInsuredList(uiMode, UICONTAINERTYPES.Tab);
		}

		return addInsuredList;
	}
	
	protected Component getValueList() {
		if (cargoValueList == null) {
			cargoValueList = UIFactory.getInstance().getCargoSubPolicyValueList(uiMode, uiContainerType);
		}

		return cargoValueList;
	}
	
	protected Component getSubPolicyCoverageList() {
		if (covList == null) {
			covList = (SubPolicyCoverageList) CoverageUIFactory.getInstance().createCoverageList(UIConstants.OBJECTTYPES.CGOSubPolicy, uiMode, UICONTAINERTYPES.Tab);
		}
		
		return covList;
	}

	protected Component getAdjustmentList() {
		if (adjustmentList == null) {
			adjustmentList = new CargoSubPolicyAdjustmentList(uiMode, UICONTAINERTYPES.Tab);
		}

		return adjustmentList;
	}
	
	public void loadData(CargoSubPolicy<?> cargoSubPolicy) {
		if (subPolicyView != null) {
			subPolicyUIHelper.loadView(subPolicyView, cargoSubPolicy);
		}
		if (addInsuredList != null) {
			addInsuredUIHelper.loadView(addInsuredList, cargoSubPolicy);
		}
		if (cargoValueList != null) {
			cargoValueHelper.loadView(cargoValueList, cargoSubPolicy);
		}
		if (covList != null) {
			((CargoSubPolicyCoverageUIHelper)CoverageUIFactory.getInstance().createCoverageHelper(UIConstants.OBJECTTYPES.CGOSubPolicy)).loadView(covList, cargoSubPolicy);
		}
		if (adjustmentList != null) {
			new AdjustmentUIHelper().loadView(adjustmentList, cargoSubPolicy);
		}	
	}

	@SuppressWarnings("unchecked")
	@Override
	public void loadContentData(DSTreeGridItem nodeItem) {
		if (nodeItem == null) {
			return;
		}
		
		final CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) nodeItem.getNodeObject();

		subPolicyUIHelper.loadSubpolicyData(new CargoSubPolicyUIHelper.LoadSubPolicyDataCallback() {
			
			@Override
			public void loadComplete() {
				loadData(cargoSubPolicy);
				refreshPage();
				
				tab.setEnabled(nodeItem.getNodeObject() != null);
			}
		}, cargoSubPolicy);
	}
}
