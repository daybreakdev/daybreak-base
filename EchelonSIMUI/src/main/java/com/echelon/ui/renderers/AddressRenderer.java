package com.echelon.ui.renderers;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.uicommon.converters.DSAddressFormat;
import com.vaadin.flow.data.renderer.BasicRenderer;
import com.vaadin.flow.function.ValueProvider;

public class AddressRenderer<SOURCE> extends BasicRenderer<SOURCE, Address> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1672413714530168599L;

	public AddressRenderer(ValueProvider<SOURCE, Address> valueProvider) {
		super(valueProvider);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected String getFormattedValue(Address address) {
		// TODO Auto-generated method stub
		String addLine = (new DSAddressFormat()).format(address.getAddressLine1(), address.getCity(), address.getProvState(), address.getPostalZip());
		return addLine; 
	}

}
