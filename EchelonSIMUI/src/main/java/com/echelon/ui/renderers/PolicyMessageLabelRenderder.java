package com.echelon.ui.renderers;

import java.text.MessageFormat;
import java.util.List;
import java.util.Map;

import com.ds.ins.domain.policy.PolicyMessage;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.data.renderer.BasicRenderer;
import com.vaadin.flow.function.ValueProvider;

public class PolicyMessageLabelRenderder<SOURCE> extends BasicRenderer<SOURCE, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -966968217209860190L;
	private LookupUIHelper 	lookupUIHelper = null;
	private List<PolicyMessage> messages 	   = null;
	private String labelPrefix 	= null;
	private String labelPostFix = null;

	public PolicyMessageLabelRenderder(ValueProvider<SOURCE, String> valueProvider,
							String labelPrefix, String labelPostFix) {
		super(valueProvider);
		// TODO Auto-generated constructor stub
		
		this.lookupUIHelper = BrokerUI.getHelperSession().getLookupUIHelper();
		this.labelPrefix  	= labelPrefix;
		this.labelPostFix 	= labelPostFix;
	}
	
	public void setMessages(List<PolicyMessage> messages) {
		this.messages = messages;
	}

	@Override
	protected String getFormattedValue(String value) {
		if (value == null) {
			return value;
		}
		
		PolicyMessage msg = null;
		if (messages != null && !messages.isEmpty()) {
			msg = messages.stream().filter(o -> value.equalsIgnoreCase(o.getDescription()))
									.findFirst().orElse(null);
		}
		if (msg == null) {
			return value;
		}
		
		if (msg.getRuleId() != null && !msg.getRuleId().isEmpty()) { 
			
			String labelId = (labelPrefix != null ? labelPrefix : "")
							+ msg.getRuleId()
							+ (labelPostFix != null ? labelPostFix : "");
			
			try {
				String result = BrokerUI.getHelperSession().getUiHelper().getSessionLabels().getString(labelId);
				if (msg.getMsgValues() != null && !msg.getMsgValues().isEmpty()) {
					String[] msgValues = new String[msg.getMsgValues().size()];
					
					int ii=0;
					for(Map.Entry<String,String> entry : msg.getMsgValues()) {
						String tableId = entry.getKey();
						String msgVal  = entry.getValue();
						
						if (!tableId.trim().isEmpty()) {
							LookupTableItem lookupItem = null;
							try {
								lookupItem = this.lookupUIHelper.findLookupItemByKey(msgVal, 
														this.lookupUIHelper.getLookupConfigLookup(tableId), 
														true);
							} catch (Exception e) {
							}
							
							if (lookupItem != null) {
								msgValues[ii] = lookupItem.getItemValue();
							}
							else {
								msgValues[ii] = msgVal;
							}
						}
						else {
							msgValues[ii] = msgVal;
						}
						
						ii++;
					}
					
					result = MessageFormat.format(result, msgValues);
				}
				
				return result;
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		else {
			return msg.getMessage();
		}
		
		return value;
	}

}
