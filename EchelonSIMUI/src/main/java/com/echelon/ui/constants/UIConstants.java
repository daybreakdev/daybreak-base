package com.echelon.ui.constants;

import java.io.Serializable;

import com.ds.ins.uicommon.constants.DSUIConstants;
import com.echelon.ui.components.login.LoginUI;

public class UIConstants extends DSUIConstants implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1136892241396142896L;
	
	public static final String ERRORCODE_GENERAL = "PleaseEnterAllRequiredFieldsMessage";
		
	public static enum OBJECTTYPES {
		Policy,
		AUTOSubPolicy,
		CGLSubPolicy,
		GARSubPolicy,
		CGOSubPolicy,
		CPSubPolicy, 
		Vehicle,
		Location,
		Adjustment
	}

	public static enum THEMEVARIANTS {VariantDark, VariantLight}
	
	public static enum POLICYTREENODES {
		Policy, Locations, TransactionSummary, TransactionHistory,
		Reinsurers,
		AutomobileSubPolicyNode, VehicleListNode, Vehicle, 
		CGLSubPolicyNode, CargoSubPolicyNode, GarageSubPolicyNode, CommercialPropertySubPolicyNode, 
		Documents, Messages, Notes,
		DriverListNode, Driver
	}
	
	public static enum NEWPOLICYSTEPS {
		Policy, Customer, CreateNewSubPolices, SaveNewSubPolices, UpdateSubPolices
	}
	
	public static final String CONTENTVIEW_PREFIX	= "";
	public static final String CONTENTVIEW_POSTFIX	= "ContentView";
	public static final String CONTENTVIEW_DEFAULT	= CONTENTVIEW_PREFIX+"Default"+CONTENTVIEW_POSTFIX;
	public static final String POLICY_ROUTE 	  	= CONTENTVIEW_PREFIX+"PolicyOpenView";
	public static final String POLICYSEARCH_ROUTE 	= CONTENTVIEW_PREFIX+"PolicySearchView";
	
	public static final Class<LoginUI>  DEFAULT_PAGE = LoginUI.class;

	public static final String CUST_TYPE_COMM = "COMMERCIAL";
	public static final String CUST_TYPE_PERS = "PERSONAL";
		
	public static final String TRAILERTYPE_NOTTRAILER  = "WT";
	public static final String TRAILERTYPE_CARGO  = "CA";
	public static final String VEHICLEDESC_TRAILER 	   = "TR";
	public static final String VEHICLEDESC_EXCESSTRAILER   = "ET";
	public static final String VEHICLEDESC_DEALERPLATE = "DP";
	public static final String VEHICLEDESC_OPCF27B	   = "OP27";
	
	public static final String ADDLINSUREDRELATIONSHIP = "As known to the Company";
	
	public static final String LIENLESSORTYPE_HLESSOR = "LESSOR";
	public static final String LIENLESSORTYPE_HOLDER  = "LIENHOLDER";
	public static final String LIENLESSORTYPE_DEFAULT = LIENLESSORTYPE_HLESSOR;
	
	public static final String CARGOVALUE_DEFAULT = "Customers' Personal Effects";
	public static final String CARGOVALUE_TOTAL   = "TOTAL";
	
	public static enum COV_EDIT_STATUSES {NOCHANGE, DELETED, UNDELETED, MODIFIED}
	
	public static final String CANCELLATION_REASON_INSUREDREQUEST = "Insured's request";
	
	public final static String UIACTION_ApplyAdjUW 			= "ApplyAdjUW"; 
	public final static String UIACTION_RunDataFixes 		= "RunDataFixes"; 
	public final static String UIACTION_RatingWarn 			= "RatingWarn";
	public final static String UIACTION_IssuePolicyWarn 	= "IssuePolicyWarn";

	public static enum NEWPOLICYTYPES {QUOTE, POLICY, RENEWAL};
	
	public static final int DECIMALPLACES_FACTOR_MIN_DISPLAY = 2;
	public static final int DECIMALPLACES_FACTOR_MAX_DISPLAY = 10;
}
