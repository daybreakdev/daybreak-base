package com.echelon.ui.bordereau;

//import java.awt.Window;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.processes.bordereau.BordereauxProcesses;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.services.bordereau.DSPolicyBordereau;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.bordereau.BourderauxUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

public class BordereauSummaryForm extends VerticalLayout  {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5806286856703979201L;
	private DatePicker datePickerFrom;
	private DatePicker datePickerTo;
	private Button cancelButton;
	private Button searchButton;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private FormLayout form;
	private	ComboBox<String>			programCodes;
	private List<String> programCode;
	private static final String INSPCY_BASE_QUERY = "from PolicyTransaction ptrans ";
	private static final String INSPCY_ENTITY_REF = "ptrans";
	protected IGenericPolicyProcessing 	policyProcessing;
	private ResourceBundle		labels;


	public BordereauSummaryForm(String delimiter, IGenericPolicyProcessing 	policyProcessing, ResourceBundle label) {
		super();
		this.policyProcessing = policyProcessing;
		this.labels = label;
		// TODO Auto-generated constructor stub

		createButtonLayout(delimiter);
		buildFields();
		buildForm();

	}

	private void buildFields() {
		LocalDate now = LocalDate.now();

		programCodes = new ComboBox<String>();
		programCodes.setLabel(labels.getString("BordereauProgramCodeLabel"));
		programCodes.setItems("TO", "LO");
		programCodes.addValueChangeListener(event -> {
			if (event.getValue() != null) {
				programCode = new ArrayList<>(); 
				String value = event.getValue();
				if (value.equals("LO")) {
					programCode.add(ConfigConstants.PRODUCTCODE_LHT);
				}else if (value.equals("TO")) {
					programCode.add(ConfigConstants.PRODUCTCODE_TOWING_NONFLEET);
					programCode.add(ConfigConstants.PRODUCTCODE_TOWING);
				}
				
			}
		});

		datePickerFrom = new DatePicker();
		datePickerFrom.setLabel(labels.getString("BordereauFromDateLabel"));
		datePickerFrom.setValue(now);
		dateFrom = now;
		datePickerFrom.addValueChangeListener(event -> {
			if (event.getValue() != null) {
				dateFrom = event.getValue();
			}
		});

		
		datePickerTo = new DatePicker();
		datePickerTo.setLabel(labels.getString("BordereauToDateLabel"));
		datePickerTo.setValue(now);
		dateTo = now;
		datePickerTo.addValueChangeListener(event -> {
			if (event.getValue() != null) {
				dateTo = event.getValue();
			}

		});

	}

	private void buildForm() {

		HorizontalLayout hLayoutProduct = new HorizontalLayout();
		hLayoutProduct.add(programCodes);
		
		HorizontalLayout hLayoutDates = new HorizontalLayout();
		hLayoutDates.add(datePickerFrom);
		hLayoutDates.add(datePickerTo);
		
		HorizontalLayout hLayoutButtons = new HorizontalLayout();
		cancelButton.setWidth("192px");
		searchButton.setWidth("192px");
		hLayoutButtons.add(cancelButton);
		hLayoutButtons.add(searchButton);
		this.add(hLayoutProduct);
		this.add(hLayoutDates);
		this.add(hLayoutButtons);
		
	}
	
	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateto() {
		return dateTo;
	}
	
	public List<String> getProgramCode() {
		return programCode;
	}
	
	private void createButtonLayout(String delimiter) {
		cancelButton = new Button(labels.getString("MainViewMenuItemCancelLabel"), eventsCancel -> {

			List<Component> currentlyWindow=UI.getCurrent().getChildren().collect(Collectors.toList());

			Dialog currentlyForm = (Dialog) currentlyWindow.get(1);
			currentlyForm.close();

		});
		
		UIHelper uiHelper = BrokerUI.getHelperSession().getUiHelper();
		searchButton = new Button(labels.getString("MainViewMenuItemPolicyInquiryLabel"), eventsSearch -> {
			try {
				if (dateFrom.plusDays(31).isBefore(dateTo)) {
					uiHelper.notificationMessage(labels.getString("BordereauErrorRangeDatesLabel")); 
				}else {
					BourderauxUIHelper brdUiHelper = new BourderauxUIHelper();
					policyProcessing = brdUiHelper.getpolicyProcessing(/*programCode.get(0)*/ConfigConstants.PRODUCTCODE_LHT);
					BordereauxProcesses bordereauxProcesses = new BordereauxProcesses();
				
					DSPolicyBordereau dv = new DSPolicyBordereau();
					//List<PolicyTransaction> ptlist = bordereauxProcesses.generateBordereauSummary(dateFrom, dateTo, delimiter, programCode);//I'm commenting this out to make a test that Sangeetha requested
					List<PolicyTransaction> ptlist = bordereauxProcesses.generateBordereauSummaryTemporal(dateFrom, dateTo, delimiter, true, programCode);
					StreamResource result = dv.downloadFile(ptlist, delimiter, true, null, policyProcessing);
				
				
					final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry().registerResource(result);
					UI.getCurrent().getPage().executeJavaScript("window.open($0, $1)", registration.getResourceUri().toString(), "_blank");
				}
			} catch (InsurancePolicyException e) {
				// TODO Auto-generated catch block
				uiHelper.notificationException(e);
			}
			
			/*
			//Timestamp timestampFrom = Timestamp.valueOf(dateFrom.atStartOfDay());
			//Timestamp timestampTo = Timestamp.valueOf(dateTo.atStartOfDay());
			LocalDateTime timestampFrom = LocalDateTime.of(dateFrom, LocalTime.of(0, 0, 0, 0));
			LocalDateTime timestampTo = LocalDateTime.of(dateTo, LocalTime.of(0, 0, 0, 0)).plusDays(1).minusSeconds(1);

			PolicyTransactionDAO dao = new PolicyTransactionDAO();
			List<PolicyTransaction> ptList=dao.getAllPoliciesRangeDates(timestampFrom, timestampTo);
			
			List<PolicyTransaction> ptListIssued = ptList.stream().filter(o -> o.getPolicyVersion().getBusinessStatus().equals("ISSUED")).collect(Collectors.toList());


			DSPolicyBordereau dv = new DSPolicyBordereau();
			StreamResource result = dv.downloadFile(ptListIssued, delimiter, true);
	        final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry().registerResource(result);
	        UI.getCurrent().getPage().executeJavaScript("window.open($0, $1)", registration.getResourceUri().toString(), "_blank");
			*/
		});
		
		
	}
}

