package com.echelon.ui.bordereau;

import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.processes.bordereau.BordereauxProcesses;
import com.echelon.services.bordereau.DSPolicyBordereau;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.bordereau.BourderauxUIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;
import com.vaadin.flow.server.StreamResource;

public class BordereauAllForm extends VerticalLayout {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5806286856703979201L;
	private DatePicker datePickerFrom;
	private DatePicker datePickerTo;
	private Button cancelButton;
	private Button searchButton;
	private LocalDate dateFrom;
	private LocalDate dateTo;
	private FormLayout form;
	private PolicyTransaction<?> policyTransaction;
	protected IGenericPolicyProcessing 	policyProcessing;
	protected PolicyUIHelper			policyUIHelper;
	private ResourceBundle		labels;



	public BordereauAllForm(String delimiter, PolicyTransaction<?> policyTransaction, IGenericPolicyProcessing 	policyProcessing, ResourceBundle label) {
		super();
		this.policyProcessing = policyProcessing;
		policyUIHelper = BrokerUI.getHelperSession().getPolicyUIHelper();
		// TODO Auto-generated constructor stub
		this.policyTransaction=policyTransaction;
		this.labels = label;
		createButtonLayout(delimiter);
		buildFields();
		buildForm();

	}

	private void buildFields() {
		LocalDate now = LocalDate.now();



		datePickerFrom = new DatePicker();
		datePickerFrom.setLabel(labels.getString("BordereauFromDateLabel"));
		datePickerFrom.setValue(now);
		datePickerFrom.setLocale(Locale.US);
		dateFrom = now;
		datePickerFrom.addValueChangeListener(event -> {
			if (event.getValue() != null) {
				dateFrom = event.getValue();
			}
		});

		
		datePickerTo = new DatePicker();
		datePickerTo.setLabel(labels.getString("BordereauToDateLabel"));
		datePickerTo.setValue(now);
		datePickerTo.setLocale(Locale.US);
		dateTo = now;
		datePickerTo.addValueChangeListener(event -> {
			if (event.getValue() != null) {
				dateTo = event.getValue();
			}

		});

	}

	private void buildForm() {
		form = new FormLayout();
		form.setResponsiveSteps(new FormLayout.ResponsiveStep("10em", 2));
		form.add(datePickerFrom);
		form.add(datePickerTo);
		form.add(cancelButton);
		form.add(searchButton);
		this.add(form);
		
	}
	
	public LocalDate getDateFrom() {
		return dateFrom;
	}

	public LocalDate getDateto() {
		return dateTo;
	}

	private void createButtonLayout(String delimiter) {
		cancelButton = new Button(labels.getString("MainViewMenuItemCancelLabel"), eventsCancel -> {

			List<Component> currentlyWindow=UI.getCurrent().getChildren().collect(Collectors.toList());

			Dialog currentlyForm = (Dialog) currentlyWindow.get(1);
			currentlyForm.close();

		});

		UIHelper uiHelper = BrokerUI.getHelperSession().getUiHelper();
		searchButton = new Button(labels.getString("MainViewMenuItemPolicyInquiryLabel"), eventsSearch -> {

			try {
				BordereauxProcesses bordereauxProcesses = new BordereauxProcesses();
				BourderauxUIHelper bordereauHelper = new BourderauxUIHelper();
				
				PolicyTransaction policyTransaction=null;
				if (policyUIHelper!=null ) {
					policyTransaction=policyUIHelper.getCurrentPolicyTransaction();
				}
				
				List<PolicyTransaction> ptAllList = bordereauHelper.getAllPolicyTransactions(policyTransaction);
				
				DSPolicyBordereau dv = new DSPolicyBordereau();
				List<PolicyTransaction> ptlist = bordereauxProcesses.generateBordereauAll(policyTransaction, dateFrom, dateTo, delimiter, false);
				StreamResource result = dv.downloadFile(ptlist, delimiter, false, ptAllList, policyProcessing);
				
		        final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry().registerResource(result);
		        UI.getCurrent().getPage().executeJavaScript("window.open($0, $1)", registration.getResourceUri().toString(), "_blank");
			} catch (InsurancePolicyException e) {
				uiHelper.notificationException(e);
			}
			/*
			PolicyTransactionDAO dao = new PolicyTransactionDAO();
			List<PolicyTransaction> ptList=dao.getAllPolicyTransactions(policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum());
			
			List<PolicyTransaction> ptListIssued = ptList.stream().filter(o -> (o.getPolicyVersion().getVersionDate().isAfter(dateFrom)  && o.getPolicyVersion().getVersionDate().isBefore(dateTo)) || o.getPolicyVersion().getVersionDate().isEqual(dateFrom)).collect(Collectors.toList());
			
			DSPolicyBordereau dv = new DSPolicyBordereau();
			StreamResource result = dv.downloadFile(ptListIssued, delimiter, false);
	        final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry().registerResource(result);
	        UI.getCurrent().getPage().executeJavaScript("window.open($0, $1)", registration.getResourceUri().toString(), "_blank");
			*/	        
		});
		
		
	}

}
