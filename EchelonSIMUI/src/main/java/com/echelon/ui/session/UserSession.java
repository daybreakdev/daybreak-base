package com.echelon.ui.session;

import java.util.ResourceBundle;

import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.ui.components.MainView;
import com.echelon.ui.components.baseclasses.IPolicyViewContent;
import com.echelon.ui.components.baseclasses.dto.CurrentPolicy;
import com.echelon.ui.components.baseclasses.dto.PolicySearchCriteria;
import com.echelon.ui.components.baseclasses.policy.BasePolicySearchContentView;
import com.echelon.ui.components.baseclasses.policy.BasePolicyTreeNavigator;
import com.echelon.ui.components.baseclasses.policy.BasePolicyView;
import com.vaadin.flow.component.UI;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.session.DSUserSession;

public class UserSession extends DSUserSession {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8625959079178339261L;
	private CurrentPolicy					currentPolicy;
	
	private Integer 						brokerNo;
	private Integer 						brokerOfficeNo;
	private String							brokerProvince;

	private MainView						mainView;
	private BasePolicyTreeNavigator 		policyTreeNavigator;
	private BasePolicyView					policyView;
	private DSTreeGridItem					policyNodeItem;
	private IPolicyViewContent				policyNodeContent;
	
	private BasePolicySearchContentView		policySearchView;
	private PolicySearchCriteria			policySearchCriteria;

	public UserSession() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public void resetUserSession() {
		this.mainView 			= null;
		this.brokerNo    	 	= null;
		this.brokerOfficeNo  	= null;
		this.brokerProvince  	= null;

		super.resetUserSession();
	}
	
	public void resetPolicyViewComponents() {
		setPolicyView(null);
		setPolicyTreeNavigator(null);
		setPolicyProductConfig(null);
		setLookupConfig(null);
		setLifecycleStatusConfig(null);
		setUiProductConfig(null);
		setPolicyNodeItem(null);
		setPolicyNodeContent(null);
		setOpenedTabIndex(null);
	}
	
	public void resetCurrentPolicy() {
		this.currentPolicy = null;
	}
	
	public BasePolicyTreeNavigator getPolicyTreeNavigator() {
		return policyTreeNavigator;
	}

	public void setPolicyTreeNavigator(BasePolicyTreeNavigator policyTreeNavigator) {
		this.policyTreeNavigator = policyTreeNavigator;
	}

	public MainView getMainView() {
		return mainView;
	}

	public void setMainView(MainView mainView) {
		this.mainView = mainView;
	}

	public BasePolicyView getPolicyView() {
		return policyView;
	}

	public void setPolicyView(BasePolicyView policyView) {
		this.policyView = policyView;
	}

	public DSTreeGridItem getPolicyNodeItem() {
		return policyNodeItem;
	}

	public void setPolicyNodeItem(DSTreeGridItem policyNodeItem) {
		this.policyNodeItem = policyNodeItem;
	}

	public IPolicyViewContent getPolicyNodeContent() {
		return policyNodeContent;
	}

	public void setPolicyNodeContent(IPolicyViewContent policyNodeContent) {
		this.policyNodeContent = policyNodeContent;
	}

	public PolicySearchCriteria getPolicySearchCriteria() {
		return policySearchCriteria;
	}

	public void setPolicySearchCriteria(PolicySearchCriteria policySearchCriteria) {
		this.policySearchCriteria = policySearchCriteria;
	}

	public CurrentPolicy getCurrentPolicy() {
		if (this.currentPolicy == null) {
			this.currentPolicy = new CurrentPolicy();
		}
		
		return currentPolicy;
	}

	public void setCurrentPolicy(PolicyVersion policyVersion, PolicyTransaction<?> policyTransaction, 
									SpecialtyPolicyDTO specialtyPolicyDTO,
									Object lastData) {
		this.getCurrentPolicy().setCurrentPolicy(policyVersion, policyTransaction, specialtyPolicyDTO, lastData);
	}

	public BasePolicySearchContentView getPolicySearchView() {
		return policySearchView;
	}

	public void setPolicySearchView(BasePolicySearchContentView policySearchView) {
		this.policySearchView = policySearchView;
	}

	public Integer getBrokerNo() {
		return brokerNo;
	}

	public void setBrokerNo(Integer brokerNo) {
		this.brokerNo = brokerNo;
	}

	public Integer getBrokerOfficeNo() {
		return brokerOfficeNo;
	}

	public void setBrokerOfficeNo(Integer brokerOfficeNo) {
		this.brokerOfficeNo = brokerOfficeNo;
	}

	public String getBrokerProvince() {
		return brokerProvince;
	}

	public void setBrokerProvince(String brokerProvince) {
		this.brokerProvince = brokerProvince;
	}

	@Override
	public ResourceBundle getCommonLabels() {
		if (super.getCommonLabels() == null) {
			setCommonLabels(ResourceBundle.getBundle("CommonLabels", UI.getCurrent().getLocale()));
		}
		
		return super.getCommonLabels();
	}

	@Override
	public ResourceBundle getSessionLabels() {
		if (super.getSessionLabels() == null) {
			setSessionLabels(ResourceBundle.getBundle("PolicyLabels", UI.getCurrent().getLocale()));
		}
		
		return super.getSessionLabels();
	}
	
}
