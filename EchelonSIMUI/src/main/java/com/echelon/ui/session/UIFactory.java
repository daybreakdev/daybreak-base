package com.echelon.ui.session;

import java.io.Serializable;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletContext;

import org.apache.commons.jcs.access.exception.InvalidArgumentException;
import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.dto.DSGenericFieldGroupDto;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.reports.impl.ReportServiceImpl;
import com.echelon.ui.components.baseclasses.GenericFieldsForm;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyActionsUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyDetailsUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseRatefactorUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyCancelConfirmForm;
import com.echelon.ui.components.baseclasses.policy.BasePolicyTreeNavigator;
import com.echelon.ui.components.nonfleet.NonFleetGenericFieldsForm;
import com.echelon.ui.components.nonfleet.NonFleetVehicleForm;
import com.echelon.ui.components.policy.wheels.NonFleetPolicyTreeNavigator;
import com.echelon.ui.components.towing.CargoSubPolicyValueForm;
import com.echelon.ui.components.towing.CargoSubPolicyValueList;
import com.echelon.ui.components.towing.CargoSubPolicyValueMultiForm;
import com.echelon.ui.components.towing.TowingPolicyCancelConfirmForm;
import com.echelon.ui.components.towing.TowingPolicyTreeNavigator;
import com.echelon.ui.components.towing.TowingTransactionSummaryDetailForm;
import com.echelon.ui.components.towing.VehicleForm;
import com.echelon.ui.components.trucking.LHTCargoSubPolicyValueForm;
import com.echelon.ui.components.trucking.LHTCargoSubPolicyValueList;
import com.echelon.ui.components.trucking.LHTVehicleForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.CommercialCustomerUIHelper;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.helpers.policy.NewPolicyUIHelper;
import com.echelon.ui.helpers.policy.NewSpecialtyAutoPolicyUIHelper;
import com.echelon.ui.helpers.policy.SpecialtyAutoPolicyActionsUIHelper;
import com.echelon.ui.helpers.policy.nonfleet.NonFleetPolicyActionsUIHelper;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyUIHelper;
import com.echelon.ui.helpers.policy.towing.TowingPolicyDetailsUIHelper;
import com.echelon.ui.helpers.policy.trucking.LHTCargoSubPolicyUIHelper;
import com.echelon.ui.process.policy.SpecialtyAutoPolicyProcessor;
import com.echelon.ui.process.policy.nonfleet.NonFleetPolicyProcessor;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.icon.Icon;

public class UIFactory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5439359593959122904L;
	private static final Logger log = Logger.getLogger(UIFactory.class.getName());
	
	private static UIFactory instance;
	
	public static UIFactory getInstance() {
		if (instance == null) {
			synchronized (UIFactory.class) {
				instance = new UIFactory();
			}
		}
		
		return instance;
	}

	public CustomerUIHelper<?> getCustomerUIHelper(String customerType) {
		// Can't switch on a null value
		if (customerType == null) {
			customerType = StringUtils.EMPTY;
		}

		switch (customerType) {
		case UIConstants.CUST_TYPE_COMM:
			return new CommercialCustomerUIHelper();
		case UIConstants.CUST_TYPE_PERS:
			return new PersonalCustomerUIHelper();
		default:
			log.log(Level.SEVERE, "Unknown customer type {0}. Cannot create customer UI helper", customerType);
			throw new InvalidArgumentException("Unknown customer type");
		}
	}
	
	public TowingTransactionSummaryDetailForm getTransactionSummaryDetailForm() {
		return new TowingTransactionSummaryDetailForm();
	}
	
	@SuppressWarnings("rawtypes")
	public BaseSpecialtySubPolicyContentUIHelper getSpecialtySubPolicyContentUIHelper(String subpolicyCode) {
		switch (subpolicyCode) {
		case SpecialtyAutoConstants.SUBPCY_CODE_AUTO:
			return new AutoSubPolicyUIHelper();
					
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			return new CGLSubPolicyUIHelper();
			
		case SpecialtyAutoConstants.SUBPCY_CODE_GARAGE:
			return new GarageSubPolicyUIHelper();
			
		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			if (ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(getCurrentProductCd()))
			{
				return new LHTCargoSubPolicyUIHelper();
			}
			return new CargoSubPolicyUIHelper();
			
		case SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY:
			return new CommercialPropertySubPolicyUIHelper();

		default:
			break;		
		}
		
		return null;
	}
	
	public BasePolicyActionsUIHelper getPolicyActionsUIHelper() {
		if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(getCurrentProductCd())) {
			return new NonFleetPolicyActionsUIHelper();
		}
		
		return new SpecialtyAutoPolicyActionsUIHelper();
	}
	
	public BasePolicyCancelConfirmForm getPolicyCancelConfirmForm(String uiMode, UICONTAINERTYPES uiContainerType,
																	String policyAction,
																	String policyVerTxnType,
																	String policyVerStatus) {
		return new TowingPolicyCancelConfirmForm(uiMode, uiContainerType, policyAction, policyVerTxnType, policyVerStatus);
	}
	
	public ReportServiceImpl getReportService(Properties properties, ServletContext context) throws Exception {
		return ReportServiceImpl.initService(properties, context);
	}
	
	public ReportServiceImpl getReportService() throws Exception {
		return ReportServiceImpl.getService();
	}
	
	@SuppressWarnings("rawtypes")
	public NewPolicyUIHelper getNewPolicyUIHelper(String productCd) {
		return new NewSpecialtyAutoPolicyUIHelper();
	}
	
	public BaseRatefactorUIHelper getRatefactorUIHelper(String productCode) {
		return new BaseRatefactorUIHelper();
	}
	
	public Icon getPolicyTreeNodeIcon(DSTreeGridItem treeGridItem) {
		Icon icon = null;
		
		if (treeGridItem.getNodeObject() != null && treeGridItem.getNodeObject() instanceof IBusinessEntity) {
			IBusinessEntity busEntity = (IBusinessEntity) treeGridItem.getNodeObject();
			icon = BrokerUI.getHelperSession().getPolicyUIHelper().getBusinessStatusIcon(busEntity);
			if (icon == null) {
				boolean anyChange = false;
				
				if (ConfigConstants.TREENODETYPE_CargoSubPolicyNode.equalsIgnoreCase(treeGridItem.getNodeType())) {
					anyChange = getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)
									.checkIfChildDataIsChangedInCurrentVersion(treeGridItem);
				}
				
				if (anyChange) {
					icon = BrokerUI.getHelperSession().getUiHelper().createIconModified();
				}
			}
		}
		
		return icon;
	}
	
	public VehicleForm createVehicleForm(String uiMode, UICONTAINERTYPES uiContainerType, String productCd) {
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_LHT:
				return new LHTVehicleForm(uiMode, uiContainerType);

			case ConfigConstants.PRODUCTCODE_TOWING_NONFLEET:
				return new NonFleetVehicleForm(uiMode, uiContainerType);

			default:
				break;
			}
		}
		
		return new VehicleForm(uiMode, uiContainerType);
	}
	
	public BasePolicyTreeNavigator getPolicyTreeNavigator(String productCd) {
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_TOWING_NONFLEET:
				return new NonFleetPolicyTreeNavigator();

			default:
				return new TowingPolicyTreeNavigator();
			}
		}
		
		return new BasePolicyTreeNavigator();
	}
	
	@SuppressWarnings("rawtypes")
	public SpecialtyAutoPolicyProcessor getPolicyActionsProcessor(String processAction, PolicyTransaction policyTransaction) {
		String productCd = getCurrentProductCd();
		if (policyTransaction != null) {
			productCd = policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd();
		}
		
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_TOWING_NONFLEET:
				return new NonFleetPolicyProcessor(processAction, policyTransaction);
			default:
				break;
			}
		}
		
		return new SpecialtyAutoPolicyProcessor(processAction, policyTransaction);
	}
	
	protected String getCurrentProductCd() {
		String result = null;
		
		if (BrokerUI.getHelperSession().getConfigUIHelper().getCurrentProductConfig() != null) {
			result = BrokerUI.getHelperSession().getConfigUIHelper().getCurrentProductConfig().getProductCd();
		}

		return result;
	}
	
	public GenericFieldsForm getGenericFieldsForm(String uiMode, UICONTAINERTYPES uiContainerType, DSGenericFieldGroupDto valueGroupDto,
			FormLayout followFormLayout, CoverageDto coverageDto) {
		String productCd = getCurrentProductCd();
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_TOWING_NONFLEET:
				return new NonFleetGenericFieldsForm(uiMode, uiContainerType, valueGroupDto, followFormLayout, coverageDto);

			default:
				break;
			}
		}
		
		return new GenericFieldsForm(uiMode, uiContainerType, valueGroupDto, followFormLayout, coverageDto);
	}
	
	public BasePolicyDetailsUIHelper getPolicyDetailsUIHelper() {
		return new TowingPolicyDetailsUIHelper();
	}
	
	public CargoSubPolicyValueList getCargoSubPolicyValueList(String uiMode, UICONTAINERTYPES uiContainerType) {
		String productCd = getCurrentProductCd();
		
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_LHT:
				return new LHTCargoSubPolicyValueList(uiMode, uiContainerType);
			default:
				break;
			}
		}
		
		return new CargoSubPolicyValueList(uiMode, uiContainerType);
	}
	
	public CargoSubPolicyValueForm getCargoSubPolicyValueForm(String uiMode, UIConstants.UICONTAINERTYPES uiContainerType,
																CargoSubPolicyValueMultiForm parentForm) {
		String productCd = getCurrentProductCd();
		
		if (StringUtils.isNotBlank(productCd)) {
			switch (productCd) {
			case ConfigConstants.PRODUCTCODE_LHT:
				return new LHTCargoSubPolicyValueForm(uiMode, uiContainerType, parentForm);
			default:
				break;
			}
		}
		
		return new CargoSubPolicyValueForm(uiMode, uiContainerType, parentForm);		
	}
}
