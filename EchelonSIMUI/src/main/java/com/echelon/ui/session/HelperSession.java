package com.echelon.ui.session;

import com.ds.ins.uicommon.components.DSBaseApplicationStoreHelper;
import com.ds.ins.uicommon.helpers.DSGenericUIHelper;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.ds.ins.uicommon.session.DSHelperSession;
import com.echelon.ui.components.baseclasses.helpers.LayoutUIHelper;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.SessionUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.helpers.policy.PolicyUIHelper;

public class HelperSession extends DSHelperSession<DSBaseApplicationStoreHelper, UIHelper, DSNotificatonUIHelper, SessionUIHelper, ProductConfigUIHelper, LookupUIHelper, LayoutUIHelper, DSGenericUIHelper> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8180879165678576964L;
	private PolicyUIHelper policyUIHelper;
	
	public HelperSession() {
		super();
		// TODO Auto-generated constructor stub
		
		// remove this when it becomes a bean
		postConstruct();
	}
	
	@Override
	protected void postConstruct() {
		super.postConstruct();
		
		createPolicyUIHelper();
	}

	protected void createPolicyUIHelper() {
		if (policyUIHelper == null) {
			policyUIHelper = new PolicyUIHelper();
		}
	}
	
	public PolicyUIHelper getPolicyUIHelper() {
		return policyUIHelper;
	}

	@Override
	protected void createUIHelper() {
		// TODO Auto-generated method stub
		uiHelper = new UIHelper();
	}

	@Override
	protected void createSessionUIHelper() {
		// TODO Auto-generated method stub
		sessionUIHelper = new SessionUIHelper((UIHelper) uiHelper);
	}

	@Override
	protected void createProductConfigUIHelper() {
		// TODO Auto-generated method stub
		configUIHelper = new ProductConfigUIHelper();
	}

	@Override
	protected void createLookupUIHelper() {
		// TODO Auto-generated method stub
		lookupUIHelper = new LookupUIHelper();
	}

	@Override
	protected void createLayoutUIHelper() {
		// TODO Auto-generated method stub
		layoutUIHelper = new LayoutUIHelper();
	}
	
}
