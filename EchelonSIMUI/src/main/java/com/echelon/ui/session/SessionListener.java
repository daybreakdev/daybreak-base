package com.echelon.ui.session;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;

import com.vaadin.flow.server.ServiceException;
import com.vaadin.flow.server.SessionDestroyEvent;
import com.vaadin.flow.server.SessionDestroyListener;
import com.vaadin.flow.server.SessionInitEvent;
import com.vaadin.flow.server.SessionInitListener;
import com.vaadin.flow.server.VaadinServlet;

//@WebServlet
public class SessionListener extends VaadinServlet
								implements SessionInitListener, SessionDestroyListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4853615270544916040L;

	@Override
	protected void servletInitialized() throws ServletException {
		// TODO Auto-generated method stub
		super.servletInitialized();
		
		getService().addSessionInitListener(this);
		getService().addSessionDestroyListener(this);
	}

	@Override
	public void sessionDestroy(SessionDestroyEvent event) {
		// TODO Auto-generated method stub
		System.out.println("sessionDestroy");
	}

	@Override
	public void sessionInit(SessionInitEvent event) throws ServiceException {
		// TODO Auto-generated method stub
		System.out.println("sessionInit");
	}

}
