package com.echelon.ui.session;

import com.ds.ins.uicommon.session.DSSessionUI;

public class BrokerUI extends DSSessionUI {
	
	public static void initSession() {
		if (getUserSession() == null) {
			setUserSession(new UserSession());
		}
		
		if (getHelperSession() == null) {
			setHelperSession(new HelperSession());
		}
		
		initErrorHandler();
	}

	public static UserSession getUserSession() {
		return (UserSession) DSSessionUI.getUserSession();
	}
	
	public static HelperSession getHelperSession() {
		return (HelperSession) DSSessionUI.getHelperSession();
	}

}
