package com.echelon.ui.session;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.system.UserProfile;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCovReinsuranceUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCovAddlFieldsForm;
import com.echelon.ui.components.baseclasses.policy.BaseCovAndEndorseForm;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageList;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageListEdit;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageReinsuranceList;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAdditionalForm;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.baseclasses.process.policy.BaseCoverageProcessor;
import com.echelon.ui.components.endorsements.OPCF20EndorsementForm;
import com.echelon.ui.components.nonfleet.NonFleetVehicleCoverageList;
import com.echelon.ui.components.towing.CGLSubPolicyCoverageForm;
import com.echelon.ui.components.towing.CGLSubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.CargoSubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.GarageSubPolicyCoverageListEdit;
import com.echelon.ui.components.towing.GarageSubPolicyLocCovForm;
import com.echelon.ui.components.towing.GarageSubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.SubPolicyCoverageList;
import com.echelon.ui.components.towing.SubPolicyLocCovForm;
import com.echelon.ui.components.towing.VehicleCoverageForm;
import com.echelon.ui.components.towing.VehicleCoverageList;
import com.echelon.ui.components.towing.VehicleCoverageListEdit;
import com.echelon.ui.components.trucking.CommercialPropertySubPolicyLocCovMultiForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.PolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.reinsurance.PolicyCovReinsuranceUIHelper;
import com.echelon.ui.helpers.policy.towing.AutoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.BaseTowingSubPolicyCoverageConfigUIHelper;
import com.echelon.ui.helpers.policy.towing.CGLSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.GarageSubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.towing.VehicleCoverageUIHelper;
import com.echelon.ui.helpers.policy.trucking.CommercialPropertySubPolicyCoverageUIHelper;
import com.echelon.ui.helpers.policy.trucking.LHTCargoSubPolicyCoverageUIHelper;
import com.echelon.ui.process.towing.CGLSubPolicyCoverageProcessor;
import com.echelon.ui.process.towing.TowingSubPolicyCoverageProcessor;
import com.echelon.ui.process.towing.VehicleCoverageProcessor;
import com.echelon.utils.SpecialtyAutoConstants;

public class CoverageUIFactory implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -650937667402309374L;
	private static CoverageUIFactory instance; 

	public CoverageUIFactory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static CoverageUIFactory getInstance() {
		if (instance == null) {
			synchronized (CoverageUIFactory.class) {
				instance = new CoverageUIFactory();
			}
		}
		
		return instance;
	}
	
	public BaseCoverageList createCoverageList(UIConstants.OBJECTTYPES objectType, String uiMode, UICONTAINERTYPES uiContainerType) {
		switch (objectType) {
		case Policy:
			break;

		case AUTOSubPolicy:
		case CGLSubPolicy:
		case GARSubPolicy:
		case CGOSubPolicy:
		case CPSubPolicy:
			return new SubPolicyCoverageList(uiMode, uiContainerType, objectType);
			
		case Vehicle:
			if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(getCurrentProductCd())) {
				return new NonFleetVehicleCoverageList(uiMode, uiContainerType, objectType);
			}
			
			return new VehicleCoverageList(uiMode, uiContainerType, objectType);
			
		default:
			break;
		}
		
		return new BaseCoverageList(uiMode, uiContainerType, objectType);
	}
	
	public BaseCoverageListEdit createCoverageListEdit(UIConstants.OBJECTTYPES objectType, String uiMode, UICONTAINERTYPES uiContainerType) {
		switch (objectType) {
		case Policy:
			break;

		case AUTOSubPolicy:
			break;

		case CGLSubPolicy:
			break;

		case GARSubPolicy:
			return new GarageSubPolicyCoverageListEdit(uiMode, uiContainerType, objectType);

		case CGOSubPolicy:
			break;

		case Vehicle:
			return new VehicleCoverageListEdit(uiMode, uiContainerType, objectType);

		default:
			break;
		}
		
		return new BaseCoverageListEdit(uiMode, uiContainerType, objectType);
	}
	
	public IBaseCovAndEndorseForm createCoverageForm(UIConstants.OBJECTTYPES objectType, String coverageType, String coverageCode, String uiMode, UICONTAINERTYPES uiContainerType) {
		switch (objectType) {
		case Policy:
			break;

		case AUTOSubPolicy:
			break;

		case CGLSubPolicy:
			if (SpecialtyAutoConstants.CGL_UNITS_COVERAGS.contains(coverageCode)) {
				return new CGLSubPolicyCoverageForm(uiMode, uiContainerType);
			}
			if (SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS.contains(coverageCode)) {
				return new CGLSubPolicyLocCovMultiForm(uiMode, uiContainerType);
			}
			
			break;

		case GARSubPolicy:
			if (SpecialtyAutoConstants.GARAGE_LOC_COVERAGES.contains(coverageCode)) {
				return new GarageSubPolicyLocCovMultiForm(uiMode, uiContainerType);
			}
			
			break;

		case CGOSubPolicy:
			if (SpecialtyAutoConstants.CARGO_LOC_COVERAGES.contains(coverageCode) ||
				SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS.contains(coverageCode)) {
				return new CargoSubPolicyLocCovMultiForm(uiMode, uiContainerType);
			}
			
			break;
			
		case CPSubPolicy:
			if (SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS.contains(coverageCode)) {
				return new CommercialPropertySubPolicyLocCovMultiForm(uiMode, uiContainerType);
			}
			
			break;

		case Vehicle:
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageType)) {
				return new VehicleCoverageForm(uiMode, uiContainerType);
			} else if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageType)) {
				switch (coverageCode) {
				case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF20:
					return new OPCF20EndorsementForm(uiMode, uiContainerType);
				default:
					break;
				}
			}

		default:
			break;
		}
		
		return new BaseCovAndEndorseForm(uiMode, uiContainerType, objectType);
	}
	
	public BaseCoverageUIHelper createCoverageHelper(UIConstants.OBJECTTYPES objectType) {
		switch (objectType) {
		case Policy:
			return new PolicyCoverageUIHelper();

		case AUTOSubPolicy:
			return new AutoSubPolicyCoverageUIHelper();

		case CGLSubPolicy:
			return new CGLSubPolicyCoverageUIHelper();

		case GARSubPolicy:
			return new GarageSubPolicyCoverageUIHelper();

		case CGOSubPolicy:
			if (ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(getCurrentProductCd()))
			{
				return new LHTCargoSubPolicyCoverageUIHelper();
			}
			return new CargoSubPolicyCoverageUIHelper();
			
		case CPSubPolicy:
			return new CommercialPropertySubPolicyCoverageUIHelper();

		case Vehicle:
			return new VehicleCoverageUIHelper();

		default:
			break;
		}
		
		return new BaseCoverageUIHelper(objectType, null);
	}
	
	public BaseCovReinsuranceUIHelper createCovReinsuranceUIHelper(UIConstants.OBJECTTYPES objectType) {
		return new PolicyCovReinsuranceUIHelper();
	}
	
	public BaseCoverageProcessor getCoverageProcessor(UIConstants.OBJECTTYPES objectType,
									String processAction, 
									PolicyTransaction 			 policyTransaction,
									BaseCoverageConfigUIHelper   coverageConfigUIHelper,
									IGenericProductConfig		 policyProductConfig,
									IGenericPolicyProcessing 	 policyProcessing,
									UserProfile 				 userProfile) {
		
		BaseCoverageProcessor coverageProcessor = null;
		
		switch (objectType) {
		case AUTOSubPolicy:
		case CGOSubPolicy:
		case GARSubPolicy:
		case CPSubPolicy:
			coverageProcessor = new TowingSubPolicyCoverageProcessor(processAction, policyTransaction, coverageConfigUIHelper, policyProductConfig, objectType);
			break;
			
		case CGLSubPolicy:
			coverageProcessor = new CGLSubPolicyCoverageProcessor(processAction, policyTransaction, coverageConfigUIHelper, policyProductConfig, objectType);
			break;
			
		case Vehicle:
			coverageProcessor = new VehicleCoverageProcessor(processAction, policyTransaction, coverageConfigUIHelper, policyProductConfig, objectType);
			break;
			
		default:
			coverageProcessor = new BaseCoverageProcessor(processAction, policyTransaction, coverageConfigUIHelper, policyProductConfig, objectType);
			break;
		}
		
		if (coverageProcessor != null) {
			coverageProcessor.setProcessorValues(userProfile, policyProductConfig, policyProcessing);
		}
		
		return coverageProcessor; 
	}
	
	public BaseCoverageConfigUIHelper createCoverageConfigHelper(UIConstants.OBJECTTYPES objectType) {
		switch (objectType) {
		case CGLSubPolicy:
		case GARSubPolicy:
		case CGOSubPolicy:
		case CPSubPolicy:
			return new BaseTowingSubPolicyCoverageConfigUIHelper();
		
		default:
			break;
		}
			
		return new BaseCoverageConfigUIHelper();
	}
	
	public IBaseCovAdditionalForm createCoverageAddlFieldsForm(UIConstants.OBJECTTYPES objectType, String coverageCode, String uiMode, UICONTAINERTYPES uiContainerType) {
		if (UICONTAINERTYPES.Grid.equals(uiContainerType)) {
			switch (objectType) {
			case CGLSubPolicy:
				if (SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS.contains(coverageCode)) {
					return new CGLSubPolicyLocCovMultiForm(uiMode, uiContainerType);
				}
				
				break;
	
			case GARSubPolicy:
				if (SpecialtyAutoConstants.GARAGE_LOC_COVERAGES.contains(coverageCode)) {
					return new GarageSubPolicyLocCovMultiForm(uiMode, uiContainerType);
				}
				
				break;
	
			case CGOSubPolicy:
				if (SpecialtyAutoConstants.CARGO_LOC_COVERAGES.contains(coverageCode) ||
					SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS.contains(coverageCode)) {
					return new CargoSubPolicyLocCovMultiForm(uiMode, uiContainerType);
				}
				
				break;
				
			case CPSubPolicy:
				if (SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS.contains(coverageCode)) {
					return new CommercialPropertySubPolicyLocCovMultiForm(uiMode, uiContainerType);
				}
				
				break;
				
			default:
				break;
			}
		}
		
		return new BaseCovAddlFieldsForm(uiMode, uiContainerType, objectType);
	}
	
	public List<String> getLocationCodes(UIConstants.OBJECTTYPES objectType) {
		List<String> result = new ArrayList<String>();
		
		switch (objectType) {
		case CGLSubPolicy:
			result.addAll(SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS);
			break;
			
		case GARSubPolicy:
			result.addAll(SpecialtyAutoConstants.GARAGE_LOC_COVERAGES);
			break;
			
		case CGOSubPolicy:
			result.addAll(SpecialtyAutoConstants.CARGO_LOC_COVERAGES);
			result.addAll(SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS);
			break;
			
		case CPSubPolicy:
			result.addAll(SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS);
			break;

		default:
			break;
		}
		
		return result;
	}
	
	public boolean isUWAdjustmentFieldEditable(UIConstants.OBJECTTYPES objectType, String coverageType, String coverageCode) {
		boolean isEditable = true;
		
		if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageType)) {
			isEditable = false;
		}
		else {
			switch (objectType) {
			case CGLSubPolicy:
				if (!SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD.equalsIgnoreCase(coverageCode)) {
					isEditable = false;
				}
				
				break;
	
			case CGOSubPolicy:
				if (SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC.equalsIgnoreCase(coverageCode) ||
					SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC.equalsIgnoreCase(coverageCode)) {
					isEditable = false;
				}
				
				break;
	
			default:
				break;
			}
		}
		
		return isEditable;
	}

	public SubPolicyLocCovForm createSubPolicyLocCovForm(UIConstants.OBJECTTYPES parentObjectType, String uiMode, UICONTAINERTYPES uiContainerType) {
		switch (parentObjectType) {
		case GARSubPolicy:
			return new GarageSubPolicyLocCovForm(uiMode, uiContainerType, parentObjectType);
			
		default:
			break;
		}
		
		return new SubPolicyLocCovForm(uiMode, uiContainerType, parentObjectType);
	}
	
	public BaseCoverageReinsuranceList createCoverageReinsuranceList(UIConstants.OBJECTTYPES objectType, boolean isCoverage, String uiMode, UICONTAINERTYPES uiContainerType) {
		return new BaseCoverageReinsuranceList(uiMode, uiContainerType, objectType, isCoverage);
	}
	
	private String getCurrentProductCd() {
		if (BrokerUI.getHelperSession().getConfigUIHelper().getCurrentProductConfig() != null) {
			return BrokerUI.getHelperSession().getConfigUIHelper().getCurrentProductConfig().getProductCd();
		}
		
		return null;
	}
}
