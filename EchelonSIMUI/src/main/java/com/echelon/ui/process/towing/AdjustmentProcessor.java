package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.logging.Logger;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class AdjustmentProcessor extends BasePolicyProcessor implements Serializable {
	private static final long serialVersionUID = -7001142942001554350L;

	private final Logger logger = Logger.getLogger(getClass().getName());

	private SpecialtyVehicleRisk parentVehicle;
	private SpecialtyVehicleRiskAdjustment adjustment;

	public AdjustmentProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		super(processAction, policyTransaction);
		this.parentVehicle = parentVehicle;
		this.adjustment = adjustment;
	}

	public SpecialtyVehicleRisk getParentVehicle() {
		return parentVehicle;
	}

	public SpecialtyVehicleRiskAdjustment getAdjustment() {
		return adjustment;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				adjustment = create(parentVehicle);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			break;
		case UIConstants.UIACTION_SaveNew:
			try {
				saveNew(parentVehicle, adjustment);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		case UIConstants.UIACTION_Save:
			try {
				save(adjustment);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		case UIConstants.UIACTION_Delete:
			try {
				delete(adjustment);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		case UIConstants.UIACTION_Undelete:
			try {
				undelete(policyTransaction, parentVehicle, adjustment);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		default:
			logger.warning("Unknown process action: \"" + processAction + "\"");
			break;
		}
	}

	private SpecialtyVehicleRiskAdjustment create(SpecialtyVehicleRisk parentVehicle)
			throws InsurancePolicyException, SpecialtyAutoPolicyException {
		parentVehicle = (SpecialtyVehicleRisk) loadPolicyRiskData(parentVehicle);
		final SpecialtyVehicleRiskAdjustment adjustment = ((ISpecialtyAutoPolicyProcessing) policyProcessing)
				.createSpecialtyVehicleRiskAdjustment(parentVehicle, this.policyTransaction);

		return adjustment;
	}

	private SpecialtyVehicleRiskAdjustment saveNew(SpecialtyVehicleRisk parentVehicle,
			SpecialtyVehicleRiskAdjustment adjustment) throws InsurancePolicyException, SpecialtyAutoPolicyException {
		//Removed unnecessary call
		//parentVehicle = (SpecialtyVehicleRisk) loadSubPolicyRiskData(parentVehicle);

		adjustment = save(adjustment); // Performs the prorata factor and premium change calculations
		return adjustment;
	}

	private SpecialtyVehicleRiskAdjustment save(SpecialtyVehicleRiskAdjustment adjustment)
			throws SpecialtyAutoPolicyException {
		adjustment = ((ISpecialtyAutoPolicyProcessing) policyProcessing)
				.updateSpecialtyVehicleRiskAdjustment(adjustment, this.policyTransaction, getUserProfile());
		return adjustment;
	}

	private boolean delete(SpecialtyVehicleRiskAdjustment adjustment) throws SpecialtyAutoPolicyException {
		((ISpecialtyAutoPolicyProcessing) policyProcessing).deleteSpecialtyVehicleRiskAdjustment(adjustment, this.policyTransaction, getUserProfile());
		return true;
	}

	private boolean undelete(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk parentVehicle,
			SpecialtyVehicleRiskAdjustment adjustment) throws SpecialtyAutoPolicyException {
		((ISpecialtyAutoPolicyProcessing) policyProcessing).undeleteSpecialtyVehicleRiskAdjustment(policyTransaction,
				parentVehicle, adjustment);
		return true;
	}
}
