package com.echelon.ui.process.towing;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;

public class AutoSubPolicyProcessor extends BaseSubPolicyProcessor<SpecialityAutoSubPolicy<?>> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8487068555545505961L;

	public AutoSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			SpecialityAutoSubPolicy<?> subPolicy) {
		super(processAction, policyTransaction, subPolicy);
		// TODO Auto-generated constructor stub
	}

	public AutoSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			String subPolicyTypeCd) {
		super(processAction, policyTransaction, subPolicyTypeCd);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected SpecialityAutoSubPolicy<?> createSubPolicy() throws SpecialtyAutoPolicyException  {
		loadPolicyTransaction();
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createAutoSubPolicy(policyTransaction);
	}
}
