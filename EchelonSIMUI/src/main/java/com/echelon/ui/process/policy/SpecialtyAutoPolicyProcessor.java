package com.echelon.ui.process.policy;

import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyActionsProcessor;
import com.echelon.ui.constants.UIConstants;

public class SpecialtyAutoPolicyProcessor extends BasePolicyActionsProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6214959024607833064L;
	private List<String>	warnings;
	private String			policyAction;

	public SpecialtyAutoPolicyProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	public SpecialtyAutoPolicyProcessor withParam(String policyAction) {
		this.policyAction = policyAction;
		return this;
	}
	
	public List<String> getWarnings() {
		return warnings;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_IssuePolicyWarn:
			try {
				warnings = getPolicyActionWarnings(policyTransaction);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;
			
		default:
			super.run();
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected List<String> getPolicyActionWarnings(PolicyTransaction policyTransaction) throws SpecialtyAutoPolicyException {
		List<String> warns = getSpecialtyAutoPolicyProcessing().getPolicyActionWarnings(policyAction, policyTransaction);
		return warns;
	}
}
