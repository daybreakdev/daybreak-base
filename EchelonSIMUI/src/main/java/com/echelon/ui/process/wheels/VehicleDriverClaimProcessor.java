package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class VehicleDriverClaimProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -161433935382146181L;
	private SpecialtyVehicleRisk	vehicle;
	private VehicleDriverClaim 		vehicleDriverClaim;
	
	public VehicleDriverClaimProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public VehicleDriverClaimProcessor withParams(SpecialtyVehicleRisk vehicle) {
		this.vehicle = vehicle;
		return this;
	}
	
	public VehicleDriverClaimProcessor withParams(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehicleDriverClaim) {
		this.vehicle = vehicle;
		this.vehicleDriverClaim = vehicleDriverClaim;
		return this;
	}
	
	public SpecialtyVehicleRisk getVehicle() {
		return vehicle;
	}

	public VehicleDriverClaim getVehicleDriverClaim() {
		return vehicleDriverClaim;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				vehicleDriverClaim = createVehicleDriverClaim(policyTransaction, vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewVehicleDriverClaim(policyTransaction, vehicle, vehicleDriverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveVehicleDriverClaim(policyTransaction, vehicle, vehicleDriverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteVehicleDriverClaim(policyTransaction, vehicle, vehicleDriverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteVehicleDriverClaim(policyTransaction, vehicle, vehicleDriverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}
	
	private VehicleDriverClaim createVehicleDriverClaim(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle) throws SpecialtyAutoPolicyException {
		VehicleDriverClaim vehicleDriverClaim = new VehicleDriverClaim();
		return vehicleDriverClaim;
	}
	
	private VehicleDriverClaim saveNewVehicleDriverClaim(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehicleDriverClaim) throws SpecialtyAutoPolicyException {
		vehicle.addVehicleDriverClaim(vehicleDriverClaim);
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return vehicleDriverClaim;
	}
	
	private VehicleDriverClaim saveVehicleDriverClaim(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehicleDriverClaim) throws SpecialtyAutoPolicyException {
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return vehicleDriverClaim;
	}
	
	private boolean deleteVehicleDriverClaim(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehicleDriverClaim) throws SpecialtyAutoPolicyException {
		vehicle.removeVehicleDriverClaim(vehicleDriverClaim);
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
	
	private boolean undeleteVehicleDriverClaim(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehicleDriverClaim) throws SpecialtyAutoPolicyException {
		//getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
	
}