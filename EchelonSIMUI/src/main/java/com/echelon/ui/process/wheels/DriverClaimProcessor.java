package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverClaim;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class DriverClaimProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1262985743174873884L;
	private Driver					driver;
	private DriverClaim 			driverClaim;
	
	public DriverClaimProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public DriverClaimProcessor withParams(Driver driver) {
		this.driver = driver;
		return this;
	}
	
	public DriverClaimProcessor withParams(Driver driver, DriverClaim driverClaim) {
		this.driver = driver;
		this.driverClaim = driverClaim;
		return this;
	}

	public Driver getDriver() {
		return driver;
	}

	public DriverClaim getDriverClaim() {
		return driverClaim;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				driverClaim = createDriverClaim(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewDriverClaim(policyTransaction, driver, driverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveDriverClaim(policyTransaction, driver, driverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteDriverClaim(policyTransaction, driver, driverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteDriverClaim(policyTransaction, driver, driverClaim);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}
	
	private DriverClaim createDriverClaim(PolicyTransaction<?> policyTransaction, Driver driver) throws SpecialtyAutoPolicyException {
		DriverClaim driverClaim = new DriverClaim();
		return driverClaim;
	}
	
	private DriverClaim saveNewDriverClaim(PolicyTransaction<?> policyTransaction, Driver driver, DriverClaim driverClaim) throws SpecialtyAutoPolicyException {
		driver.addDriverClaim(driverClaim);
		updatePolicyTransaction(policyTransaction);
		return driverClaim;
	}
	
	private DriverClaim saveDriverClaim(PolicyTransaction<?> policyTransaction, Driver driver, DriverClaim driverClaim) throws SpecialtyAutoPolicyException {
		updatePolicyTransaction(policyTransaction);
		return driverClaim;
	}
	
	private boolean deleteDriverClaim(PolicyTransaction<?> policyTransaction, Driver driver, DriverClaim driverClaim) throws SpecialtyAutoPolicyException {
		driver.removeDriverClaim(driverClaim);
		updatePolicyTransaction(policyTransaction);
		return true;
	}
	
	private boolean undeleteDriverClaim(PolicyTransaction<?> policyTransaction, Driver driver, DriverClaim driverClaim) throws SpecialtyAutoPolicyException {
		//getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
	
}