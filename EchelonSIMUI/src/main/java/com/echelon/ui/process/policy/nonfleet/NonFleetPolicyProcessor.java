package com.echelon.ui.process.policy.nonfleet;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.process.policy.SpecialtyAutoPolicyProcessor;

public class NonFleetPolicyProcessor extends SpecialtyAutoPolicyProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5131518409234165181L;

	public NonFleetPolicyProcessor(String processAction, PolicyTransaction policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean isRequirePreRunActionValidations(String action) {
		if (ConfigConstants.LIFECYCLEACTION_Rating.equalsIgnoreCase(action)) {
			return true;
		}
		
		return super.isRequirePreRunActionValidations(action);
	}

}
