package com.echelon.ui.process.towing;

import java.time.LocalDateTime;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyDetailProcessor;
import com.echelon.ui.constants.UIConstants;

public class TowingPolicyDetailProcessor extends BasePolicyDetailProcessor {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6622770812830041020L;
	private Integer	liabilityLimit; 

	public TowingPolicyDetailProcessor(String processAction, PolicyTransaction policyTransaction,
			LocalDateTime termEffectivate, String quotePreparedBy, Integer liabilityLimit, String extQuoteNumber) {
		super(processAction, policyTransaction, termEffectivate, quotePreparedBy, extQuoteNumber);
		this.liabilityLimit = liabilityLimit;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Update:
			if (!updateTowingPolicyTransaction(termEffectivate, quotePreparedBy, liabilityLimit, extQuoteNumber)) {
				return;
			}
			loadPolicyTransaction();
			break;
			
		default:
			super.run();

			break;
		}
	}

	protected boolean updateTowingPolicyTransaction(LocalDateTime termEffectivate, String quotePreparedBy, Integer liabilityLimit, String extQuoteNumber) {
		try {
			((ISpecialtyAutoPolicyProcessing)policyProcessing).
					updateSpecialtyAutoPackageData(liabilityLimit, quotePreparedBy, termEffectivate, extQuoteNumber,
													this.policyTransaction.getPolicyTransactionPK(), 
													this.getUserProfile());
		} catch (SpecialtyAutoPolicyException e) {
			setProcessException(e);
			return false;
		}
		
		ratePolicyTransaction(); 
		
		return true;
	}
}
