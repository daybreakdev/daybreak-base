package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class CargoSubPolicyValueProcessor extends BasePolicyProcessor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8487068555545505961L;
	private CargoSubPolicy<?> 	subPolicy;
	private CargoDetails 		cargoDetails;
	private List<CargoDetails>  cargoDetailsList;

	public CargoSubPolicyValueProcessor(String processAction, PolicyTransaction policyTransaction, 
			CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		super(processAction, policyTransaction);
		this.subPolicy = subPolicy;
		this.cargoDetails = cargoDetails;
	}

	public CargoSubPolicyValueProcessor(String processAction, PolicyTransaction policyTransaction,
			CargoSubPolicy<?> subPolicy, List<CargoDetails> cargoDetailsList) {
		super(processAction, policyTransaction);
		this.subPolicy = subPolicy;
		this.cargoDetailsList = cargoDetailsList;
	}

	public CargoSubPolicy<?> getSubPolicy() {
		return subPolicy;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewSubPolicy(subPolicy, cargoDetailsList);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				updateSubPolicy(subPolicy, cargoDetails);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;
		
		case UIConstants.UIACTION_Delete:
			try {
				deleteSubPolicy(subPolicy, cargoDetails);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;
		
		case UIConstants.UIACTION_Undelete:
			try {
				undeleteSubPolicy(subPolicy, cargoDetails);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}

	private void saveNewSubPolicy(CargoSubPolicy<?> subPolicy, List<CargoDetails> cargoDetailsList) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		if (cargoDetailsList != null && !cargoDetailsList.isEmpty()) {
			CargoSubPolicy<?> updSubPolicy = (CargoSubPolicy<?>) loadSubPolicy(subPolicy.getSubPolicyTypeCd());
			((ISpecialtyAutoPolicyProcessing)policyProcessing).createCargoDetails(updSubPolicy, cargoDetailsList, getUserProfile());
		}
	}

	private void updateSubPolicy(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) throws SpecialtyAutoPolicyException {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).updateCargoDetails(subPolicy, cargoDetails, getUserProfile());
	}

	private void deleteSubPolicy(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		subPolicy = (CargoSubPolicy<?>) loadSubPolicy(subPolicy.getSubPolicyTypeCd());
		((ISpecialtyAutoPolicyProcessing)policyProcessing).deleteCargoDetails(subPolicy, cargoDetails, getUserProfile());
	}

	private void undeleteSubPolicy(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		subPolicy = (CargoSubPolicy<?>) loadSubPolicy(subPolicy.getSubPolicyTypeCd());
		((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteCargoDetails(subPolicy, cargoDetails, getUserProfile());
	}
}
