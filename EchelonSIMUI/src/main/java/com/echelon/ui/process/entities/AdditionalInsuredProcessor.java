package com.echelon.ui.process.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class AdditionalInsuredProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8687729341754316758L;
	private SubPolicy 				parentSubPolicy;
	private AdditionalInsured 		additionalInsured;
	private List<AdditionalInsured>	additionalInsureds;

	public AdditionalInsuredProcessor(String processAction, PolicyTransaction policyTransaction, 
			SubPolicy parentSubPolicy, AdditionalInsured additionalInsured) {
		super(processAction, policyTransaction);
		this.parentSubPolicy = parentSubPolicy;
		this.additionalInsured = additionalInsured;
	}

	public AdditionalInsuredProcessor(String processAction, PolicyTransaction policyTransaction, 
			SubPolicy parentSubPolicy, List<AdditionalInsured> additionalInsureds) {
		super(processAction, policyTransaction);
		this.parentSubPolicy = parentSubPolicy;
		this.additionalInsureds = additionalInsureds;
	}

	public SubPolicy getParentSubPolicy() {
		return parentSubPolicy;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				saveNew(parentSubPolicy, additionalInsureds);
			} catch (InsurancePolicyException e2) {
				setProcessException(e2);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			updatePolicyTransaction();
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				delete(parentSubPolicy, additionalInsured);
			} catch (InsurancePolicyException e1) {
				setProcessException(e1);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undelete(parentSubPolicy, additionalInsured);
			} catch (SpecialtyAutoPolicyException e) {
				setProcessException(e);
				return;
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	private void saveNew(SubPolicy<?> parentSubPolicy, List<AdditionalInsured> additionalInsureds) throws InsurancePolicyException {
		if (additionalInsureds != null && !additionalInsureds.isEmpty()) {
			final SubPolicy<?> parentSP = loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
			additionalInsureds.stream()
			.forEach(o -> {
				o.setPolicyVersion(parentSP.getPolicyTransaction().getPolicyVersion());
				parentSP.addAdditionalIsnured(o);
			});
			
			updatePolicyTransaction();
		}
	}

	private void delete(SubPolicy<?> parentSubPolicy, AdditionalInsured additionalInsured) throws InsurancePolicyException {
		parentSubPolicy = loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
		AdditionalInsured addInsured = parentSubPolicy.getAdditionalInsureds().stream()
										.filter(o -> o.getAdditionalInsuredPK().longValue() == additionalInsured.getAdditionalInsuredPK().longValue())
										.findFirst().orElse(null);
		if (addInsured != null) {
			if (SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(parentSubPolicy.getSubPolicyTypeCd())) {
				SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) parentSubPolicy;
				if (autoSubPolicy.getAdditionalInsuredVehicles() != null) {
					List<AdditionalInsuredVehicles> list = new ArrayList<AdditionalInsuredVehicles>(); 
					list.addAll(autoSubPolicy.getAdditionalInsuredVehicles().stream()
										.filter(o -> (o.getAdditionalInsured() != null && addInsured.equals(o.getAdditionalInsured())))
										.collect(Collectors.toList()));
					
					if (!list.isEmpty()) {
						list.stream().forEach(o -> autoSubPolicy.removeAdditionalInsuredVehicles(o));
					}
				}
			}
			
			parentSubPolicy.removeAdditionalInsured(addInsured);
			updatePolicyTransaction();
		}
	}

	private void undelete(SubPolicy<?> parentSubPolicy, AdditionalInsured additionalInsured) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		parentSubPolicy = loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
		AdditionalInsured addInsured = parentSubPolicy.getAdditionalInsureds().stream()
										.filter(o -> o.getAdditionalInsuredPK().longValue() == additionalInsured.getAdditionalInsuredPK().longValue())
										.findFirst().orElse(null);
		if (addInsured != null) {
			parentSubPolicy.undeleteAdditionalInsured(addInsured);
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) parentSubPolicy;
			if (autoSubPolicy.getAdditionalInsuredVehicles() != null && !autoSubPolicy.getAdditionalInsuredVehicles().isEmpty()) {
				ISpecialtyAutoPolicyProcessing spPolicyProcessing = (ISpecialtyAutoPolicyProcessing) policyProcessing;
				for(AdditionalInsuredVehicles aiVehicles : autoSubPolicy.getAdditionalInsuredVehicles()) {
					spPolicyProcessing.undeleteAddInsVehicle((PolicyTransaction<SpecialtyVehicleRisk>)policyTransaction, autoSubPolicy, aiVehicles);
				}
			}
			updatePolicyTransaction();
		}
	}
}
