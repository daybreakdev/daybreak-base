package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverConviction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class DriverConvictionProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1262985743174873884L;
	private Driver					driver;
	private DriverConviction 		driverConviction;
	
	public DriverConvictionProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public DriverConvictionProcessor withParams(Driver driver) {
		this.driver = driver;
		return this;
	}
	
	public DriverConvictionProcessor withParams(Driver driver, DriverConviction driverConviction) {
		this.driver = driver;
		this.driverConviction = driverConviction;
		return this;
	}

	public Driver getDriver() {
		return driver;
	}

	public DriverConviction getDriverConviction() {
		return driverConviction;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				driverConviction = createDriverConviction(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewDriverConviction(policyTransaction, driver, driverConviction);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveDriverConviction(policyTransaction, driver, driverConviction);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteDriverConviction(policyTransaction, driver, driverConviction);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteDriverConviction(policyTransaction, driver, driverConviction);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}
	
	private DriverConviction createDriverConviction(PolicyTransaction<?> policyTransaction, Driver driver) throws SpecialtyAutoPolicyException {
		DriverConviction driverConviction = new DriverConviction();
		return driverConviction;
	}
	
	private DriverConviction saveNewDriverConviction(PolicyTransaction<?> policyTransaction, Driver driver, DriverConviction driverConviction) throws SpecialtyAutoPolicyException {
		driver.addDriverConviction(driverConviction);
		updatePolicyTransaction(policyTransaction);
		return driverConviction;
	}
	
	private DriverConviction saveDriverConviction(PolicyTransaction<?> policyTransaction, Driver driver, DriverConviction driverConviction) throws SpecialtyAutoPolicyException {
		updatePolicyTransaction(policyTransaction);
		return driverConviction;
	}
	
	private boolean deleteDriverConviction(PolicyTransaction<?> policyTransaction, Driver driver, DriverConviction driverConviction) throws SpecialtyAutoPolicyException {
		driver.removeDriverConviction(driverConviction);
		updatePolicyTransaction(policyTransaction);
		return true;
	}
	
	private boolean undeleteDriverConviction(PolicyTransaction<?> policyTransaction, Driver driver, DriverConviction driverConviction) throws SpecialtyAutoPolicyException {
		//getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
	
}
