package com.echelon.ui.process.towing;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;

public class GarageSubPolicyProcessor extends BaseSubPolicyProcessor<GarageSubPolicy<?>> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8487068555545505961L;

	public GarageSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			GarageSubPolicy<?> subPolicy) {
		super(processAction, policyTransaction, subPolicy);
		// TODO Auto-generated constructor stub
	}

	public GarageSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			String subPolicyTypeCd) {
		super(processAction, policyTransaction, subPolicyTypeCd);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected GarageSubPolicy<?> createSubPolicy() throws SpecialtyAutoPolicyException  {
		loadPolicyTransaction();
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createGarageSubPolicy(policyTransaction);
	}
}
