package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class LocationProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5005710536369622991L;
	private Integer 			 count;
	private List<PolicyLocation> policyLocations;
	private PolicyLocation 		 policyLocation;
	
	public LocationProcessor(String processAction, PolicyTransaction policyTransaction, 
			PolicyLocation policyLocation) {
		super(processAction, policyTransaction);
		this.policyLocation = policyLocation;
	}

	public LocationProcessor(String processAction, PolicyTransaction policyTransaction, 
			Integer count) {
		super(processAction, policyTransaction);
		this.count = count;
	}

	public List<PolicyLocation> getPolicyLocations() {
		return policyLocations;
	}

	public PolicyLocation getPolicyLocation() {
		return policyLocation;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				policyLocations = createPolicyLocations(count);
			} catch (Exception e1) {
				setProcessException(e1);
				return;
			}
			break;
			
		case UIConstants.UIACTION_SaveNew:
			try {
				updatePolicyTransaction();
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		case UIConstants.UIACTION_Save:
			try {
				policyLocation = updatePolicyLocation(policyLocation);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
		
		case UIConstants.UIACTION_Delete:
			try {
				deletePolicyLocation(policyLocation);
				policyLocation = null;
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
		
		case UIConstants.UIACTION_Undelete:
			try {
				undeletePolicyLocation(policyLocation);
				policyLocation = null;
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		case UIConstants.UIACTION_Copy:
			try {
				policyLocation = copyPolicyLocation(policyLocation, policyTransaction);
			} catch (Exception e1) {
				setProcessException(e1);
				return;
			}
			break;
			
		default:
			break;
		}
	}
	
	private List<PolicyLocation> createPolicyLocations(Integer count) throws Exception {
		loadPolicyTransaction();
		List<PolicyLocation> policyLocations = new ArrayList<PolicyLocation>();
		
		if (count != null && count > 0) {
			policyLocations = new ArrayList<PolicyLocation>();
			
			for(int ii=0; ii<count; ii++) {
				PolicyLocation loc = createPolicyLocation(policyTransaction);
				if (loc != null) {
					policyLocations.add(loc);
				}
			}
		}
		
		return policyLocations;
	}

	private PolicyLocation createPolicyLocation(PolicyTransaction<PolicyLocation> policyTransaction) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createPolicyLocation(policyTransaction);
	}
	
	private PolicyLocation updatePolicyLocation(PolicyLocation policyLocation) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).updatePolicyLocation(policyLocation, getUserProfile());
	}

	private void deletePolicyLocation(PolicyLocation policyLocation) throws Exception {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).deletePolicyLocation(policyLocation, getUserProfile());
	}

	private void undeletePolicyLocation(PolicyLocation policyLocation) throws Exception {
		policyProcessing.undeletePolicyRisk(policyTransaction, policyLocation);
		updatePolicyLocation(policyLocation);
	}

	private PolicyLocation copyPolicyLocation(PolicyLocation policyLocation, PolicyTransaction<PolicyLocation> policyTransaction) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).copyPolicyLocation(policyLocation, policyTransaction);
	}
	
	protected boolean endRunProcesses() {
		ratePolicyTransaction(null, policyLocation);
		return true;
	}
}
