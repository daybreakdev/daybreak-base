package com.echelon.ui.process.towing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.baseclasses.process.policy.BaseCoverageProcessor;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

@SuppressWarnings("serial")
public abstract class TowingCoverageProcessor extends BaseCoverageProcessor {

	public TowingCoverageProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			BaseCoverageConfigUIHelper coverageConfigUIHelper, IGenericProductConfig productConfig, OBJECTTYPES parentObjectType) {
		super(processAction, policyTransaction, coverageConfigUIHelper, productConfig, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected List<String> getRatingWarning(IBusinessEntity covParent, CoverageDto coverageDto) throws Exception {
		// TODO Auto-generated method stub
		ISpecialtyAutoPolicyProcessing specialtyAutoPolicyProcessing = (ISpecialtyAutoPolicyProcessing) getPolicyProcessing();
		if (covParent instanceof SubPolicy<?>) {
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				return specialtyAutoPolicyProcessing.getSubpolicyRatingWarnings((SubPolicy<?>) covParent, Arrays.asList((Coverage)coverageDto.getCoverage()), null);
			}
			
			if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				return specialtyAutoPolicyProcessing.getSubpolicyRatingWarnings((SubPolicy<?>) covParent, null, Arrays.asList((Endorsement)coverageDto.getCoverage()));
			}
		}
		else if (covParent instanceof Risk) {
			if (ConfigConstants.COVERAGETYPE_COVERAGE.equalsIgnoreCase(coverageDto.getCoverageType())) {
				return specialtyAutoPolicyProcessing.getRiskRatingWarnings((Risk)covParent, Arrays.asList((Coverage)coverageDto.getCoverage()), null);
			}
			
			if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(coverageDto.getCoverageType())) {
				return specialtyAutoPolicyProcessing.getRiskRatingWarnings((Risk)covParent, null, Arrays.asList((Endorsement)coverageDto.getCoverage()));
			}
		}
		
		return null;
	}

	@Override
	protected List<String> getRatingWarning(IBusinessEntity covParent, List<CoverageDto> coverageDtos)
			throws Exception {
		if (coverageDtos != null && !coverageDtos.isEmpty()) {
			ISpecialtyAutoPolicyProcessing specialtyAutoPolicyProcessing = (ISpecialtyAutoPolicyProcessing) getPolicyProcessing();
			if (covParent instanceof SubPolicy<?>) {
				SubPolicy<?> subPolicy = (SubPolicy<?>) covParent;
				List<Coverage> 		coverages 	 = new ArrayList<Coverage>();
				List<Endorsement>	endorsements = new ArrayList<Endorsement>();
				
				if (subPolicy.getCoverageDetail().getCoverages() != null) {
					coverages.addAll(subPolicy.getCoverageDetail().getCoverages());
				}
				if (subPolicy.getCoverageDetail().getEndorsements() != null) {
					endorsements.addAll(subPolicy.getCoverageDetail().getEndorsements());
				}
				
				return specialtyAutoPolicyProcessing.getSubpolicyRatingWarnings(subPolicy, coverages, endorsements);
			}
			else if (covParent instanceof Risk) {
				Risk risk = (Risk) covParent;
				List<Coverage> 		coverages 	 = new ArrayList<Coverage>();
				List<Endorsement>	endorsements = new ArrayList<Endorsement>();
				
				if (risk.getRiskCoverages() != null) {
					coverages.addAll(risk.getRiskCoverages());
				}
				if (risk.getRiskEndorsements() != null) {
					endorsements.addAll(risk.getRiskEndorsements());
				}
				
				return specialtyAutoPolicyProcessing.getRiskRatingWarnings(risk, coverages, endorsements);
			}
		}
		
		return null;
	}

}
