package com.echelon.ui.process.entities;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;

public class CustomerProcessor extends BasePolicyProcessor implements Serializable {
	private static final long serialVersionUID = -678511999901418325L;

	private final Logger log = Logger.getLogger(this.getClass().getName());

	public CustomerProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Save:
			try {
				saveInsuredAddress(policyTransaction);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		default:
			log.log(Level.WARNING, "Unknown process action {0}", processAction);
			break;
		}
	}

	private void saveInsuredAddress(PolicyTransaction<?> policyTransaction) throws Exception {
		if (policyTransaction.getPolicyVersion().getInsurancePolicy() instanceof SpecialtyAutoPackage) {
			((ISpecialtyAutoPolicyProcessing) getPolicyProcessing()).updateCustomerMainAddress(policyTransaction,
					getUserProfile());
		} else {
			updatePolicyTransaction(policyTransaction);
		}
	}
}
