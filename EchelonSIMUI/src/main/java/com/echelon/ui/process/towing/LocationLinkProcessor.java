package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.exceptions.InsurancePolicyException;

public class LocationLinkProcessor extends BasePolicyProcessor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5764074125259605853L;
	private Object 					parentObject;
	private List<PolicyLocation> 	selectedLocations;
	private CGLLocation				cglLocation;
	
	public LocationLinkProcessor(String processAction, PolicyTransaction policyTransaction, Object parentObject,
			List<PolicyLocation> selectedLocations) {
		super(processAction, policyTransaction);
		this.parentObject = parentObject;
		this.selectedLocations = selectedLocations;
	}
	
	public LocationLinkProcessor(String processAction, PolicyTransaction policyTransaction, Object parentObject,
			CGLLocation cglLocation) {
		super(processAction, policyTransaction);
		this.parentObject = parentObject;
		this.cglLocation = cglLocation;
	}

	public Object getParentObject() {
		return parentObject;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewLocations(parentObject, selectedLocations);
			} catch (InsurancePolicyException e1) {
				setProcessException(e1);
				return;
			}
			loadPolicyTransaction();
			break;
			
		case UIConstants.UIACTION_Delete:
			try {
				deleteLocation(parentObject, cglLocation);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}
	
	protected void saveNewLocations(Object parentObject, List<PolicyLocation> selectedLocations) throws InsurancePolicyException {
		if (parentObject instanceof CGLSubPolicy<?>) {
			CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) loadSubPolicy(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
			for (PolicyLocation pl : selectedLocations) {
				CGLLocation cglLocation = new CGLLocation();
				cglLocation.setPolicyLocation(pl);
				cglSubPolicy.addCglLocation(cglLocation);
			}
			policyProcessing.updateSubPolicy(cglSubPolicy, getUserProfile());
		}
	}
	
	protected void deleteLocation(Object parentObject, CGLLocation cglLocation) throws InsurancePolicyException {
		if (parentObject instanceof CGLSubPolicy<?>) {
			CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) loadSubPolicy(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
			cglLocation = cglSubPolicy.findLocationByPK(cglLocation.getCglLocationPK());
			if (cglLocation != null) {
				cglSubPolicy.removeCglLocation(cglLocation);
				policyProcessing.updateSubPolicy(cglSubPolicy, getUserProfile());
			}
		}
	}
}
