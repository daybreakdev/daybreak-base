package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class VehicleDriverProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8424482855605836939L;
	private SpecialtyVehicleRisk	vehicle;
	private VehicleDriver 			vehicleDriver;
	
	public VehicleDriverProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public VehicleDriverProcessor withParams(SpecialtyVehicleRisk vehicle) {
		this.vehicle = vehicle;
		return this;
	}
	
	public VehicleDriverProcessor withParams(SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) {
		this.vehicle = vehicle;
		this.vehicleDriver = vehicleDriver;
		return this;
	}
	
	public SpecialtyVehicleRisk getVehicle() {
		return vehicle;
	}

	public VehicleDriver getVehicleDriver() {
		return vehicleDriver;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				vehicleDriver = createVehicleDriver(policyTransaction, vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewVehicleDriver(policyTransaction, vehicle, vehicleDriver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveVehicleDriver(policyTransaction, vehicle, vehicleDriver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteVehicleDriver(policyTransaction, vehicle, vehicleDriver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteVehicleDriver(policyTransaction, vehicle, vehicleDriver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}
	
	private VehicleDriver createVehicleDriver(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle) throws SpecialtyAutoPolicyException {
		VehicleDriver vehicleDriver = new VehicleDriver();
		return vehicleDriver;
	}
	
	private VehicleDriver saveNewVehicleDriver(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) throws SpecialtyAutoPolicyException {
		vehicle.addVehicleDriver(vehicleDriver);
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return vehicleDriver;
	}
	
	private VehicleDriver saveVehicleDriver(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) throws SpecialtyAutoPolicyException {
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return vehicleDriver;
	}
	
	private boolean deleteVehicleDriver(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) throws SpecialtyAutoPolicyException {
		vehicle.removeVehicleDriver(vehicleDriver);
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
	
	private boolean undeleteVehicleDriver(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) throws SpecialtyAutoPolicyException {
		vehicle.undeleteVehicleDriver(vehicleDriver);
		getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
}
