package com.echelon.ui.process.trucking;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.echelon.domain.policy.specialtylines.CommercialPropertyLocation;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class CommercialPropertyLocationLinkProcessor extends BasePolicyProcessor
	implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4625633453675274506L;

	private Object parentObject;
	private List<PolicyLocation> selectedLocations;
	private CommercialPropertyLocation cpLocation;

	public CommercialPropertyLocationLinkProcessor(String processAction,
		PolicyTransaction policyTransaction, Object parentObject,
		List<PolicyLocation> selectedLocations) {
		super(processAction, policyTransaction);
		this.parentObject = parentObject;
		this.selectedLocations = selectedLocations;
	}

	public CommercialPropertyLocationLinkProcessor(String processAction,
		PolicyTransaction policyTransaction, Object parentObject,
		CommercialPropertyLocation cpLocation) {
		super(processAction, policyTransaction);
		this.parentObject = parentObject;
		this.cpLocation = cpLocation;
	}

	public Object getParentObject() {
		return parentObject;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewLocations(parentObject, selectedLocations);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		case UIConstants.UIACTION_Delete:
			try {
				deleteLocation(parentObject, cpLocation);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}

			loadPolicyTransaction();
			break;
		default:
			break;
		}
	}

	protected void saveNewLocations(Object parentObject,
		List<PolicyLocation> selectedLocations)
		throws InsurancePolicyException {
		if (parentObject instanceof CommercialPropertySubPolicy<?>) {
			CommercialPropertySubPolicy<?> subPolicy = (CommercialPropertySubPolicy<?>) loadSubPolicy(
				SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
			for (PolicyLocation pl : selectedLocations) {
				CommercialPropertyLocation cpLocation = new CommercialPropertyLocation();
				cpLocation.setPolicyLocation(pl);
				subPolicy.addCommercialPropertyLocation(cpLocation);
			}

			policyProcessing.updateSubPolicy(subPolicy, getUserProfile());
		}
	}

	protected void deleteLocation(Object parentObject,
		CommercialPropertyLocation cpLocation) throws InsurancePolicyException {
		if (parentObject instanceof CommercialPropertySubPolicy<?>) {
			CommercialPropertySubPolicy<?> subPolicy = (CommercialPropertySubPolicy<?>) loadSubPolicy(
				SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
			cpLocation = subPolicy
				.findLocationByPK(cpLocation.getCommercialPropertyLocationPK());
			if (cpLocation != null) {
				subPolicy.removeCommercialPropertyLocation(cpLocation);
				policyProcessing.updateSubPolicy(subPolicy, getUserProfile());
			}
		}
	}
}
