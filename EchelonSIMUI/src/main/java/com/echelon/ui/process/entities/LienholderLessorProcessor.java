package com.echelon.ui.process.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.dto.LienholderLessorSearchResults;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class LienholderLessorProcessor extends BasePolicyProcessor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -285796046580588775L;
	private Object 				lienLessorParent;
	private Object 	 			lienLessorHolder;
	private Long				lienLessorPK;
	private LienholderLessor	lienLessor;
	
	private String companyName; 
	private String city;
	private String postalZip;
	private List<LienholderLessorSearchResults> searchResults;

	public LienholderLessorProcessor(String processAction, PolicyTransaction policyTransaction,
			Object lienLessorParent, Object lienLessorHolder) {
		super(processAction, policyTransaction);
		this.lienLessorParent = lienLessorParent;
		this.lienLessorHolder = lienLessorHolder;
	}

	public LienholderLessorProcessor(String processAction, PolicyTransaction policyTransaction,
			Object lienLessorParent, Long lienLessorPK) {
		super(processAction, policyTransaction);
		this.lienLessorParent = lienLessorParent;
		this.lienLessorPK     = lienLessorPK;
	}

	public LienholderLessorProcessor(String processAction, PolicyTransaction policyTransaction, 
			String companyName, String city, String postalZip) {
		super(processAction, policyTransaction);
		this.companyName = companyName;
		this.city = city;
		this.postalZip = postalZip;
	}

	public Object getLienLessorParent() {
		return lienLessorParent;
	}

	public Object getLienLessorHolder() {
		return lienLessorHolder;
	}

	public LienholderLessor getLienLessor() {
		return lienLessor;
	}

	public List<LienholderLessorSearchResults> getSearchResults() {
		return searchResults;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				if (lienLessorParent instanceof SpecialityAutoSubPolicy) {
					saveNew((SpecialityAutoSubPolicy)lienLessorParent, (LienholderLessorSubPolicy) lienLessorHolder);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				if (lienLessorParent instanceof SpecialityAutoSubPolicy) {
					saveUpdate((SpecialityAutoSubPolicy)lienLessorParent, (LienholderLessorSubPolicy) lienLessorHolder);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				if (lienLessorParent instanceof SpecialityAutoSubPolicy) {
					delete((SpecialityAutoSubPolicy)lienLessorParent, (LienholderLessorSubPolicy) lienLessorHolder);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				if (lienLessorParent instanceof SpecialityAutoSubPolicy) {
					undelete((SpecialityAutoSubPolicy)lienLessorParent, (LienholderLessorSubPolicy) lienLessorHolder);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Search:
			try {
				searchResults = searchLienLessor(companyName, city, postalZip);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			break;

		case UIConstants.UIACTION_Select:
			try {
				lienLessor = loadLienholderLessor(lienLessorPK);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			break;
			
		default:
			break;
		}
	}

	// Subpolicy
	private LienholderLessorSubPolicy saveNew(SpecialityAutoSubPolicy parentSubPolicy, LienholderLessorSubPolicy lienLessorHolder) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		// New Lienholder Lessor
		if (lienLessorHolder.getLienholderLessor().getLienholderLessorPK() == null) {
			lienLessorHolder.getLienholderLessor().setLienLessorType(lienLessorHolder.getLienLessorType());
		}
		
		lienLessorHolder.setLienholderLessor(policyProcessing.saveLienholderLessor(lienLessorHolder.getLienholderLessor(), getUserProfile()));
		
		parentSubPolicy = (SpecialityAutoSubPolicy) loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
		parentSubPolicy.addLienholderLessorSubPolicy(lienLessorHolder);
		saveAutoSubPolicy(parentSubPolicy);
		
		return lienLessorHolder;
	}

	private LienholderLessorSubPolicy saveUpdate(SpecialityAutoSubPolicy parentSubPolicy, LienholderLessorSubPolicy lienLessorHolder) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		saveUpdate(lienLessorHolder.getLienholderLessor());
		
		saveAutoSubPolicy(parentSubPolicy);
		
		return lienLessorHolder;
	}
	
	private void delete(SpecialityAutoSubPolicy<Risk> parentSubPolicy, LienholderLessorSubPolicy lienLessorHolder) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		parentSubPolicy = (SpecialityAutoSubPolicy<Risk>) loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
		
		LienholderLessorSubPolicy currLienLessorHolder = parentSubPolicy.findLienholderLessorByPk(lienLessorHolder.getLienholderLessorSubPolicyPK());
		if (currLienLessorHolder != null) {
			if (parentSubPolicy.getLienholderLessorVehicleDetails() != null) {
				List<LienholderLessorVehicleDetails> list = new ArrayList<LienholderLessorVehicleDetails>(); 
				list.addAll(parentSubPolicy.getLienholderLessorVehicleDetails().stream()
									.filter(o -> (o.getLienholderLessor() != null && currLienLessorHolder.getLienholderLessor().equals(o.getLienholderLessor())))
									.collect(Collectors.toList()));
				
				if (!list.isEmpty()) {
					final SpecialityAutoSubPolicy<Risk> parentSp = parentSubPolicy;
					list.stream().forEach(o -> parentSp.removeLienholderLessorVehicleDetails(o));
				}
			}
	
			parentSubPolicy.removeLienholderLessorSubPolicy(currLienLessorHolder);
			saveAutoSubPolicy(parentSubPolicy);
		}
	}
	
	private void undelete(SpecialityAutoSubPolicy<Risk> parentSubPolicy, LienholderLessorSubPolicy lienLessorHolder) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) loadSubPolicy(parentSubPolicy.getSubPolicyTypeCd());
		
		LienholderLessorSubPolicy currLienLessorHolder = specialAutoSubPolicy.findLienholderLessorByPk(lienLessorHolder.getLienholderLessorSubPolicyPK());
		if (specialAutoSubPolicy.getLienholderLessorVehicleDetails() != null) {
			List<LienholderLessorVehicleDetails> list = new ArrayList<LienholderLessorVehicleDetails>(); 
			list.addAll(specialAutoSubPolicy.getLienholderLessorVehicleDetails().stream()
								.filter(o -> (o.getLienholderLessor() != null && currLienLessorHolder.getLienholderLessor().equals(o.getLienholderLessor())))
								.collect(Collectors.toList()));
			
			if (!list.isEmpty()) {
				list.stream().forEach(o ->
					{
						try {
							((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteLeanholderLessoeVehDetails(specialAutoSubPolicy, o);
						} catch (SpecialtyAutoPolicyException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
			}
		}

		((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteLienholderLessorSubPolicy(specialAutoSubPolicy, currLienLessorHolder);
		saveAutoSubPolicy(specialAutoSubPolicy);
	}
	
	private SpecialityAutoSubPolicy saveAutoSubPolicy(SpecialityAutoSubPolicy subPolicy) throws InsurancePolicyException  {
		subPolicy = (SpecialityAutoSubPolicy) ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSubPolicy(subPolicy, getUserProfile());
		return subPolicy;
	}
	
	// LienLessor
	private LienholderLessor saveUpdate(LienholderLessor lienLessor) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		lienLessor = policyProcessing.saveLienholderLessor(lienLessor, getUserProfile());
		return lienLessor;
	}

	private List<LienholderLessorSearchResults> searchLienLessor(String companyName, String city, String postalZip) throws InsurancePolicyException {
		return policyProcessing.findLienholderLessors(companyName, city, postalZip);
	}
	
	private LienholderLessor loadLienholderLessor(Long lienLessorPK) throws InsurancePolicyException {
		return policyProcessing.loadLienholderLessor(lienLessorPK);
	}
}
