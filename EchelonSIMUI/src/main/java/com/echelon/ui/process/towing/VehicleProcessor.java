package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.Comparator;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.utils.Constants;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;

public class VehicleProcessor extends BasePolicyProcessor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6894239575577754521L;
	private SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy;
	private SpecialtyVehicleRisk vehicle;

	public VehicleProcessor(String processAction, PolicyTransaction policyTransaction, 
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy,
			SpecialtyVehicleRisk vehicle) {
		super(processAction, policyTransaction);
		this.autoSubPolicy = autoSubPolicy;
		this.vehicle = vehicle;
	}

	public VehicleProcessor(String processAction, PolicyTransaction policyTransaction, 
			SpecialtyVehicleRisk vehicle) {
		super(processAction, policyTransaction);
		this.vehicle = vehicle;
	}
	
	public SpecialtyVehicleRisk getVehicle() {
		return vehicle;
	}

	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> getAutoSubPolicy() {
		return autoSubPolicy;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				vehicle = createVehicleRisk(autoSubPolicy);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_Copy:
			try {
				vehicle = copyVehicle(vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				vehicle = saveNewVehicleRisk(vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			reloadVehicle();
			break;

		case UIConstants.UIACTION_Save:
			try {
				vehicle = saveVehicleRisk(vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			reloadVehicle();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteVehicle(vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			reloadVehicle();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteVehicle(vehicle);
			} catch (Exception e) {
				setProcessException(e);
			}
			reloadVehicle();
			break;
			
		case UIConstants.UIACTION_LoadData:
			try {
				vehicle = (SpecialtyVehicleRisk) loadSubPolicyRiskData(vehicle);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
			}
			
		default:
			break;
		}
	}
	
	@SuppressWarnings("unchecked")
	private SpecialtyVehicleRisk createVehicleRisk(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) loadSubPolicy(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		SpecialtyVehicleRisk vehicleRisk = ((ISpecialtyAutoPolicyProcessing)policyProcessing).createSpecialtyVehicleRisk(autoSubPolicy);
		// ENFO-239 So no need to do any action if user cancel the action
		autoSubPolicy.getSubPolicyRisks().remove(vehicleRisk);
		vehicleRisk.setUnitExposure(Constants.PROVINCE_ONTARIO);
		return vehicleRisk;
	}
	
	@SuppressWarnings("unchecked")
	private SpecialtyVehicleRisk saveNewVehicleRisk(SpecialtyVehicleRisk vehicle) throws InsurancePolicyException, SpecialtyAutoPolicyException {
		autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) loadSubPolicy(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		autoSubPolicy.addSubPolicyRisk(vehicle);
		
		autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSpecialityAutoSubPolicy(autoSubPolicy, getUserProfile());
		
		vehicle = autoSubPolicy.getSubPolicyRisks().stream().sorted(Comparator.comparing(SpecialtyVehicleRisk::getPolicyRiskPK, Comparator.nullsLast(Comparator.naturalOrder())))
															.collect(Collectors.toList())
															.get(autoSubPolicy.getSubPolicyRisks().size() - 1);
		
		return vehicle;
	}
	
	@SuppressWarnings("unchecked")
	private SpecialtyVehicleRisk saveVehicleRisk(SpecialtyVehicleRisk vehicle) throws InsurancePolicyException, SpecialtyAutoPolicyException {
		vehicle = ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return vehicle;
	}
	
	private boolean deleteVehicle(SpecialtyVehicleRisk vehicle) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).loadSubPolicy(vehicle.getSubPolicy(), false);
		((ISpecialtyAutoPolicyProcessing)policyProcessing).deleteSpecialtyVehicleRisk(vehicle);
		return true;
	}
	
	private boolean undeleteVehicle(SpecialtyVehicleRisk vehicle) throws InsurancePolicyException {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteSubPolicyRisk(policyTransaction, autoSubPolicy, vehicle);
		updatePolicyTransaction(policyTransaction);
		return true;
	}

	private SpecialtyVehicleRisk copyVehicle(SpecialtyVehicleRisk vehicle) throws SpecialtyAutoPolicyException {
		vehicle = ((ISpecialtyAutoPolicyProcessing)policyProcessing).copySpecialtyVehicleRisk(vehicle);
		return vehicle;
	}
	
	private void reloadVehicle() {
		try {
			loadPolicyTransaction();
			if (vehicle != null && vehicle.getPolicyRiskPK() != null) {
				vehicle = (SpecialtyVehicleRisk) loadSubPolicyRisk(vehicle.getPolicyRiskPK());
			}
			else {
				vehicle = null;
			}
		} catch (InsurancePolicyException e) {
			setProcessException(e);
		}
	}
}
