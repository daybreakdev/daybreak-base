package com.echelon.ui.process.towing;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class CGLSubPolicyProcessor extends BaseSubPolicyProcessor<CGLSubPolicy<?>> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3592803986352362153L;
	private CGLSubPolicy<?> preSubPolicy;

	public CGLSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			CGLSubPolicy<?> subPolicy) {
		super(processAction, policyTransaction, subPolicy);
		// TODO Auto-generated constructor stub
	}

	public CGLSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			CGLSubPolicy<?> subPolicy, CGLSubPolicy<?> preSubPolicy) {
		super(processAction, policyTransaction, subPolicy);
		
		this.preSubPolicy = preSubPolicy;
	}

	public CGLSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			String subPolicyTypeCd) {
		super(processAction, policyTransaction, subPolicyTypeCd);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected CGLSubPolicy<?> createSubPolicy() throws SpecialtyAutoPolicyException  {
		loadPolicyTransaction();
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createCGLSubPolicy(policyTransaction);
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Save:
			try {
				subPolicy = ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateCGLSubPolicy(subPolicy, preSubPolicy, getUserProfile());
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		default:
			super.run();
			break;
		}
	}
	
}
