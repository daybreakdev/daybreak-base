package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverSuspension;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class DriverSuspensionProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3807800782549914016L;
	private Driver					driver;
	private DriverSuspension 		driverSuspension;
	
	public DriverSuspensionProcessor(String processAction, PolicyTransaction<?> policyTransaction) {
		super(processAction, policyTransaction);
		// TODO Auto-generated constructor stub
	}
	
	public DriverSuspensionProcessor withParams(Driver driver) {
		this.driver = driver;
		return this;
	}
	
	public DriverSuspensionProcessor withParams(Driver driver, DriverSuspension driverSuspension) {
		this.driver = driver;
		this.driverSuspension = driverSuspension;
		return this;
	}

	public Driver getDriver() {
		return driver;
	}

	public DriverSuspension getDriverSuspension() {
		return driverSuspension;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				driverSuspension = createDriverSuspension(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewDriverSuspension(policyTransaction, driver, driverSuspension);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveDriverSuspension(policyTransaction, driver, driverSuspension);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteDriverSuspension(policyTransaction, driver, driverSuspension);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteDriverSuspension(policyTransaction, driver, driverSuspension);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyProcessing;
	}
	
	private DriverSuspension createDriverSuspension(PolicyTransaction<?> policyTransaction, Driver driver) throws SpecialtyAutoPolicyException {
		DriverSuspension driverSuspension = new DriverSuspension();
		return driverSuspension;
	}
	
	private DriverSuspension saveNewDriverSuspension(PolicyTransaction<?> policyTransaction, Driver driver, DriverSuspension driverSuspension) throws SpecialtyAutoPolicyException {
		driver.addDriverSuspension(driverSuspension);
		updatePolicyTransaction(policyTransaction);
		return driverSuspension;
	}
	
	private DriverSuspension saveDriverSuspension(PolicyTransaction<?> policyTransaction, Driver driver, DriverSuspension driverSuspension) throws SpecialtyAutoPolicyException {
		updatePolicyTransaction(policyTransaction);
		return driverSuspension;
	}
	
	private boolean deleteDriverSuspension(PolicyTransaction<?> policyTransaction, Driver driver, DriverSuspension driverSuspension) throws SpecialtyAutoPolicyException {
		driver.removeDriverSuspenstion(driverSuspension);
		updatePolicyTransaction(policyTransaction);
		return true;
	}
	
	private boolean undeleteDriverSuspension(PolicyTransaction<?> policyTransaction, Driver driver, DriverSuspension driverSuspension) throws SpecialtyAutoPolicyException {
		//getSpecialtyAutoPolicyProcessing().updateSpecialtyVehicleRisk(vehicle, getUserProfile());
		return true;
	}
}