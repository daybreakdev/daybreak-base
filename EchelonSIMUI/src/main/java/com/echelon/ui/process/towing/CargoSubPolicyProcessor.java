package com.echelon.ui.process.towing;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class CargoSubPolicyProcessor extends BaseSubPolicyProcessor<CargoSubPolicy<?>> implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8487068555545505961L;
	private CargoSubPolicy<?> preSubPolicy;

	public CargoSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			CargoSubPolicy<?> subPolicy) {
		super(processAction, policyTransaction, subPolicy);
		// TODO Auto-generated constructor stub
	}

	public CargoSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			CargoSubPolicy<?> subPolicy, CargoSubPolicy<?> preSubPolicy) {
		super(processAction, policyTransaction, subPolicy);
		// TODO Auto-generated constructor stub
		
		this.preSubPolicy = preSubPolicy;
	}

	public CargoSubPolicyProcessor(String processAction, PolicyTransaction policyTransaction,
			String subPolicyTypeCd) {
		super(processAction, policyTransaction, subPolicyTypeCd);
		// TODO Auto-generated constructor stub
	}
	
	@Override
	protected CargoSubPolicy<?> createSubPolicy() throws SpecialtyAutoPolicyException  {
		loadPolicyTransaction();
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createCargoSubPolicy(policyTransaction);
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Save:
			try {
				subPolicy = ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateCargoSubPolicy(subPolicy, preSubPolicy, getUserProfile());
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			endRunProcesses();
			break;
			
		default:
			super.run();
			break;
		}
	}
	
}
