package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class LienLessorVehicleDetailsProcessor extends BasePolicyProcessor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3207891390914252581L;
	private Object 				 				 vehicleDetailsParent;
	private Object 					 			 lienLessorHolder;
	private LienholderLessorVehicleDetails 		 vehicleDetails;
	private List<LienholderLessorVehicleDetails> newVehicleDetailsList;
	
	public LienLessorVehicleDetailsProcessor(String processAction, PolicyTransaction policyTransaction, 
			Object vehicleDetailsParent, Object lienLessorHolder,
			LienholderLessorVehicleDetails vehicleDetails) {
		super(processAction, policyTransaction);
		this.vehicleDetailsParent = vehicleDetailsParent;
		this.lienLessorHolder     = lienLessorHolder;
		this.vehicleDetails       = vehicleDetails;
	}
	
	public LienLessorVehicleDetailsProcessor(String processAction, PolicyTransaction policyTransaction, 
			Object vehicleDetailsParent, Object lienLessorHolder,
			List<LienholderLessorVehicleDetails> newVehicleDetailsList) {
		super(processAction, policyTransaction);
		this.vehicleDetailsParent  = vehicleDetailsParent;
		this.lienLessorHolder      = lienLessorHolder;
		this.newVehicleDetailsList = newVehicleDetailsList;
	}

	public Object getVehicleDetailsParent() {
		return vehicleDetailsParent;
	}

	public Object getLienLessorHolder() {
		return lienLessorHolder;
	}

	public LienholderLessorVehicleDetails getVehicleDetails() {
		return vehicleDetails;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				if (vehicleDetailsParent instanceof SpecialityAutoSubPolicy) {
					create((SpecialityAutoSubPolicy<Risk>)vehicleDetailsParent, (LienholderLessorSubPolicy)lienLessorHolder, newVehicleDetailsList);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				if (vehicleDetailsParent instanceof SpecialityAutoSubPolicy) {
					save((SpecialityAutoSubPolicy)vehicleDetailsParent);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				if (vehicleDetailsParent instanceof SpecialityAutoSubPolicy) {
					delete((SpecialityAutoSubPolicy)vehicleDetailsParent, (LienholderLessorSubPolicy)lienLessorHolder, vehicleDetails);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				if (vehicleDetailsParent instanceof SpecialityAutoSubPolicy) {
					undelete((SpecialityAutoSubPolicy)vehicleDetailsParent, (LienholderLessorSubPolicy)lienLessorHolder, vehicleDetails);
				}
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}
	
	private void create(SpecialityAutoSubPolicy<Risk> subPolicy, LienholderLessorSubPolicy lienLessorHolder, List<LienholderLessorVehicleDetails> newVehicleDetailsList) throws InsurancePolicyException, SpecialtyAutoPolicyException {
		if (newVehicleDetailsList != null && !newVehicleDetailsList.isEmpty()) {
			final SpecialityAutoSubPolicy<SpecialtyVehicleRisk> currSubPolicy = (SpecialityAutoSubPolicy) loadSubPolicy(subPolicy.getSubPolicyTypeCd());
			
			// currSubPolicy.addLienholderLessorVehicleDetails does not keep the order of inputs
			LienholderLessorSubPolicy currLienLessorHolder = currSubPolicy.getLienholderLessors().stream().filter(
													o -> o.getLienholderLessorSubPolicyPK().longValue() == lienLessorHolder.getLienholderLessorSubPolicyPK().longValue()
												).findFirst().orElse(null);
			
			newVehicleDetailsList.stream()
					.forEach(o -> {
						if (o.getLienholderLessor() == null) {
							o.setLienholderLessor(currLienLessorHolder.getLienholderLessor());	
						}
					});
			
			newVehicleDetailsList =((ISpecialtyAutoPolicyProcessing)policyProcessing).assignLienholderLessorVehicleDetailsSequence(currSubPolicy, currLienLessorHolder.getLienholderLessor(), newVehicleDetailsList);
			
			newVehicleDetailsList.stream()
					.forEach(o -> {
						if (o.getLienholderLessorVehicleDetailsPK() == null) {
							currSubPolicy.addLienholderLessorVehicleDetails(o);
						}
					});
	
			save(currSubPolicy);
		}
	}

	private SpecialityAutoSubPolicy<SpecialtyVehicleRisk> save(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPolicy) throws InsurancePolicyException {
		subPolicy = (SpecialityAutoSubPolicy) ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSubPolicy(subPolicy, getUserProfile());
		return subPolicy;
	}
	
	private void delete(SpecialityAutoSubPolicy subPolicy, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) throws InsurancePolicyException  {
		subPolicy.removeLienholderLessorVehicleDetails(vehicleDetails);
		save(subPolicy);
	}
	
	private void undelete(SpecialityAutoSubPolicy subPolicy, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) throws InsurancePolicyException, SpecialtyAutoPolicyException  {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteLeanholderLessoeVehDetails(subPolicy, vehicleDetails);
		save(subPolicy);
	}

}
