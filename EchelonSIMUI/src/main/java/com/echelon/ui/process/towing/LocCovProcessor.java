package com.echelon.ui.process.towing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class LocCovProcessor extends TowingCoverageProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1356874327680135266L;
	private String					localCoverageType;
	private String					locCoverageCode;
	private List<LocCoverageDto>   	locCoverageDtoList;
	private LocCoverageDto		 	newLocCoverageDto;
	private List<LocCoverageDto> 	saveList;
	private List<LocCoverageDto>   	deletedList;
	private List<LocCoverageDto>   	undeletedList;


	public LocCovProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			BaseCoverageConfigUIHelper coverageConfigUIHelper,
			IGenericProductConfig productConfig,
			UIConstants.OBJECTTYPES parentObjectType,
			String localCoverageType, String locCoverageCode) {
		super(processAction, policyTransaction, coverageConfigUIHelper, productConfig, parentObjectType);
		// TODO Auto-generated constructor stub
		
		this.localCoverageType	= localCoverageType;
		this.locCoverageCode 	= locCoverageCode;
	}

	public LocCovProcessor withParameters(ICoverageConfigParent covConfigParent, IBusinessEntity covParent,
										 	CoverageDto			coverageDto,		
										 	List<LocCoverageDto> 	saveList,
										 	List<LocCoverageDto>  deletedList,
										 	List<LocCoverageDto>  undeletedList) {
		super.withParameters(covConfigParent, covParent, Arrays.asList(coverageDto), true);
		
		this.saveList = saveList;
		this.deletedList = deletedList;
		this.undeletedList = undeletedList;
		
		return this;
	}

	public List<LocCoverageDto> getLocCoverageDtoList() {
		return locCoverageDtoList;
	}

	public LocCoverageDto getNewLocCoverageDto() {
		return newLocCoverageDto;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_List:
			try {
				locCoverageDtoList = buildLocCoverageList(policyProductConfig, policyTransaction, localCoverageType, locCoverageCode);
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}
			return;

		case UIConstants.UIACTION_Add:
			newLocCoverageDto = createNewLocCoverageDto(policyProductConfig, policyTransaction, localCoverageType, locCoverageCode);
			return;

		case UIConstants.UIACTION_Delete:
			try {
				preDeleteAction();
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				preUndeleteAction();
			} catch (InsurancePolicyException e) {
				setProcessException(e);
				return;
			}				
			break;
			
		default:
			break;
		}
		
		// Process Garage Coverage
		// This one reload policy transaction and parent
		/* handle by DSTowingSubPolicyCoverageProcessor
		if (!runCoverageAction()) {
			return;
		}
		*/

		// Process Location Coverages
		if (!runLocCoveragesAction(saveList, deletedList, undeletedList)) {
			return;
		}
		
		/* handle by DSTowingSubPolicyCoverageProcessor
		try {
			updatePolicyTransaction(policyTransaction);
		} catch (Exception e) {
			setProcessException(e);
			return;
		}
		
		ratePolicyTransaction();
		*/
	}
	
	// Delete all the location coverages
	protected void preDeleteAction() throws InsurancePolicyException {
		List<LocCoverageDto> list = buildLocCoverageList(policyProductConfig, policyTransaction, localCoverageType, locCoverageCode);
		deletedList = new ArrayList<LocCoverageDto>();
		if (list != null) {
			deletedList.addAll(list);
		}
	}
	
	// Undelete all the location coverages
	protected void preUndeleteAction() throws InsurancePolicyException {
		List<LocCoverageDto> list = buildLocCoverageList(policyProductConfig, policyTransaction, localCoverageType, locCoverageCode);
		undeletedList = new ArrayList<LocCoverageDto>();
		if (list != null) {
			undeletedList.addAll(list);
		}
	}

	protected boolean runLocCoveragesAction(List<LocCoverageDto> dtos, List<LocCoverageDto> deleted, List<LocCoverageDto> undeleted) {
		List<LocCoverageDto> updList = new ArrayList<LocCoverageDto>();
		
		if (dtos != null && !dtos.isEmpty()) {
			updList.addAll(dtos);
			// No Policy Location entered
			updList.removeIf(o -> o.getPolicyLocation() == null);
			
			updList.stream().forEach(o -> {
				if (o.getPolicyLocation().getPolicyRiskPK() != null) {
					o.setPolicyLocation((PolicyLocation) findRisk(o.getPolicyLocation().getPolicyRiskPK()));
				}
			});			
		}
		if (deleted != null && !deleted.isEmpty()) {
			deleted.stream().forEach(o -> {
				if (o.getPolicyLocation() != null &&
					o.getPolicyLocation().getPolicyRiskPK() != null) {
					o.setPolicyLocation((PolicyLocation) findRisk(o.getPolicyLocation().getPolicyRiskPK()));
				}
			});			
		}
		
		if (deleted != null && !deleted.isEmpty()) {
			// No Policy Location entered
			deleted.removeIf(o -> o.getPolicyLocation() == null);
			
			/* hard-to do
			// Policy Location was re-entered
			if (updList != null && !updList.isEmpty()) {
				deleted.removeIf(o -> updList.stream().filter(o2 -> o2.getPolicyLocation() != null && o2.getPolicyLocation().equals(o.getPolicyLocation()))
										.findFirst().orElse(null) != null);
			}
			*/
		}
		
		if (deleted != null && !deleted.isEmpty()) {
			deleted.stream().forEach(o -> {
				try {
					detachCoverage(o.getPolicyLocation(), o);
				} catch (Exception e) {
					setProcessException(e);
				}
			});
		}
		
		if (undeleted != null && !undeleted.isEmpty()) {
			undeleted.stream().forEach(o -> {
				try {
					undeleteCoverage(o.getPolicyLocation(), o);
				} catch (Exception e) {
					setProcessException(e);
				}
			});
		}

		if (updList != null && !updList.isEmpty()) {
			updList.stream().forEach(o -> {
				try {
					attachCoverage(o.getPolicyLocation(), o);
				} catch (Exception e) {
					setProcessException(e);
				}
			});
		}
		
		return (getProcessException() == null); 
	}
	
	private List<LocCoverageDto> buildLocCoverageList(IGenericProductConfig productConfig,
														 	PolicyTransaction<Risk> polTransaction,
														 	String localCoverageType, String locCoverageCode) throws InsurancePolicyException {
		List<LocCoverageDto> results = new ArrayList<LocCoverageDto>();
		
		RiskConfig riskConfig = productConfig.getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION);
		if (riskConfig != null) {
			if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(localCoverageType)) {
				EndorsementConfig endorsementConfig = riskConfig.getEndorsementConfig(locCoverageCode);
				if (endorsementConfig != null) {
					List<Risk> locRisks = polTransaction.getPolicyRisks()
												.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
												.collect(Collectors.toList());
					for(Risk risk : locRisks) {
						PolicyLocation location = (PolicyLocation) risk;
						location = (PolicyLocation) loadPolicyRiskData(location);
						List<Endorsement> endorsements = location.getRiskEndorsements().stream().filter(o -> locCoverageCode.equalsIgnoreCase(o.getEndorsementCd()))
																	.collect(Collectors.toList());
						if (!endorsements.isEmpty()) {
							for(Endorsement endorsement : endorsements) {
								LocCoverageDto newCoverageDto = createLocCoverageDto(endorsementConfig, endorsement, location);
								newCoverageDto.setPolicyLocation(location);
								
								initializeCoverageDto(newCoverageDto);
								
								results.add(newCoverageDto);
							}
						}
					}
				}
			}
			else {
				CoverageConfig coverageConfig = riskConfig.getCoverageConfig(locCoverageCode);
				if (coverageConfig != null) {
					List<Risk> locRisks = polTransaction.getPolicyRisks()
												.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
												.collect(Collectors.toList());
					for(Risk risk : locRisks) {
						PolicyLocation location = (PolicyLocation) risk;
						location = (PolicyLocation) loadPolicyRiskData(location);
						List<Coverage> coverages = location.getRiskCoverages().stream().filter(o -> locCoverageCode.equalsIgnoreCase(o.getCoverageCode()))
																	.collect(Collectors.toList());
						if (!coverages.isEmpty()) {
							for(Coverage coverage : coverages) {
								LocCoverageDto newCoverageDto = createLocCoverageDto(coverageConfig, coverage, location);
								newCoverageDto.setPolicyLocation(location);
								
								initializeCoverageDto(newCoverageDto);
								
								results.add(newCoverageDto);
							}
						}
					}
				}
			}
		}
		
		return results;
	}
	
	private LocCoverageDto createNewLocCoverageDto(IGenericProductConfig productConfig, 
														PolicyTransaction<Risk> polTransaction,
														String localCoverageType, String locCoverageCode) {
		PolicyLocation location = null;
		List<Risk> locRisks = polTransaction.getPolicyRisks()
									.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
									.collect(Collectors.toList());
		if (locRisks != null && !locRisks.isEmpty()) {
			location = (PolicyLocation) locRisks.get(0);
		}
		
		RiskConfig riskConfig = productConfig.getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION);
		if (riskConfig != null) {
			if (ConfigConstants.COVERAGETYPE_ENDORSEMENT.equalsIgnoreCase(localCoverageType)) {
				EndorsementConfig endorsementConfig = riskConfig.getEndorsementConfig(locCoverageCode);
				Endorsement endorsement = null;
				try {
					CoverageDto coverageDto = createCoverage(ConfigConstants.COVERAGETYPE_ENDORSEMENT, endorsementConfig, 
																location,
																policyTransaction);
					endorsement = (Endorsement) coverageDto.getCoverage();
				} catch (Exception e) {
					setProcessException(e);
				}
				
				LocCoverageDto newCoverageDto = createLocCoverageDto(endorsementConfig, endorsement, location);
				return newCoverageDto;
			}
			else {
				CoverageConfig coverageConfig = riskConfig.getCoverageConfig(locCoverageCode);
				Coverage coverage = null;
				try {
					CoverageDto coverageDto = createCoverage(ConfigConstants.COVERAGETYPE_COVERAGE, coverageConfig, 
																location,
																policyTransaction);
					coverage = (Coverage) coverageDto.getCoverage();
				} catch (Exception e) {
					setProcessException(e);
				}
				
				LocCoverageDto newCoverageDto = createLocCoverageDto(coverageConfig, coverage, location);
				return newCoverageDto;
			}
		}
		
		return null;
	}
	
	private LocCoverageDto createLocCoverageDto(CoverageConfig coverageConfig, Coverage coverage, PolicyLocation location) {
		LocCoverageDto newCoverageDto = new LocCoverageDto(coverageConfig, coverage, location);
		initializeCoverageDto(newCoverageDto);
		
		return newCoverageDto;
	}
	
	private LocCoverageDto createLocCoverageDto(EndorsementConfig endorsementConfig, Endorsement endorsement, PolicyLocation location) {
		LocCoverageDto newCoverageDto = new LocCoverageDto(endorsementConfig, endorsement, location);
		initializeCoverageDto(newCoverageDto);
		
		return newCoverageDto;
	}

}
