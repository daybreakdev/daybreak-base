package com.echelon.ui.process.towing;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class VehicleCoverageProcessor extends TowingCoverageProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -707621991571844767L;

	public VehicleCoverageProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			BaseCoverageConfigUIHelper coverageConfigUIHelper, IGenericProductConfig productConfig, OBJECTTYPES parentObjectType) {
		super(processAction, policyTransaction, coverageConfigUIHelper, productConfig, parentObjectType);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void initializeCoverageDto(CoverageDto dto) {
		// TODO Auto-generated method stub
		super.initializeCoverageDto(dto);
		
		if (dto != null && covParent != null && covParent instanceof SpecialtyVehicleRisk) {
			SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
			
			Integer numOfUnits = 1;
			if (vehicle.getNumberOfUnits() != null && vehicle.getNumberOfUnits() > 0) {
				numOfUnits = vehicle.getNumberOfUnits();
			}
			
			dto.setDispNumOfUnits(numOfUnits);
		}
	}

	@Override
	public boolean commitRisk(Risk parent) throws InsurancePolicyException {
		try {
			((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSpecialtyVehicleRisk((SpecialtyVehicleRisk) parent, getUserProfile());
			loadPolicyTransaction();
		} catch (SpecialtyAutoPolicyException e) {
			throw new InsurancePolicyException(e);
		}
		return true;
	}

}
