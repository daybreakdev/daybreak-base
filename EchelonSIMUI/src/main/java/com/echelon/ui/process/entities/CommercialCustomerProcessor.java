package com.echelon.ui.process.entities;

import java.io.Serializable;

import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.interfaces.customer.ICommercialCustomerProcessing;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class CommercialCustomerProcessor extends BasePolicyProcessor implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3672668261573478666L;
	private Customer 		customer;
	private	PolicyLocation 	insuredLocatoion;
	private ICommercialCustomerProcessing commercialCustomerProcessing;

	public CommercialCustomerProcessor(String processAction, PolicyTransaction policyTransaction, 
			Customer customer, PolicyLocation insuredLocatoion,
			ICommercialCustomerProcessing commercialCustomerProcessing) {
		super(processAction, policyTransaction);
		this.customer = customer;
		this.insuredLocatoion = insuredLocatoion;
		this.commercialCustomerProcessing = commercialCustomerProcessing;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_Save:
			try {
				//saveCustomer(customer);
				saveInsuredAddress(policyTransaction, insuredLocatoion);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}
		
	/*
	private Customer saveCustomer(Customer customer) throws Exception {
		Customer savedCustomer = commercialCustomerProcessing.saveCommercialCustomer(customer, getUserProfile());
		return savedCustomer;
	}
	*/
	
	private void saveInsuredAddress(PolicyTransaction<?> policyTransaction, PolicyLocation insuredLocatoion) throws Exception {
		((ISpecialtyAutoPolicyProcessing)getPolicyProcessing()).updateCustomerMainAddress(policyTransaction, getUserProfile());
	}

}
