package com.echelon.ui.process.towing;

import java.util.List;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.session.CoverageUIFactory;

public class TowingSubPolicyCoverageProcessor extends TowingCoverageProcessor {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8773239808045409833L;

	public TowingSubPolicyCoverageProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			BaseCoverageConfigUIHelper coverageConfigUIHelper, IGenericProductConfig productConfig, OBJECTTYPES parentObjectType) {
		super(processAction, policyTransaction, coverageConfigUIHelper, productConfig, parentObjectType);
		// TODO Auto-generated constructor stub
	}
	
	protected boolean isLocationCoverage(String coverageCode) {
		return CoverageUIFactory.getInstance().getLocationCodes(parentObjectType).contains(coverageCode);
	}
	
	protected boolean locCovAction(String covProcessAction, IBusinessEntity covParent, CoverageDto coverageDto) {
		if (UIConstants.UIACTION_Save.equals(processAction) || UIConstants.UIACTION_SaveNew.equals(processAction)) {
			if (isLocationCoverage(coverageDto.getCoverageCode())) {
				LocCovProcessor locCovProcessor = createLocCovActionProcessor(covProcessAction, coverageDto);
				locCovProcessor.run();
			}
		}
		
		return true;
	}

	@Override
	protected boolean saveNewCov(IBusinessEntity parent, CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		boolean ok = super.saveNewCov(parent, coverageDto);
		
		if (ok) {
			ok = locCovAction(UIConstants.UIACTION_SaveNew, parent, coverageDto);
		}
		
		return ok;
	}

	@Override
	protected boolean saveCov(IBusinessEntity parent, CoverageDto coverageDto) {
		// TODO Auto-generated method stub
		boolean ok = super.saveCov(parent, coverageDto);
		
		if (ok) {
			ok = locCovAction(UIConstants.UIACTION_Save, parent, coverageDto);
		}
		
		return ok;
	}

	@Override
	public boolean detachCoverage(IBusinessEntity parent, CoverageDto coverageDto) throws InsurancePolicyException {
		if (isLocationCoverage(coverageDto.getCoverageCode())) {
			LocCovProcessor locCovProcessor = createLocCovActionProcessor(UIConstants.UIACTION_Delete, coverageDto);
			locCovProcessor.run();
		}
		
		return super.detachCoverage(parent, coverageDto);
	}

	@Override
	public boolean undeleteCoverage(IBusinessEntity parent, CoverageDto coverageDto) throws InsurancePolicyException {
		if (isLocationCoverage(coverageDto.getCoverageCode())) {
			LocCovProcessor locCovProcessor = createLocCovActionProcessor(UIConstants.UIACTION_Undelete, coverageDto);
			locCovProcessor.run();
		}
		
		return super.undeleteCoverage(parent, coverageDto);
	}

	protected LocCovProcessor createLocCovActionProcessor(String processAction, CoverageDto coverageDto) {
	 	List<LocCoverageDto> saveList 	 = null;
	 	List<LocCoverageDto> deletedList   = null;
	 	List<LocCoverageDto> undeletedList = null;
	 	
	 	if (UIConstants.UIACTION_Save.equals(processAction) || UIConstants.UIACTION_SaveNew.equals(processAction)) {
	 		if (coverageDto.getLocCovProcessDto() != null) {
		 		saveList      = coverageDto.getLocCovProcessDto().getDtoList();
		 		deletedList   = coverageDto.getLocCovProcessDto().getDeletedList();
		 		undeletedList = coverageDto.getLocCovProcessDto().getUndeletedList();
	 		}
	 	}
		
		return createLocCovProcessor(processAction, coverageDto)
				.withParameters(covConfigParent, covParent, coverageDto, 
									saveList, deletedList, undeletedList);
	}

	protected LocCovProcessor createLocCovProcessor(String processAction, CoverageDto coverageDto) {
		LocCovProcessor locCovProcessor = new LocCovProcessor(processAction, policyTransaction, 
																  coverageConfigUIHelper, productConfig, parentObjectType, 
																  coverageDto.getCoverageType(), coverageDto.getCoverageCode());
		// Copy main values
		locCovProcessor.setProcessorValues(this);
		return locCovProcessor;
	}

}
