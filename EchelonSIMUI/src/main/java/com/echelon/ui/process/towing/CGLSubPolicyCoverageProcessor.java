package com.echelon.ui.process.towing;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;

public class CGLSubPolicyCoverageProcessor extends TowingSubPolicyCoverageProcessor {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4167840272004290649L;

	public CGLSubPolicyCoverageProcessor(String processAction, PolicyTransaction<?> policyTransaction,
			BaseCoverageConfigUIHelper coverageConfigUIHelper, IGenericProductConfig productConfig,
			OBJECTTYPES parentObjectType) {
		super(processAction, policyTransaction, coverageConfigUIHelper, productConfig, parentObjectType);
	}

	@Override
	public boolean commitSubPolicy(SubPolicy<?> parent) throws InsurancePolicyException {
		
		// Need this one for saving location coverages
		super.commitSubPolicy(parent);
		
		// Need to trigger logic in updateCGLSubPolicy
		// This is the original behaviour
		CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) policyTransaction.findSubPolicyByPk(parent.getSubPolicyPK());
		try {
			((ISpecialtyAutoPolicyProcessing)this.policyProcessing).updateCGLSubPolicy(cglSubPolicy, cglSubPolicy, getUserProfile());
		} catch (SpecialtyAutoPolicyException e) {
			throw new InsurancePolicyException(e.getMessage(), e);
		}
		
		return true;
	}

}
