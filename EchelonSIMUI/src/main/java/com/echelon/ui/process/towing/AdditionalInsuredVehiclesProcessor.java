package com.echelon.ui.process.towing;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;

public class AdditionalInsuredVehiclesProcessor extends BasePolicyProcessor implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5224319373058687870L;
	private SpecialityAutoSubPolicy<Risk>		autoSubPolicy;		
	private AdditionalInsured 				 	additionalInsured;
	private AdditionalInsuredVehicles 		 	additionalInsuredVehicles;
	private List<AdditionalInsuredVehicles> 	newAddlInsuredVehiclesList;
	
	public AdditionalInsuredVehiclesProcessor(String processAction, PolicyTransaction policyTransaction,
			SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured,
			AdditionalInsuredVehicles additionalInsuredVehicles) {
		super(processAction, policyTransaction);
		this.autoSubPolicy = autoSubPolicy;
		this.additionalInsured = additionalInsured;
		this.additionalInsuredVehicles = additionalInsuredVehicles;
	}

	public AdditionalInsuredVehiclesProcessor(String processAction, PolicyTransaction policyTransaction,
			SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured,
			List<AdditionalInsuredVehicles> newAddlInsuredVehiclesList) {
		super(processAction, policyTransaction);
		this.autoSubPolicy = autoSubPolicy;
		this.additionalInsured = additionalInsured;
		this.newAddlInsuredVehiclesList = newAddlInsuredVehiclesList;
	}

	public SpecialityAutoSubPolicy<Risk> getAutoSubPolicy() {
		return autoSubPolicy;
	}

	public AdditionalInsured getAdditionalInsured() {
		return additionalInsured;
	}

	public AdditionalInsuredVehicles getAdditionalInsuredVehicles() {
		return additionalInsuredVehicles;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				create(autoSubPolicy, additionalInsured, newAddlInsuredVehiclesList);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				save(autoSubPolicy, additionalInsured, additionalInsuredVehicles);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				delete(autoSubPolicy, additionalInsured, additionalInsuredVehicles);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undelete(autoSubPolicy, additionalInsured, additionalInsuredVehicles);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}
	
	private void create(SpecialityAutoSubPolicy<Risk> subPolicy, AdditionalInsured additionalInsured, List<AdditionalInsuredVehicles> newAddlInsuredVehiclesList) throws InsurancePolicyException {
		if (newAddlInsuredVehiclesList != null && !newAddlInsuredVehiclesList.isEmpty()) {
			SpecialityAutoSubPolicy<Risk> currSubPolicy = (SpecialityAutoSubPolicy) loadSubPolicy(subPolicy.getSubPolicyTypeCd());
			
			AdditionalInsured currAdditionalInsured = currSubPolicy.getAdditionalInsureds().stream().filter(
													o -> o.getAdditionalInsuredPK().longValue() == additionalInsured.getAdditionalInsuredPK().longValue()
												).findFirst().orElse(null);
			
			newAddlInsuredVehiclesList.stream()
					.forEach(o -> {
						if (o.getAdditionalInsured() == null) {
							o.setAdditionalInsured(currAdditionalInsured);	
						}
						if (o.getAdditionalInsuredVehiclesPK() == null) {
							currSubPolicy.addAdditionalInsuredVehicles(o);
						}
					});
			
			save(currSubPolicy, currAdditionalInsured, null);
		}
	}

	private void save(SpecialityAutoSubPolicy<Risk> subPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles addlInsuredVehicles) throws InsurancePolicyException {
		subPolicy = (SpecialityAutoSubPolicy) ((ISpecialtyAutoPolicyProcessing)policyProcessing).updateSubPolicy(subPolicy, getUserProfile());
	}
	
	private void delete(SpecialityAutoSubPolicy<Risk> subPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles addlInsuredVehicles) throws InsurancePolicyException  {
		subPolicy.removeAdditionalInsuredVehicles(addlInsuredVehicles);
		save(subPolicy, additionalInsured, addlInsuredVehicles);
	}
	
	private void undelete(SpecialityAutoSubPolicy<?> subPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles addlInsuredVehicles) throws InsurancePolicyException, SpecialtyAutoPolicyException  {
		((ISpecialtyAutoPolicyProcessing)policyProcessing).undeleteAddInsVehicle((PolicyTransaction<SpecialtyVehicleRisk>)policyTransaction, 
																				 (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>)subPolicy, 
																					addlInsuredVehicles);
		save((SpecialityAutoSubPolicy<Risk>) subPolicy, additionalInsured, addlInsuredVehicles);
	}

}
