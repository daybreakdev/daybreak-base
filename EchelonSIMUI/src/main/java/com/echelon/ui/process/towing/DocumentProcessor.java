package com.echelon.ui.process.towing;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.services.document.AutoFleetSchedule;
import com.echelon.services.document.PolicyAdjustmentsLHT;
import com.echelon.services.document.PolicyAdjustmentsTowing;
import com.echelon.services.document.PolicyCancelLHT;
import com.echelon.services.document.PolicyCancelTowing;
import com.echelon.services.document.PolicyChangeLHT;
import com.echelon.services.document.PolicyChangeTowing;
import com.echelon.services.document.PolicyExtension;
import com.echelon.services.document.PolicyExtensionLHT;
import com.echelon.services.document.PolicyPackageLHT;
import com.echelon.services.document.PolicyPackageNonFleet;
import com.echelon.services.document.PolicyPackageTowing;
import com.echelon.services.document.QuoteLetter;
import com.echelon.services.document.QuoteLetterLHT;
import com.echelon.services.document.QuoteLetterNonFleet;
import com.echelon.services.document.QuoteLetterSchLHT;
import com.ds.ins.utils.Configurations;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;

public class DocumentProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2812587208731999003L;
	private MemoryBuffer memoryBuffer;
	private PolicyDocuments policyDocuments;

	public DocumentProcessor(String processAction, PolicyTransaction policyTransaction,
			PolicyDocuments policyDocuments) {
		super(processAction, policyTransaction);
		this.policyDocuments = policyDocuments;
	}

	public DocumentProcessor(String processAction, PolicyTransaction policyTransaction, PolicyDocuments policyDocuments,
			MemoryBuffer memoryBuffer) {
		super(processAction, policyTransaction);
		this.policyDocuments = policyDocuments;
		this.memoryBuffer = memoryBuffer;
	}

	public PolicyDocuments getPolicyDocuments() {
		return policyDocuments;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_SaveNew:
			try {
				loadPolicyTransaction();
				saveNewDocument(policyTransaction, policyDocuments, memoryBuffer);
				policyDocuments = updatePolicyDocument(policyDocuments);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				policyDocuments = updatePolicyDocument(policyDocuments);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Generate:
			try {
				policyTransaction = loadFullPolicyTransactionData(policyTransaction);
				policyTransaction = policyProcessing.runPolicyRules(policyTransaction,
						SpecialtyAutoConstants.RULE_GROUP_DOCUMENT);
				if (policyTransaction.getPolicyMessages() != null && !policyTransaction.getPolicyMessages().isEmpty()) {
					break;
				}

				if (policyDocuments.getStorageKey() != null && !policyDocuments.getStorageKey().isEmpty()) {
					deleteFile(policyDocuments.getStorageKey());
				}

				if (policyDocuments.getStorageType() == null) {
					policyDocuments.setStorageType("DOC");// word document as default
				}

				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
				String time = sdf.format(new Date());
				String storageKey = "";
				String ext;
				switch (policyDocuments.getStorageType()) {
				case "PDF":
					ext = ".pdf";
					break;
				case "DOC":
					ext = ".docx";
					break;
				default:
					policyDocuments.setStorageType("DOC");
					ext = ".docx";
				}

				if (policyDocuments.getDocumentId().equalsIgnoreCase("QL")) {
					String quoteNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getQuoteNum();
					storageKey = "QL_" + quoteNo + "_" + time + ".docx";
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("PP")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "PP_" + policyNo + "_" + time + ext : "PP_" + time + ext;
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("AF")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "AF_" + policyNo + "_" + time + ext : "AF_" + time + ext;
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("PC")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "PC_" + policyNo + "_" + time + ext : "PC_" + time + ext;
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("CA")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "CA_" + policyNo + "_" + time + ext : "CA_" + time + ext;
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("EP")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "EP_" + policyNo + "_" + time + ext : "EP_" + time + ext;
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("ADJ")) {
					String policyNo = policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getBasePolicyNum();
					storageKey = !policyNo.equals("") ? "ADJ_" + policyNo + "_" + time + ext : "ADJ_" + time + ext;
				}
				policyDocuments.setStorageKey(storageKey);
				// policyDocuments.setStorageType("DOC");
				policyDocuments.setBusinessStatus("G");
				policyDocuments.setDistributionTarget("BR");
				policyDocuments.setUpdatedDate(LocalDateTime.now());
				policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
						.setQuotePreparedDate(LocalDateTime.now());

				if (policyDocuments.getDocumentId().equalsIgnoreCase("QL")) {
					SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyTransaction
							.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
					if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("TO")) {
						if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
								.getProductCd().equalsIgnoreCase("TOWING")) {
							QuoteLetter quote = new QuoteLetter(policyDocuments);
							quote.create(policyDocuments.getStorageKey());
						} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
								.getProductCd().equalsIgnoreCase("NONFLEET")) {
							QuoteLetterNonFleet quote = new QuoteLetterNonFleet(policyDocuments);
							quote.create(policyDocuments.getStorageKey());
						}
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("LO")) {
						if (subPolicyAuto.getFleetBasis()
								.equalsIgnoreCase(SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED)) {
							QuoteLetterSchLHT quote = new QuoteLetterSchLHT(policyDocuments);
							quote.create(policyDocuments.getStorageKey());
						} else {
							QuoteLetterLHT quote = new QuoteLetterLHT(policyDocuments);
							quote.create(policyDocuments.getStorageKey());
						}
					}
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("PP")) {
					if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("TO")) {
						if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
								.getProductCd().equalsIgnoreCase("TOWING")) {
							PolicyPackageTowing pp = new PolicyPackageTowing(policyDocuments);
							pp.create(policyDocuments.getStorageKey());
						} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
								.getProductCd().equalsIgnoreCase("NONFLEET")) {
							PolicyPackageNonFleet pp = new PolicyPackageNonFleet(policyDocuments);
							pp.create(policyDocuments.getStorageKey());
						}
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("LO")) {
						PolicyPackageLHT pp = new PolicyPackageLHT(policyDocuments);
						pp.create(policyDocuments.getStorageKey());
					}
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("AF")) {
					AutoFleetSchedule af = new AutoFleetSchedule(policyDocuments);
					af.create(policyDocuments.getStorageKey());
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("PC")) {
					if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("TO")) {
						PolicyChangeTowing pc = new PolicyChangeTowing(policyDocuments);
						pc.create(policyDocuments.getStorageKey());
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy()
							.getProductProgram().equalsIgnoreCase("LO")) {
						PolicyChangeLHT pc = new PolicyChangeLHT(policyDocuments);
						pc.create(policyDocuments.getStorageKey());
					}
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("CA")) {
					if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("TO")) {
						PolicyCancelTowing ca = new PolicyCancelTowing(policyDocuments);
						ca.create(policyDocuments.getStorageKey());
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("LO")) {
						PolicyCancelLHT ca = new PolicyCancelLHT(policyDocuments);
						ca.create(policyDocuments.getStorageKey());
					}
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("EP")) {
					if(policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("TO")) {
						PolicyExtension ep = new PolicyExtension(policyDocuments);
						ep.create(policyDocuments.getStorageKey());
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("LO")) {
						PolicyExtensionLHT ep = new PolicyExtensionLHT(policyDocuments);
						ep.create(policyDocuments.getStorageKey());
					}
				} else if (policyDocuments.getDocumentId().equalsIgnoreCase("ADJ")) {
					SpecialtyPolicyDTO policyDTO = (new SpecialtyAutoPolicyProcesses()).getSpecialtyPolicyDTO(policyTransaction);
					if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("TO")) {
						PolicyAdjustmentsTowing pa = new PolicyAdjustmentsTowing(policyDocuments, policyDTO);
						pa.create(policyDocuments.getStorageKey());
					} else if (policyDocuments.getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getProductProgram().equalsIgnoreCase("LO")) {
						PolicyAdjustmentsLHT pa = new PolicyAdjustmentsLHT(policyDocuments, policyDTO);
						pa.create(policyDocuments.getStorageKey());
					}
				}
				policyDocuments = updatePolicyDocument(policyDocuments);
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteFile(policyDocuments.getStorageKey());
				deletePolicyDocument(policyDocuments);
				policyDocuments = null;
			} catch (Exception e) {
				setProcessException(e);
				return;
			}
			loadPolicyTransaction();
			break;

		default:
			break;
		}
	}

	private PolicyDocuments updatePolicyDocument(PolicyDocuments policyDocuments) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing) policyProcessing).updatePolicyDocument(policyDocuments,
				getUserProfile());
	}

	private void deletePolicyDocument(PolicyDocuments policyDocuments) throws Exception {
		((ISpecialtyAutoPolicyProcessing) policyProcessing).deletePolicyDocument(policyDocuments, getUserProfile());
	}

	private void saveNewDocument(PolicyTransaction policyTransaction, PolicyDocuments document,
			MemoryBuffer memoryBuffer) throws IOException, Exception {
		if (document.getPolicyTransaction() == null) {
			document.setPolicyTransaction(policyTransaction);
		}
		document.setCreatedDate(LocalDateTime.now());
		document.setUpdatedDate(LocalDateTime.now());

		if (document.getDocumentId().equalsIgnoreCase("UP")) {
			document.setBusinessStatus("U");

			String ext = FilenameUtils.getExtension(memoryBuffer.getFileName());
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String time = sdf.format(new Date());
			String storageKey = "UP_" + time + "." + ext;
			document.setStorageKey(storageKey);
			if (memoryBuffer.getFileData().getMimeType().equalsIgnoreCase("application/msword")
					|| memoryBuffer.getFileData().getMimeType().equalsIgnoreCase(
							"application/vnd.openxmlformats-officedocument.wordprocessingml.document")) {
				document.setStorageType("DOC");
			} else if (memoryBuffer.getFileData().getMimeType().equalsIgnoreCase("application/pdf")) {
				document.setStorageType("PDF");
			} else if (memoryBuffer.getFileData().getMimeType().equalsIgnoreCase("text/plain")) {
				document.setStorageType("TXT");
			}

			saveFile(memoryBuffer, document.getStorageKey());
		} else {
			document.setBusinessStatus("P");
		}
	}

	protected void saveFile(MemoryBuffer memoryBuffer, String storageKey) throws Exception, IOException {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		InputStream inputStream = memoryBuffer.getInputStream();
		File file = new File(attachPath + File.separator + storageKey);
		FileUtils.copyInputStreamToFile(inputStream, file);
		file.createNewFile();
		inputStream.close();
	}

	protected void deleteFile(String storageKey) {
		if (storageKey != null && !storageKey.equalsIgnoreCase("")) {
			String attachPath = Configurations.getInstance().getProperty("AttachPath");
			File file = new File(attachPath + File.separator + storageKey);
			if (file.exists()) {
				file.delete();
			}
		}
	}

}
