package com.echelon.ui.process.wheels;

import java.io.Serializable;

import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;

public class DriverProcessor extends BasePolicyProcessor implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6345570475067883770L;
	private Driver driver;

	@SuppressWarnings("rawtypes")
	public DriverProcessor(String processAction, PolicyTransaction policyTransaction, Driver driver) {
		super(processAction, policyTransaction);
		this.driver = driver;
	}

	public Driver getDriver() {
		return driver;
	}

	@Override
	public void run() {
		switch (processAction) {
		case UIConstants.UIACTION_New:
			try {
				driver = createDriver(policyTransaction);
			} catch (Exception e) {
				setProcessException(e);
			}
			break;

		case UIConstants.UIACTION_SaveNew:
			try {
				saveNewDriver(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Save:
			try {
				saveDriver(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Delete:
			try {
				deleteDriver(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;

		case UIConstants.UIACTION_Undelete:
			try {
				undeleteDriver(policyTransaction, driver);
			} catch (Exception e) {
				setProcessException(e);
			}
			loadPolicyTransaction();
			break;
			
		default:
			break;
		}
	}
	
	@SuppressWarnings("unchecked")
	private Driver createDriver(PolicyTransaction<?> policyTransaction) throws SpecialtyAutoPolicyException, InsurancePolicyException {
		Driver driver = policyProcessing.createNewDriver(policyTransaction);
		// ENFO-239 So no need to do any action if user cancel the action
		policyTransaction.getDrivers().remove(driver);
		return driver;
	}
	
	private Driver saveNewDriver(PolicyTransaction<?> policyTransaction, Driver driver) throws InsurancePolicyException {
		policyTransaction.addDriver(driver);
		updatePolicyTransaction(policyTransaction);
		return driver;
	}
	
	private Driver saveDriver(PolicyTransaction<?> policyTransaction, Driver driver) throws InsurancePolicyException {
		updatePolicyTransaction(policyTransaction);
		return driver;
	}
	
	private boolean deleteDriver(PolicyTransaction<?> policyTransaction, Driver driver) throws InsurancePolicyException {
		policyProcessing.deleteDriver(driver, getUserProfile());
		return true;
	}
	
	@SuppressWarnings("unchecked")
	private boolean undeleteDriver(PolicyTransaction<?> policyTransaction, Driver driver) throws InsurancePolicyException {
		policyProcessing.undeleteDriver(driver, policyTransaction);
		return true;
	}

}
