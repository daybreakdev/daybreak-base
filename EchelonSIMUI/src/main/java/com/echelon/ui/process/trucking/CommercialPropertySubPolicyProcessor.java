package com.echelon.ui.process.trucking;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;

public class CommercialPropertySubPolicyProcessor
	extends BaseSubPolicyProcessor<CommercialPropertySubPolicy<?>>
	implements Serializable {

	private CommercialPropertySubPolicy<?> preSubPolicy;

	public CommercialPropertySubPolicyProcessor(String processAction,
		PolicyTransaction policyTransaction,
		CommercialPropertySubPolicy<?> subPolicy) {
		super(processAction, policyTransaction, subPolicy);
	}

	public CommercialPropertySubPolicyProcessor(String processAction,
		PolicyTransaction policyTransaction,
		CommercialPropertySubPolicy<?> subPolicy,
		CommercialPropertySubPolicy<?> preSubPolicy) {
		super(processAction, policyTransaction, subPolicy);
		this.preSubPolicy = preSubPolicy;
	}

	public CommercialPropertySubPolicyProcessor(String processAction,
		PolicyTransaction policyTransaction, String subPolicyTypeCd) {
		super(processAction, policyTransaction, subPolicyTypeCd);
	}
	
	@Override
	protected CommercialPropertySubPolicy<?> createSubPolicy() throws SpecialtyAutoPolicyException {
		loadPolicyTransaction();
		return ((ISpecialtyAutoPolicyProcessing)policyProcessing).createCommercialPropertySubPolicy(policyTransaction);
	}
}
