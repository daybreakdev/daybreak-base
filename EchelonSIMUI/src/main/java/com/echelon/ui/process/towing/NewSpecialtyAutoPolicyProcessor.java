package com.echelon.ui.process.towing;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.customer.ICommercialCustomerProcessing;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.baseclasses.process.policy.BaseNewPolicyProcessor;
import com.echelon.ui.constants.UIConstants.NEWPOLICYSTEPS;
import com.echelon.utils.SpecialtyAutoConstants;

public class NewSpecialtyAutoPolicyProcessor extends BaseNewPolicyProcessor<SpecialtyAutoPackage, PolicyVersion, PolicyTransaction<?>, SubPolicy<?>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -349863797767968073L;

	public NewSpecialtyAutoPolicyProcessor(String processAction, PolicyTransaction policyTransaction, 
			ICommercialCustomerProcessing commercialCustomerProcessing, NewPolicyDto newPolicyDto,
			NEWPOLICYSTEPS step) {
		super(processAction, policyTransaction, commercialCustomerProcessing, newPolicyDto, step);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean createNewSubPolicies() throws Exception {
		if (!updatePolicyDetails()) {
			return false;
		}
		
		if (!refreshNewPolicyDto()) {
			return false;
		}
		
		boolean ok = false;
		
		try {
			if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy()) && newPolicyDto.getAutoSubPolicy() == null) {
				SpecialityAutoSubPolicy subPolicy = createAutoSubPolicy(newPolicyDto.getPolicyTransaction());
				newPolicyDto.setAutoSubPolicy(subPolicy);
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()) && newPolicyDto.getCargoSubPolicy() == null) {
				CargoSubPolicy subPolicy = createCargoSubPolicy(newPolicyDto.getPolicyTransaction());
				newPolicyDto.setCargoSubPolicy(subPolicy);
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()) && newPolicyDto.getcGLSubPolicy() == null) {
				CGLSubPolicy subPolicy = createCGLSubPolicy(newPolicyDto.getPolicyTransaction());
				newPolicyDto.setcGLSubPolicy(subPolicy);
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasGarageSubPolicy()) && newPolicyDto.getGarageSubPolicy() == null) {
				GarageSubPolicy subPolicy = createGarageSubPolicy(newPolicyDto.getPolicyTransaction());
				newPolicyDto.setGarageSubPolicy(subPolicy);
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCommercialPropertySubPolicy()) && newPolicyDto.getCommercialPropertySubPolicy() == null) {
				CommercialPropertySubPolicy subPolicy = createCommercialPropertySubPolicy(newPolicyDto.getPolicyTransaction());
				newPolicyDto.setCommercialPropertySubPolicy(subPolicy);
			}
			
			ok = true;
		} catch (Exception e) {
			throw e;
		}
		
		return ok;
	}

	@Override
	protected boolean saveNewSubPolicies() throws Exception {
		boolean ok = false;
		
		try {
			if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy()) && newPolicyDto.getAutoSubPolicy() != null) {
				saveAutoSubPolicy(newPolicyDto.getAutoSubPolicy());
				refreshNewPolicyDto();
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()) && newPolicyDto.getCargoSubPolicy() != null) {
				saveCargoSubPolicy(newPolicyDto.getCargoSubPolicy());
				refreshNewPolicyDto();
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()) && newPolicyDto.getcGLSubPolicy() != null) {
				saveCGLSubPolicy(newPolicyDto.getcGLSubPolicy());
				refreshNewPolicyDto();
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasGarageSubPolicy()) && newPolicyDto.getGarageSubPolicy() != null) {
				saveGarageSubPolicy(newPolicyDto.getGarageSubPolicy());
				refreshNewPolicyDto();
			}
			
			if (Boolean.TRUE.equals(newPolicyDto.getHasCommercialPropertySubPolicy()) && newPolicyDto.getCommercialPropertySubPolicy() != null) {
				saveCommercialPropertySubPolicy(newPolicyDto.getCommercialPropertySubPolicy());
				refreshNewPolicyDto();
			}
			
			ok = true;
		} catch (Exception e) {
			throw e;
		}
		
		return ok;
	}

	@Override
	protected boolean saveCustomer() throws Exception {
		getSpecialtyAutoPolicyProcessing().updateCustomerMainAddress(policyTransaction, getUserProfile());
		
		return true;
	}

	@Override
	protected boolean updateSubPolicies() throws Exception {
		getSpecialtyAutoPolicyProcessing().updateSpecialityAutoSubPolicy(newPolicyDto.getAutoSubPolicy(),
				getUserProfile());

		if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()) && newPolicyDto.getcGLSubPolicy() != null) {
			getSpecialtyAutoPolicyProcessing().updateCGLSubPolicy(newPolicyDto.getcGLSubPolicy(), null,
					getUserProfile());
		}

		if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()) && newPolicyDto.getCargoSubPolicy() != null) {
			getSpecialtyAutoPolicyProcessing().updateCargoSubPolicy(newPolicyDto.getCargoSubPolicy(), null,
					getUserProfile());
		}

		return true;
	}

	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) getPolicyProcessing();
	}

	protected SpecialityAutoSubPolicy<?> createAutoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> policyTransaction) throws SpecialtyAutoPolicyException  {
		return getSpecialtyAutoPolicyProcessing().createAutoSubPolicy(policyTransaction);
	}

	protected void saveAutoSubPolicy(SpecialityAutoSubPolicy<?> subPolicy) throws InsurancePolicyException {
		getPolicyProcessing().updatePolicyTransaction(subPolicy.getPolicyTransaction(), getUserProfile());
	}

	protected CargoSubPolicy<?> createCargoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> policyTransaction) throws SpecialtyAutoPolicyException  {
		return getSpecialtyAutoPolicyProcessing().createCargoSubPolicy(policyTransaction);
	}

	protected void saveCargoSubPolicy(CargoSubPolicy<?> subPolicy) throws InsurancePolicyException {
		getPolicyProcessing().updatePolicyTransaction(subPolicy.getPolicyTransaction(), getUserProfile());
	}

	protected CGLSubPolicy<?> createCGLSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> policyTransaction) throws SpecialtyAutoPolicyException  {
		return getSpecialtyAutoPolicyProcessing().createCGLSubPolicy(policyTransaction);
	}

	protected void saveCGLSubPolicy(CGLSubPolicy<?> subPolicy) throws InsurancePolicyException {
		getPolicyProcessing().updatePolicyTransaction(subPolicy.getPolicyTransaction(), getUserProfile());
	}

	protected GarageSubPolicy<?> createGarageSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> policyTransaction) throws SpecialtyAutoPolicyException  {
		return getSpecialtyAutoPolicyProcessing().createGarageSubPolicy(policyTransaction);
	}

	protected void saveGarageSubPolicy(GarageSubPolicy<?> subPolicy) throws InsurancePolicyException {
		getPolicyProcessing().updatePolicyTransaction(subPolicy.getPolicyTransaction(), getUserProfile());
	}
	
	protected CommercialPropertySubPolicy<?> createCommercialPropertySubPolicy(PolicyTransaction<SpecialtyVehicleRisk> policyTransaction) throws SpecialtyAutoPolicyException {
		return getSpecialtyAutoPolicyProcessing().createCommercialPropertySubPolicy(policyTransaction);
	}
	
	protected void saveCommercialPropertySubPolicy(CommercialPropertySubPolicy<?> subPolicy) throws InsurancePolicyException {
		getPolicyProcessing().updatePolicyTransaction(subPolicy.getPolicyTransaction(), getUserProfile());
	}

	protected boolean updatePolicyDetails() throws Exception {
		boolean ok = false;
		
		try {
			getSpecialtyAutoPolicyProcessing().updateSpecialtyAutoPackageData(newPolicyDto.getLiabilityLimit(), newPolicyDto.getQuotePreparedBy(), 
																				newPolicyDto.getPolicyTransaction().getPolicyTerm().getTermEffDate(), 
																				newPolicyDto.getPolicyTransaction().getPolicyTransactionPK(),
																				getUserProfile());
			ok = true;
		} catch (Exception e) {
			throw e;
		}
		
		return ok;
	}

	@Override
	protected boolean refreshNewPolicyDto() throws InsurancePolicyException {
		// TODO Auto-generated method stub
		if (!super.refreshNewPolicyDto()) {
			return false;
		}
		
		PolicyTransaction<Risk> polTransaction = newPolicyDto.getPolicyTransaction();
		if (polTransaction.getSubPolicies() != null) {
			newPolicyDto.setAutoSubPolicy(
					(SpecialityAutoSubPolicy) polTransaction.getSubPolicies().stream()
												.filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(o.getSubPolicyTypeCd()))
												.findFirst().orElse(null));
			newPolicyDto.setCargoSubPolicy(
					(CargoSubPolicy) polTransaction.getSubPolicies().stream()
												.filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_CARGO.equalsIgnoreCase(o.getSubPolicyTypeCd()))
												.findFirst().orElse(null));
			newPolicyDto.setcGLSubPolicy(
					(CGLSubPolicy) polTransaction.getSubPolicies().stream()
												.filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(o.getSubPolicyTypeCd()))
												.findFirst().orElse(null));
			newPolicyDto.setGarageSubPolicy(
					(GarageSubPolicy) polTransaction.getSubPolicies().stream()
												.filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_GARAGE.equalsIgnoreCase(o.getSubPolicyTypeCd()))
												.findFirst().orElse(null));
			
			newPolicyDto.setCommercialPropertySubPolicy(
					(CommercialPropertySubPolicy) polTransaction.getSubPolicies().stream()
												.filter(o -> SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY.equalsIgnoreCase(o.getSubPolicyTypeCd()))
												.findFirst().orElse(null));
		}
		
		return true;
	}
	
	@Override
	protected PolicyLocation getPolicyInsurerLocation(PolicyTransaction<Risk> policyTransaction) {
		PolicyLocation result = null;
		
		if (policyTransaction != null) {
			result = policyTransaction.getPolicyRisks().stream()
							.filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
							.map(o -> (PolicyLocation)o)
							.filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId()))
							.findFirst().orElse(null);
		}
		
		return result;
	}
	
}
