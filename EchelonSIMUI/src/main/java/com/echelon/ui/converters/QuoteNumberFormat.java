package com.echelon.ui.converters;

import java.io.Serializable;

public class QuoteNumberFormat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7441635789759334688L;

	public QuoteNumberFormat() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static String format(String quoteNum, Integer versionNumber) {
		String result = null;
		
		if (quoteNum != null) {
			result = quoteNum;
			
			if (versionNumber != null) {
				result = quoteNum+"-"+versionNumber;
			}
		}
		
		return result;
	}
}
