package com.echelon.ui.converters;

import java.text.DecimalFormat;

import com.ds.ins.uicommon.converters.DSNumericFormat;
import com.echelon.ui.constants.UIConstants;

public class SIMNumericFormat extends DSNumericFormat {

	public SIMNumericFormat() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public static DecimalFormat getFactorInstance() {
		return createInstance(UIConstants.DECIMALPLACES_FACTOR_MAX_DISPLAY, UIConstants.DECIMALPLACES_FACTOR_MIN_DISPLAY);
	}

}
