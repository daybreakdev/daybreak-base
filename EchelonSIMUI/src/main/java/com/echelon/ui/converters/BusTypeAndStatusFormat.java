package com.echelon.ui.converters;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;

public class BusTypeAndStatusFormat implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6112413361841321613L;
	private UIHelper uiHelper;

	public BusTypeAndStatusFormat() {
		super();
		// TODO Auto-generated constructor stub
		
		this.uiHelper = BrokerUI.getHelperSession().getUiHelper();
	}

	public String format(PolicyTransaction policyTransaction) {
		StringBuffer sb = new StringBuffer();
		
		String transType = null;
		try {
			transType = uiHelper.getSessionLabels().getString("PolicyTxnType"+policyTransaction.getPolicyVersion().getPolicyTxnType());
		} catch (Exception e) {
		}
		if (transType == null) {
			transType = policyTransaction.getPolicyVersion().getPolicyTxnType();
		}
		
		String busStatus = null;
		try {
			busStatus = uiHelper.getSessionLabels().getString("PolicyBusinessStatus"+policyTransaction.getPolicyVersion().getBusinessStatus());
		} catch (Exception e) {
		}
		if (busStatus == null) {
			policyTransaction.getPolicyTerm().getBusinessStatus();
		}
		
		if (transType != null) {
			sb.append(transType);
		}
		if (busStatus != null) {
			if (sb.length() > 0) {
				sb.append("/");
			}
			
			sb.append(busStatus);
		}
		
		return sb.toString();
	}
}
