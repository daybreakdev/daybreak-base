package com.echelon.ui.helpers.policy.towing;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.DocumentForm;
import com.echelon.ui.components.towing.DocumentList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.process.towing.DocumentProcessor;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.utils.Configurations;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.upload.receivers.MemoryBuffer;
import com.vaadin.flow.server.StreamResource;

public class DocumentUIHelper extends ContentUIHelper implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3128292945309085246L;
	
	public ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyUIHelper.getPolicyProcessing();
	}
	
	public void loadView(DocumentList component, DSTreeGridItem nodeItem) {
		component.loadData(policyUIHelper.getCurrentPolicyTransaction().getPolicyDocuments());
	}
	
	protected DocumentForm createDocumentForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		DocumentForm form = new DocumentForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<DocumentForm> createDocumentDialog(String title, String action) {
		DSUpdateDialog<DocumentForm> dialog = (DSUpdateDialog<DocumentForm>) createDialog(title, createDocumentForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupDocumentAction(String title, String action, PolicyDocuments document) {
		DSUpdateDialog<DocumentForm> dialog = createDocumentDialog(title, action);
		dialog.getContentComponent().setSaveButton(dialog.getButton(UIConstants.UIACTION_Save));
		dialog.setDataStore(new DSDialogDataStore(document));
		dialog.getContentComponent().loadData(document);
		dialog.getContentComponent().setBusinessStatus(getBusinessStatus());
		dialog.getContentComponent().setTxnBusinessStatus(getTxnBusinessStatus());
		dialog.getContentComponent().setPolicyTxnType(policyUIHelper.getCurrentPolicyTransaction().getPolicyVersion().getPolicyTxnType());
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public String getBusinessStatus() {
		return policyUIHelper.getCurrentPolicyTransaction().getPolicyVersion().getBusinessStatus();
	}
	
	public String getTxnBusinessStatus() {
		return policyUIHelper.getCurrentPolicyTransaction().getBusinessStatus();
	}
	
	public void newDocumentAction() {
		PolicyDocuments document = new PolicyDocuments();
		String title = uiHelper.getSessionLabels().getString("DocumentNewDialogTitle");
		setupDocumentAction(title, UIConstants.UIACTION_New, document);
	}
	
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<DocumentForm> dialog = (DSUpdateDialog<DocumentForm>) event.getSource();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				PolicyDocuments document = new PolicyDocuments();
				if (!dialog.getContentComponent().saveEdit(document)) {
					return;
				}
				
				MemoryBuffer memoryBuffer = null;
				if (document.getDocumentId().equalsIgnoreCase("UP")) {
					memoryBuffer = dialog.getContentComponent().getMemoryBuffer();
					if (memoryBuffer.getFileData() == null) {
						uiHelper.notificationError(
								uiHelper.getSessionLabels().getString("DocumentUploadFieldEmptyError"));
						return;
					}
				} else if (document.getDocumentId().equalsIgnoreCase("AF")) {
					SpecialityAutoSubPolicy<Risk> subPolicyAuto = (SpecialityAutoSubPolicy) policyUIHelper
							.getCurrentPolicyTransaction()
							.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
					if (!subPolicyAuto.getFleetBasis().equalsIgnoreCase("SCHD")) {
						uiHelper.notificationError(
								uiHelper.getSessionLabels().getString("DocumentAutoFleetScheduleNonScheduledError"));
						return;
					}
				}
				
				createDocumentTaskProcessor(dialog).start(createDocumentProcessor(UIConstants.UIACTION_SaveNew,
						policyUIHelper.getCurrentPolicyTransaction(), document, memoryBuffer));
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
		}
	}
	
	public void updateDocumentAction(PolicyDocuments document) {
		String title = uiHelper.getSessionLabels().getString("DocumentUpdateDialogTitle");
		setupDocumentAction(title, UIConstants.UIACTION_Update, document);
	}
	
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<DocumentForm> dialog = (DSUpdateDialog<DocumentForm>) event.getSource();
		PolicyDocuments document = (PolicyDocuments) dialog.getDataStore().getData1();
		document.setUpdatedDate(LocalDateTime.now());
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(document)) {
			createDocumentTaskProcessor(dialog)
			.start(createDocumentProcessor(UIConstants.UIACTION_Save, policyUIHelper.getCurrentPolicyTransaction(), document));
		}
	}
	
	public void deleteDocumentAction(PolicyDocuments document) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("DocumentDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(document));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		PolicyDocuments document = (PolicyDocuments) dialog.getDataStore().getData1();
		createDocumentTaskProcessor(dialog)
				.start(createDocumentProcessor(UIConstants.UIACTION_Delete, policyUIHelper.getCurrentPolicyTransaction(),
						document));
	}
	
	protected DSConfirmDialog createConfirmGenerateDialog(String title) {
		DSConfirmDialog dialog = new DSConfirmDialog(
						"", 
						uiHelper.getButtonLabel(UIConstants.UIACTION_Generate),
						this::onGenerate,
						uiHelper.getButtonLabel(UIConstants.UIACTION_Cancel),
						title);
		return dialog;
	}
	
	public void generateDocumentAction(PolicyDocuments document) {
		DSConfirmDialog dialog = createConfirmGenerateDialog(uiHelper.getSessionLabels().getString("DocumentGenerateDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(document));
		dialog.open();
	}
	
	protected void onGenerate(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		
		try {
			PolicyDocuments document =(PolicyDocuments) dialog.getDataStore().getData1();
			if (document.getDocumentId().equalsIgnoreCase("QL") || document.getDocumentId().equalsIgnoreCase("PP")
					|| document.getDocumentId().equalsIgnoreCase("AF")
					|| document.getDocumentId().equalsIgnoreCase("PC")
					|| document.getDocumentId().equalsIgnoreCase("CA")
					|| document.getDocumentId().equalsIgnoreCase("EP")
					|| document.getDocumentId().equalsIgnoreCase("ADJ")) {
				createDocumentTaskProcessor(dialog).start(createDocumentProcessor(UIConstants.UIACTION_Generate,
						policyUIHelper.getCurrentPolicyTransaction(), document));
			} else {
				uiHelper.notificationError(
						uiHelper.getSessionLabels().getString("DocumentGenerateDocumentNotValidError"));
			}
		} catch (Exception e) {
			e.printStackTrace();
			uiHelper.notificationException(e);
			return;
		}
	}
	
	public StreamResource getOpenDocumentStreamResource(PolicyDocuments document) {
		String attachPath = Configurations.getInstance().getProperty("AttachPath");
		File file = new File(attachPath + File.separator + document.getStorageKey());
		StreamResource streamResource = null;
		String ext = FilenameUtils.getExtension(file.getName());
		String fileName = file.getName();
		InsurancePolicy insurancePolicy = document.getPolicyTransaction().getPolicyVersion().getInsurancePolicy();

		if (document.getDocumentId().equalsIgnoreCase("QL")) {
			fileName = "QuoteLetter_" + insurancePolicy.getQuoteNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("PP")) {
			fileName = "PolicyPackage_" + insurancePolicy.getBasePolicyNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("AF")) {
			fileName = "AutoFleetSchedule_" + insurancePolicy.getQuoteNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("UP")) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy");
			fileName = "doc_" + document.getCreatedDate().format(formatter) + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("PC")) {
			fileName = "PolicyChangeAuto_" + insurancePolicy.getBasePolicyNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("CA")) {
			fileName = "PolicyCancel_" + insurancePolicy.getBasePolicyNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("EP")) {
			fileName = "PolicyExtension_" + insurancePolicy.getBasePolicyNum() + "." + ext;
		} else if (document.getDocumentId().equalsIgnoreCase("ADJ")) {
			fileName = "PolicyAdjustments_" + insurancePolicy.getBasePolicyNum() + "." + ext;
		}

		if (file.exists()) {
			streamResource = getStreamResource(fileName, file);
		} else {
			uiHelper.notificationError(uiHelper.getSessionLabels().getString("DocumentOpenDocumentNotFoundError"));
		}

		return streamResource;
	}

	public StreamResource getStreamResource(String filename, File content) {
		return new StreamResource(filename, () -> {
			try {
				return new ByteArrayInputStream(FileUtils.readFileToByteArray(content));
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		});
	}
	
	private void reopen(PolicyTransaction<?> policyTransaction, PolicyDocuments document, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Documents, document);
	}
	
	protected DocumentProcessor createDocumentProcessor(String action, PolicyTransaction policyTransaction,
			PolicyDocuments policyDocuments) {
		return new DocumentProcessor(action,  
										policyTransaction, 
										policyDocuments);
	}
	
	protected DocumentProcessor createDocumentProcessor(String action, PolicyTransaction policyTransaction,
			PolicyDocuments policyDocuments, MemoryBuffer memoryBuffer) {
		return new DocumentProcessor(action, policyTransaction, policyDocuments, memoryBuffer);
	}

	protected TaskProcessor createDocumentTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					DocumentProcessor processor = (DocumentProcessor) taskRun.getProcessor();

					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_SaveNew:
					case UIConstants.UIACTION_Save:
					case UIConstants.UIACTION_Generate:
						dialog.close();
						if (processor.getPolicyTransaction().getPolicyMessages() != null
								&& !processor.getPolicyTransaction().getPolicyMessages().isEmpty()) {
							policyUIHelper.reopenPolicy(processor.getPolicyTransaction(),
									UIConstants.POLICYTREENODES.Messages, null);
						} else {
							reopen(processor.getPolicyTransaction(), processor.getPolicyDocuments(),
									processor.getProcessAction());
						}
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), null, UIConstants.UIACTION_Delete);
						break;

					default:
						break;
					}
				}

				return true;
			}
		};
	}

	// ESL-258
	public boolean isDocumentEditable(PolicyDocuments document) {
		final String DOCUMENTID_UPLOAD = "UP";
		
		boolean result = false;
		
		if (document != null) {
			result = true;
			
			// This is applicable only for document id = Uploaded.
			// allow the document to be updated/deleted ONLY by the user who uploaded the doc ONLY on that particular day.
			// comparing date without time
			LocalDateTime createDate = null;
			if (document.getCreatedDate() != null) {
				LocalTime createTime = LocalTime.of(0, 0, 0, 0);
				createDate = LocalDateTime.of(document.getCreatedDate().toLocalDate(), createTime);
			}
			if (DOCUMENTID_UPLOAD.equalsIgnoreCase(document.getDocumentId()) &&
				(!uiHelper.isBelongToUser(document.getCreatedBy()) ||
				 uiHelper.isAfter24Hours(createDate))) {
				result = false;
			}
		}
		
		return result;
	}

}
