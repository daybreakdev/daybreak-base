package com.echelon.ui.helpers.entities;

import java.io.Serializable;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.entities.CustomerForm;
import com.echelon.ui.components.entities.CustomerView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.process.entities.CustomerProcessor;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.HasCustomer;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.StatCodes;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.PersonalCustomerException;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.utils.Constants;
import com.vaadin.flow.component.UI;

public abstract class CustomerUIHelper<T extends HasCustomer> extends ContentUIHelper implements Serializable {
	private static final long serialVersionUID = -6668571921269982326L;

	public abstract Customer createEmptyCustomer() throws CommercialCustomerException, PersonalCustomerException;

	public String[] getStateAndTerritoryCodesByPostalCode(String postalCode) {
		String stateCode = null;
		String territoryCode = null;
		String ruralityInd = null;

		if (StringUtils.isNotBlank(postalCode)) {
			try {
				StatCodes codes = policyUIHelper.getPolicyProcessing().findStatCodesByPostalCode(postalCode);
				if (codes != null) {
					stateCode = codes.getStatCode();
					territoryCode = codes.getTerritoryCode();
					ruralityInd = codes.getRuralityInd();
				}
			} catch (InsurancePolicyException e) {
				// TODO Auto-generated catch block
			}
		}

		return new String[] { stateCode, territoryCode, ruralityInd };
	}

	public Address createEmptyAddress() {
		final Address address = new Address();
		address.setCountry(Constants.COUNTRY_CANADA);
		address.setProvState(Constants.PROVINCE_ONTARIO);
		return address;
	}

	public CustomerView<T> loadView(CustomerView<T> customerView) {
		if (customerView == null) {
			customerView = createCustomerView(UIConstants.UIACTION_View, UICONTAINERTYPES.Inplace);
		}

		final PolicyTransaction<?> policyTransaction = policyUIHelper.getCurrentPolicyTransaction();
		if (policyTransaction != null) {
			final InsurancePolicy insurancePolicy = policyTransaction.getPolicyVersion().getInsurancePolicy();
			final Customer customer = insurancePolicy.getPolicyCustomer();
			final Address address = Optional.ofNullable(policyUIHelper.getCurrentInsurerLocation())
					.map(PolicyLocation::getLocationAddress).orElse(insurancePolicy.getInsuredAddress());

			customerView.loadData(customer, address, policyTransaction);
		}

		return customerView;
	}

	public boolean isEntityModified() {
		final boolean result = Optional.ofNullable(policyUIHelper.getInsuredLocation())
				.map(PolicyLocation::getLocationAddress).map(Address::isModified).orElse(false);
		return result;
	}
	
	public abstract CustomerView<T> createCustomerView(String action, UICONTAINERTYPES uiContainerType);

	public abstract CustomerForm<T> createCustomerForm(String action, UICONTAINERTYPES uiContainerType);

	public void updateCustomerViewAction() {
		final PolicyTransaction<?> policyTransaction = policyUIHelper.getCurrentPolicyTransaction();
		final InsurancePolicy insurancePolicy = policyTransaction.getPolicyVersion().getInsurancePolicy();
		final Customer customer = insurancePolicy.getPolicyCustomer();
		final Address address = Optional.ofNullable(policyUIHelper.getCurrentInsurerLocation())
				.map(PolicyLocation::getLocationAddress).orElse(insurancePolicy.getInsuredAddress());

		updateCustomerAction(customer, address, policyTransaction);
	}

	protected DSUpdateDialog<CustomerForm<T>> createCustomerDialog(String title, String action) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<CustomerForm<T>> dialog = (DSUpdateDialog<CustomerForm<T>>) createDialog(title,
				createCustomerForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<CustomerForm<T>> dialog = event.getSource();
		if (dialog.getContentComponent().validateEdit()) {
			dialog.setProcessStarted(); // Disable save & cancel buttons
			confirmUpdateCustomerAddress(dialog);
		}
	}

	protected void confirmUpdateCustomerAddress(DSUpdateDialog<CustomerForm<T>> dialog) {
		final DSDialogDataStore dataStore = dialog.getDataStore();
		final Customer customer = (Customer) dataStore.getData1();
		final Address address = (Address) dataStore.getData2();
		final PolicyTransaction<?> policyTransaction = (PolicyTransaction<?>) dataStore.getData3();
		if (dialog.getContentComponent().saveEdit(customer, address, policyTransaction)) {
			createCustomerTaskProcessor(dialog)
					.start(createCustomerProcessor(UIConstants.UIACTION_Save, policyTransaction));
		}
	}

	private void updateCustomerAction(Customer customer, Address address, PolicyTransaction<?> policyTransaction) {
		final String title = uiHelper.getSessionLabels().getString("CustomerUpdateDialogTitle");
		final DSUpdateDialog<CustomerForm<T>> dialog = createCustomerDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(customer, address, policyTransaction));

		final String productCode = policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd();
		final CustomerForm<T> form = dialog.getContentComponent();
		form.loadData(customer, address, policyTransaction, productCode);
		form.enableEdit();
		dialog.open();
	}

	private CustomerProcessor createCustomerProcessor(String action, PolicyTransaction<?> policyTransaction) {
		return new CustomerProcessor(action, policyTransaction);
	}

	@SuppressWarnings("serial")
	private TaskProcessor createCustomerTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					CustomerProcessor processor = (CustomerProcessor) taskRun.getProcessor();
					dialog.close();
					reopen(processor.getPolicyTransaction());
				}

				return true;
			}
		};
	}

	private void reopen(PolicyTransaction<?> policyTransaction) {
		policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Policy, null);
	}
}
