package com.echelon.ui.helpers.policy;

import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyCoverageUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;

public class PolicyCoverageUIHelper extends BasePolicyCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4948532834144130677L;
	
	public PolicyCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.Policy, POLICYTREENODES.Policy);
		// TODO Auto-generated constructor stub
	}
}
