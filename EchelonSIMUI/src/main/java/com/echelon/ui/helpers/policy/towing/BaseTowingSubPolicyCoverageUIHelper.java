package com.echelon.ui.helpers.policy.towing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseSubPolicyCoverageUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageTabs;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.towing.SubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.dto.LocCovProcessDto;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.process.towing.LocCovProcessor;
import com.echelon.ui.session.CoverageUIFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;

public class BaseTowingSubPolicyCoverageUIHelper<MFORM extends SubPolicyLocCovMultiForm> extends BaseSubPolicyCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6608844763365145702L;

	public BaseTowingSubPolicyCoverageUIHelper(OBJECTTYPES parentObjectType, POLICYTREENODES policyTreeNode) {
		super(parentObjectType, policyTreeNode);
		// TODO Auto-generated constructor stub
	}
	
	protected BaseCoverageTabs createLocCovMultiForm(CoverageDto coverageDto, String action, UIConstants.UICONTAINERTYPES containerType) {
		BaseCoverageTabs uiForm = new BaseCoverageTabs(parentObjectType, coverageDto.getCoverageType(), coverageDto.getCoverageCode(), coverageDto.isAllowReinsurance(), action, UICONTAINERTYPES.Dialog);
		return uiForm;
	}
	
	protected DSUpdateDialog<BaseCoverageTabs> createLocCovDialog(CoverageDto coverageDto, String title, String action) {
		DSUpdateDialog<BaseCoverageTabs> dialog = (DSUpdateDialog<BaseCoverageTabs>) createDialogWithActions(title,
																	action,
																	createLocCovMultiForm(coverageDto, action, UICONTAINERTYPES.Dialog),
																	UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_ManualCancel);
		dialog.getContentComponent().setParentDialog(dialog);
		dialog.setWidth("1300px");
		
		return dialog;
	}

	protected DSUpdateDialog<?> openLocCovDialog(String title, String action, ICoverageConfigParent covConfigParent,
													IBusinessEntity covParent, CoverageDto coverageDto, boolean isMultiple, 
													CoverageEntryCallback callback,
													List<CoverageDto> newCoverages) {
		DSUpdateDialog<?> dialog = createLocCovDialog(coverageDto, title, action);
		dialog.setDataStore(createDialogDataStore(covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages));
		
		SubPolicyLocCovMultiForm uiForm = null;
		if (dialog.getContentComponent() instanceof BaseCoverageTabs) {
			BaseCoverageTabs coverageTabs = (BaseCoverageTabs)dialog.getContentComponent();
			uiForm = (SubPolicyLocCovMultiForm) coverageTabs.getCovAndEndorseForm();
			
			coverageTabs.loadData(coverageDto);
		}
		else {
			uiForm = (SubPolicyLocCovMultiForm) dialog.getContentComponent();
			uiForm.loadData(coverageDto);
		}
		
		if (!UIConstants.UIACTION_View.equals(action)) {
			uiForm.enableEdit(coverageDto);
			uiForm.setFocus();
		}
		
		dialog.open();
		return dialog;
	}
	
	@Override
	protected DSUpdateDialog<?> setupCovCoverageAction(String title, String action, ICoverageConfigParent covConfigParent,
														IBusinessEntity covParent, CoverageDto coverageDto, boolean isMultiple, 
														CoverageEntryCallback callback,
														List<CoverageDto> newCoverages) {
		if (CoverageUIFactory.getInstance().getLocationCodes(parentObjectType).contains(coverageDto.getCoverageCode())) {
			return openLocCovDialog(title, action, covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages);
		}
		else {
			return super.setupCovCoverageAction(title, action, covConfigParent, covParent, coverageDto, isMultiple, callback, newCoverages);
		}
	}

	// Save sync with setupSaveAction
	@Override
	protected void onAction(DSUpdateDialog.ActionEvent event) {
		Component uiForm = event.getSource().getContentComponent();
		if (uiForm instanceof BaseCoverageTabs) {
			uiForm = (Component) ((BaseCoverageTabs)uiForm).getCovAndEndorseForm();
		}
		
		if (uiForm instanceof SubPolicyLocCovMultiForm) {
			DSUpdateDialog<?> 	  dialog 			= event.getSource();
			ICoverageConfigParent covConfigParent   = (ICoverageConfigParent) dialog.getDataStore().getData1();
			SubPolicy<?>		  parentSubPolicy	= (SubPolicy<?>) dialog.getDataStore().getData2();
			CoverageDto 		  coverageDto		= (CoverageDto) dialog.getDataStore().getData3();
			boolean 			  isMultiple 		= dialog.getDataStore().getData4() != null && (Boolean)dialog.getDataStore().getData4();
			CoverageEntryCallback callback  		= (CoverageEntryCallback) dialog.getDataStore().getData5();
			List<CoverageDto>   coverageDtoList 	= (List<CoverageDto>) dialog.getDataStore().getData6();
			
			SubPolicyLocCovMultiForm locCovMultiForm = (SubPolicyLocCovMultiForm) uiForm;
			
			// For validation 
			if (isMultiple) {
				coverageDto.setCoverageAdditionalForm(locCovMultiForm);
			}
	
			if (UIConstants.UIACTION_Add.equals(event.getAction())) {
				if (locCovMultiForm.validateEdit()) {
					locCovMultiForm.addAction(true);
				}
			}
			else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
				String saveAction = null;
				if (coverageDto.getCoveragePK() == null) {
					saveAction = UIConstants.UIACTION_SaveNew;
				}
				else {
					saveAction = UIConstants.UIACTION_Save;
				}
				
				resetEditValid(Arrays.asList(coverageDto));
				
				preValidAndSaveCoverage(parentSubPolicy, locCovMultiForm, coverageDto, saveAction, coverageDtoList);
				
				if (validAndSaveCoverage(parentSubPolicy, locCovMultiForm, coverageDto, coverageDtoList, saveAction)) {
					if (setupLocCovProcess(parentSubPolicy, locCovMultiForm, coverageDto, saveAction)) {
						if (UIConstants.UIACTION_SaveNew.equals(saveAction)) {
							// Save handled by ProcessMultipleNewCoverages 
							if (isMultiple) {
								dialog.close();
		
								if (callback != null) {
									callback.saveEvent(coverageDto);
								}
								
								return;
							}
						}
						
						runSave(covConfigParent, parentSubPolicy, coverageDto, dialog, saveAction);
						if (callback != null) {
							callback.saveEvent(coverageDto);
						}
					}
				}
				
				showEditMessages(Arrays.asList(coverageDto));
			}
			else if (UIConstants.UIACTION_ManualCancel.equals(event.getAction())) {
				if (locCovMultiForm.isDataInstanceModified()) {
					IBusinessEntity covParent = (IBusinessEntity) dialog.getDataStore().getData2();
					runReOpen(dialog, covParent);
					return;
				}
				
				DSUpdateDialog.CancelEvent cancelEvent = new DSUpdateDialog.CancelEvent(event.getSource(), true);
				onCancel(cancelEvent);
			}
		}
		else {
			super.onAction(event);
		}
	}

	protected boolean setupLocCovProcess(IBusinessEntity covParent, SubPolicyLocCovMultiForm locCovMultiForm, CoverageDto coverageDto, String action) {
		if (locCovMultiForm != null) {
			if (!locCovMultiForm.validateEdit() || !locCovMultiForm.saveEdit()) {
				return false;
			}

			LocCovProcessDto locCovProcessDto = new LocCovProcessDto(locCovMultiForm.getAllData(),
																			locCovMultiForm.getDeletedData(),
																			locCovMultiForm.getUndeletedData());
			coverageDto.setLocCovProcessDto(locCovProcessDto);
		}
		return true;
	}
	
	// Use different coverage processor to handle SP64/MTCC
	@Override
	protected boolean preValidAndSaveCoverage(IBusinessEntity covParent, IBaseCovAndEndorseForm form, CoverageDto coverageDto, 
												String action,
												List<CoverageDto> newCoverages) {
		if (CoverageUIFactory.getInstance().getLocationCodes(parentObjectType).contains(coverageDto.getCoverageCode()) ) {
			if (!setupLocCovProcess(covParent, (SubPolicyLocCovMultiForm)form, coverageDto, action)) {
				return false;
			}
			
			return true;
		}
		
		return super.preValidAndSaveCoverage(covParent, form, coverageDto, action, newCoverages);
	}
	
	public List<LocCoverageDto> buildLocCoverageList(String localCoverageType, String locCoverageCode) {
		List<LocCoverageDto> dtos = ((LocCovProcessor)(new TaskProcessor(UI.getCurrent())).processRun(
											createLocCovActionProcessor(UIConstants.UIACTION_List, localCoverageType, locCoverageCode)
										)).getLocCoverageDtoList();
		
		return dtos;
	}
	
	public LocCoverageDto createNewLocCoverageDto(String localCoverageType, String locCoverageCode) {
		LocCoverageDto dto = ((LocCovProcessor)(new TaskProcessor(UI.getCurrent())).processRun(
									createLocCovActionProcessor(UIConstants.UIACTION_Add, localCoverageType, locCoverageCode)
								)).getNewLocCoverageDto();
		return dto;
	}
	
	protected LocCovProcessor createLocCovActionProcessor(String processAction, String localCoverageType, String locCoverageCode) {
		LocCovProcessor locCovProcessor = new LocCovProcessor(processAction, policyUIHelper.getCurrentPolicyTransaction(), 
																	coverageConfigUIHelper, configUIHelper.getCurrentProductConfig(), parentObjectType,
																	localCoverageType, locCoverageCode);
		return locCovProcessor;
	}

	protected TaskProcessor createLocCovTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					if (dialog != null) {
						dialog.close();
					}
					
					LocCovProcessor processor = (LocCovProcessor) taskRun.getProcessor();
					switch (processor.getProcessAction()) {
					default:
						reopenCoverageList(processor.getPolicyTransaction(),
								processor.getCovParent(), 
								processor.getCoverageDto(), 
								UIConstants.UIACTION_Save);					
						break;
					}
				}
				
				return true;
			}
		};
	}
	
	@Override
	public List<TotalPremiumDto> createTotalPremiumDtos(IBusinessEntity covParent) {
		SubPolicy<?> subPolicy = (SubPolicy<?>) covParent;
		List<TotalPremiumDto> dtos = null;
		
		if (SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			dtos = super.createSingleTotalPremium(covParent, subPolicy.getSubPolicyPremium(), subPolicy.getCoverageDetail());
		}
		else {
			dtos = super.createBreakDownTotalPremiums(covParent, subPolicy.getSubPolicyPremium(), subPolicy.getCoverageDetail());
		}
		
		if (dtos != null && !dtos.isEmpty()) {
			dtos.removeIf(o -> TOTALKEY_GRAND.equalsIgnoreCase(o.getKey()));
		}
		
		return dtos;
	}
	
	public List<LookupTableItem> getAvailableLocations(String mainCoverageCode, PolicyLocation include, 
														List<LocCoverageDto> allDtos) {
		List<LookupTableItem> locLookupItems = new ArrayList<LookupTableItem>();
		
		LocationUIHelper locationUIHelper = new LocationUIHelper();
		if (include != null && (include.isDeleted() || include.wasDeleted())) {
			LookupTableItem locLookupTableItem = locationUIHelper.buildLocationLookupItem(include);
			if (locLookupTableItem != null) {
				locLookupItems.add(locLookupTableItem);
			}
		}
		else {
			List<LookupTableItem> availableLocations = locationUIHelper.getLocationLookupItems(true);
			if (availableLocations != null && !availableLocations.isEmpty()) {
				locLookupItems.addAll(availableLocations);
			}
		}
		
		/* do not do filter; add validation
		if (DSUIConfigConstants.COVERAGE_CM512.equalsIgnoreCase(mainCoverageCode) ||
			DSUIConfigConstants.COVERAGE_SP513.equalsIgnoreCase(mainCoverageCode) ||
			DSUIConfigConstants.COVERAGE_SP514.equalsIgnoreCase(mainCoverageCode)) {
				
		}
		*/
		
		if (allDtos != null && !allDtos.isEmpty()) {
			List<PolicyLocation> selected = new ArrayList<PolicyLocation>();
			allDtos.stream().filter(o -> !o.wasDeletedCoverage()).collect(Collectors.toList())
					.stream().forEach(o -> {
				if (o.getPolicyLocation() != null &&
					(include == null || !include.equals(o.getPolicyLocation()))) {
					selected.add(o.getPolicyLocation());
				}
			});
			locLookupItems.removeIf(o -> selected.contains(o.getItemObject()));
		}

		return locLookupItems;
	}

}
