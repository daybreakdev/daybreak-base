package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.ui.components.towing.GarageSubPolicyDialog;
import com.echelon.ui.components.towing.GarageSubPolicyView;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.process.towing.GarageSubPolicyProcessor;

public class GarageSubPolicyUIHelper extends BaseSpecialtySubPolicyContentUIHelper<GarageSubPolicy<?>, GarageSubPolicyProcessor> 
									   implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127595188814038214L;
	
	public GarageSubPolicyUIHelper() {
		super(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE, UIConstants.POLICYTREENODES.GarageSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	public void loadView(GarageSubPolicyView component, GarageSubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}

	@Override
	protected void newSubPolicyAction(GarageSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("GarageSubPolicyDialogTitle");
		GarageSubPolicyDialog dialog = new GarageSubPolicyDialog(title, UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_New);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	@Override
	protected void updateSubPolicyAction(GarageSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("GarageSubPolicyDialogTitle");
		GarageSubPolicyDialog dialog = new GarageSubPolicyDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	public void deleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("GarageSubPolicyDeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmDeleteDialog(title);
		dialog.open();
	}

	public void undeleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("GarageSubPolicyUndeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmUndeleteDialog(title);
		dialog.open();
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		GarageSubPolicyDialog dialog = (GarageSubPolicyDialog) event.getSource();
		GarageSubPolicy<?> subPolicy   = (GarageSubPolicy<?>) dialog.getDataStore().getData1();
		if (dialog.validate()) {
			try {
				if (!dialog.save(subPolicy)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createSubPolicyTaskProcessor(dialog).start(
					createSubPolicyProcessor(UIConstants.UIACTION_SaveNew, subPolicy)
				);
		}
	}

	@Override
	protected void onProcessSaveNewComplete(IDSDialog dialog, GarageSubPolicyProcessor processor) {
		super.onProcessSaveNewComplete(dialog, processor);
		
		uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnNewPolicyGarageDetails"));
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		GarageSubPolicyDialog dialog = (GarageSubPolicyDialog) event.getSource();
		GarageSubPolicy<?> subPolicy   = (GarageSubPolicy<?>) dialog.getDataStore().getData1();
		if (dialog.validate() && dialog.save(subPolicy)) {
			createSubPolicyTaskProcessor(dialog).start(
					createSubPolicyProcessor(UIConstants.UIACTION_Save, subPolicy)
				);
		}
	}

	@Override
	protected GarageSubPolicyProcessor createSubPolicyProcessor(String action, GarageSubPolicy<?> subPolicy) {
		return new GarageSubPolicyProcessor(action, policyUIHelper.getCurrentPolicyTransaction(), subPolicy);
	}

	@Override
	protected GarageSubPolicyProcessor createSubPolicyProcessor(String action) {
		return new GarageSubPolicyProcessor(action,  
								policyUIHelper.getCurrentPolicyTransaction(), 
								SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	}
}
