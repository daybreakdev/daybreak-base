package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.DriverConvictionForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.DriverConvictionProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverConviction;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class DriverConvictionUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7263100447433664725L;

	public DriverConvictionUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DriverConvictionForm createConvictionForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		DriverConvictionForm form = null;

		form = new DriverConvictionForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<DriverConvictionForm> createConvictionDialog(String title, String action) {
		DSUpdateDialog<DriverConvictionForm> dialog = (DSUpdateDialog<DriverConvictionForm>) createDialog(title, createConvictionForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupConvictionAction(String title, String action, Driver driver, DriverConviction conviction) {
		DSUpdateDialog<DriverConvictionForm> dialog = createConvictionDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(driver, conviction));
		dialog.getContentComponent().loadData(driver, conviction);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newConvictionAction(Driver driver) {
		createConvictionTaskProcessor(null).start(
				createConvictionProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null, driver)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<DriverConvictionForm> dialog = (DSUpdateDialog<DriverConvictionForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverConviction conviction = (DriverConviction) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(conviction)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createConvictionTaskProcessor(dialog).start(
					createConvictionProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									conviction, driver)
					);
		}
	}

	public void updateConvictionAction(Driver driver, DriverConviction conviction) {
		String title = uiHelper.getSessionLabels().getString("DriverConvictionUpdateDialogTitle");
		setupConvictionAction(title, UIConstants.UIACTION_Update, driver, conviction);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<DriverConvictionForm> dialog = (DSUpdateDialog<DriverConvictionForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverConviction conviction = (DriverConviction) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(conviction)) {

			createConvictionTaskProcessor(dialog).start(
					createConvictionProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											conviction, driver));
		}
	}
	
	public void deleteConvictionAction(Driver driver, DriverConviction conviction) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("DriverConvictionDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, conviction));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverConviction conviction = (DriverConviction) dialog.getDataStore().getData2();
		createConvictionTaskProcessor(dialog).start(
				createConvictionProcessor(UIConstants.UIACTION_Delete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							conviction, driver)
				);
	}
	
	public void undeleteConvictionAction(Driver driver, DriverConviction conviction) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("DriverConvictionUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, conviction));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverConviction conviction = (DriverConviction) dialog.getDataStore().getData2();
		createConvictionTaskProcessor(dialog).start(
				createConvictionProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							conviction, driver)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Driver driver, DriverConviction conviction, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Driver, driver);
	}

	public List<DriverConviction> sortConvictionList(Set<DriverConviction> convictions) {
		List<DriverConviction> result = new ArrayList<DriverConviction>(); 
		if (convictions != null) {
			result.addAll(convictions);
		}
		
		return result;
	}
	
	protected DriverConvictionProcessor createConvictionProcessor(String action, PolicyTransaction policyTransaction, 
													DriverConviction conviction, Driver driver) {
		return new DriverConvictionProcessor(action, policyTransaction).withParams(driver, conviction);
	}
	
	protected TaskProcessor createConvictionTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					DriverConvictionProcessor processor = (DriverConvictionProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupConvictionAction(uiHelper.getSessionLabels().getString("DriverConvictionNewDialogTitle"), 
												UIConstants.UIACTION_New, processor.getDriver(), processor.getDriverConviction());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverConviction(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverConviction(), UIConstants.UIACTION_Delete);
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverConviction(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}
}
