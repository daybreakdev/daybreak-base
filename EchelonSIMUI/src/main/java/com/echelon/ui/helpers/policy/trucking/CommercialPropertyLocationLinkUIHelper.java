package com.echelon.ui.helpers.policy.trucking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.echelon.domain.policy.specialtylines.CommercialPropertyLocation;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.LocationPicker;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.policy.towing.LocationUIHelper;
import com.echelon.ui.process.trucking.CommercialPropertyLocationLinkProcessor;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.Unit;

public class CommercialPropertyLocationLinkUIHelper extends ContentUIHelper
	implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5296974777888848247L;

	protected LocationPicker createLocationPicker(String action,
		UICONTAINERTYPES containerType) {
		return new LocationPicker(action, containerType);
	}

	protected DSUpdateDialog<LocationPicker> createLocationPickerDialog() {
		DSUpdateDialog<LocationPicker> dialog = (DSUpdateDialog<LocationPicker>) createDialog(
			uiHelper.getSessionLabels().getString("LocationLinkSelectionTitle"),
			createLocationPicker(UIConstants.UIACTION_Select,
				UICONTAINERTYPES.Dialog),
			UIConstants.UIACTION_New);
		dialog.setWidth(900, Unit.PIXELS);
		dialog.setHeight(750, Unit.PIXELS);

		return dialog;
	}

	public void setupSelectionAction(Object parentObject,
		List<CommercialPropertyLocation> locationList) {
		List<PolicyLocation> selectedLocations = new ArrayList<>();
		if (locationList != null && !locationList.isEmpty()) {
			selectedLocations.addAll(locationList.stream()
				.map(o -> o.getPolicyLocation()).collect(Collectors.toList()));
		}

		DSUpdateDialog<LocationPicker> dialog = createLocationPickerDialog();
		dialog.setDataStore(new DSDialogDataStore(parentObject));
		dialog.getContentComponent().loadData(selectedLocations);
		dialog.open();
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<LocationPicker> dialog = event.getSource();
		Object parentObject = dialog.getDataStore().getData1();

		if (dialog.getContentComponent().validateSelection()) {
			createLocationLinkTaskProcessor(dialog)
				.start(createSaveNewLocationLinkProcessor(parentObject,
					dialog.getContentComponent().getSelected()));
		}
	}

	public void setupDeleteAction(Object parentObject,
		CommercialPropertyLocation cpLocation) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper
			.getSessionLabels().getString("LocationLinkDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(parentObject, cpLocation));
		dialog.open();
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		Object parentObject = dialog.getDataStore().getData1();
		CommercialPropertyLocation cpLocation = (CommercialPropertyLocation) dialog
			.getDataStore().getData2();

		createLocationLinkTaskProcessor(dialog)
			.start(createDeleteLocationLinkProcessor(parentObject, cpLocation));
	}

	protected CommercialPropertyLocationLinkProcessor createSaveNewLocationLinkProcessor(
		Object parentObject, List<PolicyLocation> selectedLocations) {
		return new CommercialPropertyLocationLinkProcessor(
			UIConstants.UIACTION_SaveNew,
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(),
			parentObject, selectedLocations);
	}

	protected CommercialPropertyLocationLinkProcessor createDeleteLocationLinkProcessor(
		Object parentObject, CommercialPropertyLocation cpLocation) {
		return new CommercialPropertyLocationLinkProcessor(
			UIConstants.UIACTION_Delete,
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(),
			parentObject, cpLocation);
	}

	protected TaskProcessor createLocationLinkTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun,
				VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					dialog.close();

					CommercialPropertyLocationLinkProcessor processor = (CommercialPropertyLocationLinkProcessor) taskRun
						.getProcessor();
					reopen(processor.getPolicyTransaction(),
						processor.getParentObject());
				}

				return true;
			}
		};
	}

	protected void reopen(PolicyTransaction<?> policyTransaction,
		Object parentObject) {
		if (parentObject != null) {
			if (parentObject instanceof CommercialPropertySubPolicy<?>) {
				policyUIHelper.reopenPolicy(policyTransaction,
					POLICYTREENODES.CommercialPropertySubPolicyNode,
					parentObject);
				return;
			}
		}

		policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Policy,
			null);
	}

	public DSLookupItemRenderer<CommercialPropertyLocation> getRenderer(
		LookupUIHelper helper) {
		return new DSLookupItemRenderer<>(
			loc -> loc.getPolicyLocation().getLocationAddress().getProvState(),
			helper.getLookupConfigLookup(
				ConfigConstants.LOOKUPTABLE_Jurisdictions));
	}

	public List<PolicyLocation> getLocationLookupItems() {
		List<LookupTableItem> items = (new LocationUIHelper())
			.getLocationLookupItems(true);
		if (items != null && !items.isEmpty()) {
			return items.stream().map(o -> (PolicyLocation) o.getItemObject())
				.collect(Collectors.toList());
		}

		return new ArrayList<>();
	}
}
