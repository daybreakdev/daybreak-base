package com.echelon.ui.helpers.policy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.ds.ins.uicommon.converters.DSLocalDateFormat;

public abstract class BaseSpecialtyPolicyContentUIHelper extends ContentUIHelper {
	
	public BaseSpecialtyPolicyContentUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected ISpecialtyAutoPolicyProcessing getSpecialtyAutoPolicyProcessing() {
		return (ISpecialtyAutoPolicyProcessing) policyUIHelper.getPolicyProcessing();
	}
	
	public List<SubPolicy<?>> getSortedSubPolicies(PolicyTransaction<?> policyTransaction, String subpolicyCode) {
		List<SubPolicy<?>> subPolicies = new ArrayList<SubPolicy<?>>();
		
		if (subpolicyCode != null && policyTransaction != null) {
			List<SubPolicy<Risk>> list = ((PolicyTransaction<Risk>) policyTransaction).findSubPoliciesByType(subpolicyCode);
			if (list != null && !list.isEmpty()) {
				DSLocalDateFormat dateFormat = new DSLocalDateFormat();
				
				subPolicies = list.stream().sorted(Comparator.comparing(SubPolicy::getEffectiveDate)).collect(Collectors.toList());
			}
		}
		
		return subPolicies;
	}
}
