package com.echelon.ui.helpers.policy.trucking;

import java.util.List;

import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.domain.policy.specialtylines.VehicleRateGroupLHT;
import com.echelon.ui.components.SIMMoneyField;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;
import com.vaadin.flow.component.AbstractField;

public class LHTVehicleUIHelper extends VehicleUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5979759150500509664L;

	public LHTVehicleUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void setVehicleRateGroupFields(SIMMoneyField actualCashValueField,
			AbstractField impliedRateGroupField, AbstractField selectedRateGroupField) {
		Integer vehRateGroup = getImpliedRateGroupLHT(actualCashValueField.getModelValue());
		updateVehicleRateGroupFields(impliedRateGroupField, selectedRateGroupField, vehRateGroup);
	}

	protected Integer getImpliedRateGroupLHT(Double actualCashValue) {
		if (actualCashValue != null) {
			try {
				List<VehicleRateGroupLHT> list = getSpecialtyAutoPolicyProcessing().findVehicleRateGroupLHT(actualCashValue, actualCashValue);
				if (list != null && !list.isEmpty()) {
					return list.get(0).getImpliedRateGroup();
				}
			} catch (SpecialtyAutoPolicyException e) {
				// TODO Auto-generated catch block
				uiHelper.notificationException(e);
			}
		}
		
		return null;
	}

}
