package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.LocationPicker;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.process.towing.LocationLinkProcessor;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;

public class LocationLinkUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -575946044497191669L;

	public LocationLinkUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected LocationPicker createLocationPicker(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new LocationPicker(action, containerType);
	}
	
	protected DSUpdateDialog<LocationPicker> createLocationPickerDialog() {
		DSUpdateDialog<LocationPicker> dialog = (DSUpdateDialog<LocationPicker>) createDialog(uiHelper.getSessionLabels().getString("LocationLinkSelectionTitle"), 
														createLocationPicker(UIConstants.UIACTION_Select, UIConstants.UICONTAINERTYPES.Dialog), 
														UIConstants.UIACTION_New);
		dialog.setWidth("900px");
		dialog.setHeight("750px");
		return dialog;
	}

	public void setupSelectionAction(Object parentObject, List<CGLLocation> locationList) {
		List<PolicyLocation> selectedLocations = new ArrayList<PolicyLocation>();
		if (locationList != null && !locationList.isEmpty()) {
			selectedLocations.addAll(locationList.stream().map(o -> o.getPolicyLocation()).collect(Collectors.toList()));
		}
		
		DSUpdateDialog<LocationPicker> dialog = createLocationPickerDialog();
		dialog.setDataStore(new DSDialogDataStore(parentObject));
		dialog.getContentComponent().loadData(selectedLocations);
		dialog.open();
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<LocationPicker> dialog = (DSUpdateDialog<LocationPicker>) event.getSource();
		Object parentObject	= dialog.getDataStore().getData1();
		
		if (dialog.getContentComponent().validateSelection()) {
			createLocationLinkTaskProcessor(dialog).start(
					createSaveNewLocationLinkProcessor(parentObject, dialog.getContentComponent().getSelected())
					);
		}
	}
	
	public void setupDeleteAction(Object parentObject, CGLLocation cglLocation) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("LocationLinkDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(parentObject, cglLocation));
		dialog.open();
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog  = event.getSource();
		Object parentObject		= dialog.getDataStore().getData1();
		CGLLocation cglLocation	= (CGLLocation) dialog.getDataStore().getData2();
		
		createLocationLinkTaskProcessor(dialog).start(
				createDeleteLocationLinkProcessor(parentObject, cglLocation)
				);
	}
	
	protected LocationLinkProcessor createSaveNewLocationLinkProcessor(Object parentObject, List<PolicyLocation> selectedLocations) {
		return new LocationLinkProcessor(UIConstants.UIACTION_SaveNew, 
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(), 
											parentObject, selectedLocations);
		
	}
	
	protected LocationLinkProcessor createDeleteLocationLinkProcessor(Object parentObject, CGLLocation cglLocation) {
		return new LocationLinkProcessor(UIConstants.UIACTION_Delete, 
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction(), 
											parentObject, cglLocation);
		
	}
	
	protected TaskProcessor createLocationLinkTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					dialog.close();
					
					LocationLinkProcessor processor = (LocationLinkProcessor) taskRun.getProcessor();
					reopen(processor.getPolicyTransaction(), processor.getParentObject());
				}
				
				return true;
			}
			
		};
	}
	
	protected void reopen(PolicyTransaction<?> policyTransaction, Object parentObject) {
		if (parentObject != null) {
			if (parentObject instanceof CGLSubPolicy<?>) {
				policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.CGLSubPolicyNode, parentObject);
				return;
			}
		}
		
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Policy, null);
	}

	public DSLookupItemRenderer<CGLLocation> getRenderer(LookupUIHelper helper) {
		return new DSLookupItemRenderer<CGLLocation>(loc -> {
			return loc.getPolicyLocation().getLocationAddress().getProvState();
		}, helper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_Jurisdictions));
	}

	public List<PolicyLocation> getLocationLookupItems() {
		List<LookupTableItem> items = (new LocationUIHelper()).getLocationLookupItems(true);
		if (items != null && !items.isEmpty()) {
			return items.stream().map(o -> (PolicyLocation)o.getItemObject()).collect(Collectors.toList());
		}
		
		return new ArrayList<PolicyLocation>();
	}
}
