package com.echelon.ui.helpers.policy;

import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BaseSubPolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.components.IDSDialog;
import com.vaadin.flow.component.UI;

public abstract class BaseSpecialtySubPolicyContentUIHelper<MODEL extends SubPolicy<?>, PROCESSOR extends BaseSubPolicyProcessor<MODEL>> 
															 extends BaseSpecialtyPolicyContentUIHelper {
	protected String subPolicyTypeCd;	
	protected UIConstants.POLICYTREENODES treeNode;

	public interface LoadSubPolicyDataCallback{
				public void loadComplete();
			};

	public BaseSpecialtySubPolicyContentUIHelper(String subPolicyTypeCd, POLICYTREENODES treeNode) {
		super();
		this.subPolicyTypeCd = subPolicyTypeCd;
		this.treeNode = treeNode;
	}

	protected void reopen(PolicyTransaction policyTransaction, SubPolicy<?> subPolicy) {
		try {
			policyUIHelper.reopenPolicy(policyTransaction, treeNode, subPolicy);
		} catch (Exception e) {
			uiHelper.notificationException(e);
			return;
		}
	}

	public void newSubPolicyAction() {
		createSubPolicyTaskProcessor(null).start(
				createSubPolicyProcessor(UIConstants.UIACTION_New, null)
			);
	}

	protected void newSubPolicyAction(MODEL subPolicy) {
		
	}

	public void updateSubPolicyAction() {
		createSubPolicyTaskProcessor(null).start(
				createSubPolicyProcessor(UIConstants.UIACTION_Update)
			);
	}

	protected void updateSubPolicyAction(MODEL subPolicy) {
		
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		createSubPolicyTaskProcessor(dialog).start(
				createSubPolicyProcessor(UIConstants.UIACTION_Delete)
			);
	}

	@Override
	protected void onUndelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		createSubPolicyTaskProcessor(dialog).start(
				createSubPolicyProcessor(UIConstants.UIACTION_Undelete)
			);
	}

	protected PROCESSOR createSubPolicyProcessor(String action, MODEL subPolicy) {
		return null;
	}

	protected PROCESSOR createSubPolicyProcessor(String action) {
		return null;
	}

	protected TaskProcessor createSubPolicyTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					PROCESSOR processor = (PROCESSOR)taskRun.getProcessor();
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_Update:
						onProcessUpdateComplete(dialog, processor);
						break;
						
					case UIConstants.UIACTION_New:
						onProcessNewComplete(dialog, processor);
						break;

					case UIConstants.UIACTION_SaveNew:
						onProcessSaveNewComplete(dialog, processor);
						break;

					case UIConstants.UIACTION_Save:
						onProcessSaveComplete(dialog, processor);
						break;

					case UIConstants.UIACTION_Delete:
						onProcessDeleteComplete(dialog, processor);
						break;

					case UIConstants.UIACTION_Undelete:
						onProcessUndeleteComplete(dialog, processor);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
			
		};
	}
	
	protected void onProcessUpdateComplete(IDSDialog dialog, PROCESSOR processor) {
		updateSubPolicyAction(processor.getSubPolicy());
	}
	
	protected void onProcessNewComplete(IDSDialog dialog, PROCESSOR processor) {
		newSubPolicyAction(processor.getSubPolicy());
	}
	
	protected void onProcessSaveNewComplete(IDSDialog dialog, PROCESSOR processor) {
		dialog.close();
		reopen(processor.getPolicyTransaction(), processor.getSubPolicy());
	}
	
	protected void onProcessSaveComplete(IDSDialog dialog, PROCESSOR processor) {
		dialog.close();
		reopen(processor.getPolicyTransaction(), processor.getSubPolicy());
	}
	
	protected void onProcessDeleteComplete(IDSDialog dialog, PROCESSOR processor) {
		dialog.close();
		reopen(processor.getPolicyTransaction(), processor.getSubPolicy());
	}
	
	protected void onProcessUndeleteComplete(IDSDialog dialog, PROCESSOR processor) {
		dialog.close();
		reopen(processor.getPolicyTransaction(), processor.getSubPolicy());
	}

	public void loadSubpolicyData(LoadSubPolicyDataCallback callback,
									MODEL subpolicy) {
		if (policyUIHelper.needToLoadSubPolicyData(subpolicy)) {
			(new TaskProcessor(UI.getCurrent(), null) {
	
				@Override
				protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
					if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
						PROCESSOR processor = (PROCESSOR) taskRun.getProcessor();
						if (callback != null) {
							callback.loadComplete();
						}
					}
					
					return true;
				}
					
			})
			.start(
					createSubPolicyProcessor(UIConstants.UIACTION_LoadData, subpolicy)
				);
		}
		else {
			callback.loadComplete();
		}
	}

	public boolean checkIfChildDataIsChangedInCurrentVersion(DSTreeGridItem treeGridItem) {
		return false;
	}
}
