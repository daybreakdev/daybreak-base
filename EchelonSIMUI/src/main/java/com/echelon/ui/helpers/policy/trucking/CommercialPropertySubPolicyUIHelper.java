package com.echelon.ui.helpers.policy.trucking;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.ui.components.trucking.CommercialPropertyLocationLinkList;
import com.echelon.ui.components.trucking.CommercialPropertySubPolicyDialog;
import com.echelon.ui.components.trucking.CommercialPropertySubPolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.process.trucking.CommercialPropertySubPolicyProcessor;
import com.echelon.utils.SpecialtyAutoConstants;

public class CommercialPropertySubPolicyUIHelper extends
	BaseSpecialtySubPolicyContentUIHelper<CommercialPropertySubPolicy<?>, CommercialPropertySubPolicyProcessor>
	implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5921794063990392050L;

	public CommercialPropertySubPolicyUIHelper() {
		super(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY,
			UIConstants.POLICYTREENODES.CommercialPropertySubPolicyNode);
	}

	public void loadView(CommercialPropertySubPolicyView component,
		CommercialPropertySubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}

	public void loadView(CommercialPropertyLocationLinkList component,
		CommercialPropertySubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}

	@Override
	protected void newSubPolicyAction(
		CommercialPropertySubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels()
			.getString("CommercialPropertySubPolicyDialogTitle");
		CommercialPropertySubPolicyDialog dialog = new CommercialPropertySubPolicyDialog(
			title, UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_New);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	@Override
	protected void updateSubPolicyAction(
		CommercialPropertySubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels()
			.getString("CommercialPropertySubPolicyDialogTitle");
		CommercialPropertySubPolicyDialog dialog = new CommercialPropertySubPolicyDialog(
			title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	public void deleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels()
			.getString("CommercialPropertySubPolicyDeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmDeleteDialog(title);
		dialog.open();
	}

	public void undeleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels()
			.getString("CommercialPropertySubPolicyUndeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmUndeleteDialog(title);
		dialog.open();
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		CommercialPropertySubPolicyDialog dialog = (CommercialPropertySubPolicyDialog) event.getSource();
		CommercialPropertySubPolicy<?> subPolicy = (CommercialPropertySubPolicy<?>) dialog.getDataStore()
			.getData1();

		if (dialog.validateEdit()) {
			try {
				if (!dialog.saveEdit(subPolicy)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}

			createSubPolicyTaskProcessor(dialog).start(createSubPolicyProcessor(
				UIConstants.UIACTION_SaveNew, subPolicy));
		}
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		try {
			CommercialPropertySubPolicyDialog dialog = (CommercialPropertySubPolicyDialog) event
				.getSource();
			CommercialPropertySubPolicy<?> subPolicy = (CommercialPropertySubPolicy<?>) dialog.getDataStore()
				.getData1();
			CommercialPropertySubPolicy<?> prevSubPolicy = (CommercialPropertySubPolicy<?>) subPolicy.clone();

			if (dialog.validateEdit() && dialog.saveEdit(subPolicy)) {
				createSubPolicyTaskProcessor(dialog)
					.start(new CommercialPropertySubPolicyProcessor(
						UIConstants.UIACTION_Save,
						policyUIHelper.getCurrentPolicyTransaction(), subPolicy,
						prevSubPolicy));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected CommercialPropertySubPolicyProcessor createSubPolicyProcessor(
		String action, CommercialPropertySubPolicy<?> subPolicy) {
		return new CommercialPropertySubPolicyProcessor(action,
			policyUIHelper.getCurrentPolicyTransaction(), subPolicy);
	}

	@Override
	protected CommercialPropertySubPolicyProcessor createSubPolicyProcessor(
		String action) {
		return new CommercialPropertySubPolicyProcessor(action,
			policyUIHelper.getCurrentPolicyTransaction(),
			SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
	}
}
