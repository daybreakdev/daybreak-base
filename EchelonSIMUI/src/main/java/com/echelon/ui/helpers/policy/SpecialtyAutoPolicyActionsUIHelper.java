package com.echelon.ui.helpers.policy;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyActionsUIHelper;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyActionsProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.process.policy.SpecialtyAutoPolicyProcessor;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;

public class SpecialtyAutoPolicyActionsUIHelper extends BasePolicyActionsUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1695896820974023392L;

	public SpecialtyAutoPolicyActionsUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void onActionsTaskCompleteSucceed(IDSDialog dialog, BasePolicyActionsProcessor processor) {
		// TODO Auto-generated method stub
		super.onActionsTaskCompleteSucceed(dialog, processor);
		
		if (ConfigConstants.LIFECYCLEACTION_Renewal.equals(processor.getConfirmDto().getPolicyAction())) {
			if (processor.getPolicyTransaction() != null &&
				(processor.getPolicyTransaction().getPolicyVersion().getProducerCommissionRate() == null ||
				 processor.getPolicyTransaction().getPolicyVersion().getProducerCommissionRate().doubleValue() != 10d)) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnCommissionUpdateIfRequired"));
			}
		}
	}

	@Override
	protected boolean preExtension() {
		boolean ok = super.preExtension();
		if (ok) {
			if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				ChronoUnit.DAYS.between(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getVersionTerm().getTermExpDate(), LocalDateTime.of(LocalDate.now(), LocalTime.of(0,0,0))) > 15) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnNewPolicyExtension15DaysPastExpiry"), new DSNotificatonUIHelper.NotificatonCallback() {
					
					@Override
					public void close() {
						openConfirmDialog(ConfigConstants.LIFECYCLEACTION_Extension, null);
					}
				});
				
				ok = false; // not to continue
			}
		}
		
		return ok;
	}
	
	
	@Override
	protected List<String> getActionWarnMessages(String action) throws Exception {
		SpecialtyAutoPolicyProcessor processor = UIFactory.getInstance().getPolicyActionsProcessor(UIConstants.UIACTION_IssuePolicyWarn, policyUIHelper.getCurrentPolicyTransaction())
															.withParam(action);
		(new TaskProcessor()).manualStart(processor);
		List<String> warns = processor.getWarnings();
		
		List<String> results = new ArrayList<String>();
		if (warns != null && !warns.isEmpty()) {
			warns.stream().forEach(o -> {
				String msg = o;
				try {
					msg = uiHelper.getSessionLabels().getString(o); 
				} catch (Exception e) {
				}
				
				results.add(msg);
			});
		}
		
		return results;
	}
}
