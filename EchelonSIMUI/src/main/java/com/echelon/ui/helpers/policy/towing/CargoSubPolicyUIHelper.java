package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.towing.CargoSubPolicyDialog;
import com.echelon.ui.components.towing.CargoSubPolicyView;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.process.towing.CargoSubPolicyProcessor;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.uicommon.components.DSConfirmDialog;

public class CargoSubPolicyUIHelper extends BaseSpecialtySubPolicyContentUIHelper<CargoSubPolicy<?>, CargoSubPolicyProcessor> 
									  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127595188814038214L;

	public CargoSubPolicyUIHelper() {
		super(SpecialtyAutoConstants.SUBPCY_CODE_CARGO, UIConstants.POLICYTREENODES.CargoSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	public void loadView(CargoSubPolicyView component, CargoSubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}
	
	@Override
	protected void newSubPolicyAction(CargoSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyDialogTitle");
		CargoSubPolicyDialog dialog = new CargoSubPolicyDialog(title, UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_New);
		dialog.loadData(subPolicy);
		dialog.open();
	}
	
	@Override
	protected void updateSubPolicyAction(CargoSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyDialogTitle");
		CargoSubPolicyDialog dialog = new CargoSubPolicyDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.loadData(subPolicy);
		dialog.open();
	}
	
	public void deleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyDeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmDeleteDialog(title);
		dialog.open();
	}
	
	public void undeleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyUndeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmUndeleteDialog(title);
		dialog.open();
	}


	@Override
	protected void onSaveNew(SaveEvent event) {
		CargoSubPolicyDialog dialog = (CargoSubPolicyDialog) event.getSource();
		CargoSubPolicy<?> subPolicy   = (CargoSubPolicy<?>) dialog.getDataStore().getData1();
		if (dialog.validate()) {
			try {
				if (!dialog.save(subPolicy)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createSubPolicyTaskProcessor(dialog).start(
					createSubPolicyProcessor(UIConstants.UIACTION_SaveNew, subPolicy)
				);
		}
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		try {
			CargoSubPolicyDialog dialog   = (CargoSubPolicyDialog) event.getSource();
			CargoSubPolicy<?> subPolicy     = (CargoSubPolicy<?>) dialog.getDataStore().getData1();
			CargoSubPolicy<?> prevSubPolicy = (CargoSubPolicy<?>) subPolicy.clone();
			
			if (dialog.validate() && dialog.save(subPolicy)) {
				createSubPolicyTaskProcessor(dialog).start(
						new CargoSubPolicyProcessor(UIConstants.UIACTION_Save,  
								policyUIHelper.getCurrentPolicyTransaction(), subPolicy, prevSubPolicy)
						);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected CargoSubPolicyProcessor createSubPolicyProcessor(String action,
							CargoSubPolicy<?> subPolicy) {
		return new CargoSubPolicyProcessor(action,  
								policyUIHelper.getCurrentPolicyTransaction(), subPolicy);
	}

	@Override
	protected CargoSubPolicyProcessor createSubPolicyProcessor(String action) {
		return new CargoSubPolicyProcessor(action,  
								policyUIHelper.getCurrentPolicyTransaction(), 
								SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public boolean checkIfChildDataIsChangedInCurrentVersion(DSTreeGridItem treeGridItem) {
		boolean result = false;
		
		CargoSubPolicy<Risk> cargoSubPolicy = (CargoSubPolicy<Risk>) treeGridItem.getNodeObject();
		if (cargoSubPolicy.getCargoDetails() != null && !cargoSubPolicy.getCargoDetails().isEmpty()) {
			for(CargoDetails cargoDetails : cargoSubPolicy.getCargoDetails()) {
				if (BooleanUtils.isTrue(policyUIHelper.isEntityChangedInCurrentVersion(cargoDetails))) {
					result = true;
					break;
				}
			}
		}
		
		return result;
	}
}
