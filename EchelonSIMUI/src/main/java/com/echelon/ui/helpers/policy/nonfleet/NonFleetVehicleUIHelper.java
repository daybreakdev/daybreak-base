package com.echelon.ui.helpers.policy.nonfleet;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.Risk;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.towing.VehicleForm;
import com.echelon.ui.helpers.policy.towing.VehicleUIHelper;

public class NonFleetVehicleUIHelper extends VehicleUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5625435316139897765L;

	public NonFleetVehicleUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSUpdateDialog<VehicleForm> createVehicleDialog(String title, String action) {
		// TODO Auto-generated method stub
		DSUpdateDialog<VehicleForm> dialog = super.createVehicleDialog(title, action);
		dialog.setWidth("1300px");
		return dialog;
	}

	public List<SpecialtyVehicleRisk> getAvailableTowingVehicles(SpecialityAutoSubPolicy<Risk> autoSp) {
		List<SpecialtyVehicleRisk> results = new ArrayList<SpecialtyVehicleRisk>();
		
		if (autoSp != null && autoSp.getSubPolicyRisks() != null &&
				!autoSp.getSubPolicyRisks().isEmpty()) {
			results = autoSp.getSubPolicyRisks().stream()
					.filter(o -> o.isPending() || o.isActive())
					.map(o -> (SpecialtyVehicleRisk)o)
					.filter(o -> !isTrailer(o.getVehicleDescription()))
					.collect(Collectors.toList());
		}
		
		return results;
	}
	
	public List<LookupTableItem> getAvailableTowingVehicleLookups(SpecialityAutoSubPolicy<Risk> autoSp) {
		List<LookupTableItem> results = new ArrayList<LookupTableItem>();
		
		List<SpecialtyVehicleRisk> vehilces = getAvailableTowingVehicles(autoSp);
		if (vehilces != null && !vehilces.isEmpty()) {
			for(SpecialtyVehicleRisk veh : vehilces) {
				results.add(new LookupTableItem(
									veh.getPolicyRiskPK().toString().replace(",", ""), 
									veh.getDisplayString()));
			}
		}
		
		return results;
	}
}
