package com.echelon.ui.helpers.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.ds.ins.utils.Constants;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.ActionEvent;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.domain.dto.LienholderLessorSearchResults;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.entities.LienholderLessorForm;
import com.echelon.ui.components.entities.LienholderLessorSearch;
import com.echelon.ui.components.entities.LienholderLessorSubPolicyView;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.process.entities.LienholderLessorProcessor;
import com.vaadin.flow.component.UI;

public class LienholderLessorUIHelper<MODEL, PARENTMODEL> extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5990801900774744612L;
	
	public interface SearchLienholderLessorCallback {
		public void resutIsReady(List<LienholderLessorSearchResults> results);
	}
	
	public interface ValidateLienholderLessorCallback {
		public void complete(boolean isValid);
	}
	
	public void loadView(LienholderLessorSubPolicyView lienLessorView, SpecialityAutoSubPolicy<Risk> subPolicy) {
		lienLessorView.loadData(subPolicy);
	}
	
	protected MODEL createEmptyLienLessorHolder(LienholderLessor lienLessorHolder) {
		return null;
	}
	
	protected LienholderLessor createEmptyLienLessor() {
		LienholderLessor lienLessor = new LienholderLessor();
		lienLessor.setLienLessorType(UIConstants.LIENLESSORTYPE_DEFAULT);
		lienLessor.getLienholderLessorAddress().setCountry(Constants.COUNTRY_CANADA);
		lienLessor.getLienholderLessorAddress().setProvState(Constants.PROVINCE_ONTARIO);
		return lienLessor;
	}
	
	protected void formLoadData(LienholderLessorForm form, MODEL lienLessorHolder) {
		
	}
	
	protected boolean formSaveEdit(LienholderLessorForm form, MODEL lienLessorHolder) {
		return false;
	}

	protected LienholderLessorSearch createLienLessorSearch(UIConstants.UICONTAINERTYPES containerType) {
		LienholderLessorSearch form = new LienholderLessorSearch(UIConstants.UIACTION_Search, containerType);
		return form;
	}
	
	protected DSUpdateDialog<LienholderLessorSearch> createLienLessorSearchDialog(String title, String action) {
		DSUpdateDialog<LienholderLessorSearch> dialog = (DSUpdateDialog<LienholderLessorSearch>) createDialogWithActions(title,
																	action,
																	createLienLessorSearch(UICONTAINERTYPES.Dialog), 
																	UIConstants.UIACTION_New, UIConstants.UIACTION_Select, UIConstants.UIACTION_Cancel);
		dialog.setHeight("650px");
		return dialog;
	}

	protected LienholderLessorForm createLienLessorForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		LienholderLessorForm form = new LienholderLessorForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<LienholderLessorForm> createLienLessorDialog(String title, String action) {
		DSUpdateDialog<LienholderLessorForm> dialog = (DSUpdateDialog<LienholderLessorForm>) createDialog(title, 
																	createLienLessorForm(action, UICONTAINERTYPES.Dialog), 
																	action);
		return dialog;
	}
	
	public boolean selectAction(PARENTMODEL lienLessorParent) {
		DSUpdateDialog<LienholderLessorSearch> dialog = createLienLessorSearchDialog(
															uiHelper.getSessionLabels().getString("LienLessorSearchTitle"), 
															UIConstants.UIACTION_Select);
		dialog.setDataStore(new DSDialogDataStore(lienLessorParent));
		dialog.open();
		
		return true;
	}
	
	public boolean newAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		setupLienLessorAction(uiHelper.getSessionLabels().getString("LienLessorAddActionLabel"), 
								UIConstants.UIACTION_New, 
								lienLessorParent,
								lienLessorHolder);
		return true;
	}
	
	public boolean updateAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		setupLienLessorAction(uiHelper.getSessionLabels().getString("LienLessorUpdateActionLabel"), 
								UIConstants.UIACTION_Update, 
								lienLessorParent,
								lienLessorHolder);
		return true;
	}
	
	public boolean viewAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		setupLienLessorAction(uiHelper.getSessionLabels().getString("LienLessorViewActionLabel"), 
								UIConstants.UIACTION_View, 
								lienLessorParent,
								lienLessorHolder);
		return true;
	}
	
	protected void setupLienLessorAction(String title, String action, 
											PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		DSUpdateDialog<LienholderLessorForm> dialog = createLienLessorDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(lienLessorParent, lienLessorHolder));
		formLoadData(dialog.getContentComponent(), lienLessorHolder);
		if (!UIConstants.UIACTION_View.equals(action)) {
			dialog.getContentComponent().enableEdit();
		}
		dialog.open();
	}
	
	public boolean deleteAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("LienLessorDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(lienLessorParent, lienLessorHolder));
		dialog.open();
		
		return true;
	}
	
	public boolean undeleteAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("LienLessorUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(lienLessorParent, lienLessorHolder));
		dialog.open();
		
		return true;
	}
	
	public boolean vehDetailsAction(PARENTMODEL lienLessorParent, MODEL lienLessorHolder) {
		return true;
	}
	
	@Override
	protected void onAction(ActionEvent event) {
		DSUpdateDialog<LienholderLessorSearch> dialog = (DSUpdateDialog<LienholderLessorSearch>) event.getSource();
		PARENTMODEL lienLessorParent = (PARENTMODEL) dialog.getDataStore().getData1();
		if (UIConstants.UIACTION_New.equals(event.getAction())) {
			dialog.close();
			newAction(lienLessorParent, createEmptyLienLessorHolder(createEmptyLienLessor()));
		}
		else if (UIConstants.UIACTION_Select.equals(event.getAction())) {
			LienholderLessorSearchResults searchResult = dialog.getContentComponent().getSelected(); 
			if (searchResult == null) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("LienLessorSearchNoSelectionError"));
				return;
			}
			
			boolean found = false;
			Set<LienholderLessorSubPolicy> list = getLienholderLessors(lienLessorParent);
			if (list != null && !list.isEmpty()) {
				found = list.stream().filter(o -> 
										o.getLienholderLessor().getLienholderLessorPK().longValue() == searchResult.getId().longValue())
									 .findFirst().orElse(null) != null;
			}

			if (found) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("LienLessorSearchExistSelectionError"));
				return;				
			}
			
			createLienholderLessorTaskProcessor(dialog).start(
					createSelectLienholderLessorProcessor(lienLessorParent, searchResult.getId())
					);
		}
	}
	
	protected Set<LienholderLessorSubPolicy> getLienholderLessors(PARENTMODEL lienLessorParent) {
		return null;
	}
	
	protected MODEL createEmptyLienholderLessor() {
		return null;
	}
	
	protected LienholderLessor getLienholderLessor(MODEL lienLessorHolder) {
		return null;
	}
	
	protected void validateLienholderLessorUnique(PARENTMODEL lienLessorParent, 
														MODEL lienLessorHolder,
														LienholderLessorForm form,
														ValidateLienholderLessorCallback validateCallback) {
		MODEL work = createEmptyLienLessorHolder(createEmptyLienLessor());
		formSaveEdit(form, work);
		
		String addressLine1		= getLienholderLessor(work).getLienholderLessorAddress().getAddressLine1();
		String addressCity		= getLienholderLessor(work).getLienholderLessorAddress().getCity();
		String addressStateProv	= getLienholderLessor(work).getLienholderLessorAddress().getProvState();
		String addressPostalZip	= getLienholderLessor(work).getLienholderLessorAddress().getPostalZip();
		String companyName		= getLienholderLessor(work).getCompanyName();
		
		if (companyName != null && !companyName.isEmpty()) {
			SearchLienholderLessorCallback searchCallback = new SearchLienholderLessorCallback() {
				
				@Override
				public void resutIsReady(List<LienholderLessorSearchResults> results) {
					LienholderLessorSearchResults anyResult = results.stream().filter(o ->
						// Skip the one for update
					    (lienLessorHolder == null || 
					     getLienholderLessor(lienLessorHolder).getLienholderLessorPK() == null ||
					     getLienholderLessor(lienLessorHolder).getLienholderLessorPK().longValue() != o.getId().longValue())
					    &&
						uiHelper.equalsIgnoreCase(addressLine1, o.getAddressLine1()) 
						&& 
						uiHelper.equalsIgnoreCase(addressStateProv, o.getAddressStateProv())
					).findFirst().orElse(null);
					
					// Valid = unique company name and address
					validateCallback.complete((anyResult == null));
				}
			};
			
			searchLienholderLessor(companyName, addressCity, addressPostalZip, searchCallback);
		}
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<LienholderLessorForm> dialog = (DSUpdateDialog<LienholderLessorForm>) event.getSource();
		PARENTMODEL lienLessorParent = (PARENTMODEL) dialog.getDataStore().getData1();
		MODEL lienLessorHolder = (MODEL) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			ValidateLienholderLessorCallback validateCallback = new ValidateLienholderLessorCallback() {
				
				@Override
				public void complete(boolean isValid) {
					if (isValid) {
						boolean saved = formSaveEdit(dialog.getContentComponent(), lienLessorHolder);
						if (saved) {
							createLienholderLessorTaskProcessor(dialog).start(
										createLienholderLessorProcessor(UIConstants.UIACTION_SaveNew, lienLessorParent, lienLessorHolder)
									);
						}
					}
					else {
						uiHelper.notificationError(uiHelper.getSessionLabels().getString("LienLessorFormSameNameAndAddressError"));
					}
				}
			};
			
			validateLienholderLessorUnique(lienLessorParent, lienLessorHolder, dialog.getContentComponent(), validateCallback);
		}			
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<LienholderLessorForm> dialog = (DSUpdateDialog<LienholderLessorForm>) event.getSource();
		PARENTMODEL lienLessorParent = (PARENTMODEL) dialog.getDataStore().getData1();
		MODEL lienLessorHolder = (MODEL) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			ValidateLienholderLessorCallback validateCallback = new ValidateLienholderLessorCallback() {

				@Override
				public void complete(boolean isValid) {
					if (isValid) {
						boolean saved = formSaveEdit(dialog.getContentComponent(), lienLessorHolder);
						if (saved) {
							createLienholderLessorTaskProcessor(dialog).start(
										createLienholderLessorProcessor(UIConstants.UIACTION_Save, lienLessorParent, lienLessorHolder)
									);
						}
					} else {
						uiHelper.notificationError(uiHelper.getSessionLabels().getString("LienLessorFormSameNameAndAddressError"));
					}
				}
				
			};
			
			validateLienholderLessorUnique(lienLessorParent, lienLessorHolder, dialog.getContentComponent(), validateCallback);
		}			
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		PARENTMODEL lienLessorParent = (PARENTMODEL) dialog.getDataStore().getData1();
		MODEL lienLessorHolder = (MODEL) dialog.getDataStore().getData2();
		
		createLienholderLessorTaskProcessor(dialog).start(
					createLienholderLessorProcessor(UIConstants.UIACTION_Delete, lienLessorParent, lienLessorHolder)
				);
	}
	
	@Override
	protected void onUndelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		PARENTMODEL lienLessorParent = (PARENTMODEL) dialog.getDataStore().getData1();
		MODEL lienLessorHolder = (MODEL) dialog.getDataStore().getData2();
		
		createLienholderLessorTaskProcessor(dialog).start(
					createLienholderLessorProcessor(UIConstants.UIACTION_Undelete, lienLessorParent, lienLessorHolder)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Object lienLessorParent, Object lienLessorHolder, String action) {
		if (lienLessorParent instanceof SpecialityAutoSubPolicy) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.AutomobileSubPolicyNode, lienLessorParent, lienLessorHolder);
		}
		else if (lienLessorParent instanceof SpecialtyVehicleRisk) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Vehicle, lienLessorParent, null);
		}
	}

	protected LienholderLessorProcessor createLienholderLessorProcessor(String action,
												PARENTMODEL lienLessorParent, 
												MODEL lienLessorHolder) {
		return new LienholderLessorProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
												lienLessorParent, lienLessorHolder);
	}

	protected LienholderLessorProcessor createSelectLienholderLessorProcessor(
												PARENTMODEL lienLessorParent, 
												Long lienLessorPK) {
		return new LienholderLessorProcessor(UIConstants.UIACTION_Select, policyUIHelper.getCurrentPolicyTransaction(),
							lienLessorParent, lienLessorPK);
	}
	
	protected TaskProcessor createLienholderLessorTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					LienholderLessorProcessor processor = (LienholderLessorProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getLienLessorParent(), processor.getLienLessorHolder(), UIConstants.UIACTION_Save);

						break;

					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getLienLessorParent(), processor.getLienLessorHolder(), UIConstants.UIACTION_Save);

						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getLienLessorParent(), null, UIConstants.UIACTION_Delete);

						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getLienLessorParent(), null, UIConstants.UIACTION_Undelete);

						break;

					case UIConstants.UIACTION_Select:
						dialog.close();
						newAction((PARENTMODEL)processor.getLienLessorParent(), 
									createEmptyLienLessorHolder(processor.getLienLessor()));
						break;

					default:
						break;
					}
				}
				
				return true;
			}
			
		};
	}
	
	
	private LienholderLessorProcessor createSearchLienholderLessorProcessor(String companyName, String city, String postalZip) {
		return new LienholderLessorProcessor(UIConstants.UIACTION_Search, policyUIHelper.getCurrentPolicyTransaction(),
							companyName, city, postalZip);
	}

	public void searchLienholderLessor(String companyName, String city, String postalZip,
										SearchLienholderLessorCallback callback) {
		(new TaskProcessor(UI.getCurrent()) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					LienholderLessorProcessor processor = (LienholderLessorProcessor) taskRun.getProcessor();
					callback.resutIsReady(processor.getSearchResults());
				}
				else {
					callback.resutIsReady(new ArrayList<LienholderLessorSearchResults>());
				}
				
				return true;
			}
			
		})
		.start(createSearchLienholderLessorProcessor(companyName, city, postalZip));
	}
}
