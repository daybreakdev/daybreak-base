package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.DriverClaimForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.DriverClaimProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverClaim;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class DriverClaimUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5057231508574124219L;

	public DriverClaimUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DriverClaimForm createClaimForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		DriverClaimForm form = null;

		form = new DriverClaimForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<DriverClaimForm> createClaimDialog(String title, String action) {
		DSUpdateDialog<DriverClaimForm> dialog = (DSUpdateDialog<DriverClaimForm>) createDialog(title, createClaimForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupClaimAction(String title, String action, Driver driver, DriverClaim claim) {
		DSUpdateDialog<DriverClaimForm> dialog = createClaimDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(driver, claim));
		dialog.getContentComponent().loadData(driver, claim);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newClaimAction(Driver driver) {
		createClaimTaskProcessor(null).start(
				createClaimProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null, driver)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<DriverClaimForm> dialog = (DSUpdateDialog<DriverClaimForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverClaim claim = (DriverClaim) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(claim)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createClaimTaskProcessor(dialog).start(
					createClaimProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									claim, driver)
					);
		}
	}

	public void updateClaimAction(Driver driver, DriverClaim claim) {
		String title = uiHelper.getSessionLabels().getString("DriverClaimUpdateDialogTitle");
		setupClaimAction(title, UIConstants.UIACTION_Update, driver, claim);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<DriverClaimForm> dialog = (DSUpdateDialog<DriverClaimForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverClaim claim = (DriverClaim) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(claim)) {

			createClaimTaskProcessor(dialog).start(
					createClaimProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											claim, driver));
		}
	}
	
	public void deleteClaimAction(Driver driver, DriverClaim claim) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("DriverClaimDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, claim));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverClaim claim = (DriverClaim) dialog.getDataStore().getData2();
		createClaimTaskProcessor(dialog).start(
				createClaimProcessor(UIConstants.UIACTION_Delete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							claim, driver)
				);
	}
	
	public void undeleteClaimAction(Driver driver, DriverClaim claim) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("DriverClaimUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, claim));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverClaim claim = (DriverClaim) dialog.getDataStore().getData2();
		createClaimTaskProcessor(dialog).start(
				createClaimProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							claim, driver)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Driver driver, DriverClaim claim, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Driver, driver);
	}

	public List<DriverClaim> sortClaimList(Set<DriverClaim> claims) {
		List<DriverClaim> result = new ArrayList<DriverClaim>(); 
		if (claims != null) {
			result.addAll(claims);
		}
		
		return result;
	}
	
	protected DriverClaimProcessor createClaimProcessor(String action, PolicyTransaction policyTransaction, 
													DriverClaim claim, Driver driver) {
		return new DriverClaimProcessor(action, policyTransaction).withParams(driver, claim);
	}
	
	protected TaskProcessor createClaimTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					DriverClaimProcessor processor = (DriverClaimProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupClaimAction(uiHelper.getSessionLabels().getString("DriverClaimNewDialogTitle"), 
												UIConstants.UIACTION_New, processor.getDriver(), processor.getDriverClaim());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverClaim(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverClaim(), UIConstants.UIACTION_Delete);
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverClaim(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}
	
	public String getEchelonClaimStatus(boolean claim)
	{
		 if (claim) {
			 String status = "Y";
			 return status;
		 }
		 else {
			 String status = "N";
			 return status;
		 }
	}
}