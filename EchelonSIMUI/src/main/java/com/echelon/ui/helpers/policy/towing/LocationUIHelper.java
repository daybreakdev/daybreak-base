package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.renderers.DSLookupItemRenderer;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog.ActionEvent;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.LocationDialog;
import com.echelon.ui.components.towing.LocationForm;
import com.echelon.ui.components.towing.LocationList;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.ds.ins.uicommon.converters.DSAddressFormat;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.process.towing.LocationProcessor;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.vaadin.flow.component.UI;

public class LocationUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1104970206673886953L;
	private GarageSubPolicy<?> 		garageSubPolicy;

	@SuppressWarnings("unchecked")
	public void loadView(LocationList component, DSTreeGridItem nodeItem) {
		if (nodeItem == null) {
			return;
		}
		PolicyTransaction<?> policy = (PolicyTransaction<PolicyLocation>) nodeItem.getNodeObject();
		component.loadData(
			(policy.getPolicyRisks().stream().filter(o -> "LO".equalsIgnoreCase(o.getRiskType())).collect(Collectors.toSet()))
		);
		garageSubPolicy = (GarageSubPolicy<?>) policy.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE);
	}

	public void newLocationAction() {
		String title = uiHelper.getSessionLabels().getString("LocationNewDialogTitle");
		LocationDialog dialog = new LocationDialog(title, UIConstants.UIACTION_New, garageSubPolicy);
		initDialogWithActions(dialog);
		dialog.open();
	}

	public void updateLocationAction(PolicyLocation location) {
		String title = uiHelper.getSessionLabels().getString("LocationUpdateDialogTitle");
		LocationDialog dialog = new LocationDialog(title, UIConstants.UIACTION_Update, garageSubPolicy, location);
		dialog.setDataStore(new DSDialogDataStore(location));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.open();
	}

	public void viewLocationAction(PolicyLocation location) {
		String title = uiHelper.getSessionLabels().getString("LocationViewDialogTitle");
		LocationDialog dialog = new LocationDialog(title, UIConstants.UIACTION_View, garageSubPolicy, location);
		dialog.setDataStore(new DSDialogDataStore(location));
		initDialog(dialog, UIConstants.UIACTION_View);
		dialog.open();
	}

	public void deleteLocationAction(PolicyLocation location) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("LocationDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(location));
		dialog.open();
	}

	public void undeleteLocationAction(PolicyLocation location) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("LocationUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(location));
		dialog.open();
	}

	public void copyLocationAction(PolicyLocation location) {
		createTaskProcessor(null).start(
				createLocationProcessor(UIConstants.UIACTION_Copy, location.getPolicyTransaction(), location)
				);
	}

	private void openCopyLocationAction(PolicyLocation location) {
		String title = uiHelper.getSessionLabels().getString("LocationCopyDialogTitle");
		LocationDialog dialog = new LocationDialog(title, UIConstants.UIACTION_Copy, garageSubPolicy, location);
		dialog.setDataStore(new DSDialogDataStore(location));
		initDialog(dialog, UIConstants.UIACTION_Copy);
		dialog.open();
	}

	@Override
	protected void onAction(ActionEvent event) {
		LocationDialog dialog = (LocationDialog) event.getSource();
		if (UIConstants.UIACTION_Add.equals(event.getAction())) {
			dialog.addNewForm(true);
		}
		else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
			if (dialog.getButton(UIConstants.UIACTION_Add) != null) {
				onSaveNew(new SaveEvent(event.getSource(), true));
			}
			else {
				onSaveUpdate(new SaveEvent(event.getSource(), true));
			}
		}
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		LocationDialog dialog = (LocationDialog) event.getSource();
		if (dialog.validate()) {
			/*
			try {
				PolicyTransaction<PolicyLocation> policyTransaction = policyUIHelper.loadCurrentPolicyTransaction();
				List<DSLocationForm> forms = dialog.getMultiForm().getForms();
				for(DSLocationForm form: forms) {
					PolicyLocation policyLocation = createPolicyLocation(policyTransaction);
					if (!dialog.save(policyLocation, form)) {
						return;
					}
				}
				if (!updatePolicyTransaction(policyTransaction)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			reopen(null, action);
			dialog.close();
			*/
			
			// #1 Capture the values - call processor to pre-create a list of locations
			// Save value to locations
			// #2 call another processor to save all locations					
			List<LocationForm> forms = dialog.getMultiForm().getForms();
			if (forms != null && !forms.isEmpty()) {
				createTaskProcessorNew(dialog, forms)
					.startFirst(createNewLocationProcessor(UIConstants.UIACTION_New, policyUIHelper.getCurrentPolicyTransaction(), forms.size()));
			}
		}
	}
	
	@Override
	protected void onSaveCopy(SaveEvent event) {
		LocationDialog dialog = (LocationDialog) event.getSource();
		PolicyLocation location = (PolicyLocation) dialog.getDataStore().getData1();
		if (dialog.validate() && dialog.update(location)) {
			createTaskProcessor(dialog).start(
					createLocationProcessor(UIConstants.UIACTION_SaveNew, location.getPolicyTransaction(), location)
					);
		}
	}

	private LocationProcessor createNewLocationProcessor(String action, PolicyTransaction<?> policyTransaction, Integer count) {
		return new LocationProcessor(action,  
										policyTransaction, 
										count);
	}
	
	private TaskProcessor createTaskProcessorNew(LocationDialog dialog, List<LocationForm> forms) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				LocationProcessor processor = (LocationProcessor)taskRun.getProcessor();
				boolean ok = false;
				if (processor.getPolicyLocations() != null && !processor.getPolicyLocations().isEmpty()) {
					ok = true;
					int ii=0;
					for(LocationForm form: forms) {
						if (!dialog.save(processor.getPolicyLocations().get(ii), form)) {
							ok = false;
							break;
						}
						
						ii++;
					}
				}
				
				if (ok) {
					//#2
					createTaskProcessor(dialog).startLast(
							createLocationProcessor(UIConstants.UIACTION_SaveNew, processor.getPolicyTransaction(), null), // save all
							this.getTaskProcessInfo()
							);
				}
				
				return ok;
			}
		};
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		LocationDialog dialog = (LocationDialog) event.getSource();
		PolicyLocation location = (PolicyLocation) dialog.getDataStore().getData1();
		if (dialog.validate() && dialog.update(location)) {
			/*
			try {
				updatePolicyLocation(location);
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			reopen(null, action);
			dialog.close();
			*/
			createTaskProcessor(dialog).start(
					createLocationProcessor(UIConstants.UIACTION_Save, location.getPolicyTransaction(), location)
					);
		}
	}

	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		
		/* moved
		try {
			deletePolicyLocation((PolicyLocation) dialog.getDataStore().getData1());
		} catch (Exception e) {
			e.printStackTrace();
			uiHelper.notificationException(e);
			return;
		}
		reopen(null, UIConstants.UIACTION_Delete);
		
		dialog.close();
		*/
		
		PolicyLocation policyLocation = (PolicyLocation) dialog.getDataStore().getData1();
		createTaskProcessor(dialog).start(
				createLocationProcessor(UIConstants.UIACTION_Delete, policyLocation.getPolicyTransaction(), policyLocation)
				);
	}

	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		
		PolicyLocation policyLocation = (PolicyLocation) dialog.getDataStore().getData1();
		createTaskProcessor(dialog).start(
				createLocationProcessor(UIConstants.UIACTION_Undelete, policyLocation.getPolicyTransaction(), policyLocation)
				);
	}
	
	private LocationProcessor createLocationProcessor(String action, PolicyTransaction<?> policyTransaction, PolicyLocation policyLocation) {
		return new LocationProcessor(action,  
										policyTransaction, 
										policyLocation);
	}
	
	private TaskProcessor createTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (dialog != null) {
					dialog.close();
				}
				
				LocationProcessor processor = (LocationProcessor)taskRun.getProcessor();
				
				// create the copy 
				if (dialog == null &&
					UIConstants.UIACTION_Copy.equalsIgnoreCase(processor.getProcessAction())) {
					openCopyLocationAction(processor.getPolicyLocation());
				}
				else {
					reopen(processor.getPolicyTransaction(), processor.getPolicyLocation(), processor.getProcessAction());
				}
				
				return true;				
			}
			
		};
	}
	
/* Moved all the codes to DSLocationProcessor
	private PolicyLocation createPolicyLocation(PolicyTransaction<PolicyLocation> policyLocation) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing)(policyUIHelper.getPolicyProcessing())).createPolicyLocation(policyLocation);
	}

	public PolicyLocation updatePolicyLocation(PolicyLocation policyLocation) throws Exception {
		return ((ISpecialtyAutoPolicyProcessing)(policyUIHelper.getPolicyProcessing())).updatePolicyLocation(policyLocation);
	}

	private void deletePolicyLocation(PolicyLocation policyLocation) throws Exception {
		((ISpecialtyAutoPolicyProcessing)(policyUIHelper.getPolicyProcessing())).deletePolicyLocation(policyLocation);
	}
	@SuppressWarnings("unchecked")
	private boolean updatePolicyTransaction(PolicyTransaction<?> policyTransaction) throws InsurancePolicyException {
		(policyUIHelper.getPolicyProcessing()).updatePolicyTransaction(policyTransaction);
		return true;
	}
*/

	private void reopen(PolicyTransaction policyTransaction, PolicyLocation location, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Locations, location);
	}

	public DSLookupItemRenderer<PolicyLocation> getRenderer(LookupUIHelper helper) {
		return new DSLookupItemRenderer<PolicyLocation>(loc -> {
			return loc.getLocationAddress().getProvState();
		}, helper.getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_Jurisdictions));
	}

	public boolean isMainLocation(PolicyLocation policyLocation) {
		if (policyLocation != null) {
		  return policyLocation.getCustomerMainAddressPK() != null;
		}
		return false;
	}

	public List<LookupTableItem> getLocationLookupItems(boolean excludeDeleted) {
		List<LookupTableItem> items = new ArrayList<LookupTableItem>();
		
		if (policyUIHelper.getCurrentPolicyTransaction() != null) {
			List<Risk> locRisks = (policyUIHelper.getCurrentPolicyTransaction().getPolicyRisks()
										.stream().filter(o -> "LO".equalsIgnoreCase(o.getRiskType())).collect(Collectors.toList()));
			if (excludeDeleted) {
				locRisks = locRisks.stream().filter(o -> !o.isDeleted() && !o.wasDeleted()).collect(Collectors.toList());
			}
			for(Risk risk : locRisks) {
				PolicyLocation loc = (PolicyLocation) risk;
				if (!SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(loc.getLocationId())) {
					items.add(buildLocationLookupItem(loc));
				}
			}
		}
		
		
		return items;
	}
	
	public LookupTableItem buildLocationLookupItem(PolicyLocation loc) {
		DSAddressFormat addressFormat = new DSAddressFormat();
		
		String des = uiHelper.getSessionLabels().getString("GarageSubPolicyCoverageLocationSelectionPrefix")
				+" "+loc.getLocationId()
				+" : "+addressFormat.format(loc.getLocationAddress());
	
		LookupTableItem item = new LookupTableItem(loc.getPolicyRiskPK().toString(), des, loc);
		
		return item;
	}
}
