package com.echelon.ui.helpers.entities;

import com.echelon.ui.components.entities.CustomerForm;
import com.echelon.ui.components.entities.CustomerView;
import com.echelon.ui.components.entities.PersonalCustomerForm;
import com.echelon.ui.components.entities.PersonalCustomerView;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.Person;
import com.ds.ins.domain.entities.PersonalCustomer;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.PersonalCustomerException;
import com.ds.ins.processes.customer.PersonalCustomerProcessing;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.utils.Constants;

public class PersonalCustomerUIHelper extends CustomerUIHelper<PersonalCustomer> {
	private static final long serialVersionUID = -1983454942019859337L;

	@Override
	public Customer createEmptyCustomer() throws CommercialCustomerException, PersonalCustomerException {
		final Customer customer = new PersonalCustomerProcessing().createPersonalCustomer();
		customer.setPreferredLanguage(Constants.LANGUAGECODE_ENGLISH);
		return customer;
	}

	@Override
	public CustomerView<PersonalCustomer> createCustomerView(String action, UICONTAINERTYPES uiContainerType) {
		return new PersonalCustomerView(action, uiContainerType);
	}

	@Override
	public CustomerForm<PersonalCustomer> createCustomerForm(String action, UICONTAINERTYPES uiContainerType) {
		return new PersonalCustomerForm(action, uiContainerType);
	}
	
	public String getCustomerName(Person person) {
		String result = person.getFamilyName() + ", " + person.getGivenName();
		return result;
	}
}
