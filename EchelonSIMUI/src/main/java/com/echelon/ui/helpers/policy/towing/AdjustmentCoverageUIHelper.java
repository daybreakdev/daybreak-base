package com.echelon.ui.helpers.policy.towing;

import java.util.List;

import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageUIHelper;
import com.echelon.ui.components.towing.AdjustmentCoverageList;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;

public class AdjustmentCoverageUIHelper extends BaseCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9058868183147568896L;

	public AdjustmentCoverageUIHelper(OBJECTTYPES parentObjectType, POLICYTREENODES policyTreeNode) {
		super(parentObjectType, policyTreeNode);
		// TODO Auto-generated constructor stub
	}

	public void loadView(AdjustmentCoverageList component, SpecialtyVehicleRisk vehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		if (vehicle != null) {
			if (vehicle.getSubPolicy() != null) {
				final RiskConfig riskConfig = configUIHelper.getSubPolicyRiskConfig(vehicle.getSubPolicy().getSubPolicyTypeCd(), vehicle.getRiskType());
				if (riskConfig != null) {
					List<CoverageDto> coverageDtoList = buildFullCoverageList(riskConfig, adjustment);
					component.loadData(riskConfig, adjustment, coverageDtoList);
				}
			}
		}
	}

	public void loadView(AdjustmentCoverageList component, SubPolicy<?> subPolicy, SpecialtyVehicleRiskAdjustment adjustment) {
		if (subPolicy != null) {
			final SubPolicyConfig spConfig = configUIHelper.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
			if (spConfig != null) {
				List<CoverageDto> coverageDtoList = buildFullCoverageList(spConfig, adjustment);
				component.loadData(spConfig, adjustment, coverageDtoList);
			}
		}
	}
}
