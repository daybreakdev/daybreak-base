package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.DriverForm;
import com.echelon.ui.components.policy.wheels.DriverList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.entities.PersonalCustomerUIHelper;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.DriverProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.vaadin.flow.component.UI;

public class DriverUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5559867321173411345L;

	private PersonalCustomerUIHelper	personalCustomerUIHelper;
	
	public DriverUIHelper() {
		super();
		// TODO Auto-generated constructor stub
		
		this.personalCustomerUIHelper = new PersonalCustomerUIHelper();		
	}

	public void loadView(DriverList component, DSTreeGridItem nodeItem) {
		PolicyTransaction<?> policy = (PolicyTransaction<?>) nodeItem.getNodeObject();
		if (policy != null) {
			component.loadData(policy.getDrivers(), null);
		}
		else {
			component.loadData(new HashSet<Driver>(), null);
		}
	}
	
	public void loadView(DriverForm component, Driver driver) {
		component.loadData(driver);
	}
	
	public DriverForm createDriverForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		DriverForm form = null;

		form = new DriverForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<DriverForm> createDriverDialog(String title, String action) {
		DSUpdateDialog<DriverForm> dialog = (DSUpdateDialog<DriverForm>) createDialog(title, createDriverForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupDriverAction(String title, String action, Driver driver) {
		DSUpdateDialog<DriverForm> dialog = createDriverDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(driver));
		dialog.getContentComponent().loadData(driver);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newDriverAction() {
		createDriverTaskProcessor(null).start(
				createDriverProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<DriverForm> dialog = (DSUpdateDialog<DriverForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(driver)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createDriverTaskProcessor(dialog).start(
					createDriverProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									driver)
					);
		}
	}

	public void updateDriverAction(Driver driver) {
		String title = uiHelper.getSessionLabels().getString("DriverUpdateDialogTitle");
		setupDriverAction(title, UIConstants.UIACTION_Update, driver);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<DriverForm> dialog = (DSUpdateDialog<DriverForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(driver)) {

			createDriverTaskProcessor(dialog).start(
					createDriverProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											driver));
		}
	}
	
	public void deleteDriverAction(Driver driver) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("DriverDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		createDriverTaskProcessor(dialog).start(
				createDriverProcessor(UIConstants.UIACTION_Delete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							driver)
				);
	}
	
	public void undeleteDriverAction(Driver driver) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("DriverUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		createDriverTaskProcessor(dialog).start(
				createDriverProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							driver)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Driver driver, String action) {
		if (driver != null) { 
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Driver, driver);
		}
		else {
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.DriverListNode, driver);
		}
	}

	public List<Driver> sortDriverList(Set<Driver> drivers) {
		List<Driver> result = new ArrayList<Driver>(); 
		if (drivers != null) {
			result.addAll(drivers);
		}
		
		return result.stream().sorted(Comparator.comparing(Driver::getDriverSequence, 
										Comparator.nullsLast(Comparator.naturalOrder())))
					 	.collect(Collectors.toList());
	}
	
	protected DriverProcessor createDriverProcessor(String action, PolicyTransaction policyTransaction, 
													Driver driver) {
		return new DriverProcessor(action, policyTransaction, driver);
	}
	
	protected TaskProcessor createDriverTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					DriverProcessor processor = (DriverProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupDriverAction(uiHelper.getSessionLabels().getString("DriverNewDialogTitle"), 
											UIConstants.UIACTION_New, processor.getDriver());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						if (processor.getDriver() != null && processor.getDriver().isNew()) { // it was new
							reopen(processor.getPolicyTransaction(), null, UIConstants.UIACTION_Delete);
						}
						else {
							reopen(processor.getPolicyTransaction(), processor.getDriver(), UIConstants.UIACTION_Delete);
						}
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}
	
	public String getTreeNodeText(Driver driver) {
		String result = (driver.getDriverSequence() != null ? driver.getDriverSequence().toString()+" : " : "")
						+ personalCustomerUIHelper.getCustomerName(driver);
		return result;
	}
	
	public int getYearsLicensed(LocalDate licenseDate) {
		if(licenseDate != null){
			LocalDate now = LocalDate.now();
			Period age = Period.between(licenseDate, now);
			int yearsLicensed = age.getYears();
			return yearsLicensed;
		}
		return 0;
	}
	
	public int getDriverAge(LocalDate birthDate) {
		if(birthDate != null){
			LocalDate now = LocalDate.now();
			Period age = Period.between(birthDate, now);
			int driverAge = age.getYears();
			return driverAge;
		}
		return 0;
		
	}
	
	public List<Driver> getDriverList(List<Long> filter) {
		List<Driver> results = new ArrayList<Driver>();
		
		if (policyUIHelper.getCurrentPolicyTransaction() != null &&
			policyUIHelper.getCurrentPolicyTransaction().getDrivers() != null &&
			!policyUIHelper.getCurrentPolicyTransaction().getDrivers().isEmpty()) {
			results.addAll(policyUIHelper.getCurrentPolicyTransaction().getDrivers().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList()));
			
			if (filter != null && !filter.isEmpty()) {
				results.removeIf(o -> filter.contains(o.getPersonPk()));
			}
		}
		
		return results;
	}
	
	public String getDriverLabel(Driver driver) {
		String result = null;
		
		if (driver != null) {
			result = personalCustomerUIHelper.getCustomerName(driver);
		}
		
		return result;
		
	}
}
