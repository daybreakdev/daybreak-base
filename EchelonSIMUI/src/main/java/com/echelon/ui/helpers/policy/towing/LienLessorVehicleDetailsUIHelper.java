package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.components.DSComboBox;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.ui.components.towing.LienLessorVehicleDetailsForm;
import com.echelon.ui.components.towing.LienLessorVehicleDetailsMultiForm;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.ds.ins.uicommon.converters.DSNumberConverter;
import com.echelon.ui.process.towing.LienLessorVehicleDetailsProcessor;
import com.echelon.ui.session.CoverageUIFactory;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.ds.ins.prodconf.policy.DeductibleConfig;
import com.vaadin.flow.component.UI;

public class LienLessorVehicleDetailsUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5301267155477852649L;

	
	protected LienLessorVehicleDetailsMultiForm createVehDetailsMultiForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		LienLessorVehicleDetailsMultiForm form = new LienLessorVehicleDetailsMultiForm(action, containerType);
		return form;
	}
	
	public LienholderLessorVehicleDetails createEmptyLienLessorVehicleDetails() {
		LienholderLessorVehicleDetails newVehicleDetails = new LienholderLessorVehicleDetails();
		newVehicleDetails.setLessorDeductible(getDefaultDeductible());
		return newVehicleDetails;
	}
	
	protected DSUpdateDialog<LienLessorVehicleDetailsMultiForm> createVehDetailsMultiDialog(String title, String action) {
		DSUpdateDialog<LienLessorVehicleDetailsMultiForm> dialog = (DSUpdateDialog<LienLessorVehicleDetailsMultiForm>) createDialogWithActions(title,
																				action,
																				createVehDetailsMultiForm(action, UICONTAINERTYPES.Dialog), 
																				UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel);
		dialog.setWidth("1100px");
		return dialog;
	}
	
	protected LienLessorVehicleDetailsForm createVehDetailsForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		LienLessorVehicleDetailsForm form = new LienLessorVehicleDetailsForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<LienLessorVehicleDetailsForm> createVehDetailsDialog(String title, String action) {
		DSUpdateDialog<LienLessorVehicleDetailsForm> dialog = (DSUpdateDialog<LienLessorVehicleDetailsForm>) createDialog(title, 
																				createVehDetailsForm(action, UICONTAINERTYPES.Dialog), 
																				action);
		return dialog;
	}

	public boolean newAction(Object vehicleDetailsParent, LienholderLessorSubPolicy lienLessorHolder) {
		DSUpdateDialog<LienLessorVehicleDetailsMultiForm> dialog 
									= createVehDetailsMultiDialog(
												uiHelper.getSessionLabels().getString("LienLessorVehDetailAddActionLabel"), 
												UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(vehicleDetailsParent, lienLessorHolder));
		dialog.getContentComponent().loadData(lienLessorHolder.getLienLessorType(), lienLessorHolder.getLienholderLessor().getCompanyName());
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	@Override
	protected void onAction(DSUpdateDialog.ActionEvent event) {
		DSUpdateDialog<LienLessorVehicleDetailsMultiForm> dialog = (DSUpdateDialog<LienLessorVehicleDetailsMultiForm>) event.getSource();
		if (UIConstants.UIACTION_Add.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit()) {
				dialog.getContentComponent().addAction(true);
			}
		}
		else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit()) {
				Object vehicleDetailsParent = dialog.getDataStore().getData1();
				Object lienLessorHolder     = dialog.getDataStore().getData2();
				
				createLienLessorVehicleDetailsTaskProcessor(dialog).start(
						createLienLessorVehicleDetailsProcessor(UIConstants.UIACTION_SaveNew, vehicleDetailsParent, lienLessorHolder, null, 
								dialog.getContentComponent().getAllData())
						);
			}			
		}
	}

	public boolean updateAction(Object vehicleDetailsParent, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) {
		DSUpdateDialog<LienLessorVehicleDetailsForm> dialog = createVehDetailsDialog(
															uiHelper.getSessionLabels().getString("LienLessorVehDetailUpdateActionLabel"), 
															UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(vehicleDetailsParent, lienLessorHolder, vehicleDetails));
		dialog.getContentComponent().loadData(lienLessorHolder.getLienLessorType(), lienLessorHolder.getLienholderLessor().getCompanyName(), 
												vehicleDetails);
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	public boolean viewAction(Object vehicleDetailsParent, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) {
		DSUpdateDialog<LienLessorVehicleDetailsForm> dialog = createVehDetailsDialog(
															uiHelper.getSessionLabels().getString("LienLessorVehDetailViewActionLabel"), 
															UIConstants.UIACTION_View);
		dialog.setDataStore(new DSDialogDataStore(vehicleDetailsParent, lienLessorHolder, vehicleDetails));
		dialog.getContentComponent().loadData(lienLessorHolder.getLienLessorType(), lienLessorHolder.getLienholderLessor().getCompanyName(), 
												vehicleDetails);
		dialog.open();
		return true;
	}

	@Override
	protected void onSaveUpdate(DSUpdateDialog.SaveEvent event) {
		DSUpdateDialog<LienLessorVehicleDetailsForm> dialog = (DSUpdateDialog<LienLessorVehicleDetailsForm>) event.getSource();
		Object vehicleDetailsParent = dialog.getDataStore().getData1();
		Object lienLessorHolder	    = dialog.getDataStore().getData2();
		LienholderLessorVehicleDetails vehicleDetails = (LienholderLessorVehicleDetails) dialog.getDataStore().getData3();
		if (dialog.getContentComponent().validateEdit(true) && dialog.getContentComponent().saveEdit(vehicleDetails)) {
			createLienLessorVehicleDetailsTaskProcessor(dialog).start(
					createLienLessorVehicleDetailsProcessor(UIConstants.UIACTION_Save, vehicleDetailsParent, lienLessorHolder, vehicleDetails, null)
					);
		}			
	}

	public void deleteAction(Object vehicleDetailsParent, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("LienLessorVehDetailDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicleDetailsParent, lienLessorHolder, vehicleDetails));
		dialog.open();
	}

	@Override
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog      = event.getSource();
		Object vehicleDetailsParent = dialog.getDataStore().getData1();
		Object lienLessorHolder     = dialog.getDataStore().getData2();
		LienholderLessorVehicleDetails vehicleDetails = (LienholderLessorVehicleDetails) dialog.getDataStore().getData3();
		
		createLienLessorVehicleDetailsTaskProcessor(dialog).start(
				createLienLessorVehicleDetailsProcessor(UIConstants.UIACTION_Delete, vehicleDetailsParent, lienLessorHolder, vehicleDetails, null)
				);
	}

	public void undeleteAction(Object vehicleDetailsParent, LienholderLessorSubPolicy lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("LienLessorVehDetailUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicleDetailsParent, lienLessorHolder, vehicleDetails));
		dialog.open();
	}
	
	@Override
	protected void onUndelete(ConfirmEvent event) {
		DSConfirmDialog dialog      = event.getSource();
		Object vehicleDetailsParent = dialog.getDataStore().getData1();
		Object lienLessorHolder     = dialog.getDataStore().getData2();
		LienholderLessorVehicleDetails vehicleDetails = (LienholderLessorVehicleDetails) dialog.getDataStore().getData3();
		
		createLienLessorVehicleDetailsTaskProcessor(dialog).start(
				createLienLessorVehicleDetailsProcessor(UIConstants.UIACTION_Undelete, vehicleDetailsParent, lienLessorHolder, vehicleDetails, null)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Object vehicleDetailsParent, Object lienLessorHolder, LienholderLessorVehicleDetails vehicleDetails, String action) {
		if (vehicleDetailsParent instanceof SpecialityAutoSubPolicy) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.AutomobileSubPolicyNode, (SpecialityAutoSubPolicy<?>)vehicleDetailsParent, lienLessorHolder);
		}
		else if (vehicleDetailsParent instanceof SpecialtyVehicleRisk) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Vehicle, (SpecialtyVehicleRisk)vehicleDetailsParent);
		}
	}
	
	private LienLessorVehicleDetailsProcessor createLienLessorVehicleDetailsProcessor(String action,
								Object vehicleDetailsParent,
								Object lienLessorHolder,
								LienholderLessorVehicleDetails vehicleDetails,
								List<LienholderLessorVehicleDetails> newVehicleDetailsList
								) {
		if (UIConstants.UIACTION_SaveNew.equals(action)) {
			return new LienLessorVehicleDetailsProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
							vehicleDetailsParent, lienLessorHolder, newVehicleDetailsList);
		}
		
		return new LienLessorVehicleDetailsProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
							vehicleDetailsParent, lienLessorHolder, vehicleDetails);
	}
	
	private TaskProcessor createLienLessorVehicleDetailsTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					LienLessorVehicleDetailsProcessor processor = (LienLessorVehicleDetailsProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicleDetailsParent(), processor.getLienLessorHolder(), null, processor.getProcessAction());
						break;

					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicleDetailsParent(), processor.getLienLessorHolder(), processor.getVehicleDetails(), processor.getProcessAction());
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicleDetailsParent(), processor.getLienLessorHolder(), null, processor.getProcessAction());
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicleDetailsParent(), processor.getLienLessorHolder(), null, processor.getProcessAction());
						break;

					default:
						break;
					}
				}
				
				return true;
			}
			
		};
	}
	
	public void setupDeductibleValues(DSComboBox cb) {
		cb.setDSItems(new ArrayList<LookupTableItem>());
		
		if (configUIHelper.getCurrentProductConfig() != null) {
			ICoverageConfig coverageConfig = configUIHelper.getCurrentProductConfig()
													.getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)
													.getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)
													.getCoverageConfig(ConfigConstants.COVERAGETYPE_COVERAGE, SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP);
			BaseCoverageConfigUIHelper covConfigUIHelper = CoverageUIFactory.getInstance().createCoverageConfigHelper(UIConstants.OBJECTTYPES.Vehicle);
			covConfigUIHelper.setDeductibleFieldItems(cb, coverageConfig, null);
		}
	}
	
	protected Double getDefaultDeductible() {
		Double result = null;
		
		if (configUIHelper.getCurrentProductConfig() != null) {
			ICoverageConfig coverageConfig = configUIHelper.getCurrentProductConfig()
													.getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)
													.getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)
													.getCoverageConfig(ConfigConstants.COVERAGETYPE_COVERAGE, SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP);
			BaseCoverageConfigUIHelper covConfigUIHelper = CoverageUIFactory.getInstance().createCoverageConfigHelper(UIConstants.OBJECTTYPES.Vehicle);
			DeductibleConfig deductibleConfig = covConfigUIHelper.getDeductibleConfig(coverageConfig);
			if (deductibleConfig != null) {
				result = (new DSNumberConverter()).toDouble(deductibleConfig.getDefaultValue());
			}
		}
		
		return result;
	}
}
