package com.echelon.ui.helpers.entities;

import java.util.Set;

import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.entities.LienholderLessorForm;

public class LienholderLessorSubPolicyUIHelper extends LienholderLessorUIHelper<LienholderLessorSubPolicy, SpecialityAutoSubPolicy<Risk>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1040144342471910760L;

	public LienholderLessorSubPolicyUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected LienholderLessorSubPolicy createEmptyLienLessorHolder(LienholderLessor lienholderLessor) {
		LienholderLessorSubPolicy lienLessorHolder = new LienholderLessorSubPolicy();
		lienLessorHolder.setLienholderLessor(lienholderLessor);
		lienLessorHolder.setLienLessorType(lienholderLessor.getLienLessorType());
		
		return lienLessorHolder;
	}

	@Override
	protected LienholderLessor getLienholderLessor(LienholderLessorSubPolicy lienLessorHolder) {
		return lienLessorHolder.getLienholderLessor();
	}

	@Override
	protected Set<LienholderLessorSubPolicy> getLienholderLessors(SpecialityAutoSubPolicy<Risk> subPolicy) {
		return subPolicy.getLienholderLessors();
	}
	
	@Override
	protected void formLoadData(LienholderLessorForm form, LienholderLessorSubPolicy lienLessorHolder) {
		form.loadData(lienLessorHolder);
	}
	
	@Override
	protected boolean formSaveEdit(LienholderLessorForm form, LienholderLessorSubPolicy lienLessorHolder) {
		return form.saveEdit(lienLessorHolder);
	}
}
