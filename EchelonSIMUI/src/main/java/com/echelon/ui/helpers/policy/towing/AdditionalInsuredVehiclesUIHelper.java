package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.AdditionalInsuredVehiclesForm;
import com.echelon.ui.components.towing.AdditionalInsuredVehiclesMultiForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.process.towing.AdditionalInsuredVehiclesProcessor;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.vaadin.flow.component.UI;

public class AdditionalInsuredVehiclesUIHelper extends ContentUIHelper implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3482495196541721820L;

	protected AdditionalInsuredVehiclesMultiForm createAddlInsuredVehiclesMultiForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		AdditionalInsuredVehiclesMultiForm form = new AdditionalInsuredVehiclesMultiForm(action, containerType);
		return form;
	}
	
	public AdditionalInsuredVehicles createEmptyAdditionalInsuredVehicles() {
		AdditionalInsuredVehicles newAddlInsuredVehicles = new AdditionalInsuredVehicles();
		return newAddlInsuredVehicles;
	}
	
	protected DSUpdateDialog<AdditionalInsuredVehiclesMultiForm> createAddlInsuredVehiclesMultiDialog(String title, String action) {
		DSUpdateDialog<AdditionalInsuredVehiclesMultiForm> dialog = (DSUpdateDialog<AdditionalInsuredVehiclesMultiForm>) createDialogWithActions(title,
																				action,
																				createAddlInsuredVehiclesMultiForm(action, UICONTAINERTYPES.Dialog), 
																				UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel);
		dialog.setWidth("1000px");
		return dialog;
	}
	
	protected AdditionalInsuredVehiclesForm createAddlInsuredVehiclesForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		AdditionalInsuredVehiclesForm form = new AdditionalInsuredVehiclesForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<AdditionalInsuredVehiclesForm> createAddlInsuredVehiclesDialog(String title, String action) {
		DSUpdateDialog<AdditionalInsuredVehiclesForm> dialog = (DSUpdateDialog<AdditionalInsuredVehiclesForm>) createDialog(title, 
																				createAddlInsuredVehiclesForm(action, UICONTAINERTYPES.Dialog), 
																				action);
		return dialog;
	}

	public boolean newAction(SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured) {
		DSUpdateDialog<AdditionalInsuredVehiclesMultiForm> dialog 
									= createAddlInsuredVehiclesMultiDialog(
												uiHelper.getSessionLabels().getString("AddlInsuredVehiclesAddActionLabel"), 
												UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, additionalInsured));
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	@Override
	protected void onAction(DSUpdateDialog.ActionEvent event) {
		DSUpdateDialog<AdditionalInsuredVehiclesMultiForm> dialog = (DSUpdateDialog<AdditionalInsuredVehiclesMultiForm>) event.getSource();
		if (UIConstants.UIACTION_Add.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit()) {
				dialog.getContentComponent().addAction(true);
			}
		}
		else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit()) {
				SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) dialog.getDataStore().getData1();
				AdditionalInsured additionalInsured         = (AdditionalInsured) dialog.getDataStore().getData2();
				
				createAdditionalInsuredVehiclesTaskProcessor(dialog).start(
						createAdditionalInsuredVehiclesProcessor(UIConstants.UIACTION_SaveNew, autoSubPolicy, additionalInsured, null, 
								dialog.getContentComponent().getAllData())
						);
			}			
		}
	}

	public boolean updateAction(SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles AddlInsuredVehicles) {
		DSUpdateDialog<AdditionalInsuredVehiclesForm> dialog = createAddlInsuredVehiclesDialog(
															uiHelper.getSessionLabels().getString("AddlInsuredVehiclesUpdateActionLabel"), 
															UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, additionalInsured, AddlInsuredVehicles));
		dialog.getContentComponent().loadData(AddlInsuredVehicles);
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	public boolean viewAction(SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles AddlInsuredVehicles) {
		DSUpdateDialog<AdditionalInsuredVehiclesForm> dialog = createAddlInsuredVehiclesDialog(
															uiHelper.getSessionLabels().getString("AddlInsuredVehiclesViewActionLabel"), 
															UIConstants.UIACTION_View);
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, additionalInsured, AddlInsuredVehicles));
		dialog.getContentComponent().loadData(AddlInsuredVehicles);
		dialog.open();
		return true;
	}

	@Override
	protected void onSaveUpdate(DSUpdateDialog.SaveEvent event) {
		DSUpdateDialog<AdditionalInsuredVehiclesForm> dialog = (DSUpdateDialog<AdditionalInsuredVehiclesForm>) event.getSource();
		SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured         = (AdditionalInsured) dialog.getDataStore().getData2();
		AdditionalInsuredVehicles AddlInsuredVehicles 	= (AdditionalInsuredVehicles) dialog.getDataStore().getData3();
		if (dialog.getContentComponent().validateEdit(true) && dialog.getContentComponent().saveEdit(AddlInsuredVehicles)) {
			createAdditionalInsuredVehiclesTaskProcessor(dialog).start(
					createAdditionalInsuredVehiclesProcessor(UIConstants.UIACTION_Save, autoSubPolicy, additionalInsured, AddlInsuredVehicles, null)
					);
		}			
	}

	public void deleteAction(SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles AddlInsuredVehicles) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("AddlInsuredVehiclesDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, additionalInsured, AddlInsuredVehicles));
		dialog.open();
	}

	public void undeleteAction(SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured, AdditionalInsuredVehicles AddlInsuredVehicles) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("AddlInsuredVehiclesUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, additionalInsured, AddlInsuredVehicles));
		dialog.open();
	}

	@Override
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured         = (AdditionalInsured) dialog.getDataStore().getData2();
		AdditionalInsuredVehicles AddlInsuredVehicles    = (AdditionalInsuredVehicles) dialog.getDataStore().getData3();
		
		createAdditionalInsuredVehiclesTaskProcessor(dialog).start(
				createAdditionalInsuredVehiclesProcessor(UIConstants.UIACTION_Delete, autoSubPolicy, additionalInsured, AddlInsuredVehicles, null)
				);
	}

	@Override
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured         = (AdditionalInsured) dialog.getDataStore().getData2();
		AdditionalInsuredVehicles AddlInsuredVehicles    = (AdditionalInsuredVehicles) dialog.getDataStore().getData3();
		
		createAdditionalInsuredVehiclesTaskProcessor(dialog).start(
				createAdditionalInsuredVehiclesProcessor(UIConstants.UIACTION_Undelete, autoSubPolicy, additionalInsured, AddlInsuredVehicles, null)
				);
	}
	
	private void reopen(PolicyTransaction policyTransaction, SpecialityAutoSubPolicy<Risk> autoSubPolicy, AdditionalInsured additionalInsured, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.AutomobileSubPolicyNode, autoSubPolicy, additionalInsured);
	}
	
	private AdditionalInsuredVehiclesProcessor createAdditionalInsuredVehiclesProcessor(String action,
								SpecialityAutoSubPolicy<Risk> autoSubPolicy,
								AdditionalInsured additionalInsured,
								AdditionalInsuredVehicles AddlInsuredVehicles,
								List<AdditionalInsuredVehicles> newAddlInsuredVehiclesList
								) {
		if (UIConstants.UIACTION_SaveNew.equals(action)) {
			return new AdditionalInsuredVehiclesProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
									autoSubPolicy, additionalInsured, newAddlInsuredVehiclesList);
		}
		
		return new AdditionalInsuredVehiclesProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
									autoSubPolicy, additionalInsured, AddlInsuredVehicles);
	}
	
	private TaskProcessor createAdditionalInsuredVehiclesTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					AdditionalInsuredVehiclesProcessor processor = (AdditionalInsuredVehiclesProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_SaveNew:
					case UIConstants.UIACTION_Save:
					case UIConstants.UIACTION_Delete:
					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getAutoSubPolicy(), processor.getAdditionalInsured(), processor.getProcessAction());
						break;

					default:
						break;
					}
				}
				
				return true;
			}
			
		};
	}
}
