package com.echelon.ui.helpers.policy;

import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.baseclasses.process.policy.BaseNewPolicyProcessor;
import com.echelon.ui.components.policy.NewPolicyWizard;
import com.echelon.ui.constants.UIConstants.NEWPOLICYSTEPS;
import com.echelon.ui.process.towing.NewSpecialtyAutoPolicyProcessor;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.PersonalCustomerException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.processes.customer.CommercialCustomerProcessing;

public class NewSpecialtyAutoPolicyUIHelper extends NewPolicyUIHelper<SpecialtyAutoPackage, PolicyVersion, PolicyTransaction<?>, SubPolicy<?>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 342240945922263419L;

	@SuppressWarnings("unchecked")
	@Override
	protected IGenericPolicyProcessing<SpecialtyAutoPackage, PolicyVersion, PolicyTransaction<?>, SubPolicy<?>> getPolicyProcessing() {
		// TODO Auto-generated method stub
		return policyUIHelper.getPolicyProcessing();
	}

	@Override
	protected InsurancePolicy createEmptyInsurancePolicy(InsurancePolicy insurancePolicy) {
		if (insurancePolicy != null && insurancePolicy.getClass().equals(SpecialtyAutoPackage.class)) {
			return insurancePolicy;
		}
		
		return new SpecialtyAutoPackage();
	}
	
	@SuppressWarnings("rawtypes")
	protected SpecialityAutoSubPolicy createEmptyAutoSubPolicy() {
		SpecialityAutoSubPolicy subPolicy = new SpecialityAutoSubPolicy();
		subPolicy.setIsFleet(true);
		return subPolicy;
	}
	
	@SuppressWarnings("rawtypes")
	protected CGLSubPolicy createEmptyCGLPolicy() {
		CGLSubPolicy subPolicy = new CGLSubPolicy();
		return subPolicy;
	}

	@Override
	protected NewPolicyDto createEmptyPolicyAndCustomert(NewPolicyDto newPolicyDto)
			throws CommercialCustomerException, PersonalCustomerException, SpecialtyAutoPolicyException {
		// TODO Auto-generated method stub
		newPolicyDto = super.createEmptyPolicyAndCustomert(newPolicyDto);
		
		newPolicyDto.setHasAutomobileSubPolicy(true);
		if (newPolicyDto.getAutoSubPolicy() == null) {
			newPolicyDto.setAutoSubPolicy(createEmptyAutoSubPolicy());
		}
		if (newPolicyDto.getcGLSubPolicy() == null) {
			newPolicyDto.setcGLSubPolicy(createEmptyCGLPolicy());
		}
		if (newPolicyDto.getPolicyTransaction().getPolicyVersion().getProducerCommissionRate() == null) {
			newPolicyDto.getPolicyTransaction().getPolicyVersion().setProducerCommissionRate(10D);
		}
		
		return newPolicyDto;
	}

	@Override
	protected boolean saveSubPoliciesEdit(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		if (Boolean.TRUE.equals(newPolicyDto.getHasAutomobileSubPolicy()) && newPolicyDto.getAutoSubPolicy() != null &&
			!dialog.saveEdit(newPolicyDto.getAutoSubPolicy())) {
			return false;
		}
		
		if (Boolean.TRUE.equals(newPolicyDto.getHasCGLSubPolicy()) && newPolicyDto.getcGLSubPolicy() != null &&
			!dialog.saveEdit(newPolicyDto.getcGLSubPolicy())) {
			return false;
		}
		
		if (Boolean.TRUE.equals(newPolicyDto.getHasCargoSubPolicy()) && newPolicyDto.getCargoSubPolicy() != null &&
			!dialog.saveEdit(newPolicyDto.getCargoSubPolicy())) {
			return false;
		}
		
		return true;
	}

	@Override
	@SuppressWarnings("rawtypes")
	protected BaseNewPolicyProcessor createPolicyProcessor(String action, NewPolicyDto newPolicyDto, NEWPOLICYSTEPS step) {
		return new NewSpecialtyAutoPolicyProcessor(action, 
							newPolicyDto.getPolicyTransaction(), 
							new CommercialCustomerProcessing(), 
							newPolicyDto, step);
	}

	@Override
	public void onProductCodeChange(NewPolicyDto newPolicyDto) throws Exception {
		super.onProductCodeChange(newPolicyDto);
		
		if (configUIHelper.getCurrentProductConfig() != null) {
			if (newPolicyDto.getHasAutoPolicyField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_AUTO) != null) {
				//newPolicyDto.setHasAutomobileSubPolicy(true);
				uiHelper.setFieldValue(newPolicyDto.getHasAutoPolicyField(), Boolean.TRUE);
				//newPolicyDto.getHasAutomobileSubPolicyBinding().setAsRequiredEnabled(true);
			}
		}
	}
	
}
