package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.towing.AutoSubPolicyForm;
import com.echelon.ui.components.towing.AutoSubPolicyView;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.process.towing.AutoSubPolicyProcessor;

public class AutoSubPolicyUIHelper extends BaseSpecialtySubPolicyContentUIHelper<SpecialityAutoSubPolicy<?>, AutoSubPolicyProcessor> 
									 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127595188814038214L;

	public AutoSubPolicyUIHelper() {
		super(SpecialtyAutoConstants.SUBPCY_CODE_AUTO, UIConstants.POLICYTREENODES.AutomobileSubPolicyNode);
		// TODO Auto-generated constructor stub
	}
	
	public void loadView(AutoSubPolicyView component, SpecialityAutoSubPolicy subPolicy) {
		component.loadData(subPolicy);
	}
	
	protected AutoSubPolicyForm createSubPolicyForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		AutoSubPolicyForm form = new AutoSubPolicyForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<AutoSubPolicyForm> createSubPolicyDialog(String title, String action) {
		DSUpdateDialog<AutoSubPolicyForm> dialog = (DSUpdateDialog<AutoSubPolicyForm>) createDialog(title, createSubPolicyForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	@Override
	protected void updateSubPolicyAction(SpecialityAutoSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("AutoSubPolicyUpdateDialogTitle");
		DSUpdateDialog<AutoSubPolicyForm> dialog = createSubPolicyDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		dialog.getContentComponent().loadData(subPolicy);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<AutoSubPolicyForm> dialog = (DSUpdateDialog<AutoSubPolicyForm>) event.getSource();
		SpecialityAutoSubPolicy subPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(subPolicy)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}

			createSubPolicyTaskProcessor(dialog).start(
					createSubPolicyProcessor(UIConstants.UIACTION_Save, subPolicy)
					);
		}
	}

	@Override
	protected AutoSubPolicyProcessor createSubPolicyProcessor(String action,
							SpecialityAutoSubPolicy<?> subPolicy) {
		return new AutoSubPolicyProcessor(action, 
								policyUIHelper.getCurrentPolicyTransaction(), subPolicy);
	}

	@Override
	protected AutoSubPolicyProcessor createSubPolicyProcessor(String action) {
		return new AutoSubPolicyProcessor(action, 
								policyUIHelper.getCurrentPolicyTransaction(), 
								SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
	}
}
