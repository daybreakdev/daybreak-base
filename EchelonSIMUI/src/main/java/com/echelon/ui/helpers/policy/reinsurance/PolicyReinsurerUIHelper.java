package com.echelon.ui.helpers.policy.reinsurance;

import java.io.Serializable;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.SetUtils;

import com.ds.ins.domain.entities.Reinsurer;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.exceptions.ReinsurerException;
import com.ds.ins.processes.reinsurer.ReinsurerProcesses;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.components.DSUpdateDialog.ActionEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.policy.reinsurance.BasePolicyReinsurerContentView;
import com.echelon.ui.components.baseclasses.policy.reinsurance.PolicyReinsurerNewDialog;
import com.echelon.ui.components.baseclasses.policy.reinsurance.PolicyReinsurerViewDialog;
import com.echelon.ui.components.baseclasses.process.policy.reinsurance.BasePolicyReinsurerProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.vaadin.flow.component.UI;

/**
 * UI Helper for {@link BasePolicyReinsurerContentView}
 */
public class PolicyReinsurerUIHelper extends ContentUIHelper implements Serializable {
	private static final long serialVersionUID = -3278974512625699120L;

	public void loadView(BasePolicyReinsurerContentView component, DSTreeGridItem nodeItem) {
		component.loadData(getActivePolicyReinsurers());
	}

	public boolean isReinsuranceAllowed() {
		return configUIHelper.getCurrentProductConfig().isReinsurable();
	}

	/**
	 * Creates new PolicyReinsurer mappings.
	 */
	public void newAction() {
		try {
			// Get list of available reinsurers
			List<Reinsurer> allReinsurers = new ReinsurerProcesses().getAllReinsurers();
			Set<Long> allReinsurerPKs = allReinsurers.stream().map(Reinsurer::getCompanyPK)
				.collect(Collectors.toSet());

			// Retrieve the list of reinsurers currently mapped to the policy.
			// Because of policy versioning and transactions, we can't simply rely on
			// PolicyReinsurer.equals(). Instead, we directly compare the reinsurers'
			// companyPKs (neither Reinsurer or its parent, Company, implement equals(), so
			// Reinsurer.equals() relies on reference equality).
			Set<Long> mappedReinsurersPKs = getActivePolicyReinsurers().stream()
				.map(PolicyReinsurer::getReinsurer).map(Reinsurer::getCompanyPK)
				.collect(Collectors.toSet());

			// Compute the difference; these are the reinsurers available to be added to the
			// policy
			Set<Long> availableReinsurerPKs = SetUtils.difference(allReinsurerPKs,
				mappedReinsurersPKs);
			Set<Reinsurer> availableReinsurers = allReinsurers.stream()
				.filter(reinsurer -> availableReinsurerPKs.contains(reinsurer.getCompanyPK()))
				.collect(Collectors.toSet());

			// Finally, display a dialog with the available reinsurers.
			String title = uiHelper.getSessionLabels().getString("PolicyReinsurerNewDialogTitle");
			PolicyReinsurerNewDialog dialog = new PolicyReinsurerNewDialog(title,
				availableReinsurers);
			initDialogWithActions(dialog);
			dialog.open();
		} catch (ReinsurerException e) {
			uiHelper.notificationException(e);
		}
	}

	/**
	 * Catch-all handler for any action event. For some reason the new/save event
	 * needs to be processed via onAction, whereas the other actions can use
	 * action-specific methods (onDelete, etc.).
	 */
	@Override
	protected void onAction(ActionEvent event) {
		switch (event.getAction()) {
		case UIConstants.UIACTION_Save:
			PolicyReinsurerNewDialog dialog = (PolicyReinsurerNewDialog) event.getSource();
			Set<Reinsurer> reinsurers = dialog.getSelectedReinsurers();
			BasePolicyReinsurerProcessor processor = new BasePolicyReinsurerProcessor(
				UIConstants.UIACTION_Save, policyUIHelper.getCurrentPolicyTransaction(),
				reinsurers);
			createTaskProcessor(dialog).start(processor);
			break;
		default:
			break;
		}
	}

	/**
	 * Opens a dialog displaying information about a {@link PolicyReinsurer}.
	 * 
	 * @param policyReinsurer the {@link PolicyReinsurer} to view.
	 */
	public void viewAction(PolicyReinsurer policyReinsurer) {
		PolicyReinsurerViewDialog dialog = new PolicyReinsurerViewDialog(policyReinsurer,
			uiHelper.getSessionLabels());
		dialog.open();
	}

	/**
	 * Removes a {@link PolicyReinsurer} mapping.
	 * 
	 * @param policyReinsurer the {@link PolicyReinsurer} mapping to remove.
	 */
	public void deleteAction(PolicyReinsurer policyReinsurer) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(
			uiHelper.getSessionLabels().getString("PolicyReinsurerDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(policyReinsurer));
		dialog.open();
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		PolicyReinsurer policyReinsurer = (PolicyReinsurer) dialog.getDataStore().getData1();
		BasePolicyReinsurerProcessor processor = new BasePolicyReinsurerProcessor(
			UIConstants.UIACTION_Delete, policyUIHelper.getCurrentPolicyTransaction(),
			policyReinsurer);
		createTaskProcessor(dialog).start(processor);
	}

	@SuppressWarnings("serial")
	private TaskProcessor createTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyReinsurerProcessor processor = (BasePolicyReinsurerProcessor) taskRun
						.getProcessor();
					dialog.close();
					policyUIHelper.reopenPolicy(processor.getPolicyTransaction(),
						POLICYTREENODES.Reinsurers, null);
				}

				return true;
			}
		};
	}

	/**
	 * Retrieve the policy reinsurers attached to the current policy transaction.
	 */
	private Set<PolicyReinsurer> getActivePolicyReinsurers() {
		return policyUIHelper.getCurrentPolicyTransaction().getActivePolicyReinsurers();
	}
}
