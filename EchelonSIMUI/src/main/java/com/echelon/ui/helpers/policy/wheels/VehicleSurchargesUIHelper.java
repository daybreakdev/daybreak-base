package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.VehicleSurchargesForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.towing.VehicleProcessor;
import com.vaadin.flow.component.UI;

public class VehicleSurchargesUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3934102380152438345L;

	public VehicleSurchargesUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	@SuppressWarnings({ "unchecked" })
	public void editSurchargesAction(SpecialtyVehicleRisk vehicle) {
		DSUpdateDialog<VehicleSurchargesForm> dialog = (DSUpdateDialog<VehicleSurchargesForm>) createDialog(
																	uiHelper.getSessionLabels().getString("VehicleSurchargesUpdateDialogTitle"), 
																	new VehicleSurchargesForm(UIConstants.UIACTION_Update, UICONTAINERTYPES.Dialog), 
																	UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(vehicle));
		dialog.getContentComponent().loadData(vehicle);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}


	@SuppressWarnings("unchecked")
	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<VehicleSurchargesForm> dialog = (DSUpdateDialog<VehicleSurchargesForm>) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(vehicle)) {
			createVehicleTaskProcessor(dialog).start(
					createVehicleProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											vehicle));
		}
	}
	
	@SuppressWarnings("rawtypes")
	private VehicleProcessor createVehicleProcessor(String action, PolicyTransaction policyTransaction, 
														SpecialtyVehicleRisk vehicle) {
		return new VehicleProcessor(action, policyTransaction, vehicle);
	}
	
	@SuppressWarnings("serial")
	private TaskProcessor createVehicleTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					VehicleProcessor processor = (VehicleProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), UIConstants.UIACTION_Save);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}

	@SuppressWarnings("rawtypes")
	private void reopen(PolicyTransaction policyTransaction, SpecialtyVehicleRisk vehicle, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Vehicle, vehicle);
	}

}
