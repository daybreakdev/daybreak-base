package com.echelon.ui.helpers.policy.trucking;

import com.ds.ins.uicommon.components.IDSDialog;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyUIHelper;
import com.echelon.ui.process.towing.CargoSubPolicyProcessor;

public class LHTCargoSubPolicyUIHelper extends CargoSubPolicyUIHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8959032099139916895L;

	public LHTCargoSubPolicyUIHelper() {
		super();
	}
	
	@Override
	protected void onProcessSaveComplete(IDSDialog dialog, CargoSubPolicyProcessor processor) {
		super.onProcessSaveComplete(dialog, processor);
		uiHelper.notificationMessage(uiHelper.getSessionLabels().getString("CargoSubPolicyUpdateNotificationLabel"));
	}

}
			
	