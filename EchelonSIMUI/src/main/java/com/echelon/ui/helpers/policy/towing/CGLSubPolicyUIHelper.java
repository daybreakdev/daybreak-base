package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;

import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.ui.components.towing.CGLSubPolicyDialog;
import com.echelon.ui.components.towing.CGLSubPolicyView;
import com.echelon.ui.components.towing.LocationLinkList;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtySubPolicyContentUIHelper;
import com.echelon.ui.process.towing.CGLSubPolicyProcessor;
import com.ds.ins.uicommon.components.DSConfirmDialog;

public class CGLSubPolicyUIHelper extends BaseSpecialtySubPolicyContentUIHelper<CGLSubPolicy<?>, CGLSubPolicyProcessor> 
									implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127595188814038214L;

	public CGLSubPolicyUIHelper() {
		super(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, UIConstants.POLICYTREENODES.CGLSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	public void loadView(CGLSubPolicyView component, CGLSubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}

	public void loadView(LocationLinkList component, CGLSubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}

	@Override
	protected void newSubPolicyAction(CGLSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("CGLSubPolicyDialogTitle");
		CGLSubPolicyDialog dialog = new CGLSubPolicyDialog(title, UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_New);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	@Override
	protected void updateSubPolicyAction(CGLSubPolicy<?> subPolicy) {
		String title = uiHelper.getSessionLabels().getString("CGLSubPolicyDialogTitle");
		CGLSubPolicyDialog dialog = new CGLSubPolicyDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(subPolicy));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.loadData(subPolicy);
		dialog.open();
	}

	public void deleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("CGLSubPolicyDeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmDeleteDialog(title);
		dialog.open();
	}

	public void undeleteSubPolicyAction() {
		String title = uiHelper.getSessionLabels().getString("CGLSubPolicyUndeleteDialogTitle");
		DSConfirmDialog dialog = createConfirmUndeleteDialog(title);
		dialog.open();
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		CGLSubPolicyDialog dialog = (CGLSubPolicyDialog) event.getSource();
		CGLSubPolicy<?> subPolicy   = (CGLSubPolicy<?>) dialog.getDataStore().getData1();
		
		if (dialog.validateEdit()) {
			try {
				if (!dialog.saveEdit(subPolicy)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createSubPolicyTaskProcessor(dialog).start(
					createSubPolicyProcessor(UIConstants.UIACTION_SaveNew, subPolicy)
				);
		}
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		try {
			CGLSubPolicyDialog dialog   = (CGLSubPolicyDialog) event.getSource();
			CGLSubPolicy<?> subPolicy     = (CGLSubPolicy<?>) dialog.getDataStore().getData1();
			CGLSubPolicy<?> prevSubPolicy = (CGLSubPolicy<?>) subPolicy.clone();
			
			if (dialog.validateEdit() && dialog.saveEdit(subPolicy)) {
				createSubPolicyTaskProcessor(dialog).start(
						new CGLSubPolicyProcessor(UIConstants.UIACTION_Save, policyUIHelper.getCurrentPolicyTransaction(), subPolicy, prevSubPolicy)
					);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected CGLSubPolicyProcessor createSubPolicyProcessor(String action,
							CGLSubPolicy<?> subPolicy) {
		return new CGLSubPolicyProcessor(action, policyUIHelper.getCurrentPolicyTransaction(), subPolicy);
	}

	@Override
	protected CGLSubPolicyProcessor createSubPolicyProcessor(String action) {
		return new CGLSubPolicyProcessor(action,  
								policyUIHelper.getCurrentPolicyTransaction(), 
								SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
	}
}
