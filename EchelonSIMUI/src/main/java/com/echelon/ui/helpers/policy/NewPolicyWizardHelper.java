package com.echelon.ui.helpers.policy;

import java.io.Serializable;

import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.policy.NewPolicyWizard;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.PersonalCustomerException;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog.CancelEvent;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;

public class NewPolicyWizardHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8043819692864677321L;
	private NewPolicyWizard newPolicyWizard;
	
	public interface NewPolicyWizardCallback {
		public void continueSaveNewPolicy();
	}
	
	protected NewPolicyWizard createNewPolicyWizard(NewPolicyDto newPolicyDto) {
		NewPolicyWizard wizard = new NewPolicyWizard();

		NewPolicyWizardCallback callback = new NewPolicyWizardCallback() {
			
			@Override
			public void continueSaveNewPolicy() {
				if (getNewPolicyUIHelper().saveNewPolicyAction(wizard, newPolicyDto)) {
					if (newPolicyDto.getHasGarageSubPolicy()) {
						uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnNewPolicyGarageDetails"));
					}
					
					wizard.close();
					onComplete();
				}
			}
		};
		
		wizard.initWizard(newPolicyDto, callback);
		
		return wizard;
	}
	
	protected NewPolicyDto createNewPolicyDto(UIConstants.NEWPOLICYTYPES newPolicyType) {
		NewPolicyDto newPolicyDto = null;
		try {
			newPolicyDto = getNewPolicyUIHelper().createEmptyNewPolicyDto(newPolicyType);
		} catch (Exception e) {
			uiHelper.notificationException(e);
		}
		
		return newPolicyDto;
	}
	
	public void newQuoteWizard(boolean isNewClient) {
		NewPolicyDto newPolicyDto = createNewPolicyDto(UIConstants.NEWPOLICYTYPES.QUOTE);
		newWizzard(isNewClient, newPolicyDto);
	}
	
	public void newPolicyWizard(boolean isNewClient) {
		NewPolicyDto newPolicyDto = createNewPolicyDto(UIConstants.NEWPOLICYTYPES.POLICY);
		newWizzard(isNewClient, newPolicyDto);
	}
	
	public void newRenewalWizard() {
		NewPolicyDto newPolicyDto = createNewPolicyDto(UIConstants.NEWPOLICYTYPES.RENEWAL);
		newWizzard(true, newPolicyDto);
	}
	
	protected void newWizzard(boolean isNewClient, NewPolicyDto newPolicyDto) {
		if (!isNewClient) {
			loadCustomer(newPolicyDto);
		}
		
		newPolicyWizard = createNewPolicyWizard(newPolicyDto);
		setupWizardSaveNewPolicy(newPolicyWizard, newPolicyDto);
		newPolicyWizard.loadData();
		newPolicyWizard.open();
		onStart();
	}
	
	protected void loadCustomer(NewPolicyDto newPolicyDto) {
		if (policyUIHelper.getCurrentPolicyVersion() != null &&
			policyUIHelper.getCurrentPolicyVersion().getInsurancePolicy() != null) {
			newPolicyDto.setCustomer(policyUIHelper.getCurrentPolicyVersion().getInsurancePolicy().getPolicyCustomer());
			newPolicyDto.setAddress(policyUIHelper.getCurrentInsurerLocation().getLocationAddress());
		}
	}
	
	@SuppressWarnings("rawtypes")
	protected NewPolicyUIHelper getNewPolicyUIHelper() {
		if (newPolicyWizard != null && newPolicyWizard.getPolicyForm() != null) {
			return newPolicyWizard.getPolicyForm().getNewPolicyUIHelper();	
		}
		
		return UIFactory.getInstance().getNewPolicyUIHelper(null);
	}
	
	protected void setupWizardSaveNewPolicy(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		dialog.setDataStore(new DSDialogDataStore(newPolicyDto));
		initDialog(dialog, UIConstants.UIACTION_New);
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		NewPolicyWizard dialog = (NewPolicyWizard) event.getSource();
		if (!dialog.wizardValidateEdit()) {
			return;
		}
		
		//see the content of the callback
		//createNewPolicyWizard.continueSaveNewPolicy
	}

	@Override
	protected void onCancel(CancelEvent event) {
		NewPolicyWizard dialog = (NewPolicyWizard) event.getSource();
		policyUIHelper.updateSessionProductConfigWithPolicyVersion();
		dialog.close();
		onComplete();
	}
	
	protected void onStart() {
		if (BrokerUI.getUserSession().getPolicySearchView() != null) {
			BrokerUI.getUserSession().getPolicySearchView().disableButtonShortcuts();
		}
	}
	
	protected void onComplete() {
		if (BrokerUI.getUserSession().getPolicySearchView() != null) {
			BrokerUI.getUserSession().getPolicySearchView().enableButtonShortcuts();
		}
	}
	
	// Always create a new customer based on the customer type
	public NewPolicyDto reCreateEmptyCustomert(NewPolicyDto newPolicyDto) throws CommercialCustomerException, PersonalCustomerException {
		newPolicyDto.setCustomer(null);
		return getNewPolicyUIHelper().createEmptyCustomert(newPolicyDto);
	}
}
