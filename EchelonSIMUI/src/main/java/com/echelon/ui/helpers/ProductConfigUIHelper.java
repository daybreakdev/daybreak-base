package com.echelon.ui.helpers;

import java.util.List;

import com.ds.ins.prodconf.baseclasses.NavigationTreeConfig;
import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.prodconf.interfaces.IGenericLifecycleConfig;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.interfaces.IGenericUIConfig;
import com.ds.ins.prodconf.interfaces.ILifecycleStatusConfig;
import com.ds.ins.uicommon.helpers.DSProductConfigUIHelper;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.ui.session.BrokerUI;

public class ProductConfigUIHelper extends DSProductConfigUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5473778575159940840L;


	public ProductConfigUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		// TODO Auto-generated method stub
		return ProductConfigFactory.getInstance();
	}

	@Override
	protected IGenericProductConfig loadProductConfig() {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null) {
			return getProductConfig(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), 
					  			    BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getProductConfigurationVersion());
		}
		
		return null;
	}


	@Override
	protected ILifecycleStatusConfig loadLifecycleStatusConfig() {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			return getLifecycleStatusConfig(ConfigConstants.LIFECYCLETYPE_Policy,
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), 
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate(),
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getBusinessStatus(),
											BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType());
		}
		
		return null;
	}


	@Override
	protected List<ILifecycleStatusConfig> loadLifecycleStatusConfigs() throws Exception {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			IGenericLifecycleConfig lfConfig = ProductConfigFactory.getInstance().getLifecycleConfig(
															BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), 
															BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
			if (lfConfig != null) {
				return lfConfig.getLifecycleStatusConfigsByTransactionType(ConfigConstants.LIFECYCLETYPE_Policy,
										BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType());
			}
		}
		
		return null;
	}


	@Override
	public IGenericUIConfig loadUIConfig() {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			return getUIConfig(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), 
					  		   BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		}
		
		return null;
	}

	@Override
	public NavigationTreeConfig loadNavigationTreeConfig() {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			return getNavigationTreeConfig(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(), 
					  		   				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		}
		
		return null;
	}

	public boolean isCurrentAllowProductReinsurance() {
		final IGenericProductConfig productConfig = getCurrentProductConfig();
		return productConfig != null && productConfig.isReinsurable();
	}
}
