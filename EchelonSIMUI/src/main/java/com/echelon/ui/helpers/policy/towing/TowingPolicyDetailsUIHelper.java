package com.echelon.ui.helpers.policy.towing;

import com.echelon.ui.components.baseclasses.helpers.policy.BasePolicyDetailsUIHelper;
import com.echelon.ui.components.baseclasses.policy.BasePolicyUpdateForm;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyDetailProcessor;
import com.echelon.ui.components.towing.TowingPolicyUpdateForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.process.towing.TowingPolicyDetailProcessor;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.domain.policy.PolicyTransaction;

public class TowingPolicyDetailsUIHelper extends BasePolicyDetailsUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1185457060945410846L;

	public TowingPolicyDetailsUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected TowingPolicyUpdateForm createPolicyUpdateForm(String action, UICONTAINERTYPES containerType) {
		// TODO Auto-generated method stub
		return new TowingPolicyUpdateForm(action, containerType);
	}

	@Override
	protected BasePolicyDetailProcessor createUpdatePolicyProcessor(PolicyTransaction polTransaction,
			DSUpdateDialog<BasePolicyUpdateForm> dialog) {
		
		return new TowingPolicyDetailProcessor(UIConstants.UIACTION_Update, 
							polTransaction, 
							dialog.getContentComponent().getTermEffectiveDate().atStartOfDay(), 
							dialog.getContentComponent().getQuotePreparedBy(), 
							((TowingPolicyUpdateForm)dialog.getContentComponent()).getLiabilityLimit(),
							dialog.getContentComponent().getExternalQuoteNumber());
	}

}
