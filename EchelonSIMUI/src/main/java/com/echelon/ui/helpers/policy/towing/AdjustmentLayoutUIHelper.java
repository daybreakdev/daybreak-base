package com.echelon.ui.helpers.policy.towing;

import com.echelon.ui.components.baseclasses.helpers.LayoutUIHelper;
import com.echelon.ui.components.towing.AdjustmentActionToolbar;
import com.ds.ins.uicommon.components.DSBaseActionToolbar;
import com.ds.ins.uicommon.constants.DSUIConstants.UICOMPONENTTYPES;
import com.ds.ins.uicommon.dto.DSActionDto;

/**
 * Layout UI helper for adjustments.
 * <p>
 * Adjustments are only editable during the pending adjustment lifecycle phase;
 * this helper overrides the action toolbar creation methods so that an
 * {@link AdjustmentActionToolbar} is returned instead. The
 * {@link AdjustmentActionToolbar} determines for itself whether or not to allow
 * edits.
 * </p>
 */
public class AdjustmentLayoutUIHelper extends LayoutUIHelper {
	private static final long serialVersionUID = -8660213519196027353L;

	@Override
	public DSBaseActionToolbar createActionToolbar(UICOMPONENTTYPES uiComponentType, String... actions) {
		if (actions == null || actions.length < 1) {
			return new AdjustmentActionToolbar(uiComponentType);
		}

		return new AdjustmentActionToolbar(uiComponentType, actions);
	}

	@Override
	public DSBaseActionToolbar createActionToolbar(UICOMPONENTTYPES uiComponentType, boolean isButtonTextAndIcon,
			DSActionDto... actionAndIcons) {
		if (actionAndIcons == null || actionAndIcons.length < 1) {
			return new AdjustmentActionToolbar(uiComponentType);
		}

		boolean anyIcon = false;
		String[] actions = new String[actionAndIcons.length];
		for (int ii = 0; ii < actionAndIcons.length; ii++) {
			actions[ii] = actionAndIcons[ii].getName();
			if (actionAndIcons[ii].getIcon() != null) {
				anyIcon = true;
			}
		}

		if (anyIcon) {
			return new AdjustmentActionToolbar(uiComponentType, isButtonTextAndIcon, actionAndIcons);
		}

		return new AdjustmentActionToolbar(uiComponentType, actions);
	}
}
