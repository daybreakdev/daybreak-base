package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.DriverSuspensionForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.DriverSuspensionProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverSuspension;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class DriverSuspensionUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 56264586493513840L;

	public DriverSuspensionUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public DriverSuspensionForm createSuspensionForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		DriverSuspensionForm form = null;

		form = new DriverSuspensionForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<DriverSuspensionForm> createSuspensionDialog(String title, String action) {
		DSUpdateDialog<DriverSuspensionForm> dialog = (DSUpdateDialog<DriverSuspensionForm>) createDialog(title, createSuspensionForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupSuspensionAction(String title, String action, Driver driver, DriverSuspension suspension) {
		DSUpdateDialog<DriverSuspensionForm> dialog = createSuspensionDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(driver, suspension));
		dialog.getContentComponent().loadData(driver, suspension);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newSuspensionAction(Driver driver) {
		createSuspensionTaskProcessor(null).start(
				createSuspensionProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null, driver)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<DriverSuspensionForm> dialog = (DSUpdateDialog<DriverSuspensionForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverSuspension suspension = (DriverSuspension) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(suspension)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createSuspensionTaskProcessor(dialog).start(
					createSuspensionProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									suspension, driver)
					);
		}
	}

	public void updateSuspensionAction(Driver driver, DriverSuspension suspension) {
		String title = uiHelper.getSessionLabels().getString("DriverSuspensionUpdateDialogTitle");
		setupSuspensionAction(title, UIConstants.UIACTION_Update, driver, suspension);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<DriverSuspensionForm> dialog = (DSUpdateDialog<DriverSuspensionForm>) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverSuspension suspension = (DriverSuspension) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(suspension)) {

			createSuspensionTaskProcessor(dialog).start(
					createSuspensionProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											suspension, driver));
		}
	}
	
	public void deleteSuspensionAction(Driver driver, DriverSuspension suspension) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("DriverSuspensionDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, suspension));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverSuspension suspension = (DriverSuspension) dialog.getDataStore().getData2();
		createSuspensionTaskProcessor(dialog).start(
				createSuspensionProcessor(UIConstants.UIACTION_Delete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							suspension, driver)
				);
	}
	
	public void undeleteSuspensionAction(Driver driver, DriverSuspension suspension) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("DriverSuspensionUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(driver, suspension));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		Driver driver = (Driver) dialog.getDataStore().getData1();
		DriverSuspension suspension = (DriverSuspension) dialog.getDataStore().getData2();
		createSuspensionTaskProcessor(dialog).start(
				createSuspensionProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							suspension, driver)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, Driver driver, DriverSuspension suspension, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Driver, driver);
	}

	public List<DriverSuspension> sortSuspensionList(Set<DriverSuspension> suspensions) {
		List<DriverSuspension> result = new ArrayList<DriverSuspension>(); 
		if (suspensions != null) {
			result.addAll(suspensions);
		}
		
		return result;
	}
	
	protected DriverSuspensionProcessor createSuspensionProcessor(String action, PolicyTransaction policyTransaction, 
													DriverSuspension suspension, Driver driver) {
		return new DriverSuspensionProcessor(action, policyTransaction).withParams(driver, suspension);
	}
	
	protected TaskProcessor createSuspensionTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					DriverSuspensionProcessor processor = (DriverSuspensionProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupSuspensionAction(uiHelper.getSessionLabels().getString("DriverSuspensionNewDialogTitle"), 
												UIConstants.UIACTION_New, processor.getDriver(), processor.getDriverSuspension());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverSuspension(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverSuspension(), UIConstants.UIACTION_Delete);
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getDriver(), processor.getDriverSuspension(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}
	
	public int getSuspensionLength(LocalDate startDate, LocalDate endDate) {
		if(startDate != null && endDate != null) {
				Period period = Period.between(startDate, endDate);
				int length = period.getYears();
				return length;
			}
		return 0;
	}
}