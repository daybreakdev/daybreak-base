package com.echelon.ui.helpers.policy.reinsurance;

import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCovReinsuranceUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyCovReinsuranceUIHelper extends BaseCovReinsuranceUIHelper {
	private static final long serialVersionUID = 36181750928767392L;

	@Override
	public Integer getPolicyLiabilityLimit() {
		Integer result = null;

		if (policyUIHelper.getCurrentPolicyVersion() != null) {
			result = ((SpecialtyAutoPackage) policyUIHelper.getCurrentPolicyVersion().getInsurancePolicy())
					.getLiabilityLimit();
		}

		return result;
	}

	@Override
	protected PolicyReinsurer getPrimaryPolicyReinsurer() {
		PolicyReinsurer policyReinsurer = null;

		if (policyUIHelper.getCurrentPolicyTransaction() != null) {
			PolicyTransaction<?> policyTransaction = policyUIHelper.getCurrentPolicyTransaction();
			if (policyTransaction.getPolicyReinsurers() != null && !policyTransaction.getPolicyReinsurers().isEmpty()) {
				policyReinsurer = policyTransaction.getPolicyReinsurers().stream()
					.filter(o -> o.getReinsurer().getLegalName().toUpperCase()
							.contains(SpecialtyAutoConstants.REINSURER_ECHELON.toUpperCase()))
					.findFirst()
					.orElse(null);
			}
		}

		return policyReinsurer;
	}
}
