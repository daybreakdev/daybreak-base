package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.dto.DSMultiNewEntryDto;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.towing.CargoSubPolicyValueDialog;
import com.echelon.ui.components.towing.CargoSubPolicyValueForm;
import com.echelon.ui.components.towing.CargoSubPolicyValueList;
import com.echelon.ui.components.towing.CargoSubPolicyValueMultiForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.towing.CargoSubPolicyValueProcessor;
import com.vaadin.flow.component.UI;

public class CargoSubPolicyValueUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {
	private static final long serialVersionUID = -7600994974538411196L;

	public void loadView(CargoSubPolicyValueList component, CargoSubPolicy<?> subPolicy) {
		component.loadData(subPolicy);
	}
	
	public CargoDetails createEmptyCargoDetails() {
		CargoDetails newCargoDetails = new CargoDetails();
		
		newCargoDetails.setAverage(Double.valueOf(0));
		newCargoDetails.setuWManual(Double.valueOf(0));
		newCargoDetails.setCargoValue(Double.valueOf(0));
		newCargoDetails.setCargoPartialPremiums(Double.valueOf(0));
		
		return newCargoDetails;
	}
	
	protected CargoSubPolicyValueMultiForm createCargoSubPolicyValueMultiForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		return new CargoSubPolicyValueMultiForm(action, containerType);
	}
	
	@SuppressWarnings("unchecked")
	protected DSUpdateDialog<CargoSubPolicyValueMultiForm> createCargoSubPolicyValueMultiDialog(String title, String action) {
		DSUpdateDialog<CargoSubPolicyValueMultiForm> dialog = (DSUpdateDialog<CargoSubPolicyValueMultiForm>) createDialogWithActions(title,
																		action,
																		createCargoSubPolicyValueMultiForm(action, UICONTAINERTYPES.Dialog), 
																		UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel);
		dialog.setWidth("1100px");
		return dialog;
	}

	public void newCargoSubPolicyValueAction(CargoSubPolicy<?> parentSubPolicy) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyValueNewActionTitle");
		DSUpdateDialog<CargoSubPolicyValueMultiForm> dialog = createCargoSubPolicyValueMultiDialog(title, UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy));
		dialog.getContentComponent().loadData(parentSubPolicy);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void updateCargoSubPolicyAction(CargoSubPolicy<?> parentSubPolicy, CargoDetails selectedCargoDetails) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyValueUpdateActionTitle");
		CargoSubPolicyValueDialog dialog = new CargoSubPolicyValueDialog(title, UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, selectedCargoDetails));
		initDialog(dialog, UIConstants.UIACTION_Update);
		dialog.loadData(parentSubPolicy, selectedCargoDetails);
		dialog.enableEdit();
		dialog.open();
	}
	
	public void deleteCargoSubPolicyAction(CargoSubPolicy<?> parentSubPolicy, CargoDetails selectedCargoDetails) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyValueDeleteActionTitle");
		DSConfirmDialog dialog = createConfirmDeleteDialog(title);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, selectedCargoDetails));
		dialog.open();
	}
	
	public void undeleteCargoSubPolicyAction(CargoSubPolicy<?> parentSubPolicy, CargoDetails selectedCargoDetails) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyValueUndeleteActionTitle");
		DSConfirmDialog dialog = createConfirmUndeleteDialog(title);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, selectedCargoDetails));
		dialog.open();
	}
	
	public void viewCargoSubPolicyAction(CargoSubPolicy<?> parentSubPolicy, CargoDetails selectedCargoDetails) {
		String title = uiHelper.getSessionLabels().getString("CargoSubPolicyValueViewActionTitle");
		CargoSubPolicyValueDialog dialog = new CargoSubPolicyValueDialog(title, UIConstants.UIACTION_View);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, selectedCargoDetails));
		initDialog(dialog, UIConstants.UIACTION_View);
		dialog.loadData(parentSubPolicy, selectedCargoDetails);
		dialog.open();
	}
	
	private void reopen(PolicyTransaction policyTransaction) {
		try {
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.CargoSubPolicyNode, null);
		} catch (Exception e) {
			uiHelper.notificationException(e);
			return;
		}
	}

	@Override
	protected void onAction(DSUpdateDialog.ActionEvent event) {
		@SuppressWarnings("unchecked")
		DSUpdateDialog<CargoSubPolicyValueMultiForm> dialog = event.getSource();

		if (UIConstants.UIACTION_Add.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit()) {
				dialog.getContentComponent().addAction(true);
			}
		} else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit()) {
				CargoSubPolicy<?> subPolicy = (CargoSubPolicy<?>) dialog.getDataStore().getData1();

				boolean showWarning = false; // Default to silence
				Double tot = getTotalExposure(subPolicy, dialog.getContentComponent().getAllEntries());
				if (tot != null && tot > 100) {
					// Total exposure > 100% is an error
					final String msg = uiHelper.getSessionLabels().getString("CargoSubPolicyValueExposureNE100Message");
					uiHelper.notificationError(msg);

					return; // Reject the edit
				} else if (tot != null && tot < 100) {
					// Determine whether or not to show a warning
					final String productCode = configUIHelper.getProductCode();
					if ("TOWING".equals(productCode)) {
						// No warning: Customers' Personal Effects will automatically be adjusted so the
						// total is 100%
						showWarning = false;
					} else {
						showWarning = true;
					}
				}

				if (dialog.getContentComponent().saveEdit()) {
					createCargoValueTaskProcessor(dialog, showWarning).start(createCargoValueProcessor(
							UIConstants.UIACTION_SaveNew, subPolicy, dialog.getContentComponent().getAllData()));
				}
			}
		}
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		CargoSubPolicyValueDialog dialog = (CargoSubPolicyValueDialog) event.getSource();
		CargoSubPolicy<?> subPolicy = (CargoSubPolicy<?>) dialog.getDataStore().getData1();
		CargoDetails cargoDetails = (CargoDetails) dialog.getDataStore().getData2();

		if (dialog.validate()) {
			boolean showWarning = false; // Default to silence
			Double tot = getTotalExposure(subPolicy, cargoDetails, dialog.getCargoSubPolicyValueForm());
			if (tot != null && tot > 100) {
				// Total exposure > 100% is an error
				final String msg = uiHelper.getSessionLabels().getString("CargoSubPolicyValueExposureNE100Message");
				uiHelper.notificationError(msg);

				return; // Reject the edit
			} else if (tot != null && tot < 100) {
				// Determine whether or not to show a warning
				final String productCode = configUIHelper.getProductCode();
				if ("TOWING".equals(productCode)) {
					// No warning: Customers' Personal Effects will automatically be adjusted so the
					// total is 100%
					showWarning = false;
				} else {
					showWarning = true;
				}
			}

			if (dialog.save(cargoDetails)) {
				createCargoValueTaskProcessor(dialog, showWarning)
						.start(createCargoValueProcessor(UIConstants.UIACTION_Save, subPolicy, cargoDetails));
			}
		}
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		CargoSubPolicy<?> parentSubPolicy = (CargoSubPolicy<?>) dialog.getDataStore().getData1();
		CargoDetails selectedCargoDetails = (CargoDetails) dialog.getDataStore().getData2();

		boolean showWarning = false; // Default to silence
		Double tot = getTotalExposureForDelete(parentSubPolicy, selectedCargoDetails);
		if (tot != null && tot > 100) {
			// Total exposure > 100% is an error
			final String msg = uiHelper.getSessionLabels().getString("CargoSubPolicyValueExposureNE100Message");
			uiHelper.notificationError(msg);

			return; // Reject the edit
		} else if (tot != null && tot < 100) {
			// Determine whether or not to show a warning
			final String productCode = configUIHelper.getProductCode();
			if ("TOWING".equals(productCode)) {
				// No warning: Customers' Personal Effects will automatically be adjusted so the
				// total is 100%
				showWarning = false;
			} else {
				showWarning = true;
			}
		}

		createCargoValueTaskProcessor(dialog, showWarning)
				.start(createCargoValueProcessor(UIConstants.UIACTION_Delete, parentSubPolicy, selectedCargoDetails));
	}

	@Override
	protected void onUndelete(ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		CargoSubPolicy<?> parentSubPolicy = (CargoSubPolicy<?>) dialog.getDataStore().getData1();
		CargoDetails selectedCargoDetails = (CargoDetails) dialog.getDataStore().getData2();

		createCargoValueTaskProcessor(dialog)
				.start(createCargoValueProcessor(UIConstants.UIACTION_Undelete, parentSubPolicy, selectedCargoDetails));
	}

	protected CargoSubPolicyValueProcessor createCargoValueProcessor(String action,
												CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		return new CargoSubPolicyValueProcessor(action, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											subPolicy, 
											cargoDetails);
	}

	protected CargoSubPolicyValueProcessor createCargoValueProcessor(String action,
												CargoSubPolicy<?> subPolicy, 
												List<CargoDetails> cargoDetailsList) {
		return new CargoSubPolicyValueProcessor(action, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											subPolicy, 
											cargoDetailsList);
	}
	
	protected TaskProcessor createCargoValueTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					CargoSubPolicyValueProcessor processor = (CargoSubPolicyValueProcessor)taskRun.getProcessor();
					if (dialog != null) {
						dialog.close();
					}
					
					reopen(processor.getPolicyTransaction());
				}
				
				return true;
			}
			
		};
	}

	protected TaskProcessor createCargoValueTaskProcessor(IDSDialog dialog, boolean showTotalExposureWarning) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					CargoSubPolicyValueProcessor processor = (CargoSubPolicyValueProcessor) taskRun.getProcessor();
					if (dialog != null) {
						dialog.close();
					}

					reopen(processor.getPolicyTransaction());

					// If requested, show the total exposure != 100% warning. Do this after we save
					// and reopen the policy so the warning is not covered by the progress bar
					if (showTotalExposureWarning) {
						final String msg = uiHelper.getSessionLabels()
								.getString("CargoSubPolicyValueExposureNE100Message");
						uiHelper.notificationWarning(msg);
					}
				}

				return true;
			}
		};
	}

	public List<LookupTableItem> getValueDescriptionSelections(String includeValueDescription,
																CargoSubPolicy<?> parentSubPolicy, 
																List<DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails>> newCargoDetailsList) {
		List<LookupTableItem> allItems = null;
		try {
			allItems = lookupUIHelper.getAllCVRateDesciptions();
		} catch (SpecialtyAutoPolicyException e) {
			uiHelper.notificationException(e);
		}
		if (allItems == null) {
			allItems = new ArrayList<LookupTableItem>();
		}

		List<String> desList = new ArrayList<String>();
		
		if (parentSubPolicy != null && parentSubPolicy.getCargoDetails() != null) {
			parentSubPolicy.getCargoDetails().stream()
				.filter(o -> (o.isActive() || o.isPending()))
				.forEach(o -> {
				if (includeValueDescription == null || 
					o.getCargoDescription() == null ||
					!includeValueDescription.equalsIgnoreCase(o.getCargoDescription())) {
					desList.add(o.getCargoDescription());
				}
			});
		}

		if (newCargoDetailsList != null && !newCargoDetailsList.isEmpty()) {
			newCargoDetailsList.stream().forEach(o -> {
				if (includeValueDescription == null || 
					o.getForm().getValueDescriptionField().getValue() == null ||
					!includeValueDescription.equalsIgnoreCase(o.getForm().getValueDescriptionField().getValue())) {
					desList.add(o.getForm().getValueDescriptionField().getValue());
				}
			});
		}
		
		allItems.removeIf(o -> desList.contains(o.getItemKey()));
		
		return allItems;
	}
	
	// New Cargo Value
	private Double getTotalExposure(CargoSubPolicy<?> parentSubPolicy, 
						List<DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails>> newCargoDetailsList) {
		Double result = uiHelper.roundNumeric(
								parentSubPolicy.getCargoDetails().stream()
												.filter(o -> (o.isActive() || o.isPending()))
												.filter(o -> !UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getCargoDescription()))
												.collect(Collectors.toList())
												.stream().mapToDouble(CargoDetails::getCargoExposure).sum()
							)
						/*new BigDecimal(parentSubPolicy.getCargoDetails().stream()
								.filter(o -> !DSUIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getCargoDescription()))
								.collect(Collectors.toList())
								.stream().mapToDouble(CargoDetails::getCargoExposure).sum()
								).setScale(2, BigDecimal.ROUND_HALF_UP)
								*/
								;
		//Double result2 = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		List<DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails>> list2 = newCargoDetailsList.stream()
								.filter(o -> !UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getForm().getValueDescriptionField().getValue()))
								.collect(Collectors.toList());
		for(DSMultiNewEntryDto<CargoSubPolicyValueForm, CargoDetails> dto : list2) {
			if (dto.getForm().getExposureField().getModelValue() != null) {
				result = result + dto.getForm().getExposureField().getModelValue();
			}
		}
		
		return uiHelper.roundNumeric(result);
	}
	
	// Update Cargo Value
	private Double getTotalExposure(CargoSubPolicy<?> parentSubPolicy, CargoDetails cargoDetails, CargoSubPolicyValueForm form) {
		Double result = uiHelper.roundNumeric(
								parentSubPolicy.getCargoDetails().stream()
												.filter(o -> (o.isActive() || o.isPending()))
												.filter(o -> !UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getCargoDescription()) &&
															 o.getCargoDetailsPK().longValue() != cargoDetails.getCargoDetailsPK().longValue())
												.collect(Collectors.toList())
												.stream().mapToDouble(CargoDetails::getCargoExposure).sum()
							)
						/*new BigDecimal(parentSubPolicy.getCargoDetails().stream()
								.filter(o -> !DSUIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getCargoDescription()) &&
											 o.getCargoDetailsPK().longValue() != cargoDetails.getCargoDetailsPK().longValue())
								.collect(Collectors.toList())
								.stream().mapToDouble(CargoDetails::getCargoExposure).sum()
								).setScale(2, BigDecimal.ROUND_HALF_UP)
								*/ 
							;
		//BigDecimal result2 = new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP);
		if (form.getExposureField().getModelValue() != null) {
			result = result + form.getExposureField().getModelValue();
		}
		
		return uiHelper.roundNumeric(result);
	}
	
	// Delete cargo value
	private Double getTotalExposureForDelete(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		// Calculate total exposure, less the exposure of the (out of date) Customers'
		// Personal Effects
		Double result = uiHelper.roundNumeric(subPolicy.getCargoDetails().stream()
				.filter(o -> o.isActive() || o.isPending())
				.filter(o -> !UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getCargoDescription()))
				.mapToDouble(CargoDetails::getCargoExposure)
				.sum());

		// Remove exposure of to-be-deleted cargo value
		result -= cargoDetails.getCargoExposure();

		return uiHelper.roundNumeric(result);
	}

	public CargoDetails createTotalRow(CargoSubPolicy<?> parentSubPolicy) {
		CargoDetails result = null;
		
		if (parentSubPolicy.getCargoDetails() != null && !parentSubPolicy.getCargoDetails().isEmpty()) {
			result = new CargoDetails();
			result.setDescription(UIConstants.CARGOVALUE_TOTAL);
			result.setCargoDescription(uiHelper.getSessionLabels().getString("CargoSubPolicyValueTotalPremium"));
			result.setCargoPartialPremiums(parentSubPolicy.getTotalCargoPartialPremium());
		}
		
		return result;
	}
}
