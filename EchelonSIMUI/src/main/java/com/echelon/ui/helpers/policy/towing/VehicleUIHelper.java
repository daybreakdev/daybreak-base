package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper.NotificatonCallback;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.domain.policy.specialtylines.VehicleRateGroup;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.towing.VehicleForm;
import com.echelon.ui.components.towing.VehicleList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.towing.VehicleProcessor;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.vaadin.flow.component.AbstractField;
import com.vaadin.flow.component.UI;

public class VehicleUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4127595188814038214L;

	public interface LoadVehicleDataCallback{
				public void loadComplete();
			};

	public void loadView(VehicleList component, DSTreeGridItem nodeItem) {
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) nodeItem.getNodeObject();
		if (autoSubPolicy != null) {
			component.loadData(autoSubPolicy, autoSubPolicy.getSubPolicyRisks());
		}
		else {
			component.loadData(autoSubPolicy, new HashSet<SpecialtyVehicleRisk>());
		}
	}
	
	public void loadView(VehicleForm component, SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		component.loadData(autoSubPolicy, vehicle);
	}
	
	public VehicleForm createVehicleForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		String productCd = configUIHelper.getCurrentProductConfig() != null ? configUIHelper.getCurrentProductConfig().getProductCd() : null;
		VehicleForm form = UIFactory.getInstance().createVehicleForm(action, containerType, productCd);
		return form;
	}
	
	protected DSUpdateDialog<VehicleForm> createVehicleDialog(String title, String action) {
		DSUpdateDialog<VehicleForm> dialog = (DSUpdateDialog<VehicleForm>) createDialog(title, createVehicleForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupVehicleAction(String title, String action, SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		DSUpdateDialog<VehicleForm> dialog = createVehicleDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, vehicle));
		dialog.getContentComponent().loadData(autoSubPolicy, vehicle);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newVehicleAction(SpecialityAutoSubPolicy autoSubPolicy) {
		createVehicleTaskProcessor(null).start(
				createVehicleProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										autoSubPolicy, null)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<VehicleForm> dialog = (DSUpdateDialog<VehicleForm>) event.getSource();
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(vehicle)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			messageOnSaveVehicle(vehicle, null, new DSNotificatonUIHelper.NotificatonCallback() {
				
				@Override
				public void close() {
					createVehicleTaskProcessor(dialog).start(
							createVehicleProcessor(UIConstants.UIACTION_SaveNew, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											null, vehicle)
							);
				}
			});
		}
	}

	public void updateVehicleAction(SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		String title = uiHelper.getSessionLabels().getString("VehicleUpdateDialogTitle");
		setupVehicleAction(title, UIConstants.UIACTION_Update, autoSubPolicy, vehicle);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<VehicleForm> dialog = (DSUpdateDialog<VehicleForm>) event.getSource();
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData2();
		String origVehDesc = vehicle.getVehicleDescription();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(vehicle)) {
			
			messageOnSaveVehicle(vehicle, origVehDesc, new DSNotificatonUIHelper.NotificatonCallback() {
				
				@Override
				public void close() {
					createVehicleTaskProcessor(dialog).start(
							createVehicleProcessor(UIConstants.UIACTION_Save, 
													policyUIHelper.getCurrentPolicyTransaction(), 
													null, vehicle)
							);
				}
			});
		}
	}

	public void copyVehicleAction(SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		createVehicleTaskProcessor(null).start(
				createVehicleProcessor(UIConstants.UIACTION_Copy, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										autoSubPolicy, vehicle)
				);
	}
	
	@Override
	protected void onSaveCopy(SaveEvent event) {
		DSUpdateDialog<VehicleForm> dialog = (DSUpdateDialog<VehicleForm>) event.getSource();
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(vehicle)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			messageOnSaveVehicle(vehicle, null, new DSNotificatonUIHelper.NotificatonCallback() {
				
				@Override
				public void close() {
					createVehicleTaskProcessor(dialog).start(
							createVehicleProcessor(UIConstants.UIACTION_SaveNew, 
													policyUIHelper.getCurrentPolicyTransaction(), 
													autoSubPolicy, vehicle)
							);			
				}
			});
		}
	}
	
	public void deleteVehicleAction(SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("VehicleDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, vehicle));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData2();
		createVehicleTaskProcessor(dialog).start(
				createVehicleProcessor(UIConstants.UIACTION_Delete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							autoSubPolicy,
							vehicle)
				);
	}
	
	public void undeleteVehicleAction(SpecialityAutoSubPolicy autoSubPolicy, SpecialtyVehicleRisk vehicle) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("VehicleUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(autoSubPolicy, vehicle));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialityAutoSubPolicy autoSubPolicy = (SpecialityAutoSubPolicy) dialog.getDataStore().getData1();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData2();
		createVehicleTaskProcessor(dialog).start(
				createVehicleProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							autoSubPolicy,
							vehicle)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, SpecialtyVehicleRisk vehicle, String action) {
		if (vehicle != null) { 
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Vehicle, vehicle);
		}
		else {
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.VehicleListNode, vehicle);
		}
	}
	
	public boolean isVehicleGroup(Integer vehicleNumberOfUnits) {
		return vehicleNumberOfUnits != null && vehicleNumberOfUnits > 1;
	}
	
	@SuppressWarnings("rawtypes")
	public void setVehicleRateGroupFields(AbstractField unitYearField, AbstractField unitLPNField,
											AbstractField impliedRateGroupField, AbstractField selectedRateGroupField) {
		Integer vehYear = null;
		Double	unitLPN = null;
		
		try {
			if (unitYearField != null && unitYearField.getValue() != null) {
				if (unitYearField.getValue() instanceof Integer) {
					vehYear = (Integer) unitYearField.getValue();
				}
				else if (unitYearField.getValue() instanceof String) {
					String str = (String) unitYearField.getValue();
					vehYear = Integer.valueOf(str.replaceAll(",", ""));
				}
			}
		} catch (Exception e) {
		}
				
		try {
			if (unitLPNField != null && unitLPNField.getValue() != null) {
				String str = (String) unitLPNField.getValue();
				unitLPN = Double.valueOf(str.replaceAll(",", ""));
			}
		} catch (Exception e) {
		}
		
		Integer vehRateGroup = getImpliedRateGroup(vehYear, unitLPN);
		
		updateVehicleRateGroupFields(impliedRateGroupField, selectedRateGroupField, vehRateGroup);
	}

	protected Integer getImpliedRateGroup(Integer vehYear, Double unitLPN) {
		if (unitLPN != null) {
			try {
				List<VehicleRateGroup> list = getSpecialtyAutoPolicyProcessing().findVehicleRateGroup(vehYear, unitLPN, unitLPN);
				if (list != null && !list.isEmpty()) {
					return list.get(0).getImpliedRateGroup();
				}
			} catch (SpecialtyAutoPolicyException e) {
				// TODO Auto-generated catch block
				uiHelper.notificationException(e);
			}
		}
		
		return null;
	}
	
	protected void updateVehicleRateGroupFields(AbstractField impliedRateGroupField, AbstractField selectedRateGroupField, Integer vehRateGroup) {		
		if (impliedRateGroupField != null) {
			if (vehRateGroup != null) {
				uiHelper.setFieldValue(impliedRateGroupField, String.valueOf(vehRateGroup).replaceAll(",", ""));
			}
			else {
				uiHelper.setFieldValue(impliedRateGroupField, null);
			}
			
		}
		if (selectedRateGroupField != null) {
			if (vehRateGroup != null) {
				uiHelper.setFieldValue(selectedRateGroupField, String.valueOf(vehRateGroup).replaceAll(",", ""));
			}
			else {
				uiHelper.setFieldValue(selectedRateGroupField, null);
			}
		}		
	}
	
	public List<SpecialtyVehicleRisk> sortVehicleList(Set<SpecialtyVehicleRisk> vehicles) {
		List<SpecialtyVehicleRisk> result = new ArrayList<SpecialtyVehicleRisk>(); 
		if (vehicles != null) {
			result.addAll(vehicles);
		}
		
		return result.stream().sorted(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence, 
										Comparator.nullsLast(Comparator.naturalOrder())))
					 	.collect(Collectors.toList());
	}
	
	protected VehicleProcessor createVehicleProcessor(String action, PolicyTransaction policyTransaction, 
									SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy,
									SpecialtyVehicleRisk vehicle) {
		return new VehicleProcessor(action,  
										policyTransaction, 
										autoSubPolicy, vehicle);
	}
	
	protected TaskProcessor createVehicleTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					VehicleProcessor processor = (VehicleProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupVehicleAction(uiHelper.getSessionLabels().getString("VehicleNewDialogTitle"), 
											UIConstants.UIACTION_New, processor.getAutoSubPolicy(), processor.getVehicle());
						
						break;
						
					case UIConstants.UIACTION_Copy:
						setupVehicleAction(uiHelper.getSessionLabels().getString("VehicleCopyDialogTitle"), 
											UIConstants.UIACTION_Copy, processor.getAutoSubPolicy(), processor.getVehicle());
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						if (processor.getVehicle() != null && processor.getVehicle().isNew()) { // it was new
							reopen(processor.getPolicyTransaction(), null, UIConstants.UIACTION_Delete);
						}
						else {
							reopen(processor.getPolicyTransaction(), processor.getVehicle(), UIConstants.UIACTION_Delete);
						}
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}
	
	public String getTreeNodeText(SpecialtyVehicleRisk vehicle) {
		String result = "";
		
		if (vehicle != null) {
			result = (vehicle.getVehicleSequence() != null ? vehicle.getVehicleSequence()+" : " : "")
				   + vehicle.getDisplayString();
		}
		
		return result;
	}
	
	public String buildVehiclesLabel(Set<IBusinessEntity> vehicleEntityies) {
		StringBuffer sb = new StringBuffer();
		
		Set<SpecialtyVehicleRisk> vehicleSet = new HashSet<SpecialtyVehicleRisk>();
		for(IBusinessEntity entity : vehicleEntityies) {
			if (entity instanceof SpecialtyVehicleRisk) {
				vehicleSet.add((SpecialtyVehicleRisk)entity);
			}
		}
		
		if (!vehicleSet.isEmpty()) {
			VehicleUIHelper vehicleUIHelper = new VehicleUIHelper();
			List<SpecialtyVehicleRisk> vehicles = vehicleUIHelper.sortVehicleList(vehicleSet);
			
			vehicles.stream().forEach(o -> {
				String vehDes = vehicleUIHelper.getTreeNodeText(o);
				if (vehDes != null && !vehDes.trim().isEmpty()) {
					if (sb.length() > 0) {
						sb.append(", ");
					}
					sb.append(vehDes);
				}
			});
		}
		
		return sb.toString();
	}

	public void loadVehicleData(LoadVehicleDataCallback callback,
									SpecialityAutoSubPolicy autoSubPolicy,
									SpecialtyVehicleRisk vehicleRisk) {
		if (policyUIHelper.needToLoadSubPolicyRiskData(vehicleRisk)) {
			(new TaskProcessor(UI.getCurrent(), null) {
	
				@Override
				protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
					if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
						VehicleProcessor processor = (VehicleProcessor) taskRun.getProcessor();
						if (callback != null) {
							callback.loadComplete();
						}
					}
					
					return true;
				}
					
			})
			.start(
					new VehicleProcessor(UIConstants.UIACTION_LoadData, policyUIHelper.getCurrentPolicyTransaction(), autoSubPolicy, vehicleRisk)
				);
		}
		else {
			callback.loadComplete();
		}
	}

	private void messageOnSaveVehicle(SpecialtyVehicleRisk vehicle, String origVehDesc, NotificatonCallback callback) {
		if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription()) &&
			(origVehDesc == null || !origVehDesc.equalsIgnoreCase(vehicle.getVehicleDescription()))) {
			uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnOPCF27BAdded"), callback);
		}
		else {
			callback.close();
		}
	}

	public Double getOriginatingVehicleUnitRate(SpecialtyVehicleRiskAdjustment adjustment) {
		final SpecialtyVehicleRisk parentVehicle = adjustment.getSpecialtyVehicleRisk();
		if (parentVehicle != null) {
			final ISpecialtyAutoPolicyProcessing specialtyAutoPolicyProcessing = getSpecialtyAutoPolicyProcessing();
			final double originatingUnitRate = specialtyAutoPolicyProcessing.getOriginatingUnitRate(parentVehicle);
			return originatingUnitRate;
		}
		
		return null;
	}
	
	public boolean isTrailer(String vehicleDecription) {
		return StringUtils.isNotBlank(vehicleDecription) &&
				SpecialtyAutoConstants.SPEC_VEH_DESC_TRAILERS.contains(vehicleDecription);
	}
}
