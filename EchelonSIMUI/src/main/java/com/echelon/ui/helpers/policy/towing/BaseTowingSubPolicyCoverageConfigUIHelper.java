package com.echelon.ui.helpers.policy.towing;

import com.ds.ins.prodconf.interfaces.ICoverageConfig;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseCoverageConfigUIHelper;
import com.echelon.utils.SpecialtyAutoConstants;

public class BaseTowingSubPolicyCoverageConfigUIHelper extends BaseCoverageConfigUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1014722670184553260L;

	public BaseTowingSubPolicyCoverageConfigUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isDetailRequired(ICoverageConfig coverageConfig) {
		// TODO Auto-generated method stub
		boolean result = super.isDetailRequired(coverageConfig);
		
		// Since they are not editable defined in config, we need to open the coverage for location seleciton
		if (!result) {
			return SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS.contains(coverageConfig.getCode())
					|| SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS.contains(coverageConfig.getCode())
					|| SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS.contains(coverageConfig.getCode())
					;
		}
		
		return result;
	}

}
