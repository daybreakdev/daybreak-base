package com.echelon.ui.helpers.entities;

import com.ds.ins.domain.entities.CommercialCustomer;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.processes.customer.CommercialCustomerProcessing;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.CancelEvent;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.utils.Constants;
import com.echelon.ui.components.entities.CommercialCustomerForm;
import com.echelon.ui.components.entities.CommercialCustomerView;
import com.echelon.ui.components.entities.CustomerForm;
import com.echelon.ui.components.entities.CustomerView;
import com.echelon.ui.constants.UIConstants;

public class CommercialCustomerUIHelper extends CustomerUIHelper<CommercialCustomer> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 770143372235579289L;

	@Override
	public Customer createEmptyCustomer() throws CommercialCustomerException {
		final Customer customer = new CommercialCustomerProcessing().createCommercialCustomer();
		CommercialCustomer commercialCustomer = customer.getCommercialCustomer();
		commercialCustomer.setPreferredLanguage(Constants.LANGUAGECODE_ENGLISH);
		return customer;
	}

	@Override
	public CustomerView<CommercialCustomer> createCustomerView(String action, UICONTAINERTYPES uiContainerType) {
		return new CommercialCustomerView(action, uiContainerType);
	}

	@Override
	public CustomerForm<CommercialCustomer> createCustomerForm(String action, UICONTAINERTYPES uiContainerType) {
		return new CommercialCustomerForm(action, uiContainerType);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<CustomerForm<CommercialCustomer>> dialog = event.getSource();
		final CustomerForm<CommercialCustomer> form = dialog.getContentComponent();
		if (form.validateEdit()) {
			dialog.setProcessStarted(); // Disable save & cancel buttons
			if (((CommercialCustomerForm) form).isRINEmpty()) {
				confirmUpdateCustomerEmptyRIN(dialog);
			} else {
				confirmUpdateCustomerAddress(dialog);
			}
		}
	}

	private void confirmUpdateCustomerEmptyRIN(DSUpdateDialog<CustomerForm<CommercialCustomer>> dialog) {
		final DSConfirmDialog confirmDialog = new DSConfirmDialog(null,
				uiHelper.getButtonLabel(UIConstants.UIACTION_Yes), this::onConfirmUpdateCustomerEmptyRIN,
				uiHelper.getButtonLabel(UIConstants.UIACTION_No), this::onCancelUpdateCustomerEmptyRIN,
				uiHelper.getSessionLabels().getString("CommercialCustomerRINEmptyWarnMessage"));
		confirmDialog.setDataStore(new DSDialogDataStore(dialog));
		confirmDialog.open();
	}

	private void onConfirmUpdateCustomerEmptyRIN(ConfirmEvent event) {
		final DSConfirmDialog dialog = event.getSource();
		dialog.close();

		final DSUpdateDialog<CustomerForm<CommercialCustomer>> updateDialog = castToUpdateDialog(
				dialog.getDataStore().getData1());
		confirmUpdateCustomerAddress(updateDialog);
	}

	private void onCancelUpdateCustomerEmptyRIN(CancelEvent event) {
		final DSConfirmDialog dialog = event.getSource();
		dialog.close();

		final DSUpdateDialog<CustomerForm<CommercialCustomer>> updateDialog = castToUpdateDialog(
				dialog.getDataStore().getData1());
		updateDialog.setProcessEnded();
	}

	@SuppressWarnings("unchecked")
	private DSUpdateDialog<CustomerForm<CommercialCustomer>> castToUpdateDialog(Object dialog) {
		return (DSUpdateDialog<CustomerForm<CommercialCustomer>>) dialog;
	}
}
