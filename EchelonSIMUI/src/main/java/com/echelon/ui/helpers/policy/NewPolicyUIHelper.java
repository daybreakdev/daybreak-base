package com.echelon.ui.helpers.policy;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.dto.NewPolicyDto;
import com.echelon.ui.components.baseclasses.dto.PolicySearchCriteria;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.baseclasses.process.policy.BaseNewPolicyProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicySearchProcessor;
import com.echelon.ui.components.policy.NewPolicyWizard;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.NEWPOLICYSTEPS;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.entities.CustomerUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.PersonalCustomerException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.processes.customer.CommercialCustomerProcessing;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class NewPolicyUIHelper<INSPCY extends InsurancePolicy, PCYVER extends PolicyVersion, PCYTXN extends PolicyTransaction<?>, SUBPCY extends SubPolicy<?>> extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -787602475677629826L;
	
	@SuppressWarnings("unchecked")
	protected IGenericPolicyProcessing<INSPCY, PCYVER, PCYTXN, SUBPCY> getPolicyProcessing() {
		return policyUIHelper.getPolicyProcessing();
	}

	protected InsurancePolicy createEmptyInsurancePolicy(InsurancePolicy insurancePolicy) {
		if (insurancePolicy != null && insurancePolicy.getClass().equals(InsurancePolicy.class)) {
			return insurancePolicy;
		}
		
		return new InsurancePolicy();
	}
	
	@SuppressWarnings("rawtypes")
	protected PolicyTransaction createEmptyPolicyTransaction() {
		PolicyVersion policyVersion = new PolicyVersion();
		PolicyTransaction policyTransaction = new PolicyTransaction();
		policyTransaction.setPolicyVersion(policyVersion);
		
		return policyTransaction;
	}
	
	protected NewPolicyDto createEmptyPolicyAndCustomert(NewPolicyDto newPolicyDto)
			throws CommercialCustomerException, PersonalCustomerException, SpecialtyAutoPolicyException {
		newPolicyDto = createEmptyCustomert(newPolicyDto);
		if (newPolicyDto.getPolicyTransaction() == null) {
			newPolicyDto.setPolicyTransaction(createEmptyPolicyTransaction());
		}
		newPolicyDto.setInsurancePolicy(createEmptyInsurancePolicy(newPolicyDto.getInsurancePolicy()));
		newPolicyDto.getPolicyTransaction().getPolicyVersion().setInsurancePolicy(newPolicyDto.getInsurancePolicy());
		
		return newPolicyDto;
	}

	protected NewPolicyDto createEmptyNewPolicyDto(UIConstants.NEWPOLICYTYPES newPolicyType) {
		NewPolicyDto newPolicyDto = new NewPolicyDto(newPolicyType);
		return newPolicyDto;
	}

	protected boolean saveNewPolicyAction(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		// Save policy parameters
		if (!dialog.saveEdit(newPolicyDto)) {
			return false;
		}
		
		startSavingNewPolicyProcesses(dialog, newPolicyDto);
		
		return true;
	}

	protected boolean savePolicyEdit(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		boolean needToSavePolicy = StringUtils.isNotBlank(newPolicyDto.getExternalQuoteNumber());
		return needToSavePolicy;
	}
	
	protected boolean saveCustomerEdit(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		try {
			if (!dialog.saveEdit(newPolicyDto.getCustomer(), newPolicyDto.getAddress(), newPolicyDto.getPolicyTransaction())) {
				return false;
			}
		} catch (Exception e) {
			uiHelper.notificationException(e);
			return false;
		}
		
		return true;
	}
	
	protected boolean saveSubPoliciesEdit(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		return true;
	}
	
	@SuppressWarnings("rawtypes")
	protected BaseNewPolicyProcessor createPolicyProcessor(String action, NewPolicyDto newPolicyDto, NEWPOLICYSTEPS step) {
		return new BaseNewPolicyProcessor(action, 
								newPolicyDto.getPolicyTransaction(), 
								new CommercialCustomerProcessing(), 
								newPolicyDto, step);
	}

	// Steps to save:
	// 1. Creating Policy and Save key policy data; the policy processing includes customer and address
	// 2. Saving new policy,
	// 2.2 save UI values if necessary
	// 2.1 save additional policy data if necessary
	// 3. Saving Customer 
	// 3.1 save UI Customer, Address, Liability Limit values
	// 3.2 save Customer, Address
	// 4. Processor to create SubPolicies, including save Liability Limit values
	// 5. Saving new subpolicies
	// 5.1 save UI SubPolicies
	// 5.2 save SubPolicies
	// 6. Updating subpolicies
	// 7. Open new policy
	protected void startSavingNewPolicyProcesses(NewPolicyWizard dialog, NewPolicyDto newPolicyDto) {
		// #1 Create policy
		createPolicyTaskProcessor(dialog).startFirst(
				createPolicyProcessor(UIConstants.UIACTION_New, newPolicyDto, NEWPOLICYSTEPS.Policy) // this include customer and adddress
			);
	}

	@SuppressWarnings("serial")
	protected TaskProcessor createPolicyTaskProcessor(NewPolicyWizard dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@SuppressWarnings("rawtypes")
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BaseNewPolicyProcessor processor = (BaseNewPolicyProcessor)taskRun.getProcessor();
					
					String processAction = processor.getProcessAction();
					NEWPOLICYSTEPS processStep = processor.getStep();
					
					// #1 Complete creating policy
					// #2
					if (UIConstants.UIACTION_New.equals(processAction) && NEWPOLICYSTEPS.Policy.equals(processStep)) {
						// #2.1
						boolean needToSavePolicy = savePolicyEdit(dialog, processor.getNewPolicyDto());
					
						// 2.2
						if (needToSavePolicy) {
							createPolicyTaskProcessor(dialog).startNext(
									createPolicyProcessor(UIConstants.UIACTION_SaveNew, processor.getNewPolicyDto(), NEWPOLICYSTEPS.Policy), 
									this.getTaskProcessInfo());
							
							// not to continue;
							return true;
						}
						else {
							// go to next step #3 (Complete saving new policy)
							processAction = UIConstants.UIACTION_SaveNew;
							processStep   = NEWPOLICYSTEPS.Policy;
						}
					}
					
					// #2 Complete saving new policy
					// #3
					if (UIConstants.UIACTION_SaveNew.equals(processAction) && NEWPOLICYSTEPS.Policy.equals(processStep)) {
						// 3.1
						saveCustomerEdit(dialog, processor.getNewPolicyDto());
					
						// 3.2
						createPolicyTaskProcessor(dialog).startNext(
								createPolicyProcessor(UIConstants.UIACTION_SaveNew, processor.getNewPolicyDto(), NEWPOLICYSTEPS.Customer), 
								this.getTaskProcessInfo());
					}
					
					// #3 Complete saving customer
					// #4
					else if (UIConstants.UIACTION_SaveNew.equals(processAction) && NEWPOLICYSTEPS.Customer.equals(processStep)) {
						createPolicyTaskProcessor(dialog).startNext(
								createPolicyProcessor(UIConstants.UIACTION_New, processor.getNewPolicyDto(), NEWPOLICYSTEPS.CreateNewSubPolices), 
								this.getTaskProcessInfo());
						
					}
					
					// #4 Complete creating new subpolicies
					// #5
					else if (UIConstants.UIACTION_New.equals(processAction) && NEWPOLICYSTEPS.CreateNewSubPolices.equals(processStep)) {
						// #5.1
						saveSubPoliciesEdit(dialog, processor.getNewPolicyDto());
						
						// #5.2
						createPolicyTaskProcessor(dialog).startNext(
								createPolicyProcessor(UIConstants.UIACTION_New, processor.getNewPolicyDto(), NEWPOLICYSTEPS.SaveNewSubPolices), 
								this.getTaskProcessInfo());
					}
					
					// #5 Complete saving new subpolicies
					// #6
					else if (UIConstants.UIACTION_New.equals(processAction) && NEWPOLICYSTEPS.SaveNewSubPolices.equals(processStep)) {
						createPolicyTaskProcessor(dialog).startLast(
								createPolicyProcessor(UIConstants.UIACTION_SaveNew, processor.getNewPolicyDto(), NEWPOLICYSTEPS.UpdateSubPolices), 
								this.getTaskProcessInfo());
					}
					
					// #6 Complete updating subpolicies
					// #7
					else if (UIConstants.UIACTION_SaveNew.equals(processAction) && NEWPOLICYSTEPS.UpdateSubPolices.equals(processStep)) {
						dialog.close();
						policyUIHelper.reopenPolicy(processor.getPolicyTransaction(), POLICYTREENODES.Policy, null);
					}
				}
				
				return true;
			}
			
		};
	}
	
	public boolean validateQuoteNumberExist(String productCode, String quoteNumber) {
		if (StringUtils.isNotBlank(quoteNumber)) {
			PolicySearchCriteria criteria = new PolicySearchCriteria();
			criteria.setQuoteNum(quoteNumber);
			criteria = runSearch(productCode, criteria);
			if (criteria != null && criteria.getSearchResults() != null && !criteria.getSearchResults().isEmpty()) {
				return !criteria.getSearchResults().stream().filter(o -> quoteNumber.equalsIgnoreCase(o.getQuoteNum())).findFirst().isPresent();
			}
		}
		
		return true;
	}
	
	public boolean validatePolicyNumberExist(String productCode, String policyNumber) {
		if (StringUtils.isNotBlank(policyNumber)) {
			PolicySearchCriteria criteria = new PolicySearchCriteria();
			criteria.setPolicyNum(policyNumber);
			criteria = runSearch(productCode, criteria);
			if (criteria != null && criteria.getSearchResults() != null && !criteria.getSearchResults().isEmpty()) {
				return !criteria.getSearchResults().stream().filter(o -> policyNumber.equalsIgnoreCase(o.getPolicyNum())).findFirst().isPresent();
			}
		}
		
		return true;
	}
	
	public boolean validateQuoteNumberPrefix(String productCode, String quoteNumber) {
		if (StringUtils.isNotBlank(quoteNumber)) {
			String prefix = policyUIHelper.getQuoteNumberPrefix(productCode);
			if (StringUtils.isNotBlank(prefix)) {
				return quoteNumber.startsWith(prefix);
			}
		}
		
		return true;
	}
	
	public boolean validatePolicyNumberPrefix(String productCode, String policyNumber) {
		if (StringUtils.isNotBlank(policyNumber)) {
			String prefix = policyUIHelper.getPolicyNumberPrefix(productCode);
			if (StringUtils.isNotBlank(prefix)) {
				return policyNumber.startsWith(prefix);
			}
		}
		
		return true;
	}
	
	private PolicySearchCriteria runSearch(String productCode, PolicySearchCriteria criteria) {
		if (StringUtils.isNotBlank(productCode)) {
			criteria = ((BasePolicySearchProcessor)(new BasePolicySearchProcessor(criteria)).withProcessorValues(
								BrokerUI.getUserSession().getUserProfile(), 
								null, 
								policyUIHelper.getPolicyProductProcessing(productCode))).runSearch(criteria);
		}
		
		return criteria;
	}
	
	public void onProductCodeChange(NewPolicyDto newPolicyDto) throws Exception {
		if (configUIHelper.getCurrentProductConfig() != null) {
			newPolicyDto = createEmptyPolicyAndCustomert(newPolicyDto);
			
			if (newPolicyDto.getHasAutoPolicyField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_AUTO) == null) {
				//newPolicyDto.setHasAutomobileSubPolicy(false);
				uiHelper.setFieldValue(newPolicyDto.getHasAutoPolicyField(), Boolean.FALSE);
				//newPolicyDto.getHasAutomobileSubPolicyBinding().setAsRequiredEnabled(false);
			}
			
			if (newPolicyDto.getHasCommGeneralLiabilityField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY) == null) {
				//newPolicyDto.setHasCGLSubPolicy(false);
				uiHelper.setFieldValue(newPolicyDto.getHasCommGeneralLiabilityField(), Boolean.FALSE);
			}
			
			if (newPolicyDto.getHasMotorTruckCargoField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_CARGO) == null) {
				//newPolicyDto.setHasCargoSubPolicy(false);
				uiHelper.setFieldValue(newPolicyDto.getHasMotorTruckCargoField(), Boolean.FALSE);
			}
			
			if (newPolicyDto.getHasGarageField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE) == null) {
				//newPolicyDto.setHasGarageSubPolicy(false);
				uiHelper.setFieldValue(newPolicyDto.getHasGarageField(), Boolean.FALSE);
			}
			
			if (newPolicyDto.getHasCommercialPropertyField() != null &&
				configUIHelper.getCurrentProductConfig().getSubPolicyConfig(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY) == null) {
				//newPolicyDto.setHasCommercialPropertySubPolicy(false);
				uiHelper.setFieldValue(newPolicyDto.getHasCommercialPropertyField(), Boolean.FALSE);
			}
		}
	}
	
	public NewPolicyDto createEmptyCustomert(NewPolicyDto newPolicyDto) throws CommercialCustomerException, PersonalCustomerException {
		final CustomerUIHelper<?> customerUIHelper = UIFactory.getInstance().getCustomerUIHelper(newPolicyDto.getCustomerType());
		if (newPolicyDto.getCustomer() == null) {
			newPolicyDto.setCustomer(customerUIHelper.createEmptyCustomer());
		}
		if (newPolicyDto.getAddress() == null) {
			newPolicyDto.setAddress(customerUIHelper.createEmptyAddress());
		}
		
		return newPolicyDto;
	}
}
