package com.echelon.ui.helpers.policy.towing;

import java.util.List;
import java.util.stream.Collectors;

import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.towing.CargoSubPolicyLocCovMultiForm;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;

import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;

public class CargoSubPolicyCoverageUIHelper extends BaseTowingSubPolicyCoverageUIHelper<CargoSubPolicyLocCovMultiForm> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6735804009080805918L;

	public CargoSubPolicyCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.CGOSubPolicy, POLICYTREENODES.CargoSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSUpdateDialog<?> openLocCovDialog(String title, String action, ICoverageConfigParent covConfigParent,
			IBusinessEntity covParent, CoverageDto coverageDto, boolean isMultiple, CoverageEntryCallback callback,
			List<CoverageDto> newCoverages) {
		
		DSUpdateDialog<?> dialog = super.openLocCovDialog(title, action, covConfigParent, covParent, coverageDto, isMultiple, callback,
															newCoverages);
		if (UIConstants.UIACTION_New.equals(action)) {
			if (SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC.equalsIgnoreCase(coverageDto.getCoverageCode()) &&
					BrokerUI.getUserSession() != null &&
					BrokerUI.getUserSession().getCurrentPolicy() != null &&
					BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyRisks()
				.stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
				.collect(Collectors.toList()).size() > 1) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("CargoSubPolicyCoverageNewMTTCWarn"));
			}
		}
		
		return dialog;
	}
	
	
	//ESL-2002
	@SuppressWarnings("unchecked")
	@Override
	protected void reopenCoverageList(PolicyTransaction<?> policyTransaction, IBusinessEntity covParent, CoverageDto coverageDto, String action) {
		//If CTC endorsement exists, output notification
		if (UIConstants.UIACTION_Save.equals(action)) {
			CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
			UIFactory.getInstance().getSpecialtySubPolicyContentUIHelper(SpecialtyAutoConstants.SUBPCY_CODE_CARGO).loadSubpolicyData(
					() -> {
						if (cargoSubPolicy.getCoverageDetail().getEndorsementByCode(SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC) != null) {
							uiHelper.notificationMessage(uiHelper.getSessionLabels().getString("CargoSubPolicyCoverageUpdateNotificationLabel"));
						}
						super.reopenCoverageList(policyTransaction, covParent, coverageDto, action);
					}
					, cargoSubPolicy);
		}
		else {
			super.reopenCoverageList(policyTransaction, covParent, coverageDto, action);
		}
	}
}
