package com.echelon.ui.helpers.policy.towing;

import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;

public class AutoSubPolicyCoverageUIHelper extends BaseTowingSubPolicyCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6735804009080805918L;

	public AutoSubPolicyCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.AUTOSubPolicy, POLICYTREENODES.AutomobileSubPolicyNode);
		// TODO Auto-generated constructor stub
	}
}
