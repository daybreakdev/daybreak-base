package com.echelon.ui.helpers.policy.towing;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.prodconf.baseclasses.CoverageConfigDto;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper;
import com.ds.ins.uicommon.helpers.DSNotificatonUIHelper.NotificatonCallback;
import com.ds.ins.utils.Constants;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.dto.TotalPremiumDto;
import com.echelon.ui.components.baseclasses.helpers.policy.BaseSubPolicyRiskCoverageUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCoveragePicker;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class VehicleCoverageUIHelper extends BaseSubPolicyRiskCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4948532834144130677L;
	
	private static final List<String> MESSAGECHANGESACTIONS = Arrays.asList(UIConstants.UIACTION_Save, UIConstants.UIACTION_SaveNew, UIConstants.UIACTION_Delete, UIConstants.UIACTION_Undelete);
		
	public VehicleCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.Vehicle, UIConstants.POLICYTREENODES.Vehicle);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean validateCoverageSelections(DSUpdateDialog<BaseCoveragePicker> dialog,
			ICoverageConfigParent covConfigParent, IBusinessEntity covParent, List<CoverageConfigDto> selectedCoverages) {
		// TODO Auto-generated method stub
		boolean ok = super.validateCoverageSelections(dialog, covConfigParent, covParent, selectedCoverages);
		if (ok) {
			//VR-TOW-COV-19; warning
			if (selectedCoverages.stream().filter(o -> "EON20G".equals(o.getCoverageConfig().getCode()))
						.findFirst().isPresent()) {
				if (!selectedCoverages.stream().filter(o -> "AP".equals(o.getCoverageConfig().getCode()))
						.findFirst().isPresent()) {
					if (((SpecialtyVehicleRisk)covParent).getCoverageDetail().getActiveOrPendingCoverageByCode("AP") == null) {
						ok = false; // not to continue;
						uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("PolicyMessageVR-TOW-COV-19"),
								new DSNotificatonUIHelper.NotificatonCallback() {
	
									@Override
									public void close() {
										processNewCoverages(dialog, covConfigParent, covParent, selectedCoverages);
									}
							
								});
					}
				}
			}
		}
		
		return ok;
	}

	@Override
	protected Set<Coverage> getCoveragesForTotal(IBusinessEntity covParent, CoverageDetails coverageDetails) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
			return new HashSet<Coverage>();
		}
		
		return super.getCoveragesForTotal(covParent, coverageDetails);
	}

	@Override
	protected Set<Endorsement> getEndorsementsForTotal(IBusinessEntity covParent, CoverageDetails coverageDetails) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		
		if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
			return coverageDetails.getEndorsements().stream().filter(o ->   
					SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B.equalsIgnoreCase(o.getEndorsementCd()))
					.collect(Collectors.toSet());
		}
		
		return super.getEndorsementsForTotal(covParent, coverageDetails);
	}

	@Override
	public List<TotalPremiumDto> createTotalPremiumDtos(IBusinessEntity covParent) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		List<TotalPremiumDto> dtos = super.createBreakDownTotalPremiums(vehicle, vehicle.getRiskPremium(), vehicle.getCoverageDetail());
		
		if (dtos != null && !dtos.isEmpty()) {
			dtos.stream().filter(o -> TOTALKEY_COV.equalsIgnoreCase(o.getKey()))
						 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalCovPremLabel")));
			dtos.stream().filter(o -> TOTALKEY_END.equalsIgnoreCase(o.getKey()))
			 			 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalEndPremLabel")));
			dtos.stream().filter(o -> TOTALKEY_SUBTOT.equalsIgnoreCase(o.getKey()))
			 			 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalCovEndPremLabel")));
			dtos.stream().filter(o -> TOTALKEY_COVNPC.equalsIgnoreCase(o.getKey()))
						 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalCovNPCPremLabel")));
			dtos.stream().filter(o -> TOTALKEY_ENDNPC.equalsIgnoreCase(o.getKey()))
						 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalEndNPCPremLabel")));
			dtos.stream().filter(o -> TOTALKEY_SUBTOTNPC.equalsIgnoreCase(o.getKey()))
						 .findFirst().ifPresent(o -> o.setTotalLabel(uiHelper.getSessionLabels().getString("VehicleCoverageListTotalCovEndNPCPremLabel")));
		}
		
		return dtos;
	}
	
	@Override
	protected Double calculateTotalPremium(IBusinessEntity covParent, List<Premium> premList) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		Double result = 0D;
		
		if (vehicle.getNumberOfUnits() != null && vehicle.getNumberOfUnits() > 0 &&
			premList != null && !premList.isEmpty()) {
//			Double totPremium = premList.stream().mapToDouble(Premium::getAnnualPremium).sum();
			Double totPremium = premList.stream().mapToDouble(Premium::getTransactionPremium).sum();
			result = totPremium / vehicle.getNumberOfUnits()
					/* (new BigDecimal(totPremium / vehicle.getNumberOfUnits())
						.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP))
						.doubleValue()
						*/
					;
		}
		
		return uiHelper.roundNumeric(result);
	}

	@Override
	protected Double calculateTotalNPCPremium(IBusinessEntity covParent, List<Premium> premList) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		Double result = 0D;
		Integer numberOfUnits = vehicle.getNumberOfUnits();
		
		// Use differences
		if (Constants.VERS_TXN_TYPE_POLICYCHANGE.equalsIgnoreCase(policyUIHelper.getCurrentPolicyVersion().getPolicyTxnType())) {
			if (policyUIHelper.getCurrentPolicy() != null && policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO() != null) {
				SpecialtyPolicyItemDTO vehPolicyItemDTO = policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO().getVehicleItemByPK(vehicle.getPolicyRiskPK());
				if (vehPolicyItemDTO != null && vehPolicyItemDTO.getOriginatingUnits() != null) {
					numberOfUnits = Math.abs(vehicle.getNumberOfUnits() - vehPolicyItemDTO.getOriginatingUnits());
					if (numberOfUnits == 0) {
						numberOfUnits = vehicle.getNumberOfUnits();
					}
				}
			}
		}
		
		if (numberOfUnits != null && numberOfUnits > 0 &&
			premList != null && !premList.isEmpty()) {
			Double totPremium = premList.stream().mapToDouble(Premium::getNetPremiumChange).sum();
			result = totPremium / numberOfUnits
					/*(new BigDecimal(totPremium / numberOfUnits)
						.setScale(getDecimals(), BigDecimal.ROUND_HALF_UP))
						.doubleValue()
						*/
					;
		}
		
		return uiHelper.roundNumeric(result);
	}

	@Override
	public List<String> getWarnAndErrorOfCoverageSelction(IBusinessEntity covParent, boolean isSelected, List<CoverageConfigDto> selectionList) {
		// TODO Auto-generated method stub
		List<String> list = super.getWarnAndErrorOfCoverageSelction(covParent, isSelected, selectionList);
		
		if (isSelected) {
			SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
			SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = (SpecialityAutoSubPolicy<Risk>) vehicle.getSubPolicy();
			if (spAutoSubPolicy.getIsFleet() != null && !spAutoSubPolicy.getIsFleet() &&
				selectionList != null && !selectionList.isEmpty()) {
				if (selectionList.stream().filter(o -> SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_GDPC.equalsIgnoreCase(o.getCoverageConfig().getCode()))
								.findFirst().isPresent()) {
					list.add(uiHelper.getSessionLabels().getString("AutoSubPolicyCoverageGDPCNonFleetWarn"));
				}
			}
		}
		
		return list;
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected void reopenCoverageList(PolicyTransaction policyTransaction, IBusinessEntity covParent,
			CoverageDto coverageDto, String action) {
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) covParent;
		try {
			BasePolicyProcessor policyProcessor = new BasePolicyProcessor(action, policyTransaction);
			if (policyProcessor.needToLoadData(vehicle.getCoverageDetail())) {
				vehicle = (SpecialtyVehicleRisk) policyProcessor.loadPolicyRiskData(vehicle);
			}
		} catch (Exception e) {
			uiHelper.notificationException(e);
			VehicleCoverageUIHelper.super.reopenCoverageList(policyTransaction, covParent, coverageDto, action);
			return;
		}
		
		messageOnChange(policyTransaction, vehicle, action, new DSNotificatonUIHelper.NotificatonCallback() {
			@Override
			public void close() {
				VehicleCoverageUIHelper.super.reopenCoverageList(policyTransaction, covParent, coverageDto, action);
			}
		});
		
	}

	//when there is no AP or Coll & Comp.
	@SuppressWarnings("rawtypes")
	private void messageOnChange(PolicyTransaction policyTransaction, SpecialtyVehicleRisk vehicle, String action, NotificatonCallback callback) {
		if (StringUtils.isNotBlank(action) &&
			MESSAGECHANGESACTIONS.contains(action) &&
			SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
			if (!(vehicle.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP) != null 
				  ||
				 (vehicle.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL) != null &&
				  vehicle.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM) != null))
			   ) {
				uiHelper.notificationWarning(uiHelper.getSessionLabels().getString("WarnOPCF27BWithoutCorrectCoverages"), callback);
				return;
			}
		}

		callback.close();
	}
	
}
