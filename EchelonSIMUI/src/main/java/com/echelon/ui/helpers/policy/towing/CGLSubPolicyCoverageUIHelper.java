package com.echelon.ui.helpers.policy.towing;

import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.BaseCoverageTabs;
import com.echelon.ui.components.towing.CGLSubPolicyLocCovMultiForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.utils.SpecialtyAutoConstants;

public class CGLSubPolicyCoverageUIHelper extends BaseTowingSubPolicyCoverageUIHelper<CGLSubPolicyLocCovMultiForm> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6281252648669612770L;

	public CGLSubPolicyCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.CGLSubPolicy, POLICYTREENODES.CGLSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected DSUpdateDialog<BaseCoverageTabs> createCoverageDialog(CoverageDto coverageDto, String title,
			String action) {
		// TODO Auto-generated method stub
		DSUpdateDialog<BaseCoverageTabs> dialog = super.createCoverageDialog(coverageDto, title, action);
		
		if (SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD.equalsIgnoreCase(coverageDto.getCoverageCode())) {
			dialog.setWidth("1200px");
		}
		
		return dialog;
	}

}
