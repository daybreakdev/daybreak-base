package com.echelon.ui.helpers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.ds.ins.domain.entities.Producer;
import com.ds.ins.exceptions.BrokerException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.processes.broker.BrokerProcesses;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.uicommon.components.DSApplicationStore;
import com.ds.ins.uicommon.converters.DSNumberConverter;
import com.ds.ins.uicommon.helpers.DSLookupUIHelper;
import com.echelon.domain.policy.specialtylines.CVRate;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.ui.components.baseclasses.LookupItemValue;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;

public class LookupUIHelper extends DSLookupUIHelper {

	/**
	 *
	 */
	private static final long serialVersionUID = -7073067031730606149L;
	private List<Integer> vehilceYears;

	public LookupUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		// TODO Auto-generated method stub
		return ProductConfigFactory.getInstance();
	}

	@Override
	protected IGenericLookupConfig loadLookupConfig() {
		if (BrokerUI.getUserSession() != null &&
			BrokerUI.getUserSession().getCurrentPolicy() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion() != null &&
			BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction() != null) {
			return getLookupConfig(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getInsurancePolicy().getProductCd(),
									BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyTerm().getTermEffDate().toLocalDate());
		}

		return null;
	}

	public List<Producer> getAllBrokersByInsuranceProduct(String productCode) throws BrokerException {
		DSApplicationStore appStore = applicationStoreHelper().getApplicationStore();
		List<Producer> list = appStore.getProducerList(productCode);
		if (list == null) {
			// Only the product code is needed; passing blanks/nulls for the other parameters results in them simply being
			// ignored when querying the database.
			list = (new BrokerProcesses()).getAllBrokersByInsProduct(null, productCode, null, null, null, null, null);
			if (list == null) {
				list = new ArrayList<>();
			}

			appStore.setProducerList(productCode, list);
			applicationStoreHelper().saveApplicationStore(appStore);
		}

		return list;
	}

	public List<LookupTableItem> getAllCVRateDesciptions() throws SpecialtyAutoPolicyException  {
		DSApplicationStore appStore = applicationStoreHelper().getApplicationStore();

		if (appStore.getCvRateList().size() == 0) {
			List<CVRate> cvRates = (new SpecialtyAutoPolicyProcesses()).getAllCVRates();
			if (cvRates != null && !cvRates.isEmpty()) {
				for(CVRate cvRate : cvRates) {
					LookupTableItem item = new LookupTableItem(cvRate.getDescription(), cvRate.getDescription(), cvRate);
					appStore.getCvRateList().put(cvRate.getDescription(), item);
				}
			}

			if (cvRates.stream().filter(o -> UIConstants.CARGOVALUE_DEFAULT.equalsIgnoreCase(o.getDescription()))
								.findFirst().orElse(null) == null) {
				LookupTableItem item = new LookupTableItem(UIConstants.CARGOVALUE_DEFAULT, UIConstants.CARGOVALUE_DEFAULT);
				appStore.getCvRateList().put(UIConstants.CARGOVALUE_DEFAULT, item);
			}

			applicationStoreHelper().saveApplicationStore(appStore);
		}

		List<LookupTableItem> list = new ArrayList<LookupTableItem>();
		list.addAll(appStore.getCvRateList().values());

		return list;
	}

	public LookupTableItem getCVRateByDesciption(String description) {
		if (description != null) {
			DSApplicationStore appStore = applicationStoreHelper().getApplicationStore();
			LookupTableItem item = appStore.getCvRateList().get(description);
			return item;
		}

		return null;
	}

	public List<Integer> getVehilceYears() {
		if (vehilceYears == null) {
			vehilceYears = new ArrayList<Integer>();

			DSNumberConverter numberConverter = new DSNumberConverter();
			int startYear = LocalDate.now().getYear();
			// to Current + 1 year
			int endYear	  = LocalDate.now().getYear()+1;

			List<LookupTableItem> origList = getLookupConfigLookup(ConfigConstants.LOOKUPTABLE_VehYearsList);
			if (origList != null && !origList.isEmpty()) {
				LookupTableItem first = origList.get(0);
				try {
					Integer yr = numberConverter.toInteger(first.getItemKey());
					if (yr != null) {
						startYear = yr;
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}

			for(int yy=startYear; yy<=endYear; yy++) {
				vehilceYears.add(yy);
			}

			Collections.sort(vehilceYears, Collections.reverseOrder());
		}

		return vehilceYears;
	}

	public List<LookupItemValue> convertLookupItemValues(List<LookupTableItem> lookupTableItems) {
		List<LookupItemValue> results = new ArrayList<LookupItemValue>();
		
		if (lookupTableItems != null && !lookupTableItems.isEmpty()) {
			for(LookupTableItem item : lookupTableItems) {
				results.add(new LookupItemValue(item));
			}
		}
		
		return results;
	}
	
	public List<LookupTableItem> convertLookupTableItems(List<LookupItemValue> lookupItemValues) {
		List<LookupTableItem> results = new ArrayList<LookupTableItem>();
		
		if (lookupItemValues != null && !lookupItemValues.isEmpty()) {
			results.addAll(lookupItemValues);
		}
		
		return results;
	}
}
