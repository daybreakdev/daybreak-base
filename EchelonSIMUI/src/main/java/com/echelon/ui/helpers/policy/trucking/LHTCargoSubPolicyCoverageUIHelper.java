package com.echelon.ui.helpers.policy.trucking;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.towing.CargoSubPolicyCoverageUIHelper;

public class LHTCargoSubPolicyCoverageUIHelper extends CargoSubPolicyCoverageUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7896085521358042253L;

	public LHTCargoSubPolicyCoverageUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void reopenCoverageList(PolicyTransaction<?> policyTransaction, IBusinessEntity covParent,
			CoverageDto coverageDto, String action) {
		// TODO Auto-generated method stub
		// No need to call the message for LHT, so change action from SAVE to something else
		if (UIConstants.UIACTION_Save.equals(action)) {
			super.reopenCoverageList(policyTransaction, covParent, coverageDto, UIConstants.UIACTION_OpenPolicy);
		}
		else {
			super.reopenCoverageList(policyTransaction, covParent, coverageDto, action);
		}
	}

}
