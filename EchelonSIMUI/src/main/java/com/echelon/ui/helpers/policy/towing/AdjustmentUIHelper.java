package com.echelon.ui.helpers.policy.towing;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.towing.AdjustmentForm;
import com.echelon.ui.components.towing.CGLSubPolicyAdjustmentList;
import com.echelon.ui.components.towing.CargoSubPolicyAdjustmentList;
import com.echelon.ui.components.towing.VehicleAdjustmentList;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.process.towing.AdjustmentProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSConfirmDialog.ConfirmEvent;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class AdjustmentUIHelper extends ContentUIHelper implements Serializable {
	private static final long serialVersionUID = -8299288401931508218L;

	public void loadView(VehicleAdjustmentList adjustmentList, SpecialtyVehicleRisk vehicle) {
		final Set<SpecialtyVehicleRiskAdjustment> adjustmentSet = Optional.ofNullable(vehicle)
				.map(SpecialtyVehicleRisk::getSvrAdjustments).orElse(new HashSet<>());
		final List<SpecialtyVehicleRiskAdjustment> adjustments = adjustmentSet.stream()
																	.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum, Comparator.nullsLast(Comparator.naturalOrder())))
																	.collect(Collectors.toList());
		adjustmentList.loadData(vehicle, adjustments);
	}

	public void loadView(CGLSubPolicyAdjustmentList adjustmentList, CGLSubPolicy<?> cglSubPolicy) {
		final Set<SpecialtyVehicleRiskAdjustment> adjustmentSet = Optional.ofNullable(cglSubPolicy)
				.map(CGLSubPolicy::getSvrAdjustments).orElse(new HashSet<>());
		final List<SpecialtyVehicleRiskAdjustment> adjustments = adjustmentSet.stream()
																	.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum, Comparator.nullsLast(Comparator.naturalOrder())))
																	.collect(Collectors.toList());
		adjustmentList.loadData(cglSubPolicy, adjustments);
	}

	public void loadView(CargoSubPolicyAdjustmentList adjustmentList, CargoSubPolicy<?> cargoSubPolicy) {
		final Set<SpecialtyVehicleRiskAdjustment> adjustmentSet = Optional.ofNullable(cargoSubPolicy)
				.map(CargoSubPolicy::getSvrAdjustments).orElse(new HashSet<>());
		final List<SpecialtyVehicleRiskAdjustment> adjustments = adjustmentSet.stream()
																	.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum, Comparator.nullsLast(Comparator.naturalOrder())))
																	.collect(Collectors.toList());
		adjustmentList.loadData(cargoSubPolicy, adjustments);
	}

	public void newAction(SpecialtyVehicleRisk parentVehicle) {
		final AdjustmentProcessor processor = createProcessor(UIConstants.UIACTION_New,
				policyUIHelper.getCurrentPolicyTransaction(), parentVehicle, null);
		createTaskProcessor(null).start(processor);
	}

	public void updateAction(SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		final String title = uiHelper.getSessionLabels().getString("AdjustmentUpdateDialogTitle");
		setupAction(title, UIConstants.UIACTION_Update, parentVehicle, adjustment);
	}

	public void deleteAction(SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		final String msg = uiHelper.getSessionLabels().getString("AdjustmentDeleteDialogMessage");
		final DSConfirmDialog dialog = createConfirmDeleteDialog(msg);
		dialog.setDataStore(new DSDialogDataStore(parentVehicle, adjustment));
		dialog.open();
	}

	public void undeleteAction(SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		final String msg = uiHelper.getSessionLabels().getString("AdjustmentUndeleteDialogMessage");
		final DSConfirmDialog dialog = createConfirmUndeleteDialog(msg);
		dialog.setDataStore(new DSDialogDataStore(parentVehicle, adjustment));
		dialog.open();
	}

	@Override
	protected void onSaveNew(SaveEvent event) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<AdjustmentForm> dialog = event.getSource();
		final AdjustmentForm form = dialog.getContentComponent();
		final DSDialogDataStore dataStore = dialog.getDataStore();
		final SpecialtyVehicleRisk parentVehicle = (SpecialtyVehicleRisk) dataStore.getData1();
		final SpecialtyVehicleRiskAdjustment adjustment = (SpecialtyVehicleRiskAdjustment) dataStore.getData2();

		if (form.validate()) {
			try {
				if (!form.save(adjustment)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}

			createTaskProcessor(dialog).start(createProcessor(UIConstants.UIACTION_SaveNew,
					policyUIHelper.getCurrentPolicyTransaction(), parentVehicle, adjustment));
		}
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		@SuppressWarnings("unchecked")
		final DSUpdateDialog<AdjustmentForm> dialog = event.getSource();
		final AdjustmentForm form = dialog.getContentComponent();
		final DSDialogDataStore dataStore = dialog.getDataStore();
		final SpecialtyVehicleRisk parentVehicle = (SpecialtyVehicleRisk) dataStore.getData1();
		final SpecialtyVehicleRiskAdjustment adjustment = (SpecialtyVehicleRiskAdjustment) dataStore.getData2();

		if (form.validate() && form.save(adjustment)) {
			createTaskProcessor(dialog).start(createProcessor(UIConstants.UIACTION_Save,
					policyUIHelper.getCurrentPolicyTransaction(), parentVehicle, adjustment));
		}
	}

	@Override
	protected void onDelete(ConfirmEvent event) {
		final DSConfirmDialog dialog = event.getSource();
		final DSDialogDataStore dataStore = dialog.getDataStore();
		final SpecialtyVehicleRisk parentVehicle = (SpecialtyVehicleRisk) dataStore.getData1();
		final SpecialtyVehicleRiskAdjustment adjustment = (SpecialtyVehicleRiskAdjustment) dataStore.getData2();
		createTaskProcessor(dialog).start(createProcessor(UIConstants.UIACTION_Delete,
				policyUIHelper.getCurrentPolicyTransaction(), parentVehicle, adjustment));
	}

	@Override
	protected void onUndelete(ConfirmEvent event) {
		final DSConfirmDialog dialog = event.getSource();
		final DSDialogDataStore dataStore = dialog.getDataStore();
		final SpecialtyVehicleRisk parentVehicle = (SpecialtyVehicleRisk) dataStore.getData1();
		final SpecialtyVehicleRiskAdjustment adjustment = (SpecialtyVehicleRiskAdjustment) dataStore.getData2();
		createTaskProcessor(dialog).start(createProcessor(UIConstants.UIACTION_Undelete,
				policyUIHelper.getCurrentPolicyTransaction(), parentVehicle, adjustment));
	}

	private void setupAction(String title, String action, SpecialtyVehicleRisk parentVehicle,
			SpecialtyVehicleRiskAdjustment adjustment) {
		final DSUpdateDialog<AdjustmentForm> dialog = createDialog(title, action);
		final AdjustmentForm form = dialog.getContentComponent();

		dialog.setDataStore(new DSDialogDataStore(parentVehicle, adjustment));
		form.loadData(parentVehicle, adjustment);
		dialog.open();
	}

	private AdjustmentProcessor createProcessor(String action, PolicyTransaction<?> policyTransaction,
			SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment) {
		return new AdjustmentProcessor(action, policyTransaction, parentVehicle, adjustment);
	}

	@SuppressWarnings("serial")
	private TaskProcessor createTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {
			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					final AdjustmentProcessor processor = (AdjustmentProcessor) taskRun.getProcessor();
					final PolicyTransaction<?> policyTransaction = processor.getPolicyTransaction();
					final SpecialtyVehicleRisk parentVehicle = processor.getParentVehicle();
					final SpecialtyVehicleRiskAdjustment adjustment = processor.getAdjustment();

					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:
						setupAction(uiHelper.getSessionLabels().getString("AdjustmentNewDialogTitle"),
								UIConstants.UIACTION_New, parentVehicle, adjustment);
						break;
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(policyTransaction, parentVehicle, UIConstants.UIACTION_SaveNew);
						break;
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(policyTransaction, parentVehicle, UIConstants.UIACTION_Save);
						break;
					case UIConstants.UIACTION_Delete:
						dialog.close();
						if (adjustment != null && adjustment.isNew()) { // it was new
							reopen(policyTransaction, parentVehicle, UIConstants.UIACTION_Delete);
						} else {
							reopen(policyTransaction, parentVehicle, UIConstants.UIACTION_Delete);
						}

						break;
					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(policyTransaction, parentVehicle, UIConstants.UIACTION_Undelete);
						break;
					}
				}

				return true;
			}
		};
	}

	@SuppressWarnings("unchecked")
	private DSUpdateDialog<AdjustmentForm> createDialog(String title, String action) {
		final AdjustmentForm form = new AdjustmentForm(action, UICONTAINERTYPES.Dialog);
		return (DSUpdateDialog<AdjustmentForm>) createDialog(title, form, action);
	}

	private void reopen(PolicyTransaction<?> policyTransaction, SpecialtyVehicleRisk vehicle, String action) {
		if (vehicle != null) {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.Vehicle, vehicle);
		} else {
			policyUIHelper.reopenPolicy(policyTransaction, POLICYTREENODES.VehicleListNode, vehicle);
		}
	}
	
	public Integer getPrevAdjustmentNumOfUnits(SpecialtyVehicleRiskAdjustment adj) {
		Integer result = null;
		
		if (adj != null) {
			SpecialtyVehicleRiskAdjustment prevAdj = adj.findPrevAdjustment();
			if (prevAdj != null) {
				result = prevAdj.getNumOfUnits();
			}
			else if (policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO() != null) {
				if (adj.getAdjustmentNum() != null) {
					SpecialtyPolicyItemDTO itemDTO = policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO().getVehicleItemByAdjNum(adj.getAdjustmentNum());
					if (itemDTO != null) {
						result = itemDTO.getOriginatingUnits();
					}
				}
				
				if (adj.getSpecialtyVehicleRisk() != null) {
					SpecialtyPolicyItemDTO itemDTO = policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO().getVehicleItemByPK(adj.getSpecialtyVehicleRisk().getPolicyRiskPK());
					if (itemDTO != null) {
						result = itemDTO.getOriginatingUnits();
					}					
				}
			}
		}
		
		return result;
	}
	
	public Double getOriginatingAdjustmentUnitRate(SpecialtyVehicleRiskAdjustment adj) {
		Double result = null;
		
		if (adj != null) {
			if (policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO() != null) {
				SpecialtyPolicyItemDTO dto = policyUIHelper.getCurrentPolicy().getSpecialtyPolicyDTO().getItemDTO(adj);
				if (dto != null) {
					result = dto.getSubOriginatingUnitRate();
				}
			}
		}
		
		return result;
	}
}
