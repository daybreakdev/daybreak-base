package com.echelon.ui.helpers.policy.towing;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.ui.components.baseclasses.dto.CoverageDto;
import com.echelon.ui.components.baseclasses.policy.IBaseCovAndEndorseForm;
import com.echelon.ui.components.towing.GarageSubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.SubPolicyLocCovMultiForm;
import com.echelon.ui.components.towing.dto.LocCoverageDto;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.session.BrokerUI;

public class GarageSubPolicyCoverageUIHelper extends BaseTowingSubPolicyCoverageUIHelper<GarageSubPolicyLocCovMultiForm> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4632757540752316302L;

	public GarageSubPolicyCoverageUIHelper() {
		super(UIConstants.OBJECTTYPES.GARSubPolicy, POLICYTREENODES.GarageSubPolicyNode);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected boolean preValidAndSaveCoverage(IBusinessEntity covParent, IBaseCovAndEndorseForm form,
												CoverageDto coverageDto,
												String action,
												List<CoverageDto> newCoverages) {
		boolean ok = super.preValidAndSaveCoverage(covParent, form, coverageDto, action, newCoverages);
		
		// warning message VR-TOW-COV-12 if OEF77 does not exist 
		// when coverage 6.4 - Specified Perils (CoverageCd=SP64) is selected under Garage sub policy
		if (ok) {
			if (SpecialtyAutoConstants.GARAGE_LOC_COVERAGES.contains(coverageDto.getCoverageCode())) {
				if (UIConstants.UIACTION_SaveNew.equals(action)) {
					boolean anyOEF77 = false;
					if (newCoverages != null && !newCoverages.isEmpty()) {
						anyOEF77 = newCoverages.stream().filter(o -> SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_OEF77.equalsIgnoreCase(o.getCoverageCode()))
														.findFirst().orElse(null) != null;
					}
					if (!anyOEF77) {
						if (covParent != null) {
							SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) covParent;
							if (subPolicy.getCoverageDetail() != null) {
								if (subPolicy.getCoverageDetail().getEndorsements() != null &&
									!subPolicy.getCoverageDetail().getEndorsements().isEmpty()) {
									anyOEF77 =  subPolicy.getCoverageDetail().getEndorsements()
													.stream().filter(o -> o.isActive() || o.isPending()).collect(Collectors.toList())
													.stream().filter(o -> SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_OEF77.equalsIgnoreCase(o.getEndorsementCd()))
													.findFirst().orElse(null) != null;
								}
							}
						}
					}
					
					if (!anyOEF77) {
						coverageDto.addEditMessage(uiHelper.getSessionLabels().getString("GarageSubPolicyCoverageSP64WithoutOEF77Waning"));
					}
				}
			}
		}
		
		return ok;
	}

	@Override
	protected boolean validAndSaveCoverage(IBusinessEntity covParent, IBaseCovAndEndorseForm form, CoverageDto coverageDto,
											List<CoverageDto> workingList,
											String action) {
		
		String locIds = getInvalidSection5CoverageLocationIds(covParent, coverageDto, workingList, form);
		if (locIds != null && !locIds.isEmpty()) {
			try {
				String msg = uiHelper.getSessionLabels().getString("GarageSubPolicyCoverage"+coverageDto.getCoverageCode()+"InMultiLocError");
				coverageDto.addEditMessage(MessageFormat.format(msg, locIds));
			} catch (Exception e) {
				e.printStackTrace();
			}
			return false;
		}
		
		return super.validAndSaveCoverage(covParent, form, coverageDto, workingList, action);
	}

	@Override
	protected boolean isCoverageInGridHasAdditionalFields(CoverageDto dto) {
		return super.isCoverageInGridHasAdditionalFields(dto) ||
				(SpecialtyAutoConstants.GARAGE_SECTION5_COVERAGES.contains(dto.getCoverageCode()))
				;
	}

	// Result is a list of location IDs
	public String getInvalidSection5CoverageLocationIds(IBusinessEntity covParent, CoverageDto coverageDto, List<CoverageDto> workingList, 
														IBaseCovAndEndorseForm form) {
		String result = null;
		
		if (SpecialtyAutoConstants.GARAGE_SECTION5_COVERAGES.contains(coverageDto.getCoverageCode())) {
			// remove current coverage code
			List<String> otherSection5Coverages = new ArrayList<String>();
			otherSection5Coverages.addAll(SpecialtyAutoConstants.GARAGE_SECTION5_COVERAGES);
			otherSection5Coverages.remove(coverageDto.getCoverageCode());
			
			// selected location of current coverage
			SubPolicyLocCovMultiForm locCovMultiForm = (SubPolicyLocCovMultiForm) form;
			List<LocCoverageDto> allLocDtos = locCovMultiForm.getAllData();
			
			List<PolicyLocation> selectedLocations = new ArrayList<PolicyLocation>();
			if (allLocDtos != null && !allLocDtos.isEmpty()) {
				allLocDtos.stream().forEach(o -> {
						if (o.getPolicyLocation() != null) {
							selectedLocations.add(o.getPolicyLocation());
						}
				});
			}
			
			Set<PolicyLocation> locations = new HashSet<PolicyLocation>();
			
			// check if selected location in other coverages
			// workingList available e.g. Grid or Adding multiple
			if (workingList != null) {
				Set<PolicyLocation> locs = workingList
						.stream().filter(o -> otherSection5Coverages.contains(o.getCoverageCode()) && 
											  o.getCoverageAdditionalForm() != null &&
											  o.getCoverageAdditionalForm() instanceof SubPolicyLocCovMultiForm
								).collect(Collectors.toList())
						.stream().map(o -> (SubPolicyLocCovMultiForm)o.getCoverageAdditionalForm()).collect(Collectors.toList())
						.stream().flatMap(o -> o.getAllEntries().stream()).collect(Collectors.toList()) //list of DSMultiNewEntryDto<DSSubPolicyLocCovForm, DSLocCoverageDto>
						.stream().map(o -> o.getData().getPolicyLocation()).collect(Collectors.toSet())
						.stream().filter(o -> selectedLocations.contains(o)).collect(Collectors.toSet())
						.stream().map(o -> (PolicyLocation)o).collect(Collectors.toSet());
				if (!locs.isEmpty()) {
					locations.addAll(locs);
				}

				// Exclude the coverage on the working list
				for(CoverageDto covDto : workingList) {
					otherSection5Coverages.remove(covDto.getCoverageCode());
				}
			}
			
			// if coverage not in workingList, get it from policy version
			if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getCurrentPolicy() != null &&
				BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyRisks() != null &&
				!BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyRisks().isEmpty()) {
				Set<PolicyLocation> locs = BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction().getPolicyRisks()
						.stream().filter(o -> 
									SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()) &&
									selectedLocations.contains(o) &&
									o.getCoverageDetail().getCoverages()
												.stream().filter(o2 -> o2.isActive() || o2.isPending()).collect(Collectors.toList())
												.stream().filter(o2 -> otherSection5Coverages.contains(o2.getCoverageCode()))
												.findFirst().isPresent()
								).collect(Collectors.toSet())
						.stream().map(o -> (PolicyLocation)o).collect(Collectors.toSet());
				if (!locs.isEmpty()) {
					locations.addAll(locs);
				}
			}
			
			if (!locations.isEmpty()) {
				result = locations.stream().map(PolicyLocation::getLocationId).sorted().collect(Collectors.joining(","));
			}
		}		
		
		return result;
	}
}
