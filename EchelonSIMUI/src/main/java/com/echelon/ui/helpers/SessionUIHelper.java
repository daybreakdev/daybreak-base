package com.echelon.ui.helpers;

import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.uicommon.helpers.DSSessionUIHelper;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.process.policy.BasePolicyTransactionProcessor;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;

public class SessionUIHelper extends DSSessionUIHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1918646621697807333L;

	public SessionUIHelper(UIHelper uiHelper) {
		super(uiHelper);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void startAction() {
		requestRunDataFixes();
	}

	private void requestRunDataFixes() {
		if (BrokerUI.getUserSession().getUserProfile() != null &&
				BrokerUI.getUserSession().getOpenedDialog() == null &&
				!BrokerUI.getHelperSession().getNotificatonUIHelper().isNotificationOpened()) {
			
			new DSConfirmDialog(
					getUIHelper().getConfirmLabel(UIConstants.UIACTION_Confirm), 
					getUIHelper().getButtonLabel(UIConstants.UIACTION_Yes),
					this::confirmRunDataFixes,
					getUIHelper().getButtonLabel(UIConstants.UIACTION_No),
					this::cancelRunDataFixes,
					getUIHelper().getSessionLabels().getString("ProcessRunDataFixesConfirmMesssge"))
				.open();
			
			onActionStart();		
		}
	}
	
	protected void confirmRunDataFixes(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		(new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				dialog.close();
				onActionComplete();
				
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					BasePolicyTransactionProcessor processor = (BasePolicyTransactionProcessor) taskRun.getProcessor();
					/*
					List<String> processingResults = processor.getProcessingResults();
					if (processingResults == null || processingResults.isEmpty()) {
						uiHelper.notificationMessage(uiHelper.getSessionLabels().getString("ProcessRunAllPolicyRatingConfirmNoPolicyMessage"));
					}
					else {
						List<String> fails = processingResults.stream().filter(o -> {
														// PolicytransactionPK, Quote Number, [S]ucceed/[F]ailed
														String[] rs = o.split(","); 
															return rs.length >= 3 && "F".equalsIgnoreCase(rs[2]);
														}).collect(Collectors.toList());
						if (fails.isEmpty()) {
							uiHelper.notificationSucceed(uiHelper.getSessionLabels().getString("ProcessRunAllPolicyRatingConfirmSucceedMessage"));
						}
						else {
							uiHelper.notificationError(uiHelper.getSessionLabels().getString("ProcessRunAllPolicyRatingConfirmFailedMessage"));
						}
					}
					*/
					
					if (processor.getProcessResult()) {
						uiHelper.notificationSucceed(uiHelper.getSessionLabels().getString("ProcessRunDataFixesConfirmSucceedMessage"));
					}
					else {
						uiHelper.notificationError(uiHelper.getSessionLabels().getString("ProcessRunDataFixesConfirmFailedMessage"));
					}
				}
				
				return true;
			}
				
		})
		.withProgressDuration(UIConstants.PROGRESSDURATIONS.Service)
		.start(new BasePolicyTransactionProcessor(UIConstants.UIACTION_RunDataFixes,
						BrokerUI.getHelperSession().getPolicyUIHelper().getPolicyProductProcessing(ConfigConstants.PRODUCTCODE_TOWING)));
	}
	
	protected void cancelRunDataFixes(DSConfirmDialog.CancelEvent event) {
		DSConfirmDialog dialog = event.getSource();
		dialog.close();
		onActionComplete();
	}
	
	@Override
	public boolean onActionStart() {
		if (BrokerUI.getUserSession().getPolicySearchView() != null) {
			BrokerUI.getUserSession().getPolicySearchView().disableButtonShortcuts();
		}
		
		return super.onActionStart();
	}
	
	@Override
	public boolean onActionComplete() {
		if (!super.onActionComplete()) {
			if (BrokerUI.getUserSession().getPolicySearchView() != null) {
				BrokerUI.getUserSession().getPolicySearchView().enableButtonShortcuts();
			}
		}
		
		return true;
	}
}
