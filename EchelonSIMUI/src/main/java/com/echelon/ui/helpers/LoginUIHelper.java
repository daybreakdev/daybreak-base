package com.echelon.ui.helpers;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

import com.ds.ins.domain.dto.UserAuthenticationResultDTO;
import com.ds.ins.interfaces.services.ISecurityService;
import com.ds.ins.system.UserProfile;
import com.echelon.services.ServiceConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.server.VaadinSession;

@Dependent
public class LoginUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2720491615865800872L;
	@Inject
	private UIHelper uiHelper;
	@Inject
	private ISecurityService securityService;
	
	public LoginUIHelper() {
		super();
	}
	
	@PostConstruct
	private void postConstruct() {
		//uiHelper = new ECUIHelper();
	}
	
	public String login(String loginId, String password, Integer brokerNo, Integer brkOfficeNo, String provinceCd) {
		UserAuthenticationResultDTO authenticationResult = null;
		
		try {
			authenticationResult = securityService.authenticateUser(loginId, password);
			if (!ServiceConstants.AUTHENTICATION_SUCCEED.equalsIgnoreCase(authenticationResult.getAuthenticationResult())) {
				return authenticationResult.getAuthenticationResult();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			uiHelper.notificationException(e);
			return ServiceConstants.AUTHENTICATION_EXCEPTION;
		}

		try {
			UserProfile userProfile = authenticationResult.getUserProfile();		
			
			BrokerUI.getUserSession().setUserProfile(userProfile);
			BrokerUI.getUserSession().setBrokerNo(brokerNo);
			BrokerUI.getUserSession().setBrokerOfficeNo(brkOfficeNo);
			BrokerUI.getUserSession().setBrokerProvince(provinceCd);
	
			UI.getCurrent().navigate(UIConstants.POLICYSEARCH_ROUTE);
		} catch (Exception e) {
			uiHelper.notificationException(e);
			return ServiceConstants.AUTHENTICATION_EXCEPTION;
		}
		
		return ServiceConstants.AUTHENTICATION_SUCCEED;
	}

	public void logout() {
		try {
			BrokerUI.removeUserSession();
			if (VaadinSession.getCurrent() != null &&
				VaadinSession.getCurrent().getSession() != null) {
				VaadinSession.getCurrent().getSession().invalidate();
			}
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			uiHelper.notificationException(e1);
			return;
		}		
	}
}
