package com.echelon.ui.helpers.entities;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.ds.ins.utils.Constants;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.components.entities.AdditionalInsuredAndVehiclesView;
import com.echelon.ui.components.entities.AdditionalInsuredForm;
import com.echelon.ui.components.entities.AdditionalInsuredList;
import com.echelon.ui.components.entities.AdditionalInsuredMultiForm;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.process.entities.AdditionalInsuredProcessor;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.vaadin.flow.component.UI;

public class AdditionalInsuredUIHelper extends ContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6725813146300887929L;
	
	public void loadView(AdditionalInsuredList component, SubPolicy subPolicy) {
		component.loadData(subPolicy);
	}
	
	public void loadView(AdditionalInsuredAndVehiclesView component, SubPolicy subPolicy) {
		if (subPolicy != null && subPolicy instanceof SpecialityAutoSubPolicy) {
			component.loadData((SpecialityAutoSubPolicy<Risk>) subPolicy);
		}
		else {
			component.loadData(null);
		}
	}
	
	public AdditionalInsured createEmptyAdditionalInsured(SubPolicy subPolicy) {
		AdditionalInsured addlInsured = new AdditionalInsured();
		if (subPolicy != null && 
				SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			addlInsured.setRelationship("As known to the Company");
		}
		addlInsured.setAdditionalInsuredAddress(new Address());
		addlInsured.getAdditionalInsuredAddress().setProvState(Constants.PROVINCE_ONTARIO);
		
		return addlInsured;
	}
	
	protected AdditionalInsuredMultiForm createAddlInsuredMultiForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		AdditionalInsuredMultiForm form = new AdditionalInsuredMultiForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<AdditionalInsuredMultiForm> createAddlInsuredMultiDialog(String title, String action) {
		DSUpdateDialog<AdditionalInsuredMultiForm> dialog = (DSUpdateDialog<AdditionalInsuredMultiForm>) createDialogWithActions(title, 
																				action,
																				createAddlInsuredMultiForm(action, UICONTAINERTYPES.Dialog), 
																				UIConstants.UIACTION_Add, UIConstants.UIACTION_Save, UIConstants.UIACTION_Cancel);
		dialog.setWidth("1400px");
		return dialog;
	}
	
	protected AdditionalInsuredForm createAddlInsuredForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		AdditionalInsuredForm form = new AdditionalInsuredForm(action, containerType);
		return form;
	}
	
	protected DSUpdateDialog<AdditionalInsuredForm> createAddlInsuredDialog(String title, String action) {
		DSUpdateDialog<AdditionalInsuredForm> dialog = (DSUpdateDialog<AdditionalInsuredForm>) createDialog(title, 
																				createAddlInsuredForm(action, UICONTAINERTYPES.Dialog), 
																				action);
		return dialog;
	}

	public boolean newAction(SubPolicy parentSubPolicy) {
		DSUpdateDialog<AdditionalInsuredMultiForm> dialog 
									= createAddlInsuredMultiDialog(
												uiHelper.getSessionLabels().getString("AdditionalInsuredAddActionLabel"), 
												UIConstants.UIACTION_New);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy));
		dialog.getContentComponent().loadData(parentSubPolicy);
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	@Override
	protected void onAction(DSUpdateDialog.ActionEvent event) {
		DSUpdateDialog<AdditionalInsuredMultiForm> dialog = (DSUpdateDialog<AdditionalInsuredMultiForm>) event.getSource();
		if (UIConstants.UIACTION_Add.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit()) {
				dialog.getContentComponent().addAction(true);
			}
		}
		else if (UIConstants.UIACTION_Save.equals(event.getAction())) {
			if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit()) {
				SubPolicy parentSubPolicy = (SubPolicy) dialog.getDataStore().getData1();
				
				dialog.getContentComponent().getAllData().stream()
											.forEach(o -> {
												o.setPolicyVersion(parentSubPolicy.getPolicyTransaction().getPolicyVersion());
												parentSubPolicy.addAdditionalIsnured(o);
											});
				
				createAdditionalInsuredTaskProcessor(dialog).start(
						createNewAdditionalInsuredsProcessor(parentSubPolicy, 
																dialog.getContentComponent().getAllData())
						);
			}			
		}
	}

	public boolean updateAction(SubPolicy parentSubPolicy, AdditionalInsured additionalInsured) {
		DSUpdateDialog<AdditionalInsuredForm> dialog = createAddlInsuredDialog(
															uiHelper.getSessionLabels().getString("AdditionalInsuredUpdateActionLabel"), 
															UIConstants.UIACTION_Update);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, additionalInsured));
		dialog.getContentComponent().loadData(parentSubPolicy, additionalInsured);
		dialog.getContentComponent().enableEdit();
		dialog.open();
		return true;
	}

	public boolean viewAction(SubPolicy parentSubPolicy, AdditionalInsured additionalInsured) {
		DSUpdateDialog<AdditionalInsuredForm> dialog = createAddlInsuredDialog(
															uiHelper.getSessionLabels().getString("AdditionalInsuredViewActionLabel"), 
															UIConstants.UIACTION_View);
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, additionalInsured));
		dialog.getContentComponent().loadData(parentSubPolicy, additionalInsured);
		dialog.open();
		return true;
	}

	@Override
	protected void onSaveUpdate(DSUpdateDialog.SaveEvent event) {
		DSUpdateDialog<AdditionalInsuredForm> dialog = (DSUpdateDialog<AdditionalInsuredForm>) event.getSource();
		SubPolicy parentSubPolicy = (SubPolicy) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured = (AdditionalInsured) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit(true) && dialog.getContentComponent().saveEdit(additionalInsured)) {
			
			createAdditionalInsuredTaskProcessor(dialog).start(
					createAdditionalInsuredProcessor(UIConstants.UIACTION_Save, parentSubPolicy, additionalInsured)
					);
		}			
	}

	public void deleteAction(SubPolicy parentSubPolicy, AdditionalInsured additionalInsured) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("AdditionalInsuredDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, additionalInsured));
		dialog.open();
	}

	@Override
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		SubPolicy parentSubPolicy = (SubPolicy) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured = (AdditionalInsured) dialog.getDataStore().getData2();
		
		createAdditionalInsuredTaskProcessor(dialog).start(
				createAdditionalInsuredProcessor(UIConstants.UIACTION_Delete, parentSubPolicy, additionalInsured)
				);
	}

	public void undeleteAction(SubPolicy<?> parentSubPolicy, AdditionalInsured additionalInsured) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("AdditionalInsuredUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(parentSubPolicy, additionalInsured));
		dialog.open();
	}

	@Override
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = event.getSource();
		SubPolicy parentSubPolicy = (SubPolicy) dialog.getDataStore().getData1();
		AdditionalInsured additionalInsured = (AdditionalInsured) dialog.getDataStore().getData2();
		
		createAdditionalInsuredTaskProcessor(dialog).start(
				createAdditionalInsuredProcessor(UIConstants.UIACTION_Undelete, parentSubPolicy, additionalInsured)
				);
	}
	
	private void reopen(PolicyTransaction<?> policyTransaction, SubPolicy parentSubPolicy, String action) {
		switch (parentSubPolicy.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_AUTO:
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.AutomobileSubPolicyNode, parentSubPolicy);
			
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.CGLSubPolicyNode, parentSubPolicy);
			
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.CargoSubPolicyNode, parentSubPolicy);
			
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_GARAGE:
			policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.GarageSubPolicyNode, parentSubPolicy);
			
			break;

		default:
			break;
		}
	}

	protected AdditionalInsuredProcessor createNewAdditionalInsuredsProcessor(SubPolicy parentSubPolicy, List<AdditionalInsured> newAdditionalInsureds) {
		return new AdditionalInsuredProcessor(UIConstants.UIACTION_SaveNew, policyUIHelper.getCurrentPolicyTransaction(), 
							parentSubPolicy, newAdditionalInsureds);
	}

	protected AdditionalInsuredProcessor createAdditionalInsuredProcessor(String action, SubPolicy parentSubPolicy, AdditionalInsured additionalInsured) {
		return new AdditionalInsuredProcessor(action, policyUIHelper.getCurrentPolicyTransaction(),
							parentSubPolicy, additionalInsured);
		
	}
	
	protected TaskProcessor createAdditionalInsuredTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					AdditionalInsuredProcessor processor = (AdditionalInsuredProcessor)taskRun.getProcessor();
					dialog.close();
					
					reopen(processor.getPolicyTransaction(), processor.getParentSubPolicy(), processor.getProcessAction());
				}
				
				return true;
			}
			
		};
	}
}
