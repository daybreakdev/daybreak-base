package com.echelon.ui.helpers.bordereau;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.bordereau.BordereauxProcesses;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.services.bordereau.DSPolicyBordereau;
import com.echelon.ui.bordereau.BordereauAllForm;
import com.echelon.ui.bordereau.BordereauSummaryForm;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.constants.UIConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.utils.Constants;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.contextmenu.MenuItem;
import com.vaadin.flow.server.StreamRegistration;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.server.VaadinSession;

public class BourderauxUIHelper extends ContentUIHelper implements Serializable{
	
	private DSUpdateDialog<BordereauSummaryForm> dialog;
	private DSUpdateDialog<BordereauAllForm> bordereauAllDialog;
	protected IGenericPolicyProcessing 	policyProcessing;
	private ResourceBundle		labels;
	
	public BourderauxUIHelper(){
		policyProcessing = policyUIHelper.getPolicyProcessing();
		labels = uiHelper.getSessionLabels();
		
	}
	
	public IGenericPolicyProcessing getpolicyProcessing(String productCd){
		return policyProcessing = policyUIHelper.getPolicyProductProcessing(productCd);
	}
	
	public BourderauxUIHelper(MenuItem menuItemElement){
		PolicyTransaction policyTransaction=null;
		if (policyUIHelper!=null ) {
			policyTransaction=policyUIHelper.getCurrentPolicyTransaction();
		}
		if (policyTransaction==null) {
			menuItemElement.setEnabled(false);
		}else {
			if(policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.BUSINESS_STATUS_ISSUED) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGCHANGE)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDCHANGE) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERS_TXN_TYPE_REVERSAL)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDRENEWAL) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGRENEWAL)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDREINSTATEMENT) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGREINSTATEMENT)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDREISSUE) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGREISSUE)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.BUSINESS_STATUS_REVERSED) 
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDCANCELLATION) || policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGCANCELLATION)
					|| policyTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.TERM_STATUS_PENDING)
					|| policyTransaction.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION)
					|| policyTransaction.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT)
					|| policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd().equals(ConfigConstants.PRODUCTCODE_TOWING_NONFLEET)
					|| policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd().equals(ConfigConstants.PRODUCTCODE_TOWING)
					) {
				menuItemElement.setEnabled(true);
			}else {
				menuItemElement.setEnabled(false);
			}
		}
	}
	
	public List<PolicyTransaction> getListCurrentPolTran(PolicyTransaction currentPolicyTransaction){
		List<PolicyTransaction> ptList = new ArrayList<PolicyTransaction>(); 
		try {
			currentPolicyTransaction = policyUIHelper.getPolicyProcessing().loadPolicyTransactionFull(currentPolicyTransaction, false);
		} catch (InsurancePolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (currentPolicyTransaction!=null) {
			ptList.add(currentPolicyTransaction);
		}
		return ptList;
	}
	
	public List<PolicyTransaction> getAllPolicyTransactions(PolicyTransaction currentPolicyTransaction){
		List<PolicyTransaction> ptAllList = new ArrayList<PolicyTransaction>(); 
		try {
			//policyTransaction = policyUIHelper.getPolicyProcessing().loadPolicyTransactionFull(policyTransaction, false);
			BordereauxProcesses bordereauxProcesses = new BordereauxProcesses();
			ptAllList = bordereauxProcesses.getBordereauTransactions(currentPolicyTransaction.getPolicyTerm().getInsurancePolicy().getBasePolicyNum());
		} catch (InsurancePolicyException e) {
			e.printStackTrace();
		}



		return ptAllList;
	}
	
	public List<PolicyTransaction> getAllReissueAndAdjustmentPolicyTransactions(PolicyTransaction currentPolicyTransaction){
		List<PolicyTransaction> ptAllReissueAdjusmentList = new ArrayList<PolicyTransaction>(); 
		try {
			//policyTransaction = policyUIHelper.getPolicyProcessing().loadPolicyTransactionFull(policyTransaction, false);
			BordereauxProcesses bordereauxProcesses = new BordereauxProcesses();
			ptAllReissueAdjusmentList = bordereauxProcesses.getBordereauReissueAndAdjustmentTransactions(currentPolicyTransaction.getPolicyTerm().getInsurancePolicy().getBasePolicyNum());
		} catch (InsurancePolicyException e) {
			e.printStackTrace();
		}



		return ptAllReissueAdjusmentList;
	}
	
	public void getBourdereauxCsvCustomerSide(String delimiter){
		PolicyTransaction policyTransaction=null;
		if (policyUIHelper!=null ) {
			policyTransaction=policyUIHelper.getCurrentPolicyTransaction();
		}
    
    	List<PolicyTransaction> ptListCurrentTrans=getListCurrentPolTran(policyTransaction);
    	List<PolicyTransaction> ptAllReissueAdjustmentsList=getAllReissueAndAdjustmentPolicyTransactions(policyTransaction);
		
    	DSPolicyBordereau dv = new DSPolicyBordereau();
		StreamResource result = dv.downloadFile(ptListCurrentTrans, delimiter, false, ptAllReissueAdjustmentsList, policyProcessing);
        final StreamRegistration registration = VaadinSession.getCurrent().getResourceRegistry().registerResource(result);
        UI.getCurrent().getPage().executeJavaScript("window.open($0, $1)", registration.getResourceUri().toString(), "_blank");
		
	}
	
	
	
	public void getBourdereauxForm(String delimiter){
		dialog = createBordereauxSummarize(labels.getString("BordereauSummaryTitleFormLabel"), UIConstants.UIACTION_Update, delimiter);
		dialog.setWidth("500px");
		dialog.getButton(UIConstants.UIACTION_Save).setVisible(false);
		dialog.getButton(UIConstants.UIACTION_Cancel).setVisible(false);
		dialog.open();
		
	}
	
	
	protected DSUpdateDialog<BordereauSummaryForm> createBordereauxSummarize(String title, String action, String delimiter) {
		DSUpdateDialog<BordereauSummaryForm> dialog = (DSUpdateDialog<BordereauSummaryForm>) createDialog(title, 
				createBordereauxSummarizeForm(action, UIConstants.UICONTAINERTYPES.Dialog, delimiter), 
																action);
		return dialog;
	}
	
	protected BordereauSummaryForm createBordereauxSummarizeForm(String action, UIConstants.UICONTAINERTYPES containerType, String delimiter) {
		BordereauSummaryForm form = new BordereauSummaryForm(delimiter, policyProcessing, labels);
		return form;
	}
	
	
	public void getBourdereauxAllForm(String delimiter){
		bordereauAllDialog = createBordereauxAll(labels.getString("BordereauAllTitleFormLabel"), UIConstants.UIACTION_Update, delimiter);
		bordereauAllDialog.setWidth("500px");
		bordereauAllDialog.getButton(UIConstants.UIACTION_Save).setVisible(false);
		bordereauAllDialog.getButton(UIConstants.UIACTION_Cancel).setVisible(false);
		bordereauAllDialog.open();
		
	}
	
	protected DSUpdateDialog<BordereauAllForm> createBordereauxAll(String title, String action, String delimiter) {
		DSUpdateDialog<BordereauAllForm> dialog = (DSUpdateDialog<BordereauAllForm>) createDialog(title, 
				createBordereauxAllForm(action, UIConstants.UICONTAINERTYPES.Dialog, delimiter), 
																action);
		return dialog;
	}
	
	protected BordereauAllForm createBordereauxAllForm(String action, UIConstants.UICONTAINERTYPES containerType, String delimiter) {
		PolicyTransaction<?> pt=null;
		if (policyUIHelper!=null ) {
			//policyTransactions = policyUIHelper.getCurrentPolicy().getTransactionHistores(); 
			pt = policyUIHelper.getCurrentPolicyTransaction();
			
		}
		BordereauAllForm form = new BordereauAllForm(delimiter, pt, policyProcessing, labels);
		return form;
	}

}

