package com.echelon.ui.helpers.policy;

import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.uicommon.components.DSTreeGridItem;
import com.ds.ins.utils.Constants;
import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.ui.components.baseclasses.BaseTabLayout;
import com.echelon.ui.components.baseclasses.dto.CurrentPolicy;
import com.echelon.ui.components.baseclasses.policy.BasePolicyMessageList;
import com.echelon.utils.LHTConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.LookupUIHelper;
import com.echelon.ui.helpers.ProductConfigUIHelper;
import com.echelon.ui.helpers.UIHelper;
import com.echelon.ui.session.BrokerUI;
import com.echelon.ui.session.UIFactory;
import com.ds.ins.processes.policy.PolicyProcessesFactory;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.system.UserProfile;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.icon.Icon;

public class PolicyUIHelper implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7015214058637689683L;
	
	public PolicyUIHelper() {
		super();
	}
	
	protected UIHelper uiHelper() {
		return BrokerUI.getHelperSession().getUiHelper();
	}
	
	protected LookupUIHelper lookupUIHelper() {
		return BrokerUI.getHelperSession().getLookupUIHelper();
	}
	
	protected ProductConfigUIHelper configUIHelper() {
		return BrokerUI.getHelperSession().getConfigUIHelper();
	}
	
	public IGenericPolicyProcessing getPolicyProcessing() {
		String productCd = null;
		if (BrokerUI.getUserSession() != null) {
			if (BrokerUI.getUserSession().getPolicyProductConfig() != null) {
				productCd = BrokerUI.getUserSession().getPolicyProductConfig().getProductCd();
			}
			else if (getCurrentPolicyVersion() != null) {
				productCd = getCurrentPolicyVersion().getInsurancePolicy().getProductCd();
			}
		}
		
		return getPolicyProductProcessing(productCd);
	}
	
	public IGenericPolicyProcessing getPolicyProductProcessing(String productCd) {
		if (productCd == null || productCd.trim().length() < 1) {
			return PolicyProcessesFactory.getInstance().newPolicyProcesses("");
		}
		
		return (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses(productCd);
	}
	
	public void refreshPolicyMenuItems() {
		if (BrokerUI.getUserSession() != null &&
				BrokerUI.getUserSession().getMainView() != null) {
			BrokerUI.getUserSession().getMainView().refreshPolicyMenuItems();
		}
	}
	
	public void openPolicySearchView() {
		UI.getCurrent().navigate(UIConstants.POLICYSEARCH_ROUTE);
	}
	
	public void openPolicy(PolicyTransaction polTransaction) {
		if (polTransaction == null || polTransaction.getPolicyVersion() == null) {
			openPolicySearchView();
		}
		else {
			updateUIWithPolicyVersion(polTransaction.getPolicyVersion(), polTransaction, null, null);
			if (polTransaction != null) {
				UI.getCurrent().navigate(UIConstants.POLICY_ROUTE);
				BrokerUI.getUserSession().getPolicyView().selectTreeNode(POLICYTREENODES.Policy.name(), null);
				return;
			}

			openPolicySearchView();	
		}
	}
	
	public void reopenPolicy(PolicyTransaction polTransaction) {
		DSTreeGridItem treeGridItem = BrokerUI.getUserSession().getPolicyNodeItem();
		if (treeGridItem != null) {
			reopenPolicyAndNode(polTransaction, treeGridItem.getNodeType(), treeGridItem.getNodeObject(), null);
		}
	}
	
	public void reopenPolicy(PolicyTransaction polTransaction, UIConstants.POLICYTREENODES itemNodeType, Object itemNodeObject) {
		if (itemNodeType != null) {
			reopenPolicyAndNode(polTransaction, itemNodeType.name(), itemNodeObject, null);
		}
	}
	
	public void reopenPolicy(PolicyTransaction polTransaction, UIConstants.POLICYTREENODES itemNodeType, Object itemNodeObject, Object lastData) {
		if (itemNodeType != null) {
			reopenPolicyAndNode(polTransaction, itemNodeType.name(), itemNodeObject, lastData);
		}
	}
	
	protected void reopenPolicyAndNode(PolicyTransaction polTransaction, String itemNodeType, Object itemNodeObject, Object lastData) {
		try {
			Integer tabIndex = BrokerUI.getUserSession().getOpenedTabIndex();
			
			if (polTransaction != null && polTransaction.getPolicyVersion() != null) {
				if (POLICYTREENODES.TransactionHistory.name().equalsIgnoreCase(itemNodeType)) {
					
					List<PolicyHistoryDTO> transactions = null;
					if (itemNodeObject != null && itemNodeObject instanceof List) {
						transactions = (List<PolicyHistoryDTO>) itemNodeObject;
					}
					
					updateUIWithPolicyVersion(polTransaction.getPolicyVersion(), polTransaction, 
											transactions,
											lastData);
				}
				else {
					updateUIWithPolicyVersion(polTransaction.getPolicyVersion(), polTransaction, null, lastData);
				}
				
				UI.getCurrent().navigate(UIConstants.POLICY_ROUTE);
				BrokerUI.getUserSession().getPolicyView().selectTreeNode(itemNodeType, itemNodeObject);
				
				if (tabIndex != null && tabIndex >= 0 &&
						BrokerUI.getUserSession().getPolicyNodeContent() != null &&
						BrokerUI.getUserSession().getPolicyNodeContent() instanceof BaseTabLayout) {
					((BaseTabLayout)BrokerUI.getUserSession().getPolicyNodeContent()).openPage(tabIndex);
				}
				
				//BrokerUI.getUserSession().getCurrentPolicy().resetLastData();
				
				if (POLICYTREENODES.Messages.name().equalsIgnoreCase(itemNodeType)) {
					BasePolicyMessageList policyMessageList = (BasePolicyMessageList)BrokerUI.getUserSession().getPolicyNodeContent();
					policyMessageList.loadData();
				}

				return;
			}
		} catch (Exception e) {
			uiHelper().notificationException(e);
		}
		
		BrokerUI.getUserSession().getCurrentPolicy().resetLastData();
		openPolicySearchView();
	}
	
	public void selectPolicyItem(UIConstants.POLICYTREENODES itemNodeType, Object itemNodeObject) {
		if (itemNodeType != null &&
				BrokerUI.getUserSession() != null &&
				getCurrentPolicyVersion() != null &&
				BrokerUI.getUserSession().getPolicyView() != null) {
			BrokerUI.getUserSession().getPolicyView().selectTreeNode(itemNodeType.name(), itemNodeObject);
		}
	}
	
	public void updateSessionProductConfig(IGenericProductConfig productConfig, LocalDate effective) {
		BrokerUI.getUserSession().setPolicyProductConfig(productConfig);
		BrokerUI.getUserSession().setLookupConfig(lookupUIHelper().getLookupConfig(productConfig.getProductCd(), effective));
	}
	
	public void updateSessionProductConfigWithPolicyVersion() {
		if (BrokerUI.getUserSession() != null && getCurrentPolicyVersion() != null) {
			BrokerUI.getUserSession().setPolicyProductConfig(configUIHelper().getProductConfig(getCurrentPolicyVersion().getInsurancePolicy().getProductCd(), 
																getCurrentPolicyVersion().getProductConfigurationVersion()));
			
			BrokerUI.getUserSession().setLookupConfig(lookupUIHelper().getLookupConfig(getCurrentPolicyVersion().getInsurancePolicy().getProductCd(), null));
			
			BrokerUI.getUserSession().setLifecycleStatusConfig(configUIHelper().getLifecycleStatusConfig(
											ConfigConstants.LIFECYCLETYPE_Policy,
											getCurrentPolicyVersion().getInsurancePolicy().getProductCd(), null,
											getCurrentPolicyTransaction().getPolicyVersion().getBusinessStatus(),
											getCurrentPolicyTransaction().getPolicyVersion().getPolicyTxnType()));
		}
	}
	
	public void updateWithPolicyVersion(PolicyVersion policyVersion, PolicyTransaction policyTransaction,
										List<PolicyHistoryDTO> transactionHistores,
										Object lastData) {
		updateCurrentPolicy(policyVersion, policyTransaction, transactionHistores, lastData);
		if (policyVersion != null) {
			updateSessionProductConfigWithPolicyVersion();
		}
	}
	
	private void updateCurrentPolicy(PolicyVersion policyVersion, PolicyTransaction policyTransaction,
									List<PolicyHistoryDTO> transactionHistores,
									Object lastData) {
		if (BrokerUI.getUserSession() != null) {
			SpecialtyPolicyDTO specialtyPolicyDTO = getSpecialtyPolicyDTO(policyVersion, policyTransaction);
			
			BrokerUI.getUserSession().setCurrentPolicy(policyVersion, policyTransaction, specialtyPolicyDTO, lastData);
			BrokerUI.getUserSession().getCurrentPolicy().setTransactionHistores(transactionHistores);
		}
	}
	
	private void updateUIWithPolicyVersion(PolicyVersion policyVersion, PolicyTransaction policyTransaction,
											List<PolicyHistoryDTO> transactionHistores,
											Object lastData) {
		//boolean needReset = false;
		
		if (BrokerUI.getUserSession() != null) {
			/*
			if (getCurrentPolicyVersion != null) {
				if (policyVersion == null) {
					needReset = true;
				}
				else {
					PolicyVersion prevPolicyVersion = getCurrentPolicyVersion;
					if (prevPolicyVersion.getProductConfigurationVersion() == null || 
						policyVersion.getProductConfigurationVersion() == null ||
						!prevPolicyVersion.getProductConfigurationVersion()
											.equalsIgnoreCase(policyVersion.getProductConfigurationVersion())) {
						needReset = true;
					}
					
					if (!needReset &&
						prevPolicyVersion.getInsurancePolicy().getProductCd() == null || 
						policyVersion.getInsurancePolicy().getProductCd() == null ||
						prevPolicyVersion.getInsurancePolicy().getProductCd() !=
											policyVersion.getInsurancePolicy().getProductCd()) {
						needReset = true;
					}
				}
			}
			else {
				needReset = true;
			}
			*/
			
			/*
			Map<Long, SpecialtyVehicleRiskDTO> vehicleRiskDTOByPk = getVehicleRiskDTOByPk(policyVersion, policyTransaction);
			
			DSBrokerUI.getUserSession().setCurrentPolicy(policyVersion, policyTransaction, vehicleRiskDTOByPk, lastData);
			DSBrokerUI.getUserSession().getCurrentPolicy().setTransactionHistores(transactionHistores);
			*/
			
			updateCurrentPolicy(policyVersion, policyTransaction, transactionHistores, lastData);
			
			//if (needReset) { screen not refreshing probably
				resetPolicyViewComponents();
				if (policyVersion != null) {
					BrokerUI.getUserSession().setPolicyTreeNavigator(UIFactory.getInstance().getPolicyTreeNavigator(
													policyVersion.getInsurancePolicy().getProductCd()
												));
					updateSessionProductConfigWithPolicyVersion();
				}
			//}
		}
	}
	
	// Get Original Unites
	protected SpecialtyPolicyDTO getSpecialtyPolicyDTO(PolicyVersion policyVersion, PolicyTransaction policyTransaction) {
		SpecialtyPolicyDTO specialtyPolicyDTO = null;
		
		try {
			ISpecialtyAutoPolicyProcessing autoPolicyProcessing = (ISpecialtyAutoPolicyProcessing) getPolicyProductProcessing(policyVersion.getInsurancePolicy().getProductCd());
			specialtyPolicyDTO = autoPolicyProcessing.getSpecialtyPolicyDTO(policyTransaction);
		} catch (Exception e) {
			uiHelper().notificationException(e);
		}
		
		return specialtyPolicyDTO;
	}
	
	public void resetSessionPolicyVersion() {
		if (BrokerUI.getUserSession() != null) {
			BrokerUI.getUserSession().resetSessionPolicyVersion();			
		}
	}
	
	private void resetPolicyViewComponents() {
		if (BrokerUI.getUserSession() != null) {
			BrokerUI.getUserSession().resetPolicyViewComponents();
		}
	}
	
	public boolean isCurrentTransactionEditable() {
		if (BrokerUI.getUserSession() != null && 
			BrokerUI.getUserSession().getUserProfile() != null &&
			BrokerUI.getUserSession().getUserProfile().isHasFullAccess()) {
			
			if (configUIHelper().getCurrentLifecycleStatusConfig() != null) {
				return configUIHelper().getCurrentLifecycleStatusConfig().isTransactionEditable();
			}
		}
		
		return false; // Change it to false when all status values are correct
					  // true for missing status and/or transaction type.
	}
	
	public PolicyTransaction<?> getParentPolicyTransaction(Object data) {
		if (data != null) {
			if (data instanceof PolicyTransaction) {
				return (PolicyTransaction) data;
			}
			else if (data instanceof Risk) {
				Risk risk = (Risk)data;
				if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
					return risk.getSubPolicy().getPolicyTransaction();
				}
				else {
					return risk.getPolicyTransaction();
				}
			}
			else if (data instanceof SubPolicy<?>) {
				return ((SubPolicy<?>)data).getPolicyTransaction();
			}
		}
		
		return getCurrentPolicyTransaction();
	}
	
	public CurrentPolicy getCurrentPolicy() {
		if (BrokerUI.getUserSession() != null) {
			return BrokerUI.getUserSession().getCurrentPolicy();
		}
		
		return null;
	}
	
	public PolicyTransaction<?> getCurrentPolicyTransaction() {
		if (BrokerUI.getUserSession() != null && BrokerUI.getUserSession().getCurrentPolicy() != null) {
			return BrokerUI.getUserSession().getCurrentPolicy().getPolicyTransaction();
		}
		
		return null;
	}
	
	public PolicyVersion getCurrentPolicyVersion() {
		if (BrokerUI.getUserSession() != null && BrokerUI.getUserSession().getCurrentPolicy() != null) {
			return BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion();
		}
		
		return null;
	}
	
	public UserProfile getCurrentUser() {
		if (BrokerUI.getUserSession() != null) {
			return BrokerUI.getUserSession().getUserProfile();
		}
		
		return null;
	}
	
	public PolicyLocation getInsuredLocation() {
		PolicyLocation policyLocation = null;
		
		if (getCurrentPolicyTransaction() != null) {
			policyLocation = getCurrentPolicyTransaction().findRiskByType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION)
								.stream().map(o -> (PolicyLocation)o)
								.filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId()))
								.findFirst().orElse(null);
		}
		
		return policyLocation;
	}
	
	public Boolean isEntityChangedInCurrentVersion(IBusinessEntity businessEntity) {
		Boolean result = null;
		
		if (getAuditable(businessEntity) != null && isBusinesEntityIndicatorColumnRequired()) {
			IAuditable auditable = getAuditable(businessEntity);
			if (auditable.isNew()) {
				result = true;
			}
			else if (auditable.isModified()) {
				result = true;
			}
			else if (auditable.isDeleted()) { //not to include auditable.wasDeleted(); it was from prev version
				result = true;
			}
		}
		
		return result;
	}
	
	public Icon getBusinessStatusIcon(IBusinessEntity businessEntity) {
		Icon icon = null;
		
		if (getAuditable(businessEntity) != null && isBusinesEntityIndicatorColumnRequired()) {
			IAuditable auditable = getAuditable(businessEntity);
			if (auditable.isNew()) {
				icon = uiHelper().createIconNew();
			}
			else if (auditable.isModified()) {
				icon = uiHelper().createIconModified();
			}
			else if (auditable.isDeleted() || auditable.wasDeleted()) {
				icon = uiHelper().createIconDeleted();
			}
		}
		
		return icon;
	}
	
	public boolean isBusinesEntityIndicatorColumnRequired() {
		return BrokerUI.getUserSession().getCurrentPolicy() != null &&
				!Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType()) &&
				!Constants.VERS_TXN_TYPE_RENEWAL.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType()) &&
				!Constants.VERS_TXN_TYPE_REISSUE.equalsIgnoreCase(BrokerUI.getUserSession().getCurrentPolicy().getPolicyVersion().getPolicyTxnType())
				;
	}
	
	public IAuditable getAuditable(IBusinessEntity businessEntity) {
		if (businessEntity != null && businessEntity instanceof IAuditable) {
			return (IAuditable)businessEntity;
		}
		
		return null;
	}
	
	public boolean needToLoadTransactionData() {
		if (getCurrentPolicyTransaction() != null) {
			return needToLoadData(getCurrentPolicyTransaction().getCoverageDetail());
		}
		
		return false;
	}
	
	public boolean needToLoadRiskData(Risk risk) {
		if (risk != null) {
			return needToLoadData(risk.getCoverageDetail());
		}
		
		return false;
	}
	
	public boolean needToLoadSubPolicyData(SubPolicy<?> subPolicy) {
		if (subPolicy != null) {
			return needToLoadData(subPolicy.getCoverageDetail());
		}
		
		return false;
	}
	
	public boolean needToLoadSubPolicyRiskData(Risk risk) {
		if (risk != null) {
			return needToLoadData(risk.getCoverageDetail());
		}
		
		return false;
	}
	
	private boolean needToLoadData(CoverageDetails coverageDetails) {
		boolean toLoad = false;
		try {
			if (coverageDetails != null) {
				coverageDetails.getCoverages().stream().count();
				coverageDetails.getEndorsements().stream().count();
			}
		} catch (Exception e) {
			toLoad = true;
		}
		
		return toLoad;
	}

	// ratingWarnings format = message code | message values
	public List<String> buildRatingWarningMessagList(List<String> ratingWarnings) {
		List<String> results = new ArrayList<String>();
		
		if (ratingWarnings != null && !ratingWarnings.isEmpty()) {
			ratingWarnings.stream().forEach(o -> {
				String[] warnEntries = o.split(";");
				String msgCode  = warnEntries[0];
				String msgValue = null;
				if (warnEntries.length > 1) {
					msgValue = warnEntries[1];
				}
				
				String msg = uiHelper().getSessionLabels().getString(msgCode);
				if (msgValue != null) {
					msg = MessageFormat.format(msg, msgValue);
				}
				if (!results.contains(msg)) {
					results.add(msg);
				}
			});
		}
		
		return results;
	}
	
	public PolicyLocation getCurrentInsurerLocation() {
		PolicyLocation result = getPolicyInsurerLocation((PolicyTransaction<Risk>) getCurrentPolicyTransaction());
		
		return result;
	}
	
	public PolicyLocation getPolicyInsurerLocation(PolicyTransaction<Risk> policyTransaction) {
		PolicyLocation result = null;
		
		if (policyTransaction != null) {
			result = policyTransaction.getPolicyRisks().stream()
							.filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
							.map(o -> (PolicyLocation)o)
							.filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId()))
							.findFirst().orElse(null);
		}
		
		return result;
	}
	
	public String getPolicyNumberPrefix(String productCode) {
		if (StringUtils.isNotBlank(productCode)) {
			switch (productCode) {
			case ConfigConstants.PRODUCTCODE_LHT:
				return LHTConstants.LHT_POLICY_PREFIX;

			case ConfigConstants.PRODUCTCODE_TOWING:
				return SpecialtyAutoConstants.SPECAUTO_POLICY_PREFIX;

			default:
				break;
			}
		}
		
		return null;
	}
	
	public String getQuoteNumberPrefix(String productCode) {
		if (StringUtils.isNotBlank(productCode)) {
			switch (productCode) {
			case ConfigConstants.PRODUCTCODE_LHT:
				return LHTConstants.LHT_QUOTE_PREFIX;

			case ConfigConstants.PRODUCTCODE_TOWING:
				return SpecialtyAutoConstants.SPECAUTO_QUOTE_PREFIX;

			default:
				break;
			}
		}
		
		return null;
	}
}
