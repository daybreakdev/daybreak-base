package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.VehicleDriverForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.VehicleDriverProcessor;
import com.echelon.utils.SpecialtyAutoConstants;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class VehicleDriverUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5360370067125741250L;

	public VehicleDriverUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	public VehicleDriverForm createDriverForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		VehicleDriverForm form = null;

		form = new VehicleDriverForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<VehicleDriverForm> createVehicleDriverDialog(String title, String action) {
		DSUpdateDialog<VehicleDriverForm> dialog = (DSUpdateDialog<VehicleDriverForm>) createDialog(title, createDriverForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupVehicleDriverAction(String title, String action, SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		DSUpdateDialog<VehicleDriverForm> dialog = createVehicleDriverDialog(title, action);
		dialog.setHeight("500px");
		dialog.setDataStore(new DSDialogDataStore(vehicle, driver));
		dialog.getContentComponent().loadData(vehicle, driver);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newVehicleDriverAction(SpecialtyVehicleRisk vehicle) {
		createDriverTaskProcessor(null).start(
				createVehicleDriverProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null, vehicle)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<VehicleDriverForm> dialog = (DSUpdateDialog<VehicleDriverForm>) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriver driver = (VehicleDriver) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(driver)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createDriverTaskProcessor(dialog).start(
					createVehicleDriverProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									driver, vehicle)
					);
		}
	}

	public void updateVehicleDriverAction(SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		String title = uiHelper.getSessionLabels().getString("VehicleDriverUpdateDialogTitle");
		setupVehicleDriverAction(title, UIConstants.UIACTION_Update, vehicle, driver);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<VehicleDriverForm> dialog = (DSUpdateDialog<VehicleDriverForm>) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriver driver = (VehicleDriver) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(driver)) {

			createDriverTaskProcessor(dialog).start(
					createVehicleDriverProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											driver, vehicle));
		}
	}
	
	public void deleteVehicleDriverAction(SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("VehicleDriverDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicle, driver));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriver driver = (VehicleDriver) dialog.getDataStore().getData2();
				
		//ENFO-209
		//Continue if currently selected driver is of Principal type
		if (!isPrincileDriverTypeSelected(vehicle, driver)) {
			
			//Filter through the drivers assigned to the vehicle
			//Set replacement as the first driver assigned as secondary type, if there is none, set replacement to null
			VehicleDriver replacement = vehicle.getVehicleDrivers().stream()
					.filter(o -> SpecialtyAutoConstants.ATTACHDRIVER_TYPE_SECONDARY.equalsIgnoreCase(o.getDriverType()))
					.findFirst().orElse(null);
			
			//If the replacement vehicle driver is null, the current driver cannot be deleted
			if (replacement == null) {
				uiHelper.notificationError(uiHelper.getSessionLabels().getString("VehicleDriverFormDeleteErrorMsg"));
				dialog.close();
			}
			
			else {
				//Set the replacement driver type as principal
				replacement.setDriverType(SpecialtyAutoConstants.ATTACHDRIVER_TYPE_PRINCIPAL);
				createDriverTaskProcessor(dialog).start(
						createVehicleDriverProcessor(UIConstants.UIACTION_Delete, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									driver, vehicle)
						);
			}
		}
		//Continue if currently selected driver is not of Principal type
		else {
			createDriverTaskProcessor(dialog).start(
					createVehicleDriverProcessor(UIConstants.UIACTION_Delete, 
								policyUIHelper.getCurrentPolicyTransaction(), 
								driver, vehicle)
					);	
		}
	}
		
	
	public void undeleteVehicleDriverAction(SpecialtyVehicleRisk vehicle, VehicleDriver driver) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("VehicleDriverUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicle, driver));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriver driver = (VehicleDriver) dialog.getDataStore().getData2();
		createDriverTaskProcessor(dialog).start(
				createVehicleDriverProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							driver, vehicle)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriver driver, String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Vehicle, vehicle);
	}

	public List<VehicleDriver> sortDriverList(Set<VehicleDriver> drivers) {
		List<VehicleDriver> result = new ArrayList<VehicleDriver>(); 
		if (drivers != null) {
			result.addAll(drivers);
		}
		
		return result;
	}
	
	protected VehicleDriverProcessor createVehicleDriverProcessor(String action, PolicyTransaction policyTransaction, 
													VehicleDriver driver, SpecialtyVehicleRisk vehicle) {
		return new VehicleDriverProcessor(action, policyTransaction).withParams(vehicle, driver);
	}
	
	protected TaskProcessor createDriverTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					VehicleDriverProcessor processor = (VehicleDriverProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupVehicleDriverAction(uiHelper.getSessionLabels().getString("VehicleDriverNewDialogTitle"), 
											UIConstants.UIACTION_New, processor.getVehicle(), processor.getVehicleDriver());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriver(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriver(), UIConstants.UIACTION_Delete);
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriver(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}

	public List<Driver> getAvailableDrivers(SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) {
		List<Long> driverFilter = new ArrayList<Long>();
		if (vehicle != null && vehicle.getVehicleDrivers() != null && !vehicle.getVehicleDrivers().isEmpty()) {
			for(VehicleDriver vd : vehicle.getVehicleDrivers().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())) {
				// include current driver in the list
				if (vehicleDriver != null && vehicleDriver.getDriver() != null &&
					vehicleDriver.getDriver().equals(vd.getDriver())) {
					continue;
				}
				
				driverFilter.add(vd.getDriver().getPersonPk());
			}
		}
		
		List<Driver> drivers = (new DriverUIHelper()).getDriverList(driverFilter);
		return drivers;
	}
	
	public boolean isPrincileDriverTypeSelected(SpecialtyVehicleRisk vehicle, VehicleDriver vehicleDriver) {
		boolean result = false;
		
		if (vehicle != null && vehicle.getVehicleDrivers() != null && !vehicle.getVehicleDrivers().isEmpty()) {
			result = vehicle.getVehicleDrivers().stream().filter(o -> (o.isPending() || o.isActive())
														&& !o.equals(vehicleDriver)
														&& SpecialtyAutoConstants.ATTACHDRIVER_TYPE_PRINCIPAL.equalsIgnoreCase(o.getDriverType()))
														.findFirst().isPresent();
		}
		
		return result;
	}
}
