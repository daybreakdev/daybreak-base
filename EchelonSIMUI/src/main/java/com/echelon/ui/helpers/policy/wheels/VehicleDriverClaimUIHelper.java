package com.echelon.ui.helpers.policy.wheels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.echelon.ui.components.baseclasses.TaskProcessor;
import com.echelon.ui.components.policy.wheels.VehicleDriverClaimForm;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.BaseSpecialtyPolicyContentUIHelper;
import com.echelon.ui.process.wheels.VehicleDriverClaimProcessor;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.prodconf.baseclasses.LookupTableItem;
import com.ds.ins.uicommon.components.DSConfirmDialog;
import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSTaskRun;
import com.ds.ins.uicommon.components.DSUpdateDialog;
import com.ds.ins.uicommon.components.IDSDialog;
import com.ds.ins.uicommon.components.DSUpdateDialog.SaveEvent;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.ds.ins.uicommon.constants.DSUIConstants.VALIDATIONSTATUSES;
import com.vaadin.flow.component.UI;

public class VehicleDriverClaimUIHelper extends BaseSpecialtyPolicyContentUIHelper implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9029486160056693002L;
	private DriverUIHelper driverUIHelper;

	public VehicleDriverClaimUIHelper() {
		super();
		// TODO Auto-generated constructor stub
		
		this.driverUIHelper = new DriverUIHelper();
	}
	public VehicleDriverClaimForm createDriverClaimForm(String action, UIConstants.UICONTAINERTYPES containerType) {
		VehicleDriverClaimForm form = null;

		form = new VehicleDriverClaimForm(action, containerType);

		return form;
	}
	
	protected DSUpdateDialog<VehicleDriverClaimForm> createVehicleDriverClaimDialog(String title, String action) {
		DSUpdateDialog<VehicleDriverClaimForm> dialog = (DSUpdateDialog<VehicleDriverClaimForm>) createDialog(title, createDriverClaimForm(action, UICONTAINERTYPES.Dialog), action);
		return dialog;
	}
	
	protected void setupVehicleDriverClaimAction(String title, String action, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		DSUpdateDialog<VehicleDriverClaimForm> dialog = createVehicleDriverClaimDialog(title, action);
		dialog.setDataStore(new DSDialogDataStore(vehicle, vehDrvClaim));
		dialog.getContentComponent().loadData(vehicle, vehDrvClaim);
		dialog.getContentComponent().enableEdit();
		dialog.open();
	}
	
	public void newVehicleDriverClaimAction(SpecialtyVehicleRisk vehicle) {
		createDriverClaimTaskProcessor(null).start(
				createVehicleDriverClaimProcessor(UIConstants.UIACTION_New, 
										policyUIHelper.getCurrentPolicyTransaction(), 
										null, vehicle)
				);
	}
	
	@Override
	protected void onSaveNew(SaveEvent event) {
		DSUpdateDialog<VehicleDriverClaimForm> dialog = (DSUpdateDialog<VehicleDriverClaimForm>) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriverClaim vehDrvClaim  = (VehicleDriverClaim) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit()) {
			try {
				if (!dialog.getContentComponent().saveEdit(vehDrvClaim)) {
					return;
				}
			} catch (Exception e) {
				uiHelper.notificationException(e);
				return;
			}
			
			createDriverClaimTaskProcessor(dialog).start(
					createVehicleDriverClaimProcessor(UIConstants.UIACTION_SaveNew, 
									policyUIHelper.getCurrentPolicyTransaction(), 
									vehDrvClaim, vehicle)
					);
		}
	}

	public void updateVehicleDriverClaimAction(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		String title = uiHelper.getSessionLabels().getString("VehicleDriverClaimUpdateDialogTitle");
		setupVehicleDriverClaimAction(title, UIConstants.UIACTION_Update, vehicle, vehDrvClaim);
	}

	@Override
	protected void onSaveUpdate(SaveEvent event) {
		DSUpdateDialog<VehicleDriverClaimForm> dialog = (DSUpdateDialog<VehicleDriverClaimForm>) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriverClaim vehDrvClaim  = (VehicleDriverClaim) dialog.getDataStore().getData2();
		if (dialog.getContentComponent().validateEdit() && dialog.getContentComponent().saveEdit(vehDrvClaim)) {

			createDriverClaimTaskProcessor(dialog).start(
					createVehicleDriverClaimProcessor(UIConstants.UIACTION_Save, 
											policyUIHelper.getCurrentPolicyTransaction(), 
											vehDrvClaim, vehicle));
		}
	}
	
	public void deleteVehicleDriverClaimAction(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		DSConfirmDialog dialog = createConfirmDeleteDialog(uiHelper.getSessionLabels().getString("VehicleDriverClaimDeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicle, vehDrvClaim));
		dialog.open();
	}
	
	protected void onDelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriverClaim vehDrvClaim  = (VehicleDriverClaim) dialog.getDataStore().getData2();
				
			createDriverClaimTaskProcessor(dialog).start(
					createVehicleDriverClaimProcessor(UIConstants.UIACTION_Delete, 
								policyUIHelper.getCurrentPolicyTransaction(), 
								vehDrvClaim, vehicle)
					);
	}
	
	public void undeleteVehicleDriverClaimAction(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim ) {
		DSConfirmDialog dialog = createConfirmUndeleteDialog(uiHelper.getSessionLabels().getString("VehicleDriverClaimUndeleteDialogMessage"));
		dialog.setDataStore(new DSDialogDataStore(vehicle, vehDrvClaim));
		dialog.open();
	}
	
	protected void onUndelete(DSConfirmDialog.ConfirmEvent event) {
		DSConfirmDialog dialog = (DSConfirmDialog) event.getSource();
		SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) dialog.getDataStore().getData1();
		VehicleDriverClaim vehDrvClaim  = (VehicleDriverClaim) dialog.getDataStore().getData2();
		createDriverClaimTaskProcessor(dialog).start(
				createVehicleDriverClaimProcessor(UIConstants.UIACTION_Undelete, 
							policyUIHelper.getCurrentPolicyTransaction(), 
							vehDrvClaim, vehicle)
				);
	}

	private void reopen(PolicyTransaction policyTransaction, SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim , String action) {
		policyUIHelper.reopenPolicy(policyTransaction, UIConstants.POLICYTREENODES.Vehicle, vehicle);
	}

	public List<VehicleDriverClaim> sortDriverClaimList(Set<VehicleDriverClaim> driverClaims) {
		List<VehicleDriverClaim> result = new ArrayList<VehicleDriverClaim>(); 
		if (driverClaims != null) {
			result.addAll(driverClaims);
		}
		
		return result;
	}
	
	protected VehicleDriverClaimProcessor createVehicleDriverClaimProcessor(String action, PolicyTransaction policyTransaction, 
													VehicleDriverClaim vehDrvClaim , SpecialtyVehicleRisk vehicle) {
		return new VehicleDriverClaimProcessor(action, policyTransaction).withParams(vehicle, vehDrvClaim);
	}
	
	protected TaskProcessor createDriverClaimTaskProcessor(IDSDialog dialog) {
		return new TaskProcessor(UI.getCurrent(), dialog) {

			@Override
			protected boolean processEnd(DSTaskRun taskRun, VALIDATIONSTATUSES processStatus) {
				if (VALIDATIONSTATUSES.Succeed.equals(processStatus)) {
					VehicleDriverClaimProcessor processor = (VehicleDriverClaimProcessor)taskRun.getProcessor();
					
					switch (processor.getProcessAction()) {
					case UIConstants.UIACTION_New:						
						setupVehicleDriverClaimAction(uiHelper.getSessionLabels().getString("VehicleDriverClaimNewDialogTitle"), 
											UIConstants.UIACTION_New, processor.getVehicle(), processor.getVehicleDriverClaim());
						
						break;
						
					case UIConstants.UIACTION_SaveNew:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), null, UIConstants.UIACTION_Save);
						break;
						
					case UIConstants.UIACTION_Save:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriverClaim(), UIConstants.UIACTION_Save);
						break;

					case UIConstants.UIACTION_Delete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriverClaim(), UIConstants.UIACTION_Delete);
						break;

					case UIConstants.UIACTION_Undelete:
						dialog.close();
						reopen(processor.getPolicyTransaction(), processor.getVehicle(), processor.getVehicleDriverClaim(), UIConstants.UIACTION_Undelete);
						break;

					default:
						break;
					}
				}
				
				return true;
			}
		};
	}

	public List<LookupTableItem> getAvailableDrivers(SpecialtyVehicleRisk vehicle, VehicleDriverClaim vehDrvClaim) {
		List<Long> driverFilter = new ArrayList<Long>();
		/* Driver can multiple claims
		if (vehicle != null && vehicle.getVehicleDriverClaims() != null && !vehicle.getVehicleDriverClaims().isEmpty()) {
			for(VehicleDriverClaim vdc : vehicle.getVehicleDriverClaims().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())) {
				// include current driver in driverClaimst
				if (vehDrvClaim != null && vehDrvClaim.getDriver() != null &&
					vehDrvClaim.getDriver().equals(vdc.getDriver())) {
					continue;
				}
				
				driverFilter.add(vdc.getDriver().getPersonPk());
			}
		}
		*/
		
		List<Driver> drivers = driverUIHelper.getDriverList(driverFilter);
		
		List<LookupTableItem> results = new ArrayList<LookupTableItem>();
		if (drivers != null && !drivers.isEmpty()) {
			for(Driver drv : drivers) {
				results.add(new LookupTableItem(drv.getPersonPk().toString(), driverUIHelper.getDriverLabel(drv), drv));
			}
		}
		return results;
	}

	public String getDriverLabel(VehicleDriverClaim vehDrvClaim) {
		String result = null;
		
		if (vehDrvClaim != null) {
			result = driverUIHelper.getDriverLabel(vehDrvClaim.getDriver());
		}
		
		return result;
	}
}