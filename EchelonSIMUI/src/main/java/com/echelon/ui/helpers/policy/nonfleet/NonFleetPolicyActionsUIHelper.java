package com.echelon.ui.helpers.policy.nonfleet;

import com.ds.ins.uicommon.components.IDSDialog;
import com.echelon.ui.components.baseclasses.dto.PolicyConfirmDto;
import com.echelon.ui.helpers.policy.SpecialtyAutoPolicyActionsUIHelper;

public class NonFleetPolicyActionsUIHelper extends SpecialtyAutoPolicyActionsUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2992459203321591154L;

	public NonFleetPolicyActionsUIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	// ENFO-291 no need for NonFleet
	@Override
	protected boolean runCreatePolicyRenewalAction(PolicyConfirmDto confirmDto, IDSDialog dialog) {
		// TODO Auto-generated method stub
		return false;
	}

}
