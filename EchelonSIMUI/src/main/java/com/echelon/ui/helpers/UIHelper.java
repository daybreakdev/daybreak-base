package com.echelon.ui.helpers;

import com.rating.rounding.RoundingStrategyFactory;
import com.ds.ins.uicommon.helpers.DSUIHelper;
import com.echelon.ui.components.baseclasses.policy.BaseCovLimitAndDedField;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.helpers.policy.PolicyUIHelper;
import com.echelon.ui.session.BrokerUI;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public class UIHelper extends DSUIHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3536782211326053472L;

	public UIHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected PolicyUIHelper uiPolicyHelper() {
		return BrokerUI.getHelperSession().getPolicyUIHelper();
	}
	
	@Override
	protected String getWaringRequiredMessage() {
		return getSessionLabels().getString(UIConstants.ERRORCODE_GENERAL);
	}	

	@Override
	protected boolean setEnabledFocus(Component field) {
		if (field instanceof BaseCovLimitAndDedField) {
			BaseCovLimitAndDedField limitAndDedField = (BaseCovLimitAndDedField)field;
			if (limitAndDedField.isEnabled() && !limitAndDedField.isReadOnly()) {
				return true;
			}
		}
		
		return super.setEnabledFocus(field);
	}
	
	protected boolean setFocus(Component field) {
		if (field instanceof BaseCovLimitAndDedField) {
			((BaseCovLimitAndDedField)field).setFocus();
			return true;
		}
		
		return super.setFocus(field);
	}
	
	public Double roundNumeric(Double value) {
		if (value == null) {
			return null;
		}
		
		String transType = null;
		if (uiPolicyHelper().getCurrentPolicyTransaction() != null) {
			transType = uiPolicyHelper().getCurrentPolicyTransaction().getTransactionType();
		}
		
		return RoundingStrategyFactory.getInstance().getRoundingStrategy().round(value, transType);
	}
	
	public void setFieldRedLabel(HasStyle ... fields) {
		for(HasStyle field : fields) {
			field.addClassName("ds-red-label");
		}
	}
	
	public Component addGridRowLineTop(boolean hasRowVakue) {
		return addGridRowLine(hasRowVakue, false);
	}
	
	public Component addGridRowLineBottom(boolean hasRowVakue) {
		return addGridRowLine(hasRowVakue, true);
	}
	
	private Component addGridRowLine(boolean hasRowVakue, boolean isBottom) {
		if (hasRowVakue) {
			HorizontalLayout layout = new HorizontalLayout();
			formatFullLayout(layout);
			layout.getElement().getStyle().set((isBottom ? "border-bottom" : "border-top"),
												"1px solid var(--lumo-contrast-20pct)");
			return layout;
		}

		return null;
	}
}

