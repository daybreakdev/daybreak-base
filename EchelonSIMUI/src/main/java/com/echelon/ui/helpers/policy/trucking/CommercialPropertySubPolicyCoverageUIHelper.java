package com.echelon.ui.helpers.policy.trucking;

import com.echelon.ui.components.trucking.CommercialPropertySubPolicyLocCovMultiForm;
import com.echelon.ui.constants.UIConstants.OBJECTTYPES;
import com.echelon.ui.constants.UIConstants.POLICYTREENODES;
import com.echelon.ui.helpers.policy.towing.BaseTowingSubPolicyCoverageUIHelper;

public class CommercialPropertySubPolicyCoverageUIHelper extends
	BaseTowingSubPolicyCoverageUIHelper<CommercialPropertySubPolicyLocCovMultiForm> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6224017536832581274L;

	public CommercialPropertySubPolicyCoverageUIHelper() {
		super(OBJECTTYPES.CPSubPolicy,
			POLICYTREENODES.CommercialPropertySubPolicyNode);
	}
}
