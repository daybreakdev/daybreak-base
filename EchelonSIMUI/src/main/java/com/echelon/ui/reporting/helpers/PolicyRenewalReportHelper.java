package com.echelon.ui.reporting.helpers;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSDownload;
import com.ds.ins.uicommon.components.DSFiltersDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.reports.base.ReportResource;
import com.echelon.reports.base.SimplePDFReport;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.reporting.components.PolicyRenewalReportForm;
import com.echelon.ui.reporting.dto.RenewalReportDto;

public class PolicyRenewalReportHelper extends BaseReportHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1309018649191741771L;
	
	@Override
	public void openDialog() {
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyRenewalReport");
		PolicyRenewalReportForm form = new PolicyRenewalReportForm(UIConstants.UIACTION_View, UICONTAINERTYPES.Dialog);
		DSFiltersDialog dialog = new DSFiltersDialog(reportName, form, this::onActionConfirm);
		RenewalReportDto bean = new RenewalReportDto();
		form.loadData(bean);
		dialog.setDataStore(new DSDialogDataStore(bean));
		dialog.open();
	}
	
	@Override
	protected void onActionConfirm(DSFiltersDialog.ConfirmEvent event) {
		DSFiltersDialog dialog = (DSFiltersDialog) event.getSource();
		Object bean = dialog.getDataStore().getData1();
		DSDownload anchor = dialog.getAnchor();
		PolicyRenewalReportForm form = (PolicyRenewalReportForm) dialog.getContentComponent();
		if(!form.validate()) {
			return;
		}
		try {
			ReportResource resource = buildAndPrintReport(bean);
			anchor.processResource(resource);
			anchor.startDownload();
		} catch (Exception e) {
			uiHelper.notificationError("Error while printing report: " + e.getMessage());
		}
	}
	
	@Override
	protected ReportResource buildAndPrintReport(Object obj) throws Exception {
		SimplePDFReport report = new SimplePDFReport(reportService);
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyRenewalReport");
		report.setReportName(reportName);
		report.setTemplateName("PolicyRenewalReport");
		report.setParameters(getParams(obj));
		return report.print();
	}
	
	@Override
	protected Map<String, Object> getParams(Object obj) {
		RenewalReportDto bean = (RenewalReportDto) obj;
		Map<String, Object> params = new HashMap<String, Object>();
		String fromDate = "";
		String toDate = "";
		String byDays = "";
		int days = 0;
		if (bean.getRangeType().equalsIgnoreCase("Dates")) {
			fromDate = bean.getFromDate().toString();
			toDate = bean.getToDate().toString();
			byDays = "N";
			days = 0;
		} else {
			days = bean.getDays();
			LocalDate now = LocalDate.now();
			LocalDate to = now.plusDays(days);
			fromDate = now.toString();
			toDate = to.toString();
			byDays = "Y";
		}
		params.put("fromDate", fromDate);
		params.put("toDate", toDate);
		params.put("byDays", byDays);
		params.put("days", days);
		return params;
	}

}
