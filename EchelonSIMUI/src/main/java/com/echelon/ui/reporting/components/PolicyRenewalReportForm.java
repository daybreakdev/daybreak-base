package com.echelon.ui.reporting.components;

import java.time.LocalDate;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.reporting.dto.RenewalReportDto;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.radiobutton.RadioButtonGroup;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.renderer.ComponentRenderer;

public class PolicyRenewalReportForm extends BaseLayout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5365755015950923813L;
	private FormLayout 					form;
	private	ComboBox<String>			rangeTypeField;
	private DatePicker 					fromDateField;
	private DatePicker 					toDateField;
	private RadioButtonGroup<Integer> 	daysField;
	private Binder<RenewalReportDto>	binder;
	
	public PolicyRenewalReportForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildLayout();
		buildFields();
		buildForms();
	}
	
	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}
	
	protected void buildFields() {
		rangeTypeField = new ComboBox<String>();
		rangeTypeField.setLabel(labels.getString("ReportFormRangeTypeLabel"));
		rangeTypeField.setItems("Days", "Dates");
		fromDateField = new DatePicker(labels.getString("ReportFormFromDateLabel"));
		toDateField = new DatePicker(labels.getString("ReportFormToDateLabel"));
		daysField = new RadioButtonGroup<Integer>();
		daysField.setItems(30, 60, 90);
		daysField.setRenderer(new ComponentRenderer<Div, Integer>(o -> {
			Div div = new Div();
			div.setText(o + " Days");
			return div;
		}));
		binder = new Binder<RenewalReportDto>(RenewalReportDto.class);
		
		binder.forField(rangeTypeField)
			.asRequired()
			.bind(RenewalReportDto::getRangeType, RenewalReportDto::setRangeType);
		binder.forField(daysField)
			.withValidator(val -> checkDays(val), "Please select an option.")
			.bind(RenewalReportDto::getDays, RenewalReportDto::setDays);
		binder.forField(fromDateField)
			.withValidator(val -> checkFrom(val), "Please check entered date.")
			.bind(RenewalReportDto::getFromDate, RenewalReportDto::setFromDate);
		binder.forField(toDateField)
			.withValidator(val -> checkTo(val), "Please check entered date.")
			.bind(RenewalReportDto::getToDate, RenewalReportDto::setToDate);
		
		rangeTypeField.addValueChangeListener(e -> {
			String value = e.getValue();
			if (value == null) return;
			applyFieldsLogic(value.equalsIgnoreCase("Days"));
		});
	}
	
	protected void buildForms() {
		form = new FormLayout();
		form.add(rangeTypeField, fromDateField, toDateField, daysField);
		add(form);
		applyFieldsLogic(true);
	}
	
	private void applyFieldsLogic(boolean byDays) {
		daysField.setVisible(byDays);
		fromDateField.setVisible(!byDays);
		toDateField.setVisible(!byDays);
	}
	
	public RenewalReportDto getBean() {
		return binder.getBean();
	}
	
	public void loadData(RenewalReportDto bean) {
		binder.setBean(bean);
	}
	
	public boolean validate() {
		return uiHelper.validateValues(binder);
	}
	
	public boolean checkFrom(LocalDate date) {
		if (rangeTypeField.getValue().equals("Dates") && date == null) {
			return false;
		}
		if (toDateField.getValue() == null) {
			return true;
		}
		return !date.isAfter(toDateField.getValue());
	}
	
	public boolean checkTo(LocalDate date) {
		if (rangeTypeField.getValue().equals("Dates") && date == null) {
			return false;
		}
		if (fromDateField.getValue() == null) {
			return true;
		}
		return !date.isBefore(fromDateField.getValue());
	}
	
	public boolean checkDays(int days) {
		if (rangeTypeField.getValue().equals("Days") && days == 0) {
			return false;
		}
		return true;
	}
	
}
