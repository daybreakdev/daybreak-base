package com.echelon.ui.reporting.dto;

import java.time.LocalDate;

public class RenewalReportDto {
	private String rangeType;
	private LocalDate fromDate;
	private LocalDate toDate;
	private int days;
	
	public RenewalReportDto() {
		this.rangeType = "Days";
	}
	
	public LocalDate getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}
	
	public LocalDate getToDate() {
		return toDate;
	}
	
	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}
	
	public int getDays() {
		return days;
	}
	
	public void setDays(int days) {
		this.days = days;
	}

	public String getRangeType() {
		return rangeType;
	}

	public void setRangeType(String rangeType) {
		this.rangeType = rangeType;
	}
}