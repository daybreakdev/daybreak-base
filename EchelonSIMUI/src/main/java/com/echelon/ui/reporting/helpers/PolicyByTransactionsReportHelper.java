package com.echelon.ui.reporting.helpers;

import java.util.HashMap;
import java.util.Map;

import com.ds.ins.uicommon.components.DSDialogDataStore;
import com.ds.ins.uicommon.components.DSDownload;
import com.ds.ins.uicommon.components.DSFiltersDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.reports.base.ReportResource;
import com.echelon.reports.base.SimplePDFReport;
import com.echelon.ui.constants.UIConstants;
import com.echelon.ui.reporting.components.PolicyByTransactionsReportForm;
import com.echelon.ui.reporting.dto.TrxReportDto;

public class PolicyByTransactionsReportHelper extends BaseReportHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1309018649191741771L;
	
	@Override
	public void openDialog() {
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyByTrxReport");
		PolicyByTransactionsReportForm form = new PolicyByTransactionsReportForm(UIConstants.UIACTION_View, UICONTAINERTYPES.Dialog);
		DSFiltersDialog dialog = new DSFiltersDialog(reportName, form, this::onActionConfirm);
		TrxReportDto bean = new TrxReportDto();
		form.loadData(bean);
		dialog.setDataStore(new DSDialogDataStore(bean));
		dialog.open();
	}
	
	@Override
	protected void onActionConfirm(DSFiltersDialog.ConfirmEvent event) {
		DSFiltersDialog dialog = (DSFiltersDialog) event.getSource();
		Object bean = dialog.getDataStore().getData1();
		DSDownload anchor = dialog.getAnchor();
		PolicyByTransactionsReportForm form = (PolicyByTransactionsReportForm) dialog.getContentComponent();
		if(!form.validate()) {
			return;
		}
		try {
			ReportResource resource = buildAndPrintReport(bean);
			anchor.processResource(resource);
			anchor.startDownload();
		} catch (Exception e) {
			uiHelper.notificationError("Error while printing report: " + e.getMessage());
		}
	}
	
	@Override
	protected ReportResource buildAndPrintReport(Object obj) throws Exception {
		SimplePDFReport report = new SimplePDFReport(reportService);
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyByTrxReport");
		report.setReportName(reportName);
		report.setTemplateName("PolicyByTrxReport");
		report.setParameters(getParams(obj));
		return report.print();
	}
	
	@Override
	protected Map<String, Object> getParams(Object obj) {
		TrxReportDto bean = (TrxReportDto) obj;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromDate", bean.getFromDate().toString());
		params.put("toDate", bean.getToDate().toString());
		return params;
	}

}
