package com.echelon.ui.reporting;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import com.ds.ins.uicommon.components.DSDownload;
import com.ds.ins.uicommon.components.DSFiltersDialog;
import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.reports.base.ReportResource;
import com.echelon.reports.base.SimplePDFReport;
import com.echelon.ui.constants.UIConstants;

public class PolicyByTransactionsReportHelper extends BaseReportHelper {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1309018649191741771L;

	@Override
	public void print() {
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyByTrxReport");
		PolicyByTransactionsReportForm form = new PolicyByTransactionsReportForm(UIConstants.UIACTION_View, UICONTAINERTYPES.Dialog);
		DSFiltersDialog dialog = new DSFiltersDialog(reportName, form, this::onActionConfirm);
		dialog.open();
	}
	
	@Override
	protected void onActionConfirm(DSFiltersDialog.ConfirmEvent event) {
		DSFiltersDialog dialog = (DSFiltersDialog) event.getSource();
		DSDownload anchor = dialog.getAnchor();
		PolicyByTransactionsReportForm form = (PolicyByTransactionsReportForm) dialog.getContentComponent();
		LocalDate fromDate = form.getFromDateField().getValue();
		LocalDate toDate = form.getToDateField().getValue();
		if(!form.validate()) {
			return;
		}
		try {
			ReportResource resource = buildAndPrintReport(fromDate, toDate);
			anchor.processResource(resource);
			anchor.startDownload();
		} catch (Exception e) {
			uiHelper.notificationError("Error while printing report: " + e.getMessage());
		}
	}
	
	private ReportResource buildAndPrintReport(LocalDate fromDate, LocalDate toDate) throws Exception {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("fromDate", fromDate.toString());
		params.put("toDate", toDate.toString());
		SimplePDFReport report = new SimplePDFReport(reportService);
		String reportName = uiHelper.getSessionLabels().getString("MainViewMenuItemPolicyByTrxReport");
		report.setReportName(reportName);
		report.setTemplateName("PolicyByTrxReport");
		report.setParameters(params);
		return report.print();
	}

}
