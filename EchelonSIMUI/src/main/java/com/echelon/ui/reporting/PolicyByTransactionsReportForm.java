package com.echelon.ui.reporting;

import java.time.LocalDate;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class PolicyByTransactionsReportForm extends BaseLayout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5365755015950923813L;
	private FormLayout 				form;
	private DatePicker 				fromDateField;
	private DatePicker 				toDateField;
	private Binder<TrxReportBean>	binder;
	
	public PolicyByTransactionsReportForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildLayout();
		buildFields();
		buildForms();
	}
	
	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}
	
	protected void buildFields() {
		fromDateField = new DatePicker(labels.getString("PolicyByTrxReportFormFromLabel"));
		toDateField = new DatePicker(labels.getString("PolicyByTrxReportFormToLabel"));
		binder = new Binder<TrxReportBean>(TrxReportBean.class);
		
		binder.forField(fromDateField)
			.asRequired()
			.withValidator(val -> checkFrom(val), "Please check entered date.")
			.bind(TrxReportBean::getFromDate, TrxReportBean::setFromDate);
		binder.forField(toDateField)
			.asRequired()
			.withValidator(val -> checkTo(val), "Please check entered date.")
			.bind(TrxReportBean::getToDate, TrxReportBean::setToDate);
	}
	
	protected void buildForms() {
		form = new FormLayout();
		form.add(fromDateField);
		form.add(toDateField);
		add(form);
	}
	
	public DatePicker getFromDateField() {
		return fromDateField;
	}
	
	public DatePicker getToDateField() {
		return toDateField;
	}
	
	public boolean validate() {
		return uiHelper.validateValues(binder);
	}
	
	public boolean checkFrom(LocalDate date) {
		if (toDateField.getValue() == null) {
			return true;
		}
		return !date.isAfter(toDateField.getValue());
	}
	
	public boolean checkTo(LocalDate date) {
		if (fromDateField.getValue() == null) {
			return true;
		}
		return !date.isBefore(fromDateField.getValue());
	}
	
}

class TrxReportBean {
	private LocalDate fromDate;
	private LocalDate toDate;
	
	public LocalDate getFromDate() {
		return fromDate;
	}
	
	public void setFromDate(LocalDate fromDate) {
		this.fromDate = fromDate;
	}
	
	public LocalDate getToDate() {
		return toDate;
	}
	
	public void setToDate(LocalDate toDate) {
		this.toDate = toDate;
	}
	
}
