package com.echelon.ui.reporting.components;

import java.time.LocalDate;

import com.ds.ins.uicommon.constants.DSUIConstants.UICONTAINERTYPES;
import com.echelon.ui.components.baseclasses.BaseLayout;
import com.echelon.ui.reporting.dto.TrxReportDto;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.data.binder.Binder;

public class PolicyByTransactionsReportForm extends BaseLayout {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5365755015950923813L;
	private FormLayout 				form;
	private DatePicker 				fromDateField;
	private DatePicker 				toDateField;
	private Binder<TrxReportDto>	binder;
	
	public PolicyByTransactionsReportForm(String uiMode, UICONTAINERTYPES uiContainerType) {
		super(uiMode, uiContainerType);
		buildLayout();
		buildFields();
		buildForms();
	}
	
	protected void buildLayout() {
		uiHelper.removeLayoutSpaces(this);
	}
	
	protected void buildFields() {
		fromDateField = new DatePicker(labels.getString("ReportFormFromDateLabel"));
		toDateField = new DatePicker(labels.getString("ReportFormToDateLabel"));
		binder = new Binder<TrxReportDto>(TrxReportDto.class);
		
		binder.forField(fromDateField)
			.asRequired()
			.withValidator(val -> checkFrom(val), "Please check entered date.")
			.bind(TrxReportDto::getFromDate, TrxReportDto::setFromDate);
		binder.forField(toDateField)
			.asRequired()
			.withValidator(val -> checkTo(val), "Please check entered date.")
			.bind(TrxReportDto::getToDate, TrxReportDto::setToDate);
	}
	
	protected void buildForms() {
		form = new FormLayout();
		form.add(fromDateField);
		form.add(toDateField);
		add(form);
	}
	
	public TrxReportDto getBean() {
		return binder.getBean();
	}
	
	public void loadData(TrxReportDto bean) {
		binder.setBean(bean);
	}
	
	public boolean validate() {
		return uiHelper.validateValues(binder);
	}
	
	public boolean checkFrom(LocalDate date) {
		if (toDateField.getValue() == null) {
			return true;
		}
		return !date.isAfter(toDateField.getValue());
	}
	
	public boolean checkTo(LocalDate date) {
		if (fromDateField.getValue() == null) {
			return true;
		}
		return !date.isBefore(fromDateField.getValue());
	}
	
}
