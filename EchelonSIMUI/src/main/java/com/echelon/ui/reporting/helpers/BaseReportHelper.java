package com.echelon.ui.reporting.helpers;

import java.util.Map;

import com.ds.ins.uicommon.components.DSFiltersDialog;
import com.echelon.reports.base.ReportResource;
import com.echelon.reports.impl.ReportServiceImpl;
import com.echelon.ui.components.baseclasses.helpers.ContentUIHelper;
import com.echelon.ui.session.UIFactory;

public abstract class BaseReportHelper extends ContentUIHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3593009435736892704L;
	protected ReportServiceImpl reportService;
	
	public BaseReportHelper() {
		super();
		try {
		 reportService = UIFactory.getInstance().getReportService();
		} catch (Exception e) {
			uiHelper.notificationError("Error while trying to get reporting service: " + e.getMessage());
		}
	}
	
	public abstract void openDialog();
	
	protected abstract void onActionConfirm(DSFiltersDialog.ConfirmEvent event);
	
	protected abstract ReportResource buildAndPrintReport(Object obj) throws Exception;
	
	protected abstract Map<String, Object> getParams(Object obj);

}
