CREATE TYPE [AuditDateTime] FROM DATETIME NULL
go

CREATE TYPE [BusinessId] FROM VARCHAR(80) NULL
go

CREATE TYPE [Description] FROM VARCHAR(80) NULL
go

CREATE TYPE [Logical] FROM SMALLINT NULL
go

CREATE TYPE [PhoneNumber] FROM VARCHAR(40) NULL
go

CREATE TYPE [Premium] FROM FLOAT NULL
go

CREATE TYPE [PrimaryKeys] FROM BIGINT NOT NULL
go

CREATE TYPE [Status] FROM VARCHAR(40) NULL
go

CREATE TYPE [UserId] FROM VARCHAR(80) NULL
go

CREATE TYPE [UserKey] FROM INT NULL
go

CREATE TYPE [Version] FROM INT NULL
go

SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [AdditionalInsured] ( 
   [AdditionalInsuredPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyVersionPK] [PrimaryKeys] NULL,
   [SubPolicyPK] [PrimaryKeys] NULL,
   [AddressPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [AdditionalInsuredType] VARCHAR(40) NULL,
   [AdditionalInsuredName] VARCHAR(255) NULL,
   [Relationship] VARCHAR(40) NULL,
   [SystemStatus] [Status] NULL
)

go

ALTER TABLE  [AdditionalInsured]
ADD CONSTRAINT [PRIMARY39] PRIMARY KEY NONCLUSTERED  ([AdditionalInsuredPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyVersion] ON [AdditionalInsured] ([PolicyVersionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_SubPolicy] ON [AdditionalInsured] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Address] ON [AdditionalInsured] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [AdditionalInsuredVehicles] ( 
   [AdditionalInsuredVehiclesPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [AdditionalInsuredPK] [PrimaryKeys] NULL,
   [SubPolicyPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [Description] VARCHAR(80) NULL,
   [SerialNum] VARCHAR(80) NULL,
   [DisplayString] VARCHAR(255) NULL,
   [OriginatingPK] [PrimaryKeys] NULL
)

go

ALTER TABLE  [AdditionalInsuredVehicles]
ADD CONSTRAINT [PK_AdditionalInsuredVehicles] PRIMARY KEY NONCLUSTERED  ([AdditionalInsuredVehiclesPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [AdditionalInsured] ON [AdditionalInsuredVehicles] ([AdditionalInsuredPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [SpecialityAutoSubPolicy] ON [AdditionalInsuredVehicles] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Address] ( 
   [AddressPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [AddressType] VARCHAR(40) NULL,
   [AddressLine1] VARCHAR(120) NULL,
   [AddressLine2] VARCHAR(120) NULL,
   [AddressLine3] VARCHAR(120) NULL,
   [City] VARCHAR(80) NULL,
   [ProvState] VARCHAR(80) NULL,
   [PostalZip] VARCHAR(40) NULL,
   [Country] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Address]
ADD CONSTRAINT [PRIMARY13] PRIMARY KEY NONCLUSTERED  ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Asset] ( 
   [AssetPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Asset]
ADD CONSTRAINT [PRIMARY4] PRIMARY KEY NONCLUSTERED  ([AssetPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [BaseBusinessEntity] ( 
   [BaseBusinessEntitySK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [BaseBusinessEntity]
ADD CONSTRAINT [PRIMARY12] PRIMARY KEY NONCLUSTERED  ([BaseBusinessEntitySK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [BaseTable] ( 
   [BaseTablePK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedBy] [UserId] NULL,
   [CreatedAudit] [AuditDateTime] NULL,
   [ModifiedBy] [UserId] NULL,
   [ModifedAudit] [AuditDateTime] NULL,
   [EditVersion] [Version] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [BaseTable]
ADD CONSTRAINT [PRIMARY] PRIMARY KEY NONCLUSTERED  ([BaseTablePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CargoDetails] ( 
   [CargoDetailsPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [SubPolicyPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [CargoDescription] VARCHAR(255) NULL,
   [CargoPartialPremiums] [Premium] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [Average] FLOAT NULL,
   [UWManual] FLOAT NULL,
   [CargoValue] FLOAT NULL,
   [CargoExposure] FLOAT NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [CargoDetails]
ADD CONSTRAINT [PRIMARY23] PRIMARY KEY NONCLUSTERED  ([CargoDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CargoSubPolicy] ON [CargoDetails] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CargoSubPolicy] ( 
   [SubPolicyPK] [PrimaryKeys] NOT NULL,
   [CargoDeductible] INT NULL,
   [CargoLimit] INT NULL
)

go

ALTER TABLE  [CargoSubPolicy]
ADD CONSTRAINT [PRIMARY22] PRIMARY KEY NONCLUSTERED  ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CGLLocation] ( 
   [CGLLocationPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [SubPolicyPK] [PrimaryKeys] NULL,
   [PolicyRiskPK] [PrimaryKeys] NULL
)

go

ALTER TABLE  [CGLLocation]
ADD CONSTRAINT [PK_CGLLocation] PRIMARY KEY NONCLUSTERED  ([CGLLocationPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [PolicyLocation] ON [CGLLocation] ([PolicyRiskPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [CGLSubPolicy] ON [CGLLocation] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CGLSubPolicy] ( 
   [SubPolicyPK] [PrimaryKeys] NOT NULL,
   [BusinessDescription] VARCHAR(255) NULL,
   [CGLDeductible] INT NULL,
   [CGLLimit] INT NULL
)

go

ALTER TABLE  [CGLSubPolicy]
ADD CONSTRAINT [PRIMARY21] PRIMARY KEY NONCLUSTERED  ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CommercialCustomer] ( 
   [CompanyPK] [PrimaryKeys] NOT NULL,
   [CustomerPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [BusinessDescription] VARCHAR(120) NULL
)

go

ALTER TABLE  [CommercialCustomer]
ADD CONSTRAINT [PRIMARY28] PRIMARY KEY NONCLUSTERED  ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Customer] ON [CommercialCustomer] ([CustomerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Company] ( 
   [CompanyPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [BusinessType] VARCHAR(80) NULL,
   [Industry] VARCHAR(80) NULL,
   [LegalName] VARCHAR(255) NULL,
   [DBAName] VARCHAR(255) NULL,
   [NAICS] VARCHAR(10) NULL,
   [Website] VARCHAR(255) NULL,
   [MainEmail] VARCHAR(255) NULL,
   [PhoneNumber] [PhoneNumber] NULL,
   [FaxNumber] [PhoneNumber] NULL,
   [TollFreeNumber] [PhoneNumber] NULL,
   [RIN] VARCHAR(80) NULL,
   [PreferredLanguage] VARCHAR(40) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Company]
ADD CONSTRAINT [PRIMARY2] PRIMARY KEY NONCLUSTERED  ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Contact] ( 
   [PersonPK] [PrimaryKeys] NOT NULL,
   [AddressPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [Contact]
ADD CONSTRAINT [PRIMARY30] PRIMARY KEY NONCLUSTERED  ([PersonPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Location] ON [Contact] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Coverage] ( 
   [CoveragePK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CoverageDetailsPK] [PrimaryKeys] NULL,
   [DataExtensionPK] [PrimaryKeys] NULL,
   [CoverageCd] VARCHAR(120) NULL,
   [CoverageOrder] INT NULL,
   [CoverageNumber] INT NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [isPremiumOveride] [Logical] NULL,
   [isFullyEarned] [Logical] NULL,
   [isFlatPremium] [Logical] NULL,
   [CoverageEffective] DATETIME NULL,
   [CoverageExpiry] DATETIME NULL,
   [CoverageType] VARCHAR(80) NULL,
   [CoverageSection] VARCHAR(80) NULL,
   [Limit1Amount] INT NULL,
   [Limit1Description] VARCHAR(120) NULL,
   [Limit1Value] VARCHAR(80) NULL,
   [Limit2Amount] INT NULL,
   [Limit2Description] VARCHAR(120) NULL,
   [Limit2Value] VARCHAR(80) NULL,
   [Limit3Amount] INT NULL,
   [Limit3Description] VARCHAR(120) NULL,
   [Limit3Value] VARCHAR(80) NULL,
   [Deductible1Amount] INT NULL,
   [Deductible1Description] VARCHAR(120) NULL,
   [Deductible1Value] VARCHAR(80) NULL,
   [Deductible2Amount] INT NULL,
   [Deductible2Description] VARCHAR(120) NULL,
   [Deductible2Value] VARCHAR(80) NULL,
   [Deductible3Amount] INT NULL,
   [Deductible3Description] VARCHAR(120) NULL,
   [Deductible3Value] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [UnitPremium] [Premium] NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL,
   [NumOfUnits] INT NULL
)

go

ALTER TABLE  [Coverage]
ADD CONSTRAINT [PRIMARY17] PRIMARY KEY NONCLUSTERED  ([CoveragePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CoverageDetails] ON [Coverage] ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_DataExtension] ON [Coverage] ([DataExtensionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CoverageDetails] ( 
   [CoverageDetailsPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [isFullyEarned] [Logical] NULL,
   [isFlatPremium] [Logical] NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [CoverageDetails]
ADD CONSTRAINT [PRIMARY16] PRIMARY KEY NONCLUSTERED  ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Customer] ( 
   [CustomerPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [InsurerPK] [PrimaryKeys] NULL,
   [ProducerPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [CustomerId] [BusinessId] NULL,
   [CustomerType] VARCHAR(40) NULL,
   [CustomerSince] DATE NULL,
   [PreferredLanguage] VARCHAR(40) NULL,
   [PreferredCommunicationMethod] VARCHAR(40) NULL,
   [ProducerCustomerId] [BusinessId] NULL,
   [PreviousInsuranceCompany] VARCHAR(120) NULL,
   [InsuredSince] DATE NULL,
   [KnownSince] DATE NULL,
   [PreviousExpiry] DATE NULL,
   [GlobalUniqueId] [BusinessId] NULL
)

go

ALTER TABLE  [Customer]
ADD CONSTRAINT [PRIMARY1] PRIMARY KEY NONCLUSTERED  ([CustomerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Insurer] ON [Customer] ([InsurerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [CVRate] ( 
   [CVRatePK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [Description] VARCHAR(255) NULL,
   [MinCV] INT NULL,
   [MaxCV] INT NULL,
   [AvgCV] INT NULL,
   [CVRate] FLOAT NULL,
   [Price] INT NULL
)

go

ALTER TABLE  [CVRate]
ADD CONSTRAINT [PK_CVRate] PRIMARY KEY NONCLUSTERED  ([CVRatePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [DataExtension] ( 
   [DataExtensionPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [DataExtension]
ADD CONSTRAINT [PRIMARY43] PRIMARY KEY NONCLUSTERED  ([DataExtensionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Endorsement] ( 
   [Endorsementsk] INT NOT NULL IDENTITY (1,1),
   [CoverageDetailsPK] [PrimaryKeys] NULL,
   [DataExtensionPK] [PrimaryKeys] NULL,
   [EndorsementCd] VARCHAR(120) NULL,
   [EndorsementOrder] INT NULL,
   [EndorsementNumber] INT NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [isPremiumOveride] [Logical] NULL,
   [isFullyEarned] [Logical] NULL,
   [isFlatPremium] [Logical] NULL,
   [EndorsementEffective] DATETIME NULL,
   [EndorsementExpiry] DATETIME NULL,
   [EndorsementType] VARCHAR(80) NULL,
   [EndorsementSection] VARCHAR(80) NULL,
   [Limit1Amount] INT NULL,
   [Limit1Description] VARCHAR(120) NULL,
   [Limit1Value] VARCHAR(80) NULL,
   [Limit2Amount] INT NULL,
   [Limit2Description] VARCHAR(120) NULL,
   [Limit2Value] VARCHAR(80) NULL,
   [Limit3Amount] INT NULL,
   [Limit3Description] VARCHAR(120) NULL,
   [Deductible1Amount] INT NULL,
   [Deductible1Description] VARCHAR(120) NULL,
   [Deductible1Value] VARCHAR(80) NULL,
   [Deductible2Amount] INT NULL,
   [Deductible2Description] VARCHAR(120) NULL,
   [Deductible2Value] VARCHAR(80) NULL,
   [Deductible3Amount] INT NULL,
   [Deductible3Description] VARCHAR(120) NULL,
   [Deductible3Value] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL,
   [NumOfUnits] INT NULL
)

go

ALTER TABLE  [Endorsement]
ADD CONSTRAINT [PRIMARY20] PRIMARY KEY NONCLUSTERED  ([Endorsementsk] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CoverageDetails] ON [Endorsement] ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_DataExtension] ON [Endorsement] ([DataExtensionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [ExtensionData] ( 
   [ExtensionDataPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [ExtensionTypePK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [RowNum] INT NULL,
   [ColumnId] VARCHAR(120) NULL,
   [ColumnValue] VARCHAR(255) NULL
)

go

ALTER TABLE  [ExtensionData]
ADD CONSTRAINT [PRIMARY45] PRIMARY KEY NONCLUSTERED  ([ExtensionDataPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_ExtensionType] ON [ExtensionData] ([ExtensionTypePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [ExtensionType] ( 
   [ExtensionTypePK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [DataExtensionPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [ExtensionId] VARCHAR(80) NULL
)

go

ALTER TABLE  [ExtensionType]
ADD CONSTRAINT [PRIMARY44] PRIMARY KEY NONCLUSTERED  ([ExtensionTypePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_DataExtension] ON [ExtensionType] ([DataExtensionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [GarageSubPolicy] ( 
   [SubPolicyPK] [PrimaryKeys] NOT NULL,
   [NumFullTimeEmployees] INT NULL,
   [NumPartTimeEmployees] INT NULL,
   [BusinessDescription] VARCHAR(3500) NULL
)

go

ALTER TABLE  [GarageSubPolicy]
ADD CONSTRAINT [PRIMARY24] PRIMARY KEY NONCLUSTERED  ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [HouseHoldMembers] ( 
   [HouseHoldMembersPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PersonPK] [PrimaryKeys] NULL,
   [MemberPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [IsPrimaryContact] [Logical] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [HouseHoldMembers]
ADD CONSTRAINT [PRIMARY3] PRIMARY KEY NONCLUSTERED  ([HouseHoldMembersPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PersonalCustomer] ON [HouseHoldMembers] ([PersonPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Person] ON [HouseHoldMembers] ([MemberPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [InsurableItem] ( 
   [InsurableItemPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [InsurableItem]
ADD CONSTRAINT [PRIMARY6] PRIMARY KEY NONCLUSTERED  ([InsurableItemPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [InsurancePolicy] ( 
   [InsurancePolicyPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CustomerPK] [PrimaryKeys] NULL,
   [AddressPK] [PrimaryKeys] NULL,
   [ProducerPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [NamedInsured] VARCHAR(255) NULL,
   [MarketCd] VARCHAR(40) NULL,
   [ProductCd] VARCHAR(120) NULL,
   [ProgramCd] VARCHAR(120) NULL,
   [ControllingJurisdiction] VARCHAR(40) NULL,
   [BasePolicyNum] VARCHAR(120) NULL,
   [ReplacingPolicyNum] VARCHAR(120) NULL,
   [CurrencyCd] VARCHAR(30) NULL,
   [LanguageCd] VARCHAR(40) NULL,
   [OriginalInceptionDate] DATE NULL,
   [OriginatingSource] VARCHAR(40) NULL,
   [ExternalPolicyNum] VARCHAR(120) NULL,
   [PolicyPrefix] VARCHAR(40) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [QuoteNum] VARCHAR(120) NULL,
   [ProducerContactName] VARCHAR(120) NULL,
   [QuotePreparedDate] DATETIME NULL,
   [QuotePreparedBy] VARCHAR(120) NULL
)

go

ALTER TABLE  [InsurancePolicy]
ADD CONSTRAINT [PRIMARY8] PRIMARY KEY NONCLUSTERED  ([InsurancePolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Customer] ON [InsurancePolicy] ([CustomerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Address] ON [InsurancePolicy] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Producer] ON [InsurancePolicy] ([ProducerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Insurer] ( 
   [CompanyPK] [PrimaryKeys] NOT NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [Insurer]
ADD CONSTRAINT [PRIMARY26] PRIMARY KEY NONCLUSTERED  ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [LienholderLessor] ( 
   [LienholderLessorPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyTransactionPK] [PrimaryKeys] NULL,
   [AddressPK] [PrimaryKeys] NULL,
   [LienLessorType] VARCHAR(40) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [CompanyName] VARCHAR(120) NULL,
   [OriginatingPK] [PrimaryKeys] NULL
)

go

ALTER TABLE  [LienholderLessor]
ADD CONSTRAINT [PRIMARY37] PRIMARY KEY NONCLUSTERED  ([LienholderLessorPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTransaction] ON [LienholderLessor] ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Address] ON [LienholderLessor] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [LienholderLessorSubPolicy] ( 
   [LienholderLessorSubPolicyPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [LienholderLessorPK] [PrimaryKeys] NULL,
   [SubPolicyPK] [PrimaryKeys] NULL,
   [LienLessorType] VARCHAR(40) NULL
)

go

ALTER TABLE  [LienholderLessorSubPolicy]
ADD CONSTRAINT [PK_LienholderLessorSubPolicy] PRIMARY KEY NONCLUSTERED  ([LienholderLessorSubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [LienholderLessor] ON [LienholderLessorSubPolicy] ([LienholderLessorPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [SubPolicy] ON [LienholderLessorSubPolicy] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [LienholderLessorVehicleDetails] ( 
   [LienholderLessorVehicleDetailsPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [SubPolicyPK] [PrimaryKeys] NULL,
   [LienholderLessorPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [VehicleNum] INT NULL,
   [Description] VARCHAR(80) NULL,
   [SerialNum] VARCHAR(80) NULL,
   [DisplayString] VARCHAR(255) NULL,
   [VehicleYear] INT NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [LessorDeductible] FLOAT NULL
)

go

ALTER TABLE  [LienholderLessorVehicleDetails]
ADD CONSTRAINT [PRIMARY40] PRIMARY KEY NONCLUSTERED  ([LienholderLessorVehicleDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_LienholderLessor] ON [LienholderLessorVehicleDetails] ([LienholderLessorPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [SpecialityAutoSubPolicy] ON [LienholderLessorVehicleDetails] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Location] ( 
   [AddressPK] [PrimaryKeys] NOT NULL,
   [CompanyPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [LocationNumber] INT NULL,
   [LocationBusinessId] [BusinessId] NULL,
   [LocationType] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Location]
ADD CONSTRAINT [PRIMARY31] PRIMARY KEY NONCLUSTERED  ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Company] ON [Location] ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [NumberGenerator] ( 
   [NumberGeneratorPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [NumberType] VARCHAR(40) NULL,
   [AvailableNumber] INT NULL
)

go

ALTER TABLE  [NumberGenerator]
ADD CONSTRAINT [PRIMARY41] PRIMARY KEY NONCLUSTERED  ([NumberGeneratorPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Person] ( 
   [PersonPk] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [GivenName] VARCHAR(120) NULL,
   [FamilyName] VARCHAR(120) NULL,
   [MiddleName] VARCHAR(120) NULL,
   [Prefix] VARCHAR(80) NULL,
   [Suffix] VARCHAR(80) NULL,
   [Gender] VARCHAR(40) NULL,
   [BirthDate] DATE NULL,
   [DeathDate] DATE NULL,
   [BirthPlace] VARCHAR(255) NULL,
   [DeathPlace] VARCHAR(255) NULL,
   [Nationality] VARCHAR(120) NULL,
   [MaritalStatus] VARCHAR(40) NULL,
   [HomePhone] [PhoneNumber] NULL,
   [BusinessPhone] [PhoneNumber] NULL,
   [CellPhone] [PhoneNumber] NULL,
   [BusinessExt] INT NULL,
   [MainEmail] VARCHAR(255) NULL,
   [FaxNumber] [PhoneNumber] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Person]
ADD CONSTRAINT [PRIMARY0] PRIMARY KEY NONCLUSTERED  ([PersonPk] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PersonalCustomer] ( 
   [PersonPK] [PrimaryKeys] NOT NULL,
   [CustomerPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [PersonalCustomer]
ADD CONSTRAINT [PRIMARY29] PRIMARY KEY NONCLUSTERED  ([PersonPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Customer] ON [PersonalCustomer] ([CustomerPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PersonalLocation] ( 
   [AddressPK] [PrimaryKeys] NOT NULL,
   [PersonPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [PersonalLocation]
ADD CONSTRAINT [PRIMARY32] PRIMARY KEY NONCLUSTERED  ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PersonalCustomer] ON [PersonalLocation] ([PersonPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyDocuments] ( 
   [PolicyDocumentsPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyTransactionPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [DocumentId] VARCHAR(120) NULL,
   [LinkLevel] VARCHAR(80) NULL,
   [LinkPK] [PrimaryKeys] NULL,
   [DistributionTarget] VARCHAR(80) NULL,
   [StorageType] VARCHAR(80) NULL,
   [StorageKey] VARCHAR(255) NULL
)

go

ALTER TABLE  [PolicyDocuments]
ADD CONSTRAINT [PRIMARY33] PRIMARY KEY NONCLUSTERED  ([PolicyDocumentsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTransaction] ON [PolicyDocuments] ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyLocation] ( 
   [PolicyRiskPK] [PrimaryKeys] NOT NULL,
   [AddressPK] [PrimaryKeys] NULL,
   [CustomerMainAddressPK] [PrimaryKeys] NULL,
   [LocationNum] INT NULL,
   [LocationId] VARCHAR(40) NULL,
   [isVehiclesInBuilding] [Logical] NULL,
   [isVehiclesOnLot] [Logical] NULL,
   [NumVehicles] INT NULL
)

go

ALTER TABLE  [PolicyLocation]
ADD CONSTRAINT [PRIMARY25] PRIMARY KEY NONCLUSTERED  ([PolicyRiskPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_Address] ON [PolicyLocation] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyNotes] ( 
   [PolicyNotesPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyVersionPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [NoteDetails] VARCHAR(1024) NULL,
   [IsInternal] [Logical] NULL,
   [IsCarryOverToVersions] [Logical] NULL
)

go

ALTER TABLE  [PolicyNotes]
ADD CONSTRAINT [PRIMARY36] PRIMARY KEY NONCLUSTERED  ([PolicyNotesPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyVersion] ON [PolicyNotes] ([PolicyVersionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyRisk] ( 
   [PolicyRiskPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyTransactionPK] [PrimaryKeys] NULL,
   [SubPolicyPK] [PrimaryKeys] NULL,
   [CoverageDetailsPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [RiskTypeCd] VARCHAR(120) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [PolicyRisk]
ADD CONSTRAINT [PRIMARY15] PRIMARY KEY NONCLUSTERED  ([PolicyRiskPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTransaction] ON [PolicyRisk] ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_SubPolicy] ON [PolicyRisk] ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CoverageDetails] ON [PolicyRisk] ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyTerm] ( 
   [PolicyTermPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [InsurancePolicyPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [PolicyTerm] INT NULL,
   [TermMonths] INT NULL,
   [TermDays] INT NULL,
   [TermEffectiveDate] DATETIME NULL,
   [TermExpiryDate] DATETIME NULL,
   [ProductConfigurationVersion] INT NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [isNewBusinessTerm] [Logical] NULL,
   [isDoNotRenew] [Logical] NULL,
   [DoNotRenewReason] VARCHAR(255) NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [PolicyTerm]
ADD CONSTRAINT [PRIMARY11] PRIMARY KEY NONCLUSTERED  ([PolicyTermPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_InsurancePolicy] ON [PolicyTerm] ([InsurancePolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyTransaction] ( 
   [PolicyTransactionPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyVersionPK] [PrimaryKeys] NULL,
   [PolicyTermPK] [PrimaryKeys] NULL,
   [CoverageDetailsPK] [PrimaryKeys] NULL,
   [TransactionSeq] INT NULL,
   [TransactionType] VARCHAR(20) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [PolicyTransaction]
ADD CONSTRAINT [PRIMARY10] PRIMARY KEY NONCLUSTERED  ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyVersion] ON [PolicyTransaction] ([PolicyVersionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTerm] ON [PolicyTransaction] ([PolicyTermPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CoverageDetails] ON [PolicyTransaction] ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyVersion] ( 
   [PolicyVersionPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [InsurancePolicyPK] [PrimaryKeys] NULL,
   [PolicyTermPK] [PrimaryKeys] NULL,
   [PolicyVersionNum] INT NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [ProductConfigurationVersion] VARCHAR(20) NULL,
   [ChangeReasonCd] VARCHAR(40) NULL,
   [ChangeReasonDetails] VARCHAR(255) NULL,
   [ReceivedDate] DATETIME NULL,
   [BoundDate] DATETIME NULL,
   [IssuedDate] DATETIME NULL,
   [CancellationMethodCd] VARCHAR(40) NULL,
   [ShortTermPremiumMethodCd] VARCHAR(40) NULL,
   [LossOrDeclineReasonCd] VARCHAR(40) NULL,
   [LossorDeclineReasonDetails] VARCHAR(255) NULL,
   [ProducerCommissionRate] FLOAT NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [PolicyVersion]
ADD CONSTRAINT [PRIMARY9] PRIMARY KEY NONCLUSTERED  ([PolicyVersionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_InsurancePolicy] ON [PolicyVersion] ([InsurancePolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTerm] ON [PolicyVersion] ([PolicyTermPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyVersionAudit] ( 
   [PolicyVersionAuditPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyVersionPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [FromStatus] VARCHAR(80) NULL,
   [ToStatus] VARCHAR(80) NULL,
   [IsError] [Logical] NULL,
   [Details] VARCHAR(1024) NULL
)

go

ALTER TABLE  [PolicyVersionAudit]
ADD CONSTRAINT [PK_PolicyVersionAudit] PRIMARY KEY NONCLUSTERED  ([PolicyVersionAuditPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [PolicyVersion] ON [PolicyVersionAudit] ([PolicyVersionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Producer] ( 
   [CompanyPK] [PrimaryKeys] NOT NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [ProducerId] VARCHAR(80) NULL
)

go

ALTER TABLE  [Producer]
ADD CONSTRAINT [PRIMARY27] PRIMARY KEY NONCLUSTERED  ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Property] ( 
   [PropertyPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Property]
ADD CONSTRAINT [PRIMARY7] PRIMARY KEY NONCLUSTERED  ([PropertyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SpecialityAutoSubPolicy] ( 
   [SubPolicyPK] [PrimaryKeys] NOT NULL,
   [isFleet] [Logical] NULL,
   [FleetBasis] VARCHAR(120) NULL,
   [isRiskSharingPool] [Logical] NULL
)

go

ALTER TABLE  [SpecialityAutoSubPolicy]
ADD CONSTRAINT [PRIMARY18] PRIMARY KEY NONCLUSTERED  ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SpecialtyAutoPackage] ( 
   [InsurancePolicyPK] [PrimaryKeys] NOT NULL,
   [LiabilityLimit] INT NULL,
   [VehicleListReceivedDate] DATE NULL,
   [IsProvideVehicleRegistration] [Logical] NULL,
   [VehicleRegistrationList] VARCHAR(255) NULL,
   [IsProvideDriverMVR] [Logical] NULL,
   [ProvideDriverMVRList] VARCHAR(255) NULL,
   [IsProvideFuelTaxReport] [Logical] NULL,
   [OtherRequirements] VARCHAR(1024) NULL,
   [TerritoryCode] VARCHAR(10) NULL,
   [StatCode] VARCHAR(10) NULL,
   [RuralityInd] VARCHAR(10) NULL,
   [AutoAssocMemberNum] VARCHAR(80) NULL,
   [AutoAssocClub] VARCHAR(80) NULL
)

go

ALTER TABLE  [SpecialtyAutoPackage]
ADD CONSTRAINT [PRIMARY34] PRIMARY KEY NONCLUSTERED  ([InsurancePolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SpecialtyVehicleRisk] ( 
   [PolicyRiskPK] [PrimaryKeys] NOT NULL,
   [isIndividual] [Logical] NULL,
   [NumberOfUnits] INT NULL,
   [VehicleType] VARCHAR(80) NULL,
   [TrailerType] VARCHAR(80) NULL,
   [VehicleDescription] VARCHAR(120) NULL,
   [VehicleUse] VARCHAR(80) NULL,
   [VehicleClass] VARCHAR(40) NULL,
   [DrivingRecord] VARCHAR(40) NULL,
   [UnitLPN] FLOAT NULL,
   [UnitExposure] VARCHAR(80) NULL,
   [UnitSerialNumberorVin] VARCHAR(80) NULL,
   [UnitMake] VARCHAR(80) NULL,
   [UnitYear] INT NULL,
   [UnitModel] VARCHAR(80) NULL,
   [ImpliedRateGroup] VARCHAR(40) NULL,
   [SelectedRateGroup] VARCHAR(40) NULL,
   [UnitRate] [Premium] NULL
)

go

ALTER TABLE  [SpecialtyVehicleRisk]
ADD CONSTRAINT [PRIMARY19] PRIMARY KEY NONCLUSTERED  ([PolicyRiskPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [StatCodes] ( 
   [StatCodesPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PostalCode] VARCHAR(10) NOT NULL,
   [TerritoryCode] VARCHAR(10) NOT NULL,
   [StatCode] VARCHAR(10) NOT NULL,
   [RuralityInd] VARCHAR(10) NULL
)

go

ALTER TABLE  [StatCodes]
ADD CONSTRAINT [PRIMARY35] PRIMARY KEY NONCLUSTERED  ([StatCodesPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SubPolicy] ( 
   [SubPolicyPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyTransactionPK] [PrimaryKeys] NULL,
   [CoverageDetailsPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [WrittenPremium] [Premium] NULL,
   [TransPremium] [Premium] NULL,
   [TermPremium] [Premium] NULL,
   [AnnualPremium] [Premium] NULL,
   [OriginalPremium] [Premium] NULL,
   [isPremiumOveride] [Logical] NULL,
   [PriorWrittenPremium] [Premium] NULL,
   [PriorNetPremiumChange] [Premium] NULL,
   [NetPremiumChange] [Premium] NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [SubPolicyTypeCd] VARCHAR(120) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [PremiumModifier1] FLOAT NULL,
   [PremiumModifier2] FLOAT NULL,
   [TargetPremium] FLOAT NULL
)

go

ALTER TABLE  [SubPolicy]
ADD CONSTRAINT [PRIMARY14] PRIMARY KEY NONCLUSTERED  ([SubPolicyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_PolicyTransaction] ON [SubPolicy] ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [FK_CoverageDetails] ON [SubPolicy] ([CoverageDetailsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [Vehicle] ( 
   [VehiclePK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [Vehicle]
ADD CONSTRAINT [PRIMARY5] PRIMARY KEY NONCLUSTERED  ([VehiclePK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [VehicleRateGroup] ( 
   [VehicleRateGroupPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [VehicleAge] INT NULL,
   [LowLimit] FLOAT NULL,
   [UppLimit] FLOAT NULL,
   [ImpliedRateGroup] INT NULL
)

go

ALTER TABLE  [VehicleRateGroup]
ADD CONSTRAINT [PRIMARY42] PRIMARY KEY NONCLUSTERED  ([VehicleRateGroupPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_290' and type='F')
ALTER TABLE [AdditionalInsured] WITH NOCHECK
      ADD   CONSTRAINT [Relation_290] FOREIGN KEY
          ( [PolicyVersionPK] )
          REFERENCES [PolicyVersion]
          ( [PolicyVersionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_291' and type='F')
ALTER TABLE [AdditionalInsured] WITH NOCHECK
      ADD   CONSTRAINT [Relation_291] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_292' and type='F')
ALTER TABLE [AdditionalInsured] WITH NOCHECK
      ADD   CONSTRAINT [Relation_292] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_307' and type='F')
ALTER TABLE [AdditionalInsuredVehicles] WITH NOCHECK
      ADD   CONSTRAINT [Relation_307] FOREIGN KEY
          ( [AdditionalInsuredPK] )
          REFERENCES [AdditionalInsured]
          ( [AdditionalInsuredPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_308' and type='F')
ALTER TABLE [AdditionalInsuredVehicles] WITH NOCHECK
      ADD   CONSTRAINT [Relation_308] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SpecialityAutoSubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_244' and type='F')
ALTER TABLE [CargoDetails] WITH NOCHECK
      ADD   CONSTRAINT [Relation_244] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [CargoSubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_243' and type='F')
ALTER TABLE [CargoSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_243] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_305' and type='F')
ALTER TABLE [CGLLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_305] FOREIGN KEY
          ( [PolicyRiskPK] )
          REFERENCES [PolicyLocation]
          ( [PolicyRiskPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_306' and type='F')
ALTER TABLE [CGLLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_306] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [CGLSubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_242' and type='F')
ALTER TABLE [CGLSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_242] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_264' and type='F')
ALTER TABLE [CommercialCustomer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_264] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Company]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_265' and type='F')
ALTER TABLE [CommercialCustomer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_265] FOREIGN KEY
          ( [CustomerPK] )
          REFERENCES [Customer]
          ( [CustomerPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_269' and type='F')
ALTER TABLE [Contact] WITH NOCHECK
      ADD   CONSTRAINT [Relation_269] FOREIGN KEY
          ( [PersonPK] )
          REFERENCES [Person]
          ( [PersonPk] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_275' and type='F')
ALTER TABLE [Contact] WITH NOCHECK
      ADD   CONSTRAINT [Relation_275] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Location]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_226' and type='F')
ALTER TABLE [Coverage] WITH NOCHECK
      ADD   CONSTRAINT [Relation_226] FOREIGN KEY
          ( [CoverageDetailsPK] )
          REFERENCES [CoverageDetails]
          ( [CoverageDetailsPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_297' and type='F')
ALTER TABLE [Coverage] WITH NOCHECK
      ADD   CONSTRAINT [Relation_297] FOREIGN KEY
          ( [DataExtensionPK] )
          REFERENCES [DataExtension]
          ( [DataExtensionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_260' and type='F')
ALTER TABLE [Customer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_260] FOREIGN KEY
          ( [InsurerPK] )
          REFERENCES [Insurer]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_240' and type='F')
ALTER TABLE [Endorsement] WITH NOCHECK
      ADD   CONSTRAINT [Relation_240] FOREIGN KEY
          ( [CoverageDetailsPK] )
          REFERENCES [CoverageDetails]
          ( [CoverageDetailsPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_298' and type='F')
ALTER TABLE [Endorsement] WITH NOCHECK
      ADD   CONSTRAINT [Relation_298] FOREIGN KEY
          ( [DataExtensionPK] )
          REFERENCES [DataExtension]
          ( [DataExtensionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_296' and type='F')
ALTER TABLE [ExtensionData] WITH NOCHECK
      ADD   CONSTRAINT [Relation_296] FOREIGN KEY
          ( [ExtensionTypePK] )
          REFERENCES [ExtensionType]
          ( [ExtensionTypePK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_295' and type='F')
ALTER TABLE [ExtensionType] WITH NOCHECK
      ADD   CONSTRAINT [Relation_295] FOREIGN KEY
          ( [DataExtensionPK] )
          REFERENCES [DataExtension]
          ( [DataExtensionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_245' and type='F')
ALTER TABLE [GarageSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_245] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_272' and type='F')
ALTER TABLE [HouseHoldMembers] WITH NOCHECK
      ADD   CONSTRAINT [Relation_272] FOREIGN KEY
          ( [PersonPK] )
          REFERENCES [PersonalCustomer]
          ( [PersonPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_273' and type='F')
ALTER TABLE [HouseHoldMembers] WITH NOCHECK
      ADD   CONSTRAINT [Relation_273] FOREIGN KEY
          ( [MemberPK] )
          REFERENCES [Person]
          ( [PersonPk] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_222' and type='F')
ALTER TABLE [InsurancePolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_222] FOREIGN KEY
          ( [CustomerPK] )
          REFERENCES [Customer]
          ( [CustomerPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_279' and type='F')
ALTER TABLE [InsurancePolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_279] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_282' and type='F')
ALTER TABLE [InsurancePolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_282] FOREIGN KEY
          ( [ProducerPK] )
          REFERENCES [Producer]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_258' and type='F')
ALTER TABLE [Insurer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_258] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Company]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_286' and type='F')
ALTER TABLE [LienholderLessor] WITH NOCHECK
      ADD   CONSTRAINT [Relation_286] FOREIGN KEY
          ( [PolicyTransactionPK] )
          REFERENCES [PolicyTransaction]
          ( [PolicyTransactionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_287' and type='F')
ALTER TABLE [LienholderLessor] WITH NOCHECK
      ADD   CONSTRAINT [Relation_287] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_303' and type='F')
ALTER TABLE [LienholderLessorSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_303] FOREIGN KEY
          ( [LienholderLessorPK] )
          REFERENCES [LienholderLessor]
          ( [LienholderLessorPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_304' and type='F')
ALTER TABLE [LienholderLessorSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_304] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_300' and type='F')
ALTER TABLE [LienholderLessorVehicleDetails] WITH NOCHECK
      ADD   CONSTRAINT [Relation_300] FOREIGN KEY
          ( [LienholderLessorPK] )
          REFERENCES [LienholderLessor]
          ( [LienholderLessorPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_302' and type='F')
ALTER TABLE [LienholderLessorVehicleDetails] WITH NOCHECK
      ADD   CONSTRAINT [Relation_302] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SpecialityAutoSubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_274' and type='F')
ALTER TABLE [Location] WITH NOCHECK
      ADD   CONSTRAINT [Relation_274] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_276' and type='F')
ALTER TABLE [Location] WITH NOCHECK
      ADD   CONSTRAINT [Relation_276] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Company]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_266' and type='F')
ALTER TABLE [PersonalCustomer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_266] FOREIGN KEY
          ( [PersonPK] )
          REFERENCES [Person]
          ( [PersonPk] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_267' and type='F')
ALTER TABLE [PersonalCustomer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_267] FOREIGN KEY
          ( [CustomerPK] )
          REFERENCES [Customer]
          ( [CustomerPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_277' and type='F')
ALTER TABLE [PersonalLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_277] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_278' and type='F')
ALTER TABLE [PersonalLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_278] FOREIGN KEY
          ( [PersonPK] )
          REFERENCES [PersonalCustomer]
          ( [PersonPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_280' and type='F')
ALTER TABLE [PolicyDocuments] WITH NOCHECK
      ADD   CONSTRAINT [Relation_280] FOREIGN KEY
          ( [PolicyTransactionPK] )
          REFERENCES [PolicyTransaction]
          ( [PolicyTransactionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_247' and type='F')
ALTER TABLE [PolicyLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_247] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Address]
          ( [AddressPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_248' and type='F')
ALTER TABLE [PolicyLocation] WITH NOCHECK
      ADD   CONSTRAINT [Relation_248] FOREIGN KEY
          ( [PolicyRiskPK] )
          REFERENCES [PolicyRisk]
          ( [PolicyRiskPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_284' and type='F')
ALTER TABLE [PolicyNotes] WITH NOCHECK
      ADD   CONSTRAINT [Relation_284] FOREIGN KEY
          ( [PolicyVersionPK] )
          REFERENCES [PolicyVersion]
          ( [PolicyVersionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_224' and type='F')
ALTER TABLE [PolicyRisk] WITH NOCHECK
      ADD   CONSTRAINT [Relation_224] FOREIGN KEY
          ( [PolicyTransactionPK] )
          REFERENCES [PolicyTransaction]
          ( [PolicyTransactionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_225' and type='F')
ALTER TABLE [PolicyRisk] WITH NOCHECK
      ADD   CONSTRAINT [Relation_225] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_228' and type='F')
ALTER TABLE [PolicyRisk] WITH NOCHECK
      ADD   CONSTRAINT [Relation_228] FOREIGN KEY
          ( [CoverageDetailsPK] )
          REFERENCES [CoverageDetails]
          ( [CoverageDetailsPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_216' and type='F')
ALTER TABLE [PolicyTerm] WITH NOCHECK
      ADD   CONSTRAINT [Relation_216] FOREIGN KEY
          ( [InsurancePolicyPK] )
          REFERENCES [InsurancePolicy]
          ( [InsurancePolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_213' and type='F')
ALTER TABLE [PolicyTransaction] WITH NOCHECK
      ADD   CONSTRAINT [Relation_213] FOREIGN KEY
          ( [PolicyVersionPK] )
          REFERENCES [PolicyVersion]
          ( [PolicyVersionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_214' and type='F')
ALTER TABLE [PolicyTransaction] WITH NOCHECK
      ADD   CONSTRAINT [Relation_214] FOREIGN KEY
          ( [PolicyTermPK] )
          REFERENCES [PolicyTerm]
          ( [PolicyTermPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_227' and type='F')
ALTER TABLE [PolicyTransaction] WITH NOCHECK
      ADD   CONSTRAINT [Relation_227] FOREIGN KEY
          ( [CoverageDetailsPK] )
          REFERENCES [CoverageDetails]
          ( [CoverageDetailsPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_212' and type='F')
ALTER TABLE [PolicyVersion] WITH NOCHECK
      ADD   CONSTRAINT [Relation_212] FOREIGN KEY
          ( [InsurancePolicyPK] )
          REFERENCES [InsurancePolicy]
          ( [InsurancePolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_215' and type='F')
ALTER TABLE [PolicyVersion] WITH NOCHECK
      ADD   CONSTRAINT [Relation_215] FOREIGN KEY
          ( [PolicyTermPK] )
          REFERENCES [PolicyTerm]
          ( [PolicyTermPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_301' and type='F')
ALTER TABLE [PolicyVersionAudit] WITH NOCHECK
      ADD   CONSTRAINT [Relation_301] FOREIGN KEY
          ( [PolicyVersionPK] )
          REFERENCES [PolicyVersion]
          ( [PolicyVersionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_262' and type='F')
ALTER TABLE [Producer] WITH NOCHECK
      ADD   CONSTRAINT [Relation_262] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Company]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_241' and type='F')
ALTER TABLE [SpecialityAutoSubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_241] FOREIGN KEY
          ( [SubPolicyPK] )
          REFERENCES [SubPolicy]
          ( [SubPolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_283' and type='F')
ALTER TABLE [SpecialtyAutoPackage] WITH NOCHECK
      ADD   CONSTRAINT [Relation_283] FOREIGN KEY
          ( [InsurancePolicyPK] )
          REFERENCES [InsurancePolicy]
          ( [InsurancePolicyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_239' and type='F')
ALTER TABLE [SpecialtyVehicleRisk] WITH NOCHECK
      ADD   CONSTRAINT [Relation_239] FOREIGN KEY
          ( [PolicyRiskPK] )
          REFERENCES [PolicyRisk]
          ( [PolicyRiskPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_223' and type='F')
ALTER TABLE [SubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_223] FOREIGN KEY
          ( [PolicyTransactionPK] )
          REFERENCES [PolicyTransaction]
          ( [PolicyTransactionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_229' and type='F')
ALTER TABLE [SubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_229] FOREIGN KEY
          ( [CoverageDetailsPK] )
          REFERENCES [CoverageDetails]
          ( [CoverageDetailsPK] )
go

