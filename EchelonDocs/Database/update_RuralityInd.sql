create temporary table tempstatcode 
select distinct TerritoryCode, StatCode, RuralityInd from statcodes;

UPDATE
    specialtyautopackage sa,
    tempstatcode sc
SET
    sa.RuralityInd = sc.RuralityInd 
where
	sa.TerritoryCode = sc.TerritoryCode and
	sa.StatCode = sc.StatCode;

DROP TEMPORARY TABLE tempstatcode;