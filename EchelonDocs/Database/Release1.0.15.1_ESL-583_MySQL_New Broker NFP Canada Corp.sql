INSERT INTO company (LegalName, DisplayString, Version) 
VALUES
('NFP Canada Corp.', 'BrokerCompany5', 0)
;

INSERT INTO address (AddressLine1, City, ProvState, PostalZip, DisplayString, Version)
VALUES
('1500 Don Mills Rd., Suite 501', 'Toronto', 'ON', 'M3B 3K4', 'BrokerAddress5', 0)
;

INSERT INTO location (AddressPK, CompanyPK, LocationNumber, Version)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress5'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany5'),
 0, 0)
;

INSERT INTO producer (ProducerId, CompanyPK, Version) 
VALUES
('NFP001', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany5'), 0)
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'BrokerCompany%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'BrokerAddress%';
