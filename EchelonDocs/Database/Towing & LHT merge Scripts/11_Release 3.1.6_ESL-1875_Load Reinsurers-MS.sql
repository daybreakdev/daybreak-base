-- This script will do the following:
-- 1) Insert LHT Reinsurer companies - load company, address tables
-- 2) Insert to Reinsurer table - link with company & address

INSERT INTO company (LegalName, DBAName, PhoneNumber, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, DisplayString, BusinessType, PreferredLanguage, Version, VersionDate) 
VALUES
('General Reinsurance Corporation', 'General Reinsurance Corporation', '(416) 360-2006', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp1', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP),
('Munich Reinsurance Company of Canada', 'Munich Reinsurance Company of Canada', '(416) 359-2341', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp2', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP),
('Echelon Insurance', 'Echelon Insurance', '(416) 360-2006', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp3', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP),
('Everest Reinsurance Canada', 'Everest Reinsurance', '(416) 862-5899', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp4', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP)
;

INSERT INTO address (AddressLine1, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, City, ProvState, PostalZip, DisplayString, Version, VersionDate)
VALUES
('1 First Canadian place Suite 5705 PO Box 471', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Toronto', 'ON', 'M5X 1E4', 'LHTReinsAddr1', 0, CURRENT_TIMESTAMP),
('390 Bay Street, 22nd floor', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Toronto', 'ON', 'M5H 2Y2', 'LHTReinsAddr2', 0, CURRENT_TIMESTAMP),
('2680 Matheson Blvd E', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Mississauga', 'ON', 'L4W 0A5', 'LHTReinsAddr3', 0, CURRENT_TIMESTAMP),
('130 King Street West, Suite 2520', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Toronto', 'ON', 'M5X 1E3', 'LHTReinsAddr4', 0, CURRENT_TIMESTAMP)
;

INSERT INTO location (AddressPK, CreatedDate, CreatedBy, CompanyPK, SystemStatus, BusinessStatus, Description, LocationNumber, Version, VersionDate)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr1'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp1'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr2'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp2'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP),
 ((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr3'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp3'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr4'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp4'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP) 
;

INSERT INTO reinsurer (CompanyPK, Version, VersionDate, CreatedDate, CreatedBy, SystemStatus, BusinessStatus,  Description)
VALUES
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp1'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp1')),
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp2'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp2')),
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp3'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp3')),
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp4'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp4'))  
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'LHTReinsComp%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'LHTReinsAddr%';
