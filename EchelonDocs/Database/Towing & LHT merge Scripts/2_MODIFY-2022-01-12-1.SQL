SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [PolicyTransactionData] ( 
   [PolicyTransactionDataPK] BIGINT NOT NULL IDENTITY (1,1),
   [PolicyTransactionPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL
)

go

ALTER TABLE  [PolicyTransactionData]
ADD CONSTRAINT [PK_PolicyTransactionData] PRIMARY KEY NONCLUSTERED  ([PolicyTransactionDataPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [PolicyTransaction] ON [PolicyTransactionData] ([PolicyTransactionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SpecialityAutoExpRating] ( 
   [PolicyTransactionDataPK] BIGINT NOT NULL,
   [ExpectedMileage] FLOAT NULL,
   [NumberOfPowerUnits] INT NULL,
   [AverageKMS] FLOAT NULL,
   [ClaimsFrequency] [Premium] NULL,
   [YearsInBusiness] INT NULL,
   [DangerousMaterials] [Premium] NULL,
   [PreventionRating] [Premium] NULL,
   [CommercialCreditScore] INT NULL,
   [Zone1Exp] [Premium] NULL,
   [Zone2Exp] [Premium] NULL,
   [Zone3Exp] [Premium] NULL,
   [Zone4Exp] [Premium] NULL,
   [Zone5Exp] [Premium] NULL,
   [Zone6Exp] [Premium] NULL,
   [Zone7Exp] [Premium] NULL,
   [Zone8Exp] [Premium] NULL,
   [Zone9Exp] [Premium] NULL,
   [ModificationFactor] [Premium] NULL,
   [OffBalanceFactor] [Premium] NULL
)

go

ALTER TABLE  [SpecialityAutoExpRating]
ADD CONSTRAINT [PK_SpecialityAutoExpRating] PRIMARY KEY NONCLUSTERED  ([PolicyTransactionDataPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

IF EXISTS ( SELECT 1 from sys.foreign_keys where schema_name(schema_id)= 'dbo' and object_name(parent_object_id ) = 'SpecialtyAutoExposureRating' and name = 'Relation_324')
ALTER TABLE [dbo].[SpecialtyAutoExposureRating]
DROP CONSTRAINT [Relation_324]
go

DROP TABLE [dbo].[SpecialtyAutoExposureRating]
go
DROP TABLE [dbo].[SpecialtyAutoPolicyTransaction]
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_325' and type='F')
ALTER TABLE [PolicyTransactionData] WITH NOCHECK
      ADD   CONSTRAINT [Relation_325] FOREIGN KEY
          ( [PolicyTransactionPK] )
          REFERENCES [PolicyTransaction]
          ( [PolicyTransactionPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_326' and type='F')
ALTER TABLE [SpecialityAutoExpRating] WITH NOCHECK
      ADD   CONSTRAINT [Relation_326] FOREIGN KEY
          ( [PolicyTransactionDataPK] )
          REFERENCES [PolicyTransactionData]
          ( [PolicyTransactionDataPK] )
go

