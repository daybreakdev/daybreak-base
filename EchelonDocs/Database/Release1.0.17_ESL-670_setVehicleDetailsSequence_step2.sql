DROP TEMPORARY TABLE IF EXISTS VehicleDetailsPK;

CREATE TEMPORARY TABLE VehicleDetailsPK

select veh.LienholderLessorVehicleDetailsPK	,
										(
											select veh2.VehicleDetailsSequence
											from SubPolicy, PolicyTransaction, PolicyVersion,
												 PolicyVersion pv2, PolicyTransaction pt2, SubPolicy subpol2, LienholderLessorVehicleDetails veh2
											where SubPolicy.SubPolicyPK				  = veh.SubPolicyPK
											and PolicyTransaction.PolicyTransactionPK = SubPolicy.PolicyTransactionPK
											and PolicyVersion.PolicyVersionPK         = PolicyTransaction.PolicyVersionPK
											and pv2.InsurancePolicyPK       = PolicyVersion.InsurancePolicyPK
											and pt2.PolicyVersionPK		    = pv2.PolicyVersionPK
											and subpol2.PolicyTransactionPK = pt2.PolicyTransactionPK
											and veh2.SubPolicyPK            = subpol2.SubPolicyPK
											and veh2.LienholderLessorPK     = veh.LienholderLessorPK
											and veh2.VehicleDetailsSequence is not null
											and veh2.VehicleNum             = veh.VehicleNum
											and veh2.VehicleYear            = veh.VehicleYear
											and veh2.SerialNum              = veh.SerialNum
											) as newVehicleDetailsSequence										
	from LienholderLessorVehicleDetails veh, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleDetailsSequence  is null
	and subpol.SubPolicyPK     = veh.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK
;

update LienholderLessorVehicleDetails, VehicleDetailsPK
set LienholderLessorVehicleDetails.VehicleDetailsSequence = VehicleDetailsPK.newVehicleDetailsSequence
where LienholderLessorVehicleDetails.LienholderLessorVehicleDetailsPK = VehicleDetailsPK.LienholderLessorVehicleDetailsPK
;

DROP TEMPORARY TABLE VehicleDetailsPK;
