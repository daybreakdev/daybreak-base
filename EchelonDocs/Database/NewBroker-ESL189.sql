USE daystar;

INSERT INTO company (LegalName, DisplayString, Version) 
VALUES
('Hargraft Schofield LP', 'BrokerCompany4', 0)
;

INSERT INTO address (AddressLine1, City, ProvState, PostalZip, DisplayString, Version)
VALUES
('120 Bremner Blvd., Suite 800', 'Toronto', 'ON', 'M5J 0A8', 'BrokerAddress4', 0)
;

INSERT INTO location (AddressPK, CompanyPK, LocationNumber, Version)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress4'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany4'),
 0, 0)
;

INSERT INTO producer (ProducerId, CompanyPK, Version) 
VALUES
('HAR002', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany4'), 0)
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'BrokerCompany%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'BrokerAddress%';
