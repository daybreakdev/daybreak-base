DROP TEMPORARY TABLE IF EXISTS VehicleDetailsPK;

CREATE TEMPORARY TABLE VehicleDetailsPK

SELECT veh.LienholderLessorVehicleDetailsPK
, (select count(veh1.LienholderLessorVehicleDetailsPK)+1
											from LienholderLessorVehicleDetails veh1
											where veh1.LienholderLessorPK = veh.LienholderLessorPK
											  and veh1.SubPolicyPK  = veh.SubPolicyPK
											  and (veh1.CreatedDate < veh.CreatedDate
											       or 
												   (veh1.CreatedDate = veh.CreatedDate 
												    and veh1.LienholderLessorVehicleDetailsPK < veh.LienholderLessorVehicleDetailsPK)
												  )) as newVehicleDetailsSequence
	from LienholderLessorVehicleDetails veh, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleDetailsSequence is null
	and subpol.SubPolicyPK     = veh.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK

	and not exists (
		select veh0.LienholderLessorVehicleDetailsPK
		from PolicyVersion pv0, PolicyTransaction pt0, SubPolicy subpol0, LienholderLessorVehicleDetails veh0
		where pv0.InsurancePolicyPK     = ip.InsurancePolicyPK
		and pt0.PolicyVersionPK         = pv0.PolicyVersionPK
		and subpol0.PolicyTransactionPK = pt0.PolicyTransactionPK
		and veh0.SubPolicyPK            = subpol0.SubPolicyPK 
		and veh0.LienholderLessorPK     = veh.LienholderLessorPK
		and veh0.VehicleNum             = veh.VehicleNum
		and veh0.VehicleYear            = veh.VehicleYear
		and veh0.SerialNum              = veh.SerialNum
		and veh0.CreatedDate            < veh.CreatedDate
	)
;

update LienholderLessorVehicleDetails, VehicleDetailsPK
set LienholderLessorVehicleDetails.VehicleDetailsSequence = VehicleDetailsPK.newVehicleDetailsSequence
where LienholderLessorVehicleDetails.LienholderLessorVehicleDetailsPK = VehicleDetailsPK.LienholderLessorVehicleDetailsPK
;

DROP TEMPORARY TABLE VehicleDetailsPK;
