ALTER TABLE SpecialtyAutoPackage
      ADD AutoAssocClub VARCHAR(80) NULL
;

ALTER TABLE LienholderLessorRisk
      ADD LienLessorType VARCHAR(40) NULL
;

ALTER TABLE LienholderLessorVehicleDetails
      ADD LienLessorType VARCHAR(40) NULL
;

