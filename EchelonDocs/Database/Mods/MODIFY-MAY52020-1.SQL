ALTER TABLE PolicyTerm
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

ALTER TABLE PolicyTransaction
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

ALTER TABLE PolicyVersion
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

ALTER TABLE Coverage
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL, 
      ADD NumOfUnits INT(10) NULL
;

ALTER TABLE CoverageDetails
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

ALTER TABLE Endorsement
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL, 
      ADD NumOfUnits INT(10) NULL
;

ALTER TABLE GarageSubPolicy
      ADD NumFullTimeEmployees INT(10) NULL, 
      ADD NumPartTimeEmployees INT(10) NULL, 
      ADD BusinessDescription VARCHAR(255) NULL
;

ALTER TABLE PolicyLocation
      ADD CustomerMainAddressPK BIGINT(10) NULL
;

ALTER TABLE PolicyRisk
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

ALTER TABLE PolicyDocuments
      MODIFY LinkPK BIGINT(10) NULL
;

ALTER TABLE LienholderLessor
      MODIFY PolicyTransactionPK BIGINT(10) NULL, 
      MODIFY LienLessorType VARCHAR(40) NULL
;

ALTER TABLE LienholderLessorRisk
      MODIFY LienholderLessorPK BIGINT(10) NULL, 
      MODIFY PolicyRiskPK BIGINT(10) NULL
;

ALTER TABLE AdditionalInsured
      ADD SystemStatus VARCHAR(40) NULL
;

ALTER TABLE LienholderLessorVehicleDetails
      ADD DisplayString VARCHAR(255) NULL
;

