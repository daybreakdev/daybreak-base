ALTER TABLE Coverage
      ADD DataExtensionPK BIGINT(10) NULL, 
      ADD INDEX FK_DataExtension (DataExtensionPK)
;

ALTER TABLE Endorsement
      ADD DataExtensionPK BIGINT(10) NULL, 
      ADD INDEX FK_DataExtension (DataExtensionPK)
;

ALTER TABLE Coverage
      ADD CONSTRAINT Relation_297 FOREIGN KEY
          ( DataExtensionPK )
          REFERENCES DataExtension
          ( DataExtensionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Endorsement
      ADD CONSTRAINT Relation_298 FOREIGN KEY
          ( DataExtensionPK )
          REFERENCES DataExtension
          ( DataExtensionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

