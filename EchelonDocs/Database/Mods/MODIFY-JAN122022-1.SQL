CREATE TABLE  PolicyTransactionData
(
  PolicyTransactionDataPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  PolicyTransactionPK BIGINT(10) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  PRIMARY KEY (PolicyTransactionDataPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_PolicyTransaction ON PolicyTransactionData (PolicyTransactionPK)
;

CREATE TABLE  SpecialityAutoExpRating
(
  PolicyTransactionDataPK BIGINT(10) NOT NULL,
  ExpectedMileage FLOAT(10,0) NULL,
  NumberOfPowerUnits INT(10) NULL,
  AverageKMS FLOAT(10,0) NULL,
  ClaimsFrequency FLOAT(10,2) NULL,
  YearsInBusiness INT(10) NULL,
  DangerousMaterials FLOAT(10,2) NULL,
  PreventionRating FLOAT(10,2) NULL,
  CommercialCreditScore INT(10) NULL,
  Zone1Exp FLOAT(10,2) NULL,
  Zone2Exp FLOAT(10,2) NULL,
  Zone3Exp FLOAT(10,2) NULL,
  Zone4Exp FLOAT(10,2) NULL,
  Zone5Exp FLOAT(10,2) NULL,
  Zone6Exp FLOAT(10,2) NULL,
  Zone7Exp FLOAT(10,2) NULL,
  Zone8Exp FLOAT(10,2) NULL,
  Zone9Exp FLOAT(10,2) NULL,
  ModificationFactor FLOAT(10,2) NULL,
  OffBalanceFactor FLOAT(10,2) NULL,
  PRIMARY KEY (PolicyTransactionDataPK)
)
ENGINE= InnoDB
;

ALTER TABLE specialtyautoexposurerating DROP FOREIGN KEY Relation_327
;
DROP TABLE specialtyautopolicytransaction
;

DROP TABLE specialtyautoexposurerating
;

ALTER TABLE PolicyTransactionData
      ADD CONSTRAINT Relation_328 FOREIGN KEY
          ( PolicyTransactionPK )
          REFERENCES PolicyTransaction
          ( PolicyTransactionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE SpecialityAutoExpRating
      ADD CONSTRAINT Relation_331 FOREIGN KEY
          ( PolicyTransactionDataPK )
          REFERENCES PolicyTransactionData
          ( PolicyTransactionDataPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

