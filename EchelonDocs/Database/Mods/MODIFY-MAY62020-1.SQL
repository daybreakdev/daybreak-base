ALTER TABLE CargoDetails
      ADD DisplayString VARCHAR(255) NULL, 
      ADD Description VARCHAR(80) NULL
;

ALTER TABLE PolicyLocation
      CHANGE Building isVehiclesInBuilding TINYINT(10) NULL, 
      CHANGE Lot isVehiclesOnLot TINYINT(10) NULL
;

ALTER TABLE SubPolicy
      ADD PremiumModifier1 FLOAT(10,2) NULL, 
      ADD PremiumModifier2 FLOAT(10,2) NULL, 
      ADD TargetPremium FLOAT(10,2) NULL
;

