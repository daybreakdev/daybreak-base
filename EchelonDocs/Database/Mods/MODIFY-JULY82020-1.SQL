ALTER TABLE CargoDetails
      MODIFY CargoValue FLOAT(5,4) NULL, 
      MODIFY CargoExposure FLOAT(5,2) NULL
;

ALTER TABLE LienholderLessorRisk
      DROP INDEX FK_PolicyRisk, 
      DROP FOREIGN KEY Relation_289, 
      DROP COLUMN PolicyRiskPK, 
      DROP COLUMN LienLessorType
;

ALTER TABLE LienholderLessorVehicleDetails
      ADD LessorDeductible FLOAT(10,2) NULL
;

CREATE TABLE  CGLLocation
(
  CGLLocationPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  SubPolicyPK BIGINT(10) NULL,
  PolicyRiskPK BIGINT(10) NULL,
  PRIMARY KEY (CGLLocationPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_CGLSubPolicy ON CGLLocation (SubPolicyPK)
;

CREATE INDEX FK_PolicyLocation ON CGLLocation (PolicyRiskPK)
;

ALTER TABLE CGLLocation
      ADD CONSTRAINT Relation_306 FOREIGN KEY
          ( SubPolicyPK )
          REFERENCES CGLSubPolicy
          ( SubPolicyPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE CGLLocation
      ADD CONSTRAINT Relation_307 FOREIGN KEY
          ( PolicyRiskPK )
          REFERENCES PolicyLocation
          ( PolicyRiskPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

