ALTER TABLE CargoDetails
      MODIFY UWManual FLOAT(10,2) NULL
;

ALTER TABLE CargoSubPolicy
      ADD TotalCargoPartialPremium FLOAT(10,2) NULL
;

ALTER TABLE LienholderLessorVehicleDetails
      ADD VehicleDetailsSequence INT(10) NULL, 
      MODIFY VehicleNum VARCHAR(30) NULL
;

