ALTER TABLE Coverage
      ADD CoverageNumber INT(10) NULL
;

ALTER TABLE Endorsement
      ADD EndorsementNumber INT(10) NULL
;

CREATE TABLE  DataExtension
(
  DataExtensionPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  PRIMARY KEY (DataExtensionPK)
)
ENGINE= InnoDB
;

CREATE TABLE  ExtensionData
(
  ExtensionDataPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  ExtensionTypePK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  RowNum INT(10) NULL,
  ColumnId VARCHAR(120) NULL,
  ColumnValue VARCHAR(255) NULL,
  PRIMARY KEY (ExtensionDataPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_ExtensionType ON ExtensionData (ExtensionTypePK)
;

CREATE TABLE  ExtensionType
(
  ExtensionTypePK BIGINT(10) NOT NULL AUTO_INCREMENT,
  DataExtensionPK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  ExtensionId VARCHAR(80) NULL,
  PRIMARY KEY (ExtensionTypePK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_DataExtension ON ExtensionType (DataExtensionPK)
;

CREATE TABLE  NumberGenerator
(
  NumberGeneratorPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  NumberType VARCHAR(40) NULL,
  AvailableNumber BIGINT(10) NULL,
  PRIMARY KEY (NumberGeneratorPK)
)
ENGINE= InnoDB
;

CREATE TABLE  VehicleRateGroup
(
  VehicleRateGroupPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  VehicleAge INT(10) NULL,
  LowLimit FLOAT(8,2) NULL,
  UppLimit FLOAT(8,2) NULL,
  ImpliedRateGroup INT(10) NULL,
  PRIMARY KEY (VehicleRateGroupPK)
)
ENGINE= InnoDB
;

ALTER TABLE ExtensionData
      ADD CONSTRAINT Relation_296 FOREIGN KEY
          ( ExtensionTypePK )
          REFERENCES ExtensionType
          ( ExtensionTypePK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE ExtensionType
      ADD CONSTRAINT Relation_295 FOREIGN KEY
          ( DataExtensionPK )
          REFERENCES DataExtension
          ( DataExtensionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

