ALTER TABLE PersonalCustomer
      ADD InsuredSince DATE NULL, 
      ADD PreviousInsuranceCompany VARCHAR(255) NULL, 
      ADD PreviousExpiry DATE NULL
;

CREATE TABLE  Driver
(
  DriverPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  PolicyTransactionPK BIGINT(10) NULL,
  PersonPK BIGINT(10) NULL,
  AddressPK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  OriginatingPK BIGINT(10) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  LicenseProvState VARCHAR(40) NULL,
  DateLicensed DATE NULL,
  LicenseNumber VARCHAR(80) NULL,
  LicenseClass VARCHAR(40) NULL,
  LicenseStatus VARCHAR(40) NULL,
  CommercialLicenseDate DATE NULL,
  CommercialLicenseClass VARCHAR(40) NULL,
  PRIMARY KEY (DriverPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_PolicyTransaction ON Driver (PolicyTransactionPK)
;

CREATE INDEX FK_Person ON Driver (PersonPK)
;

CREATE INDEX FK_Address ON Driver (AddressPK)
;

CREATE TABLE  DriverClaim
(
  DriverClaimPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  DriverPK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  OriginatingPK BIGINT(10) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  PRIMARY KEY (DriverClaimPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_Driver ON DriverClaim (DriverPK)
;

CREATE TABLE  DriverConviction
(
  DriverConvictionPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  DriverPK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  OriginatingPK BIGINT(10) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  ConvictionDate DATE NULL,
  ConvictionType VARCHAR(120) NULL,
  ConvictionCode VARCHAR(80) NULL,
  ExpiryDate DATE NULL,
  Category VARCHAR(120) NULL,
  PRIMARY KEY (DriverConvictionPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_Driver ON DriverConviction (DriverPK)
;

CREATE TABLE  VehicleDriver
(
  VehicleDriverPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  PolicyRiskPK BIGINT(10) NULL,
  DriverPK BIGINT(10) NULL,
  CreatedDate DATETIME NULL,
  CreatedBy VARCHAR(80) NULL,
  UpdatedDate DATETIME NULL,
  UpdatedBy VARCHAR(80) NULL,
  SystemStatus VARCHAR(40) NULL,
  BusinessStatus VARCHAR(40) NULL,
  DisplayString VARCHAR(255) NULL,
  Description VARCHAR(80) NULL,
  OriginatingPK BIGINT(10) NULL,
  Version INT(10) NULL,
  VersionDate DATETIME NULL,
  VersionAction VARCHAR(40) NULL,
  DriverType VARCHAR(40) NULL,
  PRIMARY KEY (VehicleDriverPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_SpecialtyVehicleRisk ON VehicleDriver (PolicyRiskPK)
;

CREATE INDEX FK_Driver ON VehicleDriver (DriverPK)
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_348 FOREIGN KEY
          ( PolicyTransactionPK )
          REFERENCES PolicyTransaction
          ( PolicyTransactionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_349 FOREIGN KEY
          ( PersonPK )
          REFERENCES Person
          ( PersonPk )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_350 FOREIGN KEY
          ( AddressPK )
          REFERENCES Address
          ( AddressPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE DriverClaim
      ADD CONSTRAINT Relation_351 FOREIGN KEY
          ( DriverPK )
          REFERENCES Driver
          ( DriverPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE DriverConviction
      ADD CONSTRAINT Relation_352 FOREIGN KEY
          ( DriverPK )
          REFERENCES Driver
          ( DriverPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE VehicleDriver
      ADD CONSTRAINT Relation_353 FOREIGN KEY
          ( PolicyRiskPK )
          REFERENCES SpecialtyVehicleRisk
          ( PolicyRiskPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE VehicleDriver
      ADD CONSTRAINT Relation_354 FOREIGN KEY
          ( DriverPK )
          REFERENCES Driver
          ( DriverPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

