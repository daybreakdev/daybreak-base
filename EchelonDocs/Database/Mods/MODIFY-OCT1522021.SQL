CREATE TABLE  SpecialtyAutoPolicyTransaction
(
  SpecialtyAutoPolicyTransactionPK BIGINT(10) NOT NULL AUTO_INCREMENT,
  PolicyTransactionPK BIGINT(10) NULL,
  InsurancePolicyPK BIGINT(10) NOT NULL,
  LiabilityLimit INT(10) NULL,
  VehicleListReceivedDate DATE NULL,
  IsProvideVehicleRegistration TINYINT(10) NULL,
  VehicleRegistrationList VARCHAR(255) NULL,
  IsProvideDriverMVR TINYINT(10) NULL,
  ProvideDriverMVRList VARCHAR(255) NULL,
  IsProvideFuelTaxReport TINYINT(10) NULL,
  OtherRequirements VARCHAR(1024) NULL,
  TerritoryCode VARCHAR(10) NULL,
  StatCode VARCHAR(10) NULL,
  RuralityInd VARCHAR(10) NULL,
  AutoAssocMemberNum VARCHAR(80) NULL,
  AutoAssocClub VARCHAR(80) NULL,
  PRIMARY KEY (SpecialtyAutoPolicyTransactionPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_PolicyTransaction ON SpecialtyAutoPolicyTransaction (PolicyTransactionPK)
;

ALTER TABLE SpecialtyAutoPolicyTransaction
      ADD CONSTRAINT Relation_321 FOREIGN KEY
          ( PolicyTransactionPK )
          REFERENCES PolicyTransaction
          ( PolicyTransactionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

