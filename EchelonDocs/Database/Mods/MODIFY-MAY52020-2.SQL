ALTER TABLE CargoDetails
      ADD Average FLOAT(8,2) NULL, 
      ADD UWManual FLOAT(3,2) NULL, 
      ADD CargoValue FLOAT(4,4) NULL, 
      ADD CargoExposure FLOAT(4,2) NULL
;

