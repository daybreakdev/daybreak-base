ALTER TABLE driverclaim
      ADD PersonPK BIGINT(10) NULL, 
      ADD INDEX FK_Driver (PersonPK)
;

ALTER TABLE driverconviction
      ADD PersonPK BIGINT(10) NULL, 
      ADD INDEX FK_Driver (PersonPK)
;

ALTER TABLE vehicledriver
      ADD PersonPK BIGINT(10) NULL, 
      ADD INDEX FK_Driver (PersonPK)
;

CREATE TABLE  Driver
(
  PersonPK BIGINT(10) NOT NULL,
  AddressPK BIGINT(10) NULL,
  PolicyTransactionPK BIGINT(10) NULL,
  LicenseProvState VARCHAR(40) NULL,
  DateLicensed DATE NULL,
  LicenseNumber VARCHAR(80) NULL,
  LicenseClass VARCHAR(40) NULL,
  LicenseStatus VARCHAR(40) NULL,
  CommercialLicenseDate DATE NULL,
  CommercialLicenseClass VARCHAR(40) NULL,
  PRIMARY KEY (PersonPK)
)
ENGINE= InnoDB
;

CREATE INDEX FK_Address ON Driver (AddressPK)
;

CREATE INDEX FK_PolicyTransaction ON Driver (PolicyTransactionPK)
;

ALTER TABLE driverclaim
      ADD CONSTRAINT Relation_364 FOREIGN KEY
          ( PersonPK )
          REFERENCES Driver
          ( PersonPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE driverconviction
      ADD CONSTRAINT Relation_365 FOREIGN KEY
          ( PersonPK )
          REFERENCES Driver
          ( PersonPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE vehicledriver
      ADD CONSTRAINT Relation_366 FOREIGN KEY
          ( PersonPK )
          REFERENCES Driver
          ( PersonPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_361 FOREIGN KEY
          ( PersonPK )
          REFERENCES Person
          ( PersonPk )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_362 FOREIGN KEY
          ( AddressPK )
          REFERENCES Address
          ( AddressPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

ALTER TABLE Driver
      ADD CONSTRAINT Relation_363 FOREIGN KEY
          ( PolicyTransactionPK )
          REFERENCES PolicyTransaction
          ( PolicyTransactionPK )
      ON DELETE RESTRICT
      ON UPDATE RESTRICT
;

