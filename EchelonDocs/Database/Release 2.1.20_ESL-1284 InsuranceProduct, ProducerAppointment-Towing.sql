-- This script will do the following:
-- 1) Insert records to Insurance Product for Towing and LHT
-- 2) Update producerappointments table for Towing - link with producer & address (company)

INSERT INTO InsuranceProduct (Description, MarketCd, ProductCd, ProgramCd, Jurisdictions, CreatedDate, CreatedBy, Version, VersionDate) 
VALUES
('Ontario Towing', 'Specialty Insurance System', 'TOWING', 'TO', 'ON', CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP),
('Ontario LHT', 'Specialty Insurance System', 'LHT', 'LO', 'ON', CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP);

INSERT INTO producerappointments (InsuranceProductPK, CompanyPK, CreatedDate, CreatedBy, Version, VersionDate, AddressPK, DisplayString, Description, Jurisdictions)
VALUES
( (SELECT InsuranceProduct.InsuranceProductPK from InsuranceProduct where ProgramCd = 'TO'), 
  (SELECT producer.CompanyPK FROM producer WHERE producer.ProducerID = 'PBL003'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'PBL003'),
  'Towing Broker','PBL Insurance Limited','ON'),
( (SELECT InsuranceProduct.InsuranceProductPK from InsuranceProduct where ProgramCd = 'TO'), 
  (SELECT producer.CompanyPK FROM producer WHERE producer.ProducerID = 'MIC002'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'MIC002'), 
  'Towing Broker','My Insurance Brokers','ON'),
( (SELECT InsuranceProduct.InsuranceProductPK from InsuranceProduct where ProgramCd = 'TO'), 
  (SELECT producer.CompanyPK FROM producer WHERE producer.ProducerID = 'BMI002'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'BMI002'), 
  'Towing Broker','Baird MacGregor Insurance Brokers LP','ON'),
( (SELECT InsuranceProduct.InsuranceProductPK from InsuranceProduct where ProgramCd = 'TO'), 
  (SELECT producer.CompanyPK FROM producer WHERE producer.ProducerID = 'HAR002'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'HAR002'), 
  'Towing Broker','Hargraft Schofield LP','ON'),
( (SELECT InsuranceProduct.InsuranceProductPK from InsuranceProduct where ProgramCd = 'TO'), 
  (SELECT producer.CompanyPK FROM producer WHERE producer.ProducerID = 'NFP001'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'NFP001'), 
  'Towing Broker','NFP Canada Corp.','ON')
; 