DROP TEMPORARY TABLE IF EXISTS VehiclePK;

CREATE TEMPORARY TABLE VehiclePK
select veh.PolicyRiskPK, risk.DisplayString,
						(
							select veh2.VehicleSequence
							from PolicyRisk, SubPolicy, PolicyTransaction, PolicyVersion, PolicyVersion pv2, PolicyTransaction pt2, SubPolicy subpol2, PolicyRisk risk2, SpecialtyVehicleRisk veh2
							where PolicyRisk.PolicyRiskPK             = veh.PolicyRiskPK
							and SubPolicy.SubPolicyPK				  = PolicyRisk.SubPolicyPK
							and PolicyTransaction.PolicyTransactionPK = SubPolicy.PolicyTransactionPK
							and PolicyVersion.PolicyVersionPK         = PolicyTransaction.PolicyVersionPK
							and pv2.InsurancePolicyPK       = PolicyVersion.InsurancePolicyPK
							and pt2.PolicyVersionPK		    = pv2.PolicyVersionPK
							and subpol2.PolicyTransactionPK = pt2.PolicyTransactionPK
							and risk2.SubPolicyPK           = subpol2.SubPolicyPK 
							and veh2.PolicyRiskPK           = risk2.PolicyRiskPK
							and veh2.VehicleSequence	    is not null
							and risk2.DisplayString         = PolicyRisk.DisplayString
						) as newVehicleSequence
	from SpecialtyVehicleRisk veh, PolicyRisk risk, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleSequence  is null
	and risk.PolicyRiskPK      = veh.PolicyRiskPK
	and subpol.SubPolicyPK     = risk.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK
	/*and ip.QuoteNum in ('CAON200044', 'CAON200043')*/
;

update SpecialtyVehicleRisk, VehiclePK
set SpecialtyVehicleRisk.VehicleSequence = VehiclePK.newVehicleSequence
where SpecialtyVehicleRisk.PolicyRiskPK = VehiclePK.PolicyRiskPK
;

DROP TEMPORARY TABLE VehiclePK;
