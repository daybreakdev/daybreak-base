-- This script will do the following:
-- 1) Insert Everest Reinsurance Canada company - load company, address tables
-- 2) Insert to Reinsurer table - link with company & address

INSERT INTO company (LegalName, DBAName, PhoneNumber, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, DisplayString, BusinessType, PreferredLanguage, Version, VersionDate) 
VALUES
('Everest Reinsurance Canada', 'Everest Reinsurance', '(416) 862-5899', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp4', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP)
;

INSERT INTO address (AddressLine1, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, City, ProvState, PostalZip, DisplayString, Version, VersionDate)
VALUES
('130 King Street West, Suite 2520', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Toronto', 'ON', 'M5X 1E3', 'LHTReinsAddr4', 0, CURRENT_TIMESTAMP)
;

INSERT INTO location (AddressPK, CreatedDate, CreatedBy, CompanyPK, SystemStatus, BusinessStatus, Description, LocationNumber, Version, VersionDate)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr4'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp4'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP)
;

INSERT INTO reinsurer (CompanyPK, Version, VersionDate, CreatedDate, CreatedBy, SystemStatus, BusinessStatus,  Description)
VALUES
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp4'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp4'))
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'LHTReinsComp%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'LHTReinsAddr%';