-- Adds a new policy number count for LHT policies
INSERT INTO NumberGenerator
    (NumberType, AvailableNumber)
VALUES
    ('LHTPOLICYNUMCNT', 217015400)
;
