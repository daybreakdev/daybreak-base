/*
Fix Coverages
*/
IF OBJECT_ID('#TempCov', 'U') IS NOT NULL 
DROP TABLE #TempCov; 
CREATE TABLE #TempCov (
	BasePolicyNum varchar(120),
	QuoteNum varchar(120),
	CoverageCd varchar(120),
    CoveragePK int,
    LiabilityLimit int
);

-- Subpolicy
INSERT INTO #TempCov 
select ip.BasePolicyNum, ip.QuoteNum, cc.CoverageCd, cc.CoveragePK, spk.LiabilityLimit
from Coverage cc
, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where cc.CoverageCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (cc.Limit1Amount is null or cc.Limit1Amount = 0)
and sp.CoverageDetailsPK = cc.CoverageDetailsPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

--Vehicle
INSERT INTO #TempCov 
select ip.BasePolicyNum, ip.QuoteNum, cc.CoverageCd, cc.CoveragePK, spk.LiabilityLimit
from Coverage cc
, PolicyRisk pr, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where cc.CoverageCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (cc.Limit1Amount is null or cc.Limit1Amount = 0)
and pr.CoverageDetailsPK = cc.CoverageDetailsPK
and sp.SubPolicyPK = pr.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

--SELECT * FROM #TempCov;
/*
Base Policy Num	Quote Num	CoverageCd	CoveragePK	Liability Limit
OTO212015158	CAON200079	TPL			14302		5000000
OTO212015157	CAON200083	TPL			16109		2000000
				CAON200086	TPL			21024		2000000
OTO212015157	CAON200083	TPL			22169		2000000
OTO212015157	CAON200083	TPL			22215		2000000
				CAON200089	BIPD		22444		5000000
				CAON200089	PAIL		22445		5000000
				CAON200089	TPL			22448		5000000
OTO212015143	CAON200064	TPL			15224		5000000
OTO212015143	CAON200064	TPL			15722		5000000
OTO212015157	CAON200083	TPL			16117		2000000
OTO212015157	CAON200083	TPL			16124		2000000
OTO212015157	CAON200083	TPL			16131		2000000
OTO212015157	CAON200083	TPL			16138		2000000
OTO212015157	CAON200083	TPL			16145		2000000
OTO212015158	CAON200079	TPL			16154		5000000
OTO212015158	CAON200079	TPL			16174		5000000
OTO212015158	CAON200079	TPL			16176		5000000
OTO212015158	CAON200079	TPL			16186		5000000
OTO212015158	CAON200079	TPL			16189		5000000
OTO212015158	CAON200079	TPL			16201		5000000
OTO212015158	CAON200079	TPL			16208		5000000
OTO212015158	CAON200079	TPL			16215		5000000
OTO212015158	CAON200079	TPL			16219		5000000
OTO212015158	CAON200079	TPL			16229		5000000
OTO212015158	CAON200079	TPL			16236		5000000
OTO212015143	CAON200064	TPL			16318		5000000
OTO212015127	CAON200029	TPL			21162		5000000
				CAON200086	TPL			21985		2000000
				CAON200086	TPL			21999		2000000
				CAON200086	TPL			22011		2000000
				CAON200086	TPL			21992		2000000
				CAON200086	TPL			22004		2000000
OTO212015157	CAON200083	TPL			22177		2000000
OTO212015157	CAON200083	TPL			22184		2000000
OTO212015157	CAON200083	TPL			22191		2000000
OTO212015157	CAON200083	TPL			22198		2000000
OTO212015157	CAON200083	TPL			22205		2000000
OTO212015157	CAON200083	TPL			22223		2000000
OTO212015157	CAON200083	TPL			22230		2000000
OTO212015157	CAON200083	TPL			22237		2000000
OTO212015157	CAON200083	TPL			22244		2000000
OTO212015157	CAON200083	TPL			22251		2000000
OTO212015157	CAON200083	TPL			22260		2000000
				CAON200089	TPL			22492		5000000
				CAON200089	TPL			22456		5000000
				CAON200089	TPL			22478		5000000
				CAON200089	TPL			22500		5000000
				CAON200088	TPL			22520		5000000
				CAON200088	TPL			22521		5000000
				CAON200088	TPL			22528		5000000
*/

update Coverage
set Coverage.Limit1Amount = (select #TempCov.LiabilityLimit from #TempCov where #TempCov.CoveragePK = Coverage.CoveragePK)
where Coverage.CoveragePK in (select #TempCov.CoveragePK from #TempCov)
;

Drop Table #TempCov;

/*
Fix Endorsements
*/

IF OBJECT_ID('##TempEnd', 'U') IS NOT NULL 
DROP TABLE #TempEnd; 
CREATE TABLE #TempEnd (
	BasePolicyNum varchar(120),
	QuoteNum varchar(120),
	EndorsementCd varchar(120),
    Endorsementsk int,
    LiabilityLimit int
);

-- SubPolicy
INSERT INTO #TempEnd 
select ip.BasePolicyNum, ip.QuoteNum, ee.EndorsementCd, ee.Endorsementsk, spk.LiabilityLimit
from Endorsement ee
, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where ee.EndorsementCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (ee.Limit1Amount is null or ee.Limit1Amount = 0)
and sp.CoverageDetailsPK = ee.CoverageDetailsPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

--Vehicle
INSERT INTO #TempEnd 
select ip.BasePolicyNum, ip.QuoteNum, ee.EndorsementCd, ee.Endorsementsk, spk.LiabilityLimit
from Endorsement ee
, PolicyRisk pr, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where ee.EndorsementCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (ee.Limit1Amount is null or ee.Limit1Amount = 0)
and pr.CoverageDetailsPK = ee.CoverageDetailsPK
and sp.SubPolicyPK = pr.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

--SELECT * FROM #TempEnd;
/*
Base Policy Num	Quote Num	EndorsementCd	Endorsementsk	Liability Limit
OTO212015134	CAON200053	EON102			7550			5000000
OTO212015146	CAON200070	EON102			8632			5000000
OTO212015159	CAON200084	EON102			8669			5000000
*/

update Endorsement
set Endorsement.Limit1Amount = (select #TempEnd.LiabilityLimit from #TempEnd where #TempEnd.Endorsementsk = Endorsement.Endorsementsk)
where Endorsement.Endorsementsk in (select #TempEnd.Endorsementsk from #TempEnd)
;

Drop Table #TempEnd;
