
update SubPolicy
set SubPolicy.EffectiveDate = (select ptm.TermEffectiveDate from PolicyTransaction pt, PolicyTerm ptm
								where pt.PolicyTransactionPK = SubPolicy.PolicyTransactionPK
								  and ptm.PolicyTermPK = pt.PolicyTermPK
								)
where SubPolicy.SubPolicyPK in (
select sp.SubPolicyPK
from SubPolicy sp, PolicyTransaction pt, PolicyVersion pv
where sp.EffectiveDate is null
and sp.BusinessStatus = 'NEW'
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and (pv.PolicyTxnType = 'NEWBUSINESS' or pv.PolicyTxnType = 'RENEWAL')
);

update SubPolicy
set SubPolicy.EffectiveDate = (select pv.VersionDate from PolicyTransaction pt, PolicyVersion pv
								where pt.PolicyTransactionPK = SubPolicy.PolicyTransactionPK
								  and pv.PolicyVersionPK = pt.PolicyVersionPK
								)
where SubPolicy.SubPolicyPK in (
select sp.SubPolicyPK
from SubPolicy sp, PolicyTransaction pt, PolicyVersion pv
where sp.EffectiveDate is null
and sp.BusinessStatus = 'NEW'
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and pv.PolicyTxnType = 'POLICYCHANGE'
);

WHILE exists(select sp.SubPolicyPK from SubPolicy sp where sp.EffectiveDate is null)
BEGIN 

update SubPolicy
set SubPolicy.EffectiveDate = (select sp.EffectiveDate from SubPolicy sp where sp.SubPolicyPK = SubPolicy.OriginatingPK)
where SubPolicy.SubPolicyPK in (
	select top 999999999 sp.SubPolicyPK from SubPolicy sp where sp.EffectiveDate is null order by sp.OriginatingPK
);

END