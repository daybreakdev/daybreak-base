update SpecialtyVehicleRisk
set SpecialtyVehicleRisk.VehicleSequence = (
											select count(risk2.PolicyRiskPK)
											from PolicyRisk, PolicyRisk risk2, SpecialtyVehicleRisk veh2
											where PolicyRisk.PolicyRiskPK = SpecialtyVehicleRisk.PolicyRiskPK
											and risk2.SubPolicyPK         = PolicyRisk.SubPolicyPK 
											and veh2.PolicyRiskPK         = risk2.PolicyRiskPK
											and risk2.CreatedDate        <= PolicyRisk.CreatedDate
											)
where SpecialtyVehicleRisk.PolicyRiskPK in 
(select veh.PolicyRiskPK
	from SpecialtyVehicleRisk veh, PolicyRisk risk, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleSequence  is null
	and risk.PolicyRiskPK      = veh.PolicyRiskPK
	and subpol.SubPolicyPK     = risk.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK
	/*and ip.QuoteNum in ('CAON200044', 'CAON200043')*/

	and not exists (
		select risk0.PolicyRiskPK
		from PolicyVersion pv0, PolicyTransaction pt0, SubPolicy subpol0, PolicyRisk risk0, SpecialtyVehicleRisk veh0
		where pv0.InsurancePolicyPK     = ip.InsurancePolicyPK
		and pt0.PolicyVersionPK         = pv0.PolicyVersionPK
		and subpol0.PolicyTransactionPK = pt0.PolicyTransactionPK
		and risk0.SubPolicyPK           = subpol0.SubPolicyPK 
		and veh0.PolicyRiskPK           = risk0.PolicyRiskPK
		and risk0.DisplayString         = risk.DisplayString
		and risk0.CreatedDate           < risk.CreatedDate
	)
)
