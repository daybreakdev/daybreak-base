update PolicyTerm
set PolicyTerm.RatebookId = 'ONTOWING', PolicyTerm.RatebookVersion = 1
where PolicyTerm.PolicyTermPK in (
select pt.PolicyTermPK
from PolicyTerm pt , InsurancePolicy ip
where (pt.RatebookId is null or pt.RatebookId != 'ONTOWING' or pt.RatebookVersion is null or pt.RatebookVersion != 1)
and ip.InsurancePolicyPK = pt.InsurancePolicyPK
and ip.ProductCd = 'TOWING'
)