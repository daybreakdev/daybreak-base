-- This script will do the following:
-- 1) Insert LHT ON brokers - load company, address and location tables
-- 2) Insert to producer table - link with company
-- 3) Load details to producerappointments table - link with producer & address (company)

INSERT INTO company (LegalName, DBAName, PhoneNumber, FaxNumber, CreatedDate, CreatedBy, DisplayString, BusinessType, PreferredLanguage, Version, VersionDate) 
VALUES
('Brokerlink Inc.', 'Brokerlink Inc.', '905-727-4605', '905-727-5770', CURRENT_TIMESTAMP , 'system', 'LHTBrComp1', 'LHT', 'E', 0, CURRENT_TIMESTAMP)
;

INSERT INTO address (AddressLine1, CreatedDate, CreatedBy, City, ProvState, PostalZip, DisplayString, Version, VersionDate)
VALUES
('400-238 Wellington Street East', CURRENT_TIMESTAMP , 'system', 'Aurora', 'ON', 'L4G 1J5', 'LHTBrAddr1', 0, CURRENT_TIMESTAMP)
;

INSERT INTO location (AddressPK, CreatedDate, CreatedBy, CompanyPK, LocationNumber, Version, VersionDate)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTBrAddr1'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTBrComp1'),
 0, 0, CURRENT_TIMESTAMP)
;

INSERT INTO producer (ProducerId, CreatedDate, CreatedBy, CompanyPK, Version, VersionDate) 
VALUES
('BKL001', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTBrComp1'), 0, CURRENT_TIMESTAMP)
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'LHTBrComp%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'LHTBrAddr%';

-- All LHT Producer Appointments & Links
INSERT INTO producerappointments (InsuranceProductPK, CompanyPK, CreatedDate, CreatedBy, Version, VersionDate, AddressPK, DisplayString, Description, Jurisdictions)
VALUES
( (SELECT InsuranceProductPK from InsuranceProduct where ProgramCd = 'LO'), 
  (SELECT CompanyPK FROM producer WHERE producer.ProducerID = 'BKL001'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT AddressPK FROM location, producer WHERE location.CompanyPK = producer.CompanyPK AND producer.ProducerID = 'BKL001'), 
  'LHT ON Broker','Brokerlink Inc.','ON')
; 