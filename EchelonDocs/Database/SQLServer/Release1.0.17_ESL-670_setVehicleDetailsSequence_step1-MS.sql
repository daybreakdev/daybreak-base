update LienholderLessorVehicleDetails
set LienholderLessorVehicleDetails.VehicleDetailsSequence = (select count(veh1.LienholderLessorVehicleDetailsPK)+1
											from LienholderLessorVehicleDetails veh1
											where veh1.LienholderLessorPK = LienholderLessorVehicleDetails.LienholderLessorPK
											  and veh1.SubPolicyPK  = LienholderLessorVehicleDetails.SubPolicyPK
											  and (veh1.CreatedDate < LienholderLessorVehicleDetails.CreatedDate
											       or 
												   (veh1.CreatedDate = LienholderLessorVehicleDetails.CreatedDate 
												    and veh1.LienholderLessorVehicleDetailsPK < LienholderLessorVehicleDetails.LienholderLessorVehicleDetailsPK)
												  ))
where LienholderLessorVehicleDetails.LienholderLessorVehicleDetailsPK in 
(select veh.LienholderLessorVehicleDetailsPK
	from LienholderLessorVehicleDetails veh, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleDetailsSequence is null
	and subpol.SubPolicyPK     = veh.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK
	--and ip.QuoteNum in ('CAON200049','CAON200056')

	and not exists (
		select veh0.LienholderLessorVehicleDetailsPK
		from PolicyVersion pv0, PolicyTransaction pt0, SubPolicy subpol0, LienholderLessorVehicleDetails veh0
		where pv0.InsurancePolicyPK     = ip.InsurancePolicyPK
		and pt0.PolicyVersionPK         = pv0.PolicyVersionPK
		and subpol0.PolicyTransactionPK = pt0.PolicyTransactionPK
		and veh0.SubPolicyPK            = subpol0.SubPolicyPK 
		and veh0.LienholderLessorPK     = veh.LienholderLessorPK
		and veh0.VehicleNum             = veh.VehicleNum
		and veh0.VehicleYear            = veh.VehicleYear
		and veh0.SerialNum              = veh.SerialNum
		and veh0.CreatedDate            < veh.CreatedDate
	)
)
