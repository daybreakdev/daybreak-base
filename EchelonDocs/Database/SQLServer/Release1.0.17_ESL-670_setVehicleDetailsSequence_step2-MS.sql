update LienholderLessorVehicleDetails
set LienholderLessorVehicleDetails.VehicleDetailsSequence =
											(
											select veh2.VehicleDetailsSequence
											from SubPolicy, PolicyTransaction, PolicyVersion,
												 PolicyVersion pv2, PolicyTransaction pt2, SubPolicy subpol2, LienholderLessorVehicleDetails veh2
											where SubPolicy.SubPolicyPK				  = LienholderLessorVehicleDetails.SubPolicyPK
											and PolicyTransaction.PolicyTransactionPK = SubPolicy.PolicyTransactionPK
											and PolicyVersion.PolicyVersionPK         = PolicyTransaction.PolicyVersionPK
											and pv2.InsurancePolicyPK       = PolicyVersion.InsurancePolicyPK
											and pt2.PolicyVersionPK		    = pv2.PolicyVersionPK
											and subpol2.PolicyTransactionPK = pt2.PolicyTransactionPK
											and veh2.SubPolicyPK            = subpol2.SubPolicyPK
											and veh2.LienholderLessorPK     = LienholderLessorVehicleDetails.LienholderLessorPK
											and veh2.VehicleDetailsSequence is not null
											and veh2.VehicleNum             = LienholderLessorVehicleDetails.VehicleNum
											and veh2.VehicleYear            = LienholderLessorVehicleDetails.VehicleYear
											and veh2.SerialNum              = LienholderLessorVehicleDetails.SerialNum
											)
where LienholderLessorVehicleDetails.LienholderLessorVehicleDetailsPK in 
(select veh.LienholderLessorVehicleDetailsPK
	from LienholderLessorVehicleDetails veh, SubPolicy subpol, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
	where veh.VehicleDetailsSequence  is null
	and subpol.SubPolicyPK     = veh.SubPolicyPK
	and pt.PolicyTransactionPK = subpol.PolicyTransactionPK
	and pv.PolicyVersionPK     = pt.PolicyVersionPK
	and ip.InsurancePolicyPK   = pv.InsurancePolicyPK
	--and ip.QuoteNum in ('CAON200049','CAON200056')
)