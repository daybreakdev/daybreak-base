UPDATE PolicyVersion 
SET BusinessStatus = 'PENDINGRENEWAL'
WHERE PolicyVersionPK = 555;

UPDATE PolicyTransaction 
SET PolicyVersionPK = NULL, PolicyTermPK = NULL 
WHERE PolicyTransactionPK IN (818,819);

UPDATE PolicyVersion 
SET InsurancePolicyPK = NULL, PolicyTermPK = NULL 
WHERE PolicyVersionPK = 520;