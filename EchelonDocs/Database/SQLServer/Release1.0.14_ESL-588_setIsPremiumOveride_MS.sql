update Coverage set isPremiumOveride = 1
where CoverageCd in ('DF','TTC','BI','CHM','EBE','UA','TLL','WLL','IB','SP','AB','MTCC','BIPD','CL','IR','CM','DCPD','AP','CTC','PD','OCI','CL61','SP64','MRC','DC')
and isPremiumOveride = 0
and AnnualPremium is not null;

update Endorsement set isPremiumOveride = 1
where EndorsementCd in ('OPCF2','OEF72','OPCF43','OPCF20','OPCF27B','OPCF25A','OPCF27','OPCF38','EON20G','OPCF44R')
and isPremiumOveride = 0
and AnnualPremium is not null;