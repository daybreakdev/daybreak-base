update address 
	set AddressLine1 = '825 Queen Street East', PostalZip = 'M4M 1H8'
	where AddressPK = (
		select l.AddressPK from 	
		company C, location L
		where C.CompanyPK = L.CompanyPK 
		and C.LegalName = 'Hargraft Schofield LP')