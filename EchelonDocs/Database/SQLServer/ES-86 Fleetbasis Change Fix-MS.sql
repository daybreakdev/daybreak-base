update SpecialityAutoSubPolicy
set SpecialityAutoSubPolicy.isFleet = 0
where SpecialityAutoSubPolicy.isFleet is null
and SpecialityAutoSubPolicy.SubPolicyPK in (select ssp.SubPolicyPK
from SpecialityAutoSubPolicy ssp, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
where ssp.isFleet is null
and sp.SubPolicyPK = ssp.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and ip.ProductCd in ('NONFLEET'));

update SpecialityAutoSubPolicy
set SpecialityAutoSubPolicy.isFleet = 1
where SpecialityAutoSubPolicy.isFleet is null
and SpecialityAutoSubPolicy.SubPolicyPK in (select ssp.SubPolicyPK
from SpecialityAutoSubPolicy ssp, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip
where ssp.isFleet is null
and sp.SubPolicyPK = ssp.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and ip.ProductCd in ('TOWING','LHT'));