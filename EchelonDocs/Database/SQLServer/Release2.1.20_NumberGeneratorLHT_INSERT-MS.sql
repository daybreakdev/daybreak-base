-- This script adds a new quote & policy number count for LHT policies
INSERT INTO NumberGenerator
(NumberType,AvailableNumber)
VALUES
	('LHTQUOTENUMCNT',210500),
    ('LHTPOLICYNUMCNT', 217015400)
;