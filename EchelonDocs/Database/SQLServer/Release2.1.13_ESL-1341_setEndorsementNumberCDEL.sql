DROP TABLE #TempEndNumberCDEL;

SELECT endt1.BasePolicyNum, MaxEndNo, en.EndorsementCd AS EndCode, en.EndorsementNumber AS EndNumber, en.Endorsementsk AS EndPK 
INTO #TempEndNumberCDEL
FROM insurancepolicy ip, policyversion pv, policytransaction ptx, subpolicy sp, coveragedetails cd, Endorsement en, 
	(SELECT BasePolicyNum, max(EndNumber) + 1 AS MaxEndNo FROM 
			(SELECT ip.BasePolicyNum, en.EndorsementCd AS EndCode, en.EndorsementNumber AS EndNumber
			FROM insurancepolicy ip, policyversion pv, policytransaction ptx, Endorsement en
			WHERE ip.InsurancePolicyPK = pv.InsurancePolicyPK
			AND ptx.PolicyVersionPK = pv.PolicyVersionPK
			AND en.CoverageDetailsPK = ptx.CoverageDetailsPK
			AND pv.SystemStatus = 'ACTIVE' 
			AND pv.BusinessStatus = 'ISSUED'
			UNION
			SELECT ip.BasePolicyNum, en.EndorsementCd AS EndCode, en.EndorsementNumber AS EndNumber 
			FROM insurancepolicy ip, policyversion pv, policytransaction ptx, policyrisk pr, subpolicy sp, coveragedetails cd, Endorsement en 
			WHERE ip.InsurancePolicyPK = pv.InsurancePolicyPK 
			AND ptx.PolicyVersionPK = pv.PolicyVersionPK
			AND sp.PolicyTransactionPK = ptx.PolicyTransactionPK
			AND pr.SubPolicyPK = sp.SubPolicyPK
			AND cd.CoverageDetailsPK = pr.CoverageDetailsPK
			AND en.CoverageDetailsPK = cd.CoverageDetailsPK 
			AND pv.SystemStatus = 'ACTIVE' 
			AND pv.BusinessStatus = 'ISSUED'
			UNION
			SELECT ip.BasePolicyNum, en.EndorsementCd AS EndCode, en.EndorsementNumber AS EndNumber 
			FROM insurancepolicy ip, policyversion pv, policytransaction ptx, subpolicy sp, coveragedetails cd, Endorsement en
			WHERE ip.InsurancePolicyPK = pv.InsurancePolicyPK
			AND ptx.PolicyVersionPK = pv.PolicyVersionPK
			AND sp.PolicyTransactionPK = ptx.PolicyTransactionPK
			AND cd.CoverageDetailsPK = sp.CoverageDetailsPK 
			AND en.CoverageDetailsPK = cd.CoverageDetailsPK 
			AND pv.SystemStatus = 'ACTIVE' 
			AND pv.BusinessStatus = 'ISSUED') endt 
	GROUP by BasePolicyNum) endt1 
WHERE endt1.BasePolicyNum = ip.BasePolicyNum 
AND ip.InsurancePolicyPK = pv.InsurancePolicyPK 
AND ptx.PolicyVersionPK = pv.PolicyVersionPK 
AND sp.PolicyTransactionPK = ptx.PolicyTransactionPK 
AND cd.CoverageDetailsPK = sp.CoverageDetailsPK 
AND en.CoverageDetailsPK = cd.CoverageDetailsPK 
AND pv.SystemStatus = 'ACTIVE'	
AND pv.BusinessStatus = 'ISSUED'
AND en.EndorsementCd = 'CDEL'
AND en.EndorsementNumber IS NULL;

UPDATE Endorsement
SET Endorsement.EndorsementNumber = #TempEndNumberCDEL.MaxEndNo
FROM #TempEndNumberCDEL
WHERE Endorsement.Endorsementsk = #TempEndNumberCDEL.EndPK;

DROP TABLE #TempEndNumberCDEL;