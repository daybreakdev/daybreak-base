-- This script will do the following:
-- 1) Insert Echelon Insurance company - load company, address tables
-- 2) Insert to Reinsurer table - link with company & address

INSERT INTO company (LegalName, DBAName, PhoneNumber, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, DisplayString, BusinessType, PreferredLanguage, Version, VersionDate) 
VALUES
('Echelon Insurance', 'Echelon Insurance', '(416) 360-2006', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'LHTReinsComp3', 'ReInsurer', 'E', 0, CURRENT_TIMESTAMP)
;

INSERT INTO address (AddressLine1, CreatedDate, CreatedBy, SystemStatus, BusinessStatus, City, ProvState, PostalZip, DisplayString, Version, VersionDate)
VALUES
('2680 Matheson Blvd E', CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 'Mississauga', 'ON', 'L4W 0A5', 'LHTReinsAddr3', 0, CURRENT_TIMESTAMP)
;

INSERT INTO location (AddressPK, CreatedDate, CreatedBy, CompanyPK, SystemStatus, BusinessStatus, Description, LocationNumber, Version, VersionDate)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'LHTReinsAddr3'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp3'), 'ACTIVE', 'ACTIVE', 'Reins Location',
 0, 0, CURRENT_TIMESTAMP)
;

INSERT INTO reinsurer (CompanyPK, Version, VersionDate, CreatedDate, CreatedBy, SystemStatus, BusinessStatus,  Description)
VALUES
((SELECT CompanyPK FROM company WHERE company.DisplayString = 'LHTReinsComp3'),
 0, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP , 'system', 'ACTIVE', 'ACTIVE', 
 (SELECT LegalName FROM company WHERE company.DisplayString = 'LHTReinsComp3'))
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'LHTReinsComp%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'LHTReinsAddr%';