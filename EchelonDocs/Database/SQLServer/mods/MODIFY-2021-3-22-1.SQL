ALTER TABLE [CGLLocation]
ADD    [CreatedDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [CreatedBy] [UserId] NULL
go

ALTER TABLE [CGLLocation]
ADD    [UpdatedDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [UpdatedBy] [UserId] NULL
go

ALTER TABLE [CGLLocation]
ADD    [SystemStatus] [Status] NULL
go

ALTER TABLE [CGLLocation]
ADD    [BusinessStatus] [Status] NULL
go

ALTER TABLE [CGLLocation]
ADD    [Version] [Version] NULL
go

ALTER TABLE [CGLLocation]
ADD    [VersionDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [VersionAction] VARCHAR(40) NULL
go

ALTER TABLE [CGLLocation]
ADD    [Description] VARCHAR(80) NULL
go

ALTER TABLE [CGLLocation]
ADD    [DisplayString] VARCHAR(255) NULL
go

