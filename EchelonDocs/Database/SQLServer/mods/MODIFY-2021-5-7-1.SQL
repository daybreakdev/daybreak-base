ALTER TABLE [SubPolicy]
ADD    [SubPolicyNamedInsured] VARCHAR(255) NULL
go

ALTER TABLE [PolicyNotes]
ADD    [OriginatingVersion] [Version] NULL
go

ALTER TABLE [PolicyNotes]
ADD    [OriginatingDate] [AuditDateTime] NULL
go

