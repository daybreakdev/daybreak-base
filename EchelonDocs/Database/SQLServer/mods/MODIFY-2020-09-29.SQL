ALTER TABLE [PolicyTerm]
ADD    [RatebookId] VARCHAR(80) NULL
go

ALTER TABLE [PolicyTerm]
ADD    [RatebookVersion] INT NULL
go

ALTER TABLE [PolicyTransaction]
ADD    [RateFactorsLinkPK] [PrimaryKeys] NULL
go

ALTER TABLE [PolicyTransaction]
ADD    [isReRateReqd] [Logical] NULL
go

ALTER TABLE [PolicyTransaction]
ADD    [LastRatedDateTime] DATETIME NULL
go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [PolicyTransaction] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

ALTER TABLE [Coverage]
ADD    [RateFactorsLinkPK] [PrimaryKeys] NULL
go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [Coverage] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

ALTER TABLE [Endorsement]
ADD    [RateFactorsLinkPK] [PrimaryKeys] NULL
go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [Endorsement] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

ALTER TABLE [PolicyRisk]
ADD    [RateFactorsLinkPK] [PrimaryKeys] NULL
go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [PolicyRisk] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

ALTER TABLE [SubPolicy]
ADD    [RateFactorsLinkPK] [PrimaryKeys] NULL
go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [SubPolicy] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [RateFactor] ( 
   [RateFactorPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [RateFactorsLinkPK] [PrimaryKeys] NULL
)

go

ALTER TABLE  [RateFactor]
ADD CONSTRAINT [PK_RateFactor] PRIMARY KEY NONCLUSTERED  ([RateFactorPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [RateFactorsLink] ON [RateFactor] ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [RateFactorsLink] ( 
   [RateFactorsLinkPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL
)

go

ALTER TABLE  [RateFactorsLink]
ADD CONSTRAINT [PK_RateFactorsLink] PRIMARY KEY NONCLUSTERED  ([RateFactorsLinkPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_310' and type='F')
ALTER TABLE [PolicyTransaction] WITH NOCHECK
      ADD   CONSTRAINT [Relation_310] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_311' and type='F')
ALTER TABLE [Coverage] WITH NOCHECK
      ADD   CONSTRAINT [Relation_311] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_312' and type='F')
ALTER TABLE [Endorsement] WITH NOCHECK
      ADD   CONSTRAINT [Relation_312] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_313' and type='F')
ALTER TABLE [PolicyRisk] WITH NOCHECK
      ADD   CONSTRAINT [Relation_313] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_314' and type='F')
ALTER TABLE [SubPolicy] WITH NOCHECK
      ADD   CONSTRAINT [Relation_314] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_309' and type='F')
ALTER TABLE [RateFactor] WITH NOCHECK
      ADD   CONSTRAINT [Relation_309] FOREIGN KEY
          ( [RateFactorsLinkPK] )
          REFERENCES [RateFactorsLink]
          ( [RateFactorsLinkPK] )
go

