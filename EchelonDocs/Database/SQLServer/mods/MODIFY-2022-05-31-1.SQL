IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DriverClaim]') AND name = N'Driver')
DROP INDEX [Driver] ON [dbo].[DriverClaim]
go

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Relation_347]') AND parent_object_id = OBJECT_ID(N'[DriverClaim]'))
ALTER TABLE [DriverClaim] DROP CONSTRAINT [Relation_347]
go

ALTER TABLE [dbo].[DriverClaim]
DROP COLUMN [DriverPK]
go

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[DriverConviction]') AND name = N'Driver')
DROP INDEX [Driver] ON [dbo].[DriverConviction]
go

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Relation_348]') AND parent_object_id = OBJECT_ID(N'[DriverConviction]'))
ALTER TABLE [DriverConviction] DROP CONSTRAINT [Relation_348]
go

ALTER TABLE [dbo].[DriverConviction]
DROP COLUMN [DriverPK]
go

IF EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[VehicleDriver]') AND name = N'Driver')
DROP INDEX [Driver] ON [dbo].[VehicleDriver]
go

IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[Relation_349]') AND parent_object_id = OBJECT_ID(N'[VehicleDriver]'))
ALTER TABLE [VehicleDriver] DROP CONSTRAINT [Relation_349]
go

ALTER TABLE [dbo].[VehicleDriver]
DROP COLUMN [DriverPK]
go

