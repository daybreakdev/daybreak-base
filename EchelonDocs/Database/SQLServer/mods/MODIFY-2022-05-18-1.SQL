SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [SpecialtyVehicleRiskAdjustment] ( 
   [SpecialtyVehicleRiskAdjustmentPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PolicyRiskPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [OriginatingPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [VehicleDescription] VARCHAR(255) NULL,
   [AdjustmentType] VARCHAR(80) NULL,
   [AdjustmentDate] DATE NULL,
   [ProRataFactor] FLOAT NULL,
   [PremiumChange] [Premium] NULL
)

go

ALTER TABLE  [SpecialtyVehicleRiskAdjustment]
ADD CONSTRAINT [PK_SpecialtyVehicleRiskAdjustment] PRIMARY KEY NONCLUSTERED  ([SpecialtyVehicleRiskAdjustmentPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [SpecialtyVehicleRisk] ON [SpecialtyVehicleRiskAdjustment] ([PolicyRiskPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_343' and type='F')
ALTER TABLE [SpecialtyVehicleRiskAdjustment] WITH NOCHECK
      ADD   CONSTRAINT [Relation_343] FOREIGN KEY
          ( [PolicyRiskPK] )
          REFERENCES [SpecialtyVehicleRisk]
          ( [PolicyRiskPK] )
go

