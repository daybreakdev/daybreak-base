EXECUTE sp_rename '[Building].[PercentageSprinklerd]', 'PercentageSprinklered', 'COLUMN'
go

ALTER TABLE [RelatedPolicy]
ADD    [SystemStatus] [Status] NULL
go

ALTER TABLE [RelatedPolicy]
ADD    [BusinessStatus] [Status] NULL
go

ALTER TABLE [DriverClaim]
ADD    [LossDate] DATE NULL
go

ALTER TABLE [DriverClaim]
ADD    [Vehicle] VARCHAR(80) NULL
go

ALTER TABLE [DriverClaim]
ADD    [ClaimType] VARCHAR(30) NULL
go

ALTER TABLE [DriverClaim]
ADD    [FaultResponsibility] VARCHAR(10) NULL
go

ALTER TABLE [DriverClaim]
ADD    [TotalPaid] [Premium] NULL
go

ALTER TABLE [DriverClaim]
ADD    [ClaimNum] INT NULL
go

ALTER TABLE [DriverClaim]
ADD    [isInsurerClaim] [Logical] NULL
go

SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [DriverSuspension] ( 
   [DriverSuspensionPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [PersonPK] [PrimaryKeys] NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [SuspensionType] VARCHAR(40) NULL,
   [SuspensionReason] VARCHAR(120) NULL,
   [SuspensionStartDate] DATE NULL,
   [SuspensionEndDate] DATE NULL,
   [Source] VARCHAR(120) NULL,
   [Length] INT NULL
)

go

ALTER TABLE  [DriverSuspension]
ADD CONSTRAINT [PK_DriverSuspension] PRIMARY KEY NONCLUSTERED  ([DriverSuspensionPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [Driver] ON [DriverSuspension] ([PersonPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_359' and type='F')
ALTER TABLE [DriverSuspension] WITH NOCHECK
      ADD   CONSTRAINT [Relation_359] FOREIGN KEY
          ( [PersonPK] )
          REFERENCES [Driver]
          ( [PersonPK] )
go

