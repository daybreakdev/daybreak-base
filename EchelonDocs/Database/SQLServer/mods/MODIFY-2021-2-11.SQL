ALTER TABLE [PolicyVersion]
ADD    [isReversed] [Logical] NULL
go

ALTER TABLE [PolicyVersion]
ADD    [ReversedDate] DATETIME NULL
go

ALTER TABLE [PolicyVersion]
ADD    [ReversedVersionNum] INT NULL
go

