ALTER TABLE [Insurer]
ADD    [InsurerID] VARCHAR(80) NULL
go

SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [InsuranceProduct] ( 
   [InsuranceProductpk] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [CompanyPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [MarketCd] VARCHAR(60) NULL,
   [ProductCd] VARCHAR(60) NULL,
   [ProgramCd] VARCHAR(60) NULL,
   [ClientType] VARCHAR(60) NULL,
   [Jurisdictions] VARCHAR(255) NULL,
   [TransactionTypes] VARCHAR(255) NULL
)

go

ALTER TABLE  [InsuranceProduct]
ADD CONSTRAINT [PK_InsuranceProduct] PRIMARY KEY NONCLUSTERED  ([InsuranceProductpk] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [Insurer] ON [InsuranceProduct] ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


SET ANSI_NULLS ON
go
SET QUOTED_IDENTIFIER ON
go
SET ANSI_PADDING ON
go
SET ANSI_WARNINGS ON
go

CREATE TABLE  [ProducerAppointments] ( 
   [ProducerAppointmentsPK] [PrimaryKeys] NOT NULL IDENTITY (1,1),
   [InsuranceProductPK] [PrimaryKeys] NULL,
   [CompanyPK] [PrimaryKeys] NULL,
   [Version] [Version] NULL,
   [VersionDate] [AuditDateTime] NULL,
   [VersionAction] VARCHAR(40) NULL,
   [CreatedDate] [AuditDateTime] NULL,
   [CreatedBy] [UserId] NULL,
   [UpdatedDate] [AuditDateTime] NULL,
   [UpdatedBy] [UserId] NULL,
   [SystemStatus] [Status] NULL,
   [BusinessStatus] [Status] NULL,
   [DisplayString] VARCHAR(255) NULL,
   [Description] VARCHAR(80) NULL,
   [Jurisdictions] VARCHAR(255) NULL,
   [TransactionTypes] VARCHAR(255) NULL,
   [EffectiveDate] DATE NULL,
   [ExpiryDate] DATE NULL,
   [isAllLocations] [Logical] NULL,
   [isPilotProducer] [Logical] NULL,
   [AddressPK] [PrimaryKeys] NULL
)

go

ALTER TABLE  [ProducerAppointments]
ADD CONSTRAINT [PK_ProducerAppointments] PRIMARY KEY NONCLUSTERED  ([ProducerAppointmentsPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [InsuranceProduct] ON [ProducerAppointments] ([InsuranceProductPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [Producer] ON [ProducerAppointments] ([CompanyPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go

CREATE NONCLUSTERED INDEX [Location] ON [ProducerAppointments] ([AddressPK] ASC)
WITH ( IGNORE_DUP_KEY = OFF)

go


IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_315' and type='F')
ALTER TABLE [InsuranceProduct] WITH NOCHECK
      ADD   CONSTRAINT [Relation_315] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Insurer]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_316' and type='F')
ALTER TABLE [ProducerAppointments] WITH NOCHECK
      ADD   CONSTRAINT [Relation_316] FOREIGN KEY
          ( [InsuranceProductPK] )
          REFERENCES [InsuranceProduct]
          ( [InsuranceProductpk] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_317' and type='F')
ALTER TABLE [ProducerAppointments] WITH NOCHECK
      ADD   CONSTRAINT [Relation_317] FOREIGN KEY
          ( [CompanyPK] )
          REFERENCES [Producer]
          ( [CompanyPK] )
go

IF NOT EXISTS (SELECT 1 FROM sys.objects where name='Relation_318' and type='F')
ALTER TABLE [ProducerAppointments] WITH NOCHECK
      ADD   CONSTRAINT [Relation_318] FOREIGN KEY
          ( [AddressPK] )
          REFERENCES [Location]
          ( [AddressPK] )
go

