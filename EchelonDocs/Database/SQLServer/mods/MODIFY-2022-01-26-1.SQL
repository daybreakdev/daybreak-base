ALTER TABLE [CoverageReinsurance]
ADD    [PremiumModifier1] FLOAT NULL
go

ALTER TABLE [CoverageReinsurance]
ADD    [PremiumModifier2] FLOAT NULL
go

ALTER TABLE [CoverageReinsurance]
ADD    [TargetPremium] FLOAT NULL
go

ALTER TABLE [EndorsementReinsurance]
ADD    [PremiumModifier1] FLOAT NULL
go

ALTER TABLE [EndorsementReinsurance]
ADD    [PremiumModifier2] FLOAT NULL
go

ALTER TABLE [EndorsementReinsurance]
ADD    [TargetPremium] FLOAT NULL
go

