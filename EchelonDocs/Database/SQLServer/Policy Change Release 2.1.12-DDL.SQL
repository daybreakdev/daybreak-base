-- MODIFY-2021-1-25.SQL 
ALTER TABLE [PolicyRisk]
ADD    [EffectiveDate] DATETIME NULL
go

ALTER TABLE [PolicyRisk]
ADD    [ExpiryDate] DATETIME NULL
go

ALTER TABLE [SubPolicy]
ADD    [EffectiveDate] DATETIME NULL
go

ALTER TABLE [SubPolicy]
ADD    [ExpiryDate] DATETIME NULL
go

-- MODIFY-2021-2-11.SQL
ALTER TABLE [PolicyVersion]
ADD    [isReversed] [Logical] NULL
go

ALTER TABLE [PolicyVersion]
ADD    [ReversedDate] DATETIME NULL
go

ALTER TABLE [PolicyVersion]
ADD    [ReversedVersionNum] INT NULL
go

-- MODIFY-2021-3-19.SQL
ALTER TABLE [LienholderLessorSubPolicy]
ADD    [CreatedBy] [UserId] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [CreatedAudit] [AuditDateTime] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [ModifiedBy] [UserId] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [ModifedAudit] [AuditDateTime] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [EditVersion] [Version] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [Version] [Version] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [VersionDate] [AuditDateTime] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [VersionAction] VARCHAR(40) NULL
go

-- MODIFY-2021-3-22.SQL
ALTER TABLE [LienholderLessorSubPolicy]
ADD    [SystemStatus] [Status] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [BusinessStatus] [Status] NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [DisplayString] VARCHAR(255) NULL
go

ALTER TABLE [LienholderLessorSubPolicy]
ADD    [Description] VARCHAR(80) NULL
go

EXECUTE sp_rename '[LienholderLessorSubPolicy].[CreatedAudit]', 'CreatedDate', 'COLUMN'
go

EXECUTE sp_rename '[LienholderLessorSubPolicy].[ModifiedBy]', 'UpdatedBy', 'COLUMN'
go

EXECUTE sp_rename '[LienholderLessorSubPolicy].[ModifedAudit]', 'UpdatedDate', 'COLUMN'
go

ALTER TABLE [dbo].[LienholderLessorSubPolicy]
DROP COLUMN [EditVersion]
go

-- MODIFY-2021-3-22-1.SQL
ALTER TABLE [CGLLocation]
ADD    [CreatedDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [CreatedBy] [UserId] NULL
go

ALTER TABLE [CGLLocation]
ADD    [UpdatedDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [UpdatedBy] [UserId] NULL
go

ALTER TABLE [CGLLocation]
ADD    [SystemStatus] [Status] NULL
go

ALTER TABLE [CGLLocation]
ADD    [BusinessStatus] [Status] NULL
go

ALTER TABLE [CGLLocation]
ADD    [Version] [Version] NULL
go

ALTER TABLE [CGLLocation]
ADD    [VersionDate] [AuditDateTime] NULL
go

ALTER TABLE [CGLLocation]
ADD    [VersionAction] VARCHAR(40) NULL
go

ALTER TABLE [CGLLocation]
ADD    [Description] VARCHAR(80) NULL
go

ALTER TABLE [CGLLocation]
ADD    [DisplayString] VARCHAR(255) NULL
go

-- MODIFY-2021-5-7-1.SQL

ALTER TABLE [SubPolicy]
ADD    [SubPolicyNamedInsured] VARCHAR(255) NULL
go

ALTER TABLE [PolicyNotes]
ADD    [OriginatingVersion] [Version] NULL
go

ALTER TABLE [PolicyNotes]
ADD    [OriginatingDate] [AuditDateTime] NULL
go

-- MODIFY-2021-5-12-1.SQL

ALTER TABLE [PolicyNotes]
ADD    [OriginatingAuthor] [UserId] NULL
go
