-- This script will do the following:
-- 1) Insert records to Insurance Product for Non Fleet
-- 2) Insert brokers - load company, address and location tables
-- 3) Insert to producer table - link with company
-- 4) Load details to producerappointments table - link with producer & address (company)

INSERT INTO InsuranceProduct (Description, MarketCd, ProductCd, ProgramCd, Jurisdictions, CreatedDate, CreatedBy, Version, VersionDate) 
VALUES
('Ontario NonFleet', 'Specialty Insurance System', 'NONFLEET', 'TO', 'ON', CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP);

-- Not sure about the folloiwng. I dont think we need these...
INSERT INTO company (LegalName, DBAName, CreatedDate, CreatedBy, DisplayString, BusinessType, PreferredLanguage, Version, VersionDate) 
VALUES
('PBL Insurance Limited', 'PBL Insurance Limited', CURRENT_TIMESTAMP, 'system', 'NFBrComp1', 'NONFLEET', 'E', 0, CURRENT_TIMESTAMP),
('My Insurance Brokers', 'My Insurance Brokers', CURRENT_TIMESTAMP, 'system', 'NFBrComp2', 'NONFLEET', 'E', 0, CURRENT_TIMESTAMP),
('Baird MacGregor Insurance Brokers LP', 'Baird MacGregor Insurance Brokers LP', CURRENT_TIMESTAMP, 'system', 'NFBrComp3', 'NONFLEET', 'E', 0, CURRENT_TIMESTAMP),
('Hargraft Schofield LP', 'Hargraft Schofield LP', CURRENT_TIMESTAMP, 'system', 'NFBrComp4', 'NONFLEET', 'E', 0, CURRENT_TIMESTAMP),
('NFP Canada Corp.', 'NFP Canada Corp.', CURRENT_TIMESTAMP, 'system', 'NFBrComp5', 'NONFLEET', 'E', 0, CURRENT_TIMESTAMP)
;

INSERT INTO address (AddressLine1, CreatedDate, CreatedBy, City, ProvState, PostalZip, DisplayString, Version, VersionDate)
VALUES
('1500 Don Mills Road, Suite 501', CURRENT_TIMESTAMP , 'system', 'North York', 'ON', 'M3B 3K4', 'NFBrAddr1', 0, CURRENT_TIMESTAMP),
('50 West Wilmot Street, Unit 5', CURRENT_TIMESTAMP , 'system', 'Richmond Hill', 'ON', 'L4B 1M5', 'NFBrAddr2', 0, CURRENT_TIMESTAMP),
('825 Queen Street East', CURRENT_TIMESTAMP , 'system', 'Toronto', 'ON', 'M6J 1G1', 'NFBrAddr3', 0, CURRENT_TIMESTAMP),
('825 Queen Street East', CURRENT_TIMESTAMP , 'system', 'Toronto', 'ON', 'M4M 1H8', 'NFBrAddr4', 0, CURRENT_TIMESTAMP),
('1500 Don Mills Rd., Suite 501', CURRENT_TIMESTAMP , 'system', 'Toronto', 'ON', 'M3B 3K4', 'NFBrAddr5', 0, CURRENT_TIMESTAMP)
;

INSERT INTO location (AddressPK, CreatedDate, CreatedBy, CompanyPK, LocationNumber, Version, VersionDate)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'NFBrAddr1'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp1'),
 0, 0, CURRENT_TIMESTAMP),
 ((SELECT AddressPK FROM address WHERE address.DisplayString = 'NFBrAddr2'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp2'),
 0, 0, CURRENT_TIMESTAMP),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'NFBrAddr3'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp3'),
 0, 0, CURRENT_TIMESTAMP),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'NFBrAddr4'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp4'),
 0, 0, CURRENT_TIMESTAMP),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'NFBrAddr5'), CURRENT_TIMESTAMP , 'system',
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp5'),
 0, 0, CURRENT_TIMESTAMP) 
;

INSERT INTO producer (ProducerId, CreatedDate, CreatedBy, CompanyPK, Version, VersionDate) 
VALUES
('PBL003', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp1'), 0, CURRENT_TIMESTAMP),
('MIC002', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp2'), 0, CURRENT_TIMESTAMP),
('BMI002', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp3'), 0, CURRENT_TIMESTAMP),
('HAR002', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp4'), 0, CURRENT_TIMESTAMP),
('NFP001', CURRENT_TIMESTAMP , 'system', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'NFBrComp5'), 0, CURRENT_TIMESTAMP)
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'NFBrComp%';
UPDATE address SET DisplayString = null WHERE DisplayString like 'NFBrAddr%';

-- All Producer Appointments & Links
INSERT INTO producerappointments (InsuranceProductPK, CompanyPK, CreatedDate, CreatedBy, Version, VersionDate, AddressPK, DisplayString, Description, Jurisdictions)
VALUES
( (SELECT InsuranceProductPK from InsuranceProduct WHERE ProgramCd = 'TO' AND ProductCd = 'NONFLEET'), 
  (SELECT company.CompanyPK FROM company, producer WHERE producer.CompanyPK = company.CompanyPK AND producer.ProducerID = 'PBL003' AND company.BusinessType='NONFLEET'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK  FROM location, producer, company WHERE location.CompanyPK = producer.CompanyPK AND location.CompanyPK = company.CompanyPK AND company.BusinessType='NONFLEET' AND producer.ProducerID = 'PBL003'), 
  'TOWING Non Fleet Broker','PBL Insurance Limited','ON'),
( (SELECT InsuranceProductPK from InsuranceProduct WHERE ProgramCd = 'TO' AND ProductCd = 'NONFLEET'), 
  (SELECT company.CompanyPK FROM company, producer WHERE producer.CompanyPK = company.CompanyPK AND producer.ProducerID = 'MIC002' AND company.BusinessType='NONFLEET'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK  FROM location, producer, company WHERE location.CompanyPK = producer.CompanyPK AND location.CompanyPK = company.CompanyPK AND company.BusinessType='NONFLEET' AND producer.ProducerID = 'MIC002'), 
  'TOWING Non Fleet Broker','My Insurance Brokers','ON'),  
( (SELECT InsuranceProductPK from InsuranceProduct WHERE ProgramCd = 'TO' AND ProductCd = 'NONFLEET'), 
  (SELECT company.CompanyPK FROM company, producer WHERE producer.CompanyPK = company.CompanyPK AND producer.ProducerID = 'BMI002' AND company.BusinessType='NONFLEET'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK  FROM location, producer, company WHERE location.CompanyPK = producer.CompanyPK AND location.CompanyPK = company.CompanyPK AND company.BusinessType='NONFLEET' AND producer.ProducerID = 'BMI002'), 
  'TOWING Non Fleet Broker','Baird MacGregor Insurance Brokers LP','ON'),
( (SELECT InsuranceProductPK from InsuranceProduct WHERE ProgramCd = 'TO' AND ProductCd = 'NONFLEET'), 
  (SELECT company.CompanyPK FROM company, producer WHERE producer.CompanyPK = company.CompanyPK AND producer.ProducerID = 'HAR002' AND company.BusinessType='NONFLEET'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK  FROM location, producer, company WHERE location.CompanyPK = producer.CompanyPK AND location.CompanyPK = company.CompanyPK AND company.BusinessType='NONFLEET' AND producer.ProducerID = 'HAR002'), 
  'TOWING Non Fleet Broker','Hargraft Schofield LP','ON'),
( (SELECT InsuranceProductPK from InsuranceProduct WHERE ProgramCd = 'TO' AND ProductCd = 'NONFLEET'), 
  (SELECT company.CompanyPK FROM company, producer WHERE producer.CompanyPK = company.CompanyPK AND producer.ProducerID = 'NFP001' AND company.BusinessType='NONFLEET'), CURRENT_TIMESTAMP , 'system', 0, CURRENT_TIMESTAMP,
  (SELECT location.AddressPK  FROM location, producer, company WHERE location.CompanyPK = producer.CompanyPK AND location.CompanyPK = company.CompanyPK AND company.BusinessType='NONFLEET' AND producer.ProducerID = 'NFP001'), 
  'TOWING Non Fleet Broker','NFP Canada Corp.','ON') 
; 
