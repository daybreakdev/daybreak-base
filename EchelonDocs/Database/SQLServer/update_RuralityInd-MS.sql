UPDATE specialtyautopackage
SET specialtyautopackage.RuralityInd = (
select distinct StatCodes.RuralityInd from StatCodes where StatCodes.TerritoryCode = specialtyautopackage.TerritoryCode
and StatCodes.StatCode = specialtyautopackage.StatCode
)
where specialtyautopackage.RuralityInd is null;