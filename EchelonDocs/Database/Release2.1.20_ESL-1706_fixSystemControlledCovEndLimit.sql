use daystar;

/*
Fix Coverages
*/
DROP TEMPORARY TABLE IF EXISTS TempCov;
CREATE TEMPORARY TABLE TempCov (
	BasePolicyNum varchar(120),
	QuoteNum varchar(120),
	CoverageCd varchar(120),
    CoveragePK int,
    LiabilityLimit int
);

##Subpolicy
INSERT INTO TempCov 
select ip.BasePolicyNum, ip.QuoteNum, cc.CoverageCd, cc.CoveragePK, spk.LiabilityLimit
from Coverage cc
, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where cc.CoverageCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (cc.Limit1Amount is null or cc.Limit1Amount = 0)
and sp.CoverageDetailsPK = cc.CoverageDetailsPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

##Vehicle
INSERT INTO TempCov 
select ip.BasePolicyNum, ip.QuoteNum, cc.CoverageCd, cc.CoveragePK, spk.LiabilityLimit
from Coverage cc
, PolicyRisk pr, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where cc.CoverageCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (cc.Limit1Amount is null or cc.Limit1Amount = 0)
and pr.CoverageDetailsPK = cc.CoverageDetailsPK
and sp.SubPolicyPK = pr.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

##SELECT * FROM TempCov;

update Coverage, TempCov
set Coverage.Limit1Amount = TempCov.LiabilityLimit
where Coverage.CoveragePK = TempCov.CoveragePK
;

Drop TEMPORARY Table TempCov;

/*
Fix Endorsements
*/
DROP TEMPORARY TABLE IF EXISTS TempEnd;
CREATE TEMPORARY TABLE TempEnd (
	BasePolicyNum varchar(120),
	QuoteNum varchar(120),
	EndorsementCd varchar(120),
    Endorsementsk int,
    LiabilityLimit int
);

##SubPolicy
INSERT INTO TempEnd 
select ip.BasePolicyNum, ip.QuoteNum, ee.EndorsementCd, ee.Endorsementsk, spk.LiabilityLimit
from Endorsement ee
, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where ee.EndorsementCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (ee.Limit1Amount is null or ee.Limit1Amount = 0)
and sp.CoverageDetailsPK = ee.CoverageDetailsPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

##Vehicle
INSERT INTO TempEnd 
select ip.BasePolicyNum, ip.QuoteNum, ee.EndorsementCd, ee.Endorsementsk, spk.LiabilityLimit
from Endorsement ee
, PolicyRisk pr, SubPolicy sp, PolicyTransaction pt, PolicyVersion pv, InsurancePolicy ip, SpecialtyAutoPackage spk
where ee.EndorsementCd in ('EON102','TPL','BIPD','PAIL','SPF6TPL')
and (ee.Limit1Amount is null or ee.Limit1Amount = 0)
and pr.CoverageDetailsPK = ee.CoverageDetailsPK
and sp.SubPolicyPK = pr.SubPolicyPK
and pt.PolicyTransactionPK = sp.PolicyTransactionPK
and pv.PolicyVersionPK = pt.PolicyVersionPK
and ip.InsurancePolicyPK = pv.InsurancePolicyPK
and spk.InsurancePolicyPK = ip.InsurancePolicyPK
;

##SELECT * FROM TempEnd;

update Endorsement, TempEnd
set Endorsement.Limit1Amount = TempEnd.LiabilityLimit
where Endorsement.Endorsementsk = TempEnd.Endorsementsk
;

Drop TEMPORARY Table TempEnd;
