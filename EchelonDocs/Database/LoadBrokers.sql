USE daystar;

INSERT INTO company (LegalName, DisplayString, Version) 
VALUES
('PBL Insurance Limited', 'BrokerCompany1', 0),
('My Insurance Brokers', 'BrokerCompany2', 0),
('Baird MacGregor Insurance Brokers LP', 'BrokerCompany3', 0),
('Hargraft Schofield LP', 'BrokerCompany4', 0),
('NFP Canada Corp.', 'BrokerCompany5', 0)
;

INSERT INTO address (AddressLine1, City, ProvState, PostalZip, DisplayString, Version)
VALUES
('1500 Don Mills Road, Suite 501', 'North York', 'ON', 'M3B 3K4', 'BrokerAddress1', 0),
('50 West Wilmot Street, Unit 5', 'Richmond Hill', 'ON', 'L4B 1M5', 'BrokerAddress2', 0),
('825 Queen Street East', 'Toronto', 'ON', 'M6J 1G1', 'BrokerAddress3', 0),
('825 Queen Street East', 'Toronto', 'ON', 'M4M 1H8', 'BrokerAddress4', 0),
('1500 Don Mills Rd., Suite 501', 'Toronto', 'ON', 'M3B 3K4', 'BrokerAddress5', 0)
;

INSERT INTO location (AddressPK, CompanyPK, LocationNumber, Version)
VALUES
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress1'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany1'),
 0, 0),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress2'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany2'),
 0, 0),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress3'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany3'),
 0, 0),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress4'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany4'),
 0, 0),
((SELECT AddressPK FROM address WHERE address.DisplayString = 'BrokerAddress5'),
 (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany5'),
 0, 0) 
;

INSERT INTO producer (ProducerId, CompanyPK, Version) 
VALUES
('PBL003', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany1'), 0),
('MIC002', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany2'), 0),
('BMI002', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany3'), 0),
('HAR002', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany4'), 0),
('NFP001', (SELECT CompanyPK FROM company WHERE company.DisplayString = 'BrokerCompany5'), 0)
;

UPDATE company SET DisplayString = null WHERE DisplayString like 'BrokerCompany%';

UPDATE address SET DisplayString = null WHERE DisplayString like 'BrokerAddress%';
