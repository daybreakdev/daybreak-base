package com.echelon.tests.acceptancetests;

public class SerenitySessionConstants {
	public static final String POLICY = "policy";
	public static final String PRODUCT_CODE = "productCode";
	public static final String PROGRAM_CODE = "programCode";
	public static final String CUSTOMER_TYPE = "customerType";
	public static final String EFFECTIVE_DATE = "effectiveDate";
}
