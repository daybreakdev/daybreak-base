package com.echelon.tests.steps;

import static com.echelon.tests.acceptancetests.SerenitySessionConstants.CUSTOMER_TYPE;
import static com.echelon.tests.acceptancetests.SerenitySessionConstants.PRODUCT_CODE;
import static com.echelon.tests.acceptancetests.SerenitySessionConstants.PROGRAM_CODE;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.time.LocalDate;

import com.echelon.tests.acceptancetests.SerenitySessionConstants;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.core.Serenity;

public class PolicyCreationStepDefinitions {
	@Given("a {word} customer")
	public void setCustomerType(String customerType) {
		Serenity.setSessionVariable(CUSTOMER_TYPE).to(customerType);
	}

	@Given("the product code {word}")
	public void setProductCode(String productCode) {
		Serenity.setSessionVariable(PRODUCT_CODE).to(productCode);
	}

	@Given("the program code {word}")
	public void setProgramCode(String programCode) {
		Serenity.setSessionVariable(PROGRAM_CODE).to(programCode);
	}

	@Given("^an effective date of (\\d{4})-(\\d{2})-(\\d{2})$")
	public void setEffectiveDate(int year, int month, int dayOfMonth) {
		LocalDate effectiveDate = LocalDate.of(year, month, dayOfMonth);
		Serenity.setSessionVariable(SerenitySessionConstants.EFFECTIVE_DATE).to(effectiveDate);
	}

	@When("^I create a new customer and insurance policy$")
	public void createANewCustomerAndInsurancePolicy() throws Throwable {
		assumeTrue(false, "Hibernate initialization errors");
	}
}
