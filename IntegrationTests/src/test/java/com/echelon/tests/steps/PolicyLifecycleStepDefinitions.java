package com.echelon.tests.steps;

import static com.echelon.tests.acceptancetests.SerenitySessionConstants.POLICY;
import static org.assertj.core.api.Assertions.assertThat;

import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.utils.Constants;

import io.cucumber.java.en.Then;
import net.serenitybdd.core.Serenity;

public class PolicyLifecycleStepDefinitions {
	@Then("^a new policy should be created$")
	public void verifyNewPolicyCreation() {
		InsurancePolicy policy = Serenity.sessionVariableCalled(POLICY);
		assertThat(policy).isNotNull();
		
		assertThat(policy.getBusinessStatus()).isEqualTo(Constants.BUSINESS_STATUS_PENDING);
		assertThat(policy.getSystemStatus()).isEqualTo(Constants.SYSTEN_STATUS_PENDING);
	}
}
