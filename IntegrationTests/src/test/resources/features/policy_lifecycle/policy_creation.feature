Feature: Creating a new insurance policy
  Creation of a new commercial customer and insurance policy

  Background:
    Given a COMMERCIAL customer
    
  Scenario Outline: Creating a new customer and insurance policy
  	Given the product code <product>
  	* the program code <program>
  	* an effective date of 2022-03-01
  	When I create a new customer and insurance policy
  	Then a new policy should be created
  	
  	Examples:
  		| product | program |
  		| TOWING  | TO      |
  		| LHT     | LO      |
