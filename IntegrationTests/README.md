# Integration Tests

This project contains the SIM's integration tests. They are run every time code is pushed to the
remote repository.

## Architecture

At a high level, the product is split into its component **features**: each feature gets a feature
definition file (see `src/test/resources/features/`) which defines the tests which specify the
feature's desired behaviour. Feature definitions are written in
[Gherkin](https://cucumber.io/docs/gherkin/reference/).

A feature definition is composed of one or more **scenarios**, optionally grouped into business
rules. A scenario is composed of **steps**, which are mapped to **step definitions**: Java code
written using [Cucumber](https://cucumber.io/docs/cucumber)
(see `src/test/java/com/echelon/tests/steps/`).

For example, the following feature definition describes a word-guessing game:

```gherkin
Feature: Guess the word

  # The first example has two steps
  Scenario: Maker starts a game
    When the Maker starts a game
    Then the Maker waits for a Breaker to join

  # The second example has three steps
  Scenario: Breaker joins a game
    Given the Maker has started a game with the word "silky"
    When the breaker joins the Maker's game
    Then the Breaker must guess a word with 5 characters
```

Each step should be self-contained, and - to the extent possible - should not relied on shared
state. This increases the chance that existing step definitions can be re-used in subsequent tests,
promoting reusability and reducing the amount of test code that must be written. When state sharing
cannot be avoided,
[Serenity's state sharing](https://johnfergusonsmart.com/sharing-state-steps-serenity-bdd) support
should be used.

## Running the Integration Tests

When the integration tests are triggered (by Maven's `verify` goal), JUnit's looks for test suites;
`com.echelon.tests.acceptancetests.AcceptanceTestSuite` tells JUnit to run the feature
specifications found in `src/test/resources/features` using the `CucumberWithSerenity` test runner.

Once the tests have completed, [Serenity](https://serenity-bdd.info) generates an HTML test report,
found in `target/site/serenity`.

## Tips & Tricks

Gherkin provides a number of features for organizing features and reducing repetition; in
particular, **Scenario Outline**, **Background**, data tables, and tags may all be useful.
