package com.echelon.domain.dto.specialtylines;

import java.io.Serializable;
import java.util.List;

import com.ds.ins.domain.dto.ReinsuranceItemDTO;

public class ReinsuranceReportDTO implements Serializable {
	private static final long serialVersionUID = -2885944154566351254L;

	private 				List<ReinsuranceItemDTO> 			reisnuranceItems;
	private 				Double								netPremReisnurnaceTotal;
	private 				Double								grossPremReisnurnaceTotal;
	private 				Double 								netPremRetentionTotal;
	private 				Double 								grossPremRetentionTotal;
	private 				Double 								netPremGrandTotal;
	private 				Double 								grossPremGrandTotal;
	
	public ReinsuranceReportDTO() {
		super();
	}

	public List<ReinsuranceItemDTO> getReisnuranceItems() {
		return reisnuranceItems;
	}

	public void setReisnuranceItems(List<ReinsuranceItemDTO> reisnuranceItems) {
		this.reisnuranceItems = reisnuranceItems;
	}

	public Double getNetPremReisnurnaceTotal() {
		return netPremReisnurnaceTotal;
	}

	public void setNetPremReisnurnaceTotal(Double netPremReisnurnaceTotal) {
		this.netPremReisnurnaceTotal = netPremReisnurnaceTotal;
	}

	public Double getGrossPremReisnurnaceTotal() {
		return grossPremReisnurnaceTotal;
	}

	public void setGrossPremReisnurnaceTotal(Double grossPremReisnurnaceTotal) {
		this.grossPremReisnurnaceTotal = grossPremReisnurnaceTotal;
	}

	public Double getNetPremRetentionTotal() {
		return netPremRetentionTotal;
	}

	public void setNetPremRetentionTotal(Double netPremRetentionTotal) {
		this.netPremRetentionTotal = netPremRetentionTotal;
	}

	public Double getGrossPremRetentionTotal() {
		return grossPremRetentionTotal;
	}

	public void setGrossPremRetentionTotal(Double grossPremRetentionTotal) {
		this.grossPremRetentionTotal = grossPremRetentionTotal;
	}

	public Double getNetPremGrandTotal() {
		return netPremGrandTotal;
	}

	public void setNetPremGrandTotal(Double netPremGrandTotal) {
		this.netPremGrandTotal = netPremGrandTotal;
	}

	public Double getGrossPremGrandTotal() {
		return grossPremGrandTotal;
	}

	public void setGrossPremGrandTotal(Double grossPremGrandTotal) {
		this.grossPremGrandTotal = grossPremGrandTotal;
	}
	
}
