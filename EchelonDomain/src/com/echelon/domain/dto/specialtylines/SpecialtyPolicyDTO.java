package com.echelon.domain.dto.specialtylines;

import java.util.HashMap;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;

public class SpecialtyPolicyDTO {

	//Key = current PK
	private HashMap<Long, SpecialtyPolicyItemDTO> 		vehicleItemsByPK;
	//Key = adjustment number
	private HashMap<Integer, SpecialtyPolicyItemDTO>   	vehicleItemsByAdjNum;
	private SpecialtyPolicyItemDTO 						cglSubPolicyItem;
	private SpecialtyPolicyItemDTO 						cargoSubPolicyItem;
	
	public SpecialtyPolicyDTO() {
		super();
		
		this.vehicleItemsByPK = new HashMap<Long, SpecialtyPolicyItemDTO>();
		this.vehicleItemsByAdjNum = new HashMap<Integer, SpecialtyPolicyItemDTO>();
	}

	public HashMap<Long, SpecialtyPolicyItemDTO> getVehicleItemsByPK() {
		return vehicleItemsByPK;
	}

	public HashMap<Integer, SpecialtyPolicyItemDTO> getVehicleItemsByAdjNum() {
		return vehicleItemsByAdjNum;
	}

	public void setVehicleItemsByAdjNum(HashMap<Integer, SpecialtyPolicyItemDTO> vehicleItemsByAdjNum) {
		this.vehicleItemsByAdjNum = vehicleItemsByAdjNum;
	}

	public SpecialtyPolicyItemDTO getCglSubPolicyItem() {
		return cglSubPolicyItem;
	}

	public void setCglSubPolicyItem(SpecialtyPolicyItemDTO cglSubPolicyItem) {
		this.cglSubPolicyItem = cglSubPolicyItem;
	}

	public SpecialtyPolicyItemDTO getCargoSubPolicyItem() {
		return cargoSubPolicyItem;
	}

	public void setCargoSubPolicyItem(SpecialtyPolicyItemDTO cargoSubPolicyItem) {
		this.cargoSubPolicyItem = cargoSubPolicyItem;
	}
	
	public SpecialtyPolicyItemDTO getVehicleItemByPK(Long pk) {
		SpecialtyPolicyItemDTO result = vehicleItemsByPK.get(pk);
		return result;
	}
	
	public SpecialtyPolicyItemDTO getVehicleItemByAdjNum(Integer num) {
		SpecialtyPolicyItemDTO result = vehicleItemsByAdjNum.get(num);
		return result;
	}
	
	public SpecialtyPolicyItemDTO getItemDTO(SpecialtyVehicleRiskAdjustment adj) {
		SpecialtyPolicyItemDTO result = null;
		
		if (adj != null) {
			if (adj.getSpecialtyVehicleRisk() != null) {
				result = getVehicleItemByPK(adj.getSpecialtyVehicleRisk().getPolicyRiskPK());
			}
			else if (adj.getCglSubPolicy() != null) {
				result = getCglSubPolicyItem();
			}
			else if (adj.getCargoSubPolicy() != null) {
				result = getCargoSubPolicyItem();
			}
		}
		
		return result;
	}
}
