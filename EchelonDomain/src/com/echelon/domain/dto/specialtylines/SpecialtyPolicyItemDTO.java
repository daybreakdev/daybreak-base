package com.echelon.domain.dto.specialtylines;

public class SpecialtyPolicyItemDTO {

	private Long    originatingPK;
	private Double 	originatingUnitRate;
	private Integer	originatingUnits;
	private Long	subOriginatingPK;
	private Double 	subOriginatingUnitRate;

	public SpecialtyPolicyItemDTO(Long originatingPK) {
		super();
		this.originatingPK = originatingPK;
	}

	public SpecialtyPolicyItemDTO(Long originatingPK, Double originatingUnitRate, Integer originatingUnits, 
			Long subOriginatingPK, Double subOriginatingUnitRate) {
		super();
		this.originatingPK = originatingPK;
		this.originatingUnitRate = originatingUnitRate;
		this.subOriginatingPK = subOriginatingPK;
		this.originatingUnits = originatingUnits;
		this.subOriginatingUnitRate = subOriginatingUnitRate;
	}

	public SpecialtyPolicyItemDTO(Long originatingPK, Integer originatingUnits, 
			Long subOriginatingPK, Double subOriginatingUnitRate) {
		super();
		this.originatingPK = originatingPK;
		this.subOriginatingPK = subOriginatingPK;
		this.originatingUnits = originatingUnits;
		this.subOriginatingUnitRate = subOriginatingUnitRate;
	}

	public Long getOriginatingPK() {
		return originatingPK;
	}

	public void setOriginatingPK(Long originatingPK) {
		this.originatingPK = originatingPK;
	}

	public Double getOriginatingUnitRate() {
		return originatingUnitRate;
	}

	public void setOriginatingUnitRate(Double originatingUnitRate) {
		this.originatingUnitRate = originatingUnitRate;
	}

	public Long getSubOriginatingPK() {
		return subOriginatingPK;
	}

	public void setSubOriginatingPK(Long subOriginatingPK) {
		this.subOriginatingPK = subOriginatingPK;
	}

	public Integer getOriginatingUnits() {
		return originatingUnits;
	}

	public void setOriginatingUnits(Integer originatingUnits) {
		this.originatingUnits = originatingUnits;
	}

	public Double getSubOriginatingUnitRate() {
		return subOriginatingUnitRate;
	}

	public void setSubOriginatingUnitRate(Double subOriginatingUnitRate) {
		this.subOriginatingUnitRate = subOriginatingUnitRate;
	}
	
}
