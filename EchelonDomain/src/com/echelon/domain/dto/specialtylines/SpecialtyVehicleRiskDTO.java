package com.echelon.domain.dto.specialtylines;

public class SpecialtyVehicleRiskDTO {
	private Long 			policyRiskPK;
	private Integer 		numberOfUnits;
	private Double			unitRate;
	private Double 			unitPremium;

	public SpecialtyVehicleRiskDTO(Long policyRiskPK, Integer numberOfUnits, Double unitRate, Double unitPremium) {
		super();
		this.policyRiskPK = policyRiskPK;
		this.numberOfUnits = numberOfUnits;
		this.unitRate = unitRate;
		this.unitPremium = unitPremium;
	}

	public Long getPolicyRiskPK() {
		return policyRiskPK;
	}

	public void setPolicyRiskPK(Long policyRiskPK) {
		this.policyRiskPK = policyRiskPK;
	}

	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}

	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}

	public Double getUnitRate() {
		return unitRate;
	}

	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}

	public Double getUnitPremium() {
		return unitPremium;
	}

	public void setUnitPremium(Double unitPremium) {
		this.unitPremium = unitPremium;
	}
	
}
