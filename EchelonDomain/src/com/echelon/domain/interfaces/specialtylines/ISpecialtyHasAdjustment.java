package com.echelon.domain.interfaces.specialtylines;

import java.util.Set;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;

public interface ISpecialtyHasAdjustment {

	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustments();
	public SpecialtyVehicleRiskAdjustment getSvrAdjustmentsByAdjNum(String groupID, Integer adjNum);
	public void addSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment);
}
