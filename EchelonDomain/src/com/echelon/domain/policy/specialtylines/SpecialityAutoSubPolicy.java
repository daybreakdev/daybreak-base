package com.echelon.domain.policy.specialtylines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;

@Entity
@Table(name = "SpecialityAutoSubPolicy")
@PrimaryKeyJoinColumn(name = "SubPolicyPK")
public class SpecialityAutoSubPolicy <RISKTYPE extends Risk> extends SubPolicy<RISKTYPE> {
	private static final long serialVersionUID = -846574131558714250L;
	private static List<String> copyExcludeList = Arrays.asList("lienholderLessorVehicleDetails", "additionalInsuredVehicles");

	private Boolean isFleet;
	private String fleetBasis;
	private Boolean isRiskSharingPool;
	
	@OneToMany(mappedBy = "autoSubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<LienholderLessorVehicleDetails> lienholderLessorVehicleDetails;
	
	@OneToMany(mappedBy = "autoSubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AdditionalInsuredVehicles> additionalInsuredVehicles;
	
	public SpecialityAutoSubPolicy() {
		this.lienholderLessorVehicleDetails = new HashSet<LienholderLessorVehicleDetails>();
		this.additionalInsuredVehicles		= new HashSet<AdditionalInsuredVehicles>();
	}
	
	public void addLienholderLessorVehicleDetails(LienholderLessorVehicleDetails details) {
		details.setAutoSubPolicy(this);
		lienholderLessorVehicleDetails.add(details);
	}
	
	public void removeLienholderLessorVehicleDetails(LienholderLessorVehicleDetails details) {		
		if (details.isActive() && (details.isNoChange() || details.isModified())) {
			details.setSystemStatus(IAuditable.INACTIVE);
			details.setBusinessStatus(IAuditable.DELETED);
		} else if (details.isPending()) {
			lienholderLessorVehicleDetails.remove(details);
			details.setAutoSubPolicy(null);
		}
	}
	
	public void addAdditionalInsuredVehicles(AdditionalInsuredVehicles vehicles) {
		vehicles.setAutoSubPolicy(this);
		additionalInsuredVehicles.add(vehicles);
	}
	
	public void removeAdditionalInsuredVehicles(AdditionalInsuredVehicles vehicles) {
		if (vehicles.isActive() && (vehicles.isNoChange() || vehicles.isModified())) {
			vehicles.setSystemStatus(IAuditable.INACTIVE);
			vehicles.setBusinessStatus(IAuditable.DELETED);
		} else if (vehicles.isPending()) {
			additionalInsuredVehicles.remove(vehicles);
			vehicles.setAutoSubPolicy(null);
		}
	}
	
	public Boolean getIsFleet() {
		return isFleet;
	}
	public void setIsFleet(Boolean isFleet) {
		this.isFleet = isFleet;
	}
	public String getFleetBasis() {
		return fleetBasis;
	}
	public void setFleetBasis(String fleetBasis) {
		this.fleetBasis = fleetBasis;
	}
	public Boolean getIsRiskSharingPool() {
		return isRiskSharingPool;
	}
	public void setIsRiskSharingPool(Boolean isRiskSharingPool) {
		this.isRiskSharingPool = isRiskSharingPool;
	}
	public Set<LienholderLessorVehicleDetails> getLienholderLessorVehicleDetails() {
		return lienholderLessorVehicleDetails;
	}
	public void setLienholderLessorVehicleDetails(Set<LienholderLessorVehicleDetails> lienholderLessorVehicleDetails) {
		this.lienholderLessorVehicleDetails = lienholderLessorVehicleDetails;
	}
	public Set<AdditionalInsuredVehicles> getAdditionalInsuredVehicles() {
		return additionalInsuredVehicles;
	}
	public void setAdditionalInsuredVehicles(Set<AdditionalInsuredVehicles> additionalInsuredVehicles) {
		this.additionalInsuredVehicles = additionalInsuredVehicles;
	}

	public LienholderLessorVehicleDetails findLHLVehDetailsByOriginatingPK(Long origPK) {
		LienholderLessorVehicleDetails rv = null;
		if (origPK != null) {
			for (LienholderLessorVehicleDetails lhlvd : this.lienholderLessorVehicleDetails) {
				if (origPK.equals(lhlvd.getOriginatingPK())) {
					rv = lhlvd;
					break;
				}
			}
		}
		return rv;
	}

	public AdditionalInsuredVehicles findADIVehicleByOriginatingPK(Long origPK) {
		AdditionalInsuredVehicles rv = null;
		if (origPK != null) {
			for (AdditionalInsuredVehicles lhlvd : this.additionalInsuredVehicles) {
				if (origPK.equals(lhlvd.getOriginatingPK())) {
					rv = lhlvd;
					break;
				}
			}
		}
		return rv;
	}
	
	public AdditionalInsuredVehicles findADIVehicleByPK(Long inPK) {
		AdditionalInsuredVehicles rv = null;
		if (inPK != null) {
			for (AdditionalInsuredVehicles lhlvd : this.additionalInsuredVehicles) {
				if (inPK.equals(lhlvd.getAdditionalInsuredVehiclesPK())) {
					rv = lhlvd;
					break;
				}
			}
		}
		return rv;
	}
	
	public Set<LienholderLessorVehicleDetails> findLHLVehDetailsByLienholderLessor(LienholderLessor lienholderLessor) {
		Set<LienholderLessorVehicleDetails> rv = null;
		
		if (lienholderLessor != null &&
			getLienholderLessorVehicleDetails() != null && !getLienholderLessorVehicleDetails().isEmpty()) {
			rv = getLienholderLessorVehicleDetails().stream()
						.filter(o -> o.getLienholderLessor() != null &&
									 o.getLienholderLessor().equals(lienholderLessor))
						.collect(Collectors.toSet());
		}
		
		return rv;
	}
	
	public LienholderLessorVehicleDetails findLHLVehDetailsByPk(Long inPK) {
		LienholderLessorVehicleDetails rv = null;
		for (LienholderLessorVehicleDetails lhlVD : this.lienholderLessorVehicleDetails) {
			if (Objects.equals(inPK, lhlVD.getLienholderLessorVehicleDetailsPK())) {
				rv = lhlVD;
				break;
			}
		}
		return rv;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof SpecialityAutoSubPolicy)) return false;
		return getSubPolicyPK() != null && getSubPolicyPK().equals(((SpecialityAutoSubPolicy<?>) obj).getSubPolicyPK());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		SpecialityAutoSubPolicy<Risk> aspClone = (SpecialityAutoSubPolicy<Risk>) super.clone();
		aspClone.setLienholderLessorVehicleDetails(new HashSet<LienholderLessorVehicleDetails>());
		for (LienholderLessorVehicleDetails origLHLVD : this.lienholderLessorVehicleDetails) {
			aspClone.addLienholderLessorVehicleDetails((LienholderLessorVehicleDetails) origLHLVD.clone());
		}
		aspClone.setAdditionalInsuredVehicles(new HashSet<AdditionalInsuredVehicles>());
		for (AdditionalInsuredVehicles origADIV : this.additionalInsuredVehicles) {
			AdditionalInsuredVehicles cloneADIV = (AdditionalInsuredVehicles) origADIV.clone(); 
			aspClone.addAdditionalInsuredVehicles(cloneADIV);

			// Update the cloned AdditionalInsured
			if (origADIV.getAdditionalInsured() != null) {
				cloneADIV.setAdditionalInsured(aspClone.findAdditionalInsuredByOriginatingPK(origADIV.getAdditionalInsured().getAdditionalInsuredPK()));
			}
		}
		final Stream<SpecialtyVehicleRisk> cloneVehicles = aspClone.getSubPolicyRisks().stream()
				.filter(risk -> risk instanceof SpecialtyVehicleRisk)
				.map(SpecialtyVehicleRisk.class::cast);
		cloneVehicles.forEach(vc -> {
			if (vc.getTowingVehiclePK() != null) {
				Risk towingClone = aspClone.findSubPcyRiskByOriginatingPK(vc.getTowingVehiclePK());
				if (towingClone != null)
					vc.setTowingVehiclePK(towingClone.getPolicyRiskPK());
			}
		});
		return aspClone;
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		List<String> rv = (new ArrayList<String>());
		rv.addAll(super.getCopyExcludeList());
		rv.addAll(copyExcludeList);
		return rv;
	}

}
