package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.baseclasses.BaseBusinessEntity;
import com.ds.ins.domain.policy.AdditionalInsured;

@Entity
@Table(name = "AdditionalInsuredVehicles")
public class AdditionalInsuredVehicles extends AbstractAuditable {
	private static final long serialVersionUID = 3725036655784573033L;
	
	private static List<String> auditExcludeList = Arrays.asList(
			"additionalInsuredVehiclesPK", "additionalInsured", "autoSubPolicy", "originatingPK"
	);
	
	private static List<String> copyExcludeList = Arrays.asList(
			"additionalInsuredVehiclesPK", "additionalInsured", "autoSubPolicy", "originatingPK"
	);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AdditionalInsuredVehiclesPK", updatable = false, nullable = false)
	private 				Long 							additionalInsuredVehiclesPK;
		
	@ManyToOne
	@JoinColumn(name = "AdditionalInsuredPK")
	private 				AdditionalInsured 				additionalInsured;
	
	@ManyToOne
	@JoinColumn(name = "SubPolicyPK", nullable = true)
	private					SpecialityAutoSubPolicy<?>  	autoSubPolicy;

	private 				String							SerialNum;
	private 				Long							originatingPK;
	
	public Long getAdditionalInsuredVehiclesPK() {
		return additionalInsuredVehiclesPK;
	}
	public void setAdditionalInsuredVehiclesPK(Long additionalInsuredVehiclesPK) {
		this.additionalInsuredVehiclesPK = additionalInsuredVehiclesPK;
	}
	public AdditionalInsured getAdditionalInsured() {
		return additionalInsured;
	}
	public void setAdditionalInsured(AdditionalInsured additionalInsured) {
		this.additionalInsured = additionalInsured;
	}
	public Long getOriginatingPK() {
		return originatingPK;
	}
	public void setOriginatingPK(Long originatingPK) {
		this.originatingPK = originatingPK;
	}
	public String getSerialNum() {
		return SerialNum;
	}
	public void setSerialNum(String serialNum) {
		SerialNum = serialNum;
	}
	public SpecialityAutoSubPolicy<?> getAutoSubPolicy() {
		return autoSubPolicy;
	}
	public void setAutoSubPolicy(SpecialityAutoSubPolicy<?> autoSubPolicy) {
		this.autoSubPolicy = autoSubPolicy;
	}
	@Override
	public int hashCode() {
		return 31;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof AdditionalInsuredVehicles)) return false;
		return getAdditionalInsuredVehiclesPK() != null 
				&& getAdditionalInsuredVehiclesPK().equals(((AdditionalInsuredVehicles) obj).getAdditionalInsuredVehiclesPK());
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		AdditionalInsuredVehicles clone = (AdditionalInsuredVehicles) super.clone();
		clone.setAdditionalInsuredVehiclesPK(null);
		clone.setOriginatingPK(this.additionalInsuredVehiclesPK);
		clone.setAdditionalInsured(this.additionalInsured);
		clone.setAutoSubPolicy(null);
		return clone;
	}
	
	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}
	
}
