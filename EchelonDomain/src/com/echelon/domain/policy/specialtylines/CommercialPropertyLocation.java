package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;

@Entity
@Table(name = "CommercialPropertyLocation")
public class CommercialPropertyLocation extends AbstractAuditable {
	private static final long serialVersionUID = -2726623408696380894L;

	private static List<String> auditExcludeList = Arrays.asList(
		"commercialPropertyLocationPK", "commercialPropertySubPolicy",
		"policyLocation");
	private static List<String> copyExcludeList = Arrays.asList(
		"commercialPropertyLocationPK", "commercialPropertySubPolicy",
		"policyLocation");

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "CommercialPropertyLocationPK", updatable = false, nullable = false)
	private Long commercialPropertyLocationPK;

	@ManyToOne
	@JoinColumn(name = "SubPolicyPK", nullable = true)
	private CommercialPropertySubPolicy<?> commercialPropertySubPolicy;

	@ManyToOne
	@JoinColumn(name = "PolicyRiskPK")
	private PolicyLocation policyLocation;

	public Long getCommercialPropertyLocationPK() {
		return commercialPropertyLocationPK;
	}

	public void setCommercialPropertyLocationPK(
		Long commercialPropertyLocationPK) {
		this.commercialPropertyLocationPK = commercialPropertyLocationPK;
	}

	public CommercialPropertySubPolicy<?> getCommercialPropertySubPolicy() {
		return commercialPropertySubPolicy;
	}

	public void setCommercialPropertySubPolicy(
		CommercialPropertySubPolicy<?> commercialPropertySubPolicy) {
		this.commercialPropertySubPolicy = commercialPropertySubPolicy;
	}

	public PolicyLocation getPolicyLocation() {
		return policyLocation;
	}

	public void setPolicyLocation(PolicyLocation policyLocation) {
		this.policyLocation = policyLocation;
	}

	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}

	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		CommercialPropertyLocation clone = (CommercialPropertyLocation) super.clone();
		clone.setCommercialPropertyLocationPK(null);
		clone.setCommercialPropertySubPolicy(this.commercialPropertySubPolicy);
		clone.setPolicyLocation(this.policyLocation);
		return clone;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof CommercialPropertyLocation))
			return false;

		Long pk = getCommercialPropertyLocationPK();
		return pk != null && pk.equals(((CommercialPropertyLocation) obj)
			.getCommercialPropertyLocationPK());
	}
}
