package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.baseclasses.BaseBusinessEntity;
import com.ds.ins.domain.policy.Risk;

@Entity
@Table(name = "CargoDetails")
public class CargoDetails extends AbstractAuditable {
	private static final long serialVersionUID = -942883353014753588L;
	private static List<String> auditExcludeList = Arrays.asList(
			"cargoDetailsPK", "subPolicy", "originatingPK"
	);
	
	private static List<String> copyExcludeList = Arrays.asList(
			"cargoDetailsPK", "subPolicy", "originatingPK"
	);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CargoDetailsPK", updatable = false, nullable = false)
	private 					Long 					cargoDetailsPK;
	
	@ManyToOne
	@JoinColumn(name="SubPolicyPK", nullable=true)
	private 				CargoSubPolicy<Risk> 		subPolicy;
	
	private					String						cargoDescription;
	private 				Double 						cargoPartialPremiums;
	private					Double						average;
	private					Double						uWManual;
	private 				Double						cargoValue;
	private					Double 						cargoExposure;
	private 				Long						originatingPK;
	
	public Long getCargoDetailsPK() {
		return cargoDetailsPK;
	}
	public void setCargoDetailsPK(Long cargoDetailsPK) {
		this.cargoDetailsPK = cargoDetailsPK;
	}
	public CargoSubPolicy<Risk> getSubPolicy() {
		return subPolicy;
	}
	public void setSubPolicy(CargoSubPolicy<Risk> subPolicy) {
		this.subPolicy = subPolicy;
	}
	public String getCargoDescription() {
		return cargoDescription;
	}
	public void setCargoDescription(String cargoDescription) {
		this.cargoDescription = cargoDescription;
	}
	public Double getCargoPartialPremiums() {
		return cargoPartialPremiums;
	}
	public void setCargoPartialPremiums(Double cargoPartialPremiums) {
		this.cargoPartialPremiums = cargoPartialPremiums;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Double getuWManual() {
		return uWManual;
	}
	public void setuWManual(Double uWManual) {
		this.uWManual = uWManual;
	}
	public Double getCargoValue() {
		return cargoValue;
	}
	public void setCargoValue(Double cargoValue) {
		this.cargoValue = cargoValue;
	}
	public Double getCargoExposure() {
		return cargoExposure;
	}
	public void setCargoExposure(Double cargoExposure) {
		this.cargoExposure = cargoExposure;
	}
	public Long getOriginatingPK() {
		return originatingPK;
	}
	public void setOriginatingPK(Long originatingPK) {
		this.originatingPK = originatingPK;
	}
	
	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof CargoDetails)) return false;
		return cargoDetailsPK != null && cargoDetailsPK.equals(((CargoDetails) obj).getCargoDetailsPK());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		CargoDetails clone = (CargoDetails) super.clone();
		clone.setCargoDetailsPK(null);
		clone.setOriginatingPK(this.cargoDetailsPK);
		return clone;
	}
	
	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}
	
}
