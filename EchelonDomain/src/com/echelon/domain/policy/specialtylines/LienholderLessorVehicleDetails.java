package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.baseclasses.BaseBusinessEntity;
import com.ds.ins.domain.policy.LienholderLessor;

@Entity
@Table(name = "LienholderLessorVehicleDetails")
public class LienholderLessorVehicleDetails extends AbstractAuditable {
	private static final long serialVersionUID = -6223229712262965093L;
	private static List<String> auditExcludeList = Arrays.asList(
			"lienholderLessorVehicleDetailsPK", "lienholderLessor", "autoSubPolicy", "originatingPK"
	);
	private static List<String> copyExcludeList = Arrays.asList(
			"lienholderLessorVehicleDetailsPK", "lienholderLessor", "autoSubPolicy", "originatingPK"
	);

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "LienholderLessorVehicleDetailsPK", updatable = false, nullable = false)
	private 				Long 							lienholderLessorVehicleDetailsPK;
		
	@ManyToOne
	@JoinColumn(name = "LienholderLessorPK")
	private 				LienholderLessor 				lienholderLessor;
	
	@ManyToOne
	@JoinColumn(name = "SubPolicyPK", nullable = true)
	private					SpecialityAutoSubPolicy<?>  	autoSubPolicy;
	
	private 				String							vehicleNum;
	private 				String							serialNum;
	private 				Integer							vehicleYear;
	private 				Long							originatingPK;
	private 				Double							lessorDeductible;
	private					Integer							vehicleDetailsSequence;
	
	public Long getLienholderLessorVehicleDetailsPK() {
		return lienholderLessorVehicleDetailsPK;
	}
	public void setLienholderLessorVehicleDetailsPK(Long lienholderLessorVehicleDetailsPK) {
		this.lienholderLessorVehicleDetailsPK = lienholderLessorVehicleDetailsPK;
	}
	public String getVehicleNum() {
		return vehicleNum;
	}
	public void setVehicleNum(String vehicleNum) {
		this.vehicleNum = vehicleNum;
	}
	public String getSerialNum() {
		return serialNum;
	}
	public void setSerialNum(String serialNum) {
		this.serialNum = serialNum;
	}
	public LienholderLessor getLienholderLessor() {
		return lienholderLessor;
	}
	public void setLienholderLessor(LienholderLessor lienholderLessor) {
		this.lienholderLessor = lienholderLessor;
	}
	public Integer getVehicleYear() {
		return vehicleYear;
	}
	public void setVehicleYear(Integer vehicleYear) {
		this.vehicleYear = vehicleYear;
	}
	public Long getOriginatingPK() {
		return originatingPK;
	}
	public void setOriginatingPK(Long originatingPK) {
		this.originatingPK = originatingPK;
	}
	
	public SpecialityAutoSubPolicy<?> getAutoSubPolicy() {
		return autoSubPolicy;
	}
	public void setAutoSubPolicy(SpecialityAutoSubPolicy<?> subPolicy) {
		this.autoSubPolicy = subPolicy;
	}
	
	public Integer getVehicleDetailsSequence() {
		return vehicleDetailsSequence;
	}
	public void setVehicleDetailsSequence(Integer vehicleDetailsSequence) {
		this.vehicleDetailsSequence = vehicleDetailsSequence;
	}
	@Override
	public int hashCode() {
		return 31;
	}
	public Double getLessorDeductible() {
		return lessorDeductible;
	}
	public void setLessorDeductible(Double lessorDeductible) {
		this.lessorDeductible = lessorDeductible;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof LienholderLessorVehicleDetails)) return false;
		return getLienholderLessorVehicleDetailsPK() != null 
				&& getLienholderLessorVehicleDetailsPK().equals(((LienholderLessorVehicleDetails) obj).getLienholderLessorVehicleDetailsPK());
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		LienholderLessorVehicleDetails clone = (LienholderLessorVehicleDetails) super.clone();
		clone.setLienholderLessorVehicleDetailsPK(null);
		clone.setOriginatingPK(this.lienholderLessorVehicleDetailsPK);
		clone.setLienholderLessor(this.lienholderLessor);
		clone.setAutoSubPolicy(null);
		return clone;
	}
	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}
}
