package com.echelon.domain.policy.specialtylines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.wheels.Driver;

@Entity
@Table(name = "SpecialtyVehicleRisk")
@PrimaryKeyJoinColumn(name = "PolicyRiskPK")
public class SpecialtyVehicleRisk extends Risk {
	private static final long serialVersionUID = -5730703186610789094L;
	
	private static List<String> copyExcludeList = Arrays.asList(
			"unitRate", "vehicleSequence", "vehicleDrivers", "svrAdjustments", "vehicleDriverClaims"
	);
	
	private Boolean 							isIndividual;
	private Integer 							numberOfUnits;
	private String 								vehicleType;
	private String 								trailerType;
	private String 								vehicleDescription;
	private String 								vehicleUse;
	private String 								vehicleClass;
	private String 								drivingRecord;
	private Double 								unitLPN;
	private String 								unitExposure;
	private String 								unitSerialNumberorVin;
	private String 								unitMake;
	private Integer 							unitYear;
	private String 								unitModel;
	private String 								impliedRateGroup;
	private String 								selectedRateGroup;
	private Double								unitRate;
	private Integer								avgKmsPerYear;
	private Integer								vehicleSequence;
	private String 								typeOfUnit;
	private Double 								actualCashValue;
	  
	//added by Marwan Sep 8, 2022
	private Double claimsSurcharge;
	private Double claimsSurchargeOverride;
	private Double minorConvictionSurcharge;
	private Double minorConvictionSurchargeOverride;
	private Double majorConvictionSurcharge;
	private Double majorConvictionSurchargeOverride;
	private Double criminalConvictionSurcharge;
	private Double criminalConvictionSurchargeOverride;
	private Double principleYoungDriverSurcharge;
	private Double principleYoungDriverSurchargeOverride;
	private Double secondaryYoungDriverSurcharge;
	private Double secondaryYoungDriverSurchargeOverride;
	private Double totalSurcharge;
	private Double totalSurchargeOverride;
	//end of additions
	
	@OneToMany(mappedBy = "specialtyVehicleRisk", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<VehicleDriver> vehicleDrivers;

	@OneToMany(mappedBy = "specialtyVehicleRisk", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<SpecialtyVehicleRiskAdjustment> svrAdjustments;
	  
	@OneToMany(mappedBy = "specialtyVehicleRisk", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<VehicleDriverClaim> vehicleDriverClaims;
		
	@Column(name = "IRP")
	private Integer								irp;

	//added by Marwan Aug. 22, 2022
  	private Boolean 							isNonStandard;
  	//end of additions
  	
  	//added by Marwan Sep. 30, 2022
  	private Long 								towingVehiclePK;
  	//end of additions
	
	public SpecialtyVehicleRisk() {
	    vehicleDrivers = new HashSet<VehicleDriver>();
		svrAdjustments = new HashSet<SpecialtyVehicleRiskAdjustment>();
	    vehicleDriverClaims = new HashSet<VehicleDriverClaim>();
	}	
	
	public void addVehicleDriver(VehicleDriver vd, Driver driver) {
		vd.setSpecialtyVehicleRisk(this);
		if (driver != null) {
			vd.setDriver(driver);
		}
		vehicleDrivers.add(vd);	  
	}
	  
	public void addVehicleDriver(VehicleDriver vd) {
		addVehicleDriver(vd, null);
	}
	  
	public void removeVehicleDriver(VehicleDriver vd) {
		if (vd.isActive() && (vd.isNoChange() || vd.isModified())) {
			vd.setSystemStatus(IAuditable.INACTIVE);
			vd.setBusinessStatus(IAuditable.DELETED);
		} else {
			vehicleDrivers.remove(vd);
			vd.setSpecialtyVehicleRisk(null);
			vd.setDriver(null);
		}
	}
	  
	public void addVehicleDriverClaim(VehicleDriverClaim vdc, Driver driver) {
		vdc.setSpecialtyVehicleRisk(this);
		if (driver != null) {
			vdc.setDriver(driver);
		}
		vehicleDriverClaims.add(vdc);
	}
	  
	public void addVehicleDriverClaim(VehicleDriverClaim vdc) {
		addVehicleDriverClaim(vdc, null);
	}
	  
	public void removeVehicleDriverClaim(VehicleDriverClaim vdc) {
		if (vdc.isActive() && (vdc.isNoChange() || vdc.isModified())) {
			vdc.setSystemStatus(IAuditable.INACTIVE);
			vdc.setBusinessStatus(IAuditable.DELETED);
		} else {
			vehicleDriverClaims.remove(vdc);
			vdc.setSpecialtyVehicleRisk(null);
			vdc.setDriver(null);
		}
	}
	
	public Boolean getIsIndividual() {
		return isIndividual;
	}
	public void setIsIndividual(Boolean isIndividual) {
		this.isIndividual = isIndividual;
	}
	public Integer getNumberOfUnits() {
		return numberOfUnits;
	}
	public void setNumberOfUnits(Integer numberOfUnits) {
		this.numberOfUnits = numberOfUnits;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getTrailerType() {
		return trailerType;
	}
	public void setTrailerType(String trailerType) {
		this.trailerType = trailerType;
	}
	public String getVehicleDescription() {
		return vehicleDescription;
	}
	public void setVehicleDescription(String vehicleDescription) {
		this.vehicleDescription = vehicleDescription;
	}
	public String getVehicleUse() {
		return vehicleUse;
	}
	public void setVehicleUse(String vehicleUse) {
		this.vehicleUse = vehicleUse;
	}
	public String getVehicleClass() {
		return vehicleClass;
	}
	public void setVehicleClass(String vehicleClass) {
		this.vehicleClass = vehicleClass;
	}
	public String getDrivingRecord() {
		return drivingRecord;
	}
	public void setDrivingRecord(String drivingRecord) {
		this.drivingRecord = drivingRecord;
	}
	public Double getUnitLPN() {
		return unitLPN;
	}
	public void setUnitLPN(Double unitLPN) {
		this.unitLPN = unitLPN;
	}
	public String getUnitExposure() {
		return unitExposure;
	}
	public void setUnitExposure(String unitExposure) {
		this.unitExposure = unitExposure;
	}
	public String getUnitSerialNumberorVin() {
		return unitSerialNumberorVin;
	}
	public void setUnitSerialNumberorVin(String unitSerialNumberorVin) {
		this.unitSerialNumberorVin = unitSerialNumberorVin;
	}
	public String getUnitMake() {
		return unitMake;
	}
	public void setUnitMake(String unitMake) {
		this.unitMake = unitMake;
	}
	public Integer getUnitYear() {
		return unitYear;
	}
	public void setUnitYear(Integer unitYear) {
		this.unitYear = unitYear;
	}
	public String getUnitModel() {
		return unitModel;
	}
	public void setUnitModel(String unitModel) {
		this.unitModel = unitModel;
	}
	public String getImpliedRateGroup() {
		return impliedRateGroup;
	}
	public void setImpliedRateGroup(String impliedRateGroup) {
		this.impliedRateGroup = impliedRateGroup;
	}
	public String getSelectedRateGroup() {
		return selectedRateGroup;
	}
	public void setSelectedRateGroup(String selectedRateGroup) {
		this.selectedRateGroup = selectedRateGroup;
	}
	public Double getUnitRate() {
		return unitRate;
	}
	public void setUnitRate(Double unitRate) {
		this.unitRate = unitRate;
	}
	public Integer getAvgKmsPerYear() {
		return avgKmsPerYear;
	}
	public void setAvgKmsPerYear(Integer avgKmsPerYear) {
		this.avgKmsPerYear = avgKmsPerYear;
	}
	public Integer getVehicleSequence() {
		return vehicleSequence;
	}
	public void setVehicleSequence(Integer vehicleSequence) {
		this.vehicleSequence = vehicleSequence;
	}
	public String getTypeOfUnit() {
		return typeOfUnit;
	}
	public void setTypeOfUnit(String typeOfUnit) {
		this.typeOfUnit = typeOfUnit;
	}
	public Double getActualCashValue() {
		return actualCashValue;
	}
	public void setActualCashValue(Double actualCashValue) {
		this.actualCashValue = actualCashValue;
	}
	public Integer getIrp() {
		return irp;
	}
	public void setIrp(Integer irp) {
		this.irp = irp;
	}
	public Boolean getIsNonStandard() {
		return isNonStandard;
	}
  	public void setIsNonStandard(Boolean isNonStandard) {
		this.isNonStandard = isNonStandard;
  	}
	
	public Set<VehicleDriver> getVehicleDrivers() {
		return vehicleDrivers;
	}

	public void setVehicleDrivers(Set<VehicleDriver> vehicleDrivers) {
		this.vehicleDrivers = vehicleDrivers;
	}

	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustments() {
		return svrAdjustments;
	}
	public void setSvrAdjustments(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		this.svrAdjustments = svrAdjustments;
	}

	public Set<VehicleDriverClaim> getVehicleDriverClaims() {
		return vehicleDriverClaims;
	}

	public void setVehicleDriverClaims(Set<VehicleDriverClaim> vehicleDriverClaims) {
		this.vehicleDriverClaims = vehicleDriverClaims;
	}

  public Double getClaimsSurcharge() {
	  return claimsSurcharge;
  }

  public void setClaimsSurcharge(Double claimsSurcharge) {
	  this.claimsSurcharge = claimsSurcharge;
  }

  public Double getClaimsSurchargeOverride() {
	  return claimsSurchargeOverride;
  }

  public void setClaimsSurchargeOverride(Double claimsSurchargeOverride) {
	  this.claimsSurchargeOverride = claimsSurchargeOverride;
  }

  public Double getMinorConvictionSurcharge() {
	  return minorConvictionSurcharge;
  }

  public void setMinorConvictionSurcharge(Double minorConvictionSurcharge) {
	  this.minorConvictionSurcharge = minorConvictionSurcharge;	
  }

  public Double getMinorConvictionSurchargeOverride() {
	  return minorConvictionSurchargeOverride;
  }

  public void setMinorConvictionSurchargeOverride(Double minorConvictionSurchargeOverride) {
	  this.minorConvictionSurchargeOverride = minorConvictionSurchargeOverride;
  }

  public Double getMajorConvictionSurcharge() {
	  return majorConvictionSurcharge;
  }

  public void setMajorConvictionSurcharge(Double majorConvictionSurcharge) {
	  this.majorConvictionSurcharge = majorConvictionSurcharge;
  }

  public Double getMajorConvictionSurchargeOverride() {
	  return majorConvictionSurchargeOverride;
  }

  public void setMajorConvictionSurchargeOverride(Double majorConvictionSurchargeOverride) {
	  this.majorConvictionSurchargeOverride = majorConvictionSurchargeOverride;
  }

  public Double getCriminalConvictionSurcharge() {
	  return criminalConvictionSurcharge;
  }

  public void setCriminalConvictionSurcharge(Double criminalConvictionSurcharge) {
	  this.criminalConvictionSurcharge = criminalConvictionSurcharge;
  }

  public Double getCriminalConvictionSurchargeOverride() {
	  return criminalConvictionSurchargeOverride;
  }

  public void setCriminalConvictionSurchargeOverride(Double criminalConvictionSurchargeOverride) {
	  this.criminalConvictionSurchargeOverride = criminalConvictionSurchargeOverride;
  }

  public Double getPrincipleYoungDriverSurcharge() {
	  return principleYoungDriverSurcharge;
  }

  public void setPrincipleYoungDriverSurcharge(Double principleYoungDriverSurcharge) {
	  this.principleYoungDriverSurcharge = principleYoungDriverSurcharge;
  }

  public Double getPrincipleYoungDriverSurchargeOverride() {
	  return principleYoungDriverSurchargeOverride;
  }

  public void setPrincipleYoungDriverSurchargeOverride(Double principleYoungDriverSurchargeOverride) {
	  this.principleYoungDriverSurchargeOverride = principleYoungDriverSurchargeOverride;
  }

  public Double getSecondaryYoungDriverSurcharge() {
	  return secondaryYoungDriverSurcharge;
  }

  public void setSecondaryYoungDriverSurcharge(Double secondaryYoungDriverSurcharge) {
	  this.secondaryYoungDriverSurcharge = secondaryYoungDriverSurcharge;
  }

  public Double getSecondaryYoungDriverSurchargeOverride() {
	  return secondaryYoungDriverSurchargeOverride;
  }

  public void setSecondaryYoungDriverSurchargeOverride(Double secondaryYoungDriverSurchargeOverride) {
	  this.secondaryYoungDriverSurchargeOverride = secondaryYoungDriverSurchargeOverride;
  }

  public Double getTotalSurcharge() {
	  return totalSurcharge;
  }

  public void setTotalSurcharge(Double totalSurcharge) {
	  this.totalSurcharge = totalSurcharge;
  }

  public Double getTotalSurchargeOverride() {
	  return totalSurchargeOverride;
  }

  public void setTotalSurchargeOverride(Double totalSurchargeOverride) {
	  this.totalSurchargeOverride = totalSurchargeOverride;
  }

  public Long getTowingVehiclePK() {
	  return towingVehiclePK;
  }

  public void setTowingVehiclePK(Long towingVehiclePK) {
	  this.towingVehiclePK = towingVehiclePK;
  }

@Override
  public int hashCode() {
	  return 31;
  }

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof SpecialtyVehicleRisk)) return false;
		return getPolicyRiskPK() != null && getPolicyRiskPK().equals(((SpecialtyVehicleRisk) obj).getPolicyRiskPK());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		SpecialtyVehicleRisk svrClone = (SpecialtyVehicleRisk) super.clone();
		svrClone.svrAdjustments = new HashSet<SpecialtyVehicleRiskAdjustment>();
	    svrClone.setVehicleDrivers(new HashSet<VehicleDriver>());
	    for (VehicleDriver origVD : this.vehicleDrivers) {
	    	VehicleDriver cloneVD = (VehicleDriver)origVD.clone();
	    	svrClone.addVehicleDriver(cloneVD);
	    	if (origVD.getDriver() != null) {
	    		cloneVD.setDriver(svrClone.findDriverByOriginatingPK(origVD.getDriver().getPersonPk()));
	    	}
	    }
	    svrClone.vehicleDriverClaims = new HashSet<VehicleDriverClaim>();
	    for (VehicleDriverClaim origVDC : this.vehicleDriverClaims) {
	    	VehicleDriverClaim cloneVDC = (VehicleDriverClaim)origVDC.clone();
	    	svrClone.addVehicleDriverClaim(cloneVDC);
	    	if (origVDC.getDriver() != null) {
	    		cloneVDC.setDriver(svrClone.findDriverByOriginatingPK(origVDC.getDriver().getPersonPk()));
	    	}
	    }
		return svrClone;
	}


	@Override
	public List<String> getAuditExcludeList() {
		List<String> rv = (new ArrayList<String>());
		rv.addAll(super.getAuditExcludeList());
		rv.addAll(Arrays.asList("unitRate", "vehicleSequence"));
		return rv;
	}
	  
	public Driver findDriverByOriginatingPK(Long driverPK) {
		Driver rv = null;
		if (driverPK != null && this.getPolicyTransaction() != null) {
			for (Driver d : this.getPolicyTransaction().getDrivers()) {
				if (driverPK.equals(d.getOriginatingPK())) {
					rv = d;
					break;
				}
			}
		}
		return rv;
	}
	  
	public VehicleDriver findVehicleDriverByPk(Long vdPK) {
		VehicleDriver rv = null;
		if (vdPK != null && this.getVehicleDrivers() != null) {
			rv = this.getVehicleDrivers().stream().filter(vd -> vd.getVehicleDriverPK().equals(vdPK)).findFirst().orElse(null);
		}
		return rv;
	}
	    
	public VehicleDriver findVehicleDriverByOriginatingPk(Long vdOrigPK) {
		VehicleDriver rv = null;
		if (vdOrigPK != null && this.getVehicleDrivers() != null) {
			rv = this.getVehicleDrivers().stream().filter(vd -> vd.getOriginatingPk().equals(vdOrigPK)).findFirst().orElse(null);
		}
		return rv;
	}
	  
	public VehicleDriverClaim findVehicleDriverClaimByPk(Long vdcPk) {
		VehicleDriverClaim rv = null;
		if (vdcPk != null && this.vehicleDriverClaims != null) {
			rv = this.getVehicleDriverClaims().stream().filter(vdc -> vdc.getVehicleDriverClaimPK().equals(vdcPk)).findFirst().orElse(null);
		}
		return rv;
	}
	  
	public VehicleDriverClaim findVehicleDriverClaimByOriginatingPk(Long vdcOrigPk) {
		VehicleDriverClaim rv = null;
		if (vdcOrigPk != null && this.vehicleDriverClaims != null) {
			rv = this.getVehicleDriverClaims().stream().filter(vdc -> vdc.getOriginatingPk().equals(vdcOrigPk)).findFirst().orElse(null);
		}	  
		return rv;
	}

	public VehicleDriver undeleteVehicleDriver(VehicleDriver vd) {
		if (vd != null) {
			vd = this.findVehicleDriverByPk(vd.getVehicleDriverPK());
			if (vd != null && vd.isInactive() && vd.isDeleted()) {
				vd.setSystemStatus(IAuditable.ACTIVE);
				vd.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		return vd;
	}
	  
	public VehicleDriverClaim undeleteVehicleDriverClaim(VehicleDriverClaim vdc) {
		if (vdc != null) {
			vdc = this.findVehicleDriverClaimByPk(vdc.getVehicleDriverClaimPK());
			if (vdc != null && vdc.isInactive() && vdc.isDeleted()) {
				vdc.setSystemStatus(IAuditable.ACTIVE);
				vdc.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		return vdc;
	}
	  
	public void addSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		if (this.getSvrAdjustments().isEmpty()) {
			svrAdjustment.setAdjustmentNum((this.getPolicyRiskPK().intValue() * 100) + 1);
		} else {
			Integer currentMaxNum = this.getSvrAdjustments().stream().max(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum)).get().getAdjustmentNum();
			svrAdjustment.setAdjustmentNum(currentMaxNum + 1);
		}		

		svrAdjustment.setSpecialtyVehicleRisk(this);
		svrAdjustments.add(svrAdjustment);
	}
	
	public void removeSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		if (svrAdjustment.isActive() && (svrAdjustment.isNoChange() || svrAdjustment.isModified())) {
			svrAdjustment.setSystemStatus(IAuditable.INACTIVE);
			svrAdjustment.setBusinessStatus(IAuditable.DELETED);
		} else if (svrAdjustment.isPending()) {
			svrAdjustments.remove(svrAdjustment);
			svrAdjustment.setSpecialtyVehicleRisk(null);
		}
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		List<String> rv = (new ArrayList<String>());
		rv.addAll(super.getCopyExcludeList());
		rv.addAll(copyExcludeList);
		return rv;
	}
}
