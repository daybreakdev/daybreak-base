package com.echelon.domain.policy.specialtylines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VehicleRateGroup")
public class VehicleRateGroup implements Serializable {
	private static final long serialVersionUID = 472829666825062236L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VehicleRateGroupPK", updatable = false, nullable = false)
	private 		Long 		vehicleRateGroupPK;
	
	private			Integer 	vehicleAge;
	private			Double 		lowLimit;
	private			Double		uppLimit;
	private			Integer 	impliedRateGroup;
	
	public Long getVehicleRateGroupPK() {
		return vehicleRateGroupPK;
	}
	public void setVehicleRateGroupPK(Long vehicleRateGroupPK) {
		this.vehicleRateGroupPK = vehicleRateGroupPK;
	}
	public Integer getVehicleAge() {
		return vehicleAge;
	}
	public void setVehicleAge(Integer vehicleAge) {
		this.vehicleAge = vehicleAge;
	}
	public Double getLowLimit() {
		return lowLimit;
	}
	public void setLowLimit(Double lowLimit) {
		this.lowLimit = lowLimit;
	}
	public Double getUppLimit() {
		return uppLimit;
	}
	public void setUppLimit(Double uppLimit) {
		this.uppLimit = uppLimit;
	}
	public Integer getImpliedRateGroup() {
		return impliedRateGroup;
	}
	public void setImpliedRateGroup(Integer impliedRateGroup) {
		this.impliedRateGroup = impliedRateGroup;
	}
	
}
