package com.echelon.domain.policy.specialtylines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.interfaces.specialtylines.ISpecialtyHasAdjustment;

@Entity
@Table(name = "CargoSubPolicy")
@PrimaryKeyJoinColumn(name = "SubPolicyPK")
public class CargoSubPolicy<RISKTYPE extends Risk> extends SubPolicy<RISKTYPE> implements ISpecialtyHasAdjustment {
	private static final long serialVersionUID = 5815728662937463724L;
	private static List<String> copyExcludeList = Arrays.asList("svrAdjustments");

	@OneToMany(mappedBy = "subPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private 	Set<CargoDetails> 	cargoDetails;
	
	private 	Integer				cargoDeductible;
	private 	Integer				cargoLimit;
	private 	Double				totalCargoPartialPremium;
	
	//added by Marwan Oct. 6, 2022
	@OneToMany(mappedBy = "cargoSubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private 	Set<SpecialtyVehicleRiskAdjustment> 	svrAdjustments;
	//end of additions
	
	public CargoSubPolicy() {
		this.cargoDetails = new HashSet<CargoDetails>();
		this.svrAdjustments = new HashSet<SpecialtyVehicleRiskAdjustment>();
	}
	
	@SuppressWarnings("unchecked")
	public void addCargoDetails(CargoDetails cargoDetail) {
		cargoDetail.setSubPolicy((CargoSubPolicy<Risk>) this);
		this.cargoDetails.add(cargoDetail);
	}
	
	public void removedCargoDetails(CargoDetails cargoDetail) {
		if (cargoDetail.isActive() && (cargoDetail.isNoChange() || cargoDetail.isModified())) {
			cargoDetail.setSystemStatus(INACTIVE);
			cargoDetail.setBusinessStatus(DELETED);
		} else if (cargoDetail.isPending()) {
			this.cargoDetails.remove(cargoDetail);
			cargoDetail.setSubPolicy(null);
		}
	}
	
	public Integer getCargoDeductible() {
		return cargoDeductible;
	}
	public void setCargoDeductible(Integer cargoDeductible) {
		this.cargoDeductible = cargoDeductible;
	}
	public Integer getCargoLimit() {
		return cargoLimit;
	}
	public void setCargoLimit(Integer cargoLimit) {
		this.cargoLimit = cargoLimit;
	}
	public Set<CargoDetails> getCargoDetails() {
		return cargoDetails;
	}
	public void setCargoDetails(Set<CargoDetails> cargoDetails) {
		this.cargoDetails = cargoDetails;
	}

	public Double getTotalCargoPartialPremium() {
		return totalCargoPartialPremium;
	}

	public void setTotalCargoPartialPremium(Double totalCargoPartialPremium) {
		this.totalCargoPartialPremium = totalCargoPartialPremium;
	}

	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustments() {
		return svrAdjustments;
	}

	public void setSvrAdjustments(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		this.svrAdjustments = svrAdjustments;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof CargoSubPolicy)) return false;
		return getSubPolicyPK() != null && getSubPolicyPK().equals(((CargoSubPolicy<?>) obj).getSubPolicyPK());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object clone() throws CloneNotSupportedException {
		CargoSubPolicy<RISKTYPE> clone = (CargoSubPolicy<RISKTYPE>) super.clone();
		clone.setSvrAdjustments(new HashSet<SpecialtyVehicleRiskAdjustment>());
		clone.setCargoDetails(new HashSet<CargoDetails>());
		for (CargoDetails cg : this.cargoDetails) {
			clone.addCargoDetails((CargoDetails) cg.clone());
		}
		return clone;
	}
	
	public CargoDetails findCargoDetailsByPK(Long cgPK) {
		CargoDetails rv = null;
		if (cgPK != null) {
			rv = this.getCargoDetails().stream().filter(cgd -> cgd.getCargoDetailsPK().equals(cgPK)).findFirst().orElse(null);
		}
		return rv;
	}
	
	@SuppressWarnings("unchecked")
	public void addSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		svrAdjustment.setCargoSubPolicy((CargoSubPolicy<Risk>) this);
		svrAdjustments.add(svrAdjustment);
	}
	
	public void removeSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		if (svrAdjustment.isActive() && (svrAdjustment.isNoChange() || svrAdjustment.isModified())) {
			svrAdjustment.setSystemStatus(IAuditable.INACTIVE);
			svrAdjustment.setBusinessStatus(IAuditable.DELETED);
		} else if (svrAdjustment.isPending()) {
			svrAdjustments.remove(svrAdjustment);
			svrAdjustment.setCargoSubPolicy(null);
		}
	}
	
	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustmentsByGroupID(String groupID) {
		Set<SpecialtyVehicleRiskAdjustment> results = new HashSet<SpecialtyVehicleRiskAdjustment>();
		if (groupID != null) {
			results = this.svrAdjustments.stream().filter(o -> groupID.equalsIgnoreCase(o.getGroupID())).collect(Collectors.toSet());
		}
		return results;
	}
	
	public SpecialtyVehicleRiskAdjustment getSvrAdjustmentsByAdjNum(String groupID, Integer adjNum) {
		SpecialtyVehicleRiskAdjustment result = null;
		
		if (adjNum != null) {
			Set<SpecialtyVehicleRiskAdjustment> ls = getSvrAdjustmentsByGroupID(groupID);
			result = ls.stream().filter(o -> o.getAdjustmentNum().equals(adjNum)).findFirst().orElse(null);
		}
		
		return result;
	}

	@Override
	public List<String> getCopyExcludeList() {
		List<String> rv = (new ArrayList<String>());
		rv.addAll(super.getCopyExcludeList());
		rv.addAll(copyExcludeList);
		return rv;
	}
	
}
