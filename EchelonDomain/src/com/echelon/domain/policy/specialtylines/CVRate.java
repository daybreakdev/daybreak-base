package com.echelon.domain.policy.specialtylines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CVRate")
public class CVRate implements Serializable {
	private static final long serialVersionUID = 4257606377331866460L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CVRatePK", updatable = false, nullable = false)
	private Long 		cvRatePK;
	
	private String 		description;
	private Integer 	minCV;
	private Integer		maxCV;
	private Integer		avgCV;
	
	@Column(name = "CVRate")
	private Double 		cvRate;
	
	private Integer		price;

	public Long getCvRatePK() {
		return cvRatePK;
	}

	public void setCvRatePK(Long cvRatePK) {
		this.cvRatePK = cvRatePK;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getMinCV() {
		return minCV;
	}

	public void setMinCV(Integer minCV) {
		this.minCV = minCV;
	}

	public Integer getMaxCV() {
		return maxCV;
	}

	public void setMaxCV(Integer maxCV) {
		this.maxCV = maxCV;
	}

	public Integer getAvgCV() {
		return avgCV;
	}

	public void setAvgCV(Integer avgCV) {
		this.avgCV = avgCV;
	}

	public Double getCvRate() {
		return cvRate;
	}

	public void setCvRate(Double cvRate) {
		this.cvRate = cvRate;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}
	
}
