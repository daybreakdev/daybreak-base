package com.echelon.domain.policy.specialtylines;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VehicleRateGroupLHT")
public class VehicleRateGroupLHT implements Serializable {
	private static final long serialVersionUID = -2379850141061000907L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "VehicleRateGroupLHTPK", updatable = false, nullable = false)
	private Long 	vehicleRateGroupLHTPK;
	
	private Double 	lowLimitACV;
	private Double 	uppLimitACV;
	private Integer impliedRateGroup;
	
	public Long getVehicleRateGroupLHTPK() {
		return vehicleRateGroupLHTPK;
	}
	public void setVehicleRateGroupLHTPK(Long vehicleRateGroupLHTPK) {
		this.vehicleRateGroupLHTPK = vehicleRateGroupLHTPK;
	}
	public Double getLowLimitACV() {
		return lowLimitACV;
	}
	public void setLowLimitACV(Double lowLimitACV) {
		this.lowLimitACV = lowLimitACV;
	}
	public Double getUppLimitACV() {
		return uppLimitACV;
	}
	public void setUppLimitACV(Double uppLimitACV) {
		this.uppLimitACV = uppLimitACV;
	}
	public Integer getImpliedRateGroup() {
		return impliedRateGroup;
	}
	public void setImpliedRateGroup(Integer impliedRateGroup) {
		this.impliedRateGroup = impliedRateGroup;
	}
}
