package com.echelon.domain.policy.specialtylines;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.policy.PolicyTransactionData;

@Entity
@Table(name = "SpecialityAutoExpRating")
@PrimaryKeyJoinColumn(name = "PolicyTransactionDataPK")
public class SpecialityAutoExpRating extends PolicyTransactionData {
	private static final long serialVersionUID = -6278504790202661695L;
	
	private Double expectedMileage;
	private Integer numberOfPowerUnits;
	private Double averageKMS;
	private Double claimsFrequency;
	private Integer yearsInBusiness;
	private Double dangerousMaterials;
	private Double preventionRating;
	private Integer commercialCreditScore;
	private Double zone1Exp;
	private Double zone2Exp;
	private Double zone3Exp;
	private Double zone4Exp;
	private Double zone5Exp;
	private Double zone6Exp;
	private Double zone7Exp;
	private Double zone8Exp;
	private Double zone9Exp;
	private Double modificationFactor;
	private Double offBalanceFactor;
	
	public Double getExpectedMileage() {
		return expectedMileage;
	}
	
	public void setExpectedMileage(Double expectedMileage) {
		this.expectedMileage = expectedMileage;
	}
	
	public Integer getNumberOfPowerUnits() {
		return numberOfPowerUnits;
	}
	
	public void setNumberOfPowerUnits(Integer numberOfPowerUnits) {
		this.numberOfPowerUnits = numberOfPowerUnits;
	}
	
	public Double getAverageKMS() {
		return averageKMS;
	}
	
	public void setAverageKMS(Double averageKMS) {
		this.averageKMS = averageKMS;
	}
	
	public Double getClaimsFrequency() {
		return claimsFrequency;
	}
	
	public void setClaimsFrequency(Double claimsFrequency) {
		this.claimsFrequency = claimsFrequency;
	}
	
	public Integer getYearsInBusiness() {
		return yearsInBusiness;
	}
	
	public void setYearsInBusiness(Integer yearsInBusiness) {
		this.yearsInBusiness = yearsInBusiness;
	}
	
	public Double getDangerousMaterials() {
		return dangerousMaterials;
	}
	
	public void setDangerousMaterials(Double dangerousMaterials) {
		this.dangerousMaterials = dangerousMaterials;
	}
	
	public Double getPreventionRating() {
		return preventionRating;
	}
	
	public void setPreventionRating(Double preventionRating) {
		this.preventionRating = preventionRating;
	}
	
	public Integer getCommercialCreditScore() {
		return commercialCreditScore;
	}
	
	public void setCommercialCreditScore(Integer commercialCreditScore) {
		this.commercialCreditScore = commercialCreditScore;
	}
	
	public Double getZone1Exp() {
		return zone1Exp;
	}
	
	public void setZone1Exp(Double zone1Exp) {
		this.zone1Exp = zone1Exp;
	}
	
	public Double getZone2Exp() {
		return zone2Exp;
	}
	
	public void setZone2Exp(Double zone2Exp) {
		this.zone2Exp = zone2Exp;
	}
	
	public Double getZone3Exp() {
		return zone3Exp;
	}
	
	public void setZone3Exp(Double zone3Exp) {
		this.zone3Exp = zone3Exp;
	}
	
	public Double getZone4Exp() {
		return zone4Exp;
	}
	
	public void setZone4Exp(Double zone4Exp) {
		this.zone4Exp = zone4Exp;
	}
	
	public Double getZone5Exp() {
		return zone5Exp;
	}
	
	public void setZone5Exp(Double zone5Exp) {
		this.zone5Exp = zone5Exp;
	}
	
	public Double getZone6Exp() {
		return zone6Exp;
	}
	
	public void setZone6Exp(Double zone6Exp) {
		this.zone6Exp = zone6Exp;
	}
	
	public Double getZone7Exp() {
		return zone7Exp;
	}
	
	public void setZone7Exp(Double zone7Exp) {
		this.zone7Exp = zone7Exp;
	}
	
	public Double getZone8Exp() {
		return zone8Exp;
	}
	
	public void setZone8Exp(Double zone8Exp) {
		this.zone8Exp = zone8Exp;
	}
	
	public Double getZone9Exp() {
		return zone9Exp;
	}
	
	public void setZone9Exp(Double zone9Exp) {
		this.zone9Exp = zone9Exp;
	}
	
	public Double getModificationFactor() {
		return modificationFactor;
	}
	
	public void setModificationFactor(Double modificationFactor) {
		this.modificationFactor = modificationFactor;
	}
	
	public Double getOffBalanceFactor() {
		return offBalanceFactor;
	}
	
	public void setOffBalanceFactor(Double offBalanceFactor) {
		this.offBalanceFactor = offBalanceFactor;
	}

}
