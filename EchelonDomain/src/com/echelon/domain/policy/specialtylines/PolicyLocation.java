package com.echelon.domain.policy.specialtylines;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.policy.Risk;

@SuppressWarnings("rawtypes")
@Entity
@Table(name = "PolicyLocation")
@PrimaryKeyJoinColumn(name = "PolicyRiskPK")
public class PolicyLocation extends Risk {
	private static final long serialVersionUID = -6911495209536417269L;
	private static final String RISK_TYPE = "Location";

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "AddressPK")
	private					Address				locationAddress;
	private					Integer 			locationNum;
	private					String 				locationId; 
	private 				Integer				numVehicles;
	private 				Long				customerMainAddressPK;
	
	@Column(name = "isVehiclesInBuilding")
	private 				Boolean 			isVehiclesInBuilding;
	
	@Column(name = "isVehiclesOnLot")
	private 				Boolean				isVehiclesOnLot;
	
	/*
	@ManyToMany(mappedBy="cglLocations", fetch = FetchType.EAGER)
	private 				Set<CGLSubPolicy>	cglSubPolicies;
	*/ 
	
	public PolicyLocation() {
		this.setRiskType(RISK_TYPE);
		this.locationAddress = new Address();
		//this.cglSubPolicies = new HashSet<CGLSubPolicy>();
	}

	public Address getLocationAddress() {
		return locationAddress;
	}

	public void setLocationAddress(Address locationAddress) {
		this.locationAddress = locationAddress;
	}

	public Integer getLocationNum() {
		return locationNum;
	}

	public void setLocationNum(Integer locationNum) {
		this.locationNum = locationNum;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public Integer getNumVehicles() {
		return numVehicles;
	}

	public void setNumVehicles(Integer numVehicles) {
		this.numVehicles = numVehicles;
	}

	public Long getCustomerMainAddressPK() {
		return customerMainAddressPK;
	}

	public void setCustomerMainAddressPK(Long customerMainAddressPK) {
		this.customerMainAddressPK = customerMainAddressPK;
	}

	public Boolean getIsVehiclesInBuilding() {
		return isVehiclesInBuilding;
	}

	public void setIsVehiclesInBuilding(Boolean isVehiclesInBuilding) {
		this.isVehiclesInBuilding = isVehiclesInBuilding;
	}

	public Boolean getIsVehiclesOnLot() {
		return isVehiclesOnLot;
	}

	public void setIsVehiclesOnLot(Boolean isVehiclesOnLot) {
		this.isVehiclesOnLot = isVehiclesOnLot;
	}

	/*
	public Set<CGLSubPolicy> getCglSubPolicies() {
		return cglSubPolicies;
	}

	public void setCglSubPolicies(Set<CGLSubPolicy> cglSubPolicies) {
		this.cglSubPolicies = cglSubPolicies;
	}
	*/

	@Override
	public Object clone() throws CloneNotSupportedException {
		PolicyLocation clone = (PolicyLocation) super.clone();
		//clone.setCglSubPolicies(new HashSet<CGLSubPolicy>());
		clone.locationAddress = (Address) locationAddress.clone();
		return clone;
	}
}
