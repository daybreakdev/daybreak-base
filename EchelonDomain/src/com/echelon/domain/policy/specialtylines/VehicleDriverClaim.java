package com.echelon.domain.policy.specialtylines;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.policy.wheels.Driver;

@Entity
@Table(name = "VehicleDriverClaim")
@PrimaryKeyJoinColumn(name = "VehicleDriverClaimPK")
public class VehicleDriverClaim extends AbstractAuditable {
	private static final long serialVersionUID = 2706664802120061644L;
	
	private static List<String> auditExcludeList = Arrays.asList("vehicleDriverClaimPK", 
			"specialtyVehicleRisk", "driver", "originatingPk");
	private static List<String> copyExcludeList = Arrays.asList("vehicleDriverClaimPK", 
			"specialtyVehicleRisk", "driver", "originatingPk");
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "vehicleDriverClaimPK", updatable = false, nullable = false)
	private 			Long 				vehicleDriverClaimPK;
	
	@ManyToOne
	@JoinColumn(name = "PolicyRiskPK", nullable = true)
	private SpecialtyVehicleRisk specialtyVehicleRisk;
	
	@ManyToOne
	@JoinColumn(name = "PersonPK", nullable = true)
	private Driver driver;

	private 			LocalDate 			lossDate; 		
	private 			String 				vehicle;
	private 			String 				claimType;
	private 			String 				faultResponsibility; 	
	private 			Double 				totalPaid;
	private 			Integer 			claimNum;
	private 			Boolean 			isInsurerClaim;
	private 			Long 				originatingPk;
	
	public VehicleDriverClaim() {
		super();
	}

	public LocalDate getLossDate() {
		return lossDate;
	}

	public void setLossDate(LocalDate lossDate) {
		this.lossDate = lossDate;
	}

	public String getVehicle() {
		return vehicle;
	}

	public void setVehicle(String vehicle) {
		this.vehicle = vehicle;
	}

	public String getClaimType() {
		return claimType;
	}

	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}

	public String getFaultResponsibility() {
		return faultResponsibility;
	}

	public void setFaultResponsibility(String faultResponsibility) {
		this.faultResponsibility = faultResponsibility;
	}

	public Double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}

	public Integer getClaimNum() {
		return claimNum;
	}

	public void setClaimNum(Integer claimNum) {
		this.claimNum = claimNum;
	}

	public Boolean getIsInsurerClaim() {
		return isInsurerClaim;
	}

	public void setIsInsurerClaim(Boolean isInsurerClaim) {
		this.isInsurerClaim = isInsurerClaim;
	}

	public Long getVehicleDriverClaimPK() {
		return vehicleDriverClaimPK;
	}

	public void setVehicleDriverClaimPK(Long vehicleDriverClaimPK) {
		this.vehicleDriverClaimPK = vehicleDriverClaimPK;
	}

	public SpecialtyVehicleRisk getSpecialtyVehicleRisk() {
		return specialtyVehicleRisk;
	}

	public void setSpecialtyVehicleRisk(SpecialtyVehicleRisk specialtyVehicleRisk) {
		this.specialtyVehicleRisk = specialtyVehicleRisk;
	}

	public Driver getDriver() {
		return driver;
	}

	public void setDriver(Driver driver) {
		this.driver = driver;
	}

	public Long getOriginatingPk() {
		return originatingPk;
	}

	public void setOriginatingPk(Long originatingPk) {
		this.originatingPk = originatingPk;
	}

	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}

	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}
	
	@Override
	public int hashCode() {
		return 31;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		VehicleDriverClaim vdcClone = (VehicleDriverClaim)super.clone();
		vdcClone.setVehicleDriverClaimPK(null);
		vdcClone.setSpecialtyVehicleRisk(null);
		vdcClone.setDriver(null);
		vdcClone.setOriginatingPk(this.vehicleDriverClaimPK);
		return vdcClone;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof VehicleDriverClaim)) return false;
		return getVehicleDriverClaimPK() != null && getVehicleDriverClaimPK().equals(((VehicleDriverClaim) obj).getVehicleDriverClaimPK());
	}

}
