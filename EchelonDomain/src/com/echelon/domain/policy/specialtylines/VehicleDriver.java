package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.policy.wheels.Driver;

@NamedQueries({
	@NamedQuery(
		name = "get_vehicledrivers_vehicle_has_surcharges",
		query = "select vdrv.driver.personPk, count(vdrv.specialtyVehicleRisk.policyRiskPK) from VehicleDriver vdrv"
				+" where vdrv.specialtyVehicleRisk.subPolicy.policyTransaction.policyTransactionPK = :p_policyTransactionPK "
				+" and (vdrv.systemStatus = 'PENDING' or vdrv.systemStatus = 'ACTIVE') "
				+" and (vdrv.specialtyVehicleRisk.systemStatus = 'PENDING' or vdrv.specialtyVehicleRisk.systemStatus = 'ACTIVE') "
				//ENFO-259; checking the surcharges is not necessary
				//+" and (vdrv.specialtyVehicleRisk.totalSurchargeOverride > 1 or vdrv.specialtyVehicleRisk.totalSurcharge > 1) "
				+" and (vdrv.driver.systemStatus = 'PENDING' or vdrv.driver.systemStatus = 'ACTIVE') "
				+" group by vdrv.driver.personPk "
				+" having count(vdrv.specialtyVehicleRisk.policyRiskPK) > 1"
	)
})

@Entity
@Table(name = "VehicleDriver")
public class VehicleDriver extends AbstractAuditable {

  private static final long serialVersionUID = -8052655841875520709L;

  private static List<String> auditExcludeList = Arrays.asList("vehicleDriverPK",
      "specialtyVehicleRisk", "driver", "originatingPk");
  private static List<String> copyExcludeList = Arrays.asList("vehicleDriverPK",
      "specialtyVehicleRisk", "driver", "originatingPk");

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "VehicleDriverPK", updatable = false, nullable = false)
  private Long vehicleDriverPK;

  @ManyToOne
  @JoinColumn(name = "PolicyRiskPK", nullable = true)
  private SpecialtyVehicleRisk specialtyVehicleRisk;

  @ManyToOne
  @JoinColumn(name = "PersonPK", nullable = true)
  private Driver driver;

  @Column(name = "OriginatingPK")
  private Long originatingPk;

  @Column(name = "DriverType")
  private String driverType;

  public VehicleDriver() {
	  super();
  }

  public Long getVehicleDriverPK() {
    return vehicleDriverPK;
  }

  public void setVehicleDriverPK(Long vehicleDriverPK) {
    this.vehicleDriverPK = vehicleDriverPK;
  }

  public SpecialtyVehicleRisk getSpecialtyVehicleRisk() {
    return specialtyVehicleRisk;
  }

  public void setSpecialtyVehicleRisk(SpecialtyVehicleRisk specialtyVehicleRisk) {
    this.specialtyVehicleRisk = specialtyVehicleRisk;
  }

  public Driver getDriver() {
    return driver;
  }

  public void setDriver(Driver driver) {
    this.driver = driver;
  }

  public Long getOriginatingPk() {
    return originatingPk;
  }

  public void setOriginatingPk(Long originatingPk) {
    this.originatingPk = originatingPk;
  }

  public String getDriverType() {
    return driverType;
  }

  public void setDriverType(String driverType) {
    this.driverType = driverType;
  }

  @Override
  public List<String> getAuditExcludeList() {
    return auditExcludeList;
  }

  @Override
  public List<String> getCopyExcludeList() {
    return copyExcludeList;
  }

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		VehicleDriver vdClone = (VehicleDriver)super.clone();
		vdClone.setVehicleDriverPK(null);
		vdClone.setSpecialtyVehicleRisk(null);
		vdClone.setDriver(null);
		vdClone.setOriginatingPk(this.vehicleDriverPK);
		return vdClone;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof VehicleDriver)) return false;
		return getVehicleDriverPK() != null && getVehicleDriverPK().equals(((VehicleDriver)obj).getVehicleDriverPK());
	}
  
}
