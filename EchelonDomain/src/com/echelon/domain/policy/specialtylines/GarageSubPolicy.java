package com.echelon.domain.policy.specialtylines;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;

@Entity
@Table(name = "GarageSubPolicy")
@PrimaryKeyJoinColumn(name = "SubPolicyPK")
public class GarageSubPolicy<RISKTYPE extends Risk> extends SubPolicy<RISKTYPE> {
	private static final long serialVersionUID = 5113369799565638739L;
	
	private 		Integer 		numFullTimeEmployees;
	private 		Integer 		numPartTimeEmployees;
	private 		String			businessDescription;

	public Integer getNumFullTimeEmployees() {
		return numFullTimeEmployees;
	}

	public void setNumFullTimeEmployees(Integer numFullTimeEmployees) {
		this.numFullTimeEmployees = numFullTimeEmployees;
	}

	public Integer getNumPartTimeEmployees() {
		return numPartTimeEmployees;
	}

	public void setNumPartTimeEmployees(Integer numPartTimeEmployees) {
		this.numPartTimeEmployees = numPartTimeEmployees;
	}

	public String getBusinessDescription() {
		return businessDescription;
	}

	public void setBusinessDescription(String businessDescription) {
		this.businessDescription = businessDescription;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof GarageSubPolicy)) return false;
		return getSubPolicyPK() != null && getSubPolicyPK().equals(((GarageSubPolicy<?>) obj).getSubPolicyPK());
	}
}
