package com.echelon.domain.policy.specialtylines;

import java.util.Arrays;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.AbstractAuditable;

@Entity
@Table(name = "CGLLocation")
public class CGLLocation extends AbstractAuditable {
	private static final long serialVersionUID = -8609118357388656405L;
	
	private static List<String> auditExcludeList = Arrays.asList(
			"cglLocationPK", "cglSubPolicy", "policyLocation"
	);
	
	private static List<String> copyExcludeList = Arrays.asList(
			"cglLocationPK", "cglSubPolicy", "policyLocation"
	);
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "CGLLocationPK", updatable = false, nullable = false)
	private 		Long 					cglLocationPK;
	
	@ManyToOne
	@JoinColumn(name = "SubPolicyPK", nullable = true)
	private 		CGLSubPolicy<?> 		cglSubPolicy;
	
	@ManyToOne
	@JoinColumn(name = "PolicyRiskPK")
	private 		PolicyLocation			policyLocation;

	public Long getCglLocationPK() {
		return cglLocationPK;
	}

	public void setCglLocationPK(Long cglLocationPK) {
		this.cglLocationPK = cglLocationPK;
	}

	public CGLSubPolicy<?> getCglSubPolicy() {
		return cglSubPolicy;
	}

	public void setCglSubPolicy(CGLSubPolicy<?> cglSubPolicy) {
		this.cglSubPolicy = cglSubPolicy;
	}

	public PolicyLocation getPolicyLocation() {
		return policyLocation;
	}

	public void setPolicyLocation(PolicyLocation policyLocation) {
		this.policyLocation = policyLocation;
	}

	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}

	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		CGLLocation clone = (CGLLocation)super.clone();
		clone.setCglLocationPK(null);
		clone.setCglSubPolicy(this.cglSubPolicy);
		clone.setPolicyLocation(this.policyLocation);
		return clone;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof CGLLocation)) return false;
		return getCglLocationPK() != null 
				&& getCglLocationPK().equals(((CGLLocation) obj).getCglLocationPK());
	}
}
