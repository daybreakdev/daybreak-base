package com.echelon.domain.policy.specialtylines;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.baseclasses.AbstractAuditable;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.Risk;

@Entity
@Table(name = "SpecialtyVehicleRiskAdjustment")
@PrimaryKeyJoinColumn(name = "SpecialtyVehicleRiskAdjustmentPK")
public class SpecialtyVehicleRiskAdjustment extends AbstractAuditable {
	private static final long serialVersionUID = -2893325402513802807L;
	private static List<String> auditExcludeList = Arrays.asList(
			"specialtyVehicleRiskAdjustmentPK", "originatingPK", ""
	);
	private static List<String> copyExcludeList = Arrays.asList(
			"specialtyVehicleRiskAdjustmentPK", "originatingPK", ""
	);	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SpecialtyVehicleRiskAdjustmentPK", updatable = false, nullable = false)
	private 		Long 					specialtyVehicleRiskAdjustmentPK;
	
	@ManyToOne
	@JoinColumn(name="PolicyRiskPK", nullable=true)
	private 		SpecialtyVehicleRisk 	specialtyVehicleRisk;
	
	private 		String 					vehicleDescription;
	private 		String 					adjustmentType;
	private 		LocalDate 				adjustmentDate;
	private 		Double 					proRataFactor;
	private 		Double 					premiumChange;
	private 		Integer 				numOfUnits;
	private 		Long 					originatingPK;
	
	//added by Marwan Sep. 30, 2022
	@OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "CoverageDetailsPK")
	private 		CoverageDetails			coverageDetail;
	//end of additions
	
	//added by Marwan Oct. 7, 2022
	private 		Integer 				adjustmentNum;
	
	@ManyToOne
	@JoinColumn(name="CGLSubPolicyPK", nullable=true)
	private 		CGLSubPolicy<Risk> 		cglSubPolicy;
	
	@ManyToOne
	@JoinColumn(name="CargoSubPolicyPK", nullable=true)
	private 		CargoSubPolicy<Risk> 	cargoSubPolicy;
	//end of additions
	
	public SpecialtyVehicleRiskAdjustment() {
		super();
		coverageDetail  = new CoverageDetails();
	}

	public Long getSpecialtyVehicleRiskAdjustmentPK() {
		return specialtyVehicleRiskAdjustmentPK;
	}

	public void setSpecialtyVehicleRiskAdjustmentPK(Long specialtyVehicleRiskAdjustmentPK) {
		this.specialtyVehicleRiskAdjustmentPK = specialtyVehicleRiskAdjustmentPK;
	}

	public SpecialtyVehicleRisk getSpecialtyVehicleRisk() {
		return specialtyVehicleRisk;
	}

	public void setSpecialtyVehicleRisk(SpecialtyVehicleRisk specialtyVehicleRisk) {
		this.specialtyVehicleRisk = specialtyVehicleRisk;
	}

	public String getVehicleDescription() {
		return vehicleDescription;
	}

	public void setVehicleDescription(String vehicleDescription) {
		this.vehicleDescription = vehicleDescription;
	}

	public String getAdjustmentType() {
		return adjustmentType;
	}

	public void setAdjustmentType(String adjustmentType) {
		this.adjustmentType = adjustmentType;
	}

	public LocalDate getAdjustmentDate() {
		return adjustmentDate;
	}

	public void setAdjustmentDate(LocalDate adjustmentDate) {
		this.adjustmentDate = adjustmentDate;
	}

	public Double getProRataFactor() {
		return proRataFactor;
	}

	public void setProRataFactor(Double proRataFactor) {
		this.proRataFactor = proRataFactor;
	}

	public Double getPremiumChange() {
		return premiumChange;
	}

	public void setPremiumChange(Double premiumChange) {
		this.premiumChange = premiumChange;
	}

	public Integer getNumOfUnits() {
		return numOfUnits;
	}

	public void setNumOfUnits(Integer numOfUnits) {
		this.numOfUnits = numOfUnits;
	}

	public Long getOriginatingPK() {
		return originatingPK;
	}

	public void setOriginatingPK(Long originatingPK) {
		this.originatingPK = originatingPK;
	}
	
	public CoverageDetails getCoverageDetail() {
		return coverageDetail;
	}

	public void setCoverageDetail(CoverageDetails coverageDetail) {
		this.coverageDetail = coverageDetail;
	}

	public CGLSubPolicy<Risk> getCglSubPolicy() {
		return cglSubPolicy;
	}

	public void setCglSubPolicy(CGLSubPolicy<Risk> cglSubPolicy) {
		this.cglSubPolicy = cglSubPolicy;
	}

	public CargoSubPolicy<Risk> getCargoSubPolicy() {
		return cargoSubPolicy;
	}

	public void setCargoSubPolicy(CargoSubPolicy<Risk> cargoSubPolicy) {
		this.cargoSubPolicy = cargoSubPolicy;
	}

	public Integer getAdjustmentNum() {
		return adjustmentNum;
	}

	public void setAdjustmentNum(Integer adjustmentNum) {
		this.adjustmentNum = adjustmentNum;
	}

	@Override
	public List<String> getAuditExcludeList() {
		return auditExcludeList;
	}

	@Override
	public List<String> getCopyExcludeList() {
		return copyExcludeList;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof SpecialtyVehicleRiskAdjustment)) return false;
		return getSpecialtyVehicleRiskAdjustmentPK() != null && getSpecialtyVehicleRiskAdjustmentPK().equals(
				((SpecialtyVehicleRiskAdjustment)obj).getSpecialtyVehicleRiskAdjustmentPK());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		SpecialtyVehicleRiskAdjustment adjClone = (SpecialtyVehicleRiskAdjustment)super.clone();
		adjClone.setSpecialtyVehicleRiskAdjustmentPK(null);
		adjClone.setOriginatingPK(this.specialtyVehicleRiskAdjustmentPK);
		adjClone.setSpecialtyVehicleRisk(null);
		adjClone.setCargoSubPolicy(null);
		adjClone.setCglSubPolicy(null);
		return adjClone;
	}
	
	public void addCoverage(Coverage coverage) {
		coverageDetail.addCoverage(coverage);
	}
	
	public void removeCoverage(Coverage coverage) {
		coverageDetail.removeCoverage(coverage);
	}
	
	public void addEndorsement(Endorsement endorsement) {
		coverageDetail.addEndorsement(endorsement);
	}
	
	public void removeEndorsement(Endorsement endorsement) {
		coverageDetail.removeEndorsement(endorsement);
	}
	
	protected Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustmentsFromParent(String groupID) {
		if (this.getSpecialtyVehicleRisk() != null) {
			return this.getSpecialtyVehicleRisk().getSvrAdjustments();
		}
		
		if (this.getCglSubPolicy() != null) {
			return this.getCglSubPolicy().getSvrAdjustmentsByGroupID(groupID);
		}
		
		if (this.getCargoSubPolicy() != null) {
			return this.getCargoSubPolicy().getSvrAdjustmentsByGroupID(groupID);
		}
		
		return new HashSet<SpecialtyVehicleRiskAdjustment>();
	}
	
	public String getGroupID() {
		return this.getDisplayString();
	}
	
	public void setGroupID(String groupID) {
		this.setDisplayString(groupID);
	}
	
	public SpecialtyVehicleRiskAdjustment findPrevAdjustment() {
		return findPrevAdjustment(this.getGroupID(), this.getAdjustmentNum());
	}
	public SpecialtyVehicleRiskAdjustment findPrevAdjustment(String groupID, Integer adjNum) {
		SpecialtyVehicleRiskAdjustment prevAdj = null;
		
		if (adjNum != null && StringUtils.isNotBlank(groupID)) {
			prevAdj = this.getSvrAdjustmentsFromParent(groupID).stream()
							.filter(o -> o.getAdjustmentNum() != null &&
										 o.getAdjustmentNum() < adjNum).collect(Collectors.toList())
							.stream()
							.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum).reversed())
							.findFirst().orElse(null);
		}
		// This is a new adjustment, so use the last existing one
		else {
			prevAdj = findLastAdjustment(groupID);
		}
		
		return prevAdj;
	}
	
	public SpecialtyVehicleRiskAdjustment findLastAdjustment(String groupID) {
		SpecialtyVehicleRiskAdjustment lastAdj = 
					this.getSvrAdjustmentsFromParent(groupID).stream()
					.filter(o -> o.getAdjustmentNum() != null).collect(Collectors.toList())
					.stream()
					.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum).reversed())
					.findFirst().orElse(null);
		
		return lastAdj;
	}
	
	public SpecialtyVehicleRiskAdjustment findNextAdjustment() {
		return findNextAdjustment(this.getGroupID(), this.getAdjustmentNum());
	}
	
	public SpecialtyVehicleRiskAdjustment findNextAdjustment(String groupID, Integer adjNum) {
		SpecialtyVehicleRiskAdjustment nextAdj = null;
		
		if (adjNum != null) {
			nextAdj = this.getSvrAdjustmentsFromParent(groupID).stream()
							.filter(o -> o.getAdjustmentNum() != null &&
										 o.getAdjustmentNum() > adjNum).collect(Collectors.toList())
							.stream()
							.sorted(Comparator.comparing(SpecialtyVehicleRiskAdjustment::getAdjustmentNum))
							.findFirst().orElse(null);
		}
		
		return nextAdj;
	}
	
}
