package com.echelon.domain.policy.specialtylines;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;

@Entity
@Table(name = "CommercialPropertySubPolicy")
@PrimaryKeyJoinColumn(name = "SubPolicyPK")
public class CommercialPropertySubPolicy<RISKTYPE extends Risk>
	extends SubPolicy<RISKTYPE> {
	private static final long serialVersionUID = 7078737128565174944L;

	@OneToMany(mappedBy = "commercialPropertySubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<CommercialPropertyLocation> commercialPropertyLocations;

	public CommercialPropertySubPolicy() {
		this.commercialPropertyLocations = new HashSet<>();
	}

	public void addCommercialPropertyLocation(
		CommercialPropertyLocation location) {
		location.setCommercialPropertySubPolicy(this);
		commercialPropertyLocations.add(location);
	}

	public void removeCommercialPropertyLocation(
		CommercialPropertyLocation location) {
		if (location.isActive()
			&& (location.isNoChange() || location.isModified())) {
			location.setSystemStatus(IAuditable.INACTIVE);
			location.setBusinessStatus(IAuditable.DELETED);
		} else if (location.isPending()) {
			commercialPropertyLocations.remove(location);
			location.setCommercialPropertySubPolicy(null);
		}
	}

	public Set<CommercialPropertyLocation> getCommercialPropertyLocations() {
		return commercialPropertyLocations;
	}

	public void setCommercialPropertyLocations(
		Set<CommercialPropertyLocation> locations) {
		this.commercialPropertyLocations = locations;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof CommercialPropertySubPolicy<?>))
			return false;

		Long pk = getSubPolicyPK();
		return pk != null && pk
			.equals(((CommercialPropertySubPolicy<?>) obj).getSubPolicyPK());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		CommercialPropertySubPolicy<Risk> clone = (CommercialPropertySubPolicy<Risk>) super.clone();
		clone.commercialPropertyLocations = new HashSet<>();
		return clone;
	}

	public CommercialPropertyLocation findLocationByPK(
		Long commercialPropertyLocationPK) {
		CommercialPropertyLocation rv = null;

		if (commercialPropertyLocationPK != null
			&& this.commercialPropertyLocations != null
			&& !this.commercialPropertyLocations.isEmpty()) {
			rv = this.commercialPropertyLocations.stream()
				.filter(o -> commercialPropertyLocationPK
					.equals(o.getCommercialPropertyLocationPK()))
				.findFirst().orElse(null);
		}

		return rv;
	}
}
