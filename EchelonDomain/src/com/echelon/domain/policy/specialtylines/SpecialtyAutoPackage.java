package com.echelon.domain.policy.specialtylines;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.policy.InsurancePolicy;

@Entity
@Table(name = "SpecialtyAutoPackage")
@PrimaryKeyJoinColumn(name = "InsurancePolicyPK")
public class SpecialtyAutoPackage extends InsurancePolicy {
	private static final long serialVersionUID = -3704753646169386285L;

	private Integer		liabilityLimit;
	private LocalDate 	vehicleListReceivedDate; 	
	private Boolean 	isProvideVehicleRegistration; 	
	private String 		vehicleRegistrationList;
	private Boolean 	isProvideDriverMVR;
	private String 		provideDriverMVRList; 		
	private Boolean 	isProvideFuelTaxReport; 		
	private String 		otherRequirements;
	private String 		territoryCode;
	private String 		statCode;		
	private String 		ruralityInd;
	private String 		autoAssocMemberNum;
	private String 		autoAssocClub;

	public Integer getLiabilityLimit() {
		return liabilityLimit;
	}

	public void setLiabilityLimit(Integer liabilityLimit) {
		this.liabilityLimit = liabilityLimit;
	}

	public LocalDate getVehicleListReceivedDate() {
		return vehicleListReceivedDate;
	}

	public void setVehicleListReceivedDate(LocalDate vehicleListReceivedDate) {
		this.vehicleListReceivedDate = vehicleListReceivedDate;
	}

	public Boolean getIsProvideVehicleRegistration() {
		return isProvideVehicleRegistration;
	}

	public void setIsProvideVehicleRegistration(Boolean isProvideVehicleRegistration) {
		this.isProvideVehicleRegistration = isProvideVehicleRegistration;
	}

	public String getVehicleRegistrationList() {
		return vehicleRegistrationList;
	}

	public void setVehicleRegistrationList(String vehicleRegistrationList) {
		this.vehicleRegistrationList = vehicleRegistrationList;
	}

	public Boolean getIsProvideDriverMVR() {
		return isProvideDriverMVR;
	}

	public void setIsProvideDriverMVR(Boolean isProvideDriverMVR) {
		this.isProvideDriverMVR = isProvideDriverMVR;
	}

	public String getProvideDriverMVRList() {
		return provideDriverMVRList;
	}

	public void setProvideDriverMVRList(String provideDriverMVRList) {
		this.provideDriverMVRList = provideDriverMVRList;
	}

	public Boolean getIsProvideFuelTaxReport() {
		return isProvideFuelTaxReport;
	}

	public void setIsProvideFuelTaxReport(Boolean isProvideFuelTaxReport) {
		this.isProvideFuelTaxReport = isProvideFuelTaxReport;
	}

	public String getOtherRequirements() {
		return otherRequirements;
	}

	public void setOtherRequirements(String otherRequirements) {
		this.otherRequirements = otherRequirements;
	}

	public String getTerritoryCode() {
		return territoryCode;
	}

	public void setTerritoryCode(String territoryCode) {
		this.territoryCode = territoryCode;
	}

	public String getStatCode() {
		return statCode;
	}

	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}

	public String getRuralityInd() {
		return ruralityInd;
	}

	public void setRuralityInd(String ruralityInd) {
		this.ruralityInd = ruralityInd;
	}

	public String getAutoAssocMemberNum() {
		return autoAssocMemberNum;
	}

	public void setAutoAssocMemberNum(String autoAssocMemberNum) {
		this.autoAssocMemberNum = autoAssocMemberNum;
	}

	public String getAutoAssocClub() {
		return autoAssocClub;
	}

	public void setAutoAssocClub(String autoAssocClub) {
		this.autoAssocClub = autoAssocClub;
	}
	
}
