package com.echelon.domain.policy.specialtylines;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.interfaces.specialtylines.ISpecialtyHasAdjustment;

@Entity
@Table(name = "CGLSubPolicy")
@PrimaryKeyJoinColumn(name = "SubPolicyPK")
public class CGLSubPolicy<RISKTYPE extends Risk> extends SubPolicy<RISKTYPE> implements ISpecialtyHasAdjustment {
	private static final long serialVersionUID = -3385852496875110316L;
	private static List<String> copyExcludeList = Arrays.asList("cglLocations", "svrAdjustments");

	@Column(name = "CGLDeductible")
	private Integer		cglDeductible;
	
	@Column(name = "CGLLimit")
	private Integer		cglLimit;
	
	@OneToMany(mappedBy = "cglSubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<CGLLocation> cglLocations;
	
	//added by Marwan Oct. 6, 2022
	@OneToMany(mappedBy = "cglSubPolicy", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	private 	Set<SpecialtyVehicleRiskAdjustment> 	svrAdjustments;
	//end of additions
	
	public CGLSubPolicy() {
		this.cglLocations = new HashSet<CGLLocation>();
		this.svrAdjustments = new HashSet<SpecialtyVehicleRiskAdjustment>();
	}
	
	public void addCglLocation(CGLLocation location) {
		location.setCglSubPolicy(this);
		cglLocations.add(location);
	}
	
	public void removeCglLocation(CGLLocation location) {
		if (location.isActive() && (location.isNoChange() || location.isModified())) {
			location.setSystemStatus(IAuditable.INACTIVE);
			location.setBusinessStatus(IAuditable.DELETED);
		} else if (location.isPending()) {
			cglLocations.remove(location);
			location.setCglSubPolicy(null);
		}
	}

	public Integer getCglDeductible() {
		return cglDeductible;
	}

	public void setCglDeductible(Integer cglDeductible) {
		this.cglDeductible = cglDeductible;
	}

	public Integer getCglLimit() {
		return cglLimit;
	}

	public void setCglLimit(Integer cglLimit) {
		this.cglLimit = cglLimit;
	}
	
	public Set<CGLLocation> getCglLocations() {
		return cglLocations;
	}

	public void setCglLocations(Set<CGLLocation> cglLocations) {
		this.cglLocations = cglLocations;
	}

	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustments() {
		return svrAdjustments;
	}

	public void setSvrAdjustments(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		this.svrAdjustments = svrAdjustments;
	}

	@Override
	public int hashCode() {
		return 31;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (!(obj instanceof CGLSubPolicy)) return false;
		return getSubPolicyPK() != null && getSubPolicyPK().equals(((CGLSubPolicy<?>) obj).getSubPolicyPK());
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		@SuppressWarnings("unchecked")
		CGLSubPolicy<Risk> clone = (CGLSubPolicy<Risk>) super.clone();
		clone.cglLocations = new HashSet<CGLLocation>();
		clone.setSvrAdjustments(new HashSet<SpecialtyVehicleRiskAdjustment>());
		return clone;
	}
	
	public CGLLocation findLocationByPK(Long cglLocationPK) {
		CGLLocation rv = null;
		
		if (cglLocationPK != null &&
			this.cglLocations != null && !this.cglLocations.isEmpty()) {
			rv = this.cglLocations.stream().filter(o -> cglLocationPK.equals(o.getCglLocationPK())).findFirst().orElse(null);
		}
		
		return rv;
	}
	
	@SuppressWarnings("unchecked")
	public void addSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		svrAdjustment.setCglSubPolicy((CGLSubPolicy<Risk>) this);
		svrAdjustments.add(svrAdjustment);
	}
	
	public void removeSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment svrAdjustment) {
		if (svrAdjustment.isActive() && (svrAdjustment.isNoChange() || svrAdjustment.isModified())) {
			svrAdjustment.setSystemStatus(IAuditable.INACTIVE);
			svrAdjustment.setBusinessStatus(IAuditable.DELETED);
		} else if (svrAdjustment.isPending()) {
			svrAdjustments.remove(svrAdjustment);
			svrAdjustment.setCglSubPolicy(null);
		}
	}
	
	public Set<SpecialtyVehicleRiskAdjustment> getSvrAdjustmentsByGroupID(String groupID) {
		Set<SpecialtyVehicleRiskAdjustment> results = new HashSet<SpecialtyVehicleRiskAdjustment>();
		if (groupID != null) {
			results = this.svrAdjustments.stream().filter(o -> groupID.equalsIgnoreCase(o.getGroupID())).collect(Collectors.toSet());
		}
		return results;
	}
	
	public SpecialtyVehicleRiskAdjustment getSvrAdjustmentsByAdjNum(String groupID, Integer adjNum) {
		SpecialtyVehicleRiskAdjustment result = null;
		
		if (adjNum != null) {
			Set<SpecialtyVehicleRiskAdjustment> ls = getSvrAdjustmentsByGroupID(groupID);
			result = ls.stream().filter(o -> o.getAdjustmentNum().equals(adjNum)).findFirst().orElse(null);
		}
		
		return result;
	}
	
	@Override
	public List<String> getCopyExcludeList() {
		List<String> rv = (new ArrayList<String>());
		rv.addAll(super.getCopyExcludeList());
		rv.addAll(copyExcludeList);
		return rv;
	}

}
