package com.echelon.reports.impl;

import java.sql.Connection;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletContext;

import com.daystar.reporting.HTMLReport;
import com.daystar.reporting.PDFReport;
import com.daystar.reporting.XLSXReport;
import com.daystar.reporting.base.BaseReport;
import com.daystar.reporting.base.ReportManager;
import com.daystar.reporting.base.ReportSetup;
import com.daystar.reporting.common.ReportFormat;
import com.daystar.reporting.provider.ReportConnectionProvider;
import com.daystar.reporting.provider.ReportServiceProvider;
import com.echelon.reports.base.ReportResource;
import com.echelon.reports.exception.NotInitializedInstanceException;
import com.echelon.reports.util.ReportUtils;

import net.sf.jasperreports.engine.JRDataSource;

public class ReportServiceImpl implements ReportServiceProvider {
	private ReportSetup setup;
	private ReportManager manager;
	private ReportConnectionProvider connectionProvider;
	private static ReportServiceImpl INSTANCE;
	
	private ReportServiceImpl(Properties properties, ServletContext context) throws Exception {
		this.setup = ReportSetup.usingProperties(properties);
		this.manager = ReportManager.forWebApp(setup, context);
		setConnection(new ReportConnectionImpl());
	}
	
	public static ReportServiceImpl initService(Properties properties, ServletContext context) throws Exception {
		if (INSTANCE == null) {
			INSTANCE = new ReportServiceImpl(properties, context);
		}
		return INSTANCE;
	}
	
	public static ReportServiceImpl getService() throws NotInitializedInstanceException {
		if (INSTANCE == null) {
			throw new NotInitializedInstanceException();
		}
		return INSTANCE;
	}
	
	@Override
	public ReportResource print(String templateName, String outputName, ReportFormat format) throws Exception {
		return print(templateName, outputName, format, null, null);
	}
	
	@Override
	public ReportResource print(String templateName, String outputName, ReportFormat format,
			Map<String, Object> parameters, JRDataSource dataSource) throws Exception {
		BaseReport report = null;
		Connection conn = connectionProvider.getConnection();
		String dbVendor = ReportUtils.getDBVendor(conn.getMetaData().getDatabaseProductName());
		String tempName = templateName + dbVendor + ".jrxml";
		switch(format) {
		case PDF:
			report = new PDFReport(tempName);
			break;
		case HTML:
			report = new HTMLReport(tempName);
			break;
		case XLSX:
			report = new XLSXReport(tempName);
			break;
		}
		report.setOutputReportName(outputName);
		report.setParameters(parameters);
		report.setConnection(conn);
		report.setDataSource(dataSource);
		manager.print(report);
		return new ReportResource(report.getOutputReportName(), report.getOutputReportDir());
	}

	@Override
	public void setConnection(ReportConnectionProvider connectionProvider) {
		this.connectionProvider = connectionProvider;
	}

}
