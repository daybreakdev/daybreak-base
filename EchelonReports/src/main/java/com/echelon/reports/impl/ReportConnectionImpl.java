package com.echelon.reports.impl;

import java.sql.Connection;

import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;

import com.daystar.reporting.provider.ReportConnectionProvider;
import com.ds.ins.dal.dao.BaseHibernateDAO;
import com.ds.ins.domain.baseclasses.BaseBusinessEntity;

public class ReportConnectionImpl extends BaseHibernateDAO<BaseBusinessEntity, Long> implements ReportConnectionProvider {
	private ConnectionProvider connProvider;
	
	public ReportConnectionImpl() throws Exception {
		this.connProvider = getSessionFactory().getSessionFactoryOptions().getServiceRegistry().
			getService(ConnectionProvider.class);
	}
	
	@Override
	protected Class<BaseBusinessEntity> getModelClass() {
		return BaseBusinessEntity.class;
	}
	
	@Override
	public Connection getConnection() throws Exception {
		return connProvider.getConnection();
	}
	
}
