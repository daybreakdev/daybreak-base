package com.echelon.reports.base;

public interface Reportable {

	public ReportResource print() throws Exception;
	
}
