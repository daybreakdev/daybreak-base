package com.echelon.reports.base;

import java.util.Map;

import com.daystar.reporting.common.ReportFormat;
import com.echelon.reports.impl.ReportServiceImpl;

public class SimplePDFReport implements Reportable {
	private ReportServiceImpl service;
	private String templateName;
	private String reportName;
	private Map<String, Object> parameters;
	
	public SimplePDFReport(ReportServiceImpl service) {
		this.service = service;
	}
	
	@Override
	public ReportResource print() throws Exception {
    	return service.print(templateName, reportName, ReportFormat.PDF, parameters, null);
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getReportName() {
		return reportName;
	}

	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public void setParameters(Map<String, Object> parameters) {
		this.parameters = parameters;
	}

}
