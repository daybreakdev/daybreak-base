package com.echelon.reports.exception;

public class NotInitializedInstanceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4395073738478477824L;
	
	public NotInitializedInstanceException() {
		super("A report instance with Setup & Manager must be initialized before use.");
	}

}
