package com.echelon.interfaces.policy.specialtylines;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.system.UserProfile;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyVehicleRiskDTO;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CVRate;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.domain.policy.specialtylines.VehicleRateGroup;
import com.echelon.domain.policy.specialtylines.VehicleRateGroupLHT;

public interface ISpecialtyAutoPolicyProcessing extends IGenericPolicyProcessing<SpecialtyAutoPackage, PolicyVersion, PolicyTransaction<?>, SubPolicy<?>> {
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> createAutoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public CGLSubPolicy<SpecialtyVehicleRisk> createCGLSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public CommercialPropertySubPolicy<SpecialtyVehicleRisk> createCommercialPropertySubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public CargoSubPolicy<SpecialtyVehicleRisk> createCargoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public GarageSubPolicy<SpecialtyVehicleRisk> createGarageSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public SpecialtyVehicleRisk createSpecialtyVehicleRisk(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy) throws SpecialtyAutoPolicyException;
	public SpecialtyVehicleRisk updateSpecialtyVehicleRisk(SpecialtyVehicleRisk speciialVehicleRisk, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void deleteSpecialtyVehicleRisk(SpecialtyVehicleRisk speciialVehicleRisk) throws SpecialtyAutoPolicyException;	
	public SpecialtyVehicleRisk copySpecialtyVehicleRisk(SpecialtyVehicleRisk specialVehicleRisk) throws SpecialtyAutoPolicyException;
	public PolicyLocation createPolicyLocation(PolicyTransaction<PolicyLocation> policyTransaction) throws SpecialtyAutoPolicyException;
	public PolicyLocation updatePolicyLocation(PolicyLocation policyLocation, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void deletePolicyLocation(PolicyLocation policyLocation, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public PolicyLocation copyPolicyLocation(PolicyLocation policyLocation, PolicyTransaction<PolicyLocation> policyTransaction) throws SpecialtyAutoPolicyException;
	public PolicyDocuments updatePolicyDocument(PolicyDocuments policyDocument, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void deletePolicyDocument(PolicyDocuments policyDocument, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public List<VehicleRateGroup> findVehicleRateGroup(Integer vehicleAge, Double lowLimit, Double uppLimit) throws SpecialtyAutoPolicyException;
	public PolicyTransaction<?> updatePolicyLiabilityLimit(Integer liabilityLimit, PolicyTransaction<?> pcyTxn, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public PolicyTransaction<?> updateSpecialtyAutoPackageData(Integer liabilityLimit, String quotePreparedBy, LocalDateTime termEffective, Long pcyTxnPK, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public PolicyTransaction<?> updateSpecialtyAutoPackageData(Integer liabilityLimit, String quotePreparedBy, LocalDateTime termEffective, String externalQuoteNumber, Long pcyTxnPK, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public List<CVRate> getAllCVRates() throws SpecialtyAutoPolicyException;
	public PolicyTransaction<?> updateCustomerMainAddress(PolicyTransaction<?> policyTransaction, UserProfile userProfile) throws InsurancePolicyException;
	public SpecialityAutoSubPolicy<?> updateSpecialityAutoSubPolicy(SpecialityAutoSubPolicy<?> specialityAutoSubPolicy, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public CGLSubPolicy<?> updateCGLSubPolicy(CGLSubPolicy<?> cglSubPolicy, CGLSubPolicy<?> prevCGLSubPolicy, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void createCargoDetails(CargoSubPolicy<?> subPolicy, List<CargoDetails> cargoDetailsList, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void updateCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void deleteCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void undeleteCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public List<LienholderLessorVehicleDetails> assignLienholderLessorVehicleDetailsSequence(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy, LienholderLessor lienLessor, List<LienholderLessorVehicleDetails> lienholderLessorVehicleDetailsList) throws SpecialtyAutoPolicyException;
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> undeleteAddInsVehicle(PolicyTransaction<SpecialtyVehicleRisk> trans, SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy, AdditionalInsuredVehicles aiVehicle) throws SpecialtyAutoPolicyException;
	public CargoSubPolicy<?> updateCargoSubPolicy(CargoSubPolicy<?> cargoSubPolicy, CargoSubPolicy<?> prevCargoSubPolicy, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public Map<Long, SpecialtyVehicleRiskDTO> getVehicleRiskDTOByPk(List<Long> pkList) throws SpecialtyAutoPolicyException;
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> undeleteLeanholderLessoeVehDetails(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy, LienholderLessorVehicleDetails lhlVD) throws SpecialtyAutoPolicyException;
	public List<String>	getRatingWarnings(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public List<String> getSubpolicyRatingWarnings(SubPolicy<?> subPolicy, List<Coverage> coverages, List<Endorsement> endorsements) throws SpecialtyAutoPolicyException;
	public List<String> getRiskRatingWarnings(Risk risk, List<Coverage> coverages, List<Endorsement> endorsements) throws SpecialtyAutoPolicyException;
	public List<VehicleRateGroupLHT> findVehicleRateGroupLHT(Double lowLimit, Double uppLimit) throws SpecialtyAutoPolicyException;

	public SpecialtyVehicleRiskAdjustment createSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRisk parentVehicle, 
			PolicyTransaction<?> policyTransaction) throws SpecialtyAutoPolicyException;
	public SpecialtyVehicleRiskAdjustment updateSpecialtyVehicleRiskAdjustment(
			SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, UserProfile userProfile) throws SpecialtyAutoPolicyException;
	public void deleteSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, UserProfile userProfile)
			throws SpecialtyAutoPolicyException;
	public SpecialtyVehicleRiskAdjustment undeleteSpecialtyVehicleRiskAdjustment(PolicyTransaction<?> policyTransaction,
			SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment)
			throws SpecialtyAutoPolicyException;
		
	public double getOriginatingUnitRate(SpecialtyVehicleRisk vehicle);
	
	//added by Marwan Sep. 30, 2022
	public SpecialtyVehicleRiskAdjustment loadSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment adjustment) throws SpecialtyAutoPolicyException;
	//end of additions
	
	public boolean validateDriverSurcharged(PolicyTransaction<?> pcyTxn) throws SpecialtyAutoPolicyException;
	public List<String>	getPolicyActionWarnings(String action, PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException;
	public SpecialtyPolicyDTO getSpecialtyPolicyDTO(PolicyTransaction<?> pcyTxn) throws SpecialtyAutoPolicyException;
	public ReinsuranceReportDTO getReinsuranceReportDTO(PolicyTransaction<?> policyTransaction) throws SpecialtyAutoPolicyException; 
}