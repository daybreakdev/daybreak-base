package com.echelon.utils;

import java.util.Arrays;
import java.util.List;

public class LHTConstants extends SpecialtyAutoConstants {
	private static final long serialVersionUID = 4757544137190872391L;

	public static final String LHTQUOTENUMCNT = "LHTQUOTENUMCNT";
	public static final String LHT_QUOTE_PREFIX = "ECON";
	public static final String LHT_POLICY_PREFIX = "OLH";
	public static final String LHTPOLICYNUMCNT = "LHTPOLICYNUMCNT";
	public static final String SUBPCY_END_CODE_CTC = "CTC";
	public static final String LHT_VEH_RISK_VEHICLE_USE = "In Use (Cargo)";
	public static final String LHT_VEH_RISK_TYPEOFUNIT = "Power Unit";
	
//	Power units excludes the following vehicles:
//
//	Include Parked Power Unit
//	Include all vehicle use (In Use(cargo), In Use (Not cargo), Storage). 
	public static final List<String> LHT_CGL_COVUNITS_BASEON_INCLTYPEUNITS = Arrays.asList("POWER UNIT", "POWER UNIT PARKED");
	public static final List<String> LHT_CGL_COVUNITS_BASEON_INCLVEHUSE = Arrays.asList("IN USE (CARGO)", "IN USE (NO CARGO)", "STORAGE");
}
