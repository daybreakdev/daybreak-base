package com.echelon.utils;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.ds.ins.utils.Constants;

public class SpecialtyAutoConstants implements Serializable {
	private static final long serialVersionUID = -3910685659283581628L;

	public static final String 	SPECAUTO_QUOTE_PREFIX 				= "CAON";	
	public static final String 	SPECAUTO_POLICY_PREFIX 				= "OTO";
	public static final String 	PCY_LOCATION_ID_DEFAULT 			= "MAILING";
	public static final String 	SUBPCY_CODE_CARGO					= "CGO";
	public static final String 	SUBPCY_CODE_GEN_LIABILITY 			= "CGL";
	public static final String 	SUBPCY_CODE_AUTO					= "AUTO";
	public static final String  SUBPCY_CODE_GARAGE				    = "GAR";
	public static final String 	SUBPCY_CODE_COMMERCIAL_PROPERTY	= "CP";
	public static final String 	SUBPCY_COV_CODE_CPE					= "CPE";
	public static final Double 	SUBPCY_COV_CODE_CPE_DEF_PREM 		= 500.0;
	public static final String 	SUBPCY_COV_CODE_MTCC				= "MTCC";
	public static final String 	SUBPCY_COV_CODE_BIPD				= "BIPD";
	public static final String 	SUBPCY_COV_CODE_EBE					= "EBE";
	public static final String 	SUBPCY_COV_CODE_SP64				= "SP64";
	public static final String 	SUBPCY_COV_CODE_CM512				= "CM512";
	public static final String 	SUBPCY_COV_CODE_CM513				= "SP513";
	public static final String 	SUBPCY_COV_CODE_SP514				= "SP514";
	public static final String 	SUBPCY_COV_CODE_TLL					= "TLL";
	public static final String 	SUBPCY_COV_CODE_WLL					= "WLL";
	public static final String 	RISK_TYPE_SPEC_VEH 					= "AU";
	public static final String 	RISK_TYPE_SPEC_LOCATION				= "LO";
	public static final Integer SUBPCY_CARGO_LIMIT_DEFAULT  		= 10000;
	public static final Integer SUBPCY_CARGO_DEDUCTIBLE_DEFAULT 	= 1000;
	public static final String  SPEC_VEH_RISK_VEHICLE_USE			= "In Use (No Cargo)";
	public static final String  SPEC_VEH_RISK_DRIVING_RECORD    	= "3";
	public static final String  SPEC_VEH_RISK_DESC_OPCF27B 			= "OP27"; // value is OP27 but label = OPCF27B
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF27B 	= "OPCF27B";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF20	= "OPCF20";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF27	= "OPCF27";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF43	= "OPCF43";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF44R   = "OPCF44R";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF43A   = "OPCF43A";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_EON20A   	= "EON20A";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_EON20G   	= "EON20G";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF8	    = "OPCF8";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF38	= "OPCF38";
	public static final String  SPEC_VEH_RISK_ENDORSEMENT_OPCF48	= "OPCF48";
	
	public static final String  SPEC_VEH_RISK_COV_CODE_AB			= "AB";
	public static final String  SPEC_VEH_RISK_COV_CODE_UA			= "UA";
	public static final String  SPEC_VEH_RISK_COV_CODE_AP			= "AP";
	public static final String  SPEC_VEH_RISK_COV_CODE_PAIL			= "PAIL";
	public static final String  SPEC_VEH_RISK_COV_CODE_CL			= "CL";
	public static final String  SPEC_VEH_RISK_COV_CODE_CM			= "CM";
	public static final String  SPEC_VEH_RISK_COV_CODE_BI			= "BI";
	public static final String  SPEC_VEH_RISK_COV_CODE_PD			= "PD";
	public static final String  SPEC_VEH_RISK_COV_CODE_DCPD			= "DCPD";
	public static final String  SPEC_VEH_RISK_COV_CODE_SP			= "SP";
	public static final String  SPEC_VEH_RISK_COV_CODE_IR			= "IR";
	public static final String  SPEC_VEH_RISK_COV_CODE_MRC			= "MRC";
	public static final String  SPEC_VEH_RISK_COV_CODE_OCI			= "OCI";
	public static final String  SPEC_VEH_RISK_COV_CODE_CHM			= "CHM";
	public static final String  SPEC_VEH_RISK_COV_CODE_DF			= "DF";	
	public static final String  SPEC_VEH_RISK_COV_CODE_DC			= "DC";
	public static final String  SPEC_VEH_RISK_COV_CODE_IB			= "IB";
	public static final String  RFID_APPREM_NOSURCH					= "APPREM_NOSURCH";
	public static final String  RFID_CLPREM_NOSURCH					= "CLPPREM_NOSURCH";
	public static final String  RFID_BIPREM_NOSURCH					= "BIPREM_NOSURCH";
	public static final String 	RFID_VEHICLEDESC					= "Vehicle Desc";			
	
	public static final String  CARGO_DETAILS_DEF_DESCRIPTION		= "Customers' Personal Effects";
	public static final Double 	CARGO_DETAILS_DEF_CARGO_VALUE		= 100.0;
	public static final String  CARGO_SUBPCY_ENDORSEMENT_AOCC		= "AOCC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_MOCC 		= "MOCC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_GECGLMTC	= "GECGLMTC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_TTC 		= "TTC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_CTC		= "CTC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_ALOCC		= "ALOCC";
	public static final String  CARGO_SUBPCY_ENDORSEMENT_DLOCC		= "DLOCC";	
	public static final String  CARGO_SUBPCY_ENDORSEMENT_AICC		= "AICC";
	public static final String  CGL_SUBPCY_COVERAGE_WLL				= "WLL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_AOCC			= "AOCCGL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_MOCC 		= "MOCCGL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_GECGL 		= "GECGL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_ALOCCGL 		= "ALOCCGL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_DLOCCGL 		= "DLOCCGL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_CDEL 		= "CDEL";
	public static final String  CGL_SUBPCY_ENDORSEMENT_CRB			= "CRB";	

	public static final String COMMERCIAL_PROPERTY_SUBPCY_ENDORSEMENT_ALOCCP = "ALOCCP";
	
	public static final String  PCY_ENDORSEMENT_DOCC 				= "DOCC";
	public static final String  GARAGE_SUBPCY_ENDORSEMENT_OEF72		= "OEF72";
	public static final String  GARAGE_SUBPCY_ENDORSEMENT_AIC		= "AIC";
	public static final String  GARAGE_SUBPCY_ENDORSEMENT_OEF77  	= "OEF77";
	public static final String  GARAGE_SUBPCY_ENDORSEMENT_GDPC  	= "GDPC";
	
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_ID_27B  	       		  = "EXT27B";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHTYPE 		  = "OPCF27B_VehType";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHTYPE_OPCF278 = "OPCF 27B";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHLPN 		  = "OPCF27B_VehLpn";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHYEAR 		  = "OPCF27B_VehYear";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_ImplRG 		  = "OPCF27B_ImplRG";
	public static final String  SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_SelRG 		  = "OPCF27B_SelRG";
	public static final String  RFID_OPCF27B_VRG											  = "OPCF27B_VRG";

	public static final Set<String> ENDRSMNT_CODES_SEQUENCING   	= Stream.of(
		"EON114",
		"EON101",
		"EON102",
		"EON109",
		"EON110",
		"EON111",
		"EON112",
		"EON116",
		"EON117",
		"EON121",
		"EON122",
		"EON20G",
		"DOC",
		"GECGLMTC",
		"GECGL",
		"AIC",
		"AICC",
		"AOCCGL",
		"MOCCGL",
		"CDEL",
		"AOCC",
		"MOCC",
		"GDPC",
		"CPEE",
		"CDEC",
		"TTC",
		"GLEE",
		"IA",
		"WOS"
	).collect(Collectors.toCollection(HashSet::new));
	public static final String AUTO_SUBPCY_ENDORSEMENT_EON114 	= "EON114";
	public static final String AUTO_SUBPCY_ENDORSEMENT_OPCF21B  = "OPCF21B";
	public static final String AUTO_SUBPCY_ENDORSEMENT_OPCF5	= "OPCF5";
	public static final String AUTO_SUBPCY_ENDORSEMENT_OPCF23A  = "OPCF23A";
	public static final String AUTO_SUBPCY_FLEETBASIS_SCHEDULED	= "SCHD";
	public static final Set<String> AUTO_SUBPCY_21B_FLEETBASIS  = Stream.of(
					"21B5","21BQ","21BS","21BA"
			).collect(Collectors.toCollection(HashSet::new));
	public static final String LIENLESSORTYPE_HLESSOR = "LESSOR";
	public static final String LIENLESSORTYPE_HOLDER  = "LIENHOLDER";
	public static final Set<String> SPEC_VEH_DESC_TRAILERS = Stream.of(
			"ET", "TR"
	).collect(Collectors.toCollection(HashSet::new));
	public static final String PCY_ENDORSEMENT_EON121		= "EON121";
	public static final String PCY_ENDORSEMENT_EON122		= "EON122";
	public static final String WARN_COVEND_PREMBASEDONDED_MESSAGECODE = "WarnPremBasedOnCovDeductible";
	public static final String WARN_COV_WLL_LIMIT_MESSAGECODE 	      = "WarnPremBasedOnCovWLLLimit";
	public static final Integer WARN_COV_WLL_LIMIT_VALUE			  = 500000;
	public static final String WARN_SURCHARGE_ON_VEHICLE			  = "RatingSurchargeOnVehicleWarn";
	// Cov/End codes; vehicle types; Validate Deductibe; Warn Deductibe message value
	public static final String WARN_COVEND_PREMBASEDONDED_ENTRY_SEPARAOTR    = ";";
	public static final String WARN_COVEND_PREMBASEDONDED_SUBENTRY_SEPARAOTR = "\\|";
	public static final Set<String> WARN_COVEND_PREMBASEDONDED_LIST = Stream.of(
			"C;BI|DCPD|CL|CM|AP|SP;CM;7500;5000",
			"C;BI|DCPD|CL|CM|AP|SP;CM;12500|15000|20000;10000",
			"C;BI|DCPD|CL|CM|AP|SP;IU|PP;7500;7000",
			"C;BI|DCPD|CL|CM|AP|SP;IU|PP;12500;12000",
			"C;BI|DCPD|CL|CM|AP|SP;;50000|75000|100000;25000",
			"E;OPCF27B;CM;7500;5000",
			"E;OPCF27B;CM;12500|15000|20000;10000",
			"E;OPCF27B;IU|PP;7500;7000",
			"E;OPCF27B;IU|PP;12500;12000",
			"E;OPCF27B;;50000|75000|100000;25000"
	).collect(Collectors.toCollection(HashSet::new));
		
	public static final List<String> CGL_UNITS_COVERAGS	  	 		= Arrays.asList(SUBPCY_COV_CODE_BIPD, SPEC_VEH_RISK_COV_CODE_PAIL);
	public static final List<String> CGL_LOC_ENDORSEMENTS 	 		= Arrays.asList(CGL_SUBPCY_ENDORSEMENT_ALOCCGL);
	public static final List<String> GARAGE_LOC_COVERAGES 	 		= Arrays.asList(SUBPCY_COV_CODE_SP64, 
																					SUBPCY_COV_CODE_CM512, 
																					SUBPCY_COV_CODE_CM513, 
																					SUBPCY_COV_CODE_SP514);
	public static final List<String> GARAGE_SECTION5_COVERAGES		= Arrays.asList(SUBPCY_COV_CODE_CM512, SUBPCY_COV_CODE_CM513, SUBPCY_COV_CODE_SP514);
	public static final List<String> CARGO_LOC_COVERAGES   	 		= Arrays.asList(SUBPCY_COV_CODE_MTCC);
	public static final List<String> CARGO_LOC_ENDORSEMENTS  	  	= Arrays.asList(CARGO_SUBPCY_ENDORSEMENT_ALOCC);
	public static final List<String> CARGO_COVUNITS_BASEON_VEHUSE 	= Arrays.asList("In Use (Cargo)");
	
	public static final List<String> COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS = Arrays.asList(COMMERCIAL_PROPERTY_SUBPCY_ENDORSEMENT_ALOCCP);

	public static final String RULE_GROUP_DOCUMENT 	= "DOCUMENT";
	public static final String RULE_GROUP_RATING 	= "RATING";
	public static final List<String> EXCLUDEROLLUPTOPOLICY_RISKS = Arrays.asList(SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH, SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION);
	
//	Power units excludes the following vehicles:
//
//		Vehicle Type =�PP� and Vehicle Description = �PP�
//		Any Vehicle Type and Vehicle Description = �DP�
//		Any Vehicle Type and Vehicle Description = �TR�
//		Any Vehicle Type and Vehicle Description = �ET�
//		Any Vehicle Type and Vehicle Description = �OP27�
//		Any Vehicle Type and Vehicle Description = �ATV� (All Terrain Vehicle)
//      exclude all Vehicle Type = Private Passenger Vehicle Type =�PP�
	public static final List<String[]> CGL_COVUNITS_BASEON_EXCLVEHTYPENDESC = Arrays.asList(
												//new String[] {"PP","PP"},
												new String[] {"","DP"},
												new String[] {"","TR"},
												new String[] {"","ET"},
												new String[] {"","OP27"},
												new String[] {"","ATV"},
												new String[] {"PP",""});
// Exclude all private passenger vehicles, trailers, ATV, OPCF27B, dealer plate.
	public static final List<String[]> CARGO_COVUNITS_BASEON_EXCLVEHTYPENDESC = Arrays.asList(
												new String[] {"PP",""},
												new String[] {"","TR"},
			 									new String[] {"","ATV"},
			 									new String[] {"","OP27"},
			 									new String[] {"","DP"});
// Validation rules are required for version statuses
	public static final List<String> VALIDATIONRULLE_VERSIONSTATUSES = Arrays.asList(
												Constants.VERSION_STATUS_QUOTED,
												Constants.VERSION_STATUS_BOUND,
												Constants.VERSION_STATUS_ISSUED,
											    Constants.VERSION_STATUS_QUOTEDCHANGE,
												Constants.VERSION_STATUS_BOUNDCHANGE,
												Constants.VERSION_STATUS_ISSUEDCHANGE,
												Constants.VERSION_STATUS_QUOTEDRENEWAL,
												Constants.VERSION_STATUS_BOUNDRENEWAL,
												Constants.VERSION_STATUS_ISSUEDRENEWAL,
												Constants.VERSION_STATUS_ISSUEDREINSTATEMENT,
												Constants.VERSION_STATUS_ISSUEDREISSUE
												);
	
	public static final String SPEC_TRANSACTIONDATA_EXPOSURE_RATING = "EXPOSURE RATING";

	// Ratefactor IDs

	// Ratefactor ID
	public static final String RFID_UWADJUSTMENT				= "UWAdjustment";
	public static final String RFID_COMMISSION_FACTOR			= "CommFactor";
	public static final String RFID_PROVINCE 					= "Province";
	public static final String RFID_TERRITORY 					= "TERRITORY";
	public static final String RFID_RURALURBAN 					= "RURALURBAN";
	public static final String RFID_LIMIT 						= "Limit";
	public static final String RFID_YEARS_IN_BUSINESS 			= "Years in business";
	public static final String RFID_EXPOSURE_ZONE1 				= "Exposure Zone1";
	public static final String RFID_EXPOSURE_ZONE2 				= "Exposure Zone2";
	public static final String RFID_EXPOSURE_ZONE3 				= "Exposure Zone3";
	public static final String RFID_EXPOSURE_ZONE4 				= "Exposure Zone4";
	public static final String RFID_EXPOSURE_ZONE5 				= "Exposure Zone5";
	public static final String RFID_EXPOSURE_ZONE6 				= "Exposure Zone6";
	public static final String RFID_EXPOSURE_ZONE7 				= "Exposure Zone7";
	public static final String RFID_EXPOSURE_ZONE8 				= "Exposure Zone8";
	public static final String RFID_EXPOSURE_ZONE9 				= "Exposure Zone9";
	public static final String RFID_CLAIM_FREQUENCY 			= "Claim Frequency";
	public static final String RFID_DANGEROUS_MATERIALS 		= "Dangerous Materials";
	public static final String RFID_COMMERCIAL_CREDIT_SCORE 	= "Commercial Credit Score";
	public static final String RFID_PREVENTION_RATING			= "Prevention rating";
	public static final String RFID_TYPE_OF_VEHICLE 			= "Type of Vehicle";
	public static final String RFID_AVERAGE_KMS_X1				= "Average Kms X1";
	public static final String RFID_AVERAGE_KMS_X2				= "Average Kms X2";
	public static final String RFID_AVERAGE_KMS_Y1				= "Average Kms Y1";
	public static final String RFID_AVERAGE_KMS_Y2				= "Average Kms Y2";
	public static final String RFID_AVERAGE_KMS					= "Average Kms";
	public static final String RFID_IRP 						= "IRP";
	public static final String RFID_ACTUAL_CASH_VALUE 			= "Actual Cash Value";
	public static final String RFID_CLASS 						= "Class";
	public static final String RFID_VRG 						= "VRG";
	public static final String RFID_NUM_OF_VEHICLES 			= "# of Vehicles";
	public static final String RFID_DED 						= "Ded";
	public static final String RFID_MAX_COMMISSION				= "Max Commission";
	public static final String RFID_SELECTED_COMMISSION_RATE	= "Selected Commission Rate";
	public static final String RFID_COMM_ADJUSTMENT_FACTOR 		= "Commission Adjustment Factor";
	public static final String RFID_MODIFICATION_FACTOR 		= "Modification Factor on Policy";
	public static final String RFID_OFFBALFACTOR 				= "OffBalance Factor on Policy";
	public static final String RFID_UW_FLEX 					= "UW Flex for auto coverages";
	public static final String RFID_OPCF38_LIM					= "OPCF38 Limit";
	public static final String  RFID_PREM						= "PREM";
	public static final String  RFID_CLPREM						= "CLPREM";
	public static final String  RFID_CMPREM						= "CMPREM";
	public static final String  RFID_APPREM						= "APPREM";
	public static final String  RFID_SPPREM						= "SPPREM";
	public static final String  RFID_BIPREM						= "BIPREM";
	public static final String  RFID_ABPREM						= "ABPREM";
	public static final String  RFID_IRPREM						= "IRPREM";
	public static final String  RFID_MRCPREM					= "MRCPREM";
	public static final String  RFID_OCIPREM					= "OCIPREM";
	public static final String  RFID_CHMPREM					= "CHMPREM";
	public static final String  RFID_EON20GDED					= "EON20GDED";

	// Values from decision table
	public static final String RFID_BASERATE 			= "BASERATE";
	public static final String RFID_LIMITFACTOR 		= "LIMITFACTOR";
	public static final String RFID_DEDPHYFACTOR		= "DEDPHYFACTOR";
	public static final String RFID_DEDLIABFACTOR		= "DEDLIABFACTOR";
	public static final String RFID_DEDFACTOR 			= "DEDFACTOR";
	public static final String RFID_CLASSFACTOR 		= "CLASSFACTOR";
	public static final String RFID_EXPFACTORZN1 		= "EXPFACTORZN1";
	public static final String RFID_EXPFACTORZN2 		= "EXPFACTORZN2";
	public static final String RFID_EXPFACTORZN3 		= "EXPFACTORZN3";
	public static final String RFID_EXPFACTORZN4 		= "EXPFACTORZN4";
	public static final String RFID_EXPFACTORZN5 		= "EXPFACTORZN5";
	public static final String RFID_EXPFACTORZN6 		= "EXPFACTORZN6";
	public static final String RFID_EXPFACTORZN7 		= "EXPFACTORZN7";
	public static final String RFID_EXPFACTORZN8 		= "EXPFACTORZN8";
	public static final String RFID_EXPFACTORZN9 		= "EXPFACTORZN9";
	public static final String RFID_EXPFACTORZN 		= "EXPFACTORZN";
	public static final String RFID_VRGFACTOR			= "VRGFACTOR";
	public static final String RFID_CLMFREQFACTOR 		= "CLMFREQFACTOR";
	public static final String RFID_YRSINBUSFACTOR 		= "YRSINBUSFACTOR";
	public static final String RFID_DANGMATSFACTPR 		= "DANGMATSFACTPR";
	public static final String RFID_PREVRATEFACTOR 		= "PREVRATEFACTOR";
	public static final String RFID_CCSCOREFACTOR 		= "CCSCOREFACTOR";
	public static final String RFID_AVGKMSX1			= "AVGKMSX1FACTOR";
	public static final String RFID_AVGKMSX2			= "AVGKMSX2FACTOR";
	public static final String RFID_AVGKMSY1			= "AVGKMSY1FACTOR";
	public static final String RFID_AVGKMSY2			= "AVGKMSY2FACTOR";
	public static final String RFID_AVGKMS				= "AVGKMSFACTOR";
	public static final String RFID_VEHTYPEFACTOR 		= "VEHTYPEFACTOR";	
	public static final String RFID_NUMVEHSFACTOR 		= "NUMVEHSFACTOR";
	public static final String RFID_COMMADJFACTOR 		= "COMMADJFACTOR";
	
	// Data Extension
	public static final String DATAEXT_ID_TLL       	= "TLL_Ext";
	public static final String DATAEXT_COLID_TLL_RF	 	= "TLL_RateFactor";
	public static final String DATAEXT_ID_WLL       	= "WLL_Ext";
	public static final String DATAEXT_COLID_WLL_RF 	= "WLL_RateFactor";	
	public static final String DATAEXT_ID_CTC 			= "CTC";
	public static final String DATAEXT_COLID_CTC_REV	= "CTC_Revenue";
	public static final String DATAEXT_COLID_CTC_RF 	= "CTC_RateFactor";
	public static final String DATAEXT_ID_TTC 			= "TTC";
	public static final String DATAEXT_COLID_TTC_Days 	= "TTC_NumDays";
	public static final String DATAEXT_COLID_TTC_SurDisc= "TTC_SurDisc";	
	
	public static final String REINSURER_ECHELON = "ECHELON";
	
	// ESL-1206 to include all cancellation for Pro-rata cancellation
	public static Set<String> PRORATA_CANCELLATION_FLEETBASIS = Stream.of(
						"21B5","21BQ","21BS","21BA","SCHD"
				).collect(Collectors.toCollection(HashSet::new));
	
	public static final String ATTACHDRIVER_TYPE_PRINCIPAL = "PRINCIPAL";
	public static final String ATTACHDRIVER_TYPE_SECONDARY = "SECONDARY";
	
	public static final String DRV_CONVICTION_TYPE_CRIMINAL = "CRIM";
	public static final String DRV_CONVICTION_TYPE_MAJOR 	= "MAJ";
	public static final String DRV_CONVICTION_TYPE_MINOR 	= "MIN";
	
	public static final String CHARAGABLE_CLAIMS_TYPE = "CHRG";
	public static final Set<String> CHARAGABLE_CLAIMS_FAULTRESPONSIBILITIES = Stream.of(
						"50","75","100"
				).collect(Collectors.toCollection(HashSet::new));
	
	public static final String DRV_LICENSE_STATUS_LICENSED = "LIC";
	
	public static final String DRV_LICENSE_CLASS_G = "G";
	
	public static Set<String> ADJUSTMENT_COVERAGES_CGL = Stream.of(
			SUBPCY_COV_CODE_BIPD
	).collect(Collectors.toCollection(HashSet::new));
	
	public static Set<String> ADJUSTMENT_COVERAGES_CARGO = Stream.of(
			SUBPCY_COV_CODE_MTCC
	).collect(Collectors.toCollection(HashSet::new));
}