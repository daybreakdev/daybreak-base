package com.echelon.functions.policy.specialtylines;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.echelon.utils.SpecialtyAutoConstants;

public class SpecialtyAutoPolicyFunctions implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -9213209675424738341L;
	public static SpecialtyAutoPolicyFunctions instance;
	
	public SpecialtyAutoPolicyFunctions() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static SpecialtyAutoPolicyFunctions getInstance() {
		if (instance == null) {
			synchronized (SpecialtyAutoPolicyFunctions.class) {
				instance = new SpecialtyAutoPolicyFunctions();
			}
		}
		
		return instance;
	}

	public boolean isProRataCancellation(String fleetBasis) {
		if (StringUtils.isNotBlank(fleetBasis) &&
			(SpecialtyAutoConstants.PRORATA_CANCELLATION_FLEETBASIS.isEmpty() ||
			 SpecialtyAutoConstants.PRORATA_CANCELLATION_FLEETBASIS.contains(fleetBasis))) {
			return true;
		}
		
		return false;
	}

}
