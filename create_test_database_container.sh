#!/usr/bin/env bash
# Create a MySQL database within a docker container that will be used by the
# tests.

CONTAINER_NAME="daybreak_test_database"
MYSQL_ROOT_PASSWORD="P@ssword"
MAX_WAIT_ATTEMPTS=20
SQL_SCRIPTS=(
    "./DSTARInsDocs/Database/CREATE.SQL"
    "./DSTARInsDocs/Database/CREATE USER DAYSTAR.sql"
    "./DSTARInsDocs/Database/load_CargoValueRate.sql"
    "./DSTARInsDocs/Database/vehiclerategroup_INSERT.sql"
    "./DSTARInsDocs/Database/load_statcodes.sql"
    "./DSTARInsDocs/Database/NumberGenerator_INSERT.sql"
    "./DSTARInsDocs/Database/LoadBrokers.sql"
)

# We need to set the `lower_case_table_names` server system variable to 1.
# This specifies that table names are to be stored in lowercase on disk and that
# comparisons of table names are not case-sensitive.
#
# This is not a problem on Windows hosts, but Linux-based MySQL servers (such as
# the one we've launched inside a Docker container) perform case-sensitive
# matching by default.
#
# See: https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_lower_case_table_names
echo "Creating the database container..."
docker run \
    --detach \
    --name "${CONTAINER_NAME}" \
    --publish 3306:3306 \
    --env MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD}" \
    mysql:5.7 mysqld --lower_case_table_names=1

# Wait for the MySQL server to complete its first-time initialization.
# This takes approximately 7 seconds.
echo "Waiting for MySQL initialization."
echo "${MAX_WAIT_ATTEMPTS} connection attempts will be made..."
while \
    [[ "${MAX_WAIT_ATTEMPTS}" -gt 0 ]] \
    && ! docker exec "${CONTAINER_NAME}" mysql \
        --user root \
        --password="${MYSQL_ROOT_PASSWORD}" \
        --batch \
        --execute 'SELECT 1'
do
    MAX_WAIT_ATTEMPTS=$((MAX_WAIT_ATTEMPTS - 1))
    echo "Connection attempt failed. ${MAX_WAIT_ATTEMPTS} attempts left..."
    sleep 1
done

# If this is true, we exhausted all wait attempts without success.
# Print a failure message to stderr, display the database container logs to help
# debug the issue, and exit with a non-zero (error) status.
if [ "${MAX_WAIT_ATTEMPTS}" -le 0 ]
then
    echo >&2 "Error: database initialization appears to have failed"
    docker logs "${CONTAINER_NAME}"
    exit 1
fi

# Create the `daystar` database.
echo "MySQL initialization successful. Seeding database..."
echo "Creating daystar database"
docker exec "${CONTAINER_NAME}" mysql \
    --batch \
    --user root \
    --password="${MYSQL_ROOT_PASSWORD}" \
    --execute 'CREATE DATABASE daystar'

# Tests are not run on localhost; they're run in another Docker container.
# As such, we need to create a 'daystar' user that will be allowed to connect
# from that subnet. (Note that while the SQL scripts create a 'daystar' user,
# that user is specified only for connections from 'localhost').
#
# We specify the host as an IPv4 address, including a netmask.
#
# See: https://dev.mysql.com/doc/refman/5.7/en/account-names.html
echo "Creating daystar user on local subnet"
docker exec "${CONTAINER_NAME}" mysql \
    --batch \
    --user root \
    --password="${MYSQL_ROOT_PASSWORD}" \
    --execute "CREATE USER 'daystar'@'172.16.0.0/255.240.0.0' IDENTIFIED BY 'd4yst4r'"
docker exec "${CONTAINER_NAME}" mysql \
    --batch \
    --user root \
    --password="${MYSQL_ROOT_PASSWORD}" \
    --execute "GRANT ALL ON daystar.* TO 'daystar'@'172.16.0.0/255.240.0.0'"

# Run the database initialization scripts in order.
# Each script is piped into the docker exec command, which runs the SQL script
# inside the `daystar` database.
for SCRIPT in "${SQL_SCRIPTS[@]}"
do
    echo "Running script ${SCRIPT}"
    cat "${SCRIPT}" | docker exec --interactive "${CONTAINER_NAME}" mysql \
        --batch \
        --user root \
        --password="${MYSQL_ROOT_PASSWORD}" \
        --database daystar
done

echo "Database successfully seeded."
