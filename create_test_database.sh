#!/usr/bin/env bash

# Create a MySQL database within a Docker container that can be used by the
# integration tests.

#################################################
# Variable definitions
#################################################
CONTAINER="testdb"
MAX_TRIES=10 # Number of database connection attempts
SLEEP_DURATION=3 # Seconds to wait between database connection attempts

MYSQL_ROOT_PASSWORD="P@ssword"
DB_USER='daystar'
DB_PASS='d4yst4r'
DB_SUBNET='172.16.0.0/255.240.0.0' # Subnet used by Bitbucket pipelines

SQL_SCRIPTS=(
    "CREATE.SQL"
    "CREATE USER DAYSTAR.SQL"
    "vehiclerategroup_INSERT.sql"
    "LHTvehiclerategroup_INSERT.sql"
    "load_statcodes.sql"
    "load_CargoValueRate.sql"
    "LoadBrokers.sql"
    "ESL-1284_Load LHT Brokers.sql"
    "ESL-1676_Update TO Brokers_MS.sql"
    "NumberGenerator_INSERT.sql"
    "NumberGeneratorLHT_INSERT.sql"
    "ESL-1538_Update_LHT_Policy_Number_Sequence.sql"
    "ESL-1875_Load Reinsurers.sql"
)

#################################################
# Function definitions
#################################################
# Executes a SQL script
exec_script()
{
    cat "$1" | docker exec --interactive "${CONTAINER}" mysql \
        --defaults-extra-file=/root/config.cnf \
        --batch \
        --database=daystar
}

# Executes a SQL command which is provided as a string
exec_sql()
{
    docker exec "${CONTAINER}" mysql \
        --defaults-extra-file=/root/config.cnf \
        --batch \
        --execute="$*"
}

#################################################
# Script body
#################################################

# Start by creating the container.
#
# We need to set the `lower_case_table_names` server system variable to 1. This
# Specifies that table names are to be stored in lowercase on disk, and that
# comparisons of table names are case-insensitive.
#
# This is not a problem on Windows hosts, but Linux-based MySQL servers perform
# case-sensitive matching by default (this is perhaps related to whether or not
# the underlying filesystem is case-sensitive).
#
# See: https://dev.mysql.com/doc/refman/5.7/en/server-system-variables.html#sysvar_lower_case_table_names
echo "|> Creating the database container"
docker run \
    --detach \
    --name "${CONTAINER}" \
    --publish 3306:3306 \
    --env MYSQL_ROOT_PASSWORD="${MYSQL_ROOT_PASSWORD}" \
    mysql:5.7 mysqld --lower_case_table_names=1

# This is a bit tricky. MySQL doesn't like having the password specified on the command line, but
# obviously we can't provide it interactively. Workaround is to create a config file specifying the
# username and password, and then use that when authenticating.
# We print the file contents, pipe it to the docker exec command, and use `dd` to copy from stdin
# to /root/config.cnf.
#
# Using dd feels a bit hacky, but it was the easiest thing I could find to copying stdin to a file.
echo "|> Creating the MySQL login config file"
printf "[client]\nuser = 'root'\npassword='${MYSQL_ROOT_PASSWORD}'\n" \
    | docker exec --interactive "${CONTAINER}" dd of=/root/config.cnf 2> /dev/null

# MySQL takes a bit of time before it's ready to accept connections. We need to delay until the
# server is up.
#
# We wait a maximum of $MAX_TRIES * $SLEEP_DURATION seconds
echo -e "|> Waiting for database server initialization"
NUM_TRIES=1
while true; do
    if [[ ${NUM_TRIES} -gt ${MAX_TRIES} ]]; then
        >&2 echo "ERROR: could not connect to database. Aborting..."
        exit 1
    fi

    exec_sql "SELECT 1;" > /dev/null
    if [[ $? -eq 0 ]]; then
        echo -e "\tSuccessfully connected to database"
        break
    else
        echo -e "\tCould not connect to database. Attempt ${NUM_TRIES} of ${MAX_TRIES}"
        NUM_TRIES=$((NUM_TRIES + 1))
        sleep ${SLEEP_DURATION}
    fi
done

# Next we need to create the database
echo "|> Creating database"
exec_sql "CREATE DATABASE daystar;"

# Create the user.
# There's a script to create the user, but it creates it only on localhost. This is fine for local
# development, but when we're working with Docker we need a user that can log in remotely.
# Accordingly, we'll create a user that can log in from the local subnet.
echo "|> Creating user ${DB_USER} on subnet ${DB_SUBNET}"
exec_sql "CREATE USER '${DB_USER}'@'${DB_SUBNET}' IDENTIFIED BY '${DB_PASS}';"
exec_sql "GRANT ALL ON daystar.* TO '${DB_USER}'@'${DB_SUBNET}';"

echo "|> Seeding database"
for SCRIPT in "${SQL_SCRIPTS[@]}"; do
    SCRIPT="./EchelonDocs/Database/${SCRIPT}"
    echo -e "\t|> Running script ${SCRIPT}"
    exec_script "${SCRIPT}"
done

echo "|> Database initialization complete"
