package com.echelon.services.coverage.LHT;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ObjectUtils;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.coverage.SubPolicyCoverageService;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.utils.LHTConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class LHTSubPolicyCoverageService extends SubPolicyCoverageService {
	final static Logger logger = Logger.getLogger(LHTSubPolicyCoverageService.class.getName());
			
	private LHTCoverageServiceHelper covServiceHelper;

	public LHTSubPolicyCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super(policyServicesFactory);
		// ESL-2050
		// the super class use this variable
		coverageServiceHelper = policyServicesFactory.newCoverageServiceHelper();
		
		covServiceHelper = (LHTCoverageServiceHelper) policyServicesFactory.newCoverageServiceHelper();
	}

	@Override
	public Coverage setCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Coverage coverage) {
		coverage = super.setCoverageDefault(transaction, subPolicy, coverage);
		switch (subPolicy.getSubPolicyTypeCd()) {
			case LHTConstants.SUBPCY_CODE_CARGO:
				applyCargoCoverageChanges((CargoSubPolicy<?>) subPolicy, coverage);
				break;
				
			default:
				break;
		}
		try {
			if (ObjectUtils.compare(covServiceHelper.getCoverageSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
				CoverageConfig covConfig = getCoverageConfig(transaction, subPolicy, coverage);
				if (covConfig != null) {
					if (covConfig.isReinsurable()) {
						coverage.setReinsured(Boolean.TRUE);
					}
				}
			}
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTSubPolicyCoverageService::Could not set coverage default", e);
		}
		return coverage;
	}

	@Override
	public Endorsement setEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Endorsement endorsement) {
		endorsement = super.setEndorsementDefault(transaction, subPolicy, endorsement);
		switch (subPolicy.getSubPolicyTypeCd()) {
			case LHTConstants.SUBPCY_CODE_CARGO:
				applyCargoEndorsementChanges((CargoSubPolicy<?>) subPolicy, endorsement);
				break;
			
			default:
				break;
		}
		try {
			if (ObjectUtils.compare(covServiceHelper.getEndorsementSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
				EndorsementConfig endConfig = getEndorsementConfig(transaction, subPolicy, endorsement);
				if (endConfig != null) {
					if (endConfig.isReinsurable()) {
						endorsement.setReinsured(Boolean.TRUE);
					}
				}
			}
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTSubPolicyCoverageService::Could not set endorsement default", e);
		}
		return endorsement;
	}

	@Override
	protected Endorsement setEndorsementCTCDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Endorsement endorsement) {
		// TODO Auto-generated method stub
		return endorsement;
	}

	private void applyCargoEndorsementChanges(CargoSubPolicy<?> cargoSubPolicy, Endorsement endorsement) {
		if (LHTConstants.SUBPCY_END_CODE_CTC.equalsIgnoreCase(endorsement.getEndorsementCd())) {
			
			//ESL-2002
			if (cargoSubPolicy.getCargoLimit() != null) {
				if (endorsement.getEndorsementsk() == null)
				{
					endorsement.setLimit1amount(cargoSubPolicy.getCargoLimit());
				}
			}
			
			if (cargoSubPolicy.getCargoDeductible() != null) {
				if (endorsement.getEndorsementsk() == null)
				{
					endorsement.setDeductible1amount(cargoSubPolicy.getCargoDeductible());
				}
			}
		}
	}
	
	
	//ESL-2002
	private void applyCargoCoverageChanges(CargoSubPolicy<?> cargoSubPolicy, Coverage coverage) {
		if (SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC.equalsIgnoreCase(coverage.getCoverageCode())) {
			if (cargoSubPolicy.getCargoLimit() != null) {
				if (coverage.getCoveragePK() == null)
				{
					coverage.setLimit1(cargoSubPolicy.getCargoLimit());
				}
			}
			if (cargoSubPolicy.getCargoDeductible() != null) {
				if (coverage.getCoveragePK() == null)
				{
					coverage.setDeductible1(cargoSubPolicy.getCargoDeductible());
				}
			}
		}
	}


	private CoverageConfig getCoverageConfig(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Coverage cov) throws InsurancePolicyException {
		CoverageConfig rv = null;
		if (transaction != null && cov != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the coverage config", e);
			}
			if (productConfig != null) {
				SubPolicyConfig subPcyConfig = productConfig.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
				if (subPcyConfig != null) {
					rv = subPcyConfig.getCoverageConfig(cov.getCoverageCode());
				}
			}			
		}
		return rv;
	}
	
	private EndorsementConfig getEndorsementConfig(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Endorsement end) throws InsurancePolicyException {
		EndorsementConfig rv = null;
		if (transaction != null && end != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the endorsement config", e);
			}
			if (productConfig != null) {
				SubPolicyConfig subPcyConfig = productConfig.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
				if (subPcyConfig != null) {
					rv = subPcyConfig.getEndorsementConfig(end.getEndorsementCd());
				}
			}			
		}
		return rv;
	}
}
