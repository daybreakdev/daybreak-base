package com.echelon.services.coverage;

import java.util.List;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class SubPolicyCoverageService {
	private Integer totalCGLVehNumOfUnits;
	private Integer totalCARVehNumOfUnits;
	
	protected CoverageServiceHelper coverageServiceHelper;
	
	public SubPolicyCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super();
		
		this.coverageServiceHelper = policyServicesFactory.newCoverageServiceHelper();
	}

	public Coverage setCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Coverage coverage) {
		calcNumOfUnits(transaction, subPolicy, coverage);
		
		switch (subPolicy.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			applyCGLDeductibleChanged((CGLSubPolicy<?>) subPolicy, coverage);
			break;
			
		default:
			break;
		}
		
		return coverage;
	}

	public Coverage setCoverageValues(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Coverage coverage) {
		calcNumOfUnits(transaction, subPolicy, coverage);
		
		return coverage;
	}
	
	public Endorsement setEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Endorsement endorsement) {
		switch (endorsement.getEndorsementCd()) {
		case SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_AIC:
			CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) subPolicy;
			endorsement.setLimit1amount(cglSubPolicy.getCglLimit());
			endorsement.setDeductible1amount(cglSubPolicy.getCglDeductible());
			break;
			
		case SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_AICC:
			Coverage mtccCov = subPolicy.getCoverageDetail().getCoverageByCode(SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC);
			if (mtccCov != null) {
				endorsement.setLimit1amount(mtccCov.getLimit1());
				endorsement.setDeductible1amount(mtccCov.getDeductible1());
			}			
			break;
		
		//ESL-2002
		case SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC:
			//Change CTC endorsement based on MTCC coverage *towing only*
			setEndorsementCTCDefault(transaction, subPolicy, endorsement);
			break;
			
		default:
			break;
		}
		return endorsement;
	}
	
	protected Endorsement setEndorsementCTCDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Endorsement endorsement) {
		if (transaction.getPolicyVersion().getInsurancePolicy().getProductCd().equalsIgnoreCase(ConfigConstants.PRODUCTCODE_TOWING)) {
			Coverage mtccCov = subPolicy.getCoverageDetail().getCoverageByCode(SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC);
			if (mtccCov != null) {
				endorsement.setLimit1amount(mtccCov.getLimit1());
			}
		}
		
		return endorsement;
	}

	public void applyCGLDeductibleChanged(CGLSubPolicy<?> cglSubPolicy, Coverage coverage) {
		if (SpecialtyAutoConstants.SUBPCY_COV_CODE_TLL.equalsIgnoreCase(coverage.getCoverageCode())) {
			if (cglSubPolicy.getCglDeductible() != null) {
				coverage.setDeductible1(cglSubPolicy.getCglDeductible());
			}
		}
	}
	
	protected void calcNumOfUnits(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Coverage coverage) {
		switch (subPolicy.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			if (SpecialtyAutoConstants.CGL_UNITS_COVERAGS.contains(coverage.getCoverageCode())) {
				if (totalCGLVehNumOfUnits == null) {
					totalCGLVehNumOfUnits = calcCglTotalVehicleNumOfUnits(transaction); 
				}
				coverage.setNumOfUnits(totalCGLVehNumOfUnits);
				if (coverage.getCoveragePremium() != null) {
					//coverage.getCoveragePremium().setFlatPremium(false);
				}
			}
			else {
				coverage.setNumOfUnits(null);
			}
			
			break;

		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			if (SpecialtyAutoConstants.CARGO_LOC_COVERAGES.contains(coverage.getCoverageCode())) {
				if (totalCARVehNumOfUnits == null) {
					totalCARVehNumOfUnits = calcCargoTotalVehicleNumOfUnits(transaction); 
				}
				coverage.setNumOfUnits(totalCARVehNumOfUnits);
				if (coverage.getCoveragePremium() != null) {
					//coverage.getCoveragePremium().setFlatPremium(true);
				}
			}
			else {
				coverage.setNumOfUnits(null);
			}
			
			break;

		default:
			break;
		}
	}
	
	protected Integer calcCglTotalVehicleNumOfUnits(PolicyTransaction<?> transaction) {
		Integer totNumOfUnits = 0;
		List<Risk> risks = coverageServiceHelper.getActiveVehicles(transaction);
		if (!risks.isEmpty()) {
			for(Risk risk : risks) {
				SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
				if (!coverageServiceHelper.isVehicleCGLExcluded(vehRisk)) {
					totNumOfUnits += vehRisk.getNumberOfUnits();
				}
			}
		}
				
		return totNumOfUnits;
	}
	
	protected Integer calcCargoTotalVehicleNumOfUnits(PolicyTransaction<?> transaction) {
		Integer totNumOfUnits = 0;
		List<Risk> risks = coverageServiceHelper.getActiveVehicles(transaction);
		if (!risks.isEmpty()) {
			for(Risk risk : risks) {
				SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
				if (!coverageServiceHelper.isVehicleCargoExcluded(vehRisk)) {
					totNumOfUnits += vehRisk.getNumberOfUnits();
				}
			}
		}
				
		return totNumOfUnits;
	}
}
