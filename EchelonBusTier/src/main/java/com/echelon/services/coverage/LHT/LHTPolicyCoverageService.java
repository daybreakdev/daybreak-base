package com.echelon.services.coverage.LHT;

import org.apache.commons.lang3.ObjectUtils;

import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;

public class LHTPolicyCoverageService {
	private LHTCoverageServiceHelper covServiceHelper;
	
	public LHTPolicyCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super();
		covServiceHelper = (LHTCoverageServiceHelper) policyServicesFactory.newCoverageServiceHelper();
	}

	public Coverage setCoverageDefault(PolicyTransaction<?> transaction, Coverage cov) throws InsurancePolicyException {
		if (ObjectUtils.compare(covServiceHelper.getCoverageSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
			CoverageConfig covConfig = getCoverageConfig(transaction, cov);
			if (covConfig != null) {
				if (covConfig.isReinsurable()) {
					cov.setReinsured(Boolean.TRUE);
				}
			}
		}
		return cov;
	}
	
	public Endorsement setEnorsementDefault(PolicyTransaction<?> transaction, Endorsement end) throws InsurancePolicyException {
		if (ObjectUtils.compare(covServiceHelper.getEndorsementSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
			EndorsementConfig endConfig = getEndorsementConfig(transaction, end);
			if (endConfig != null) {
				if (endConfig.isReinsurable()) {
					end.setReinsured(Boolean.TRUE);
				}
			}
		}
		return end;
	}
	
	private CoverageConfig getCoverageConfig(PolicyTransaction<?> transaction, Coverage cov) throws InsurancePolicyException {
		CoverageConfig rv = null;
		if (transaction != null && cov != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the coverage config", e);
			}
			if (productConfig != null) {
				rv = productConfig.getCoverageConfig(cov.getCoverageCode());
			}			
		}
		return rv;
	}
	
	private EndorsementConfig getEndorsementConfig(PolicyTransaction<?> transaction, Endorsement end) throws InsurancePolicyException {
		EndorsementConfig rv = null;
		if (transaction != null && end != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the endorsement config", e);
			}
			if (productConfig != null) {
				rv = productConfig.getEndorsementConfig(end.getEndorsementCd());
			}			
		}
		return rv;
	}
}
