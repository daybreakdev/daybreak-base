package com.echelon.services.coverage;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.LimitConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class SubPolicyRiskCoverageService {

	public SubPolicyRiskCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super();
		// TODO Auto-generated constructor stub
	}

	public Endorsement setEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Endorsement endorsement) {
		if (SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF44R.equalsIgnoreCase(endorsement.getEndorsementCd())) {
			SpecialtyAutoPackage specialtyAutoPackage = (SpecialtyAutoPackage) transaction.getPolicyVersion().getInsurancePolicy();
			if (specialtyAutoPackage.getLiabilityLimit() != null &&
				specialtyAutoPackage.getLiabilityLimit().longValue() > 1000000) {
				endorsement.setLimit1amount(2000000);
			}
			else {
				endorsement.setLimit1amount(1000000);
			}
		}
		else if (SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B.equalsIgnoreCase(endorsement.getEndorsementCd())) {
			if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
				SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk)risk;
				if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
					Coverage apCoverage = risk.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP);
					if (apCoverage != null) {
						endorsement.setDeductible1amount(apCoverage.getDeductible1());
					}
					
					// Check if the LPN is one of the valid values in limit config
					if (vehicle.getUnitLPN() != null) {
						try {
							Integer defaultLimit = vehicle.getUnitLPN().intValue();
							EndorsementConfig endorsementConfig = getEndorsementConfig(transaction, subPolicy, risk, endorsement);
							if (endorsementConfig != null && endorsementConfig.getLimitConfigs() != null && !endorsementConfig.getLimitConfigs().isEmpty()) {
								boolean foundValidValue = false;
								for(LimitConfig limitConfig : endorsementConfig.getLimitConfigs()) {
									if (StringUtils.isNotBlank(limitConfig.getValidValues())) {
										String[] validValues = limitConfig.getValidValues().split(ConfigConstants.LIMITCONFIGVALUES_SEPARAOTR);
										for(String validValue : validValues) {
											try {
												Integer validIntValue = Integer.valueOf(validValue.trim().replace(",", ""));
												if (validIntValue.intValue() == defaultLimit.intValue()) {
													foundValidValue = true;
													break;
												}
											} catch (Exception e) {
												// TODO: handle exception
											}
										}
									}
									
									if (foundValidValue) {
										break;
									}
								}
								
								if (!foundValidValue) {
									defaultLimit = null;
								}
							}
							
							endorsement.setLimit1amount(defaultLimit);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					/*
					 * ESL-1829 - 
					 * Currently, when SPEC_VEH_RISK_DESC_OPCF27B = �OP27�, we populate vehicle type to default to  OPCF27B. 
					 * Please remove this. Let it stay blank. Add a warning message indicating to the user to specify vehicle type for OPCF27B. 
					 * You can add to the existing message to remove all coverages except Ap/Comp & Coll.
					 *
					DataExtension dataExtension  = endorsement.getDataExtension();
					EndorsementConfig endorsementConfig = null;
					// this should exist because the caller has the config
					try {
						endorsementConfig = getEndorsementConfig(transaction, subPolicy, risk, endorsement);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if (endorsementConfig != null) {
						dataExtension = PolicyServicesFactory.getInstance().newCoverageDataExtensionService()
								.createDataExtensionDataDefault(endorsementConfig, endorsement.getDataExtension(), 
																SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_ID_27B, 
																SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHTYPE);
						
						if (dataExtension != null) {
							ExtensionData extensionData = dataExtension.findDataExtensionData(
																SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_ID_27B, 
																SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHTYPE);
							if (extensionData != null) {
								extensionData.setColumnValue(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_VEHTYPE_OPCF278);
							}
							
							endorsement.setDataExtension(dataExtension);
						}
					}
					*/
				}
			}
		}
		
		return endorsement;
	}

	private EndorsementConfig getEndorsementConfig(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Endorsement endorsement) throws Exception {
		EndorsementConfig endorsementConfig = null;
		
		IGenericProductConfig productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
															transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
															transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
															transaction.getPolicyTerm().getTermEffDate().toLocalDate());
		if (productConfig != null) {
			SubPolicyConfig subPolicyConfig = productConfig.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
			if (subPolicyConfig != null) {
				RiskConfig riskConfig = subPolicyConfig.getRiskType(risk.getRiskType());
				if (riskConfig != null) {
					endorsementConfig = riskConfig.getEndorsementConfig(endorsement.getEndorsementCd());
				}
			}
		}
		
		return endorsementConfig;
	}
}
