package com.echelon.services.coverage;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.coverage.DSCoverageService;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.services.policy.PolicyServicesFactory;

public class CoverageService extends DSCoverageService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1590617682077074611L;

	public CoverageService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return PolicyServicesFactory.getInstance();
	}
	
	// ESL-2050
	// For class to use the right SubPolicyCoverageService
	public SubPolicyCoverageService newSubPolicyCoverageService() {
		return new SubPolicyCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}
	
	protected SubPolicyRiskCoverageService newSubPolicyRiskCoverageService() {
		return new SubPolicyRiskCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}

	@Override
	public Coverage setSubpolicyCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Coverage coverage) {
		// TODO Auto-generated method stub
		return newSubPolicyCoverageService().setCoverageDefault(transaction, subPolicy, coverage);
	}

	@Override
	public Coverage setSubpolicyCoverageValues(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Coverage coverage) {
		// TODO Auto-generated method stub
		return newSubPolicyCoverageService().setCoverageValues(transaction, subPolicy, coverage);
	}

	@Override
	public Endorsement setSubpolicyEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Endorsement endorsement) {
		// TODO Auto-generated method stub
		return newSubPolicyCoverageService().setEndorsementDefault(transaction, subPolicy, endorsement);
	}

	@Override
	public Endorsement setSubPolicyRiskEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Risk risk, Endorsement endorsement) {
		// TODO Auto-generated method stub
		return newSubPolicyRiskCoverageService().setEndorsementDefault(transaction, subPolicy, risk, endorsement);
	}

}
