package com.echelon.services.coverage.LHT;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.utils.LHTConstants;

@SuppressWarnings("serial")
public class LHTCoverageServiceHelper extends CoverageServiceHelper {
	
	public LHTCoverageServiceHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	private int getPolicyMaxRetainedLimit(PolicyTransaction<?> pcyTxn, IGenericProductConfig prodConfig) {
		/*
		 * This code is intended to be temporary. The intent is to allow LHT ON policies
		 * issued using the old (v1) ratebook to use the old maxRetainedLimit value of
		 * 2,000,000. Policies issued using v2 will use the current value of 5M instead.
		 * 
		 * See ES-106 for further information.
		 */
		if ("ONLHT".equalsIgnoreCase(pcyTxn.getPolicyTerm().getRatebookId())
				&& Objects.equals(1, pcyTxn.getPolicyTerm().getRatebookVersion())) {
			return 2_000_000;
		}
		// End of ES-106 "maxRetainedLimit override" code

		return prodConfig.getMaxRetainedLimit();
	}

	public Integer getProductMaxRetainedLimit(PolicyTransaction<?> pcyTxn) {
		Integer rv = null;
		IGenericProductConfig productConfig = null;
		try {
			productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", pcyTxn.getPolicyVersion().getInsurancePolicy().getProductCd(), 
					pcyTxn.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
					pcyTxn.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
					pcyTxn.getPolicyTerm().getTermEffDate().toLocalDate());
		} catch (Exception e) {
			
		}
		if (productConfig != null) {
			rv = getPolicyMaxRetainedLimit(pcyTxn, productConfig);
		}
		return rv;
	}

	@Override
	public Integer getCoverageSystemControlledLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return super.getCoverageSystemControlledLimit(pcyTxn);
	}

	@Override
	public Integer getEndorsementSystemControlledLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return super.getEndorsementSystemControlledLimit(pcyTxn);
	}

	@Override
	public boolean isVehicleCGLExcluded(SpecialtyVehicleRisk vehRisk) {
		// TODO Auto-generated method stub
		return !(StringUtils.isNotBlank(vehRisk.getTypeOfUnit()) &&
				 LHTConstants.LHT_CGL_COVUNITS_BASEON_INCLTYPEUNITS.contains(vehRisk.getTypeOfUnit().toUpperCase()) &&
				 StringUtils.isNotBlank(vehRisk.getVehicleUse()) &&
				 LHTConstants.LHT_CGL_COVUNITS_BASEON_INCLVEHUSE.contains(vehRisk.getVehicleUse().toUpperCase()));
	}

	@Override
	public boolean isVehicleCargoExcluded(SpecialtyVehicleRisk vehRisk) {
		// TODO Auto-generated method stub
		return !(LHTConstants.LHT_VEH_RISK_VEHICLE_USE.equalsIgnoreCase(vehRisk.getVehicleUse())
				 && 
				 LHTConstants.LHT_VEH_RISK_TYPEOFUNIT.equalsIgnoreCase(vehRisk.getTypeOfUnit()));
	}
	
	protected boolean isVehicleGroupUnitRateIncludeCARGO(SpecialtyVehicleRisk vehRisk) {
		return !isVehicleCargoExcluded(vehRisk);
	}	
	
}
