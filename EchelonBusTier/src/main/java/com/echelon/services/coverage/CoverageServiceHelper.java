package com.echelon.services.coverage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.services.coverage.DSCoverageServiceHelper;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.services.policy.PolicyServicesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class CoverageServiceHelper extends DSCoverageServiceHelper {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6385861957108320372L;

	public CoverageServiceHelper() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return PolicyServicesFactory.getInstance();
	}

	@Override
	protected Integer getCoverageSystemControlledLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return getPolicyLiabilityLimit(pcyTxn);
	}

	@Override
	protected Integer getEndorsementSystemControlledLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return getPolicyLiabilityLimit(pcyTxn);
	}

	protected Integer getPolicyLiabilityLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		Integer rv = null;
		if (pcyTxn != null && pcyTxn.getPolicyVersion() != null && pcyTxn.getPolicyVersion().getInsurancePolicy() != null) {
			SpecialtyAutoPackage pcy = (SpecialtyAutoPackage) pcyTxn.getPolicyVersion().getInsurancePolicy();
			rv = pcy.getLiabilityLimit();	
		}
		return rv;
	}

	public boolean isVehicleCGLExcluded(SpecialtyVehicleRisk vehRisk) {
		return isVehicleExcluded(vehRisk, SpecialtyAutoConstants.CGL_COVUNITS_BASEON_EXCLVEHTYPENDESC);
	}

	public boolean isVehicleCargoExcluded(SpecialtyVehicleRisk vehRisk) {
		return StringUtils.isBlank(vehRisk.getVehicleUse()) || 
				!SpecialtyAutoConstants.CARGO_COVUNITS_BASEON_VEHUSE.contains(vehRisk.getVehicleUse()) ||
				isVehicleExcluded(vehRisk, SpecialtyAutoConstants.CARGO_COVUNITS_BASEON_EXCLVEHTYPENDESC);
	}

	protected boolean isVehicleExcluded(SpecialtyVehicleRisk vehRisk, List<String[]> exclList) {
		// Match exclusion
		boolean match = false;
		
		for(String[] exclVehTypeDesc : exclList) {
			String exclVehType = exclVehTypeDesc[0];
			String exclVehDesc = exclVehTypeDesc[1];
			
			if ((exclVehType.trim().isEmpty() || exclVehType.equalsIgnoreCase(vehRisk.getVehicleType()))
				&&
				(exclVehDesc.trim().isEmpty() || exclVehDesc.equalsIgnoreCase(vehRisk.getVehicleDescription()))
				) {
				match = true;	
			}
		}

		return match;
	}
	
	public List<Risk> getAllVehicles(PolicyTransaction<?> transaction) {
		return getVehicles(transaction, false);
	}
	
	public List<Risk> getActiveVehicles(PolicyTransaction<?> transaction) {
		return getVehicles(transaction, true);
	}
	
	@SuppressWarnings("unchecked")
	private List<Risk> getVehicles(PolicyTransaction<?> transaction, boolean isActiveOnly) {
		List<Risk> risks = new ArrayList<Risk>();
		
		SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) transaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (subPolicy != null && subPolicy.getSubPolicyRisks() != null) {
			risks = subPolicy.getSubPolicyRisks().stream()
					.filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(o.getRiskType()) &&
							     (!isActiveOnly || (!o.isDeleted() && !o.wasDeleted())))
					.collect(Collectors.toList());
		}
		
		return risks;
	}
	
	@SuppressWarnings("unchecked")
	public boolean isCalcVehicleGroupUnitRate(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, String forSubPolicyCode) {
		boolean isOk = false;
		
		if (SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
			
			// ESL-384 If Fleet Basis is Scheduled (SCHD) do not calculate Unit Rate by adding CGL remium
			isOk = !SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED.equalsIgnoreCase(autoSubPolicy.getFleetBasis())
					//ES-156 correction
					//!isVehicleCGLExcluded(vehRisk)
					//ESL-252
					//&& Constants.VERSION_STATUS_ISSUED.equalsIgnoreCase(transaction.getPolicyVersion().getBusinessStatus())
					;
			if (isOk) {
				// Determine by next condition
				isOk = false;
				if (StringUtils.isNotBlank(forSubPolicyCode)) {
					switch (forSubPolicyCode) {
					case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
						isOk = isVehicleGroupUnitRateIncludeCGL(vehRisk);
						break;
						
					case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
						isOk = isVehicleGroupUnitRateIncludeCARGO(vehRisk);
						break;
						
					default:
						break;
					}
				}
			}
		}
		
		return isOk;
	}
	
	protected boolean isVehicleGroupUnitRateIncludeCGL(SpecialtyVehicleRisk vehRisk) {
		return !isVehicleCGLExcluded(vehRisk);
	}
	
	protected boolean isVehicleGroupUnitRateIncludeCARGO(SpecialtyVehicleRisk vehRisk) {
		return false; // For LHT only
	}
}
