package com.echelon.services.coverage.LHT;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.apache.commons.lang3.ObjectUtils;

import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.coverage.SubPolicyRiskCoverageService;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;

public class LHTSubPolicyRiskCoverageService extends SubPolicyRiskCoverageService {
	final static Logger logger = Logger.getLogger(LHTSubPolicyRiskCoverageService.class.getName());
			
	private LHTCoverageServiceHelper covServiceHelper;

	public LHTSubPolicyRiskCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super(policyServicesFactory);
		
		covServiceHelper = (LHTCoverageServiceHelper) policyServicesFactory.newCoverageServiceHelper();
	}
	
	public Coverage setCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Coverage coverage) {
		try {
			if (ObjectUtils.compare(covServiceHelper.getCoverageSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
				CoverageConfig covConfig = getCoverageConfig(transaction, subPolicy, risk, coverage);
				if (covConfig != null) {
					if (covConfig.isReinsurable()) {
						coverage.setReinsured(Boolean.TRUE);
					}
				}
			}
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTSubPolicyRiskCoverageService::Could not set coverage default", e);
		}
		return coverage;
	}

	@Override
	public Endorsement setEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk,
			Endorsement endorsement) {
		endorsement = super.setEndorsementDefault(transaction, subPolicy, risk, endorsement);
		try {
			if (ObjectUtils.compare(covServiceHelper.getEndorsementSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
				EndorsementConfig endConfig = getEndorsementConfig(transaction, subPolicy, risk, endorsement);
				if (endConfig != null) {
					if (endConfig.isReinsurable()) {
						endorsement.setReinsured(Boolean.TRUE);
					}
				}
			}
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTSubPolicyRiskCoverageService::Could not set endorsement default", e);
		}
		return endorsement;
	}

	private CoverageConfig getCoverageConfig(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Coverage cov) throws InsurancePolicyException {
		CoverageConfig rv = null;
		if (transaction != null && cov != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the coverage config", e);
			}
			if (productConfig != null) {
				SubPolicyConfig subPcyConfig = productConfig.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
				if (subPcyConfig != null) {
					RiskConfig riskConfig = subPcyConfig.getRiskType(risk.getRiskType());
					if (riskConfig != null) {
						rv = riskConfig.getCoverageConfig(cov.getCoverageCode());
					}
				}
			}			
		}
		return rv;
	}
	
	private EndorsementConfig getEndorsementConfig(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Endorsement end) throws InsurancePolicyException {
		EndorsementConfig rv = null;
		if (transaction != null && end != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the endorsement config", e);
			}
			if (productConfig != null) {
				SubPolicyConfig subPcyConfig = productConfig.getSubPolicyConfig(subPolicy.getSubPolicyTypeCd());
				if (subPcyConfig != null) {
					RiskConfig riskConfig = subPcyConfig.getRiskType(risk.getRiskType());
					if (riskConfig != null) {
						rv = riskConfig.getEndorsementConfig(end.getEndorsementCd());
					}
				}
			}			
		}
		return rv;
	}
}
