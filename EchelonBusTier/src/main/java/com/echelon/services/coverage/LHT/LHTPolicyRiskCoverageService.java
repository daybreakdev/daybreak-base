package com.echelon.services.coverage.LHT;

import org.apache.commons.lang3.ObjectUtils;

import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.RiskConfig;

public class LHTPolicyRiskCoverageService {
	private LHTCoverageServiceHelper covServiceHelper;

	public LHTPolicyRiskCoverageService(IPolicyServicesFactory policyServicesFactory) {
		super();
		covServiceHelper = (LHTCoverageServiceHelper) policyServicesFactory.newCoverageServiceHelper();
	}
	
	public Coverage setCoverageDefault(PolicyTransaction<?> transaction, Risk risk, Coverage coverage) throws InsurancePolicyException {
		if (ObjectUtils.compare(covServiceHelper.getCoverageSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
			CoverageConfig covConfig = getCoverageConfig(transaction, risk, coverage);
			if (covConfig != null) {
				if (covConfig.isReinsurable()) {
					coverage.setReinsured(Boolean.TRUE);
				}
			}
		}
		return coverage;
	}
	
	public Endorsement setEndorsementDefault(PolicyTransaction<?> transaction, Risk risk, Endorsement endorsement) throws InsurancePolicyException {
		if (ObjectUtils.compare(covServiceHelper.getEndorsementSystemControlledLimit(transaction), covServiceHelper.getProductMaxRetainedLimit(transaction)) > 0) {
			EndorsementConfig endConfig = getEndorsementConfig(transaction, risk, endorsement);
			if (endConfig != null) {
				if (endConfig.isReinsurable()) {
					endorsement.setReinsured(Boolean.TRUE);
				}
			}
		}
		return endorsement;
	}
	
	private CoverageConfig getCoverageConfig(PolicyTransaction<?> transaction, Risk risk, Coverage cov) throws InsurancePolicyException {
		CoverageConfig rv = null;
		if (transaction != null && cov != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the coverage config", e);
			}
			if (productConfig != null) {
				RiskConfig riskConfig = productConfig.getRiskType(risk.getRiskType());
				if (riskConfig != null) {
					rv = riskConfig.getCoverageConfig(cov.getCoverageCode());
				}
			}			
		}
		return rv;
	}
	
	private EndorsementConfig getEndorsementConfig(PolicyTransaction<?> transaction, Risk risk, Endorsement end) throws InsurancePolicyException {
		EndorsementConfig rv = null;
		if (transaction != null && end != null) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", transaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						transaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						transaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						transaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				throw new InsurancePolicyException("Could not get the endorsement config", e);
			}
			if (productConfig != null) {
				RiskConfig riskConfig = productConfig.getRiskType(risk.getRiskType());
				if (riskConfig != null) {
					rv = riskConfig.getEndorsementConfig(end.getEndorsementCd());
				}
			}			
		}
		return rv;
	}
	
}
