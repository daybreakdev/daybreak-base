package com.echelon.services.coverage.LHT;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.services.policy.DSPolicyServicesFactory;

import java.util.logging.Level;
import java.util.logging.Logger;
import com.echelon.services.coverage.CoverageService;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.services.policy.LHT.LHTPolicyServicesFactory;

public class LHTCoverageService extends CoverageService {
	final static Logger logger = Logger.getLogger(LHTCoverageService.class.getName());
	
	public LHTCoverageService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return LHTPolicyServicesFactory.getInstance();
	}
	
	protected LHTPolicyCoverageService newPolicyCoverageService() {
		// TODO Auto-generated method stub
		return new LHTPolicyCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}
	
	protected LHTPolicyRiskCoverageService newPolicyRiskCoverageService() {
		// TODO Auto-generated method stub
		return new LHTPolicyRiskCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}

	// ESL-2050
	// For super to use the right SubPolicyCoverageService
	@Override
	public LHTSubPolicyCoverageService newSubPolicyCoverageService() {
		// TODO Auto-generated method stub
		return new LHTSubPolicyCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}

	@Override
	protected LHTSubPolicyRiskCoverageService newSubPolicyRiskCoverageService() {
		// TODO Auto-generated method stub
		return new LHTSubPolicyRiskCoverageService((IPolicyServicesFactory)getPolicyServicesFactory());
	}

	@Override
	public Coverage setSubpolicyCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Coverage coverage) {
		coverage = super.setSubpolicyCoverageDefault(transaction, subPolicy, coverage);
		coverage = newSubPolicyCoverageService().setCoverageDefault(transaction, subPolicy, coverage);
		return coverage;
	}

	@Override
	public Endorsement setSubPolicyRiskEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Risk risk, Endorsement endorsement) {
		endorsement = super.setSubPolicyRiskEndorsementDefault(transaction, subPolicy, risk, endorsement);
		endorsement = newSubPolicyRiskCoverageService().setEndorsementDefault(transaction, subPolicy, risk, endorsement);
		return endorsement;
	}

	@Override
	public Coverage setPolicyCoverageDefault(PolicyTransaction<?> transaction, Coverage coverage) {
		coverage = super.setPolicyCoverageDefault(transaction, coverage);
		try {
			coverage = newPolicyCoverageService().setCoverageDefault(transaction, coverage);
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTCoverageService::Could not set policy coverage default", e);
		}
		return coverage;
	}

	@Override
	public Endorsement setPolicyEndorsementDefault(PolicyTransaction<?> transaction, Endorsement endorsement) {
		endorsement = super.setPolicyEndorsementDefault(transaction, endorsement);
		try {
			endorsement = newPolicyCoverageService().setEnorsementDefault(transaction, endorsement);
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTCoverageService::Could not set policy endorsement default", e);
		}
		return endorsement; 
	}

	@Override
	public Coverage setPolicyRiskCoverageDefault(PolicyTransaction<?> transaction, Risk risk, Coverage coverage) {
		coverage = super.setPolicyRiskCoverageDefault(transaction, risk, coverage);
		try {
			coverage = newPolicyRiskCoverageService().setCoverageDefault(transaction, risk, coverage);
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTCoverageService::Could not set policy risk coverage default", e);
		}
		return coverage;
	}

	@Override
	public Endorsement setPolicyRiskEndorsementDefault(PolicyTransaction<?> transaction, Risk risk,
			Endorsement endorsement) {
		endorsement = super.setPolicyRiskEndorsementDefault(transaction, risk, endorsement);
		try {
			endorsement = newPolicyRiskCoverageService().setEndorsementDefault(transaction, risk, endorsement);
		} catch (InsurancePolicyException e) {
			logger.log(Level.WARNING, "LHTCoverageService::Could not set policy risk endorsement default", e);
		}
		return endorsement;
	}

	@Override
	public Coverage setSubPolicyRiskCoverageDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk,
			Coverage coverage) {
		coverage =  super.setSubPolicyRiskCoverageDefault(transaction, subPolicy, risk, coverage);
		coverage = newSubPolicyRiskCoverageService().setCoverageDefault(transaction, subPolicy, risk, coverage);
		return coverage;
	}

	private static final long serialVersionUID = 2178861641305347788L;

	@Override
	public Endorsement setSubpolicyEndorsementDefault(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy,
			Endorsement endorsement) {
		endorsement = super.setSubpolicyEndorsementDefault(transaction, subPolicy, endorsement);
		endorsement = newSubPolicyCoverageService().setEndorsementDefault(transaction, subPolicy, endorsement);
		return endorsement;
	}

	
}
