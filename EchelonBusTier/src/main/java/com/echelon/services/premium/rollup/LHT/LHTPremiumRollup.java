package com.echelon.services.premium.rollup.LHT;

import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.services.policy.LHT.LHTPolicyServicesFactory;
import com.echelon.services.premium.rollup.PremiumRollup;

@SuppressWarnings("serial")
public class LHTPremiumRollup extends PremiumRollup {

	public LHTPremiumRollup() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return LHTPolicyServicesFactory.getInstance();
	}

}
