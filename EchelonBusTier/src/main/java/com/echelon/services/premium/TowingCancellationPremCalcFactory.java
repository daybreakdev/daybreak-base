package com.echelon.services.premium;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.ds.ins.processes.policy.PolicyProcessesFactory;
import com.ds.ins.services.premium.CancellationPremCalcFactory;
import com.ds.ins.services.premium.FlatCancellationPremCalc;
import com.ds.ins.services.premium.ICancellationPremCalc;
import com.ds.ins.services.premium.ProRataCancellationPremCalc;
import com.ds.ins.utils.Constants;

public class TowingCancellationPremCalcFactory extends CancellationPremCalcFactory {
	static Map<String, ICancellationPremCalc> towingCancPremCalcMap = new HashMap<>();
	
	static {
		towingCancPremCalcMap.put(Constants.CANCELLATION_TYPE_FLAT, new FlatCancellationPremCalc());
		towingCancPremCalcMap.put(Constants.CANCELLATION_TYPE_PRORATA, new ProRataCancellationPremCalc());
		towingCancPremCalcMap.put(Constants.CANCELLATION_TYPE_SHORTRATE, new TowingShortRateCancellationPremCalc());
	}
	
	private static TowingCancellationPremCalcFactory instance;
	
	public static TowingCancellationPremCalcFactory getInstance() {
		if (instance == null) {
			synchronized (PolicyProcessesFactory.class) {
				instance = new TowingCancellationPremCalcFactory();
			}
		}
		return instance;
	}
	
	protected TowingCancellationPremCalcFactory() {
		//empty constructor
	}
	
	public Optional<ICancellationPremCalc> getCancellationPremCalc(String cancellationType) {
		return Optional.ofNullable(towingCancPremCalcMap.get(cancellationType));
	}
}
