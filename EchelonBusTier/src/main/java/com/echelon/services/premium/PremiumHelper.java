package com.echelon.services.premium;

import java.time.LocalDate;

import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.premium.DSPremiumHelper;
import com.ds.ins.utils.Constants;
import com.rating.rounding.RoundingStrategyFactory;
import com.ds.ins.services.premium.DSPremiumDto;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.echelon.utils.SpecialtyAutoConstants;

@SuppressWarnings("serial")
public class PremiumHelper extends DSPremiumHelper {

	public PremiumHelper() {
		super();
	}
	
	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return LHTRoundingStrategyFactory.getInstance();
	}

	public DSPremiumDto createPremiumDto(IBusinessEntity covParent, Coverage coverage) {
		DSPremiumDto premiumDto = createPremiumDto(coverage);
		
		if (covParent instanceof SubPolicy) {
			SubPolicy<?> subPolicy = (SubPolicy<?>) covParent;
			if (SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
				premiumDto.setUnit(coverage.getNumOfUnits());
			}
		}
		else if (covParent instanceof Risk) {
			Risk risk = (Risk) covParent;
			if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
				SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
				premiumDto.setUnit(vehRisk.getNumberOfUnits());
			}
		}
		
		return premiumDto;
	}
	
	public DSPremiumDto createPremiumDto(IBusinessEntity covParent, Endorsement endorsement) {
		DSPremiumDto premiumDto = createPremiumDto(endorsement);
		
		if (covParent instanceof Risk) {
			Risk risk = (Risk) covParent;
			if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
				SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
				premiumDto.setUnit(vehRisk.getNumberOfUnits());
			}
		}
		
		return premiumDto;
	}

	@Override
	public void updatePremium(String versionType, String transType, String systemStatus, String businessStatus,
			LocalDate termEffDate, LocalDate termExpDate, LocalDate changeEffDate, DSPremiumDto premiumDto,
			String mode) {
		
		if (Constants.VERS_TXN_TYPE_ADJUSTMENT.equalsIgnoreCase(versionType)) {
			return;
		}
		
		super.updatePremium(versionType, transType, systemStatus, businessStatus, termEffDate, termExpDate, changeEffDate,
				premiumDto, mode);
	}
	
	public Premium updatePremiumValues(String versionType, String txnType, Premium premium) {
		premium = updatePremiumValues(premium, txnType);
		return premium;
	}
}
