package com.echelon.services.premium;

import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.ds.ins.services.premium.DSPremiumService;
import com.echelon.services.policy.PolicyServicesFactory;

@SuppressWarnings("serial")
public class PremiumService extends DSPremiumService {

	public PremiumService() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return PolicyServicesFactory.getInstance();
	}

}
