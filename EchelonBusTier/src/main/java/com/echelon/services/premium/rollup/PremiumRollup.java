package com.echelon.services.premium.rollup;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.ds.ins.services.premium.DSPremiumDto;
import com.ds.ins.services.premium.rollup.DSPremiumRollup;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.services.policy.PolicyServicesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

@SuppressWarnings("serial")
public class PremiumRollup extends DSPremiumRollup {

	public PremiumRollup() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return PolicyServicesFactory.getInstance();
	}
	
	protected SubPolicyPremiumRollup createSubPolicyPremiumRollup() {
		return new SubPolicyPremiumRollup((IPolicyServicesFactory)getPolicyServicesFactory());
	}
	
	protected SubPolicyRiskPremiumRollup createSubPolicyRiskPremiumRollup() {
		return new SubPolicyRiskPremiumRollup((IPolicyServicesFactory)getPolicyServicesFactory());
	}

	@Override
	protected void addToPolicyTransactionPremiumList(Set<Premium> premiumList, Risk risk) {
		if (!SpecialtyAutoConstants.EXCLUDEROLLUPTOPOLICY_RISKS.contains(risk.getRiskType())) {
			super.addToPolicyTransactionPremiumList(premiumList, risk);
		}
	}
	
	@Override
	protected SubPolicy<?> preTotalPolicyTransactionSubPolicyPremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy) {
		if (!Constants.VERS_TXN_TYPE_REVERSAL.equalsIgnoreCase(transaction.getPolicyVersion().getPolicyTxnType())) {
			createSubPolicyPremiumRollup().totalGarageLocationPremium(transaction, subPolicy);
		}
		
		return subPolicy;
	}

	@Override
	protected Set<Premium> getPolicyTransactionSubPolicyPremiums(PolicyTransaction<?> transaction,
			SubPolicy<?> subPolicy, Set<Premium> premiums) {
		// TODO Auto-generated method stub
		return createSubPolicyPremiumRollup().getSubPolicyRiskPremiums(transaction, subPolicy, premiums);
	}

	@Override
	protected Premium sumUpPolicyTransactionRiskPremiums(Risk risk, Premium riskPremiunm, Premium totRiskPremiunm) {
		
		if (SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(risk.getRiskType())) {
			return getPremiumHelper().resetPremium(riskPremiunm);
		}
		
		return super.sumUpPolicyTransactionRiskPremiums(risk, riskPremiunm, totRiskPremiunm);
	}
	
	@Override
	protected Risk totalPolicyTransactionSubPolicyRiskPremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk) {
		super.totalPolicyTransactionSubPolicyRiskPremium(transaction, subPolicy, risk);
		
		return createSubPolicyRiskPremiumRollup().totalRiskPremium(transaction, subPolicy, risk);
	}
	
	@Override
	protected Premium totalPolicyTransactionSubPolicyRiskCoveragePremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk, Premium totPremiunm, List<DSPremiumDto> premiumDtoList) {
		premiumDtoList = createSubPolicyRiskPremiumRollup().getCoveragePremiumList(risk, totPremiunm, premiumDtoList);
		
		return super.totalPolicyTransactionSubPolicyRiskCoveragePremium(transaction, subPolicy, risk, totPremiunm, premiumDtoList);
	}
	
	public SpecialtyVehicleRiskAdjustment totalAdjustmentPremium(SpecialtyVehicleRiskAdjustment adjustment) {
		List<DSPremiumDto> premiumDtos = getPremiumHelper().getCoverageDetailPremiums(adjustment, Arrays.asList(adjustment.getCoverageDetail()));

		Premium totPremiunm = adjustment.getCoverageDetail().getCoveragePremium();
		totPremiunm = getPremiumHelper().totalCoveragePremium(totPremiunm, premiumDtos);
		adjustment.getCoverageDetail().setCoveragePremium(totPremiunm);
		
		return adjustment;
	}
}
