package com.echelon.services.premium.rollup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.premium.DSPremiumDto;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.services.premium.PremiumHelper;
import com.echelon.utils.SpecialtyAutoConstants;

public class SubPolicyPremiumRollup {

	private CoverageServiceHelper coverageServiceHelper;
	private PremiumHelper		  premiumHelper;
	
	public SubPolicyPremiumRollup(IPolicyServicesFactory policyServicesFactory) {
		super();
		this.coverageServiceHelper = policyServicesFactory.newCoverageServiceHelper();
		this.premiumHelper = policyServicesFactory.newPremiumHelper();
	}

	protected Set<Premium> getSubPolicyRiskPremiums(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Set<Premium> premiums) {		
		switch (subPolicy.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_AUTO:
			List<Risk> vehicles = coverageServiceHelper.getAllVehicles(transaction);
			if (vehicles != null && !vehicles.isEmpty()) {
				vehicles.stream()
							.filter(o -> o.getRiskPremium() != null)
							.collect(Collectors.toList())
							.stream().forEach(o -> premiums.add(o.getRiskPremium()));
			}
			break;

		default:
			break;
		}
		
		return premiums;
	}
	
	public List<Premium> totalGarageLocationPremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy) {
		List<Premium> totPremiunms = new ArrayList<Premium>();
		
		if (SpecialtyAutoConstants.SUBPCY_CODE_GARAGE.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd()) &&
				subPolicy.getCoverageDetail() != null) {
			// All Location premiums
			List<DSPremiumDto> locPremiumDtos = new ArrayList<DSPremiumDto>();
			
			transaction.getPolicyRisks().stream().filter(o -> SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION.equalsIgnoreCase(o.getRiskType()))
					.collect(Collectors.toList())
					.forEach(o -> {
						if (o.getCoverageDetail() != null) {
							List<DSPremiumDto> locPremDtos = this.premiumHelper.getCoverageDetailPremiums(o, Arrays.asList(o.getCoverageDetail()));
							if (locPremDtos != null) {
								locPremiumDtos.addAll(locPremDtos);
							}
						}
					});
			
			if (locPremiumDtos != null && !locPremiumDtos.isEmpty()) {
				for(String code : SpecialtyAutoConstants.GARAGE_LOC_COVERAGES) {
					Coverage coverage = subPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode(code);
					if (coverage != null) {
						// All location coverage premiums
						List<DSPremiumDto> locCovPremiumDtos = locPremiumDtos.stream()
																.filter(o -> o.getCoverage() != null && code.equalsIgnoreCase(o.getCoverage().getCoverageCode()))
																.collect(Collectors.toList());
						if (!locCovPremiumDtos.isEmpty()) {
							Premium totPremiunm = this.premiumHelper.totalCoveragePremium(coverage.getCoveragePremium(), locCovPremiumDtos);
							totPremiunm.setPremiumOverride(locCovPremiumDtos.stream().filter(o -> BooleanUtils.isTrue(o.getPremium().isPremiumOverride())).findAny().isPresent());
							coverage.setCoveragePremium(totPremiunm);
							
							totPremiunms.add(totPremiunm);
						}
					}
				}
			}
		}
		
		return totPremiunms;
	}

}
