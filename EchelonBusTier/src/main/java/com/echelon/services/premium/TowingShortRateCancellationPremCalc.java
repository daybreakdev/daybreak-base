package com.echelon.services.premium;

import org.apache.commons.lang3.BooleanUtils;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageReinsurance;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.EndorsementReinsurance;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.rating.rounding.IRoundingStrategy;
import com.ds.ins.services.premium.ShortRateCancellationPremCalc;
import com.echelon.rating.towing.TowingShortRateCancProcessVer1;

public class TowingShortRateCancellationPremCalc extends ShortRateCancellationPremCalc {

	@Override
	public Double calcCovTransPrem(Coverage cov, PolicyTransaction<?> pcyTxn, IRoundingStrategy roundStrategy) throws InsurancePolicyException {
		Double shortRate = getShortRate(pcyTxn);
		Double transPrem = calcTransPrem(cov.getCoveragePremium(), shortRate, roundStrategy);
		cov.addCoverageRateFactor(new RateFactor("ShortRateFactor", null, shortRate));
		
		//DAYB-202
		/*		TowingShortRateCancProcessVer1 srcProc = new TowingShortRateCancProcessVer1();
		try {
			Double shortRate = srcProc.getShortRate(pcyTxn);
			if (shortRate != null) {
				transPrem = cov.getCoveragePremium().getAnnualPremium() * (1 - shortRate);
				transPrem = roundStrategy.roundOffSet(0 - transPrem);
				cov.addCoverageRateFactor(new RateFactor("ShortRateFactor", null, shortRate));
			} else {
				throw new InsurancePolicyException("The short rate for cancellation is NULL");
			}
		} catch (RatingException e) {
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		} catch (Exception e) {
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		}
		*/
		
		if (BooleanUtils.isTrue(cov.isReinsured())) {
			for(CoverageReinsurance rein : cov.getCoverageReinsurances()) {
				Double reinTransPrem = calcTransPrem(rein.getCovReinsurancePremium(), shortRate, roundStrategy);
				rein.getCovReinsurancePremium().setTransactionPremium(reinTransPrem);
			}
		}
		
		return transPrem;
	}

	@Override
	public Double calcEndTransPrem(Endorsement end, PolicyTransaction<?> pcyTxn, IRoundingStrategy roundStrategy) throws InsurancePolicyException {
		Double shortRate = getShortRate(pcyTxn);
		Double transPrem = calcTransPrem(end.getEndorsementPremium(), shortRate, roundStrategy);
		end.addEndorsementRateFactor(new RateFactor("ShortRateFactor", null, shortRate));
		
		//DAYB-202
		/*		TowingShortRateCancProcessVer1 srcProc = new TowingShortRateCancProcessVer1();
		try {
			Double shortRate = srcProc.getShortRate(pcyTxn);
			if (shortRate != null) {
				transPrem = end.getEndorsementPremium().getAnnualPremium() * (1 - shortRate);
				transPrem = roundStrategy.roundOffSet(0 - transPrem);
				end.addEndorsementRateFactor(new RateFactor("ShortRateFactor", null, shortRate));
			} else {
				throw new InsurancePolicyException("The short rate for cancellation is NULL");
			}
		} catch (RatingException e) {
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		} catch (Exception e) {
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		}
		*/
		
		if (BooleanUtils.isTrue(end.isReinsured())) {
			for(EndorsementReinsurance rein : end.getEndorsementReinsurances()) {
				Double reinTransPrem = calcTransPrem(rein.getEndReinsurancePremium(), shortRate, roundStrategy);
				rein.getEndReinsurancePremium().setTransactionPremium(reinTransPrem);
			}
		}
				
		return transPrem;
	}
	
	private Double getShortRate(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		Double shortRate = null;
		
		TowingShortRateCancProcessVer1 srcProc = new TowingShortRateCancProcessVer1();
		try {
			shortRate = srcProc.getShortRate(pcyTxn);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		}
		
		return shortRate;
	}
	
	private Double calcTransPrem(Premium premium, Double shortRate, IRoundingStrategy roundStrategy) throws InsurancePolicyException {
		Double transPrem = null;
		try {
			if (shortRate != null) {
				transPrem = premium.getAnnualPremium() * (1 - shortRate);
				transPrem = roundStrategy.roundOffSet(0 - transPrem);
			} else {
				throw new InsurancePolicyException("The short rate for cancellation is NULL");
			}
		} catch (Exception e) {
			throw new InsurancePolicyException("Could not get short rate for cancellation", e);
		}
		return transPrem;
	}

}
