package com.echelon.services.premium.LHT;

import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.services.policy.LHT.LHTPolicyServicesFactory;
import com.echelon.services.premium.PremiumService;

@SuppressWarnings("serial")
public class LHTPremiumService extends PremiumService {

	public LHTPremiumService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return LHTPolicyServicesFactory.getInstance();
	}

}
