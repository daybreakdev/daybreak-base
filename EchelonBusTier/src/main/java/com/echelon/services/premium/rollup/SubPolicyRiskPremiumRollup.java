package com.echelon.services.premium.rollup;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.services.premium.DSPremiumDto;
import com.rating.rounding.IRoundingStrategy;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class SubPolicyRiskPremiumRollup {
	private IRoundingStrategy 	  roundStrategy;
	private CoverageServiceHelper coverageServiceHelper;

	public SubPolicyRiskPremiumRollup(IPolicyServicesFactory policyServicesFactory) {
		super();
		this.roundStrategy = policyServicesFactory.getRoundingStrategy();
		this.coverageServiceHelper = policyServicesFactory.newCoverageServiceHelper();
	}

	protected Double calcPreTotalRiskPremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk,
			String forSubPolicyCode, String forSubPolicyCoverageCode) {
		Double result = null;
		
		switch (risk.getRiskType()) {
		
		case SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH:
			if (coverageServiceHelper.isCalcVehicleGroupUnitRate(transaction, subPolicy, risk, forSubPolicyCode)) {
				SubPolicy<?> foundSubPolicy = transaction.findSubPolicyByType(forSubPolicyCode);
				if (foundSubPolicy != null) {
					if (foundSubPolicy.getCoverageDetail() != null) {
						Coverage coverage = foundSubPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode(forSubPolicyCoverageCode);
						if (coverage != null && coverage.getCoveragePremium() != null && coverage.getCoveragePremium().getTransactionPremium() != null &&
							coverage.getNumOfUnits() != null && coverage.getNumOfUnits() > 0) {
							result = roundStrategy.round(coverage.getCoveragePremium().getAnnualPremium() / coverage.getNumOfUnits(), transaction.getTransactionType());
//							result = roundStrategy.round(coverage.getCoveragePremium().getTransactionPremium() / coverage.getNumOfUnits(), transaction.getTransactionType());
						}
					}
				}
			}
			break;
		default:
			break;
		}
		
		return result;
	}

	public Risk totalRiskPremium(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk) {
		switch (risk.getRiskType()) {
		case SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH:
			if (!StringUtils.equals(Constants.VERS_TXN_TYPE_ADJUSTMENT, transaction.getPolicyVersion().getPolicyTxnType())) {
				SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
				Double unitRate = risk.getRiskPremium().getAnnualPremium();
//				Double unitRate = risk.getRiskPremium().getTransactionPremium();
				if (unitRate == null) {
					unitRate = 0D;
				}
				
				if (unitRate.doubleValue() != 0 && vehRisk.getNumberOfUnits() != null && vehRisk.getNumberOfUnits() > 1) {
					unitRate = roundStrategy.round(unitRate / vehRisk.getNumberOfUnits(), transaction.getTransactionType());
							/*
							((new BigDecimal(unitRate)).divide(new BigDecimal(vehRisk.getNumberOfUnits()), BigDecimal.ROUND_HALF_UP))
									.setScale(0, BigDecimal.ROUND_HALF_UP)
									.doubleValue();
									*/
				}
				
				final Double cglBIPDPremium = calcPreTotalRiskPremium(transaction, subPolicy, risk,
						SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY, SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD);
				if (cglBIPDPremium != null && cglBIPDPremium != 0) {
					unitRate = roundStrategy.round(unitRate + cglBIPDPremium, transaction.getTransactionType());
							/*
							((new BigDecimal(unitRate)).add(new BigDecimal(cglBIPDPremium)))
									.setScale(0, BigDecimal.ROUND_HALF_UP)
									.doubleValue();
									*/
				}

				// ESL-1759 include Cargo MTCC premium in unit rate **for LHT only**
				// This condition should not be here, other functionality e.g. 21B Adjustment also requires this change
				// This IF statment is moved to LHTCoverageServiceHelper.isCalcVehicleGroupUnitRate
				//if (StringUtils.equalsIgnoreCase(ConfigConstants.PRODUCTCODE_LHT,
						//transaction.getPolicyVersion().getInsurancePolicy().getProductCd())) {
				final Double cargoMTCCPremium = calcPreTotalRiskPremium(transaction, subPolicy, risk,
						SpecialtyAutoConstants.SUBPCY_CODE_CARGO, SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC);
				if (cargoMTCCPremium != null && cargoMTCCPremium != 0) {
					unitRate = roundStrategy.round(unitRate + cargoMTCCPremium, transaction.getTransactionType());
				}
				//}

				vehRisk.setUnitRate(unitRate);				
			}			
			break;
			
		default:
			break;
		}
		
		return risk;
	}

	// ESL-537 ust specify total premium of the vehicle = OPCF27B premium and vehicle description = OPCF27B
	// Adjust the premiumDtoList only
	public List<DSPremiumDto> getCoveragePremiumList(Risk risk, Premium totPremiunm, List<DSPremiumDto> premiumDtoList) {

		List<DSPremiumDto> wrkPremDtoList = new ArrayList<DSPremiumDto>();
		
		if (premiumDtoList != null && !premiumDtoList.isEmpty()) {
			wrkPremDtoList.addAll(premiumDtoList);
			
			if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
				SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk) risk;
				if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
					DSPremiumDto OPCF27Bprem = wrkPremDtoList.stream().filter(o -> o.getEndorsement() != null &&  
							SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B.equalsIgnoreCase(o.getEndorsement().getEndorsementCd()))
							.findFirst().orElse(null);
					if (OPCF27Bprem != null) {
						wrkPremDtoList.clear();						
						wrkPremDtoList.add(OPCF27Bprem);
					}
				}
			}
		}
		
		return wrkPremDtoList;
	}

	/* moved to coverageServiceHelper
	@SuppressWarnings("unchecked")
	protected boolean isCalcVehicleGroupUnitRate(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy, Risk risk) {
		if (SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			SpecialityAutoSubPolicy<Risk> autoSubPolicy = (SpecialityAutoSubPolicy<Risk>) subPolicy;
			SpecialtyVehicleRisk vehRisk = (SpecialtyVehicleRisk) risk;
			
			// ESL-384 If Fleet Basis is Scheduled (SCHD) do not calculate Unit Rate by adding CGL remium
			return  !"SCHD".equalsIgnoreCase(autoSubPolicy.getFleetBasis()) &&
					!coverageServiceHelper.isVehicleCGLExcluded(vehRisk)
					//ESL-252
					//&& Constants.VERSION_STATUS_ISSUED.equalsIgnoreCase(transaction.getPolicyVersion().getBusinessStatus())
					;
		}
		
		return false;
	}
	*/
}
