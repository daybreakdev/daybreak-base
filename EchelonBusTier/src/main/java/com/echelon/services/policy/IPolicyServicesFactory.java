package com.echelon.services.policy;

import com.ds.ins.services.coverage.DSCoverageDataExtensionService;
import com.echelon.services.coverage.CoverageService;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.services.premium.PremiumHelper;
import com.echelon.services.premium.PremiumService;
import com.echelon.services.premium.rollup.PremiumRollup;
import com.echelon.services.validation.ValidationService;
import com.rating.rounding.IRoundingStrategy;

public interface IPolicyServicesFactory {

	public PolicyService newPolicyService();
	public CoverageService newCoverageService();
	public CoverageServiceHelper newCoverageServiceHelper();
	public PremiumService newPremiumService();
	public PremiumRollup newPremiumRollup();
	public PremiumHelper newPremiumHelper();
	public ValidationService newValidationService();
	public DSCoverageDataExtensionService newCoverageDataExtensionService();
	public IRoundingStrategy getRoundingStrategy();
}
