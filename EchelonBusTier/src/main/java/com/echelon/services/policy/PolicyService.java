package com.echelon.services.policy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.services.policy.DSPolicyService;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.ds.ins.system.UserProfile;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.services.conversion.SIMRatingConversion;
import com.echelon.services.coverage.CoverageService;
import com.echelon.utils.SpecialtyAutoConstants;

public class PolicyService extends DSPolicyService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9129455047669053518L;
	private static final Logger log	= Logger.getLogger(PolicyService.class.getName());
	
	private CoverageService coverageService;

	public PolicyService() {
		super();
		// TODO Auto-generated constructor stub
		
		coverageService = (CoverageService)getPolicyServicesFactory().newCoverageService();
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return PolicyServicesFactory.getInstance();
	}

	@SuppressWarnings("rawtypes")
	public List<String> rateAllPolicyTransactions(IGenericPolicyProcessing policyProcessing, UserProfile userProfile) throws InsurancePolicyException {
		List<String> results = new ArrayList<String>();
		
		log.info("Rate Policy Transaction - searching for policy transactions");
		List<Long> pcyTxnPKs = (new PolicyTransactionDAO()).getAllNonIssuedPolicyTransactions(Constants.VERSION_STATUS_ISSUED);
							 // = Arrays.asList(10088L); 
		long totCount = ((pcyTxnPKs != null && !pcyTxnPKs.isEmpty()) ? pcyTxnPKs.size() : 0); 
		log.info("Rate Policy Transaction - policy transactions found:"+totCount);
		if (pcyTxnPKs != null && !pcyTxnPKs.isEmpty()) {
			long count = 1;
			for(Long pcyTxnPK : pcyTxnPKs) {
				boolean ok = true;
				PolicyTransaction<?> pcyTxn = null;
				Throwable ex = null;
				try {
					//pcyTxn = policyProcessing.ratePolicyTransaction(pcyTxnPK, userProfile);
					//pcyTxn = new PolicyTransactionDAO().findById(pcyTxnPK);
					pcyTxn = (new SIMRatingConversion()).process(pcyTxnPK, policyProcessing, userProfile);
				} catch (Throwable e) {
					ok = false;
					ex = e;
				}
				
				String pk    = String.valueOf(pcyTxnPK).replaceAll(",", "");
				String quote = "";
				String rs    = ok ? "S" : "F";
				if (pcyTxn != null) {
					quote = pcyTxn.getPolicyVersion().getInsurancePolicy().getQuoteNum();
				}
				
				log.info("Rate Policy Transaction ("
							+ count+" of "+totCount
							+ ") - Quote:"+quote+", policyTransactionPK:"+pk+", Result:"+rs);
				
				if (ex != null) {
					ex.printStackTrace();
				}
				
				results.add(pk+","+quote+","+rs);
				count++;
			}
		}
		
		return results; 
	}

	@Override
	protected SubPolicy<?> preRateSubPolicy(PolicyTransaction<?> transaction, SubPolicy<?> subPolicy) {
		subPolicy = super.preRateSubPolicy(transaction, subPolicy);
		
		if (SpecialtyAutoConstants.SUBPCY_CODE_GARAGE.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			resetRatePremium(subPolicy.getCoverageDetail(), Arrays.asList(SpecialtyAutoConstants.SUBPCY_COV_CODE_CPE));
		}
		
		return subPolicy;
	}
	
	
	// ES-138
	// Keep rounding for non-LHT
	public void updateAllCargoPartialPremium(SubPolicy<?> subPolicy) {
		if (SpecialtyAutoConstants.SUBPCY_CODE_CARGO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) subPolicy;
			if (cargoSubPolicy.getCargoDetails() != null && !cargoSubPolicy.getCargoDetails().isEmpty()) {
				cargoSubPolicy.getCargoDetails().stream().forEach(o -> updateCargoPartialPremium(cargoSubPolicy, o));
			}
		}
	}
	
	protected double calculateCargoPartialPremium(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		double average = 0d; 
		if (cargoDetails.getuWManual() != null && cargoDetails.getuWManual() > 0) {
			average = cargoDetails.getuWManual();
		}
		else if (cargoDetails.getAverage() != null && cargoDetails.getAverage() > 0) {
			average = cargoDetails.getAverage();
		}
		
		double mtccLimit = 0d;
		Coverage mtcc = subPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC);
		if (mtcc != null && mtcc.getLimit1() != null && mtcc.getLimit1() > 0) {
			mtccLimit = mtcc.getLimit1();
		}
		
		double minLimit = 0d;
		if (average > 0 && mtccLimit > 0) {
			minLimit = Math.min(average, mtccLimit);
		}
		else if (average > 0) {
			minLimit = average;
		}
		else {
			minLimit = mtccLimit;
		}
		
		double result = minLimit * 
							(cargoDetails.getCargoExposure() != null ? (cargoDetails.getCargoExposure() / 100) : 0) * 
							(cargoDetails.getCargoValue() != null ? (cargoDetails.getCargoValue() / 100) : 0)
							;
		return result;
	}
	
	protected double roundCargoPartialPremium(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, double cargoPartialPremiums) {
		double result = roundStrategy.round(cargoPartialPremiums, subPolicy.getPolicyTransaction().getTransactionType());
		return result;
	}
	
	public void updateCargoPartialPremium(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		double cargoPartialPremiums = calculateCargoPartialPremium(subPolicy, cargoDetails);
		cargoPartialPremiums = roundCargoPartialPremium(subPolicy, cargoDetails, cargoPartialPremiums);
		cargoDetails.setCargoPartialPremiums(cargoPartialPremiums);
	}
	
	protected Double calculateTotalCargoPartialPremium(CargoSubPolicy<?> subPolicy) {
		Double result = subPolicy.getCargoDetails()
							.stream().filter(o -> ((o.isActive() || o.isPending()) && o.getCargoPartialPremiums() != null)).collect(Collectors.toList())
							.stream().mapToDouble(CargoDetails::getCargoPartialPremiums).sum()
							;
		return result;
	}
	
	protected Double roundTotalCargoPartialPremium(CargoSubPolicy<?> subPolicy, Double totPrem) {
		Double result = roundStrategy.round(totPrem, subPolicy.getPolicyTransaction().getTransactionType());
		return result;
	}
	
	public void updateTotalCargoPartialPremium(CargoSubPolicy<?> subPolicy) {
		Double totPrem = 0D;
		
		if (subPolicy.getCargoDetails() != null && !subPolicy.getCargoDetails().isEmpty()) {
			totPrem = calculateTotalCargoPartialPremium(subPolicy);
			totPrem = roundTotalCargoPartialPremium(subPolicy, totPrem);
			
			/*
			totPrem = roundStrategy.round(
						subPolicy.getCargoDetails()
						.stream().filter(o -> ((o.isActive() || o.isPending()) && o.getCargoPartialPremiums() != null)).collect(Collectors.toList())
						.stream().mapToDouble(CargoDetails::getCargoPartialPremiums).sum()
						, subPolicy.getPolicyTransaction().getTransactionType()
						);
						*/
					/* new BigDecimal(
							subPolicy.getCargoDetails()
							.stream().filter(o -> o.getCargoPartialPremiums() != null).collect(Collectors.toList())
							.stream().mapToDouble(CargoDetails::getCargoPartialPremiums).sum()
							).setScale(0, BigDecimal.ROUND_HALF_UP);
							*/
		}
		
		subPolicy.setTotalCargoPartialPremium(totPrem);
	}
	
	@SuppressWarnings("rawtypes")
	public void applyCGLDeductibleChanged(CGLSubPolicy cglSubPolicy, CGLSubPolicy prevCGLSubPolicy) {
		if (prevCGLSubPolicy != null) {
			if (cglSubPolicy.getCglDeductible() != null &&
				(prevCGLSubPolicy.getCglDeductible() == null ||
				 prevCGLSubPolicy.getCglDeductible().longValue() != cglSubPolicy.getCglDeductible().longValue())) {
				if (cglSubPolicy.getCoverageDetail() != null) {
					Coverage coverage = cglSubPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SUBPCY_COV_CODE_TLL);
					if (coverage != null) {
						coverageService.newSubPolicyCoverageService().applyCGLDeductibleChanged(cglSubPolicy, coverage);	
					}
				}
			}
		}
	}
	
	public void applyCoverageWhenSubpolicyAdded(SubPolicy<?> subPolicy) {
		
	}
	
	public void applyCoverageWhenSubpolicyDeleted(SubPolicy<?> subPolicy) {
		
	}

}
