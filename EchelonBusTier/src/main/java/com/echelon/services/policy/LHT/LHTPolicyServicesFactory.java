package com.echelon.services.policy.LHT;

import com.ds.ins.services.coverage.DSCoverageDataExtensionService;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.echelon.services.coverage.LHT.LHTCoverageService;
import com.echelon.services.coverage.LHT.LHTCoverageServiceHelper;
import com.echelon.services.policy.IPolicyServicesFactory;
import com.echelon.services.premium.PremiumHelper;
import com.echelon.services.premium.LHT.LHTPremiumService;
import com.echelon.services.premium.rollup.LHT.LHTPremiumRollup;
import com.echelon.services.validation.ValidationService;
import com.rating.rounding.IRoundingStrategy;

public class LHTPolicyServicesFactory extends DSPolicyServicesFactory<LHTPolicyService, LHTCoverageService, LHTCoverageServiceHelper, LHTPremiumService, LHTPremiumRollup, PremiumHelper, ValidationService, DSCoverageDataExtensionService> 
										implements IPolicyServicesFactory {

	private static LHTPolicyServicesFactory instance = new LHTPolicyServicesFactory();
	
	public static LHTPolicyServicesFactory getInstance() {
		return instance;
	}

	private LHTPolicyServicesFactory() {}
	
	@Override
	public LHTPolicyService createPolicyService() {
		// TODO Auto-generated method stub
		// ESL-2050 for PolicyService to use right LHTPolicyServicesFactory
		return new LHTPolicyService();
	}

	@Override
	protected LHTCoverageService createCoverageService() {
		// TODO Auto-generated method stub
		return new LHTCoverageService();
	}

	@Override
	protected LHTCoverageServiceHelper createCoverageServiceHelper() {
		// TODO Auto-generated method stub
		return new LHTCoverageServiceHelper();
	}

	@Override
	protected LHTPremiumService createPremiumService() {
		// TODO Auto-generated method stub
		return new LHTPremiumService();
	}

	@Override
	protected LHTPremiumRollup createPremiumRollup() {
		// TODO Auto-generated method stub
		return new LHTPremiumRollup();
	}

	@Override
	protected PremiumHelper createPremiumHelper() {
		// TODO Auto-generated method stub
		return new PremiumHelper();
	}

	@Override
	protected ValidationService createValidationService() {
		// TODO Auto-generated method stub
		return new ValidationService();
	}

	public IRoundingStrategy getRoundingStrategy() {
		return LHTRoundingStrategyFactory.getInstance().getRoundingStrategy();
	}
	
}
