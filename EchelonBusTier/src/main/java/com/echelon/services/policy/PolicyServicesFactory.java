package com.echelon.services.policy;

import com.ds.ins.services.coverage.DSCoverageDataExtensionService;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.services.coverage.CoverageService;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.services.premium.PremiumHelper;
import com.echelon.services.premium.PremiumService;
import com.echelon.services.premium.rollup.PremiumRollup;
import com.echelon.services.validation.ValidationService;
import com.rating.rounding.IRoundingStrategy;
import com.rating.rounding.RoundingStrategyFactory;

public class PolicyServicesFactory extends DSPolicyServicesFactory<PolicyService, CoverageService, CoverageServiceHelper, PremiumService, PremiumRollup, PremiumHelper, ValidationService, DSCoverageDataExtensionService>
									implements IPolicyServicesFactory {

	private static PolicyServicesFactory instance = new PolicyServicesFactory();

	public static PolicyServicesFactory getInstance() {
		return instance;
	}
	
	private PolicyServicesFactory() {
		//empty constructor
	}

	@Override
	public PolicyService createPolicyService() {
		// TODO Auto-generated method stub
		return new PolicyService();
	}

	@Override
	protected CoverageService createCoverageService() {
		// TODO Auto-generated method stub
		return new CoverageService();
	}

	@Override
	protected CoverageServiceHelper createCoverageServiceHelper() {
		// TODO Auto-generated method stub
		return new CoverageServiceHelper();
	}

	@Override
	protected PremiumService createPremiumService() {
		// TODO Auto-generated method stub
		return new PremiumService();
	}

	@Override
	protected PremiumRollup createPremiumRollup() {
		// TODO Auto-generated method stub
		return new PremiumRollup();
	}

	@Override
	protected PremiumHelper createPremiumHelper() {
		// TODO Auto-generated method stub
		return new PremiumHelper();
	}

	@Override
	protected ValidationService createValidationService() {
		// TODO Auto-generated method stub
		return new ValidationService();
	}

	public IRoundingStrategy getRoundingStrategy() {
		return RoundingStrategyFactory.getInstance().getRoundingStrategy();
	}
}
