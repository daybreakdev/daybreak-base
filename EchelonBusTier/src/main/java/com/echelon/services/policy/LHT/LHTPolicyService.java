package com.echelon.services.policy.LHT;

import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.echelon.services.policy.PolicyService;
import com.rating.rounding.RoundingStrategyFactory;

public class LHTPolicyService extends PolicyService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8265335354805057060L;

	public LHTPolicyService() {
		super();
		// TODO Auto-generated constructor stub
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		// TODO Auto-generated method stub
		return LHTPolicyServicesFactory.getInstance();
	}

	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return LHTRoundingStrategyFactory.getInstance();
	}

	// ES-138
	// New calculation for LHT
	// No rounding for LHT
	@Override
	protected double calculateCargoPartialPremium(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails) {
		double calculatedLimit = 0d; 
		if (cargoDetails.getuWManual() != null && cargoDetails.getuWManual() > 0) {
			calculatedLimit = cargoDetails.getuWManual();
		}
		else if (cargoDetails.getAverage() != null && cargoDetails.getAverage() > 0) {
			calculatedLimit = cargoDetails.getAverage();
		}
		
		double result = calculatedLimit
						* (cargoDetails.getCargoExposure()  != null ? (cargoDetails.getCargoExposure() / 100) : 0)
						* (cargoDetails.getCargoValue() 	!= null ? (cargoDetails.getCargoValue() / 100) : 0)
						;
		return result;
	}

	@Override
	protected double roundCargoPartialPremium(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails,
			double cargoPartialPremiums) {
		return cargoPartialPremiums;
	}

	@Override
	protected Double roundTotalCargoPartialPremium(CargoSubPolicy<?> subPolicy, Double totPrem) {
		return totPrem;
	}

}
