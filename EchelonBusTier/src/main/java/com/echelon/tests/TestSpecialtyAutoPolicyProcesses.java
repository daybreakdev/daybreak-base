package com.echelon.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ds.ins.dal.dao.AddressDAO;
import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.domain.dto.LienholderLessorSearchResults;
import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.ds.ins.domain.dto.PolicySearchResults;
import com.ds.ins.domain.dto.PolicyTermPremiumDTO;
import com.ds.ins.domain.dto.PolicyTransactionPremiumDTO;
import com.ds.ins.domain.dto.PremiumDTO;
//import com.ds.ins.domain.dto.ReinsuranceItemDTO;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.entities.Producer;
import com.ds.ins.domain.policy.AdditionalInsured;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.StatCodes;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.BrokerException;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.processes.broker.BrokerProcesses;
import com.ds.ins.processes.customer.CommercialCustomerProcessing;
import com.ds.ins.processes.customer.PersonalCustomerProcessing;
import com.ds.ins.system.UserProfile;
import com.ds.ins.utils.Constants;
import com.echelon.dal.dao.specialitylines.SpecialtyVehicleRiskDAO;
//import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyVehicleRiskDTO;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;
import com.echelon.utils.SpecialtyAutoConstants;

class TestSpecialtyAutoPolicyProcesses {
	static UserProfile userProfile;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		userProfile = new UserProfile();
		userProfile.setLoginId("maziz");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}
/*
	@Test
	void test() {
		//SpecialtyAutoPolicyProcesses impl = new SpecialtyAutoPolicyProcesses();
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getPolicyVersions());
			PolicyVersion pv = pcy.getPolicyVersions().get(0);
			assertNotNull(pv.getPolicyTransactions());
			PolicyTransaction<SpecialtyVehicleRisk> txn = (PolicyTransaction<SpecialtyVehicleRisk>) pv.getPolicyTransactions().get(0);
			try {
				//SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = impl.createAutoSubPolicy(txn);
				SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) txn.findSubPolicyByType("AUTO");
				assertNotNull(subPcy);
				SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(subPcy);
				assertNotNull(veh);
				txn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(txn, userProfile);
			} catch (SpecialtyAutoPolicyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
*/	
	@Test
	void testCreateNewInsurancePolicyNoExpDateNoTermMonths() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			CommercialCustomerProcessing commImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			commCust = commImpl.createCommercialCustomer();
			commCust.setCustomerType("COMMERCIAL");
			commCust = commImpl.saveCommercialCustomer(commCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("123 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4L 8Y8");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", LocalDate.now(), insurerAddress, userProfile);
			assertNotNull(pcy);
			
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testCreateNewInsurancePolicyWithPersonalCustomer() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			PersonalCustomerProcessing persImpl = new PersonalCustomerProcessing();
			Customer persCust = null;
			persCust = persImpl.createPersonalCustomer();
			persCust.setCustomerType("PERSONAL");
			persCust = persImpl.savePersonalCustomer(persCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("123 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4L 8Y8");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(persCust, "TOWING", "TO", LocalDate.now(), insurerAddress, userProfile);
			assertNotNull(pcy);
			
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testCreateNewInsurancePolicyWithExpDateNoTermMonths() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			CommercialCustomerProcessing commImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			commCust = commImpl.createCommercialCustomer();
			commCust.setCustomerType("COMMERCIAL");
			commCust = commImpl.saveCommercialCustomer(commCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("456 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4M 8Y9");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			LocalDate effDate = LocalDate.now();
			LocalDate expDate = effDate.plusWeeks(3);
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", effDate, expDate, null, insurerAddress, userProfile);
			assertNotNull(pcy);
			
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testCreateNewInsurancePolicyNoExpDateWithTermMonths() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			CommercialCustomerProcessing commImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			commCust = commImpl.createCommercialCustomer();
			commCust.setCustomerType("COMMERCIAL");
			commCust.getCommercialCustomer().setLegalName("Test Commercial Customer Inc.");
			commCust = commImpl.saveCommercialCustomer(commCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("789 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4N 8Y6");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			LocalDate effDate = LocalDate.now();
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", effDate, null, 6, insurerAddress, userProfile);
			assertNotNull(pcy);
			
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testUpdatePolicyVersion_FromInsurancePolicy() {
		String policyNumber = String.valueOf(LocalDateTime.now().getNano());
		try {
			//SpecialtyAutoPolicyProcesses impl = new SpecialtyAutoPolicyProcesses();
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			pcy.setBasePolicyNum(policyNumber);
			assertNotNull(pcy.getPolicyVersions());
			PolicyVersion pcyVer = pcy.getPolicyVersions().get(0);
			assertNotNull(pcyVer);
			pcyVer.setProducerCommissionRate(5.5);
			pcy = impl.updateInsurancePolicy(pcy, userProfile);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testUpdatePolicyAndPolicyVersion_FromPolicyTransaction() {
		String policyNumber = String.valueOf(LocalDateTime.now().getNano());
		try {
			//SpecialtyAutoPolicyProcesses impl = new SpecialtyAutoPolicyProcesses();
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			pcy.setBasePolicyNum(policyNumber);
			pcy = impl.updateInsurancePolicy(pcy, userProfile);
			
			PolicyTransaction<Risk> pcyTxn = (PolicyTransaction<Risk>) impl.findLatestPolicyTransaction(policyNumber);
			assertNotNull(pcyTxn);
			PolicyVersion pcyVer = pcyTxn.getPolicyVersion();
			assertNotNull(pcyVer);
			pcyVer.setProducerCommissionRate(7.5);
			pcy = (SpecialtyAutoPackage) pcyTxn.getPolicyVersion().getInsurancePolicy();
			pcy.setNamedInsured("testUpdatePolicyAndPolicyVersion_FromPolicyTransaction");
			if (pcy.getInsuredAddress() != null) {
				pcy.getInsuredAddress().setAddressLine1("345 Elm Drive");
				pcy.getInsuredAddress().setCity("Brampton");
				pcy.getInsuredAddress().setProvState("ON");
				pcy.getInsuredAddress().setPostalZip("L4L 1M1");
				pcy.getInsuredAddress().setCountry("CA");
			}
			//impl.updateInsurancePolicy((SpecialtyAutoPackage) pcyTxn.getPolicyVersion().getInsurancePolicy());
			impl.updatePolicyTransaction(pcyTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testFinderMethods() {
		String policyNumber = String.valueOf(LocalDateTime.now().getNano());
		try {
			//SpecialtyAutoPolicyProcesses impl = new SpecialtyAutoPolicyProcesses();
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			pcy.setBasePolicyNum(policyNumber);
			pcy = impl.updateInsurancePolicy(pcy, userProfile);
		
			List<PolicyVersion> pcyVers1 			= impl.findAllPolicyVersions(policyNumber);
			assertNotNull(pcyVers1);
			assertFalse(pcyVers1.isEmpty());
			
			List<PolicyVersion> pcyVers2 			= impl.findAllVersionsByTerm(policyNumber, 1);
			assertNotNull(pcyVers2);
			assertFalse(pcyVers2.isEmpty());
			
			PolicyVersion latestPcyVer 				= impl.findLatestPolicyVersion(policyNumber);
			assertNotNull(latestPcyVer);
			
			PolicyTransaction<?> pcyTxn 			= impl.findLatestPolicyTransaction(policyNumber);
			assertNotNull(pcyTxn);
			
			PolicyTransaction<?> pcyTxn_fromQuote 	= impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			
			List<PolicyVersion> pcyVers3			= impl.findAllPolicyVersionsByPolicyNumber(policyNumber);
			assertNotNull(pcyVers3);
			assertFalse(pcyVers3.isEmpty());
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}

	@Test
	void testPolicySearch() {
		//String policyNumber = "123456";
		String policyNumber = String.valueOf(LocalDateTime.now().getNano());
		String insuredName = "ACME Towing Company";
		try {
			//SpecialtyAutoPolicyProcesses impl = new SpecialtyAutoPolicyProcesses();
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			CommercialCustomerProcessing commCustImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			try {
				commCust = commCustImpl.createCommercialCustomer();
			} catch (CommercialCustomerException e) {
				fail("An exception occurred", e);
			}
			commCust.getCommercialCustomer().setLegalName(insuredName);
			try {
				commCust = commCustImpl.saveCommercialCustomer(commCust, userProfile);
			} catch (CommercialCustomerException e) {
				fail("An exception occurred", e);
			}
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			//pcy.setBasePolicyNum(policyNumber);
			//pcy = impl.updateInsurancePolicy(pcy, userProfile);
			
			try {
				List<PolicySearchResults> searchResults = impl.findPolicies(null, insuredName, null, quoteNum);
				assertNotNull(searchResults);
				assertFalse(searchResults.isEmpty());
				assertTrue(searchResults.size() == 1);
				PolicySearchResults result = searchResults.get(0);
				PolicyTransaction<Risk> pcyTxn = (PolicyTransaction<Risk>) impl.findLatestPolicyTransactionByQuoteNumber(quoteNum, result.getPolicyTerm(), result.getVersionNumber());
				assertNotNull(pcyTxn);
				
			} catch (SpecialtyAutoPolicyException e) {
				fail("An exception occurred", e);
			}
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testPolicySearchNotFound() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");			
			List<PolicySearchResults> searchResults = impl.findPolicies(null, "Does Not Exist", null, "0000001");
			assertNotNull(searchResults);
			assertTrue(searchResults.isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void testPolicySearchForRenewals() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			List<PolicySearchResults> searchResults = impl.findPolicies(null, null, null, "47");
		} catch (SpecialtyAutoPolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testLienHolderLessorMappings() {
		String policyNumber = String.valueOf(LocalDateTime.now().getNano());
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			pcy.setBasePolicyNum(policyNumber);
			pcy = impl.updateInsurancePolicy(pcy, userProfile);
			
			LienholderLessor lessor = new LienholderLessor();
			lessor.setBusinessStatus("ACTIVE");
			lessor.setCompanyName("My Lien Holder Lessor Company" + Math.random());
			lessor.getLienholderLessorAddress().setAddressLine1("123 Elm Drive");
			lessor.getLienholderLessorAddress().setCity("Toronto");
			lessor.getLienholderLessorAddress().setProvState("ON");
			lessor.getLienholderLessorAddress().setPostalZip("L4L 1M1");
			lessor.getLienholderLessorAddress().setCountry("CA");
			lessor.setLienLessorType("LESSOR");
			lessor = impl.saveLienholderLessor(lessor, userProfile);
			
			LienholderLessor lessor2 = new LienholderLessor();
			lessor2.setBusinessStatus("ACTIVE");
			lessor2.setCompanyName("My Lienholder Lessor Company Two" + Math.random());
			lessor2.getLienholderLessorAddress().setAddressLine1("234 Main Street");
			lessor2.getLienholderLessorAddress().setCity("Toronto");
			lessor2.getLienholderLessorAddress().setProvState("ON");
			lessor2.getLienholderLessorAddress().setPostalZip("L5L 2M2");
			lessor2.getLienholderLessorAddress().setCountry("CA");
			lessor2.setLienLessorType("LESSOR");
			lessor2 = impl.saveLienholderLessor(lessor2, userProfile);
			
			PolicyTransaction<SpecialtyVehicleRisk> pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.findLatestPolicyTransaction(policyNumber);
			assertNotNull(pcyTxn);
			
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			assertNotNull(subPcy);
			SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh);
			SpecialtyVehicleRisk veh2 = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh2);
			
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			
			/*
			LienholderLessorSubPolicy lhlsp1 = new LienholderLessorSubPolicy();
			lhlsp1.setLienholderLessor(lessor2);
			lhlsp1.setLienLessorType("LESSOR");
			subPcy.addLienholderLessorSubPolicy(lhlsp1);
			*/
			subPcy.addLienholderLessor(lessor, "LESSOR");
			subPcy.addLienholderLessor(lessor2, "LIENHOLDER");
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);

			/*
			veh.addLienholderLessor(lessor);
			veh = impl.updateSpecialtyVehicleRisk(veh);
			
			veh2.addLienholderLessor(lessor);
			veh2 = impl.updateSpecialtyVehicleRisk(veh2);
			
			veh.removeLienholderLessor(lessor);
			veh = impl.updateSpecialtyVehicleRisk(veh);
			*/
			
			subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			LienholderLessorSubPolicy lhlsp1 = subPcy.getLienholderLessors().iterator().next();
			subPcy.removeLienholderLessorSubPolicy(lhlsp1);
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			
			//impl.deleteSpecialtyVehicleRisk(veh);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		} catch (SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testLienholderLessorSearchAndLoad() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			LienholderLessor lessor = new LienholderLessor();
			lessor.setBusinessStatus("ACTIVE");
			lessor.setCompanyName("My Lien Holder Lessor Company");
			lessor.getLienholderLessorAddress().setAddressLine1("123 Elm Drive");
			lessor.getLienholderLessorAddress().setCity("Toronto");
			lessor.getLienholderLessorAddress().setProvState("ON");
			lessor.getLienholderLessorAddress().setPostalZip("L4L 1M1");
			lessor.getLienholderLessorAddress().setCountry("CA");
			lessor.setLienLessorType("LESSOR");
			lessor = impl.saveLienholderLessor(lessor, userProfile);
			
			LienholderLessor lessor2 = new LienholderLessor();
			lessor2.setBusinessStatus("ACTIVE");
			lessor2.setCompanyName("My Other Lien Holder Lessor Company");
			lessor2.getLienholderLessorAddress().setAddressLine1("700 Main Street");
			lessor2.getLienholderLessorAddress().setCity("Toronto");
			lessor2.getLienholderLessorAddress().setProvState("ON");
			lessor2.getLienholderLessorAddress().setPostalZip("M4M 2N2");
			lessor2.getLienholderLessorAddress().setCountry("CA");
			lessor2.setLienLessorType("LESSOR");
			lessor2 = impl.saveLienholderLessor(lessor, userProfile);
			
			List<LienholderLessorSearchResults> results = impl.findLienholderLessors("Company", "Toronto", null);
			assertNotNull(results);
			assertFalse(results.isEmpty());
			
			LienholderLessor lhl = impl.loadLienholderLessor(results.get(0).getId());
			assertNotNull(lhl);
		} catch (InsurancePolicyException e) {
			fail("Could not save lien holder lessor", e);
		}
	}
	
	@Test
	void testDeclineQuote() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
		
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			UserProfile userProf = new UserProfile();
			userProf.setLoginId("maziz");
			pcyTxn_fromQuote = impl.declineQuote(pcyTxn_fromQuote, "From Echeclon", "Decline description here...", userProf);
			assertNotNull(pcyTxn_fromQuote);
			assertEquals(Constants.TERM_STATUS_DECLINED, pcyTxn_fromQuote.getPolicyTerm().getBusinessStatus());
			assertEquals(Constants.VERSION_STATUS_DECLINED, pcyTxn_fromQuote.getPolicyVersion().getBusinessStatus());
			assertEquals("From Echeclon", pcyTxn_fromQuote.getPolicyVersion().getChangeReasonCd());
			assertEquals("Decline description here...", pcyTxn_fromQuote.getPolicyVersion().getChangeReasonDetails());
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testNotRequireQuote() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
		
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			pcyTxn_fromQuote = impl.notRequireQuote(pcyTxn_fromQuote, "By Broker", "Not require quote description here...", userProfile);
			assertNotNull(pcyTxn_fromQuote);
			assertEquals(Constants.TERM_STATUS_NOTREQUIRED, pcyTxn_fromQuote.getPolicyTerm().getBusinessStatus());
			assertEquals(Constants.VERSION_STATUS_NOTREQUIRED, pcyTxn_fromQuote.getPolicyVersion().getBusinessStatus());
			assertEquals("By Broker", pcyTxn_fromQuote.getPolicyVersion().getChangeReasonCd());
			assertEquals("Not require quote description here...", pcyTxn_fromQuote.getPolicyVersion().getChangeReasonDetails());
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testIssueQuote() {
		try {
			Producer prdcr = new Producer();
			prdcr.setLegalName("ACME Producer Company " + Math.random());
			try {
				prdcr = (new BrokerProcesses()).saveBroker(prdcr, userProfile);
			} catch (BrokerException e) {
				fail("Could not create broker", e);
			}
			
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			
			pcy = (SpecialtyAutoPackage) pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy();
			pcy.setPolicyProducer(prdcr);
			pcy.setNamedInsured("testIssueQuote");
			if (pcy.getInsuredAddress() != null) {
				pcy.getInsuredAddress().setAddressLine1("345 Elm Drive");
				pcy.getInsuredAddress().setCity("Brampton");
				pcy.getInsuredAddress().setProvState("ON");
				pcy.getInsuredAddress().setPostalZip("L4L 1M1");
				pcy.getInsuredAddress().setCountry("CA");
			}
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn_fromQuote.findSubPolicyByType("AUTO");
			SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(autoSP);
			pcyTxn_fromQuote = impl.updatePolicyTransaction(pcyTxn_fromQuote, userProfile);
			pcyTxn_fromQuote = impl.ratePolicyTransaction(pcyTxn_fromQuote, userProfile);
			
			pcyTxn_fromQuote = impl.issueQuote(pcyTxn_fromQuote, userProfile);
			assertNotNull(pcyTxn_fromQuote);
			assertEquals(Constants.TERM_STATUS_QUOTED, pcyTxn_fromQuote.getPolicyTerm().getBusinessStatus());
			assertEquals(Constants.VERSION_STATUS_QUOTED, pcyTxn_fromQuote.getPolicyVersion().getBusinessStatus());
			assertNotNull(pcyTxn_fromQuote.getPolicyDocuments());
			assertFalse(pcyTxn_fromQuote.getPolicyDocuments().isEmpty());
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		} catch (SpecialtyAutoPolicyException e) {
			fail("Specialty Vehicle Risk Error", e);
		}
	}
	
	@Test
	void testBindQuote() {
		try {
			Producer prdcr = new Producer();
			prdcr.setLegalName("ACME Producer Company For Bind " + Math.random());
			try {
				prdcr = (new BrokerProcesses()).saveBroker(prdcr, userProfile);
			} catch (BrokerException e) {
				fail("Could not create broker", e);
			}
			
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			
			pcy = (SpecialtyAutoPackage) pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy();
			pcy.setPolicyProducer(prdcr);
			pcy.setNamedInsured("testIssueQuote");
			if (pcy.getInsuredAddress() != null) {
				pcy.getInsuredAddress().setAddressLine1("789 Maingate Drive");
				pcy.getInsuredAddress().setCity("Mississauga");
				pcy.getInsuredAddress().setProvState("ON");
				pcy.getInsuredAddress().setPostalZip("L4N 3M3");
				pcy.getInsuredAddress().setCountry("CA");
			}
			pcyTxn_fromQuote = impl.updatePolicyTransaction(pcyTxn_fromQuote, userProfile);
			
			pcyTxn_fromQuote = impl.issueQuote(pcyTxn_fromQuote, userProfile);
			
			pcyTxn_fromQuote = impl.bindQuote(pcyTxn_fromQuote, userProfile);
			
			assertNotNull(pcyTxn_fromQuote);
			assertEquals(Constants.TERM_STATUS_BOUND, pcyTxn_fromQuote.getPolicyTerm().getBusinessStatus());
			assertEquals(Constants.VERSION_STATUS_BOUND, pcyTxn_fromQuote.getPolicyVersion().getBusinessStatus());
			assertTrue(StringUtils.isNoneBlank(pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy().getBasePolicyNum()));
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testIssuePolicy() {
		try {
			Producer prdcr = new Producer();
			prdcr.setLegalName("ACME Producer Company For Issue Policy " + Math.random());
			try {
				prdcr = (new BrokerProcesses()).saveBroker(prdcr, userProfile);
			} catch (BrokerException e) {
				fail("Could not create broker", e);
			}
			
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			
			pcy = (SpecialtyAutoPackage) pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy();
			pcy.setPolicyProducer(prdcr);
			pcy.setNamedInsured("testIssueQuote");
			if (pcy.getInsuredAddress() != null) {
				pcy.getInsuredAddress().setAddressLine1("456 Eastgate Street");
				pcy.getInsuredAddress().setCity("Toronto");
				pcy.getInsuredAddress().setProvState("ON");
				pcy.getInsuredAddress().setPostalZip("L4Z 5M5");
				pcy.getInsuredAddress().setCountry("CA");
			}
			pcyTxn_fromQuote = impl.updatePolicyTransaction(pcyTxn_fromQuote, userProfile);
			
			pcyTxn_fromQuote = impl.issueQuote(pcyTxn_fromQuote, userProfile);
			pcyTxn_fromQuote = impl.bindQuote(pcyTxn_fromQuote, userProfile);
			pcyTxn_fromQuote = impl.issuePolicy(pcyTxn_fromQuote, userProfile);
			
			assertNotNull(pcyTxn_fromQuote);
			assertEquals(Constants.TERM_STATUS_ISSUED, pcyTxn_fromQuote.getPolicyTerm().getBusinessStatus());
			assertEquals(Constants.VERSION_STATUS_ISSUED, pcyTxn_fromQuote.getPolicyVersion().getBusinessStatus());
			assertTrue(StringUtils.isNoneBlank(pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy().getBasePolicyNum()));
			assertNotNull(pcyTxn_fromQuote.getPolicyDocuments());
			assertFalse(pcyTxn_fromQuote.getPolicyDocuments().isEmpty());
			assertTrue(pcyTxn_fromQuote.getPolicyDocuments().size() > 1);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testRatePolicyMethods() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			
			PolicyTransaction<SpecialtyVehicleRisk> pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum());
			assertNotNull(pcyTxn);
			
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			assertNotNull(subPcy);
			SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh);
			SpecialtyVehicleRisk veh2 = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh2);
			
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");

			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.rollupPolicy(pcyTxn.getPolicyTransactionPK(), userProfile);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		} catch (SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testUpdatePolicyLiabilityLimit() {
		try {
			Producer prdcr = new Producer();
			prdcr.setLegalName("ACME Producer Company " + Math.random());
			try {
				prdcr = (new BrokerProcesses()).saveBroker(prdcr, userProfile);
			} catch (BrokerException e) {
				fail("Could not create broker", e);
			}
			
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			PolicyTransaction<?> pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber(quoteNum);
			assertNotNull(pcyTxn_fromQuote);
			
			pcyTxn_fromQuote = impl.updatePolicyLiabilityLimit(1000, pcyTxn_fromQuote, userProfile);
			assertEquals(1000, ((SpecialtyAutoPackage) pcyTxn_fromQuote.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit());
			
		} catch (InsurancePolicyException | SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testGetPolicyVersionByQuoteNumberWithTxns() {
		try {
			Producer prdcr = new Producer();
			prdcr.setLegalName("ACME Producer Company " + Math.random());
			try {
				prdcr = (new BrokerProcesses()).saveBroker(prdcr, userProfile);
			} catch (BrokerException e) {
				fail("Could not create broker", e);
			}
			
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			String quoteNum = pcy.getQuoteNum();
			
			List<PolicyVersion> allVersWithTxns = impl.findAllPolicyVersionByQuoteNumberAndTerm(quoteNum, 1);
			assertNotNull(allVersWithTxns);
			assertFalse(allVersWithTxns.isEmpty());
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testFindStatCodesByPostalCode() {
		String postalCode = "K0A 0A1";
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			StatCodes sc = impl.findStatCodesByPostalCode(postalCode);
			assertNotNull(sc);
		} catch (InsurancePolicyException e) {
			fail("An excpetion occurred", e);
		}
		
		postalCode = "K0A 9A9";
		try {
			StatCodes sc = impl.findStatCodesByPostalCode(postalCode);
		} catch (Exception e) {
			assertTrue(e instanceof InsurancePolicyException);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testCopyQuote() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			
			LienholderLessor lessor = new LienholderLessor();
			lessor.setBusinessStatus("ACTIVE");
			lessor.setCompanyName("My Lien Holder Lessor Company" + Math.random());
			lessor.getLienholderLessorAddress().setAddressLine1("123 Elm Drive");
			lessor.getLienholderLessorAddress().setCity("Toronto");
			lessor.getLienholderLessorAddress().setProvState("ON");
			lessor.getLienholderLessorAddress().setPostalZip("L4L 1M1");
			lessor.getLienholderLessorAddress().setCountry("CA");
			lessor.setLienLessorType("LESSOR");
			
			lessor = impl.saveLienholderLessor(lessor, userProfile);
			
			PolicyTransaction<?> pcyTxn = impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum());
			assertNotNull(pcyTxn);
			
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			assertNotNull(subPcy);
			SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh);
			SpecialtyVehicleRisk veh2 = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh2);
			subPcy.addLienholderLessor(lessor, "LESSOR");
			
			pcyTxn = impl.updatePolicyTransaction(pcyTxn, userProfile);
			
			PolicyLocation newLoc = impl.createPolicyLocation((PolicyTransaction<PolicyLocation>) pcyTxn);
			CGLSubPolicy<?> cglSubPcy = impl.createCGLSubPolicy((PolicyTransaction<SpecialtyVehicleRisk>) pcyTxn);
			CGLLocation cglLocation = new CGLLocation();
			cglLocation.setPolicyLocation(newLoc);
			cglSubPcy.addCglLocation(cglLocation);
			pcyTxn = impl.updatePolicyTransaction(pcyTxn, userProfile);
			
			PolicyTransaction<SpecialtyVehicleRisk> newQuote = (PolicyTransaction<SpecialtyVehicleRisk>) impl.copyQuote(pcyTxn, userProfile);
			assertNotNull(newQuote);

			subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) newQuote.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
			assertNotNull(subPcy);
			assertNotNull(subPcy.getSubPolicyRisks());
			assertFalse(subPcy.getSubPolicyRisks().isEmpty());
			assertEquals(2, subPcy.getSubPolicyRisks().size());
		}
		catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		} catch (SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testCGLandCGO_SubPolicies() {
		try {
			ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
			SpecialtyAutoPackage pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy.getQuoteNum());
			
			PolicyTransaction<SpecialtyVehicleRisk> pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum());
			assertNotNull(pcyTxn);
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyLiabilityLimit(1000, pcyTxn, userProfile);
			
			CargoSubPolicy<?> cargoSP = impl.createCargoSubPolicy(pcyTxn);
			assertNotNull(cargoSP);
			assertNotNull(cargoSP.getCoverageDetail().getCoverages());
			Coverage cpe = cargoSP.getCoverageDetail().getCoverageByCode(SpecialtyAutoConstants.SUBPCY_COV_CODE_CPE);
			assertNotNull(cpe);
			assertEquals(SpecialtyAutoConstants.SUBPCY_COV_CODE_CPE_DEF_PREM, cpe.getCoveragePremium().getOriginalPremium());
			
			CGLSubPolicy<?> cglSP = impl.createCGLSubPolicy(pcyTxn);
			assertNotNull(cglSP);
			assertEquals(1000, cglSP.getCglLimit());
			
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		} catch (SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testAdditionalInsureds() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		SpecialtyAutoPackage pcy;
		try {
			pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
			assertNotNull(pcy.getQuoteNum());
			PolicyTransaction<SpecialtyVehicleRisk> pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum());
			assertNotNull(pcyTxn);
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			assertNotNull(subPcy);
			AdditionalInsured ai = new AdditionalInsured();
			ai.setAdditionalInsuredName("Additional Insured Name two");
			ai.setAdditionalInsuredType("TYPE_2");
			ai.setBusinessStatus("ACTIVE_2");
			ai.setDescription("AI Description Teo");
			ai.setRelationship("Relationship_2");
			Address aiAddress = new Address();
			aiAddress.setAddressLine1("3455 Main Street");
			ai.setAdditionalInsuredAddress(aiAddress);
			subPcy.addAdditionalIsnured(ai);
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			
			PolicyTransaction<?> pcyTxnCopy = impl.copyQuote(pcyTxn, userProfile);
			
		} catch (InsurancePolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testPrimaryPolicyLocation() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			CommercialCustomerProcessing commImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			commCust = commImpl.createCommercialCustomer();
			commCust.setCustomerType("COMMERCIAL");
			commCust = commImpl.saveCommercialCustomer(commCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("123 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4L 8Y8");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", LocalDate.now(), insurerAddress, userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			assertNotNull(pcy.getPolicyTerms());
			assertEquals(pcy.getPolicyTerms().size(), 1);
			assertNotNull(pcy.getPolicyVersions());
			assertEquals(pcy.getPolicyVersions().size(), 1);
			
			PolicyTerm term = pcy.getPolicyTerms().get(0);
			PolicyVersion vers = pcy.getPolicyVersions().get(0);
			
			PolicyTransaction<Risk> pcyTxn = (PolicyTransaction<Risk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum(), term.getTermNumber(), vers.getPolicyVersionNum());
			assertNotNull(pcyTxn);
			assertNotNull(pcyTxn.getPolicyRisks());
			assertTrue(pcyTxn.getPolicyRisks().isEmpty());
			
			pcyTxn = (PolicyTransaction<Risk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			assertEquals(1, pcyTxn.getPolicyRisks().size());
			
			pcyTxn = (PolicyTransaction<Risk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			assertEquals(1, pcyTxn.getPolicyRisks().size());
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void testUpdateSpecialtyAutoPackageData() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			CommercialCustomerProcessing commImpl = new CommercialCustomerProcessing();
			Customer commCust = null;
			commCust = commImpl.createCommercialCustomer();
			commCust.setCustomerType("COMMERCIAL");
			commCust = commImpl.saveCommercialCustomer(commCust, userProfile);
			
			Address insurerAddress = new Address();
			insurerAddress.setAddressLine1("123 Random Street");
			insurerAddress.setCity("Tornoto");
			insurerAddress.setProvState("ON");
			insurerAddress.setPostalZip("L4L 8Y8");
			insurerAddress.setCountry("CA");
			(new AddressDAO()).save(insurerAddress);
			
			SpecialtyAutoPackage pcy = impl.createNewInsurancePolicy(commCust, "TOWING", "TO", LocalDate.now(), insurerAddress, userProfile);
			assertNotNull(pcy);
			assertNotNull(pcy.getQuoteNum());
			assertNotNull(pcy.getPolicyTerms());
			assertEquals(pcy.getPolicyTerms().size(), 1);
			assertNotNull(pcy.getPolicyVersions());
			assertEquals(pcy.getPolicyVersions().size(), 1);
			
			PolicyTransaction<Risk> pcyTxn = (PolicyTransaction<Risk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum(), 1, 1);
			assertNotNull(pcyTxn);
			
			LocalDateTime termEff = LocalDateTime.now().plusMonths(2);
			pcyTxn = (PolicyTransaction<Risk>) impl.updateSpecialtyAutoPackageData(1000, "Mr. Quote Dude", termEff, pcyTxn.getPolicyTransactionPK(), userProfile);
			
		} catch (Exception e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testSubPctAndRiskFinders() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		SpecialtyAutoPackage pcy;
		try {
			pcy = impl.createNewCustomerAndInsurancePolicy("COMMERCIAL", "TOWING", "TO", LocalDate.now(), userProfile);
				
			PolicyTransaction<SpecialtyVehicleRisk> pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.findLatestPolicyTransactionByQuoteNumber(pcy.getQuoteNum());
			assertNotNull(pcyTxn);
			
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			assertNotNull(subPcy);
			SpecialtyVehicleRisk veh = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh);
			SpecialtyVehicleRisk veh2 = impl.createSpecialtyVehicleRisk(subPcy);
			assertNotNull(veh2);
			
			pcyTxn = (PolicyTransaction<SpecialtyVehicleRisk>) impl.updatePolicyTransaction(pcyTxn, userProfile);
			subPcy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType("AUTO");
			Long vehPK = subPcy.getSubPolicyRisks().iterator().next().getPolicyRiskPK();
			
			SubPolicy<Risk> anotherSubPcy = (SubPolicy<Risk>) impl.findSubPolicyByPK(subPcy.getSubPolicyPK());
			assertNotNull(anotherSubPcy);
			assertTrue(anotherSubPcy instanceof SpecialityAutoSubPolicy);
			
			Risk anotherRisk = impl.findRiskByPK(vehPK);
			assertNotNull(anotherRisk);
			assertTrue(anotherRisk instanceof SpecialtyVehicleRisk);
		} catch (InsurancePolicyException | SpecialtyAutoPolicyException e) {
			fail("An exception occurred", e);
		}
	}
	
	@Test
	void testRatePolicy() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> pcyTxn_fromQuote;
		try {
			pcyTxn_fromQuote = impl.findLatestPolicyTransactionByQuoteNumber("CAON200001");
			assertNotNull(pcyTxn_fromQuote);
			System.out.println("Trying to run rating now");
			pcyTxn_fromQuote = impl.ratePolicyTransaction(pcyTxn_fromQuote.getPolicyTransactionPK(), userProfile);
		} catch (InsurancePolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	void testReversePolicyChange() {
		final Long SOURCE_TXN_PK = 151L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(SOURCE_TXN_PK);
		if (sourceTxn != null) {
			try {
				PolicyTransaction<?> revTxn = impl.reversePolicyChange(sourceTxn, userProfile);
				//assertEquals(Constants.SYSTEN_STATUS_INACTIVE, revTxn.getPolicyVersion().getSystemStatus());
				//assertEquals(Constants.BUSINESS_STATUS_REVERSAL, revTxn.getPolicyVersion().getBusinessStatus());
				//assertNotNull(revTxn.getPolicyVersion().getReversedVersionNum(), "Reversed Version Number should be populaged");
				assertEquals(Constants.SYSTEN_STATUS_INACTIVE, revTxn.getSystemStatus());
				assertEquals(Constants.BUSINESS_STATUS_REVERSAL, revTxn.getBusinessStatus());
			} catch (InsurancePolicyException e) {
				e.printStackTrace();
			}
		} else {
			throw new RuntimeException("Could not load policy transaction with PK: " + SOURCE_TXN_PK);
		}
	}
	
	@Test
	void testGetSpecialtyVehicleRiskDTO() {
		final List<Long> PKLIST = Arrays.asList(2L, 4L, 6L, 8L);
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			Map<Long, SpecialtyVehicleRiskDTO> dtoMap = impl.getVehicleRiskDTOByPk(PKLIST);
			assertFalse(dtoMap.isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			throw new RuntimeException("Could not load vehicle risk DTO map with PK List: " + PKLIST);
		}
	}
	
	@Test
	void testCreatePolicyRenewal() {
		final Long SOURCE_TXN_PK = 110L;
		
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(SOURCE_TXN_PK);
		if (sourceTxn != null) {
			try {
				PolicyTransaction<?> rwlTxn = impl.createPolicyRenewal(sourceTxn, "Renewal Testing", Boolean.TRUE, Boolean.TRUE, userProfile);
			} catch (InsurancePolicyException e) {
				e.printStackTrace();
			}
		} else {
			throw new RuntimeException("Could not load policy transaction with PK: " + SOURCE_TXN_PK);
		}
	}
	
	@Test
	void testGetPcyHistoryByPcyNumber() {
		final String PCY_NUM = "OTO212015115LOCAL";
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		
		List<PolicyHistoryDTO> dtoList;
		try {
			dtoList = impl.findPolicyHistoryByPolicyNumber(PCY_NUM);
			assertEquals(3, dtoList.size());
		} catch (InsurancePolicyException e) {
			fail(e);
		}
	}
	
	@Test
	void testGetPolicyHistoryByQuoteNumber() {
		final String QUOTE_NUM = "CAON200047LOCAL";
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		
		List<PolicyHistoryDTO> dtoList;
		try {
			dtoList = impl.findPolicyHistoryByQuoteNumber(QUOTE_NUM);
			assertEquals(3, dtoList.size());
		} catch (InsurancePolicyException e) {
			fail(e);
		}
	}
	
	@Test
	void testUpdatePolicyVersionDescription() {
		final Long PCY_VER_PK = 80L;
		final String PCY_VER_DESC = "Marwan Test";

		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			Boolean updateResult = impl.updatePolicyVersionDescription(PCY_VER_PK, PCY_VER_DESC);
			assertTrue(updateResult);
		} catch (InsurancePolicyException e) {
			fail(e);
		}
	}
	
	@Test
	void testGetPolicyTxnPremByTerm() {
		final Long PCY_TERM_PK = 29L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		
		try {
			List<PolicyTransactionPremiumDTO> dtoList = impl.findPolicyTransactionPremiumByTermPk(PCY_TERM_PK);
			assertEquals(1, dtoList.size());
		} catch (InsurancePolicyException e) {
			fail(e);
		}
	}
	
	@Test
	void testUpdateTermPrem() {
		final Long PCY_TERM_PK = 29L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTermPremiumDTO dto = new PolicyTermPremiumDTO();
		dto.setPolicyTermPK(PCY_TERM_PK);
		dto.setTermPremium(new PremiumDTO());
		
		try {
			Boolean updateResult = impl.updatePolicyTermPremium(dto);
			assertTrue(updateResult);
		} catch (InsurancePolicyException e) {
			fail(e);
		}
	}
	
	@Test
	void testFlatPolicyCancellation() {
		final Long PCY_TXN_PK = 298L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		LocalDate cancEffDate = LocalDate.of(2021, 1, 1);
		try {
			PolicyTransaction<?> cancTxn = impl.createPolicyCancellation(sourceTxn, cancEffDate, Constants.CANCELLATION_TYPE_FLAT, "Cancellation Reason", userProfile);
			assertNotNull(cancTxn);
		} catch (InsurancePolicyException e) {
			//e.printStackTrace();
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testIssueCancellation() {
		final Long PCY_TXN_PK = 301L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> cancTxn = impl.issuePolicyCancellation(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testCopyRenewalQuote() {
		final Long PCY_TXN_PK = 402L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> rwlQuoteTxn = impl.copyRenewalQuote(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testBindRenewal() {
		final Long PCY_TXN_PK = 402L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> rwlQuoteTxn = impl.bindPolicyRenewal(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testCreateReinstatement() {
		final Long PCY_TXN_PK = 421L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> reiQuoteTxn = impl.createPolicyReinstatement(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testIssueReinstatement() {
		final Long PCY_TXN_PK = 421L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> reiQuoteTxn = impl.issuePolicyReinstatement(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testCreateReissue() {
		final Long PCY_TXN_PK = 430L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> reiQuoteTxn = impl.createPolicyReissue(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testDeclineReissue() {
		final Long PCY_TXN_PK = 427L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> reiQuoteTxn = impl.declinePolicyReissue(sourceTxn, "DECLINE_REASON_CD", "DECLINE_REASON_DESC", userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testIssueReissue() {
		final Long PCY_TXN_PK = 432L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		try {
			PolicyTransaction<?> reiQuoteTxn = impl.issuePolicyReissue(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Exception occurred....Test failed", e);
		}
	}
	
	@Test
	void testGetAllBrokersByInsProduct() {
		BrokerProcesses impl = new BrokerProcesses();
		String inMarketCd = "";
		String inProductCd = "TOWING";
		String inProgramCd = "TO"; 
		String inClientType = ""; 
		String inJurisdictions = "ON";
		String inTransactionTypes = "";
		LocalDate inEffDate = LocalDate.now();
		
		try {
			List<Producer> rv = impl.getAllBrokersByInsProduct(inMarketCd, inProductCd, inProgramCd, inClientType, inJurisdictions, inTransactionTypes, inEffDate);
			assertFalse(rv == null, "List of producers should not be NULL");
			assertFalse(rv.isEmpty(), "List of producers should not be empty");
			assertTrue(rv.size() == 2);
		} catch (BrokerException e) {
			fail("Could not get list of producers", e);
		}
	}
	
	@Test
	void testSpecialityAutoExpRating() {
		final Long PCY_TXN_PK = 543L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction<?> sourceTxn = (new PolicyTransactionDAO()).findById(PCY_TXN_PK);
		assertNotNull(sourceTxn);
		SpecialityAutoExpRating saer = new SpecialityAutoExpRating();
		saer.setAverageKMS((double) 10);
		saer.setClaimsFrequency((double) 2);
		saer.setCommercialCreditScore(700);
		saer.setDangerousMaterials((double) 100);
		saer.setExpectedMileage((double) 10000);
		saer.setModificationFactor(0.5);
		saer.setNumberOfPowerUnits(20);
		saer.setOffBalanceFactor(0.2);
		saer.setPreventionRating((double) 120);
		saer.setYearsInBusiness(5);
		saer.setZone1Exp(0.1);
		saer.setZone2Exp(0.2);
		saer.setZone3Exp(0.3);
		saer.setZone4Exp(0.4);
		saer.setZone5Exp(0.5);
		saer.setZone6Exp(0.6);
		saer.setZone7Exp(0.7);
		saer.setZone8Exp(0.8);
		saer.setZone9Exp(0.9);
		sourceTxn.addPolicyTransactionData(saer);
		try {
			sourceTxn = impl.updatePolicyTransaction(sourceTxn, userProfile);
		} catch (InsurancePolicyException e) {
			fail("Could not add SpecialityAutoExpRating", e);
		}
	}
	/*
	@Test
	void testGetEchelonReinsuranceItemsByTxnPk() {
		final Long PCY_TXN_PK = 596L;
		SpecialtyVehicleRiskDAO dao = new SpecialtyVehicleRiskDAO();
		ReinsuranceItemDTO dto = dao.getEchelonReinsuranceItemsByTxnPk(PCY_TXN_PK);
		assertNotNull(dto);
		assertNotNull(dto.getNetPremium());
		System.out.println("Net Premium: " + dto.getNetPremium());
	}
	
	@Test
	void testGetReinsuranceReportDTO() {
		final Long PCY_TXN_PK = 596L;
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			ReinsuranceReportDTO dto = impl.getReinsuranceReportDTO(PCY_TXN_PK);
			assertNotNull(dto);
			assertNotNull(dto.getReisnuranceItems());
			assertFalse(dto.getReisnuranceItems().isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			fail("Resinsurance report failed", e);
		}
		
	}
	*/
}
