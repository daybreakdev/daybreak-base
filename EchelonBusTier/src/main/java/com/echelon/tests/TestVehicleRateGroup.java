package com.echelon.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.echelon.domain.policy.specialtylines.VehicleRateGroup;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;

class TestVehicleRateGroup {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testSearch_All_Params() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			List<VehicleRateGroup> rv = impl.findVehicleRateGroup(1, 0D, 10000D);
			assertNotNull(rv);
			assertFalse(rv.isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			fail("An excpetion occurred", e);
		}
	}
	
	@Test
	void testSearch_VehAgeOnly() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			List<VehicleRateGroup> rv = impl.findVehicleRateGroup(1, null, null);
			assertNotNull(rv);
			assertFalse(rv.isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			fail("An excpetion occurred", e);
		}
	}

	@Test
	void testSearch_VehAge_lowLimit() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			List<VehicleRateGroup> rv = impl.findVehicleRateGroup(1, 10001D, null);
			assertNotNull(rv);
			assertFalse(rv.isEmpty());
		} catch (SpecialtyAutoPolicyException e) {
			fail("An excpetion occurred", e);
		}
	}

	@Test
	void testSearch_VehAge_exceedUpperLimit() {
		ISpecialtyAutoPolicyProcessing impl = (ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		try {
			List<VehicleRateGroup> rv = impl.findVehicleRateGroup(1, 332000D, 332000D);
			assertNotNull(rv);
			assertFalse(rv.isEmpty());
			assertSame(rv.get(0).getImpliedRateGroup(), 50); 
		} catch (SpecialtyAutoPolicyException e) {
			fail("An excpetion occurred", e);
		}
		
		try {
			List<VehicleRateGroup> rv = impl.findVehicleRateGroup(1, 332001D, 332001D);
			assertNotNull(rv);
			assertFalse(rv.isEmpty());
			assertSame(rv.get(0).getImpliedRateGroup(), 50); 
		} catch (SpecialtyAutoPolicyException e) {
			fail("An excpetion occurred", e);
		}
	}
}
