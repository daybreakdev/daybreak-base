package com.echelon.tests;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.junit.jupiter.api.Test;

import com.ds.ins.dal.dao.AddressDAO;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.CommercialCustomer;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.CommercialCustomerException;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.processes.customer.CommercialCustomerProcessing;
import com.ds.ins.services.premium.DSPremiumService;
import com.ds.ins.system.UserProfile;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class TestBuildTestCases {
	
	private class LocalSpecialtyAutoPolicyProcesses extends SpecialtyAutoPolicyProcesses {
		public PolicyTransaction<?> localUpdateOnsetTransaction(PolicyTransaction<?> inPcyTxn, UserProfile userProfile) throws InsurancePolicyException {
			return updateRenewalOnsetTransaction(inPcyTxn, Boolean.FALSE, userProfile);
		}
	}
	
	@Test
	void newPolicyWithExistingDataFull() throws Exception {
		//newPolicyWithExistingData("CAON200112");
		
		// SIM big policy
		newPolicyWithExistingData("CAON200002");
	}	
	
	private void newPolicyWithExistingData(String quoteNumber) throws Exception {
		LocalSpecialtyAutoPolicyProcesses localPolicyProcesses = new LocalSpecialtyAutoPolicyProcesses();
		
		UserProfile userProfile = new UserProfile();
		userProfile.setLoginId("daystar");
		
		SpecialtyAutoPolicyProcesses policyProcesses = (SpecialtyAutoPolicyProcesses) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction currTrans = policyProcesses.findLatestPolicyTransactionByQuoteNumber(quoteNumber, 1, 1); 
		if (currTrans != null) {
			currTrans = policyProcesses.findLatestPolicyTransaction(currTrans.getPolicyVersion().getInsurancePolicy().getBasePolicyNum());
		}
		assertNotNull(currTrans);
		currTrans = policyProcesses.loadPolicyTransactionFull(currTrans, true);
		PolicyTransaction<?> clonedTrans = (PolicyTransaction<?>) currTrans.clone();
		clonedTrans = policyProcesses.postCopyQuote(currTrans, clonedTrans);

		LocalDateTime now = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMdd-HH:mm:ss");
		String nowStr = formatter.format(now);

		SpecialtyAutoPackage insurancePolicy = (SpecialtyAutoPackage) currTrans.getPolicyVersion().getInsurancePolicy();

		Customer origCustomer = insurancePolicy.getPolicyCustomer();
		CommercialCustomer ccuCustomer = (CommercialCustomer) origCustomer.getCommercialCustomer().clone();
		ccuCustomer.setCompanyPK(null);
		ccuCustomer.setLocations(new HashSet<>());
		ccuCustomer.setLegalName(nowStr);
		
		Customer customer = (Customer) origCustomer.clone();
		customer.setCustomerPk(null);
		customer.setCommercialCustomer(ccuCustomer);
		ccuCustomer.setCustomer(customer);
		customer = (new CommercialCustomerProcessing()).saveCommercialCustomer(customer);
		
		
		Address insuredAddress = (Address) insurancePolicy.getInsuredAddress().clone();
		(new AddressDAO()).save(insuredAddress);
		SpecialtyAutoPackage newInsurancePolicy = policyProcesses.createNewInsurancePolicy(customer, 
													insurancePolicy.getProductCd(), insurancePolicy.getProductProgram(), 
													now.toLocalDate(), 
													insuredAddress, 
													userProfile);

		PolicyTransaction<?> newTrans = policyProcesses.findLatestPolicyTransactionByQuoteNumber(newInsurancePolicy.getQuoteNum());
		newInsurancePolicy = (SpecialtyAutoPackage) newTrans.getPolicyVersion().getInsurancePolicy();

		newInsurancePolicy.setLiabilityLimit(insurancePolicy.getLiabilityLimit());
		newInsurancePolicy.setPolicyProducer(insurancePolicy.getPolicyProducer());
		newInsurancePolicy.setProducerContactName(insurancePolicy.getProducerContactName());
		newInsurancePolicy.setAutoAssocMemberNum(insurancePolicy.getAutoAssocMemberNum());
		newInsurancePolicy.setAutoAssocClub(insurancePolicy.getAutoAssocClub());
		newInsurancePolicy.setStatCode(insurancePolicy.getStatCode());
		newInsurancePolicy.setTerritoryCode(insurancePolicy.getTerritoryCode());
		newInsurancePolicy.setRuralityInd(insurancePolicy.getRuralityInd());
		newInsurancePolicy.setQuotePreparedBy(insurancePolicy.getQuotePreparedBy());
		
		//(PolicyTransaction<?>) newInsurancePolicy.getPolicyVersions().get(0).getPolicyTransactions().iterator().next();
		//newTrans = localPolicyProcesses.localUpdateOnsetTransaction(newTrans, userProfile);
		
		clonedTrans.setTransactionType(newTrans.getTransactionType());
		clonedTrans.setSystemStatus(newTrans.getSystemStatus());
		clonedTrans.setBusinessStatus(newTrans.getBusinessStatus());
		clonedTrans.setPolicyVersion(newTrans.getPolicyVersion());
		clonedTrans.setPolicyTerm(newTrans.getPolicyTerm());
		clonedTrans.setTransactionSeq(2);
		clonedTrans.getPolicyVersion().setProducerCommissionRate(currTrans.getPolicyVersion().getProducerCommissionRate());
		clonedTrans = localPolicyProcesses.localUpdateOnsetTransaction(clonedTrans, userProfile);
		
		cleanBusinessEntity(clonedTrans, new HashMap<IBusinessEntity, Boolean>());

		cleanCoverageDetails(newTrans, clonedTrans.getCoverageDetail());
		for(Risk risk : clonedTrans.getPolicyRisks()) {
			cleanCoverageDetails(newTrans, risk.getCoverageDetail());
		}
		
		for(SubPolicy<?> subPolicy : clonedTrans.getSubPolicies()) {
			cleanCoverageDetails(newTrans, subPolicy.getCoverageDetail());
			
			for(Risk risk : subPolicy.getSubPolicyRisks()) {
				cleanCoverageDetails(newTrans, risk.getCoverageDetail());
			}
		}		
		
		PolicyLocation mainLoc = clonedTrans.getPolicyRisks().stream().flatMap(o->Stream.of((PolicyLocation)o)).collect(Collectors.toList())
		 													 .stream().filter(o -> SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId())).findFirst().orElse(null);
		if (mainLoc != null) {
			mainLoc.setCustomerMainAddressPK(clonedTrans.getPolicyVersion().getInsurancePolicy().getInsuredAddress().getAddressPK());
			
			final PolicyLocation mloc = mainLoc;
			clonedTrans.getPolicyRisks().stream().filter(o -> !o.equals(mloc)).collect(Collectors.toList())
										.stream().flatMap(o->Stream.of((PolicyLocation)o)).collect(Collectors.toList())
										.stream().forEach(o -> o.setCustomerMainAddressPK(null));
		}

		List<CGLLocation> cglLocations = new ArrayList<CGLLocation>();
		CGLSubPolicy cglSubPolicy = (CGLSubPolicy) clonedTrans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		if (cglSubPolicy != null) {
			if (cglSubPolicy.getCglLocations() != null && !cglSubPolicy.getCglLocations().isEmpty()) {
				cglLocations.addAll(cglSubPolicy.getCglLocations());
				cglSubPolicy.getCglLocations().clear();
			}
		}
		clonedTrans = updatePolicyTransaction(clonedTrans, policyProcesses, userProfile);
		
		cglSubPolicy = (CGLSubPolicy) clonedTrans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		if (cglSubPolicy != null && cglLocations != null && !cglLocations.isEmpty()) {
			for(CGLLocation cglLoc : cglLocations) {
				PolicyLocation polLoc = clonedTrans.getPolicyRisks().stream().flatMap(o->Stream.of((PolicyLocation)o)).collect(Collectors.toList())
											.stream().filter(o -> cglLoc.getPolicyLocation().getLocationId().equalsIgnoreCase(o.getLocationId()))
											.findFirst().orElse(null);
				if (polLoc != null) {
					CGLLocation cglLocation = new CGLLocation();
					cglLocation.setPolicyLocation(polLoc);
					cglSubPolicy.addCglLocation(cglLocation);
				}
			}
		}
		clonedTrans = updatePolicyTransaction(clonedTrans, policyProcesses, userProfile);
		
		clonedTrans = policyProcesses.rollupPolicy(clonedTrans.getPolicyTransactionPK(), userProfile);
		
		/* Remove subpolicies */
		/*
		clonedTrans = policyProcesses.removeSubPolicy(clonedTrans, clonedTrans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO));
		clonedTrans = policyProcesses.removeSubPolicy(clonedTrans, clonedTrans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY));
		clonedTrans = policyProcesses.removeSubPolicy(clonedTrans, clonedTrans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GARAGE));
		clonedTrans = policyProcesses.updatePolicyTransaction(clonedTrans, userProfile);
		*/
		/*
		clonedTrans = policyProcesses.issueQuote(clonedTrans, userProfile);
		clonedTrans = policyProcesses.bindQuote(clonedTrans, userProfile);
		clonedTrans = policyProcesses.issuePolicy(clonedTrans, userProfile);
		*/
		
		System.out.println("Customer:"+nowStr);
		System.out.println("QuoteNum:"+newInsurancePolicy.getQuoteNum());
	}
	
	private PolicyTransaction updatePolicyTransaction(PolicyTransaction polTransaction, SpecialtyAutoPolicyProcesses policyProcesses, UserProfile userProfile) throws InsurancePolicyException {
		polTransaction = policyProcesses.updatePolicyTransaction(polTransaction, userProfile);
		polTransaction = policyProcesses.findLatestPolicyTransactionByQuoteNumber(polTransaction.getPolicyVersion().getInsurancePolicy().getQuoteNum(), 1, 1);
		polTransaction = policyProcesses.loadPolicyTransaction(polTransaction, true);
		return polTransaction;
	}
	
	private void cleanCoverageDetails(PolicyTransaction<?> newTrans, CoverageDetails coverageDetails) {
		for(Coverage cov : coverageDetails.getCoverages()) {
			cov.setCoverageEffDate(newTrans.getPolicyTerm().getTermEffDate().toLocalDate());
			cov.setCoverageExpDate(newTrans.getPolicyTerm().getTermExpDate().toLocalDate());
			cov.setCoveragePremium(cleanPremium(cov.getCoveragePremium()));
		}
		for(Endorsement end : coverageDetails.getEndorsements()) {
			end.setEndorsementEffective(newTrans.getPolicyTerm().getTermEffDate().toLocalDate());
			end.setEndorsementExpiry(newTrans.getPolicyTerm().getTermExpDate().toLocalDate());
			end.setEndorsementPremium(cleanPremium(end.getEndorsementPremium()));
		}
	}
	
	private Premium cleanPremium(Premium origPremium) {
		DSPremiumService premService = new DSPremiumService();
		
		Premium newPremium = premService.initializePremium(new Premium()); 
		if (origPremium != null && BooleanUtils.isTrue(origPremium.isPremiumOverride())) {
			newPremium.setAnnualPremium(origPremium.getAnnualPremium());
			newPremium.setPremiumOverride(Boolean.TRUE);
		}
		
		return newPremium;
	}
	
	private void cleanBusinessEntity(IBusinessEntity busEntity, HashMap<IBusinessEntity, Boolean> hmDone) throws Exception {
		if (hmDone.get(busEntity) != null) {
			return;
		}
		
		hmDone.put(busEntity, Boolean.TRUE);
		setBusinessEntityValue(busEntity, "setOriginatingPK", null);
		
		for(Method meth : busEntity.getClass().getMethods()) {
			if (meth.getName().startsWith("get") &&
				meth.getReturnType() != null && meth.getParameters().length == 0) {
				List _values = new ArrayList();
				if (meth.getReturnType() == Set.class || meth.getReturnType() == List.class) {
					try {
						Collection values = (Collection) meth.invoke(busEntity);
						if (values != null && !values.isEmpty()) {
							_values.addAll(values);
						}
					} catch (Exception e) {
						//e.printStackTrace();
					}
				}
				else {
					try {
						Object value = meth.invoke(busEntity);
						if (value != null) {
							_values.add(value);
						}
					} catch (Exception e) {
						//e.printStackTrace();
					}					
				}
				
				if (!_values.isEmpty()) {
					for(Object _val : _values) {
						if (_val instanceof IBusinessEntity) {
							cleanBusinessEntity((IBusinessEntity) _val, hmDone);
						}
					}
				}
			}
		}
	}
	
	private Method getObjectMethod(Class clazz, String methName) {
		for(Method meth : clazz.getMethods()) {
			if (methName.equalsIgnoreCase(meth.getName())) {
				return meth;
			}
		}
		
		return null;
	}
	
	private void setBusinessEntityValue(IBusinessEntity busEntity, String methName, Object value) throws Exception {
		Method meth = getObjectMethod(busEntity.getClass(), methName);
		if (meth != null) {
			try {
				meth.invoke(busEntity, value);
			} catch (Exception e) {
				throw e;
			}
		}
	}
	
	private Object getBusinessEntityValue(IBusinessEntity busEntity, String methName) throws Exception {
		Method meth = getObjectMethod(busEntity.getClass(), methName);
		
		if (meth != null) {
			try {
				return meth.invoke(busEntity);
			} catch (Exception e) {
				throw e;
			}
		}
		
		return null;
	}
	
	private void saveValues(PolicyTransaction trans) {
		
	}
	
	private void restoreValues(PolicyTransaction trans) {
		
	}
	
	@Test
	void testSort() {
		Set<Integer> values = new HashSet<Integer>();
		values.add(12000); 
		values.add(5000); 
		values.add(10000); 
		values.add(7000); 
		values.add(25000);
		values.stream().collect(Collectors.toList())
		 				.stream().sorted().forEach(o -> System.out.println(o));
	}
	
	@Test
	void testRating() throws InsurancePolicyException {
		PolicyRatingFactory.getInstance().getPolicyRatingManager().preRatingProcess();
		
		final String QUOTENUMBER = "CAON200112";
		
		LocalSpecialtyAutoPolicyProcesses localPolicyProcesses = new LocalSpecialtyAutoPolicyProcesses();
		
		UserProfile userProfile = new UserProfile();
		userProfile.setLoginId("daystar");
		
		SpecialtyAutoPolicyProcesses policyProcesses = (SpecialtyAutoPolicyProcesses) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses("TOWING");
		PolicyTransaction currTrans = policyProcesses.findLatestPolicyTransactionByQuoteNumber(QUOTENUMBER, 1, 1);
		assertNotNull(currTrans);
		
		localPolicyProcesses.ratePolicyTransaction(currTrans.getPolicyTransactionPK(), userProfile);
	}
	
	@Test
	void testListContains() {
		List<String> userGroupIds	= Arrays.asList("Citrix","Security Groups","Member Servers","User Accounts","XenApp 7");
		String propGroupIds 		= "xCitrix,xSecurity Groups,Member Servers,xUser Accounts,xXenApp 7";
		
		Stream.of(propGroupIds.split(",")).collect(Collectors.toList()).forEach(System.out::println);
		
		// Any group in common
		List<?> commonGroups = Stream.of(propGroupIds.split(",")).collect(Collectors.toList())
										.stream().filter(userGroupIds::contains)
										.collect(Collectors.toList());
		System.out.println(!commonGroups.isEmpty());
	}
}
