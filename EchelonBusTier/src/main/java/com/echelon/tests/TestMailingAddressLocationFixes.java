package com.echelon.tests;

import org.junit.jupiter.api.Test;

import com.ds.ins.exceptions.InsurancePolicyException;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.services.conversion.MailingAddressLocationFixes;

public class TestMailingAddressLocationFixes {

	@SuppressWarnings("static-access")
	@Test
	void runTest() {
		try {
			MailingAddressLocationFixes runFix = new MailingAddressLocationFixes();
			runFix.setPolicyProcessing((ISpecialtyAutoPolicyProcessing) SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses(ConfigConstants.PRODUCTCODE_TOWING));
			runFix.main(new String[] {"17"});
		} catch (InsurancePolicyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("Testing completed");
	}
}
