package com.echelon.tests;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import com.ds.ins.domain.policy.PolicyMessage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.services.validation.DSValidationService;
import com.ds.ins.utils.Constants;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;

public class TestValidation {

	@Test
	void testOne() throws Exception {
		PolicyTransaction pt = (new SpecialtyAutoPolicyProcesses()).findLatestPolicyTransactionByQuoteNumber("CAON200009");
		List<PolicyMessage> messages = (new DSValidationService()).validatePolicyTransaction(pt, Constants.VERSION_STATUS_QUOTED);
		Assertions.assertNotNull(messages);
		messages.stream().map(PolicyMessage::getRuleId).forEach(System.out::println);
	}
}
