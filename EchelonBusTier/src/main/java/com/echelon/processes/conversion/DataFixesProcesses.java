package com.echelon.processes.conversion;

import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.services.conversion.MailingAddressLocationFixes;
import com.echelon.services.conversion.MidtermChangeDataFixes;

public class DataFixesProcesses {

	@SuppressWarnings({ "rawtypes", "static-access" })
	public boolean runMidtermChangeDataFixes(IGenericPolicyProcessing policyProcessing) throws InsurancePolicyException {
		MidtermChangeDataFixes midtermChangeDataFixes = new MidtermChangeDataFixes();
		
		midtermChangeDataFixes.setPolicyProcessing(policyProcessing);
		midtermChangeDataFixes.main(null);
		
		return true;
	}
	
	@SuppressWarnings("static-access")
	public boolean runMailingAddressLocationFixes(ISpecialtyAutoPolicyProcessing policyProcessing) throws InsurancePolicyException {
		MailingAddressLocationFixes mailingAddressLocationFixes = new MailingAddressLocationFixes();
		
		mailingAddressLocationFixes.setPolicyProcessing(policyProcessing);
		mailingAddressLocationFixes.main(null);
		
		return true;
	}
}
