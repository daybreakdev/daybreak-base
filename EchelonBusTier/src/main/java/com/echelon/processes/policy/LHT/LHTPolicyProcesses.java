package com.echelon.processes.policy.LHT;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.ds.ins.dal.dao.ReinsurerDAO;
import com.ds.ins.dal.dao.SubPolicyDAO;
import com.ds.ins.domain.entities.Reinsurer;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.rating.rounding.RoundingStrategyFactory;
import com.rating.utils.CorePolicyRatingHelperFactory;
import com.ds.ins.system.UserProfile;
import com.ds.ins.utils.Constants;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;
import com.echelon.rating.lht.LHTPolicyRatingHelperFactory;
import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.echelon.services.policy.LHT.LHTPolicyServicesFactory;
import com.echelon.utils.LHTConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class LHTPolicyProcesses extends SpecialtyAutoPolicyProcesses {

	private static final long serialVersionUID = 7013780904722265860L;
	
	final static Logger logger = Logger.getLogger(LHTPolicyProcesses.class.getName());

	@Override
	public String createQuoteNumberPrefix(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return LHTConstants.LHT_QUOTE_PREFIX;
	}
	
	@Override
	public String getQuoteNumberGenerator(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		// TODO Auto-generated method stub
		return LHTConstants.LHTQUOTENUMCNT;
	}
	
	@Override
	public String createPolicyNumberPrefix(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return LHTConstants.LHT_POLICY_PREFIX;
	}
	
	@Override
	public String getPolicyNumberGenerator(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return LHTConstants.LHTPOLICYNUMCNT;
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		return LHTPolicyServicesFactory.getInstance();
	}
	
	@Override
	protected void setCargoSubPolicyDefaults(CargoSubPolicy<?> subPolicy) {
		// ESL-1941: LHT policies should not have a default Motor Truck Cargo limit or
		// deductible
	}
	
	@Override
	public CargoSubPolicy<?> updateCargoSubPolicy(CargoSubPolicy<?> cargoSubPolicy,
			CargoSubPolicy<?> prevCargoSubPolicy, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		cargoSubPolicy = super.updateCargoSubPolicy(cargoSubPolicy, prevCargoSubPolicy, userProfile);
		if (cargoSubPolicy.getCoverageDetail() != null && cargoSubPolicy.getCoverageDetail().getEndorsements() != null && 
				!cargoSubPolicy.getCoverageDetail().getEndorsements().isEmpty()) {
			for (Endorsement end : cargoSubPolicy.getCoverageDetail().getEndorsements()) {
				getCoverageService().setSubpolicyEndorsementDefault(cargoSubPolicy.getPolicyTransaction(), cargoSubPolicy, end);
			}
			cargoSubPolicy = (CargoSubPolicy<?>) (new SubPolicyDAO()).merge(cargoSubPolicy);
		}
				
		return cargoSubPolicy;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void createDefaultCargoDetails(SubPolicy<?> subPcy) {
		// TODO Auto-generated method stub
		// ESL-1932 Do not create default CargoDetails
	}

	@Override
	protected void setRiskDefaults(Risk risk) throws InsurancePolicyException {
		super.setRiskDefaults(risk);
		if (risk instanceof SpecialtyVehicleRisk) {
			SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
			svr.setVehicleUse(LHTConstants.LHT_VEH_RISK_VEHICLE_USE);
			
			//ESL-1831 - codes sync with UI LHTVehicleForm.unitExposureValueChanged
			if (Constants.PROVINCE_ONTARIO.equalsIgnoreCase(risk.getSubPolicy().getPolicyTransaction().getPolicyVersion().getInsurancePolicy().getControllingJurisdiction()) &&
				Constants.PROVINCE_ONTARIO.equalsIgnoreCase(svr.getUnitExposure())) {
				svr.setIrp(0);
			}
			else {
				svr.setIrp(1);
			}
		}
	}

	@Override
	public List<String> getRatingWarnings(PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		// ESL-1953 - not for LHT
		//return super.getRatingWarnings(trans);
		return new ArrayList<String>();
	}

	@Override
	public List<String> getRiskRatingWarnings(Risk risk, List<Coverage> coverages, List<Endorsement> endorsements)
			throws SpecialtyAutoPolicyException {
		// ESL-1953 - not for LHT
		//return super.getRiskRatingWarnings(risk, coverages, endorsements);
		return new ArrayList<String>();
	}

	@Override
	public List<String> getSubpolicyRatingWarnings(SubPolicy<?> subPolicy, List<Coverage> coverages,
			List<Endorsement> endorsements) throws SpecialtyAutoPolicyException {
		List<String> messages = new ArrayList<String>();
		
		if (isCGCovLWLLLimitsExceeded(subPolicy, coverages, endorsements)) {
			messages.add(SpecialtyAutoConstants.WARN_COV_WLL_LIMIT_MESSAGECODE);
		}
		
		return messages;
	}
	
	
	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return LHTRoundingStrategyFactory.getInstance();
	}
	
	@Override
	public SpecialtyAutoPackage postCreateNewInsurancePolicyPreSave(SpecialtyAutoPackage insurancePolicy,
			UserProfile userProfile) throws InsurancePolicyException {
		insurancePolicy = super.postCreateNewInsurancePolicyPreSave(insurancePolicy, userProfile);

		//ESL-1982 Precreate the Policy Reinsurer for Echelon
		List<Reinsurer> rinsList = (new ReinsurerDAO()).searchByName(SpecialtyAutoConstants.REINSURER_ECHELON);
		if (rinsList != null && !rinsList.isEmpty()) {
			Reinsurer echelonReins = rinsList.get(0);
			PolicyReinsurer pcyReins = new PolicyReinsurer();
			pcyReins.setReinsurer(echelonReins);
			PolicyTransaction<?> pcyTxn = insurancePolicy.getPolicyVersions().get(0).getPolicyTransactions().stream().findFirst().get();
			pcyTxn.addPolicyReinsurer(pcyReins);
		}
		
		return insurancePolicy;
	}

	@Override
	protected CorePolicyRatingHelperFactory getPolicyRatingHelperFactory() {
		return LHTPolicyRatingHelperFactory.getInstance();
	}

	
}
