/*

 _______ _________ _______  _______
(  ____ \\__   __/(  ___  )(  ____ )
| (    \/   ) (   | (   ) || (    )|
| (_____    | |   | |   | || (____)|
(_____  )   | |   | |   | ||  _____)
      ) |   | |   | |   | || (
/\____) |   | |   | (___) || )
\_______)   )_(   (_______)|/

If you are merging code from the EchelonSpecificProjects branch to the LHT branch, DO NOT override changes made in this class in the LHT branch.

METHODS AFFECTED:
=================
- createNewCustomerAndInsurancePolicy(...) x2 methods
- createNewInsurancePolicy(...) x2 methods

*/
package com.echelon.processes.policy.specialitylines;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.OptionalInt;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;

import com.ds.ins.dal.dao.InsurancePolicyDAO;
import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.dal.dao.SubPolicyDAO;
import com.ds.ins.domain.baseclasses.IAuditable;
import com.ds.ins.domain.baseclasses.IBusinessEntity;
import com.ds.ins.domain.dto.ReinsuranceItemDTO;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.entities.Customer;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.CoverageDetails;
import com.ds.ins.domain.policy.CoverageReinsurance;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.EndorsementReinsurance;
import com.ds.ins.domain.policy.InsurancePolicy;
import com.ds.ins.domain.policy.LienholderLessor;
import com.ds.ins.domain.policy.LienholderLessorSubPolicy;
import com.ds.ins.domain.policy.PolicyDocuments;
import com.ds.ins.domain.policy.PolicyReinsurer;
import com.ds.ins.domain.policy.PolicyTerm;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersion;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.processes.policy.GenericPolicyProcesses;
import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.prodconf.interfaces.ICoverageConfigParent;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.LimitConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;
import com.ds.ins.services.document.DSPolicyDocumentsService;
import com.ds.ins.services.policy.DSPolicyServicesFactory;
import com.ds.ins.services.premium.CancellationPremCalcFactory;
import com.ds.ins.system.LoggedUser;
import com.ds.ins.system.UserProfile;
import com.ds.ins.utils.Constants;
import com.ds.ins.utils.ObjectCopyUtils;
import com.echelon.dal.dao.specialitylines.CVRateDAO;
import com.echelon.dal.dao.specialitylines.PolicyDocumentDAO;
import com.echelon.dal.dao.specialitylines.PolicyLocationDAO;
import com.echelon.dal.dao.specialitylines.SpecialtySubPolicyDAO;
import com.echelon.dal.dao.specialitylines.SpecialtyVehicleRiskAdjustmentDAO;
import com.echelon.dal.dao.specialitylines.SpecialtyVehicleRiskDAO;
import com.echelon.dal.dao.specialitylines.VehicleDriverClaimDAO;
import com.echelon.dal.dao.specialitylines.VehicleDriverDAO;
import com.echelon.dal.dao.specialitylines.VehicleRateGroupDAO;
import com.echelon.dal.dao.specialitylines.VehicleRateGroupLHTDAO;
import com.echelon.domain.dto.specialtylines.ReinsuranceReportDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyPolicyItemDTO;
import com.echelon.domain.dto.specialtylines.SpecialtyVehicleRiskDTO;
import com.echelon.domain.interfaces.specialtylines.ISpecialtyHasAdjustment;
import com.echelon.domain.policy.specialtylines.AdditionalInsuredVehicles;
import com.echelon.domain.policy.specialtylines.CGLLocation;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CVRate;
import com.echelon.domain.policy.specialtylines.CargoDetails;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.CommercialPropertyLocation;
import com.echelon.domain.policy.specialtylines.CommercialPropertySubPolicy;
import com.echelon.domain.policy.specialtylines.GarageSubPolicy;
import com.echelon.domain.policy.specialtylines.LienholderLessorVehicleDetails;
import com.echelon.domain.policy.specialtylines.PolicyLocation;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRiskAdjustment;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.domain.policy.specialtylines.VehicleDriverClaim;
import com.echelon.domain.policy.specialtylines.VehicleRateGroup;
import com.echelon.domain.policy.specialtylines.VehicleRateGroupLHT;
import com.echelon.interfaces.policy.specialtylines.ISpecialtyAutoPolicyProcessing;
import com.echelon.prodconf.ProductConfigFactory;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.rating.towing.TowingPolicyRatingHelperFactory;
import com.echelon.services.coverage.CoverageServiceHelper;
import com.echelon.services.document.PolicyDocumentsService;
import com.echelon.services.policy.PolicyService;
import com.echelon.services.policy.PolicyServicesFactory;
import com.echelon.services.premium.PremiumHelper;
import com.echelon.services.premium.TowingCancellationPremCalcFactory;
import com.echelon.services.premium.rollup.PremiumRollup;
import com.echelon.utils.LHTConstants;
import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.coreclasses.CorePolicyRatingFactory;
import com.rating.utils.CorePolicyRatingHelperFactory;


public class SpecialtyAutoPolicyProcesses extends GenericPolicyProcesses<SpecialtyAutoPackage, PolicyVersion, PolicyTransaction<?>, SubPolicy<?>> implements ISpecialtyAutoPolicyProcessing, Serializable {

	private static final long serialVersionUID = -7322743318679105172L;
	private static final String SUBPOLICY_AUTOSUBPOLICY 	= "AUTO";
	private static final String SUBPOLICY_CGLSUBPOLICY 	  	= "CGL";
	private static final String SUBPOLICY_CARGOSUBPOLICY   	= "CGO";
	private static final String SUBPOLICY_GARAGESUBPOLICY  	= "GAR";
	private static final String RISK_TYPE_SPEC_VEH 			= "AU";
	private static final String RISK_TYPE_PCY_LOCATION 		= "LO";
	
	final static Logger logger = Logger.getLogger(SpecialtyAutoPolicyProcesses.class.getName());
	
	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		return ProductConfigFactory.getInstance();
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	protected DSPolicyServicesFactory getPolicyServicesFactory() {
		return PolicyServicesFactory.getInstance();
	}

	protected PolicyService getSpecialtyAutoPolicyPolicyService() {
		return (PolicyService) getPolicyService();
	}

	@Override
	protected SpecialtyAutoPackage getPolicyInstance() {
		return new SpecialtyAutoPackage();
	}

	@Override
	protected CorePolicyRatingFactory getPolicyRatingFactory() {
		return PolicyRatingFactory.getInstance();
	}
	
	protected CoverageServiceHelper getCoverageServiceHelper () {
		return (CoverageServiceHelper) getPolicyServicesFactory().newCoverageServiceHelper();
	}

	@Override
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> createAutoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException {
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> rv = new SpecialityAutoSubPolicy<>();
		trans.addSubPolicy(rv);
		rv.setSubPolicyTypeCd(SUBPOLICY_AUTOSUBPOLICY);
		try {
			setSubPolicyDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		return rv;
	}

	@Override
	public SpecialtyVehicleRisk createSpecialtyVehicleRisk(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy) throws SpecialtyAutoPolicyException {
		SpecialtyVehicleRisk rv;
		
		rv = new SpecialtyVehicleRisk();
		
		specialAutoSubPolicy.addSubPolicyRisk(rv);
		rv.setRiskType(RISK_TYPE_SPEC_VEH);
		try {
			setRiskDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		return rv;
	}

	@Override
	protected void setRiskDefaults(Risk risk) throws InsurancePolicyException {
		super.setRiskDefaults(risk);
		if (risk instanceof SpecialtyVehicleRisk) {
			SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
			svr.setUnitExposure(Constants.PROVINCE_ONTARIO);
			svr.setVehicleUse(SpecialtyAutoConstants.SPEC_VEH_RISK_VEHICLE_USE);
			svr.setDrivingRecord(SpecialtyAutoConstants.SPEC_VEH_RISK_DRIVING_RECORD);
			
			//ESL-344
			if (svr.getSubPolicy() != null) {
				SpecialityAutoSubPolicy<?> autoSubPolicy = (SpecialityAutoSubPolicy) svr.getSubPolicy();
				if (SpecialtyAutoConstants.AUTO_SUBPCY_FLEETBASIS_SCHEDULED.equalsIgnoreCase(autoSubPolicy.getFleetBasis())) {
					svr.setNumberOfUnits(1);
					svr.setDrivingRecord("3");
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setPolicyDefaults(PolicyTransaction<?> pcyTxn, Boolean setQuoteNumber) throws InsurancePolicyException {
		super.setPolicyDefaults(pcyTxn, setQuoteNumber);
		Customer pcyCustomer = pcyTxn.getPolicyTerm().getInsurancePolicy().getPolicyCustomer();
		if (pcyCustomer != null) {
			if (pcyCustomer.getCommercialCustomer() != null && 
					StringUtils.isBlank(pcyCustomer.getCommercialCustomer().getPreferredLanguage())) {
				pcyCustomer.getCommercialCustomer().setPreferredLanguage(Constants.LANGUAGECODE_ENGLISH);
			}
		}
		Address insAddress = pcyTxn.getPolicyTerm().getInsurancePolicy().getInsuredAddress();
		if (insAddress != null) {
			if (StringUtils.isBlank(insAddress.getCountry())) {
				insAddress.setCountry(Constants.COUNTRY_CANADA);
			}
		}
		IGenericProductConfig config = getProductConfig(pcyTxn);
		if (config != null) {
			//SubPolicyConfig subPolicyConfig = config.getSubPolicyConfig(subPcy.getSubPolicyTypeCd());
			List<SubPolicyConfig> subPcyConfigs = config.getSubPolicyConfigs();
			if (subPcyConfigs != null && !subPcyConfigs.isEmpty()) {
				for (SubPolicyConfig subPcyConfig : subPcyConfigs) {
					if (subPcyConfig.isMandatory()) {
						if (SUBPOLICY_AUTOSUBPOLICY.equalsIgnoreCase(subPcyConfig.getSubPolicyCode())) {
							try {
								createAutoSubPolicy((PolicyTransaction<SpecialtyVehicleRisk>) pcyTxn);
							} catch (SpecialtyAutoPolicyException e) {
								throw new InsurancePolicyException("Could not create sub policy", e);
							}
						}
					}
				}
			}
		}
	}

	@Override
	public SpecialtyVehicleRisk updateSpecialtyVehicleRisk(SpecialtyVehicleRisk specialVehicleRisk , UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			if (specialVehicleRisk  != null) {
				LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
				
				//specialVehicleRisk  = (new SpecialtyVehicleRiskDAO()).merge(specialVehicleRisk );
				
				PolicyTransaction<?> policyTransaction = specialVehicleRisk .getSubPolicy().getPolicyTransaction();
				
				IGenericProductConfig productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						policyTransaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						policyTransaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate());
				if (productConfig != null) {
					if (ObjectUtils.compare(((SpecialtyAutoPackage)policyTransaction.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit(), 
							getPolicyMaxRetainedLimit(policyTransaction).intValue()) > 0) {

						if (getCoverageService().needToLoadCoverageDetails(specialVehicleRisk .getCoverageDetail())) {
							specialVehicleRisk  = (SpecialtyVehicleRisk) loadSubPolicyRisk(specialVehicleRisk , false);
						}
						SpecialtyVehicleRisk spr = specialVehicleRisk ;
						
						SubPolicyConfig spConfig = productConfig.getSubPolicyConfig(specialVehicleRisk .getSubPolicy().getSubPolicyTypeCd());
						if (spConfig != null) {
							RiskConfig spRiskConfig = spConfig.getRiskType(spr.getRiskType());
							if (spRiskConfig != null) {
								//subpolicy risk coverages
								spr.getCoverageDetail().getCoverages().stream().filter(sprc -> spr.isActive() || sprc.isPending()).forEach(sprc -> {
									CoverageConfig covConfig = spRiskConfig.getCoverageConfig(sprc.getCoverageCode());
									if (covConfig != null && covConfig.isReinsurable()) {
										sprc.setReinsured(Boolean.TRUE);
									}																										
								});
		
								//subpolicy risk endorsements
								spr.getCoverageDetail().getEndorsements().stream().filter(spre -> spre.isActive() || spre.isPending()).forEach(spre -> {
									EndorsementConfig endConfig = spRiskConfig.getEndorsementConfig(spre.getEndorsementCd());
									if (endConfig != null && endConfig.isReinsurable()) {
										spre.setReinsured(Boolean.TRUE);
									}
								});
							}
						}
					}
				}
				
				rollupPolicy(policyTransaction, specialVehicleRisk .getSubPolicy(), specialVehicleRisk , true, userProfile);
				
				LoggedUser.logOut();
			}
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not save the vehicle risk.", ex);
		}
		return specialVehicleRisk ;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void deleteSpecialtyVehicleRisk(SpecialtyVehicleRisk specialVehicleRisk ) throws SpecialtyAutoPolicyException {
		try {
			SpecialityAutoSubPolicy specialityAutoSubPolicy = (SpecialityAutoSubPolicy) specialVehicleRisk .getSubPolicy();
			specialVehicleRisk.getVehicleDrivers().forEach( vd -> {
				if (vd.isActive() && !vd.isDeleted()) {
					vd.setSystemStatus(IAuditable.INACTIVE);
					vd.setBusinessStatus(IAuditable.DELETED);
				}
			});
			PolicyTransaction<?> policyTransaction = specialityAutoSubPolicy.getPolicyTransaction();
			//specialityAutoSubPolicy.removeSubPolicyRisk(specialVehicleRisk );
			specialityAutoSubPolicy = (SpecialityAutoSubPolicy) removeSubPolicyRisk(specialityAutoSubPolicy.getPolicyTransaction(), specialityAutoSubPolicy, specialVehicleRisk );
			reSequenceVehicleSequences(specialityAutoSubPolicy);
			//updateSubPolicy(specialityAutoSubPolicy, null);
			
			rollupPolicy(policyTransaction, specialityAutoSubPolicy, specialVehicleRisk , true, null);
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not delete the vehicle risk.", ex);
		}
	}

	@Override
	public SubPolicy<?> undeleteSubPolicyRisk(PolicyTransaction<?> pcyTxn, SubPolicy<?> subPolicy, Risk risk)
			throws InsurancePolicyException {
		// TODO Auto-generated method stub
		subPolicy = super.undeleteSubPolicyRisk(pcyTxn, subPolicy, risk);
		rollupPolicy(pcyTxn, subPolicy, risk, false, null);
		return subPolicy;
	}

	@Override
	public PolicyLocation createPolicyLocation(PolicyTransaction<PolicyLocation> policyTransaction) throws SpecialtyAutoPolicyException {
		PolicyLocation rv = new PolicyLocation();
		policyTransaction.addPolicyRisk(rv);
		rv.setRiskType(RISK_TYPE_PCY_LOCATION);
		try {
			setRiskDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		return rv;
	}

	@Override
	public PolicyLocation updatePolicyLocation(PolicyLocation policyLocation, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
		try {
			policyLocation = (new PolicyLocationDAO()).merge(policyLocation);
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not save the policy location.", ex);
		}
		LoggedUser.logOut();
		return policyLocation;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void deletePolicyLocation(PolicyLocation policyLocation, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			policyLocation = (PolicyLocation) loadPolicyRisk(policyLocation, Boolean.FALSE);
		} catch (InsurancePolicyException e) {
			logger.log(Level.SEVERE, "Could not load the policy location (PK): " + policyLocation.getPolicyRiskPK(), e);
			throw new SpecialtyAutoPolicyException("Could not load the policy location (PK): " + policyLocation.getPolicyRiskPK(), e);
		}
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
		if (policyLocation.isActive() && !policyLocation.isDeleted()) {
			policyLocation.setSystemStatus(IAuditable.INACTIVE);
			policyLocation.setBusinessStatus(IAuditable.DELETED);
			if (policyLocation.getPolicyTransaction() != null) {
				if (policyLocation.getPolicyTransaction().getPolicyVersion().getVersionDate() != null) {
					policyLocation.setExpiryDate(policyLocation.getPolicyTransaction().getPolicyVersion().getVersionDate());
				}
				try {
					for (Coverage cov : policyLocation.getCoverageDetail().getCoverages().stream().collect(Collectors.toSet())) {
						policyLocation = (PolicyLocation) removeRiskCoverage(cov, policyLocation.getPolicyTransaction(), policyLocation);
					}
					for (Endorsement end : policyLocation.getCoverageDetail().getEndorsements().stream().collect(Collectors.toSet())) {
						policyLocation = (PolicyLocation) removeRiskEndorsement(end, policyLocation.getPolicyTransaction(), policyLocation);
					}
				} catch (InsurancePolicyException ipex) {
					logger.log(Level.WARNING, "Could not remove coverge/endorsement", ipex);
					throw new SpecialtyAutoPolicyException("Could not remove coverge/endorsement", ipex);
				}
				CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)policyLocation.getPolicyTransaction().findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
				if (cglSP != null) {
					removeCGLLocationsByPolicyLocation(cglSP, policyLocation);
				}
			}
			try {
				(new PolicyLocationDAO()).merge(policyLocation);
			} catch (Exception ex) {
				throw new SpecialtyAutoPolicyException("Could not mark the policy location as DELETED.", ex);
			}
		} else if (policyLocation.isPending()) {
			if (policyLocation.getPolicyTransaction() != null) {
				CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)policyLocation.getPolicyTransaction().findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
				if (cglSP != null) {
					removeCGLLocationsByPolicyLocation(cglSP, policyLocation);
				}
			}
			try {
				(new PolicyLocationDAO()).delete(policyLocation);
			} catch (Exception ex) {
				throw new SpecialtyAutoPolicyException("Could not delete the policy location.", ex);
			}
		}
		LoggedUser.logOut();
	}
	
	@SuppressWarnings("unchecked")
	private void removeCGLLocationsByPolicyLocation(CGLSubPolicy<Risk> cglSP, PolicyLocation policyLocation) throws SpecialtyAutoPolicyException {
		if (cglSP != null) {
			SubPolicyDAO dao = new SubPolicyDAO();
			//Remove any CGLLocation records that have the deleted policy location
			Set<CGLLocation> cglLocSet = new HashSet<CGLLocation>(cglSP.getCglLocations());
			//for (CGLLocation cglLoc : cglSP.getCglLocations()) {
			for (CGLLocation cglLoc : cglLocSet) {
				if (cglLoc.getPolicyLocation() != null && cglLoc.getPolicyLocation().equals(policyLocation)) {
					cglSP.removeCglLocation(cglLoc);
				}
			}
			try {
				cglSP = (CGLSubPolicy<Risk>) dao.merge(cglSP);
			} catch (Exception ex) {
				throw new SpecialtyAutoPolicyException("Could not remove CGLLocation records for the deleted policy location.", ex);
			}
		}
	}
	
	@Override
	public PolicyDocuments updatePolicyDocument(PolicyDocuments policyDocument, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
		try {
			policyDocument = (new PolicyDocumentDAO()).merge(policyDocument);
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not save the policy document.", ex);
		}
		LoggedUser.logOut();
		return policyDocument;
	}
	
	@Override
	public void deletePolicyDocument(PolicyDocuments policyDocument, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
			(new PolicyDocumentDAO()).delete(policyDocument);
			LoggedUser.logOut();
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not delete the policy document.", ex);
		}
	}

	@Override
	public CGLSubPolicy<SpecialtyVehicleRisk> createCGLSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		CGLSubPolicy<SpecialtyVehicleRisk> rv = new CGLSubPolicy<>();
		trans.addSubPolicy(rv);
		rv.setSubPolicyTypeCd(SUBPOLICY_CGLSUBPOLICY);
		try {
			SpecialtyAutoPackage pcy = (SpecialtyAutoPackage) trans.getPolicyVersion().getInsurancePolicy();
			if (pcy.getLiabilityLimit() != null) {
				rv.setCglLimit(pcy.getLiabilityLimit());
			}
			setSubPolicyDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		return rv;
	}
	
	@Override
	public CommercialPropertySubPolicy<SpecialtyVehicleRisk> createCommercialPropertySubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		CommercialPropertySubPolicy<SpecialtyVehicleRisk> rv = new CommercialPropertySubPolicy<>();
		trans.addSubPolicy(rv);
		rv.setSubPolicyTypeCd(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		try {
			setSubPolicyDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}

		return rv;
	}

	@Override
	public CargoSubPolicy<SpecialtyVehicleRisk> createCargoSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		CargoSubPolicy<SpecialtyVehicleRisk> rv = new CargoSubPolicy<>();

		setCargoSubPolicyDefaults(rv);

		trans.addSubPolicy(rv);
		rv.setSubPolicyTypeCd(SUBPOLICY_CARGOSUBPOLICY);

		try {
			setSubPolicyDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}

		return rv;
	}
	
	/**
	 * Set the default limit and deductible for Motor Truck Cargo subpolicies.
	 * @param subPolicy the Cargo subpolicy
	 */
	protected void setCargoSubPolicyDefaults(CargoSubPolicy<?> subPolicy) {
		subPolicy.setCargoLimit(SpecialtyAutoConstants.SUBPCY_CARGO_LIMIT_DEFAULT);
		subPolicy.setCargoDeductible(SpecialtyAutoConstants.SUBPCY_CARGO_DEDUCTIBLE_DEFAULT);
	}

	@Override
	public GarageSubPolicy<SpecialtyVehicleRisk> createGarageSubPolicy(PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		GarageSubPolicy<SpecialtyVehicleRisk> rv = new GarageSubPolicy<>();
		trans.addSubPolicy(rv);
		rv.setSubPolicyTypeCd(SUBPOLICY_GARAGESUBPOLICY);
		try {
			setSubPolicyDefaults(rv);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		return rv;
	}

	@Override
	public SpecialtyVehicleRisk copySpecialtyVehicleRisk(SpecialtyVehicleRisk specialVehicleRisk) throws SpecialtyAutoPolicyException {
		// TODO Auto-generated method stub
		SpecialtyVehicleRisk newVehicleRisk = null;
		boolean isPolicyChange = Constants.VERS_TXN_TYPE_POLICYCHANGE.equalsIgnoreCase(specialVehicleRisk
																.getSubPolicy().getPolicyTransaction().getPolicyVersion().getPolicyTxnType());
		
		try {
			newVehicleRisk = (SpecialtyVehicleRisk) specialVehicleRisk.clone();
			newVehicleRisk.setVehicleSequence(null); // will be populated on save
			//ESL-813 START
			newVehicleRisk.setSystemStatus(IAuditable.PENDING);
			newVehicleRisk.setBusinessStatus(IAuditable.NEW);
			newVehicleRisk.setOriginatingPK(null);
			Iterator<Coverage> covItr = newVehicleRisk.getRiskCoverages().iterator();
			while (covItr.hasNext()) {
				Coverage cov = covItr.next();
				if (cov.isDeleted()) {
					covItr.remove();
				} else {
					cov.setSystemStatus(IAuditable.PENDING);
					cov.setBusinessStatus(IAuditable.NEW);
					cov.setOriginatingPK(null);
					if (isPolicyChange && cov.getCoveragePremium() != null) { 
						/* ESL-1418
						if (cov.getCoveragePremium().getPremiumModifier1() != null) {
							cov.getCoveragePremium().setPremiumModifier1(null);
						}
						*/
						if (BooleanUtils.isTrue(cov.getCoveragePremium().isPremiumOverride())) {
							cov.getCoveragePremium().setPremiumOverride(false);
						}
					}
				}
			}
			Iterator<Endorsement> endItr = newVehicleRisk.getRiskEndorsements().iterator();
			while (endItr.hasNext()) {
				Endorsement end = endItr.next();
				if (end.isDeleted()) {
					endItr.remove();
				} else {
					end.setSystemStatus(IAuditable.PENDING);
					end.setBusinessStatus(IAuditable.NEW);
					end.setOriginatingPk(null);
					if (isPolicyChange && end.getEndorsementPremium() != null) {
						/* ESL-1418
						if (end.getEndorsementPremium().getPremiumModifier1() != null) {
							end.getEndorsementPremium().setPremiumModifier1(null);
						}
						*/
						if (BooleanUtils.isTrue(end.getEndorsementPremium().isPremiumOverride())) {
							end.getEndorsementPremium().setPremiumOverride(false);
						}
					}
				}
			}

			if (newVehicleRisk.getRateFactorsLink() != null) {
				Iterator<RateFactor> rfItr = newVehicleRisk.getRiskRateFactors().iterator();
				while (rfItr.hasNext()) {
					RateFactor rf = rfItr.next();
					if (StringUtils.equalsIgnoreCase(IAuditable.DELETED, rf.getBusinessStatus())) {
						rfItr.remove();
					} else {
						rf.setSystemStatus(IAuditable.PENDING);
						rf.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
			//ESL-813 FINISH
			//ENFO-235
			Iterator<VehicleDriver> vdrItr = newVehicleRisk.getVehicleDrivers().iterator();
			while (vdrItr.hasNext()) {
				VehicleDriver vdr = vdrItr.next();
				if (vdr.isDeleted()) {
					vdrItr.remove();
				} else {
					// Find the driver from the source vehilce
					if (vdr.getOriginatingPk() != null) {
						VehicleDriver oriVdr = specialVehicleRisk.findVehicleDriverByPk(vdr.getOriginatingPk());
						if (oriVdr != null && oriVdr.getDriver() != null) {
							vdr.setDriver(oriVdr.getDriver());
						}
					}
					
					vdr.setSystemStatus(IAuditable.PENDING);
					vdr.setBusinessStatus(IAuditable.NEW);
					vdr.setOriginatingPk(null);
				}
			}
			Iterator<VehicleDriverClaim> vdrcItr = newVehicleRisk.getVehicleDriverClaims().iterator();
			while (vdrcItr.hasNext()) {
				VehicleDriverClaim vdrc = vdrcItr.next();
				if (vdrc.isDeleted()) {
					vdrcItr.remove();
				} else {
					// Find the driver from the source vehilce
					if (vdrc.getOriginatingPk() != null) {
						VehicleDriverClaim oriVdrc = specialVehicleRisk.findVehicleDriverClaimByPk(vdrc.getOriginatingPk());
						if (oriVdrc != null && oriVdrc.getDriver() != null) {
							vdrc.setDriver(oriVdrc.getDriver());
						}
					}
					
					vdrc.setSystemStatus(IAuditable.PENDING);
					vdrc.setBusinessStatus(IAuditable.NEW);
					vdrc.setOriginatingPk(null);
				}
			}
			//ENFO-235 FINISH
		} catch (CloneNotSupportedException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		
		return newVehicleRisk;
	}

	@Override
	public List<VehicleRateGroup> findVehicleRateGroup(Integer vehicleAge, Double lowLimit, Double uppLimit)
			throws SpecialtyAutoPolicyException {
		final int MINIUMYEAR = 25;
		
		if (vehicleAge == null) {
			// default current year for Vehicle Group 
			//if (vehicleUIHelper.isVehicleGroup(numOfUnitsField.getValue())) {
				vehicleAge = LocalDate.now().getYear();
			//}
		}
		
		int currYear = LocalDate.now().getYear();
		int years = currYear - vehicleAge;
		if (years < 0) {
			years = 0;
		}
		int useVehAge = Math.min(years, MINIUMYEAR);
		
		List<VehicleRateGroup> results = null;
		
		try {
			results = (new VehicleRateGroupDAO()).findBy_VehAge_lowLimit_uppLimit(useVehAge, lowLimit, uppLimit);
			if (results == null || results.isEmpty()) {
				// Anything greater than that
				results = findVehicleRateGroup(useVehAge, null, uppLimit);				
			}
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Search failed", e);
		}
		
		return results;
	}

	@Override
	public String createPolicyNumberPrefix(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return SpecialtyAutoConstants.SPECAUTO_POLICY_PREFIX;
	}

	@Override
	public String createQuoteNumberPrefix(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		return SpecialtyAutoConstants.SPECAUTO_QUOTE_PREFIX;
	}

	@SuppressWarnings("unchecked")
	@Override
	public PolicyTransaction<?> preUpdatePolicyTransaction(PolicyTransaction<?> policyTransaction)
			throws InsurancePolicyException {
		policyTransaction = super.preUpdatePolicyTransaction(policyTransaction);
		
		// ESL-1563 not necessary
		/*
		Address insuredAdd = policyTransaction.getPolicyVersion().getInsurancePolicy().getInsuredAddress();
		if (insuredAdd != null) {
			PolicyLocation loc = null;
			for (Risk risk : policyTransaction.getPolicyRisks()) {
				if (RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(risk.getRiskType())) {
					PolicyLocation l = (PolicyLocation)risk;
					if (l.getCustomerMainAddressPK() != null && insuredAdd.getAddressPK() != null && 
								l.getCustomerMainAddressPK().equals(insuredAdd.getAddressPK())) {
						loc = l;
						break;
					}
				}
			}
			if (loc == null) {
				try {
					loc = createPolicyLocation((PolicyTransaction<PolicyLocation>) policyTransaction);
					loc.setCustomerMainAddressPK(policyTransaction.getPolicyVersion().getInsurancePolicy().getInsuredAddress().getAddressPK());
					loc.setLocationId(SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT);
					updatePolicyLocationAddress(loc, insuredAdd);
				} catch (SpecialtyAutoPolicyException e) {
					throw new InsurancePolicyException("Could not create policy location", e);
				}
			}
		}
		*/
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialityAutoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO); 
		assignVehicleSequence(specialityAutoSubPolicy);
		changeDefaultEndorsements(policyTransaction, specialityAutoSubPolicy);
		setCoverageDefaults(policyTransaction);
		updateNewSubPolicyReinsured(policyTransaction);
		
		return policyTransaction;
	}
	
	// ESL-1563 Changed to update Insured Address from MAILING address
	// create the mailing address at the same time as creating a new policy
	private PolicyTransaction<?> createCustomerMainAddress(PolicyTransaction<?> policyTransaction) throws InsurancePolicyException {
		Address insuredAddress = policyTransaction.getPolicyVersion().getInsurancePolicy().getInsuredAddress(); 
		if (insuredAddress != null) {
			PolicyLocation loc = null;
			for (Risk risk : policyTransaction.getPolicyRisks()) {
				if (RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(risk.getRiskType())) {
					PolicyLocation l = (PolicyLocation)risk;
					if (l.getCustomerMainAddressPK() != null && insuredAddress.getAddressPK() != null && 
								l.getCustomerMainAddressPK().equals(insuredAddress.getAddressPK())) {
						loc = l;
						break;
					}
				}
			}
			
			if (loc == null) {
				try {
					loc = createPolicyLocation((PolicyTransaction<PolicyLocation>) policyTransaction);
					loc.setCustomerMainAddressPK(insuredAddress.getAddressPK());
					loc.setLocationId(SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT);
					updatePolicyLocationAddress(insuredAddress, loc.getLocationAddress());
				} catch (Exception e) {
					throw new InsurancePolicyException("Could not create main policy location", e); 
				}
			}
		}
		
		return policyTransaction;
	}

	// ESL-1563 Changed to update Insured Address from MAILING address
	@Override
	public PolicyTransaction<?> updateCustomerMainAddress(PolicyTransaction<?> policyTransaction, UserProfile userProfile) throws InsurancePolicyException {
		if (policyTransaction != null) {
			Address insuredAdd = policyTransaction.getPolicyVersion().getInsurancePolicy().getInsuredAddress();
			if (insuredAdd != null) {
				PolicyLocation loc = null;
				for (Risk risk : policyTransaction.getPolicyRisks()) {
					if (RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(risk.getRiskType())) {
						PolicyLocation l = (PolicyLocation)risk;
						if (l.getCustomerMainAddressPK() != null && insuredAdd.getAddressPK() != null && 
									l.getCustomerMainAddressPK().equals(insuredAdd.getAddressPK())) {
							loc = l;
							break;
						}
					}
				}
				
				if (loc != null) {
					try {
						LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
						updatePolicyLocationAddress(loc.getLocationAddress(), insuredAdd);
						policyTransaction = (new PolicyTransactionDAO()).merge(policyTransaction);
						LoggedUser.logOut();
					} catch (Exception e) {
						throw new InsurancePolicyException("Could not save policy main location", e);
					}
				}
			}
		}
		return policyTransaction;
	}

	protected void updatePolicyLocationAddress(Address source, Address target) {
		target.setAddressLine1(source.getAddressLine1());
		target.setAddressLine2(source.getAddressLine2());
		target.setAddressLine3(source.getAddressLine3());
		target.setCity(source.getCity());
		target.setCountry(source.getCountry());
		target.setPostalZip(source.getPostalZip());
		target.setProvState(source.getProvState());
	}

	@Override
	public Integer getPolicyLiabilityLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		Integer rv = null;
		if (pcyTxn != null && pcyTxn.getPolicyVersion() != null && pcyTxn.getPolicyVersion().getInsurancePolicy() != null) {
			SpecialtyAutoPackage pcy = (SpecialtyAutoPackage) pcyTxn.getPolicyVersion().getInsurancePolicy();
			rv = pcy.getLiabilityLimit();	
		}
		return rv;
	}

	@Override
	public PolicyTransaction<?> updatePolicyLiabilityLimit(Integer liabilityLimit, PolicyTransaction<?> pcyTxn, 
				UserProfile userProfile) throws SpecialtyAutoPolicyException {
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
		updatePolicyLiabilityLimitImpl(liabilityLimit, pcyTxn);
		try {
			pcyTxn = updatePolicyTransaction(pcyTxn, userProfile);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not update the policy transaction", e);
		}
		LoggedUser.logOut();
		return pcyTxn;
	}
	
	private void updatePolicyLiabilityLimitImpl(Integer liabilityLimit, PolicyTransaction<?> pcyTxn) throws SpecialtyAutoPolicyException {
		try {
			
			IGenericProductConfig config = getProductConfig(pcyTxn);
			for (SubPolicy<?> subPcy : pcyTxn.getSubPolicies()) {
				SubPolicyConfig subPcyConfig = config.getSubPolicyConfig(subPcy.getSubPolicyTypeCd());
				if (subPcyConfig != null) {
					for (Coverage cov : subPcy.getCoverageDetail().getCoverages()) {
						CoverageConfig covConf = subPcyConfig.getCoverageConfig(cov.getCoverageCode());
						if (covConf != null && covConf.getLimitConfigs() != null && !covConf.getLimitConfigs().isEmpty()) {
							LimitConfig lc = covConf.getLimitConfigs().get(0);
							if (lc.isSystemControlled()) {
								cov.setLimit1(liabilityLimit);
							}
						}
					}
					for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
						EndorsementConfig endConf = subPcyConfig.getEndorsementConfig(end.getEndorsementCd());
						if (endConf != null && endConf.getLimitConfigs() != null && !endConf.getLimitConfigs().isEmpty()) {
							LimitConfig lc = endConf.getLimitConfigs().get(0);
							if (lc.isSystemControlled()) {
								end.setLimit1amount(liabilityLimit);
							}
						}
					}
					for (Risk risk : subPcy.getSubPolicyRisks()) {
						RiskConfig riskConf = subPcyConfig.getRiskType(risk.getRiskType());
						if (riskConf != null) {
							for (Coverage cov : risk.getCoverageDetail().getCoverages()) {
								CoverageConfig covConf = riskConf.getCoverageConfig(cov.getCoverageCode());
								if (covConf != null && covConf.getLimitConfigs() != null && !covConf.getLimitConfigs().isEmpty()) {
									LimitConfig lc = covConf.getLimitConfigs().get(0);
									if (lc.isSystemControlled()) {
										cov.setLimit1(liabilityLimit);
									}
								}
							}
							for (Endorsement end : risk.getCoverageDetail().getEndorsements()) {
								EndorsementConfig endConf = riskConf.getEndorsementConfig(end.getEndorsementCd());
								if (endConf != null && endConf.getLimitConfigs() != null && !endConf.getLimitConfigs().isEmpty()) {
									LimitConfig lc = endConf.getLimitConfigs().get(0);
									if (lc.isSystemControlled()) {
										end.setLimit1amount(liabilityLimit);
									}
								}
							}
						}
					}
				}
			}
			
			for (SubPolicy<?> subPcy : pcyTxn.getSubPolicies()) {
				if (SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
					CGLSubPolicy<Risk> cglSubPcy = (CGLSubPolicy<Risk>)subPcy;
					cglSubPcy.setCglLimit(liabilityLimit);
				}
			}
			
			SpecialtyAutoPackage pcy = (SpecialtyAutoPackage) pcyTxn.getPolicyVersion().getInsurancePolicy();
			pcy.setLiabilityLimit(liabilityLimit);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not update the policy transaction", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected void setSubPolicyDefaults(SubPolicy<?> subPcy) throws InsurancePolicyException {
		super.setSubPolicyDefaults(subPcy);
		//EC-1542 - START
		if (SpecialtyAutoConstants.SUBPCY_CODE_CARGO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
			createDefaultCargoDetails(subPcy);
		} 
		//EC-1542 - END
		//EC-1406 - START
		else if (SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
			CGLSubPolicy<Risk> cglSubPcy = (CGLSubPolicy<Risk>)subPcy;
			cglSubPcy.setCglLimit(getPolicyLiabilityLimit(cglSubPcy.getPolicyTransaction()));
		}
		//EC-1406 - END
		//ESL-943 - START
		else if (SpecialtyAutoConstants.SUBPCY_CODE_GARAGE.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
			if (StringUtils.equalsIgnoreCase(subPcy.getPolicyTransaction().getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_POLICYCHANGE)) {
				createNewSubPolicyEndorsementIfNotExists(subPcy.getPolicyTransaction(), subPcy, SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_OEF72);
			}
		}
		//ESL-943 - END
	}
	
	// ESL-1932
	protected void createDefaultCargoDetails(SubPolicy<?> subPcy) {
		for (Coverage cov : subPcy.getCoverageDetail().getCoverages()) {
			if (SpecialtyAutoConstants.SUBPCY_COV_CODE_CPE.equalsIgnoreCase(cov.getCoverageCode())) {
				cov.getCoveragePremium().setOriginalPremium(SpecialtyAutoConstants.SUBPCY_COV_CODE_CPE_DEF_PREM);
				break;
			}
		}
		CargoSubPolicy<?> cgoSP = (CargoSubPolicy<?>)subPcy;
		CargoDetails cd = new CargoDetails();
		cd.setCargoDescription(SpecialtyAutoConstants.CARGO_DETAILS_DEF_DESCRIPTION);
		cd.setCargoExposure(SpecialtyAutoConstants.CARGO_DETAILS_DEF_CARGO_VALUE);
		cd.setAverage(0.0);
		cd.setuWManual(0.0);
		cd.setCargoPartialPremiums(0.0);
		cd.setCargoValue(0.0);
		cgoSP.addCargoDetails(cd);
	}

	@Override
	public PolicyTransaction<?> updateSpecialtyAutoPackageData(Integer liabilityLimit, String quotePreparedBy,
			LocalDateTime termEffective, Long pcyTxnPK, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		// TODO Auto-generated method stub
		return updateSpecialtyAutoPackageData(liabilityLimit, quotePreparedBy, termEffective, null, pcyTxnPK, userProfile);
	}

	@Override
	public PolicyTransaction<?> updateSpecialtyAutoPackageData(Integer liabilityLimit, String quotePreparedBy,
			LocalDateTime termEffective, String externalQuoteNumber, Long pcyTxnPK, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		PolicyTransaction<?> rv = null;
		if (pcyTxnPK != null) {
			try {
				PolicyTransaction<?> pcyTxn = (new PolicyTransactionDAO()).findById(pcyTxnPK);
				pcyTxn = loadPolicyTransactionFull(pcyTxn, true);
				LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : "");
			
				if (liabilityLimit != null) {
					updatePolicyLiabilityLimitImpl(liabilityLimit, pcyTxn);
				}
				if (StringUtils.isNotBlank(quotePreparedBy)) {
					InsurancePolicy pcy = pcyTxn.getPolicyVersion().getInsurancePolicy();
					pcy.setQuotePreparedBy(quotePreparedBy);
				}
				if (StringUtils.isNotBlank(externalQuoteNumber)) {
					InsurancePolicy pcy = pcyTxn.getPolicyVersion().getInsurancePolicy();
					pcy.setExternalQuoteNum(externalQuoteNumber);
				}
				if (termEffective != null) {
					pcyTxn = updateTermEffective(pcyTxn, termEffective);
					pcyTxn = updateVersionEffective(pcyTxn, termEffective);
				}
				
				pcyTxn = updateCoverageEndorsementReinsured(pcyTxn);
				
				pcyTxn = updatePolicyTransaction(pcyTxn, userProfile);
			} catch (InsurancePolicyException e) {
				throw new SpecialtyAutoPolicyException("Could not update the policy transaction", e);
			}
			LoggedUser.logOut();
		} else {
			throw new SpecialtyAutoPolicyException("The policy transaction PK is NULL");
		}
		return rv;
	}

	private Integer getMaxEndorsementNumber(PolicyTransaction<?> pcyTxn) {
		Integer rv = null;
		Set<Integer> endNumbers = new HashSet<Integer>();
		SubPolicy<?> autoSP = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null) {
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (end.getEndorsementNumber() != null) {
					endNumbers.add(end.getEndorsementNumber());
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : pcyTxn.getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (end.getEndorsementNumber() != null) {
						endNumbers.add(end.getEndorsementNumber());
					}
				}
			}
		}
		for (Endorsement end : pcyTxn.getCoverageDetail().getEndorsements()) {
			if (end.getEndorsementNumber() != null) {
				endNumbers.add(end.getEndorsementNumber());
			}
		}
		rv = (!endNumbers.isEmpty() ? Collections.max(endNumbers) : 0);
		return rv;
	}
	
	private void assignEndorsementSeqNumbers(PolicyTransaction<?> pcyTxn, Boolean isNewBusiness) {
		SubPolicy<?> autoSP = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		Integer endSeq;
		if (isNewBusiness) {
			endSeq = 1;
		} else {
			//get the MAX endorsement sequence number
			endSeq = getMaxEndorsementNumber(pcyTxn) + 1;
		}
		if (autoSP != null) {
			if (isNewBusiness) {
				Endorsement eon114 = autoSP.getCoverageDetail().getEndorsementByCode(SpecialtyAutoConstants.AUTO_SUBPCY_ENDORSEMENT_EON114);
				if (eon114 != null) {
					if (StringUtils.equalsIgnoreCase(eon114.getBusinessStatus(), IAuditable.NEW)) {
						eon114.setEndorsementNumber(endSeq);
						endSeq++;
					}
				}
			}
			for (Endorsement end : autoSP.getCoverageDetail().getEndorsements()) {
				if (!SpecialtyAutoConstants.AUTO_SUBPCY_ENDORSEMENT_EON114.equalsIgnoreCase(end.getEndorsementCd()) && 
						SpecialtyAutoConstants.ENDRSMNT_CODES_SEQUENCING.contains(end.getEndorsementCd())) {
					if (StringUtils.equalsIgnoreCase(end.getBusinessStatus(), IAuditable.NEW)) {
						end.setEndorsementNumber(endSeq);
						endSeq++;
					}
				}
			}
			for (Risk spRisk : autoSP.getSubPolicyRisks()) {
				for (Endorsement end : spRisk.getCoverageDetail().getEndorsements()) {
					if (SpecialtyAutoConstants.ENDRSMNT_CODES_SEQUENCING.contains(end.getEndorsementCd())) {
						if (StringUtils.equalsIgnoreCase(end.getBusinessStatus(), IAuditable.NEW)) {
							end.setEndorsementNumber(endSeq);
							endSeq++;
						}
					}
				}
			}
		}
		for (SubPolicy<?> subPcy : pcyTxn.getSubPolicies()) {
			if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPcy.getSubPolicyTypeCd())) {
				for (Endorsement end : subPcy.getCoverageDetail().getEndorsements()) {
					if (SpecialtyAutoConstants.ENDRSMNT_CODES_SEQUENCING.contains(end.getEndorsementCd())) {
						if (StringUtils.equalsIgnoreCase(end.getBusinessStatus(), IAuditable.NEW)) {
							end.setEndorsementNumber(endSeq);
							endSeq++;
						}
					}
				}
			}
		}
		for (Endorsement end : pcyTxn.getCoverageDetail().getEndorsements()) {
			if (SpecialtyAutoConstants.ENDRSMNT_CODES_SEQUENCING.contains(end.getEndorsementCd())) {
				if (StringUtils.equalsIgnoreCase(end.getBusinessStatus(), IAuditable.NEW)) {
					end.setEndorsementNumber(endSeq);
					endSeq++;
				}
			}
		}
	}
	
	private void assignEndorsementSeqNumbers(PolicyTransaction<?> pcyTxn) {
		assignEndorsementSeqNumbers(pcyTxn, true);
	}

	@SuppressWarnings("unchecked")
	@Override
	public PolicyTransaction<?> postCopyQuote(PolicyTransaction<?> origPcyTxn, PolicyTransaction<?> copyPcyTxn)
			throws InsurancePolicyException {
		copyPcyTxn = super.postCopyQuote(origPcyTxn, copyPcyTxn);
		copyPcyTxn = copyQuoteCommon(origPcyTxn, copyPcyTxn);
		return copyPcyTxn;
	}
	
	@Override
	public PolicyTransaction<?> postCopyRenewalQuote(PolicyTransaction<?> origPcyTxn, PolicyTransaction<?> copyPcyTxn)
			throws InsurancePolicyException {
		copyPcyTxn = super.postCopyRenewalQuote(origPcyTxn, copyPcyTxn);
		copyPcyTxn = copyQuoteCommon(origPcyTxn, copyPcyTxn);
		return copyPcyTxn;
	}

	private PolicyTransaction<?> copyQuoteCommon(PolicyTransaction<?> origPcyTxn, PolicyTransaction<?> copyPcyTxn) {
		SubPolicy<?> origSP = origPcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		if (origSP != null) {
			CGLSubPolicy<Risk> origCglSP = (CGLSubPolicy<Risk>)origSP;
			if (!origCglSP.getCglLocations().isEmpty()) {
				SubPolicy<?> copySP = copyPcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
				if (copySP != null) {
					CGLSubPolicy<Risk> copyCglSP = (CGLSubPolicy<Risk>)copySP;
					for (CGLLocation origCglLoc : origCglSP.getCglLocations()) {
						PolicyLocation copyLoc = (PolicyLocation) copyPcyTxn.findRiskByOriginatingPK(origCglLoc.getPolicyLocation().getPolicyRiskPK());
						if (copyLoc != null) {
							CGLLocation cglLoc = new CGLLocation();
							cglLoc.setPolicyLocation(copyLoc);
							copyCglSP.addCglLocation(cglLoc);
						}
					}
				}
			}
		}
		
		processDriverObjets(copyPcyTxn, origPcyTxn);
		
		return copyPcyTxn;
	}
	
	@SuppressWarnings("unchecked")
	private void processDriverObjets(PolicyTransaction<?> copyPcyTxn, PolicyTransaction<?> origPcyTxn) {
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> origAutoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) origPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> copyAutoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) copyPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);

		if (origAutoSP != null && copyAutoSP != null) {
			copyAutoSP.getSubPolicyRisks().stream().forEach(copyRisk -> {
				SpecialtyVehicleRisk copySVR = copyRisk;
				SpecialtyVehicleRisk origSVR = origAutoSP.findSubPcyRiskByPk(copySVR.getOriginatingPK());
				if (origSVR != null) {
					copySVR.getVehicleDrivers().stream().forEach(copyVD -> {
						if (copyVD.getDriver() == null) {
							VehicleDriver origVD = origSVR.findVehicleDriverByPk(copyVD.getOriginatingPk());
							if (origVD != null) {
								Driver copyDriver = copyPcyTxn.findDriverByOriginatingPk(origVD.getDriver().getPersonPk());
								copyVD.setDriver(copyDriver);
							}
						}
					});
						
					copySVR.getVehicleDriverClaims().stream().forEach(copyVDC -> {
						if (copyVDC.getDriver() == null) {
							VehicleDriverClaim origVDC = origSVR.findVehicleDriverClaimByPk(copyVDC.getOriginatingPk());
							if (origVDC != null) {
								Driver copyDriver = copyPcyTxn.findDriverByOriginatingPk(origVDC.getDriver().getPersonPk());
								copyVDC.setDriver(copyDriver);
							}
						}
					});
				}
			});
		}
	}

	@Override
	public List<CVRate> getAllCVRates() throws SpecialtyAutoPolicyException {
		return (new CVRateDAO()).findAll();
	}
	
	// ESL-134: This for update new auto subpolicy
	@Override
	public SpecialityAutoSubPolicy<?> updateSpecialityAutoSubPolicy(SpecialityAutoSubPolicy<?> specialityAutoSubPolicy, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			if (specialityAutoSubPolicy != null) {
				if (specialityAutoSubPolicy.getFleetBasis() != null && 
					SpecialtyAutoConstants.AUTO_SUBPCY_21B_FLEETBASIS.contains(specialityAutoSubPolicy.getFleetBasis())) {
					createNewSubPolicyEndorsementIfNotExists(specialityAutoSubPolicy.getPolicyTransaction(), specialityAutoSubPolicy, 
															 SpecialtyAutoConstants.AUTO_SUBPCY_ENDORSEMENT_OPCF21B);
				}
				
				updateSubPolicy(specialityAutoSubPolicy, userProfile);
			}
		} catch (Exception ex) {
			throw new SpecialtyAutoPolicyException("Could not save the vehicle risk.", ex);
		}
		return specialityAutoSubPolicy;
	}

	@Override
	public SubPolicy<?> updateSubPolicy(SubPolicy<?> subPolicy, UserProfile userProfile)
			throws InsurancePolicyException {
		
		//ESL-134 - START
		if (SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
			if (subPolicy.getLienholderLessors() != null) {
				LienholderLessorSubPolicy lienLessorSubPolicy = subPolicy.getLienholderLessors().stream().filter(o -> o.getLienholderLessorSubPolicyPK() == null)
																				.findFirst().orElse(null);
				// For adding new Lien/Lessor
				if (lienLessorSubPolicy != null) {
					if (SpecialtyAutoConstants.LIENLESSORTYPE_HLESSOR.equalsIgnoreCase(lienLessorSubPolicy.getLienLessorType())) {
						createNewSubPolicyEndorsementIfNotExists(subPolicy.getPolicyTransaction(), subPolicy, SpecialtyAutoConstants.AUTO_SUBPCY_ENDORSEMENT_OPCF5);
					}
					else if (SpecialtyAutoConstants.LIENLESSORTYPE_HOLDER.equalsIgnoreCase(lienLessorSubPolicy.getLienLessorType())) {
						createNewSubPolicyEndorsementIfNotExists(subPolicy.getPolicyTransaction(), subPolicy, SpecialtyAutoConstants.AUTO_SUBPCY_ENDORSEMENT_OPCF23A);
					}
				}
			}
		}
		//ESL-134 - END
		
		try {
			PolicyTransaction<?> policyTransaction = subPolicy.getPolicyTransaction();
			Risk newRisk = null;
			
			IGenericProductConfig productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
					policyTransaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
					policyTransaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
					policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate());
			if (productConfig != null) {
				if (ObjectUtils.compare(((SpecialtyAutoPackage)policyTransaction.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit(), 
						getPolicyMaxRetainedLimit(policyTransaction).intValue()) > 0) {

					SubPolicy<?> sp = subPolicy;
					SubPolicyConfig spConfig = productConfig.getSubPolicyConfig(sp.getSubPolicyTypeCd());
					if (spConfig != null) {
						if (getCoverageService().needToLoadCoverageDetails(sp.getCoverageDetail())) {
							sp = loadSubPolicy(sp, false);
						}
						
						//subpolicy coverages
						sp.getCoverageDetail().getCoverages().stream().filter(spc -> spc.isActive() || spc.isPending()).forEach(spc -> {
							CoverageConfig covConfig = spConfig.getCoverageConfig(spc.getCoverageCode());
							if (covConfig != null && covConfig.isReinsurable()) {
								spc.setReinsured(Boolean.TRUE);
							}																
						});

						//subpolicy endorsements
						sp.getCoverageDetail().getEndorsements().stream().filter(spe -> spe.isActive() || spe.isPending()).forEach(spe -> {
							EndorsementConfig endConfig = spConfig.getEndorsementConfig(spe.getEndorsementCd());
							if (endConfig != null && endConfig.isReinsurable()) {
								spe.setReinsured(Boolean.TRUE);
							}
						});

						//For saving new risk
						newRisk = subPolicy.getSubPolicyRisks().stream().filter(o -> o.getPolicyRiskPK() == null).findFirst().orElse(null);
						if (newRisk != null) {
							Risk spr = newRisk;
							RiskConfig spRiskConfig = spConfig.getRiskType(spr.getRiskType());
							if (spRiskConfig != null) {
								//subpolicy risk coverages
								spr.getCoverageDetail().getCoverages().stream().filter(sprc -> spr.isActive() || sprc.isPending()).forEach(sprc -> {
									CoverageConfig covConfig = spRiskConfig.getCoverageConfig(sprc.getCoverageCode());
									if (covConfig != null && covConfig.isReinsurable()) {
										sprc.setReinsured(Boolean.TRUE);
									}																										
								});

								//subpolicy risk endorsements
								spr.getCoverageDetail().getEndorsements().stream().filter(spre -> spre.isActive() || spre.isPending()).forEach(spre -> {
									EndorsementConfig endConfig = spRiskConfig.getEndorsementConfig(spre.getEndorsementCd());
									if (endConfig != null && endConfig.isReinsurable()) {
										spre.setReinsured(Boolean.TRUE);
									}
								});
							}
						}
					}
				}
			}
			
			if (newRisk != null) {
				rollupPolicy(policyTransaction, subPolicy, newRisk, true, userProfile);
			}
			else {
				subPolicy = super.updateSubPolicy(subPolicy, userProfile);
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception in updateSubPolicy", e);
			throw new InsurancePolicyException("Exception in updateSubPolicy", e);
		}
		
		return subPolicy;
	}
	
	private void assignVehicleSequence(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy) {
		if (autoSubPolicy != null &&
			autoSubPolicy.getSubPolicyRisks().stream().filter(o -> o.getVehicleSequence() == null).findAny().isPresent()) {
			OptionalInt maxSeq = autoSubPolicy.getSubPolicyRisks().stream().filter(o -> o.getVehicleSequence() != null).collect(Collectors.toList())
									.stream().mapToInt(SpecialtyVehicleRisk::getVehicleSequence)
									.max();
			// Array can be changed inside inner scope
			int[] nextSeq = new int[] {(maxSeq.isPresent() ? maxSeq.getAsInt()+1 : 1)};
			autoSubPolicy.getSubPolicyRisks().stream().filter(o -> o.getVehicleSequence() == null).collect(Collectors.toList())
							.stream().forEach(o -> o.setVehicleSequence(nextSeq[0]++));
		}
	}
	
	private void reSequenceVehicleSequences(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy) {
		if (autoSubPolicy != null &&
			!autoSubPolicy.getSubPolicyRisks().isEmpty()) {
			// In case null VehicleSequence found
			assignVehicleSequence(autoSubPolicy);
			
			// Array can be changed inside inner scope
			int[] nextSeq = new int[] {(1)};
			autoSubPolicy.getSubPolicyRisks().stream().sorted(Comparator.comparing(SpecialtyVehicleRisk::getVehicleSequence)).collect(Collectors.toList())
							.stream().forEach(o -> o.setVehicleSequence(nextSeq[0]++));
		}
	}
	
	// Set new coverage default
	private void setCoverageDefaults(PolicyTransaction<?> policyTransaction) throws InsurancePolicyException {
		CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		if (cglSubPolicy != null) {
			// For new CGL only
			if (cglSubPolicy.getSubPolicyPK() == null) {
				if (cglSubPolicy.getCoverageDetail() != null && 
						cglSubPolicy.getCoverageDetail().getCoverages() != null &&
						!cglSubPolicy.getCoverageDetail().getCoverages().isEmpty()) {
					cglSubPolicy.getCoverageDetail().getCoverages().forEach(o -> 
							getCoverageService().setSubpolicyCoverageDefault(policyTransaction, cglSubPolicy, o)
					);
				}
			}
		}
		
		
		//ESL-2002
		CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
		if (cargoSubPolicy != null)
		{
			//For new cargo only 
			if (cargoSubPolicy.getSubPolicyPK() == null) {
				if (cargoSubPolicy.getCoverageDetail() != null && 
						cargoSubPolicy.getCoverageDetail().getCoverages() != null &&
						!cargoSubPolicy.getCoverageDetail().getCoverages().isEmpty()) {
					
					cargoSubPolicy.getCoverageDetail().getCoverages().forEach(o -> 
							getCoverageService().setSubpolicyCoverageDefault(policyTransaction, cargoSubPolicy, o)
					);
					
					cargoSubPolicy.getCoverageDetail().getEndorsements().forEach(o -> 
							getCoverageService().setSubpolicyEndorsementDefault(policyTransaction, cargoSubPolicy, o)
					);
				}
			}
		}


		
		// ESL-899
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialityAutoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (specialityAutoSubPolicy != null) {
			for(SpecialtyVehicleRisk veh : 
					specialityAutoSubPolicy.getSubPolicyRisks().stream()
								.filter(o -> o.getPolicyRiskPK() == null && SpecialtyAutoConstants.SPEC_VEH_DESC_TRAILERS.contains(o.getVehicleDescription()))
								.collect(Collectors.toList())) {
				Coverage cov = veh.getCoverageDetail().getCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AB);
				if (cov != null) {
					veh.getCoverageDetail().removeCoverage(cov);
				}
				cov = veh.getCoverageDetail().getCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_UA);
				if (cov != null) {
					veh.getCoverageDetail().removeCoverage(cov);
				}
			}
		}
	}

	private void changeDefaultEndorsements(PolicyTransaction<?> pcyTxn, SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy) throws InsurancePolicyException {
		if (autoSubPolicy.getSubPolicyRisks() != null && !autoSubPolicy.getSubPolicyRisks().isEmpty()) {
			for(SpecialtyVehicleRisk vehicle : autoSubPolicy.getSubPolicyRisks()) {
				// ESL-617 - only OPCF27B endorsement for new vehicle.
				if (vehicle.getPolicyRiskPK() == null) {
					if (SpecialtyAutoConstants.SPEC_VEH_RISK_DESC_OPCF27B.equalsIgnoreCase(vehicle.getVehicleDescription())) {
						EndorsementConfig endConfig = null;
						IGenericProductConfig productConfig = getProductConfig(pcyTxn);
						if (productConfig != null) {
							SubPolicyConfig subPolicyConfig = productConfig.getSubPolicyConfig(autoSubPolicy.getSubPolicyTypeCd());
							if (subPolicyConfig != null) {
								RiskConfig riskConfig = subPolicyConfig.getRiskType(SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH);
								if (riskConfig != null) {
									endConfig = riskConfig.getEndorsementConfig(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B);
								}
							}
						}
						
						if (endConfig != null) {
							vehicle.getCoverageDetail().getEndorsements().clear();
							vehicle.getCoverageDetail().addEndorsement(createSubPolicyRiskEndorsement(endConfig, pcyTxn, autoSubPolicy, vehicle));
						}
					}
				}
			}
		}
	}

	@Override
	public void createCargoDetails(CargoSubPolicy<?> subPolicy, List<CargoDetails> cargoDetailsList, UserProfile userProfile)
			throws SpecialtyAutoPolicyException {
		try {
			cargoDetailsList.stream().forEach(o -> {
				o.setSystemStatus(Constants.SYSTEN_STATUS_PENDING);
				o.setBusinessStatus(Constants.BUSINESS_STATUS_NEW);
				subPolicy.addCargoDetails(o);
			});
			updateDefaultValuePercentage(subPolicy);
			cargoDetailsList.stream().forEach(o -> getSpecialtyAutoPolicyPolicyService().updateCargoPartialPremium(subPolicy, o));
			getSpecialtyAutoPolicyPolicyService().updateTotalCargoPartialPremium(subPolicy);
			updateSubPolicy(subPolicy, userProfile);
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Could not save new Cargo Details", e);
		}
	}

	@Override
	public void updateCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile)
			throws SpecialtyAutoPolicyException {
		try {
			updateDefaultValuePercentage(subPolicy);
			getSpecialtyAutoPolicyPolicyService().updateCargoPartialPremium(subPolicy, cargoDetails);
			getSpecialtyAutoPolicyPolicyService().updateTotalCargoPartialPremium(subPolicy);
			updateSubPolicy(subPolicy, userProfile);
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Could not save Cargo Details", e);
		}
	}
	
	@Override
	public void deleteCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile)
			throws SpecialtyAutoPolicyException {
		try {
			CargoDetails cgdToDelete = subPolicy.findCargoDetailsByPK(cargoDetails.getCargoDetailsPK());
			if (cgdToDelete != null) {
				subPolicy.removedCargoDetails(cgdToDelete);
				updateDefaultValuePercentage(subPolicy);
				getSpecialtyAutoPolicyPolicyService().updateTotalCargoPartialPremium(subPolicy);
				updateSubPolicy(subPolicy, userProfile);
			}
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Could not delete Cargo Details", e);
		}
	}

	@Override
	public void undeleteCargoDetails(CargoSubPolicy<?> subPolicy, CargoDetails cargoDetails, UserProfile userProfile)
			throws SpecialtyAutoPolicyException {
		if (cargoDetails != null) {
			CargoDetails cgdToUnDelete = subPolicy.findCargoDetailsByPK(cargoDetails.getCargoDetailsPK());
			if (cgdToUnDelete != null) {
				if (cgdToUnDelete.isInactive() && cgdToUnDelete.isDeleted()) {
					cgdToUnDelete.setSystemStatus(IAuditable.ACTIVE);
					cgdToUnDelete.setBusinessStatus(IAuditable.NOCHANGE);
					updateDefaultValuePercentage(subPolicy);
					getSpecialtyAutoPolicyPolicyService().updateTotalCargoPartialPremium(subPolicy);
					try {
						updateSubPolicy(subPolicy, userProfile);
					} catch (Exception e) {
						throw new SpecialtyAutoPolicyException("Undelete cargo details failed", e);
					}
				} else {
					throw new SpecialtyAutoPolicyException("Undelete cargo details failed. The cargo details object has the wrong status field values: " + 
							cgdToUnDelete.getSystemStatus() + "/" + cgdToUnDelete.getBusinessStatus());
				}
			} else {
				throw new SpecialtyAutoPolicyException("Undelete cargo details failed. Couldn't find cargo details object with PK: " + cargoDetails.getCargoDetailsPK());
			}			
		} else {
			throw new SpecialtyAutoPolicyException("The cargo details object to undelete is NULL");
		}
	}

	private CargoDetails updateDefaultValuePercentage(CargoSubPolicy<?> subPolicy) {
		CargoDetails defaultDetails = null;
		
		if (subPolicy.getCargoDetails() != null && !subPolicy.getCargoDetails().isEmpty()) {
			defaultDetails = subPolicy.getCargoDetails().stream()
											.filter(o -> ((o.isActive() || o.isPending()) && SpecialtyAutoConstants.CARGO_DETAILS_DEF_DESCRIPTION.equalsIgnoreCase(o.getCargoDescription())))
											.findFirst().orElse(null);
			if (defaultDetails != null) {
				Double others = new BigDecimal(
										subPolicy.getCargoDetails().stream()
													.filter(o -> ((o.isActive() || o.isPending()) && !SpecialtyAutoConstants.CARGO_DETAILS_DEF_DESCRIPTION.equalsIgnoreCase(o.getCargoDescription())))
													.collect(Collectors.toList())
													.stream().mapToDouble(CargoDetails::getCargoExposure).sum()
													
											).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
				
				Double result = new BigDecimal(100 - others).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
				if (result.doubleValue() > 0.00d) {
					defaultDetails.setCargoExposure(result.doubleValue());
				}
				else {
					defaultDetails.setCargoExposure(0D);
				}
			}
		}
		
		return defaultDetails;
	}

	@Override
	protected PolicyTransaction<?> rollupRatedPremiums(PolicyTransaction<?> pcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		PolicyTransaction<?> policyTransaction = super.rollupRatedPremiums(pcyTxn, userProfile);
		
		CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) pcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSubPolicy != null) {
			getSpecialtyAutoPolicyPolicyService().updateAllCargoPartialPremium(cargoSubPolicy);
		}
		
		return policyTransaction;
	}

	@Override
	public CGLSubPolicy<?> updateCGLSubPolicy(CGLSubPolicy<?> cglSubPolicy, CGLSubPolicy<?> prevCGLSubPolicy,
			UserProfile userProfile) throws SpecialtyAutoPolicyException {
		
		try {
			getSpecialtyAutoPolicyPolicyService().applyCGLDeductibleChanged(cglSubPolicy, prevCGLSubPolicy);
			cglSubPolicy = (CGLSubPolicy<?>) updateSubPolicy(cglSubPolicy, userProfile);
			
			// ESL-894 Addition of endorsement automatically
			if (StringUtils.equalsIgnoreCase(cglSubPolicy.getPolicyTransaction().getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_POLICYCHANGE) &&
				cglSubPolicy.isModified()) {
				if (createNewSubPolicyEndorsementIfNotExists(cglSubPolicy.getPolicyTransaction(), cglSubPolicy, SpecialtyAutoConstants.CGL_SUBPCY_ENDORSEMENT_MOCC)
						!= null) {
					cglSubPolicy = (CGLSubPolicy<?>) (new SubPolicyDAO()).merge(cglSubPolicy);
				}
			}
			
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not save CGL subpolicy", e);
		}

		return cglSubPolicy;
	}

	@Override
	public CargoSubPolicy<?> updateCargoSubPolicy(CargoSubPolicy<?> cargoSubPolicy, CargoSubPolicy<?> prevCargoSubPolicy, 
			UserProfile userProfile) throws SpecialtyAutoPolicyException {
				
		try {
			cargoSubPolicy = (CargoSubPolicy<?>) updateSubPolicy(cargoSubPolicy, userProfile);
			
			// ESL-894 Addition of endorsement automatically
			if (StringUtils.equalsIgnoreCase(cargoSubPolicy.getPolicyTransaction().getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_POLICYCHANGE) &&
					cargoSubPolicy.isModified()) {
				if (createNewSubPolicyEndorsementIfNotExists(cargoSubPolicy.getPolicyTransaction(), cargoSubPolicy, SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_MOCC)
						!= null) {
					cargoSubPolicy = (CargoSubPolicy<?>) (new SubPolicyDAO()).merge(cargoSubPolicy);
				}
			}
			
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not save CARGO subpolicy", e);
		}
		
		return cargoSubPolicy;
	}

	@Override
	public PolicyTransaction<?> postCreatePolicyChange(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn,
			LocalDate changeEffDate, UserProfile userProfile) throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyChange(pcyTxn, origpcyTxn, changeEffDate, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}

	@Override
	public PolicyTransaction<?> postCreatePolicyAdjustment(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn,
			LocalDate changeEffDate, UserProfile userProfile) throws InsurancePolicyException {
		// ESL-2057: add adjustments endorsements automatically
		final SpecialityAutoSubPolicy<?> autoSubPolicy = (SpecialityAutoSubPolicy<?>) pcyTxn
				.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSubPolicy != null) {
			switch (autoSubPolicy.getFleetBasis()) {
			case "21B5": // 50/50 annual
				createSubPolicyEndorsementIfNotExists(pcyTxn, autoSubPolicy, "EON116");
				break;
			case "21BQ": // prorata quarterly
			case "21BS": // prorata semi-annual
			case "21BA": // prorata annual
				createSubPolicyEndorsementIfNotExists(pcyTxn, autoSubPolicy, "EON117");
				break;
			}
		}
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}

	@SuppressWarnings("unchecked")
	private void copyCGLLocations(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn) {
		SubPolicy<?> origSP = origpcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		if (origSP != null) {
			CGLSubPolicy<Risk> origCglSP = (CGLSubPolicy<Risk>)origSP;
			if (!origCglSP.getCglLocations().isEmpty()) {
				SubPolicy<?> copySP = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
				if (copySP != null) {
					CGLSubPolicy<Risk> copyCglSP = (CGLSubPolicy<Risk>)copySP;
					for (CGLLocation origCglLoc : origCglSP.getCglLocations()) {
						PolicyLocation copyLoc = (PolicyLocation) pcyTxn.findRiskByOriginatingPK(origCglLoc.getPolicyLocation().getPolicyRiskPK());
						if (copyLoc != null) {
							CGLLocation cglLoc = new CGLLocation();
							cglLoc.setPolicyLocation(copyLoc);
							cglLoc.setBusinessStatus(origCglLoc.getBusinessStatus());
							cglLoc.setSystemStatus(origCglLoc.getSystemStatus());
							copyCglSP.addCglLocation(cglLoc);
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	private void copyCommercialPropertyLocations(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origPcyTxn) {
		SubPolicy<?> originalSubPolicy = origPcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (originalSubPolicy != null) {
			CommercialPropertySubPolicy<Risk> originalCPSubPolicy = (CommercialPropertySubPolicy<Risk>) originalSubPolicy;
			
			if (!originalCPSubPolicy.getCommercialPropertyLocations().isEmpty()) {
				SubPolicy<?> copySubPolicy = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
				if (copySubPolicy != null) {
					CommercialPropertySubPolicy<Risk> copyCPSubPolicy = (CommercialPropertySubPolicy<Risk>) copySubPolicy;
					
					for (CommercialPropertyLocation originalCPLocation : originalCPSubPolicy.getCommercialPropertyLocations()) {
						PolicyLocation copyLocation = (PolicyLocation) pcyTxn.findRiskByOriginatingPK(originalCPLocation.getPolicyLocation().getPolicyRiskPK());
						
						if (copyLocation != null) {
							CommercialPropertyLocation copyCPLocation = new CommercialPropertyLocation();
							copyCPLocation.setPolicyLocation(copyLocation);
							copyCPLocation.setBusinessStatus(originalCPLocation.getBusinessStatus());
							copyCPLocation.setSystemStatus(originalCPLocation.getSystemStatus());
							copyCPSubPolicy.addCommercialPropertyLocation(copyCPLocation);
						}
					}
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PolicyTransaction<?> postCreatePolicyCancellation(PolicyTransaction<?> pcyTxn,
			PolicyTransaction<?> origpcyTxn, LocalDate cancellationEffDate, UserProfile userProfile)
			throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyCancellation(pcyTxn, origpcyTxn, cancellationEffDate, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) pcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			createPolicyEndorsementIfNotExists(pcyTxn, SpecialtyAutoConstants.PCY_ENDORSEMENT_EON121);
			//added by Marwan Sep. 1, 2022
			processDriverObjets(pcyTxn, origpcyTxn);
			//end of additions
		}
		return pcyTxn;
	}
	
	@Override
	public PolicyVersion postCreatePolicyRenewal(PolicyVersion pcyVer, PolicyVersion origpcyVer, UserProfile userProfile)
			throws InsurancePolicyException {
		pcyVer = super.postCreatePolicyRenewal(pcyVer, origpcyVer, userProfile);
		// ESL-1145 - do not carry forward the broker notes
		if (pcyVer.getPolicyNotes() != null && !pcyVer.getPolicyNotes().isEmpty()) {
			pcyVer.getPolicyNotes().removeIf(o -> BooleanUtils.isFalse(o.getIsInternal()));
		}
		return pcyVer;
	}

	@Override
	public PolicyTransaction<?> postCreatePolicyRenewal(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyRenewal(pcyTxn, origpcyTxn, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}
	
	@Override
	public PolicyTransaction<?> postCreatePolicyExtension(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyExtension(pcyTxn, origpcyTxn, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}

	@Override
	public PolicyTransaction<?> postCreatePolicyReissue(PolicyTransaction<?> pcyTxn, PolicyTransaction<?> origpcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyReissue(pcyTxn, origpcyTxn, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}

	@Override
	public PolicyTransaction<?> postCreatePolicyReinstatement(PolicyTransaction<?> pcyTxn,
			PolicyTransaction<?> origpcyTxn, UserProfile userProfile) throws InsurancePolicyException {
		pcyTxn = super.postCreatePolicyReinstatement(pcyTxn, origpcyTxn, userProfile);
		copyCGLLocations(pcyTxn, origpcyTxn);
		copyCommercialPropertyLocations(pcyTxn, origpcyTxn);
		processDriverObjets(pcyTxn, origpcyTxn);
		return pcyTxn;
	}

	@Override
	public PolicyTransaction<?> preIssuePolicyAdjustment(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		pcyTxn = super.preIssuePolicyAdjustment(pcyTxn);
		//Change logic - moved to consolidatePendingVehicleAdjustment
		//consolidatePendingPolicyAdjustments(pcyTxn);
		pcyTxn = ratePolicyTransaction(pcyTxn, null);

		/*
		 * ESL-2093 do not change unit rate during adjustments
		 * 
		 * For each vehicle that has at least one adjustment, reset its unit rate to
		 * that of its originating vehicle. No need to reset vehicles without any
		 * adjustments, as their unit rate shouldn't have changed.
		 */
		@SuppressWarnings("unchecked")
		final Set<SpecialtyVehicleRisk> vehicles = Optional.ofNullable(pcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY))
			.map(SpecialityAutoSubPolicy.class::cast)
			.map(SpecialityAutoSubPolicy::getSubPolicyRisks)
			.orElseGet(HashSet::new);
		for (SpecialtyVehicleRisk vehicle : vehicles) {
			if (!vehicle.getSvrAdjustments().isEmpty()) {
				try {
					SpecialtyPolicyItemDTO itemDTO = getOriginalSvrValues(vehicle);
					if (itemDTO != null) {
						vehicle.setUnitRate(itemDTO.getOriginatingUnitRate());
						//vehicle.setUnitRate(getOriginatingUnitRate(vehicle));
					}
				} catch (Exception e) {
					throw new InsurancePolicyException("Failed to find original vehcile unit rate", e);
				}
			}
		}

		return pcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateNewOffsetTransaction(PolicyTransaction<?> inPcyTxn, LocalDate changeEffDate,
			UserProfile userProfile) {
		inPcyTxn = super.updateNewOffsetTransaction(inPcyTxn, changeEffDate, userProfile);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				if (pl.getLocationAddress() != null) 
					pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				aiv.setBusinessStatus(IAuditable.NOCHANGE);
			}
			
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				lhlVD.setBusinessStatus(IAuditable.NOCHANGE);
			}
			autoSP.getSubPolicyRisks().stream().filter(r -> StringUtils.equals(r.getRiskType(), SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)).forEach(risk -> {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				svr.getSvrAdjustments().stream().forEach(svra -> {
					svra.setBusinessStatus(IAuditable.NOCHANGE);
				});
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> vd.setBusinessStatus(IAuditable.NOCHANGE));
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> vdc.setBusinessStatus(IAuditable.NOCHANGE));
				}
			});
		}
		
		//update the CGLLocation status fields
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				cglLoc.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				location.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> cgd.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateNewOnsetTransaction(PolicyTransaction<?> inPcyTxn, LocalDate changeEffDate,
			UserProfile userProfile) {
		inPcyTxn = super.updateNewOnsetTransaction(inPcyTxn, changeEffDate, userProfile);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				if (pl.getLocationAddress() != null) {
					pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
					if (!pl.getLocationAddress().isInactive()) {
						pl.getLocationAddress().setVersionDate(changeEffDate);
					}
				}
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				aiv.setBusinessStatus(IAuditable.NOCHANGE);
				if (!aiv.isInactive()) {
					aiv.setVersionDate(changeEffDate);
				}
			}
			
			for(LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				lhlVD.setBusinessStatus(IAuditable.NOCHANGE);
				if (!lhlVD.isInactive()) {
					lhlVD.setVersionDate(changeEffDate);
				}
			}
			autoSP.getSubPolicyRisks().stream().filter(r -> StringUtils.equals(r.getRiskType(), SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)).forEach(risk -> {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				svr.getSvrAdjustments().stream().forEach(svra -> {
					svra.setBusinessStatus(IAuditable.NOCHANGE);
					if (!svra.isInactive()) {
						svra.setVersionDate(changeEffDate);
					}
				});
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> {
						vd.setBusinessStatus(IAuditable.NOCHANGE);
						if (!vd.isInactive()) {
							vd.setVersionDate(changeEffDate);
						}
					});
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> {
						vdc.setBusinessStatus(IAuditable.NOCHANGE);
						if (!vdc.isInactive()) {
							vdc.setVersionDate(changeEffDate);
						}
					});
				}
			});
		}
		
		//update the CGLLocation status fields
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				cglLoc.setBusinessStatus(IAuditable.NOCHANGE);
				if (!cglLoc.isInactive()) {
					cglLoc.setVersionDate(changeEffDate);
				}
			}
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				location.setBusinessStatus(IAuditable.NOCHANGE);
				if (!location.isInactive()) {
					location.setVersionDate(changeEffDate);
				}
			}
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> {
				cgd.setBusinessStatus(IAuditable.NOCHANGE);
				if (!cgd.isInactive()) {
					cgd.setVersionDate(changeEffDate);
				}
			});
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateCancellationOffsetTransaction(PolicyTransaction<?> inPcyTxn,
			LocalDate cancellationEffDate, UserProfile userProfile) throws InsurancePolicyException {
		
		inPcyTxn = super.updateCancellationOffsetTransaction(inPcyTxn, cancellationEffDate, userProfile);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		locationList.stream().forEach(r -> {
			PolicyLocation pl = (PolicyLocation)r;
			if (pl.getLocationAddress() != null) 
				pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
		});
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			autoSP.getAdditionalInsuredVehicles().stream().forEach(aiv -> aiv.setBusinessStatus(IAuditable.NOCHANGE));
			autoSP.getLienholderLessorVehicleDetails().stream().forEach(lhlVD -> lhlVD.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		//update the CGLLocation status fields
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			cglSP.getCglLocations().stream().forEach(cglLoc -> cglLoc.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			commercialPropertySubPolicy.getCommercialPropertyLocations().stream()
				.forEach(location -> location.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> cgd.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		return inPcyTxn;
	}

	@Override
	protected PolicyTransaction<?> updateReinstatementOnsetTransaction(PolicyTransaction<?> inPcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.updateReinstatementOnsetTransaction(inPcyTxn, userProfile);
		inPcyTxn = updateBusinessStatusForLOBObjects(inPcyTxn);
		return inPcyTxn;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateReissueOnsetTransaction(PolicyTransaction<?> inPcyTxn, UserProfile userProfile)
			throws InsurancePolicyException {
		inPcyTxn = super.updateReissueOnsetTransaction(inPcyTxn, userProfile);
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		locationList.stream().forEach(r -> {
			PolicyLocation pl = (PolicyLocation)r;
			if (pl.getLocationAddress() != null) 
				pl.getLocationAddress().setBusinessStatus(IAuditable.NEW);
				pl.getLocationAddress().setSystemStatus(IAuditable.PENDING);
		});
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			autoSP.getAdditionalInsuredVehicles().stream().forEach(aiv -> {
				aiv.setBusinessStatus(IAuditable.NEW);
				aiv.setSystemStatus(IAuditable.PENDING);
			});
			autoSP.getLienholderLessorVehicleDetails().stream().forEach(lhlVD -> {
				lhlVD.setBusinessStatus(IAuditable.NEW);
				lhlVD.setSystemStatus(IAuditable.PENDING);
			});
		}
		CGLSubPolicy<?> cglSP = (CGLSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			cglSP.getCglLocations().stream().forEach(cglLoc -> {
				cglLoc.setBusinessStatus(IAuditable.NEW);
				cglLoc.setSystemStatus(IAuditable.PENDING);
			});
		}
		
		CommercialPropertySubPolicy<?> commercialPropertySubPolicy = (CommercialPropertySubPolicy<?>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			commercialPropertySubPolicy.getCommercialPropertyLocations().stream().forEach(location -> {
					location.setBusinessStatus(IAuditable.NEW);
					location.setSystemStatus(IAuditable.PENDING);
				});
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> {
				cgd.setBusinessStatus(IAuditable.NEW);
				cgd.setSystemStatus(IAuditable.PENDING);				
			});
		}
		
		return inPcyTxn;
	}

	@Override
	protected PolicyTransaction<?> updateReissueOffsetTransaction(PolicyTransaction<?> inPcyTxn,
			UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.updateReissueOffsetTransaction(inPcyTxn, userProfile);
		inPcyTxn = updateBusinessStatusForLOBObjects(inPcyTxn);
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	private PolicyTransaction<?> updateBusinessStatusForLOBObjects(PolicyTransaction<?> inPcyTxn) {
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		locationList.stream().forEach(r -> {
			PolicyLocation pl = (PolicyLocation)r;
			if (pl.getLocationAddress() != null) 
				pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
		});
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			autoSP.getAdditionalInsuredVehicles().stream().forEach(aiv -> aiv.setBusinessStatus(IAuditable.NOCHANGE));
			autoSP.getLienholderLessorVehicleDetails().stream().forEach(lhlVD -> lhlVD.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		//update the CGLLocation status fields
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			cglSP.getCglLocations().stream().forEach(cglLoc -> cglLoc.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			commercialPropertySubPolicy.getCommercialPropertyLocations().stream()
				.forEach(location -> location.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> cgd.setBusinessStatus(IAuditable.NOCHANGE));
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> processReversalOffset(PolicyTransaction<?> inPcyTxn, PolicyTransaction<?> origOnset, UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.processReversalOffset(inPcyTxn, origOnset, userProfile);
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				aiv.setBusinessStatus(IAuditable.NOCHANGE);
			}
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				lhlVD.setBusinessStatus(IAuditable.NOCHANGE);
			}
			//vehicle driver
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> vd.setBusinessStatus(IAuditable.NOCHANGE));
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> vdc.setBusinessStatus(IAuditable.NOCHANGE));
				}
			}
		}
		
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				cglLoc.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				location.setBusinessStatus(IAuditable.NOCHANGE);
			}
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> processReversalOnset(PolicyTransaction<?> inPcyTxn, PolicyTransaction<?> inPcyTxnOffset, PolicyTransaction<?> origOnset, PolicyTransaction<?> origOffset, UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.processReversalOnset(inPcyTxn, inPcyTxnOffset, origOnset, origOffset, userProfile);
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				//pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
				if (pl.getLocationAddress().isNew()) {
					pl.getLocationAddress().setBusinessStatus(IAuditable.DELETED);
					pl.getLocationAddress().setSystemStatus(IAuditable.INACTIVE);
				} else if (pl.getLocationAddress().isDeleted()) {
					pl.getLocationAddress().setBusinessStatus(IAuditable.UNDELETED);
					pl.getLocationAddress().setSystemStatus(IAuditable.ACTIVE);
				} else if (pl.getLocationAddress().isModified()) {
					Risk curentOffsetRisk = inPcyTxnOffset.findRiskByOriginatingPK(r.getOriginatingPK());
					if (curentOffsetRisk != null) {
						Risk origOnsetRisk = origOnset.findRiskByPk(curentOffsetRisk.getOriginatingPK());
						if (origOnsetRisk != null) {
							Risk origOffsetRisk = origOffset.findRiskByPk(origOnsetRisk.getOriginatingPK());
							if (Objects.isNull(origOffsetRisk)) {
								origOffsetRisk = origOffset.findRiskByOriginatingPK(origOnsetRisk.getOriginatingPK());
							}
							if (origOffsetRisk != null) {
								ObjectCopyUtils.getInstance().copyAuditable(pl.getLocationAddress(), ((PolicyLocation)origOffsetRisk).getLocationAddress());
							}
						}
					}
				}
				//added by Marwan - May 17, 2021
				if (pl.getLocationAddress() != null) {
					pl.getLocationAddress().setAddressPK(null);
				}
				//end of additions
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		SpecialityAutoSubPolicy<Risk> origOnsetAutoSP = (SpecialityAutoSubPolicy<Risk>) origOnset.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		SpecialityAutoSubPolicy<Risk> origOffsetAutoSP = (SpecialityAutoSubPolicy<Risk>) origOffset.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				if (aiv.isNew()) {
					aiv.setBusinessStatus(IAuditable.DELETED);
					aiv.setSystemStatus(IAuditable.INACTIVE);
				} else if (aiv.isDeleted()) {
					aiv.setBusinessStatus(IAuditable.UNDELETED);
					aiv.setSystemStatus(IAuditable.ACTIVE);
				} else if (aiv.isModified()) {
					AdditionalInsuredVehicles origOnsetAIV = origOnsetAutoSP.findADIVehicleByPK(aiv.getOriginatingPK());
					if (origOnsetAIV != null) {
						AdditionalInsuredVehicles origOffsetAIV = origOffsetAutoSP.findADIVehicleByPK(origOnsetAIV.getOriginatingPK());
						if (Objects.isNull(origOffsetAIV)) {
							origOffsetAIV = origOffsetAutoSP.findADIVehicleByOriginatingPK(origOnsetAIV.getOriginatingPK());
						}
						if (origOffsetAIV != null) {
							ObjectCopyUtils.getInstance().copyAuditable(aiv, origOffsetAIV);
						}
					}
				}
			}
			
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				if (lhlVD.isNew()) {
					lhlVD.setBusinessStatus(IAuditable.DELETED);
					lhlVD.setSystemStatus(IAuditable.INACTIVE);
				} else if (lhlVD.isDeleted()) {
					lhlVD.setBusinessStatus(IAuditable.UNDELETED);
					lhlVD.setSystemStatus(IAuditable.ACTIVE);
				} else if (lhlVD.isModified()) {
					LienholderLessorVehicleDetails origOnsetLhlVD = origOnsetAutoSP.findLHLVehDetailsByPk(lhlVD.getLienholderLessorVehicleDetailsPK());
					if (origOnsetLhlVD != null) {
						LienholderLessorVehicleDetails origOffsetLhlVD = origOffsetAutoSP.findLHLVehDetailsByPk(origOnsetLhlVD.getOriginatingPK());
						if (Objects.isNull(origOffsetLhlVD)) {
							origOffsetLhlVD = origOffsetAutoSP.findLHLVehDetailsByOriginatingPK(origOnsetLhlVD.getOriginatingPK());
						}
						if (origOffsetLhlVD != null) {
							ObjectCopyUtils.getInstance().copyAuditable(lhlVD, origOffsetLhlVD);
						}
					}
				}
			}
			
			//vehicle driver
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					for (VehicleDriver vd : svr.getVehicleDrivers()) {
						if (vd.isNew()) {
							vd.setBusinessStatus(IAuditable.DELETED);
							vd.setSystemStatus(IAuditable.INACTIVE);
						} else if (vd.isDeleted()) {
							vd.setBusinessStatus(IAuditable.UNDELETED);
							vd.setSystemStatus(IAuditable.ACTIVE);
						} else if (vd.isModified()) {
							//find the orig onset vehicle
							SpecialtyVehicleRisk origOnsetSVR = (SpecialtyVehicleRisk) origOnsetAutoSP.findSubPcyRiskByPk(vd.getSpecialtyVehicleRisk().getOriginatingPK());
							//find the orig offset vehicle
							if (origOnsetSVR != null) {
								SpecialtyVehicleRisk origOffsetSVR = (SpecialtyVehicleRisk) origOffsetAutoSP.findSubPcyRiskByPk(origOnsetSVR.getOriginatingPK());
								if (origOffsetSVR != null) {
									VehicleDriver origOnsetVD = origOnsetSVR.findVehicleDriverByPk(vd.getOriginatingPk());
									if (origOnsetVD != null) {
										VehicleDriver origOffsetVD = origOffsetSVR.findVehicleDriverByPk(origOnsetVD.getOriginatingPk());
										if (Objects.isNull(origOffsetVD)) {
											origOffsetVD = origOffsetSVR.findVehicleDriverByOriginatingPk(origOnsetVD.getOriginatingPk());
										}
										if (origOffsetVD != null) {
											ObjectCopyUtils.getInstance().copyAuditable(vd, origOffsetVD);
										}
									}
								}
							}
						}
					}
				}
				
				//vehicle driver claim
				if (svr.getVehicleDriverClaims() != null) {
					for (VehicleDriverClaim vdc : svr.getVehicleDriverClaims()) {
						if (vdc.isNew()) {
							vdc.setBusinessStatus(IAuditable.DELETED);
							vdc.setSystemStatus(IAuditable.INACTIVE);							
						} else if (vdc.isDeleted()) {
							vdc.setBusinessStatus(IAuditable.UNDELETED);
							vdc.setSystemStatus(IAuditable.ACTIVE);							
						} else if (vdc.isModified()) {
							//find the orig onset vehicle
							SpecialtyVehicleRisk origOnsetSVR = (SpecialtyVehicleRisk) origOnsetAutoSP.findSubPcyRiskByPk(vdc.getSpecialtyVehicleRisk().getOriginatingPK());
							//find the orig offset vehicle
							if (origOnsetSVR != null) {
								SpecialtyVehicleRisk origOffsetSVR = (SpecialtyVehicleRisk) origOffsetAutoSP.findSubPcyRiskByPk(origOnsetSVR.getOriginatingPK());
								if (origOffsetSVR != null) {
									VehicleDriverClaim origOnsetVDC = origOnsetSVR.findVehicleDriverClaimByPk(vdc.getOriginatingPk());
									if (origOnsetVDC != null) {
										VehicleDriverClaim origOffsetVDC = origOffsetSVR.findVehicleDriverClaimByPk(origOnsetVDC.getOriginatingPk());
										if (Objects.isNull(origOffsetVDC)) {
											origOffsetVDC = origOffsetSVR.findVehicleDriverClaimByOriginatingPk(origOnsetVDC.getOriginatingPk());
										}
										if (origOffsetVDC != null) {
											ObjectCopyUtils.getInstance().copyAuditable(vdc, origOffsetVDC);
										}
									}
								}
							}
						}
					}
				}
			}			
		}
		
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				if (cglLoc.isNew()) {
					cglLoc.setBusinessStatus(IAuditable.DELETED);
					cglLoc.setSystemStatus(IAuditable.INACTIVE);
				} else if (cglLoc.isDeleted()) {
					cglLoc.setBusinessStatus(IAuditable.UNDELETED);
					cglLoc.setSystemStatus(IAuditable.ACTIVE);
				}
			}
		}		
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				if (location.isNew()) {
					location.setBusinessStatus(IAuditable.DELETED);
					location.setSystemStatus(IAuditable.INACTIVE);
				} else if (location.isDeleted()) {
					location.setBusinessStatus(IAuditable.UNDELETED);
					location.setSystemStatus(IAuditable.ACTIVE);
				}
			}
		}
		
		return inPcyTxn;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> processReversalOnset_V0(PolicyTransaction<?> inPcyTxn, PolicyTransaction<?> inPcyTxnOffset, PolicyTransaction<?> origOnset, PolicyTransaction<?> origOffset, UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.processReversalOnset_V0(inPcyTxn, inPcyTxnOffset, origOnset, origOffset, userProfile);
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				//pl.getLocationAddress().setBusinessStatus(IAuditable.NOCHANGE);
				if (pl.getLocationAddress().isNew()) {
					pl.getLocationAddress().setBusinessStatus(IAuditable.DELETED);
					pl.getLocationAddress().setSystemStatus(IAuditable.INACTIVE);
				} else if (pl.getLocationAddress().isDeleted()) {
					pl.getLocationAddress().setBusinessStatus(IAuditable.UNDELETED);
					pl.getLocationAddress().setSystemStatus(IAuditable.ACTIVE);
				} else if (pl.getLocationAddress().isModified()) {
					Risk origOnsetRisk = origOnset.findRiskByPk(r.getOriginatingPK());
					if (origOnsetRisk != null) {
						Risk origOffsetRisk = origOffset.findRiskByOriginatingPK(origOnsetRisk.getOriginatingPK());
						if (origOffsetRisk != null) {
							ObjectCopyUtils.getInstance().copyAuditable(pl.getLocationAddress(), ((PolicyLocation)origOffsetRisk).getLocationAddress());
						}
					}
				}
				//added by Marwan - May 17, 2021
				if (pl.getLocationAddress() != null) {
					pl.getLocationAddress().setAddressPK(null);
				}
				//end of additions
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		SpecialityAutoSubPolicy<Risk> origOnsetAutoSP = (SpecialityAutoSubPolicy<Risk>) origOnset.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		SpecialityAutoSubPolicy<Risk> origOffsetAutoSP = (SpecialityAutoSubPolicy<Risk>) origOffset.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				if (aiv.isNew()) {
					aiv.setBusinessStatus(IAuditable.DELETED);
					aiv.setSystemStatus(IAuditable.INACTIVE);
				} else if (aiv.isDeleted()) {
					aiv.setBusinessStatus(IAuditable.UNDELETED);
					aiv.setSystemStatus(IAuditable.ACTIVE);
				} else if (aiv.isModified()) {
					AdditionalInsuredVehicles origOnsetAIV = origOnsetAutoSP.findADIVehicleByPK(aiv.getOriginatingPK());
					if (origOnsetAIV != null) {
						AdditionalInsuredVehicles origOffsetAIV = origOffsetAutoSP.findADIVehicleByOriginatingPK(origOnsetAIV.getOriginatingPK());
						if (origOffsetAIV != null) {
							ObjectCopyUtils.getInstance().copyAuditable(aiv, origOffsetAIV);
						}
					}
				}
			}
			
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				if (lhlVD.isNew()) {
					lhlVD.setBusinessStatus(IAuditable.DELETED);
					lhlVD.setSystemStatus(IAuditable.INACTIVE);
				} else if (lhlVD.isDeleted()) {
					lhlVD.setBusinessStatus(IAuditable.UNDELETED);
					lhlVD.setSystemStatus(IAuditable.ACTIVE);
				} else if (lhlVD.isModified()) {
					LienholderLessorVehicleDetails origOnsetLhlVD = origOnsetAutoSP.findLHLVehDetailsByPk(lhlVD.getLienholderLessorVehicleDetailsPK());
					if (origOnsetLhlVD != null) {
						LienholderLessorVehicleDetails origOffsetLhlVD = origOffsetAutoSP.findLHLVehDetailsByOriginatingPK(origOnsetLhlVD.getOriginatingPK());
						if (origOffsetLhlVD != null) {
							ObjectCopyUtils.getInstance().copyAuditable(lhlVD, origOffsetLhlVD);
						}
					}
				}
			}
			
			//vehicle driver
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					for (VehicleDriver vd : svr.getVehicleDrivers()) {
						if (vd.isNew()) {
							vd.setBusinessStatus(IAuditable.DELETED);
							vd.setSystemStatus(IAuditable.INACTIVE);
						} else if (vd.isDeleted()) {
							vd.setBusinessStatus(IAuditable.UNDELETED);
							vd.setSystemStatus(IAuditable.ACTIVE);
						} else if (vd.isModified()) {
							//find the orig onset vehicle
							SpecialtyVehicleRisk origOnsetSVR = (SpecialtyVehicleRisk) origOnsetAutoSP.findSubPcyRiskByPk(vd.getSpecialtyVehicleRisk().getOriginatingPK());
							//find the orig offset vehicle
							if (origOnsetSVR != null) {
								SpecialtyVehicleRisk origOffsetSVR = (SpecialtyVehicleRisk) origOffsetAutoSP.findSubPcyRiskByOriginatingPK(origOnsetSVR.getOriginatingPK());
								if (origOffsetSVR != null) {
									VehicleDriver origOnsetVD = origOnsetSVR.findVehicleDriverByPk(vd.getOriginatingPk());
									if (origOnsetVD != null) {
										VehicleDriver origOffsetVD = origOffsetSVR.findVehicleDriverByOriginatingPk(origOnsetVD.getOriginatingPk());
										if (origOffsetVD != null) {
											ObjectCopyUtils.getInstance().copyAuditable(vd, origOffsetVD);
										}
									}
								}
							}
						}
					}
				}
				
				//vehicle driver claim
				if (svr.getVehicleDriverClaims() != null) {
					for (VehicleDriverClaim vdc : svr.getVehicleDriverClaims()) {
						if (vdc.isNew()) {
							vdc.setBusinessStatus(IAuditable.DELETED);
							vdc.setSystemStatus(IAuditable.INACTIVE);							
						} else if (vdc.isDeleted()) {
							vdc.setBusinessStatus(IAuditable.UNDELETED);
							vdc.setSystemStatus(IAuditable.ACTIVE);							
						} else if (vdc.isModified()) {
							//find the orig onset vehicle
							SpecialtyVehicleRisk origOnsetSVR = (SpecialtyVehicleRisk) origOnsetAutoSP.findSubPcyRiskByPk(vdc.getSpecialtyVehicleRisk().getOriginatingPK());
							//find the orig offset vehicle
							if (origOnsetSVR != null) {
								SpecialtyVehicleRisk origOffsetSVR = (SpecialtyVehicleRisk) origOffsetAutoSP.findSubPcyRiskByOriginatingPK(origOnsetSVR.getOriginatingPK());
								if (origOffsetSVR != null) {
									VehicleDriverClaim origOnsetVDC = origOnsetSVR.findVehicleDriverClaimByPk(vdc.getOriginatingPk());
									if (origOnsetVDC != null) {
										VehicleDriverClaim origOffsetVDC = origOffsetSVR.findVehicleDriverClaimByOriginatingPk(origOnsetVDC.getOriginatingPk());
										if (origOffsetVDC != null) {
											ObjectCopyUtils.getInstance().copyAuditable(vdc, origOffsetVDC);
										}
									}
								}
							}
						}
					}
				}
			}			
		}
		
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				if (cglLoc.isNew()) {
					cglLoc.setBusinessStatus(IAuditable.DELETED);
					cglLoc.setSystemStatus(IAuditable.INACTIVE);
				} else if (cglLoc.isDeleted()) {
					cglLoc.setBusinessStatus(IAuditable.UNDELETED);
					cglLoc.setSystemStatus(IAuditable.ACTIVE);
				}
			}
		}		
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				if (location.isNew()) {
					location.setBusinessStatus(IAuditable.DELETED);
					location.setSystemStatus(IAuditable.INACTIVE);
				} else if (location.isDeleted()) {
					location.setBusinessStatus(IAuditable.UNDELETED);
					location.setSystemStatus(IAuditable.ACTIVE);
				}
			}
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updatePolicyObjStatusFields(PolicyTransaction<?> inPcyTxn, String inSystemStatus,
			String inBusinessStatus) {
		inPcyTxn = super.updatePolicyObjStatusFields(inPcyTxn, inSystemStatus, inBusinessStatus);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				if (pl.getLocationAddress().isPending()) {
					pl.getLocationAddress().setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus))
						pl.getLocationAddress().setBusinessStatus(inBusinessStatus);
				}
			}
		}
		
		//update the AdditionalInsuredVehicles Status Fields
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				if (aiv.isPending()) {
					aiv.setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus)) {
						aiv.setBusinessStatus(inBusinessStatus);
					}
				}
			}
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				if (lhlVD.isPending()) {
					lhlVD.setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus)) {
						lhlVD.setBusinessStatus(inBusinessStatus);
					}
				}
			}
			autoSP.getSubPolicyRisks().stream().filter(r -> StringUtils.equals(r.getRiskType(), SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)).forEach(risk -> {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				svr.getSvrAdjustments().stream().forEach(svra -> {
					if (svra.isPending()) {
						svra.setSystemStatus(inSystemStatus);
						if (StringUtils.isNotBlank(inBusinessStatus)) {
							svra.setBusinessStatus(inBusinessStatus);
						}
					}
				});
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> {
						if (vd.isPending()) {
							vd.setSystemStatus(inSystemStatus);
							if (StringUtils.isNotBlank(inBusinessStatus)) {
								vd.setBusinessStatus(inBusinessStatus);
							}
						}
					});
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> {
						if (vdc.isPending()) {
							vdc.setSystemStatus(inSystemStatus);
							if (StringUtils.isNotBlank(inBusinessStatus)) {
								vdc.setBusinessStatus(inBusinessStatus);
							}
						}
					});
				}

			});
		}
		
		//update the CGLLocation status fields
		CGLSubPolicy<Risk> cglSP = (CGLSubPolicy<Risk>)inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			for (CGLLocation cglLoc : cglSP.getCglLocations()) {
				if (cglLoc.isPending()) {
					cglLoc.setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus)) {
						cglLoc.setBusinessStatus(inBusinessStatus);
					}
				}
			}
		}

		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				if (location.isPending()) {
					location.setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus)) {
						location.setBusinessStatus(inBusinessStatus);
					}
				}
			}
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> {
				if (cgd.isPending()) {
					cgd.setSystemStatus(inSystemStatus);
					if (StringUtils.isNotBlank(inBusinessStatus)) {
						cgd.setBusinessStatus(inBusinessStatus);
					}
				}
			});
		}
		
		return inPcyTxn;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateSystemStatusAfterPolicyChange(PolicyTransaction<?> inPcyTxn,
			String inSystemStatus) {
		inPcyTxn = super.updateSystemStatusAfterPolicyChange(inPcyTxn, inSystemStatus);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				if (pl.getLocationAddress().isActive())
					pl.getLocationAddress().setSystemStatus(inSystemStatus);
			}
		}
		
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				if (aiv.isActive()) 
					aiv.setSystemStatus(inSystemStatus);
			}
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				if (lhlVD.isActive()) {
					lhlVD.setSystemStatus(inSystemStatus);
				}
			}
			autoSP.getSubPolicyRisks().stream().filter(r -> StringUtils.equals(r.getRiskType(), SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH)).forEach(risk -> {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				svr.getSvrAdjustments().stream().forEach(svra -> {
					if (svra.isActive()) {
						svra.setSystemStatus(inSystemStatus);
					}
				});
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> {
						if (vd.isActive()) {
							vd.setSystemStatus(inSystemStatus);
						}	
					});
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> {
						if (vdc.isActive()) {
							vdc.setSystemStatus(inSystemStatus);
						}
					});
				}

			});
		}
		
		// Update the CommercialPropertyLocation status fields
		CommercialPropertySubPolicy<Risk> commercialPropertySubPolicy = (CommercialPropertySubPolicy<Risk>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			for (CommercialPropertyLocation location : commercialPropertySubPolicy.getCommercialPropertyLocations()) {
				if (location.isActive()) {
					location.setSystemStatus(inSystemStatus);
				}
			}
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null) {
			cargoSP.getCargoDetails().stream().forEach(cgd -> {
				if (cgd.isActive()) {
					cgd.setSystemStatus(inSystemStatus);
				}
			});
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> restoreSystemStatusAfterUndoPolicyChange(PolicyTransaction<?> inPcyTxn,
			String inSystemStatus) {
		inPcyTxn = super.restoreSystemStatusAfterUndoPolicyChange(inPcyTxn, inSystemStatus);
		
		//update the PolicyLocation Address Status Fields
		List<Risk> locationList = (List<Risk>) inPcyTxn.findRiskByType(RISK_TYPE_PCY_LOCATION);
		if (!locationList.isEmpty()) {
			for (Risk r : locationList) {
				PolicyLocation pl = (PolicyLocation)r;
				if (pl.getLocationAddress().isInactive() && !pl.getLocationAddress().isDeleted())
					pl.getLocationAddress().setSystemStatus(inSystemStatus);
			}
		}
		
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			for (AdditionalInsuredVehicles aiv : autoSP.getAdditionalInsuredVehicles()) {
				if (aiv.isInactive() && !aiv.isDeleted())
					aiv.setBusinessStatus(IAuditable.NOCHANGE);
			}
			for (LienholderLessorVehicleDetails lhlVD : autoSP.getLienholderLessorVehicleDetails()) {
				if (lhlVD.isInactive() && !lhlVD.isDeleted())
					lhlVD.setBusinessStatus(IAuditable.NOCHANGE);
			}
			//vehicle driver
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					svr.getVehicleDrivers().stream().forEach(vd -> vd.setBusinessStatus(IAuditable.NOCHANGE));
				}
				if (svr.getVehicleDriverClaims() != null) {
					svr.getVehicleDriverClaims().stream().forEach(vdc -> vdc.setBusinessStatus(IAuditable.NOCHANGE));
				}
			}
		}
		
		return inPcyTxn;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateRenewalOnsetTransaction(PolicyTransaction<?> inPcyTxn, Boolean resetPremOverride, UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.updateRenewalOnsetTransaction(inPcyTxn, resetPremOverride, userProfile);
		LocalDate effDate = inPcyTxn.getPolicyTerm().getTermEffDate().toLocalDate();
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {
			reSequenceVehicleSequences(autoSP);
			
			if (autoSP.getAdditionalInsuredVehicles() != null && !autoSP.getAdditionalInsuredVehicles().isEmpty()) {
				Iterator<AdditionalInsuredVehicles> aivItr = autoSP.getAdditionalInsuredVehicles().iterator();
				while (aivItr.hasNext()) {
					AdditionalInsuredVehicles aiv = aivItr.next();
					if (aiv.isInactive()) {
						aiv.setAutoSubPolicy(null);
						aiv.setAdditionalInsured(null);
						aivItr.remove();
					} else {
						aiv.setSystemStatus(IAuditable.PENDING);
						aiv.setBusinessStatus(IAuditable.NEW);
						aiv.setVersionDate(effDate);
					}
				}
			}
			
			if (autoSP.getLienholderLessorVehicleDetails() != null && !autoSP.getLienholderLessorVehicleDetails().isEmpty()) {
				Iterator<LienholderLessorVehicleDetails> lhlvdItr = autoSP.getLienholderLessorVehicleDetails().iterator();
				Integer vehDetSeq = new Integer(1);
				while (lhlvdItr.hasNext()) {
					LienholderLessorVehicleDetails lhlvd = lhlvdItr.next();
					if (lhlvd.isInactive()) {
						lhlvd.setAutoSubPolicy(null);
						lhlvd.setLienholderLessor(null);
						lhlvdItr.remove();
					} else {
						lhlvd.setSystemStatus(IAuditable.PENDING);
						lhlvd.setBusinessStatus(IAuditable.NEW);
						lhlvd.setVehicleDetailsSequence(vehDetSeq++);
						lhlvd.setVersionDate(effDate);
					}
				}
			}
			
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					Iterator<VehicleDriver> vdItr = svr.getVehicleDrivers().iterator();
					while (vdItr.hasNext()) {
						VehicleDriver vd = vdItr.next();
						if (vd.isInactive()) {
							vd.setDriver(null);
							vd.setSpecialtyVehicleRisk(null);
							vdItr.remove();
						} else {
							vd.setSystemStatus(IAuditable.PENDING);
							vd.setBusinessStatus(IAuditable.NEW);
							vd.setVersionDate(effDate);
						}
					}
				}
				
				if (svr.getVehicleDriverClaims() != null) {
					Iterator<VehicleDriverClaim> vdcItr = svr.getVehicleDriverClaims().iterator();
					while (vdcItr.hasNext()) {
						VehicleDriverClaim vdc = vdcItr.next();
						if (vdc.isInactive()) {
							vdc.setDriver(null);
							vdc.setSpecialtyVehicleRisk(null);
							vdcItr.remove();
						} else {
							vdc.setSystemStatus(IAuditable.PENDING);
							vdc.setBusinessStatus(IAuditable.NEW);
							vdc.setVersionDate(effDate);
						}
					}
				}
			}
		}

		CGLSubPolicy<?> cglSP = (CGLSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			// Remove deleted location
			if (cglSP.getCglLocations() != null && !cglSP.getCglLocations().isEmpty()) {
				Iterator<CGLLocation> locItr = cglSP.getCglLocations().iterator();
				while (locItr.hasNext()) {
					CGLLocation loc = locItr.next();
					if (loc.isInactive()) {
						locItr.remove();
					} else {
						loc.setSystemStatus(IAuditable.PENDING);
						loc.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
			
			// ESL-1548
			createSubPolicyEndorsementIfNotExists(inPcyTxn, cglSP, SpecialtyAutoConstants.CGL_SUBPCY_ENDORSEMENT_CDEL);
		}
		
		CommercialPropertySubPolicy<?> commercialPropertySubPolicy = (CommercialPropertySubPolicy<?>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			// Remove deleted locations
			if (commercialPropertySubPolicy.getCommercialPropertyLocations() != null && !commercialPropertySubPolicy.getCommercialPropertyLocations().isEmpty()) {
				Iterator<CommercialPropertyLocation> locationItr = commercialPropertySubPolicy.getCommercialPropertyLocations().iterator();
				while (locationItr.hasNext()) {
					CommercialPropertyLocation location = locationItr.next();
					if (location.isInactive()) {
						locationItr.remove();
					} else {
						location.setSystemStatus(IAuditable.PENDING);
						location.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
		}
		
		CargoSubPolicy<?> cgoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cgoSP != null) {
			if (cgoSP.getCoverageDetail().getEndorsements() != null && !cgoSP.getCoverageDetail().getEndorsements().isEmpty()) {
				cgoSP.getCoverageDetail().getEndorsements().removeIf(o -> SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC.equalsIgnoreCase(o.getEndorsementCd()));
			}
			if (cgoSP.getCargoDetails() != null && !cgoSP.getCargoDetails().isEmpty()) {
				Iterator<CargoDetails> cgdItr = cgoSP.getCargoDetails().iterator();
				while (cgdItr.hasNext()) {
					CargoDetails cgd = cgdItr.next();
					if (cgd.isInactive()) {
						cgdItr.remove();
					} else {
						cgd.setSystemStatus(IAuditable.PENDING);
						cgd.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
		}
		
		GarageSubPolicy<?> garSP = (GarageSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_GARAGESUBPOLICY);
		if (garSP != null) {
			if (garSP.getCoverageDetail().getEndorsements() != null && !garSP.getCoverageDetail().getEndorsements().isEmpty()) {
				garSP.getCoverageDetail().getEndorsements().removeIf(o -> SpecialtyAutoConstants.GARAGE_SUBPCY_ENDORSEMENT_OEF72.equalsIgnoreCase(o.getEndorsementCd()));
			}
		}
		
		if (inPcyTxn.getPolicyRisks() != null && !inPcyTxn.getPolicyRisks().isEmpty()) {
			reNumberLocations(inPcyTxn);
			
			inPcyTxn.getPolicyRisks().stream().filter(o -> RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(o.getRiskType()))
					.map(o -> (PolicyLocation)o)
					.forEach(o -> {
						o.getLocationAddress().setSystemStatus(IAuditable.PENDING);
						o.getLocationAddress().setBusinessStatus(IAuditable.NEW);
					});
		}
		
		return inPcyTxn;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateExtensionOnsetTransaction(PolicyTransaction<?> inPcyTxn, Long origTermDays,
			UserProfile userProfile) throws InsurancePolicyException {
		inPcyTxn = super.updateExtensionOnsetTransaction(inPcyTxn, origTermDays, userProfile);
		LocalDate effDate = inPcyTxn.getPolicyTerm().getTermEffDate().toLocalDate();
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) inPcyTxn.findSubPolicyByType(SUBPOLICY_AUTOSUBPOLICY);
		if (autoSP != null) {			
			if (autoSP.getAdditionalInsuredVehicles() != null && !autoSP.getAdditionalInsuredVehicles().isEmpty()) {
				Iterator<AdditionalInsuredVehicles> aivItr = autoSP.getAdditionalInsuredVehicles().iterator();
				while (aivItr.hasNext()) {
					AdditionalInsuredVehicles aiv = aivItr.next();
					if (aiv.isInactive()) {
						aiv.setAutoSubPolicy(null);
						aiv.setAdditionalInsured(null);
						aivItr.remove();
					} else {
						aiv.setSystemStatus(IAuditable.PENDING);
						aiv.setBusinessStatus(IAuditable.NEW);
						aiv.setVersionDate(effDate);
					}
				}
			}
			
			if (autoSP.getLienholderLessorVehicleDetails() != null && !autoSP.getLienholderLessorVehicleDetails().isEmpty()) {
				Iterator<LienholderLessorVehicleDetails> lhlvdItr = autoSP.getLienholderLessorVehicleDetails().iterator();
				Integer vehDetSeq = new Integer(1);
				while (lhlvdItr.hasNext()) {
					LienholderLessorVehicleDetails lhlvd = lhlvdItr.next();
					if (lhlvd.isInactive()) {
						lhlvd.setAutoSubPolicy(null);
						lhlvd.setLienholderLessor(null);
						lhlvdItr.remove();
					} else {
						lhlvd.setSystemStatus(IAuditable.PENDING);
						lhlvd.setBusinessStatus(IAuditable.NEW);
						lhlvd.setVehicleDetailsSequence(vehDetSeq++);
						lhlvd.setVersionDate(effDate);
					}
				}
			}
			
			for (Risk risk : autoSP.getSubPolicyRisks()) {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				if (svr.getVehicleDrivers() != null) {
					Iterator<VehicleDriver> vdItr = svr.getVehicleDrivers().iterator();
					while (vdItr.hasNext()) {
						VehicleDriver vd = vdItr.next();
						if (vd.isInactive()) {
							vd.setDriver(null);
							vd.setSpecialtyVehicleRisk(null);
							vdItr.remove();
						} else {
							vd.setSystemStatus(IAuditable.PENDING);
							vd.setBusinessStatus(IAuditable.NEW);
							vd.setVersionDate(effDate);
						}
					}
				}
				
				if (svr.getVehicleDriverClaims() != null) {
					Iterator<VehicleDriverClaim> vdcItr = svr.getVehicleDriverClaims().iterator();
					while (vdcItr.hasNext()) {
						VehicleDriverClaim vdc = vdcItr.next();
						if (vdc.isInactive()) {
							vdc.setDriver(null);
							vdc.setSpecialtyVehicleRisk(null);
							vdcItr.remove();
						} else {
							vdc.setSystemStatus(IAuditable.PENDING);
							vdc.setBusinessStatus(IAuditable.NEW);
							vdc.setVersionDate(effDate);
						}
					}
				}
			}
		}

		CGLSubPolicy<?> cglSP = (CGLSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
		if (cglSP != null) {
			// Remove deleted location
			if (cglSP.getCglLocations() != null && !cglSP.getCglLocations().isEmpty()) {
				Iterator<CGLLocation> locItr = cglSP.getCglLocations().iterator();
				while (locItr.hasNext()) {
					CGLLocation loc = locItr.next();
					if (loc.isInactive()) {
						locItr.remove();
					} else {
						loc.setSystemStatus(IAuditable.PENDING);
						loc.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
		}
		
		CommercialPropertySubPolicy<?> commercialPropertySubPolicy = (CommercialPropertySubPolicy<?>) inPcyTxn.findSubPolicyByType(LHTConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY);
		if (commercialPropertySubPolicy != null) {
			// Remove deleted location
			if (commercialPropertySubPolicy.getCommercialPropertyLocations() != null && !commercialPropertySubPolicy.getCommercialPropertyLocations().isEmpty()) {
				Iterator<CommercialPropertyLocation> locationItr = commercialPropertySubPolicy.getCommercialPropertyLocations().iterator();
				while (locationItr.hasNext()) {
					CommercialPropertyLocation location = locationItr.next();
					if (location.isInactive()) {
						locationItr.remove();
					} else {
						location.setSystemStatus(IAuditable.PENDING);
						location.setBusinessStatus(IAuditable.NEW);
					}
				}
			}
		}
		
		CargoSubPolicy<?> cargoSP = (CargoSubPolicy<?>) inPcyTxn.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
		if (cargoSP != null && cargoSP.getCargoDetails() != null && !cargoSP.getCargoDetails().isEmpty()) {
			Iterator<CargoDetails> cgdItr = cargoSP.getCargoDetails().iterator();
			while (cgdItr.hasNext()) {
				CargoDetails cgd = cgdItr.next();
				if (cgd.isInactive()) {
					cgdItr.remove();
				} else {
					cgd.setSystemStatus(IAuditable.PENDING);
					cgd.setBusinessStatus(IAuditable.NEW);
				}
			}
		}
		
		if (inPcyTxn.getPolicyRisks() != null && !inPcyTxn.getPolicyRisks().isEmpty()) {
			reNumberLocations(inPcyTxn);
			
			inPcyTxn.getPolicyRisks().stream().filter(o -> RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(o.getRiskType()))
					.map(o -> (PolicyLocation)o)
					.forEach(o -> {
						o.getLocationAddress().setSystemStatus(IAuditable.PENDING);
						o.getLocationAddress().setBusinessStatus(IAuditable.NEW);
					});
		}
		
		return inPcyTxn;
	}

	@Override
	public List<LienholderLessorVehicleDetails> assignLienholderLessorVehicleDetailsSequence(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy,
																LienholderLessor lienLessor,
																List<LienholderLessorVehicleDetails> lienholderLessorVehicleDetailsList) {
		if (autoSubPolicy != null && 
			lienLessor != null &&
			lienholderLessorVehicleDetailsList != null &&
			!lienholderLessorVehicleDetailsList.isEmpty()) {
			Set<LienholderLessorVehicleDetails> set = autoSubPolicy.findLHLVehDetailsByLienholderLessor(lienLessor);
			OptionalInt maxSeq = null;
			if (set != null && !set.isEmpty()) {
				maxSeq = set.stream().filter(o -> o.getVehicleDetailsSequence() != null).collect(Collectors.toList())
									.stream().mapToInt(LienholderLessorVehicleDetails::getVehicleDetailsSequence)
									.max();
			}
			int[] nextSeq = new int[] {(maxSeq != null && maxSeq.isPresent() ? maxSeq.getAsInt()+1 : 1)};
			lienholderLessorVehicleDetailsList.stream().filter(o -> o.getVehicleDetailsSequence() == null).collect(Collectors.toList())
			   								  .stream().forEach(o -> o.setVehicleDetailsSequence(nextSeq[0]++));
		}
		
		return lienholderLessorVehicleDetailsList;
	}

	@Override
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> undeleteAddInsVehicle(
			PolicyTransaction<SpecialtyVehicleRisk> trans,
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy, AdditionalInsuredVehicles aiVehicle)
			throws SpecialtyAutoPolicyException {
		AdditionalInsuredVehicles theAIVeh = specialAutoSubPolicy.findADIVehicleByPK(aiVehicle.getAdditionalInsuredVehiclesPK());
		if (theAIVeh != null) {
			if (StringUtils.equalsIgnoreCase(IAuditable.INACTIVE, theAIVeh.getSystemStatus()) && 
					StringUtils.equalsIgnoreCase(IAuditable.DELETED, theAIVeh.getBusinessStatus())) {
				theAIVeh.setSystemStatus(IAuditable.ACTIVE);
				theAIVeh.setBusinessStatus(IAuditable.NOCHANGE);
			}
		} else {
			throw new SpecialtyAutoPolicyException("Additional insured vehicle not found.");
		}
		return specialAutoSubPolicy;
	}

	// ESL-894 Addition of endorsement automatically
	@Override
	protected List<Endorsement> createDefaultEndorsements(ICoverageConfigParent covConfigParent,
			PolicyTransaction<?> pcyTxn, IBusinessEntity covParent) throws InsurancePolicyException {
		// TODO Auto-generated method stub
		List<Endorsement> newEndorsements = super.createDefaultEndorsements(covConfigParent, pcyTxn, covParent);
		
		if (StringUtils.equalsIgnoreCase(pcyTxn.getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_POLICYCHANGE)) {
			if (covParent != null && covParent instanceof SubPolicy) {
				SubPolicy<Risk> subPolicy = (SubPolicy<Risk>) covParent;
				switch (subPolicy.getSubPolicyTypeCd()) {
				case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
					Endorsement newEndorsement = createNewSubPolicyEndorsementIfNotExists(pcyTxn, subPolicy, SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_AOCC);
					if (newEndorsement != null) {
						newEndorsements.add(newEndorsement);
					}
					break;

				case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
					newEndorsement = createNewSubPolicyEndorsementIfNotExists(pcyTxn, subPolicy, SpecialtyAutoConstants.CGL_SUBPCY_ENDORSEMENT_AOCC);
					if (newEndorsement != null) {
						newEndorsements.add(newEndorsement);
					}
					break;

				default:
					break;
				}
			}
		}
		
		return newEndorsements;
	}

	@Override
	public PolicyTransaction<?> updatePolicyTransaction(PolicyTransaction<?> policyTransaction, UserProfile userProfile)
			throws InsurancePolicyException {
		// TODO Auto-generated method stub
		PolicyTransaction<?> pcyTxn = super.updatePolicyTransaction(policyTransaction, userProfile);
		

		return pcyTxn;
	}

	/* ESL-926 on hold
	@Override
	public PolicyTransaction<?> ratePolicy(Long pcyTxnId, UserProfile userProfile) throws InsurancePolicyException {
		// TODO Auto-generated method stub
		PolicyTransaction<?> pcyTxn = super.ratePolicy(pcyTxnId, userProfile);
		
		//post the event here....
		logger.info("....before posting MODIFY event....");
		EventBusFactory.getEventBus().post(new PolicyModifiedEvent(this, pcyTxn));
		logger.info("....after posting MODIFY event....");
		
		return pcyTxn;
	}
	*/
	
	// ESL-894 Addition of endorsement automatically
	@Override
	public PolicyTransaction<?> postRemoveSubPolicy(PolicyTransaction<?> pcyTxn, SubPolicy<?> subPolicy)
			throws InsurancePolicyException {
		pcyTxn = super.postRemoveSubPolicy(pcyTxn, subPolicy);
		//pcyTxn = loadPolicyTransactionFull(pcyTxn, false);
		if (pcyTxn != null && StringUtils.equalsIgnoreCase(pcyTxn.getPolicyVersion().getPolicyTxnType(), Constants.VERS_TXN_TYPE_POLICYCHANGE)) {
			if ((pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO) != null &&
				 pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO).isDeleted()) 
				||
				(pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY) != null &&
				 pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY).isDeleted())) {
				createPolicyEndorsementIfNotExists(pcyTxn, SpecialtyAutoConstants.PCY_ENDORSEMENT_DOCC);
			}
		}

		removeLocationCoveragesWhenSubPolicyIsDeleted(pcyTxn, subPolicy);
		
		return pcyTxn;
	}

	private void removeLocationCoveragesWhenSubPolicyIsDeleted(PolicyTransaction<?> policyTransaction, SubPolicy<?> subPolicy) {
		List<Risk> risks = (List<Risk>) policyTransaction.findRiskByType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION);
		if (risks != null && !risks.isEmpty()) {
			risks.stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())
				 .stream().forEach(o -> {
					try {
						removeLocationCoverages(policyTransaction, o, subPolicy.getSubPolicyTypeCd());
					} catch (InsurancePolicyException e) {
						throw new RuntimeException(e);
					}
				});
		}
	}

	private void removeLocationCoverages(PolicyTransaction<?> policyTransaction, Risk risk, String subPolicyCode) throws InsurancePolicyException {
		List<String> locCovCodes = new ArrayList<String>();
		List<String> locEndCodes = new ArrayList<String>();
		
		switch (subPolicyCode) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			locEndCodes.addAll(SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS);
			break;

		case SpecialtyAutoConstants.SUBPCY_CODE_GARAGE:
			locCovCodes.addAll(SpecialtyAutoConstants.GARAGE_LOC_COVERAGES);
			break;

		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			locCovCodes.addAll(SpecialtyAutoConstants.CARGO_LOC_COVERAGES);
			locEndCodes.addAll(SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS);
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY:
			locEndCodes.addAll(SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS);
			break;

		default:
			break;
		}
		
		if (risk.getCoverageDetail() != null) {
			for(String code : locCovCodes) {
				Coverage locCov = risk.getCoverageDetail().getActiveOrPendingCoverageByCode(code);
				if (locCov != null) {
					removeRiskCoverage(locCov, policyTransaction, risk);
				}
			}
			
			for(String code : locEndCodes) {
				Endorsement locEnd = risk.getCoverageDetail().getActiveOrPendingEndorsementByCode(code);
				if (locEnd != null) {
					removeRiskEndorsement(locEnd, policyTransaction, risk);
				}
			}
		}
	}

	@Override
	public PolicyTransaction<?> postUndeleteSubPolicy(PolicyTransaction<?> pcyTxn, SubPolicy<?> subPolicy)
			throws InsurancePolicyException {
		// TODO Auto-generated method stub
		pcyTxn = super.postUndeleteSubPolicy(pcyTxn, subPolicy);
		pcyTxn = loadPolicyTransactionFull(pcyTxn, true);
		undeleteLocationCoveragesWhenSubPolicyIsUndeleted(pcyTxn, subPolicy);
		
		return pcyTxn;
	}

	private void undeleteLocationCoveragesWhenSubPolicyIsUndeleted(PolicyTransaction<?> policyTransaction, SubPolicy<?> subPolicy) {
		List<Risk> risks = (List<Risk>) policyTransaction.findRiskByType(SpecialtyAutoConstants.RISK_TYPE_SPEC_LOCATION);
		if (risks != null && !risks.isEmpty()) {
			risks.stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())
				 .stream().forEach(o -> {
					try {
						undeleteLocationCoverages(policyTransaction, o, subPolicy.getSubPolicyTypeCd());
					} catch (InsurancePolicyException e) {
						throw new RuntimeException(e);
					}
				});
		}
	}
	
	private void undeleteLocationCoverages(PolicyTransaction<?> policyTransaction, Risk risk, String subPolicyCode) throws InsurancePolicyException {
		List<String> locCovCodes = new ArrayList<String>();
		List<String> locEndCodes = new ArrayList<String>();
		
		switch (subPolicyCode) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			locEndCodes.addAll(SpecialtyAutoConstants.CGL_LOC_ENDORSEMENTS);
			break;

		case SpecialtyAutoConstants.SUBPCY_CODE_GARAGE:
			locCovCodes.addAll(SpecialtyAutoConstants.GARAGE_LOC_COVERAGES);
			break;

		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			locCovCodes.addAll(SpecialtyAutoConstants.CARGO_LOC_COVERAGES);
			locEndCodes.addAll(SpecialtyAutoConstants.CARGO_LOC_ENDORSEMENTS);
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_COMMERCIAL_PROPERTY:
			locEndCodes.addAll(SpecialtyAutoConstants.COMMERCIAL_PROPERTY_LOC_ENDORSEMENTS);
			break;

		default:
			break;
		}
		
		if (risk.getCoverageDetail() != null) {
			for(String code : locCovCodes) {
				Coverage locCov = risk.getCoverageDetail().getActiveOrPendingCoverageByCode(code);
				if (locCov != null) {
					undeleteRiskCoverage(locCov, policyTransaction, risk);
				}
			}
			
			for(String code : locEndCodes) {
				Endorsement locEnd = risk.getCoverageDetail().getActiveOrPendingEndorsementByCode(code);
				if (locEnd != null) {
					undeleteRiskEndorsement(locEnd, policyTransaction, risk);
				}
			}
		}
	}

	@Override
	public Map<Long, SpecialtyVehicleRiskDTO> getVehicleRiskDTOByPk(List<Long> pkList) throws SpecialtyAutoPolicyException {
		try {
			return new SpecialtyVehicleRiskDAO().getSpecialtyVehicleRiskDTOMap(pkList);
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Could not retrieve specialty vehicle risk DTO's", e);
		}
	}

	@Override
	public SpecialityAutoSubPolicy<SpecialtyVehicleRisk> undeleteLeanholderLessoeVehDetails(
			SpecialityAutoSubPolicy<SpecialtyVehicleRisk> specialAutoSubPolicy, LienholderLessorVehicleDetails lhlVD)
			throws SpecialtyAutoPolicyException {
		LienholderLessorVehicleDetails theLHLVD = specialAutoSubPolicy.findLHLVehDetailsByPk(lhlVD.getLienholderLessorVehicleDetailsPK());
		if (theLHLVD != null) {
			if (theLHLVD.isInactive() && theLHLVD.isDeleted()) {
				theLHLVD.setSystemStatus(IAuditable.ACTIVE);
				theLHLVD.setBusinessStatus(IAuditable.NOCHANGE);
			}
		} else {
			throw new SpecialtyAutoPolicyException("Lien holder lessor vehicle details not found");
		}
		return specialAutoSubPolicy;
	}
	
	private void reNumberLocations(PolicyTransaction<?> pcyTxn) {
		// Renumber the location from A.... excluding MAILING
		int[] cc = new int[] {65}; //A
		pcyTxn.getPolicyRisks().stream().filter(o -> RISK_TYPE_PCY_LOCATION.equalsIgnoreCase(o.getRiskType())).collect(Collectors.toList())
					.stream().map(o -> (PolicyLocation)o).collect(Collectors.toList())
					.stream().filter(o -> !SpecialtyAutoConstants.PCY_LOCATION_ID_DEFAULT.equalsIgnoreCase(o.getLocationId())).collect(Collectors.toList())
					.stream().sorted(Comparator.comparing(PolicyLocation::getLocationId)).collect(Collectors.toList())
					.stream().forEach(o -> {
						o.setLocationId(String.copyValueOf(Character.toChars(cc[0])));
						cc[0]++;
						if (cc[0] > 90) { // 90 is Z
							cc[0] = 65;
						}
					});
	}

	@Override
	public List<String> getRatingWarnings(PolicyTransaction<SpecialtyVehicleRisk> trans) throws SpecialtyAutoPolicyException {
		List<String> messages = new ArrayList<String>();
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSP = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) trans.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSP != null && autoSP.getSubPolicyRisks() != null && !autoSP.getSubPolicyRisks().isEmpty()) {
			Set<Integer> allMessageValues = new HashSet<Integer>();
			for(Risk risk : autoSP.getSubPolicyRisks()) {
				if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
					SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk)risk;
					
					List<Coverage> coverages = new ArrayList<Coverage>();
					if (risk.getRiskCoverages() != null && !risk.getRiskCoverages().isEmpty()) {
						coverages.addAll(risk.getRiskCoverages());
					}
					
					List<Endorsement> endorsements = new ArrayList<Endorsement>();
					if (risk.getRiskEndorsements() != null && !risk.getRiskEndorsements().isEmpty()) {
						endorsements.addAll(risk.getRiskEndorsements());
					}
					
					Set<Integer> messageValues = getRatingWarningMessageValues(vehicle.getVehicleType(), coverages, endorsements);
					allMessageValues.addAll(messageValues);
				}
			}
			
			if (!allMessageValues.isEmpty()) {
				allMessageValues.stream().sorted().forEach(o -> messages.add(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_MESSAGECODE+";"+o));
			}
		}
		
		return messages;
	}
	

	@Override
	public List<String> getRiskRatingWarnings(Risk risk, List<Coverage> coverages, List<Endorsement> endorsements)
			throws SpecialtyAutoPolicyException {
		List<String> messages = new ArrayList<String>();
		if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
			SpecialtyVehicleRisk vehicle = (SpecialtyVehicleRisk)risk;
			Set<Integer> messageValues = getRatingWarningMessageValues(vehicle.getVehicleType(), coverages, endorsements);
			messages = finalizeRatingWarningMessages(messageValues);
		}
		return messages;
	}
	
	private Set<Integer> getRatingWarningMessageValues(String vehicleType, Collection<Coverage> coverages, Collection<Endorsement> endorsements) {
		Set<Integer> allMessageValues = new HashSet<Integer>();
		
		if (coverages != null && !coverages.isEmpty()) {
			Integer[] idx      = new Integer[]{0};
			String[]  covCodes = new String[coverages.size()]; 
			Integer[] amounts  = new Integer[coverages.size()];
			
			coverages.stream().filter(o -> !o.isDeleted() && !o.wasDeleted())
							  .forEach(o -> {
				covCodes[idx[0]] = o.getCoverageCode();
				amounts[idx[0]]  = o.getDeductible1();
				idx[0]           = idx[0] + 1; 
			});
			
			Set<Integer> messageValues = getRatingWarningMessageValues(vehicleType, true, covCodes, amounts);
			allMessageValues.addAll(messageValues);
		}
		
		if (endorsements != null && !endorsements.isEmpty()) {
			Integer[] idx      = new Integer[]{0};
			String[]  covCodes = new String[endorsements.size()]; 
			Integer[] amounts  = new Integer[endorsements.size()];
			
			endorsements.stream().filter(o -> !o.isDeleted() && !o.wasDeleted())
								 .forEach(o -> {
				covCodes[idx[0]] = o.getEndorsementCd();
				amounts[idx[0]]  = o.getDeductible1amount();
				idx[0]           = idx[0] + 1; 
			});
			
			Set<Integer> messageValues = getRatingWarningMessageValues(vehicleType, false, covCodes, amounts);
			allMessageValues.addAll(messageValues);
		}
		
		return allMessageValues;
	}
	
	private Set<Integer> getRatingWarningMessageValues(String vehicleType, boolean isCoverage, String[] covCodes, Integer[] amounts) {
		Set<Integer> messageValues = new HashSet<Integer>();
		
		if (covCodes != null && covCodes.length > 0 && amounts != null && amounts.length > 0) {
			for(int ii=0; ii<covCodes.length; ii++) {
				String covCode = covCodes[ii];
				Integer amount = amounts[ii];
				if (StringUtils.isNotBlank(covCode) && amount != null) {
					for(String warnValueEntry : SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_LIST) {
						String[] warnValues = warnValueEntry.split(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_ENTRY_SEPARAOTR);
						String warnCovEndInd   = warnValues[0];
						String warnCovEndCodes = warnValues[1];
						String warnVehTypes    = warnValues[2];
						String warnDeductubles = warnValues[3];
						String warnMsgValueStr = warnValues[4];
						
						List<String> warnCovEndCodeList = new ArrayList<String>();
						if (StringUtils.isNotBlank(warnCovEndCodes)) {
							warnCovEndCodeList = Arrays.stream(warnCovEndCodes.split(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_SUBENTRY_SEPARAOTR)).collect(Collectors.toList());
						}
						
						List<String> warnVehTypeList = new ArrayList<String>();
						if (StringUtils.isNotBlank(warnVehTypes)) {
							warnVehTypeList = Arrays.stream(warnVehTypes.split(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_SUBENTRY_SEPARAOTR)).collect(Collectors.toList());
						}
						
						List<Integer> warnDeductubleList = new ArrayList<Integer>();
						if (StringUtils.isNotBlank(warnDeductubles)) {
							for(String ded : warnDeductubles.split(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_SUBENTRY_SEPARAOTR)) {
								Integer dedValue = 0;
								try {
									dedValue = Integer.valueOf(ded);
								} catch (Exception e) {}
								
								warnDeductubleList.add(dedValue);
							}
						}
						
						Integer warnMsgValue = null;
						try {
							warnMsgValue = Integer.valueOf(warnMsgValueStr);
						} catch (Exception e) {
						}
						
						boolean recordFound = false; 
						
						// All Vehicle types when warnVehTypes is empty
						if (warnVehTypeList.isEmpty() ||
							(vehicleType != null && warnVehTypeList.contains(vehicleType))) {
							
							final List<String> warnCovEndCodeLs = warnCovEndCodeList;
							switch (warnCovEndInd) {
							case "C":
								if (isCoverage) {
									if (warnCovEndCodeLs.contains(covCode) && warnDeductubleList.contains(amount)) {
										recordFound = true;
									}
								}
								break;
								
							case "E":
								if (!isCoverage) {
									if (warnCovEndCodeLs.contains(covCode) && warnDeductubleList.contains(amount)) {
										recordFound = true;
									}
								}
								break;
			
							default:
								break;
							}
						}
						
						if (recordFound) {							
							//messages.add(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_MESSAGECODE+";"+warnMsgValues);
							if (warnMsgValue != null) {
								messageValues.add(warnMsgValue);
							}
						}
					}
				}
			}
		}
		
		return messageValues;
	}
	
	private List<String> finalizeRatingWarningMessages(Set<Integer> messageValues) {
		List<String> messages = new ArrayList<String>();
		
		if (messageValues != null && !messageValues.isEmpty()) {
			if (messageValues != null && !messageValues.isEmpty()) {
				messageValues.stream().sorted().forEach(o -> messages.add(SpecialtyAutoConstants.WARN_COVEND_PREMBASEDONDED_MESSAGECODE+";"+o));
			}
		}
		
		return messages;
	}

	@Override
	protected CancellationPremCalcFactory getCancellationPremCalcFactory() {
		return TowingCancellationPremCalcFactory.getInstance();
	}

	@Override
	protected DSPolicyDocumentsService getDocumentService() {
		return new PolicyDocumentsService();
	}

	@Override
	public SpecialtyAutoPackage createNewInsurancePolicy(Customer customer, String insuranceProduct,
			String insuranceProgram, LocalDate effDate, Address insuredAddress, UserProfile userProfile)
			throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewInsurancePolicy(customer, insuranceProduct, insuranceProgram, effDate, insuredAddress,
																	userProfile);
		return rv;
	}

	@Override
	public SpecialtyAutoPackage createNewInsurancePolicy(Customer customer, String insuranceProduct,
			String insuranceProgram, LocalDate effDate, LocalDate expDate, Integer termMonths, Address insuredAddress,
			UserProfile userProfile) throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewInsurancePolicy(customer, insuranceProduct, insuranceProgram, effDate, expDate, termMonths,
				insuredAddress, userProfile);
		
		PolicyTransaction<?> policyTransaction = rv.getPolicyVersions().get(0).getPolicyTransactions().iterator().next();
		policyTransaction = createCustomerMainAddress(policyTransaction);
		
		(new InsurancePolicyDAO()).merge(rv);
		
		return rv;
	}

	@Override
	public SpecialtyAutoPackage createNewCustomerAndInsurancePolicy(String customerType, String insuranceProduct,
			String insuranceProgram, LocalDate effDate, UserProfile userProfile) throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewCustomerAndInsurancePolicy(customerType, insuranceProduct, insuranceProgram, effDate,
																			userProfile);
		return rv;
	}

	@Override
	public SpecialtyAutoPackage createNewCustomerAndInsurancePolicy(String customerType, String insuranceProduct,
			String insuranceProgram, LocalDate effDate, LocalDate expDate, Integer termMonths, UserProfile userProfile)
			throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewCustomerAndInsurancePolicy(customerType, insuranceProduct, insuranceProgram, effDate, expDate,
				termMonths, userProfile);
		
		PolicyTransaction<?> policyTransaction = rv.getPolicyVersions().get(0).getPolicyTransactions().iterator().next();
		policyTransaction = createCustomerMainAddress(policyTransaction);
		
		(new InsurancePolicyDAO()).merge(rv);
		
		return rv;
	}

	@Override
	public PolicyLocation copyPolicyLocation(PolicyLocation policyLocation,
			PolicyTransaction<PolicyLocation> policyTransaction) throws SpecialtyAutoPolicyException {
		
		try {
			policyLocation = (PolicyLocation) loadPolicyRisk(policyLocation, true);
		} catch (InsurancePolicyException e1) {
			throw new SpecialtyAutoPolicyException("Could not copy policy location", e1);
		}

		PolicyLocation rv = null;
		
		try {
			rv = (PolicyLocation) policyLocation.clone();
			rv.setOriginatingPK(null);
			rv.setLocationId(null);
			rv.setCustomerMainAddressPK(null);
			rv.getCoverageDetail().getCoverages().clear();
			rv.getCoverageDetail().getEndorsements().clear();
			if (rv.getRateFactorsLink() != null) {
				rv.setRateFactorsLink(new RateFactorsLink());
			}
			policyTransaction.addPolicyRisk(rv);
		} catch (CloneNotSupportedException e) {
			throw new SpecialtyAutoPolicyException("Could not copy policy location", e);
		}
		
		return rv;
	}

	@Override
	public SpecialtyAutoPackage createNewCustomerAndManualPolicyRenewal(String customerType, String insuranceProduct,
			String insuranceProgram, String quoteNumber, String policyNumber, Integer termNumber, LocalDate effDate,
			LocalDate expDate, Integer termMonths, UserProfile userProfile) throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewCustomerAndManualPolicyRenewal(customerType, insuranceProduct, insuranceProgram, quoteNumber,
				policyNumber, termNumber, effDate, expDate, termMonths, userProfile);
		PolicyTransaction<?> pcyTxn = rv.getPolicyVersions().stream().findFirst().get().getPolicyTransactions().stream().findFirst().get();
		pcyTxn = createCustomerMainAddress(pcyTxn);
		
		(new InsurancePolicyDAO()).merge(rv);
		return rv;
	}

	@Override
	public SpecialtyAutoPackage createNewManualPolicyRenewal(Customer customer, String insuranceProduct,
			String insuranceProgram, String quoteNumber, String policyNumber, Integer termNumber, LocalDate effDate,
			LocalDate expDate, Integer termMonths, Address insuredAddress, UserProfile userProfile)
			throws InsurancePolicyException {
		SpecialtyAutoPackage rv = super.createNewManualPolicyRenewal(customer, insuranceProduct, insuranceProgram, quoteNumber, policyNumber,
				termNumber, effDate, expDate, termMonths, insuredAddress, userProfile);
		PolicyTransaction<?> pcyTxn = rv.getPolicyVersions().stream().findFirst().get().getPolicyTransactions().stream().findFirst().get();
		pcyTxn = createCustomerMainAddress(pcyTxn);
		
		(new InsurancePolicyDAO()).merge(rv);
		return rv;
	}

	@Override
	public List<VehicleRateGroupLHT> findVehicleRateGroupLHT(Double lowLimit, Double uppLimit)
			throws SpecialtyAutoPolicyException {
		try {
			return (new VehicleRateGroupLHTDAO()).findBy_lowLimit_uppLimit(lowLimit, uppLimit);
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException("Search failed", e);
		}
	}

	@Override
	public List<String> getSubpolicyRatingWarnings(SubPolicy<?> subPolicy, List<Coverage> coverages,
			List<Endorsement> endorsements) throws SpecialtyAutoPolicyException {
		// TODO Auto-generated method stub
		return new ArrayList<String>();
	}
	
	protected PolicyReinsurer getPrimaryPolicyReinsurer(PolicyTransaction<?> pcyTxn) {
		// TODO Auto-generated method stub
		PolicyReinsurer policyReinsurer = null;
		
		if (pcyTxn.getPolicyReinsurers() != null && !pcyTxn.getPolicyReinsurers().isEmpty()) {
			policyReinsurer = pcyTxn.getPolicyReinsurers()
										.stream().filter(o -> o.getReinsurer().getLegalName().toUpperCase().contains(SpecialtyAutoConstants.REINSURER_ECHELON.toUpperCase()))
										.findFirst().orElse(null);
		}
		
		return policyReinsurer;
	}

	@Override
	protected void preDeleteDriver(Driver driver) throws InsurancePolicyException {
		super.preDeleteDriver(driver);
		// get all vehicle driver records for the current driver and "delete" them
		VehicleDriverDAO vdDAO = new VehicleDriverDAO();
		try {
			List<VehicleDriver> vdList = vdDAO.getVehicleDriversByDriver(driver);
			if (vdList != null && !vdList.isEmpty()) {
				for (VehicleDriver vd : vdList) {
					if (vd.isActive() && !vd.isDeleted()) {
						vd.setSystemStatus(IAuditable.INACTIVE);
						vd.setBusinessStatus(IAuditable.DELETED);
						vdDAO.merge(vd);
					} else if (vd.isPending()) {
						vdDAO.delete(vd);
					}
				}
			}
		} catch (Exception e) {
			throw new InsurancePolicyException("Pre delete driver failed - issue wtih vehicle drivers", e);
		}
		// get all vehicle driver claim records for the current driver and "delete" them
		VehicleDriverClaimDAO vdcDAO = new VehicleDriverClaimDAO();
		try {
			List<VehicleDriverClaim> vdcList = vdcDAO.getVehicleDriverClaimsByDriver(driver);
			if (vdcList != null && !vdcList.isEmpty()) {
				for (VehicleDriverClaim vdc : vdcList) {
					if (vdc.isActive() && !vdc.isDeleted()) {
						vdc.setSystemStatus(IAuditable.INACTIVE);
						vdc.setBusinessStatus(IAuditable.DELETED);
						vdcDAO.merge(vdc);
					} else {
						vdcDAO.delete(vdc);
					}
				}
			}
		} catch (Exception e) {
			throw new InsurancePolicyException("Pre delete driver failed - issue eith v ehicle driver claims", e);
		}
	}

	@Override
	protected PolicyTransaction<?> createPolicyPrimaryReinsurance(PolicyTransaction<?> pcyTxn,
			PolicyReinsurer policyReinsurer, Long defaultLimit, IGenericProductConfig productConfig) throws InsurancePolicyException {
		// TODO Auto-generated method stub		
		return super.createPolicyPrimaryReinsurance(pcyTxn, getPrimaryPolicyReinsurer(pcyTxn), defaultLimit, productConfig);
	}
	
	// Added by Marwan Apr. 12, 2022 --> ESL-1712
	@Override
	public PolicyTransaction<?> postIssuePolicyChange(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		pcyTxn = super.postIssuePolicyChange(pcyTxn);
		if (pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCustomerType().equalsIgnoreCase(Constants.CUST_TYPE_COMM)) {
			pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().setDbaName(
				pcyTxn.getPolicyVersion().getInsurancePolicy().getPolicyCustomer().getCommercialCustomer().getLegalName()	
			);
		}
		return pcyTxn;
	}
	//end of additions

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> initializeRollPolicy(PolicyTransaction<?> pcyTxn, SubPolicy<?> subPcy, Risk risk,
			List<ImmutablePair<SubPolicy<?>, Risk>> subPolicyAndRiskPairs, UserProfile userProfile)
			throws InsurancePolicyException {
		pcyTxn = super.initializeRollPolicy(pcyTxn, subPcy, risk, subPolicyAndRiskPairs, userProfile);
		
		if (subPcy == null && risk == null) {
			// the caller manages to load the data
		}
		else {
			// For saving vehicle; rollup for current vehicle only
			if (subPcy != null && risk != null && SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(risk.getRiskType())) {
				subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(subPcy, risk));
				
				SubPolicy<?> cglSubPolicy = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
				if (cglSubPolicy != null && (cglSubPolicy.isPending() || cglSubPolicy.isActive())) {
					subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(cglSubPolicy, null));
				}
				
				SubPolicy<?> cargoSubPolicy = pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
				if (cargoSubPolicy != null && (cargoSubPolicy.isPending() || cargoSubPolicy.isActive())) {
					subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(cargoSubPolicy, null));				
				}
			}
			// For saving others; all excepts vehicles
			else {
				pcyTxn.getPolicyRisks().forEach(o -> subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(null, o)));
				
				for(Risk polRisk : pcyTxn.getPolicyRisks()) {
					if (risk != null && risk.equals(polRisk)) {
						polRisk = risk;
					}
					
					subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(null, polRisk));
				}
	
				
				for(SubPolicy<?> _sp : pcyTxn.getSubPolicies()) {
					SubPolicy<Risk> sp = (SubPolicy<Risk>) _sp;
					
					if (subPcy != null && subPcy.equals(sp)) {
						sp = (SubPolicy<Risk>) subPcy;
					}
					
					subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(sp, null));
					
					if (!SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(sp.getSubPolicyTypeCd())) {
						for(Risk spRisk : sp.getSubPolicyRisks()) {
							if (risk != null && risk.equals(spRisk)) {
								spRisk = risk;
							}
							
							subPolicyAndRiskPairs.add(new ImmutablePair<SubPolicy<?>, Risk>(sp, spRisk));
						}
					}
				}
			}
		}
		
		return pcyTxn;
	}

	// copied from LHTPolicyProcess
	protected PolicyTransaction<?> updateCoverageEndorsementReinsured(PolicyTransaction<?> policyTransaction) throws SpecialtyAutoPolicyException {
		try {
			IGenericProductConfig productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
					policyTransaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
					policyTransaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
					policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate());
			if (productConfig != null) {
				if (ObjectUtils.compare(((SpecialtyAutoPackage)policyTransaction.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit(), 
						Integer.valueOf(getPolicyMaxRetainedLimit(policyTransaction).intValue())) > 0) {

					policyTransaction = loadPolicyTransactionFull(policyTransaction, false);

					//policy coverages
					policyTransaction.getCoverageDetail().getCoverages().stream().filter(pc -> pc.isActive() || pc.isPending()).forEach(pc -> {
						CoverageConfig covConfig = productConfig.getCoverageConfig(pc.getCoverageCode());
						if (covConfig != null && covConfig.isReinsurable()) {
							pc.setReinsured(Boolean.TRUE);
						}
					});

					//policy endorsements
					policyTransaction.getCoverageDetail().getEndorsements().stream().filter(end -> end.isActive() || end.isPending()).forEach(end -> {
						EndorsementConfig endConfig = productConfig.getEndorsementConfig(end.getEndorsementCd());
						if (endConfig != null && endConfig.isReinsurable()) {
							end.setReinsured(Boolean.TRUE);
						}
					});
					
					policyTransaction.getPolicyRisks().stream().filter(r -> r.isActive() || r.isPending()).forEach(r -> {
						RiskConfig riskConfig = productConfig.getRiskType(r.getRiskType());
						if (riskConfig != null) {
							//policy risk coverages
							r.getCoverageDetail().getCoverages().stream().filter(rc -> rc.isActive() || rc.isPending()).forEach(rc -> {
								CoverageConfig covConfig = riskConfig.getCoverageConfig(rc.getCoverageCode());
								if (covConfig != null && covConfig.isReinsurable()) {
									rc.setReinsured(Boolean.TRUE);
								}								
							});

							//policy risk endorsements
							r.getCoverageDetail().getEndorsements().stream().filter(re -> re.isActive() || re.isPending()).forEach(re -> {
								EndorsementConfig endConfig = riskConfig.getEndorsementConfig(re.getEndorsementCd());
								if (endConfig != null && endConfig.isReinsurable()) {
									re.setReinsured(Boolean.TRUE);
								}
							});
						}
					});

					policyTransaction.getSubPolicies().stream().filter(sp -> sp.isActive() || sp.isPending()).forEach(sp -> {
						SubPolicyConfig spConfig = productConfig.getSubPolicyConfig(sp.getSubPolicyTypeCd());
						if (spConfig != null) {
							//subpolicy coverages
							sp.getCoverageDetail().getCoverages().stream().filter(spc -> spc.isActive() || spc.isPending()).forEach(spc -> {
								CoverageConfig covConfig = spConfig.getCoverageConfig(spc.getCoverageCode());
								if (covConfig != null && covConfig.isReinsurable()) {
									spc.setReinsured(Boolean.TRUE);
								}																
							});

							//subpolicy endorsements
							sp.getCoverageDetail().getEndorsements().stream().filter(spe -> spe.isActive() || spe.isPending()).forEach(spe -> {
								EndorsementConfig endConfig = spConfig.getEndorsementConfig(spe.getEndorsementCd());
								if (endConfig != null && endConfig.isReinsurable()) {
									spe.setReinsured(Boolean.TRUE);
								}
							});

							sp.getSubPolicyRisks().stream().filter(spr -> spr.isActive() || spr.isPending()).forEach(spr -> {
								RiskConfig spRiskConfig = spConfig.getRiskType(spr.getRiskType());
								if (spRiskConfig != null) {
									//subpolicy risk coverages
									spr.getCoverageDetail().getCoverages().stream().filter(sprc -> spr.isActive() || sprc.isPending()).forEach(sprc -> {
										CoverageConfig covConfig = spRiskConfig.getCoverageConfig(sprc.getCoverageCode());
										if (covConfig != null && covConfig.isReinsurable()) {
											sprc.setReinsured(Boolean.TRUE);
										}																										
									});

									//subpolicy risk endorsements
									spr.getCoverageDetail().getEndorsements().stream().filter(spre -> spre.isActive() || spre.isPending()).forEach(spre -> {
										EndorsementConfig endConfig = spRiskConfig.getEndorsementConfig(spre.getEndorsementCd());
										if (endConfig != null && endConfig.isReinsurable()) {
											spre.setReinsured(Boolean.TRUE);
										}
									});
								}
							});
						}
					});
				} else {
					// Limit is at or below maxRetainedLimit; clear isReinsured flags.
					policyTransaction = loadPolicyTransactionFull(policyTransaction, false);

					//policy coverages
					policyTransaction.getCoverageDetail().getCoverages().stream().filter(pc -> pc.isActive() || pc.isPending()).forEach(pc -> {
						CoverageConfig covConfig = productConfig.getCoverageConfig(pc.getCoverageCode());
						if (covConfig != null && covConfig.isReinsurable()) {
							pc.setReinsured(Boolean.FALSE);
						}
					});

					//policy endorsements
					policyTransaction.getCoverageDetail().getEndorsements().stream().filter(end -> end.isActive() || end.isPending()).forEach(end -> {
						EndorsementConfig endConfig = productConfig.getEndorsementConfig(end.getEndorsementCd());
						if (endConfig != null && endConfig.isReinsurable()) {
							end.setReinsured(Boolean.FALSE);
						}
					});

					policyTransaction.getPolicyRisks().stream().filter(r -> r.isActive() || r.isPending()).forEach(r -> {
						RiskConfig riskConfig = productConfig.getRiskType(r.getRiskType());
						if (riskConfig != null) {
							//policy risk coverages
							r.getCoverageDetail().getCoverages().stream().filter(rc -> rc.isActive() || rc.isPending()).forEach(rc -> {
								CoverageConfig covConfig = riskConfig.getCoverageConfig(rc.getCoverageCode());
								if (covConfig != null && covConfig.isReinsurable()) {
									rc.setReinsured(Boolean.FALSE);
								}
							});

							//policy risk endorsements
							r.getCoverageDetail().getEndorsements().stream().filter(re -> re.isActive() || re.isPending()).forEach(re -> {
								EndorsementConfig endConfig = riskConfig.getEndorsementConfig(re.getEndorsementCd());
								if (endConfig != null && endConfig.isReinsurable()) {
									re.setReinsured(Boolean.FALSE);
								}
							});
						}
					});

					policyTransaction.getSubPolicies().stream().filter(sp -> sp.isActive() || sp.isPending()).forEach(sp -> {
						SubPolicyConfig spConfig = productConfig.getSubPolicyConfig(sp.getSubPolicyTypeCd());
						if (spConfig != null) {
							//subpolicy coverages
							sp.getCoverageDetail().getCoverages().stream().filter(spc -> spc.isActive() || spc.isPending()).forEach(spc -> {
								CoverageConfig covConfig = spConfig.getCoverageConfig(spc.getCoverageCode());
								if (covConfig != null && covConfig.isReinsurable()) {
									spc.setReinsured(Boolean.FALSE);
								}
							});

							//subpolicy endorsements
							sp.getCoverageDetail().getEndorsements().stream().filter(spe -> spe.isActive() || spe.isPending()).forEach(spe -> {
								EndorsementConfig endConfig = spConfig.getEndorsementConfig(spe.getEndorsementCd());
								if (endConfig != null && endConfig.isReinsurable()) {
									spe.setReinsured(Boolean.FALSE);
								}
							});

							sp.getSubPolicyRisks().stream().filter(spr -> spr.isActive() || spr.isPending()).forEach(spr -> {
								RiskConfig spRiskConfig = spConfig.getRiskType(spr.getRiskType());
								if (spRiskConfig != null) {
									//subpolicy risk coverages
									spr.getCoverageDetail().getCoverages().stream().filter(sprc -> spr.isActive() || sprc.isPending()).forEach(sprc -> {
										CoverageConfig covConfig = spRiskConfig.getCoverageConfig(sprc.getCoverageCode());
										if (covConfig != null && covConfig.isReinsurable()) {
											sprc.setReinsured(Boolean.FALSE);
										}
									});

									//subpolicy risk endorsements
									spr.getCoverageDetail().getEndorsements().stream().filter(spre -> spre.isActive() || spre.isPending()).forEach(spre -> {
										EndorsementConfig endConfig = spRiskConfig.getEndorsementConfig(spre.getEndorsementCd());
										if (endConfig != null && endConfig.isReinsurable()) {
											spre.setReinsured(Boolean.FALSE);
										}
									});
								}
							});
						}
					});
				}
			}
		} catch (Exception e) {
			logger.log(Level.WARNING, "Exception in updateCoverageEndorsementReinsured", e);
			throw new SpecialtyAutoPolicyException("Exception in updateCoverageEndorsementReinsured", e);
		}

		return policyTransaction;
	}

	@Override
	public SpecialtyVehicleRiskAdjustment createSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRisk parentVehicle, PolicyTransaction<?> policyTransaction)
			throws SpecialtyAutoPolicyException {
		final SpecialtyVehicleRiskAdjustment adjustment = new SpecialtyVehicleRiskAdjustment();	
		//causing issue when attaching new adjustment to vehicle too earlier
		//cancel new adjustment - the new adjustment will exist
		//parentVehicle.addSpecialtyVehicleRiskAdjustment(adjustment);
		adjustment.setSpecialtyVehicleRisk(parentVehicle);
		adjustment.setGroupID(parentVehicle.getPolicyRiskPK().toString());
		
		for (Coverage rc : parentVehicle.getRiskCoverages().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())) {
			try {
				Coverage rcClone = createNewAdjustmentCoveragePremium(rc);
				adjustment.addCoverage(rcClone);
			} catch (CloneNotSupportedException e) {
				throw new SpecialtyAutoPolicyException("Could not clone coverage for adjustment", e);
			}
		}
		for (Endorsement re : parentVehicle.getRiskEndorsements().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList())) {
			try {
				Endorsement reClone = createNewAdjustmentEndorsementPremium(re);
				adjustment.addEndorsement(reClone);
			} catch (CloneNotSupportedException e) {
				throw new SpecialtyAutoPolicyException("Could not clone endorsement for adjustment", e);
			}
		}
					
		return adjustment;
	}
	
	private Coverage createNewAdjustmentCoveragePremium(Coverage coverage) throws CloneNotSupportedException {
		Coverage rcClone = (Coverage)coverage.clone();
		rcClone.setSystemStatus(Constants.SYSTEN_STATUS_PENDING);
		rcClone.setBusinessStatus(Constants.BUSINESS_STATUS_NEW);

		if (BooleanUtils.isTrue(rcClone.isReinsured())) {
			for(CoverageReinsurance rcrClone : rcClone.getCoverageReinsurances()) {
				rcrClone.setSystemStatus(Constants.SYSTEN_STATUS_PENDING);
				rcrClone.setBusinessStatus(Constants.BUSINESS_STATUS_NEW);
			}
		}

		rcClone = updateNewAdjustmentCoveragePremium(rcClone);

		return rcClone;
	}
	
	private Coverage updateNewAdjustmentCoveragePremium(Coverage coverage) {
		Premium premium = coverage.getCoveragePremium();
		premium = updateNewAdjustmentPremium(premium);
		
		if (BooleanUtils.isTrue(coverage.isReinsured())) {
			for(CoverageReinsurance covRis : coverage.getCoverageReinsurances()) {
				updateNewAdjustmentPremium(covRis.getCovReinsurancePremium());
			}
		}
		
		return coverage;
	}
	
	private Endorsement createNewAdjustmentEndorsementPremium(Endorsement endorsement) throws CloneNotSupportedException {
		Endorsement reClone = (Endorsement)endorsement.clone();
		reClone.setSystemStatus(Constants.SYSTEN_STATUS_PENDING);
		reClone.setBusinessStatus(Constants.BUSINESS_STATUS_NEW);
		
		if (BooleanUtils.isTrue(reClone.isReinsured())) {
			for(EndorsementReinsurance rerClone : reClone.getEndorsementReinsurances()) {
				rerClone.setSystemStatus(Constants.SYSTEN_STATUS_PENDING);
				rerClone.setBusinessStatus(Constants.BUSINESS_STATUS_NEW);
			}
		}
		
		reClone = updateNewAdjustmentEndorsementPremium(reClone);
		
		return reClone;
	}
	
	private Endorsement updateNewAdjustmentEndorsementPremium(Endorsement endorsement) {
		Premium premium = endorsement.getEndorsementPremium();
		premium = updateNewAdjustmentPremium(premium);
		
		if (BooleanUtils.isTrue(endorsement.isReinsured())) {
			for(EndorsementReinsurance endRis : endorsement.getEndorsementReinsurances()) {
				updateNewAdjustmentPremium(endRis.getEndReinsurancePremium());
			}
		}
		
		return endorsement;
	}
	
	private Premium updateNewAdjustmentPremium(Premium premium) {
		premium.setPriorNetPremiumChange(0D);
		premium.setPriorWrittenPremium(premium.getWrittenPremium());
		return premium;
	}

	@Override
	public SpecialtyVehicleRiskAdjustment updateSpecialtyVehicleRiskAdjustment(
			SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : StringUtils.EMPTY);

		try {
			//if (getCoverageService().needToLoadCoverageDetails(policyTransaction.getCoverageDetail())) { 
				policyTransaction = loadPolicyTransactionFull(policyTransaction, false);
			//}
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}
		
		final SpecialtyVehicleRisk vehicle = adjustment.getSpecialtyVehicleRisk();
		final SpecialtyPolicyItemDTO vehicleItemDTO = getOriginalSvrValues(vehicle);
		final Integer adjustmentNum = adjustment.getAdjustmentNum();
		final String adjustmentGroupdID = adjustment.getGroupID();

		//VEHICLE - populate prorata factor, premiums, and save
		updatePendingVehicleAdjustment(adjustment, policyTransaction, vehicleItemDTO);
		
		finializeSpecialtyAdjustmentSteps(adjustment, adjustmentNum, adjustmentGroupdID, 
											adjustment.findNextAdjustment(), vehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		/* moved to finializeSpecialtyAdjustmentSteps
		//VEHICLE - Update Subsequence Vehicle Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceVehicleAdjustments(adjustment.findNextAdjustment(), policyTransaction, vehicleItemDTO);
		
		//VEHICLE - Update vehicle from adjustment
		consolidatePendingVehicleAdjustment(adjustment.getAdjustmentNum(), adjustment.getSpecialtyVehicleRisk(), policyTransaction, vehicleItemDTO, userProfile);
		
		//VEHICLE - end vehicle adjustment
		updatePendingAdjustmentEnd(vehicle, policyTransaction, vehicleItemDTO, userProfile);

		//SUBPOLICY - create & update supolicy adjustments
		updatePendingSpecialtySubpolicyAdjustments(adjustment, policyTransaction, vehicleItemDTO);
		
		//VEHICLE - Update Subsequence Subpolicy Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceSubpolicyAdjustments(adjustment.findNextAdjustment(), policyTransaction, vehicleItemDTO);
		
		//SUBPOLICY - Update subpolicies from adjustment
		consolidatePendingSubpolicyAdjustmentPremiums(adjustment.getAdjustmentNum(), policyTransaction, userProfile);
		
		//update rated premiums
		updateSpecialtyAdjustmentRatedPremium(adjustment, vehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		//end adjustment processes 
		updateSpecialtyAdjustmentEnd(vehicle, policyTransaction, vehicleItemDTO, userProfile);
		*/

		LoggedUser.logOut();
		return adjustment;
	}
	
	// The purpose is to use the standard logic to recalculate CGL and CARGO coverage units
	private void updatePendingAdjustmentEnd(SpecialtyVehicleRisk vehicle, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		//refresh premiums from adjustment changes
		try {
			rollupPolicy(policyTransaction, false, userProfile);
			
			//Keep the original unit rate - moved to rollupPolicy
			restoreVehicleUnitRate(vehicle, vehicleItemDTO);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Failed to save adjustment changes", e);
		}
	}
	
	// The purpose is to rollup all changes and save the changes
	private void updateSpecialtyAdjustmentEnd(SpecialtyVehicleRisk vehicle, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		//refresh premiums from adjustment changes
		try {
			rollupPolicy(policyTransaction, false, userProfile);
			
			//Keep the original unit rate - moved to rollupPolicy
			restoreVehicleUnitRate(vehicle, vehicleItemDTO);
			
			updatePolicyTransaction(policyTransaction, userProfile);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Failed to save adjustment changes", e);
		}
	}
	
	private void copyAdjustmentFields(SpecialtyVehicleRiskAdjustment source, SpecialtyVehicleRiskAdjustment destination) {
		destination.setAdjustmentDate(source.getAdjustmentDate());
		destination.setAdjustmentType(source.getAdjustmentType());
		destination.setProRataFactor(source.getProRataFactor());
		destination.setVehicleDescription(source.getVehicleDescription());
		destination.setNumOfUnits(source.getNumOfUnits());
	}
	
	private void updatePendingVehicleAdjustment(SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		// Calculate and set prorata factor and premium change
		SpecialtyVehicleRisk parentVehicle = adjustment.getSpecialtyVehicleRisk();
		if (adjustment.getAdjustmentNum() == null) {
			parentVehicle.addSpecialtyVehicleRiskAdjustment(adjustment);
		}
		
		final SubPolicy<Risk> subPolicy = parentVehicle.getSubPolicy();
		final PolicyTerm policyTerm = subPolicy.getPolicyTransaction().getPolicyTerm();
		final LocalDate adjustmentDate = adjustment.getAdjustmentDate();
		final double prorataFactor = calculateProrataFactorForAdjustments(policyTerm, adjustmentDate.atStartOfDay(),
				subPolicy);
		final double premiumChange = calculatePremiumChangeForAdjustment(adjustment, parentVehicle, prorataFactor, vehicleItemDTO);

		adjustment.setProRataFactor(prorataFactor);
		// final NPC will be updated in updatePendingAdjustmentPremiums
		adjustment.setPremiumChange(premiumChange);
		
		try {
			if (getCoverageService().needToLoadCoverageDetails(parentVehicle.getCoverageDetail())) {
				parentVehicle = (SpecialtyVehicleRisk) loadSubPolicyRisk(parentVehicle , false);
			}
		} catch (InsurancePolicyException e) {
		}
		
		try {
			if (getCoverageService().needToLoadCoverageDetails(adjustment.getCoverageDetail())) {
				adjustment = loadSpecialtyVehicleRiskAdjustment(adjustment);
			}
		} catch (SpecialtyAutoPolicyException e) {
		}
	
		//Update adjustment coverages and endorsements
		//Rollup to adjustment
		updatePendingAdjustmentPremiums(adjustment, policyTransaction, vehicleItemDTO);
	}

	private void updatePendingAdjustmentPremiums(SpecialtyVehicleRiskAdjustment adjustment, 
													PolicyTransaction<?> policyTransaction, 
													SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		Integer prevAdjUnits 	 = getPrevAdjustmentNumOfUnits(adjustment, vehicleItemDTO);
		int initialVehicleCount  = (prevAdjUnits != null ? prevAdjUnits : 0); 
		int adjustedVehicleCount = adjustment.getNumOfUnits();
		double prorataFactor     = adjustment.getProRataFactor();
		LocalDate termEffDate    = policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate();
		LocalDate termExpDate	 = policyTransaction.getPolicyTerm().getTermExpDate().toLocalDate();
		LocalDate changeEffDate	 = adjustment.getAdjustmentDate();
		
		if (adjustment.getCoverageDetail().getCoverages() != null) {
			for(Coverage cov : adjustment.getCoverageDetail().getCoverages()) {
				cov.setCoverageEffDate(adjustment.getAdjustmentDate());
				Premium premium = cov.getCoveragePremium();
				double unitRate = premium.getUnitPremium() != null ? premium.getUnitPremium() : 0;
				updatePendingAdjustmentPremium(premium, cov.isFlatPremium(), premium.isPremiumOverride(),
												initialVehicleCount, 
												adjustedVehicleCount, prorataFactor, unitRate,
												termEffDate, termExpDate,
												changeEffDate);
				
				if (BooleanUtils.isTrue(cov.isReinsured())) {
					for(CoverageReinsurance covRis : cov.getCoverageReinsurances()) {
						premium = covRis.getCovReinsurancePremium();
						unitRate = premium.getUnitPremium() != null ? premium.getUnitPremium() : 0;
						updatePendingAdjustmentPremium(premium, covRis.getIsFlatPremium(), premium.isPremiumOverride(),
														initialVehicleCount, 
														adjustedVehicleCount, prorataFactor, unitRate,
														termEffDate, termExpDate,
														changeEffDate);
					}
				}
			}
		}
		
		if (adjustment.getCoverageDetail().getEndorsements() != null) {
			for(Endorsement end : adjustment.getCoverageDetail().getEndorsements()) {
				end.setEndorsementEffective(adjustment.getAdjustmentDate());
				Premium premium = end.getEndorsementPremium();
				double unitRate = premium.getUnitPremium() != null ? premium.getUnitPremium() : 0;
				updatePendingAdjustmentPremium(premium, end.isFlatPremium(), premium.isPremiumOverride(),
												initialVehicleCount, 
												adjustedVehicleCount, prorataFactor, unitRate, 
												termEffDate, termExpDate,
												changeEffDate);
				
				if (BooleanUtils.isTrue(end.isReinsured())) {
					for(EndorsementReinsurance endRis : end.getEndorsementReinsurances()) {
						premium = endRis.getEndReinsurancePremium();
						unitRate = premium.getUnitPremium() != null ? premium.getUnitPremium() : 0;
						updatePendingAdjustmentPremium(premium, endRis.getIsFlatPremium(), premium.isPremiumOverride(),
														initialVehicleCount, 
														adjustedVehicleCount, prorataFactor, unitRate, 
														termEffDate, termExpDate,
														changeEffDate);
					}
				}				
			}
		}
		
		PremiumRollup premiumRollup = (PremiumRollup) getPolicyServicesFactory().newPremiumRollup();
		premiumRollup.totalAdjustmentPremium(adjustment);
		
		adjustment.setPremiumChange(adjustment.getCoverageDetail().getCoveragePremium().getNetPremiumChange());
	}

	// Following Policy change premium
	// Premium Overridden 
	// - NPC = 0
	// - Set Transaction to zero; just trying to reuse the same logic for other premoums
	// - Need to restore the other premium
	// Flat Premium 
	// - Unit Premium = Flat Premium
	// - NPC = same calculation as the others 
	private void updatePendingAdjustmentPremium(Premium premium, Boolean isFlatPremium, Boolean isPremiumOverride,
												int initialVehicleCount, int adjustedVehicleCount, double prorataFactor, double unitRate,
												LocalDate termEffDate, LocalDate termExpDate, LocalDate changeEffDate) {
		PremiumHelper premiumHelper = (PremiumHelper) getPolicyServicesFactory().newPremiumHelper();
		int difference = adjustedVehicleCount - initialVehicleCount;

		updatePendingAdjustmentRatedPremium(premium, isFlatPremium, isPremiumOverride, unitRate, adjustedVehicleCount);

		/* ES-143 apply unit change to unit premium
		if (BooleanUtils.isTrue(isPremiumOverride)) {
			// No Premium Change
			premium.setNetPremiumChange(0D);
		}
		else {
		*/
			double premiumChange = 0;
			// ES-175 make FlatPremium like PremiumOverride 
			//if (BooleanUtils.isTrue(isFlatPremium)) {
				//premiumChange = getRoundingStrategyFactory().getRoundingStrategy().round(difference * unitRate, Constants.TRANS_TYPE_ONSET);
			//}
			//else {
				premiumChange = getRoundingStrategyFactory().getRoundingStrategy().round(difference * prorataFactor * unitRate, Constants.TRANS_TYPE_ONSET);
			//}
		 
			//double transPrem = getRoundingStrategyFactory().getRoundingStrategy().calcProrata(difference * unitRate,
			//											termEffDate, termExpDate, changeEffDate, recordType, verTxnType);
		    double transPrem = premiumChange - premium.getPriorNetPremiumChange();
			premium.setTransactionPremium(transPrem);
			
			premiumHelper.updatePremiumValues(Constants.VERS_TXN_TYPE_ADJUSTMENT, Constants.TRANS_TYPE_ONSET, premium);
		//}		
	}
	
	private void updatePendingAdjustmentRatedPremium(Premium premium, Boolean isFlatPremium, Boolean isPremiumOverride, double unitRate, int numOfUnits) {
		double newPremium = getRoundingStrategyFactory().getRoundingStrategy().round(unitRate * numOfUnits, Constants.TRANS_TYPE_ONSET);
		
		//ES-170 for premium overridden, unit rate is for annual premium only.
		//ES-175 make FlatPremium like PremiumOverride
		if (BooleanUtils.isTrue(isPremiumOverride) || BooleanUtils.isTrue(isFlatPremium)) {
			premium.setAnnualPremium(newPremium);
		}
		else {
			premium.setOriginalPremium(newPremium);
			premium.setAnnualPremium(newPremium);
		}
	}
	
	private void updatePendingSubsequenceVehicleAdjustments(SpecialtyVehicleRiskAdjustment nextAdjustment, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		while(nextAdjustment != null) {
			//need to get the latest adj
			if (getCoverageService().needToLoadCoverageDetails(nextAdjustment.getCoverageDetail())) {
				nextAdjustment = loadSpecialtyVehicleRiskAdjustment(nextAdjustment);
			}
			
			updatePendingVehicleAdjustment(nextAdjustment, policyTransaction, vehicleItemDTO);
			
			nextAdjustment = nextAdjustment.findNextAdjustment();
		}
	}
	
	private void updatePendingSubsequenceSubpolicyAdjustments(SpecialtyVehicleRiskAdjustment nextAdjustment, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		while(nextAdjustment != null) {
			updatePendingSpecialtySubpolicyAdjustments(nextAdjustment, policyTransaction, vehicleItemDTO);
			
			nextAdjustment = nextAdjustment.findNextAdjustment();
		}
	}
	
	@Override
	public void deleteSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, UserProfile userProfile)
			throws SpecialtyAutoPolicyException {
		LoggedUser.logIn(userProfile != null ? userProfile.getLoginId() : StringUtils.EMPTY);

		try {
			//if (getCoverageService().needToLoadCoverageDetails(policyTransaction.getCoverageDetail())) { 
				policyTransaction = loadPolicyTransactionFull(policyTransaction, false);
			//}
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException(e);
		}

		final SpecialtyVehicleRisk parentVehicle = adjustment.getSpecialtyVehicleRisk();
		final SpecialtyVehicleRiskAdjustment nextAdj = adjustment.findNextAdjustment();
		final SpecialtyPolicyItemDTO vehicleItemDTO = getOriginalSvrValues(parentVehicle);
		final Integer adjustmentNum = adjustment.getAdjustmentNum();
		final String adjustmentGroupdID = adjustment.getGroupID();
		
		removeSpecialtyVehicleRiskAdjustment(adjustment, parentVehicle, policyTransaction);
		
		// Current adjustment is deleted
		finializeSpecialtyAdjustmentSteps(nextAdj, adjustmentNum, adjustmentGroupdID, nextAdj, parentVehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		/* moved to finializeSpecialtyAdjustmentSteps
		//Update Subsequence Vehicle Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceVehicleAdjustments(nextAdj, policyTransaction, vehicleItemDTO);
		
		//VEHICLE - Update vehicle from adjustment now ?
		consolidatePendingVehicleAdjustment(adjNum, parentVehicle, policyTransaction, vehicleItemDTO, null);
		
		//VEHICLE - end vehicle adjustment
		updatePendingAdjustmentEnd(parentVehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		//Update Subsequence Subpolicy Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceSubpolicyAdjustments(nextAdj, policyTransaction, vehicleItemDTO);

		//SUBPOLICY - Update subpolicies from adjustment
		consolidatePendingSubpolicyAdjustmentPremiums(adjNum, policyTransaction, userProfile);
		
		//update rated premiums
		updateSpecialtyAdjustmentRatedPremium(nextAdj, parentVehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		//end adjustment processes 
		updateSpecialtyAdjustmentEnd(parentVehicle, policyTransaction, vehicleItemDTO, userProfile);
		*/
		
		LoggedUser.logOut();
	}
	
	@SuppressWarnings("unchecked")
	private void removeSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment adjustment, SpecialtyVehicleRisk parentVehicle,
														PolicyTransaction<?> policyTransaction) throws SpecialtyAutoPolicyException {
		final Integer adjNum = adjustment.getAdjustmentNum();
		final String groupdID = adjustment.getGroupID();
		
		// Delete same set of adjustments
		try {
			parentVehicle.removeSpecialtyVehicleRiskAdjustment(adjustment);
			
			CGLSubPolicy<Risk> cglSubPcy = (CGLSubPolicy<Risk>) policyTransaction.findSubPolicyByType(SUBPOLICY_CGLSUBPOLICY);
			if (cglSubPcy != null) {
				SpecialtyVehicleRiskAdjustment cglAdj = cglSubPcy.getSvrAdjustmentsByAdjNum(groupdID, adjNum);
				if (cglAdj != null) {
					cglSubPcy.removeSpecialtyVehicleRiskAdjustment(cglAdj);
				}
			}
			
			CargoSubPolicy<Risk> cgoSubPcy = (CargoSubPolicy<Risk>) policyTransaction.findSubPolicyByType(SUBPOLICY_CARGOSUBPOLICY);
			if (cgoSubPcy != null) {
				SpecialtyVehicleRiskAdjustment cgoAdj = cgoSubPcy.getSvrAdjustmentsByAdjNum(groupdID, adjNum);
				if (cgoAdj != null) {
					cgoSubPcy.removeSpecialtyVehicleRiskAdjustment(cgoAdj);
				}
			}	
		} catch (Exception e) {
			throw new SpecialtyAutoPolicyException(e);
		}
	}

	@Override
	public SpecialtyVehicleRiskAdjustment undeleteSpecialtyVehicleRiskAdjustment(PolicyTransaction<?> policyTransaction,
			SpecialtyVehicleRisk parentVehicle, SpecialtyVehicleRiskAdjustment adjustment)
			throws SpecialtyAutoPolicyException {
		if (adjustment != null) {
			if (StringUtils.equalsIgnoreCase(IAuditable.INACTIVE, adjustment.getSystemStatus())
					&& StringUtils.equalsIgnoreCase(IAuditable.DELETED, adjustment.getBusinessStatus())) {
				adjustment.setSystemStatus(IAuditable.ACTIVE);
				adjustment.setBusinessStatus(IAuditable.NOCHANGE);
				updateSpecialtyVehicleRisk(parentVehicle, null);
			}
		}

		return adjustment;
	}

	@Override
	public SpecialtyVehicleRiskAdjustment loadSpecialtyVehicleRiskAdjustment(SpecialtyVehicleRiskAdjustment adjustment)
			throws SpecialtyAutoPolicyException {
		SpecialtyVehicleRiskAdjustmentDAO dao = new SpecialtyVehicleRiskAdjustmentDAO();
		try {
			dao.loadInstance(adjustment, null);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Could not load adjustment instance: " + adjustment.getSpecialtyVehicleRiskAdjustmentPK(), e);
			throw new SpecialtyAutoPolicyException("Could not load policy adjustment instance: " + adjustment.getSpecialtyVehicleRiskAdjustmentPK(), e);
		}
		
		return adjustment;
	}
	
	private double calculateProrataFactorForAdjustments(PolicyTerm policyTerm, LocalDateTime adjustmentDate,
			SubPolicy<?> subPolicy) {
		final String fleetBasis = Optional.of(subPolicy)
			.filter(sp -> sp instanceof SpecialityAutoSubPolicy<?> ? true : false)
			.map(SpecialityAutoSubPolicy.class::cast)
			.map(SpecialityAutoSubPolicy::getFleetBasis)
			.orElse(StringUtils.EMPTY);
		switch (fleetBasis) {
		case "21BQ": // prorata quarterly
		case "21BS": // prorata semiannual
		case "21BA": // prorata annual
			final LocalDateTime effectiveDate = policyTerm.getTermEffDate();
			final LocalDateTime expirationDate = policyTerm.getTermExpDate();
			final double termDays = Duration.between(effectiveDate, expirationDate).toDays();
			final double daysAfterAdjustment = Duration.between(adjustmentDate, expirationDate).toDays();
			final double factor = daysAfterAdjustment / termDays;

			// ESL-2067 Round to three decimal places
			final double roundedFactor = new BigDecimal(factor).setScale(3, RoundingMode.HALF_UP).doubleValue();
			return roundedFactor;
		case "21B5": // 50/50 annual
			return 0.5d;
		default:
			logger.log(Level.SEVERE, "Unknown fleet basis \"{0}\" for prorata factor calculation", fleetBasis);
			throw new IllegalArgumentException("Unknown fleet basis");
		}
	}
	
	private int getAdjustmentUnitDifference(SpecialtyVehicleRiskAdjustment adjustment, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		final Integer prevAdjUnits = getPrevAdjustmentNumOfUnits(adjustment, vehicleItemDTO);
		final int initialVehicleCount = prevAdjUnits != null ? prevAdjUnits : 0; //parentVehicle.getNumberOfUnits();
		final int adjustedVehicleCount = adjustment.getNumOfUnits();
		final int difference = adjustedVehicleCount - initialVehicleCount;
		
		return difference;
	}

	private double calculatePremiumChangeForAdjustment(SpecialtyVehicleRiskAdjustment adjustment,
			SpecialtyVehicleRisk parentVehicle, double prorataFactor, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		/* moved to getAdjustmentUnitDifference
		final Integer prevAdjUnits = getPrevAdjustmentNumOfUnits(adjustment, policyTransaction);
		final int initialVehicleCount = prevAdjUnits != null ? prevAdjUnits : 0; //parentVehicle.getNumberOfUnits();
		final int adjustedVehicleCount = adjustment.getNumOfUnits();
		*/
		final int difference = getAdjustmentUnitDifference(adjustment, vehicleItemDTO);
		final double unitRate = vehicleItemDTO.getSubOriginatingUnitRate();

		final double premiumChange = difference * prorataFactor * unitRate;

		// ESL-2092 round to the nearest dollar (Math.round() will return a long, which
		// gets converted back to a double)
		return Math.round(premiumChange);
	}
	
	@Override
	public double getOriginatingUnitRate(SpecialtyVehicleRisk vehicle) {
		double unitRate = 0;
		
		final Long originatingPK = vehicle.getOriginatingPK();
		if (originatingPK != null) {
			try {
				final SpecialtyVehicleRisk originatingVehicle = (SpecialtyVehicleRisk) this.findRiskByPK(originatingPK);
				if (originatingVehicle != null && originatingVehicle.getUnitRate() != null) {
					unitRate = originatingVehicle.getUnitRate();
				}
			} catch (InsurancePolicyException e) {
				e.printStackTrace();
				logger.log(Level.SEVERE, "Could not load originating risk", e);
				return 0d;
			}
		}
		
		return unitRate;
	}

	private double calculatePremiumChangeForAdjustment(SpecialtyVehicleRiskAdjustment adjustment,
			SubPolicy<?> parentSubPolicy, double prorataFactor, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		/* moved to getAdjustmentUnitDifference
		final Integer prevAdjUnits = getPrevAdjustmentNumOfUnits(adjustment, policyTransaction);
		final int initialVehicleCount = prevAdjUnits != null ? prevAdjUnits : 0; //parentVehicle.getNumberOfUnits();
		final int adjustedVehicleCount = adjustment.getNumOfUnits();
		*/
		final int difference = getAdjustmentUnitDifference(adjustment, vehicleItemDTO);
		final SpecialtyPolicyItemDTO policyItemDTO = getOriginalSvrValues(parentSubPolicy);
		final double unitRate = policyItemDTO.getSubOriginatingUnitRate();

		final double premiumChange = difference * prorataFactor * unitRate;

		// ESL-2092 round to the nearest dollar (Math.round() will return a long, which
		// gets converted back to a double)
		return Math.round(premiumChange);
	}

	//Change logic - moved to consolidatePendingVehicleAdjustment
	/**
	 * Prior to issuing an adjustment transaction, we need to consolidate each
	 * vehicle's adjustments into a change to the vehicle itself.
	 * 
	 * @param pcyTxn the policy transaction
	 * @throws SpecialtyAutoPolicyException 
	 */
	/*
	private void consolidatePendingPolicyAdjustments(PolicyTransaction<?> pcyTxn) {
		// Start by locating the auto sub policy
		final SpecialityAutoSubPolicy<?> subPolicy = SetUtils.emptyIfNull(pcyTxn.getSubPolicies()).stream()
				.filter(subPcy -> subPcy instanceof SpecialityAutoSubPolicy<?>)
				.map(SpecialityAutoSubPolicy.class::cast)
				.findFirst()
				.orElse(null);
		if (subPolicy != null) {
			// For each vehicle...
			final Stream<SpecialtyVehicleRisk> vehicles = SetUtils.emptyIfNull(subPolicy.getSubPolicyRisks()).stream()
					.filter(risk -> risk instanceof SpecialtyVehicleRisk)
					.map(SpecialtyVehicleRisk.class::cast);
			vehicles.forEach(vehicle -> {
				// ...calculate the net change to the number of units
				final Set<SpecialtyVehicleRiskAdjustment> adjustments = SetUtils
						.emptyIfNull(vehicle.getSvrAdjustments());
				// total change is SUM(adjustment.numUnits - vehicle.numUnits) for all adjustments
				final int changeInNumberOfUnits = adjustments.stream()
						.map(SpecialtyVehicleRiskAdjustment::getNumOfUnits)
						.mapToInt(adjusted -> adjusted - vehicle.getNumberOfUnits())
						.sum();
				final int adjustedNumberOfUnits = vehicle.getNumberOfUnits() + changeInNumberOfUnits;
				vehicle.setNumberOfUnits(adjustedNumberOfUnits);
			});
		}
	}
	*/
	
	private List<SpecialtyVehicleRiskAdjustment> getAllLastAdjustment(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		final String DEFAULTGROUPDID = "ADJ";
		
		List<SpecialtyVehicleRiskAdjustment> results = new ArrayList<SpecialtyVehicleRiskAdjustment>();
		
		HashMap<String, Boolean> hm = new HashMap<String, Boolean>();
		for(SpecialtyVehicleRiskAdjustment adj : svrAdjustments) {
			String groupID = adj.getGroupID();
			if (groupID == null) {
				groupID = DEFAULTGROUPDID;
			}
			
			if (hm.get(groupID) == null) {
				hm.put(groupID, Boolean.TRUE);
				
				SpecialtyVehicleRiskAdjustment lastAdj = adj.findLastAdjustment(adj.getGroupID());
				if (lastAdj != null) {
					results.add(lastAdj);
				}
			}
		}
		
		return results;
	}
	
	private Integer getLastAdjustmentTotalUnits(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		Integer totUnits = null;
		
		List<SpecialtyVehicleRiskAdjustment> lastAdjs = getAllLastAdjustment(svrAdjustments);
		for(SpecialtyVehicleRiskAdjustment adj : lastAdjs) {
			if (adj != null && adj.getNumOfUnits() != null) {
				if (totUnits == null) {
					totUnits = 0;
				}
				
				totUnits += adj.getNumOfUnits();
			}
		}
		
		return totUnits;
	}
	
	//Update vehicle coverage premiums? consolidatePendingVehicleAdjustmentPremiums(parentVehicle);
	//update and rollup ? updateSpecialtyVehicleRisk(parentVehicle, userProfile);
	private void consolidatePendingVehicleAdjustment(Integer adjNum, SpecialtyVehicleRisk vehicle, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO,
														UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			if (getCoverageService().needToLoadCoverageDetails(vehicle.getCoverageDetail())) {
				vehicle = (SpecialtyVehicleRisk) loadSubPolicyRisk(vehicle, false);
			}
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not load vehicle for adjustment #: " + adjNum, e);
		}
		try {
			for(SpecialtyVehicleRiskAdjustment adj : vehicle.getSvrAdjustments()) {
				if (getCoverageService().needToLoadCoverageDetails(adj.getCoverageDetail())) {
					loadSpecialtyVehicleRiskAdjustment(adj);
				}
			}
		} catch (SpecialtyAutoPolicyException e) {
			throw new SpecialtyAutoPolicyException("Could not load vehicle adjustments for adjustment #: " + adjNum, e);
		}
		
		consolidatePendingVehicleAdjustmentVehicle(vehicle, vehicleItemDTO);
		consolidatePendingAdjustmentPremiums(vehicle.getSvrAdjustments(), vehicle.getCoverageDetail().getCoverages(), vehicle.getCoverageDetail().getEndorsements(),
												vehicle, vehicle.getNumberOfUnits());
		
		//save vehicle and rollup - moved
		//updateSpecialtyVehicleRisk(vehicle, userProfile);
	}
	
	private void consolidatePendingVehicleAdjustmentVehicle(SpecialtyVehicleRisk vehicle, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		Integer totUnits = getLastAdjustmentTotalUnits(vehicle.getSvrAdjustments());
		if (totUnits != null) {
			vehicle.setNumberOfUnits(totUnits);
		}
		else {
			vehicle.setNumberOfUnits(vehicleItemDTO.getOriginatingUnits());
		}
	}
	
	private void consolidatePendingSubpolicyAdjustmentPremiums(Integer adjNum, PolicyTransaction<?> policyTransaction, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		consolidatePendingSubpolicyAdjustmentPremiums(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		consolidatePendingSubpolicyAdjustmentPremiums(policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_CARGO); 
	}
	
	private void consolidatePendingSubpolicyAdjustmentPremiums(PolicyTransaction<?> policyTransaction, String subpolicyTypeCd) throws SpecialtyAutoPolicyException {
		Set<Coverage> coverages = new HashSet<Coverage>();
		Set<Endorsement> endorsements = new HashSet<Endorsement>();
		SubPolicy<?> subPcy = getAdjustmentSubPolicyItems(policyTransaction, subpolicyTypeCd, coverages, endorsements);
		if (subPcy != null) {
			ISpecialtyHasAdjustment adjContainer = (ISpecialtyHasAdjustment) subPcy;
			Set<SpecialtyVehicleRiskAdjustment> spAdjs = adjContainer.getSvrAdjustments();
			
			if (coverages != null && !coverages.isEmpty()) {
				for(Coverage cov : coverages) {
					consolidatePendingAdjustmentPremiums(spAdjs, 
															Stream.of(cov).collect(Collectors.toCollection(HashSet::new)), new HashSet<Endorsement>(),  
															subPcy, cov.getNumOfUnits());
				}
			}
			
			if (endorsements != null && !endorsements.isEmpty()) {
				for(Endorsement end : endorsements) {
					consolidatePendingAdjustmentPremiums(spAdjs, 
															new HashSet<Coverage>(), Stream.of(end).collect(Collectors.toCollection(HashSet::new)),   
															subPcy, end.getNumOfUnits());
				}
			}
		}
	}
	
	private void consolidatePendingAdjustmentPremiums(Set<SpecialtyVehicleRiskAdjustment> svrAdjustments,
														Set<Coverage> coverages, Set<Endorsement> endorsements,
														IBusinessEntity parentAdjustment,
														Integer totUnits) throws SpecialtyAutoPolicyException {
		if (totUnits == null) {
			totUnits = 0;
		}
		
		svrAdjustments.stream().forEach(o -> {
			try {
				if (getCoverageService().needToLoadCoverageDetails(o.getCoverageDetail())) {
					o = loadSpecialtyVehicleRiskAdjustment(o);
				}
			} catch (SpecialtyAutoPolicyException e) {}
		});
		
		List<Coverage> filterCoverages = coverages.stream().collect(Collectors.toList());
		filterCoverages.removeIf(o -> !filterCoverages.contains(o) || o.getCoveragePremium().getUnitPremium() == null);
		
		for(Coverage cov : filterCoverages) {
			List<Coverage> adjCoverages = svrAdjustments
											.stream().map(SpecialtyVehicleRiskAdjustment::getCoverageDetail)
											.map(CoverageDetails::getCoverages)
											.flatMap(Collection::stream)
											.filter(o -> o.getOriginatingPK() != null && o.getOriginatingPK().equals(cov.getCoveragePK()))
											.collect(Collectors.toList());
			
			double changPrem = 0;
						
			changPrem += adjCoverages.stream()
							.map(Coverage::getCoveragePremium)
							.mapToDouble(Premium::getNetPremiumChange)
							.sum();
			
			Premium premium = cov.getCoveragePremium(); 
			consolidatePendingAdjustmentPremium(premium, cov.isFlatPremium(), premium.isPremiumOverride(), premium.getUnitPremium(), totUnits, changPrem);
			
			if (BooleanUtils.isTrue(cov.isReinsured())) {
				for(CoverageReinsurance covRis : cov.getCoverageReinsurances()) {
					changPrem = 0;
					
					changPrem += adjCoverages.stream()
									.map(Coverage::getCoverageReinsurances)
									.flatMap(Collection::stream)
									.filter(o -> o.getOriginatingPK() != null && o.getOriginatingPK().equals(covRis.getCoverageReinsurancePK()))
									.map(CoverageReinsurance::getCovReinsurancePremium)
									.mapToDouble(Premium::getNetPremiumChange)
									.sum();
					
					premium = covRis.getCovReinsurancePremium(); 
					consolidatePendingAdjustmentPremium(premium, covRis.getIsFlatPremium(), premium.isPremiumOverride(), premium.getUnitPremium(), totUnits, changPrem);
				}
			}
		}
		
		List<Endorsement> filterEndorsements = endorsements.stream().collect(Collectors.toList());
		filterEndorsements.removeIf(o -> !filterEndorsements.contains(o) || o.getEndorsementPremium().getUnitPremium() == null);

		for(Endorsement end : filterEndorsements) {
			List<Endorsement> adjEndorsements = svrAdjustments
													.stream().map(SpecialtyVehicleRiskAdjustment::getCoverageDetail)
													.map(CoverageDetails::getEndorsements)
													.flatMap(Collection::stream)
													.filter(o -> o.getOriginatingPk() != null && o.getOriginatingPk().equals(end.getEndorsementsk()))
													.collect(Collectors.toList());
			
			double changPrem = 0;
			
			changPrem += adjEndorsements.stream()
							.map(Endorsement::getEndorsementPremium)
							.mapToDouble(Premium::getNetPremiumChange)
							.sum();
						
			Premium premium = end.getEndorsementPremium();
			consolidatePendingAdjustmentPremium(premium, end.isFlatPremium(), premium.isPremiumOverride(), premium.getUnitPremium(), totUnits, changPrem);
			
			if (BooleanUtils.isTrue(end.isReinsured())) {
				for(EndorsementReinsurance endRis : end.getEndorsementReinsurances()) {
					changPrem = 0;
					
					changPrem += adjEndorsements.stream()
									.map(Endorsement::getEndorsementReinsurances)
									.flatMap(Collection::stream)
									.filter(o -> o.getOriginatingPK() != null && o.getOriginatingPK().equals(endRis.getEndorsementReinsurancePK()))
									.map(EndorsementReinsurance::getEndReinsurancePremium)
									.mapToDouble(Premium::getNetPremiumChange)
									.sum();
								
					premium = endRis.getEndReinsurancePremium();
					consolidatePendingAdjustmentPremium(premium, endRis.getIsFlatPremium(), premium.isPremiumOverride(), premium.getUnitPremium(), totUnits, changPrem);
				}
			}
		}
	}
	
	private Premium consolidatePendingAdjustmentPremium(Premium premium, Boolean isFlatPremium, Boolean isPremiumOverride, double unitRate, int numOfUnits, double changPrem) {
		premium.setNetPremiumChange(changPrem);
		premium.setTransactionPremium(premium.getPriorNetPremiumChange() + premium.getNetPremiumChange());
		premium.setWrittenPremium(premium.getPriorWrittenPremium() + premium.getNetPremiumChange());
		premium.setTermPremium(premium.getWrittenPremium());
		
		updatePendingAdjustmentRatedPremium(premium, isFlatPremium, isPremiumOverride, unitRate, numOfUnits);
		
		return premium;
	}

	@Override
	public PolicyTransaction<?> preCreatePolicyAdjustment(PolicyTransaction<?> policyTransaction,
			LocalDate changeEffDate, UserProfile userProfile) throws InsurancePolicyException {		
		// ESL-2058 do not allow adjustments for scheduled, mileage, receipts policies
		// Moved from UI
		final SubPolicy<?> autoSubPolicy = policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (autoSubPolicy != null) { 
			final String fleetBasis = Optional.ofNullable(autoSubPolicy)
					.map(SpecialityAutoSubPolicy.class::cast).map(SpecialityAutoSubPolicy::getFleetBasis)
					.orElse(StringUtils.EMPTY);
			switch (fleetBasis) {
			case "SCHD": // Scheduled
			case "21AM": // 21A-Mileage
			case "21AR": // 21A-Receipts
				throw new InsurancePolicyException("Do not allow adjustments for scheduled, mileage, receipts policies");
			}
		}
		
		return super.preCreatePolicyAdjustment(policyTransaction, changeEffDate, userProfile);
	}

	@Override
	public boolean validateDriverSurcharged(PolicyTransaction<?> pcyTxn)
			throws SpecialtyAutoPolicyException {
		boolean anyFound = (new VehicleDriverDAO()).anySameAttachedDriverVehiclesHasSurcharges(pcyTxn);
		return !anyFound;
	}

	@Override
	public List<String> getPolicyActionWarnings(String action, PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		// TODO Auto-generated method stub
		return new ArrayList<String>();
	}

	// Either coverages or endorsements (or both!) might be null
	protected boolean isCGCovLWLLLimitsExceeded(SubPolicy<?> subPolicy, List<Coverage> coverages,
			List<Endorsement> endorsements) throws SpecialtyAutoPolicyException {
		if (Objects.nonNull(subPolicy) && (subPolicy.isActive() || subPolicy.isPending())) {
			if (SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(subPolicy.getSubPolicyTypeCd())) {
				try {
					subPolicy = loadSubPolicy(subPolicy, false);
				} catch (Exception e) {
				}

				// Check to see if WLL is already present on the policy
				Coverage wllCoverage = subPolicy.getCoverageDetail()
						.getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.CGL_SUBPCY_COVERAGE_WLL);
				if (Objects.isNull(wllCoverage)) {
					/*
					 * ES-97
					 * 
					 * If we're in the process of adding WLL, it won't have been applied to the
					 * sub-policy yet. Check the list of coverages we were given (which might be
					 * null).
					 */
					final List<Coverage> covs = ObjectUtils.defaultIfNull(coverages, Collections.emptyList());
					wllCoverage = covs.stream().filter(cov -> SpecialtyAutoConstants.CGL_SUBPCY_COVERAGE_WLL
							.equalsIgnoreCase(cov.getCoverageCode())).findFirst().orElse(null);
				}

				if (Objects.nonNull(wllCoverage) && Objects.nonNull(wllCoverage.getLimit1())
						&& wllCoverage.getLimit1() > SpecialtyAutoConstants.WARN_COV_WLL_LIMIT_VALUE) {
					return true;
				}
			}
		}

		return false;
	}
	
	//create a read-only adjustment record for CGL and CGO sub-policies
	//use the same adjustment number to link the adjustments together.
	//To include CGL & CARGO, please check with SubPolicyRiskPremiumRollup.totalRiskPremium
	private void updatePendingSpecialtySubpolicyAdjustments(SpecialtyVehicleRiskAdjustment adjustment, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		CoverageServiceHelper coverageServiceHelper = getCoverageServiceHelper();
		
		SpecialityAutoSubPolicy<?> spSubPolicy = (SpecialityAutoSubPolicy<?>) policyTransaction.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		final int unitDifference = getAdjustmentUnitDifference(adjustment, vehicleItemDTO);
		if (coverageServiceHelper.isCalcVehicleGroupUnitRate(policyTransaction, spSubPolicy, adjustment.getSpecialtyVehicleRisk(), SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
			updatePendingSubpolicyAdjustments(adjustment, unitDifference, policyTransaction, vehicleItemDTO, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
		}
		if (coverageServiceHelper.isCalcVehicleGroupUnitRate(policyTransaction, spSubPolicy, adjustment.getSpecialtyVehicleRisk(), SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
			updatePendingSubpolicyAdjustments(adjustment, unitDifference, policyTransaction, vehicleItemDTO, SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void updatePendingSubpolicyAdjustments(SpecialtyVehicleRiskAdjustment adjustment, int unitDifference, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO,
														String forSubPolicyCode) throws SpecialtyAutoPolicyException {
		final Integer adjNum  = adjustment.getAdjustmentNum();
		final String  groupID = adjustment.getGroupID();
		
		Set<Coverage> coverages = new HashSet<Coverage>();
		Set<Endorsement> endorsements = new HashSet<Endorsement>();
		SubPolicy<Risk> subPcy = (SubPolicy<Risk>) getAdjustmentSubPolicyItems(policyTransaction, forSubPolicyCode, coverages, endorsements);
		if (subPcy != null) {
			ISpecialtyHasAdjustment adjContainer = (ISpecialtyHasAdjustment) subPcy;
			
			SpecialtyVehicleRiskAdjustment spAdj = adjContainer.getSvrAdjustmentsByAdjNum(groupID, adjNum);
			if (spAdj == null) {
				spAdj = new SpecialtyVehicleRiskAdjustment();
				setAdjustmentSubpolicy(subPcy, spAdj);
				spAdj.setGroupID(groupID);
				
				if (coverages != null && !coverages.isEmpty()) {
					for(Coverage cov : coverages) {
						if (cov != null) {
							try {
								Coverage rcClone = createNewAdjustmentCoveragePremium(cov);
								spAdj.addCoverage(rcClone);
							} catch (CloneNotSupportedException e) {
								throw new SpecialtyAutoPolicyException("Could not clone "+cov.getCoverageCode()+" coverage for "+forSubPolicyCode+" adjustment", e);
							}
						}
					}
				}
				
				spAdj.setAdjustmentNum(adjustment.getAdjustmentNum());
			}
			else {
				if (getCoverageService().needToLoadCoverageDetails(spAdj.getCoverageDetail())) {
					try {
						loadSpecialtyVehicleRiskAdjustment(spAdj);
					} catch (Exception e) {
					}
				}
			}
			
			copyAdjustmentFields(adjustment, spAdj);
			double premiumChange = calculatePremiumChangeForAdjustment(spAdj, subPcy, spAdj.getProRataFactor(), policyTransaction, vehicleItemDTO);
			// final NPC will be updated in updatePendingAdjustmentPremiums
			spAdj.setPremiumChange(premiumChange);
			updatePendingAdjustmentPremiums(spAdj, policyTransaction, vehicleItemDTO);
			
			if (adjustment.getSpecialtyVehicleRiskAdjustmentPK() == null) {
				adjContainer.addSpecialtyVehicleRiskAdjustment(spAdj);
			}
		}
	}
	
	public SpecialtyPolicyItemDTO getOriginalSvrValues(SpecialtyVehicleRisk inSvr) throws SpecialtyAutoPolicyException {
		SpecialtyPolicyItemDTO rv = new SpecialtyPolicyItemDTO(inSvr.getOriginatingPK());
		
		if (inSvr != null && inSvr.getOriginatingPK() != null) {
			SpecialtyVehicleRiskDAO svrDAO = new SpecialtyVehicleRiskDAO();
			Map<Long, SpecialtyVehicleRiskDTO> dtoMap =  svrDAO.getSpecialtyVehicleRiskDTOMap(new ArrayList<Long>(Arrays.asList(inSvr.getOriginatingPK())));
			if (dtoMap != null && !dtoMap.isEmpty()) {
				SpecialtyVehicleRiskDTO dto = dtoMap.get(inSvr.getOriginatingPK());
				if (dto != null) {
					rv = new SpecialtyPolicyItemDTO(dto.getPolicyRiskPK(), dto.getUnitRate(), dto.getNumberOfUnits(), dto.getPolicyRiskPK(), inSvr.getRiskPremium().getUnitPremium());
				}
			}
		}
		return rv;
	}
	
	private SpecialtyPolicyItemDTO getOriginalSvrValues(SubPolicy<?> inSvr) throws SpecialtyAutoPolicyException {
		SpecialtyPolicyItemDTO result = null;
				
		switch (inSvr.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			result = (new SpecialtySubPolicyDAO()).getOriginalSvrValues(inSvr.getSubPolicyPK(), SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD);
			
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			result = (new SpecialtySubPolicyDAO()).getOriginalSvrValues(inSvr.getSubPolicyPK(), SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC);
			
			break;

		default:
			result = new SpecialtyPolicyItemDTO(inSvr.getOriginatingPK());
			
			break;
		}
		
		return result;
	}
	
	private Integer getPrevAdjustmentNumOfUnits(SpecialtyVehicleRiskAdjustment adj, SpecialtyPolicyItemDTO vehicleItemDTO) throws SpecialtyAutoPolicyException {
		Integer result = null;
		
		if (adj != null) {
			SpecialtyVehicleRiskAdjustment prevAdj = adj.findPrevAdjustment();
			if (prevAdj != null) {
				result = prevAdj.getNumOfUnits();
			}
			else if (vehicleItemDTO != null) {
				result = vehicleItemDTO.getOriginatingUnits();
			}
		}
		
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public SpecialtyPolicyDTO getSpecialtyPolicyDTO(PolicyTransaction<?> pcyTxn)
			throws SpecialtyAutoPolicyException {
		SpecialtyPolicyDTO policyDTO = new SpecialtyPolicyDTO();
		
		boolean isAdjVersion = Constants.VERS_TXN_TYPE_ADJUSTMENT.equalsIgnoreCase(pcyTxn.getPolicyVersion().getPolicyTxnType());
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> apSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_AUTO);
		if (apSubPolicy != null && apSubPolicy.getSubPolicyRisks() != null) {
			for(SpecialtyVehicleRisk vehicle : apSubPolicy.getSubPolicyRisks()) {
				SpecialtyPolicyItemDTO itemDTO = getOriginalSvrValues(vehicle);
				policyDTO.getVehicleItemsByPK().put(vehicle.getPolicyRiskPK(), itemDTO);
				
				if (isAdjVersion) {
					vehicle.getSvrAdjustments().stream().forEach(o ->
							policyDTO.getVehicleItemsByAdjNum().put(o.getAdjustmentNum(), itemDTO)
					);
				}
			}
		}
		
		if (isAdjVersion) {
			CGLSubPolicy<?> cglSubPolicy = (CGLSubPolicy<?>) pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);
			if (cglSubPolicy != null) {
				SpecialtyPolicyItemDTO originalSvrValues = getOriginalSvrValues(cglSubPolicy);
				policyDTO.setCglSubPolicyItem(originalSvrValues);
			}
			
			CargoSubPolicy<?> cargoSubPolicy = (CargoSubPolicy<?>) pcyTxn.findSubPolicyByType(SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
			if (cargoSubPolicy != null) {
				SpecialtyPolicyItemDTO originalSvrValues = getOriginalSvrValues(cargoSubPolicy);
				policyDTO.setCargoSubPolicyItem(originalSvrValues);
			}
		}
		
		return policyDTO;
	}
	
	private void updateNewSubPolicyReinsured(PolicyTransaction<?> policyTransaction) throws InsurancePolicyException {
		for(SubPolicy<?> subPolicy : policyTransaction.getSubPolicies().stream().filter(o -> o.getSubPolicyPK() == null).collect(Collectors.toList())) {
			IGenericProductConfig productConfig = null;
			try {
				productConfig = ProductConfigFactory.getInstance().getProductConfig("", "", policyTransaction.getPolicyVersion().getInsurancePolicy().getProductCd(), 
						policyTransaction.getPolicyVersion().getInsurancePolicy().getProductProgram(), 
						policyTransaction.getPolicyVersion().getInsurancePolicy().getControllingJurisdiction(), 
						policyTransaction.getPolicyTerm().getTermEffDate().toLocalDate());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new InsurancePolicyException(e);
			}
			if (productConfig != null) {
				if (ObjectUtils.compare(((SpecialtyAutoPackage)policyTransaction.getPolicyVersion().getInsurancePolicy()).getLiabilityLimit(), 
						getPolicyMaxRetainedLimit(policyTransaction).intValue()) > 0) {
	
					SubPolicy<?> sp = subPolicy;
					SubPolicyConfig spConfig = productConfig.getSubPolicyConfig(sp.getSubPolicyTypeCd());
					if (spConfig != null) {
						if (getCoverageService().needToLoadCoverageDetails(sp.getCoverageDetail())) {
							sp = loadSubPolicy(sp, false);
						}
						
						//subpolicy coverages
						sp.getCoverageDetail().getCoverages().stream().filter(spc -> spc.isActive() || spc.isPending()).forEach(spc -> {
							CoverageConfig covConfig = spConfig.getCoverageConfig(spc.getCoverageCode());
							if (covConfig != null && covConfig.isReinsurable()) {
								spc.setReinsured(Boolean.TRUE);
							}																
						});
	
						//subpolicy endorsements
						sp.getCoverageDetail().getEndorsements().stream().filter(spe -> spe.isActive() || spe.isPending()).forEach(spe -> {
							EndorsementConfig endConfig = spConfig.getEndorsementConfig(spe.getEndorsementCd());
							if (endConfig != null && endConfig.isReinsurable()) {
								spe.setReinsured(Boolean.TRUE);
							}
						});
					}
				}
			}
		}
	}
	
	@Override
	public ReinsuranceReportDTO getReinsuranceReportDTO(PolicyTransaction<?> policyTransaction)
			throws SpecialtyAutoPolicyException {
		final Long policyTransactionPK = policyTransaction.getPolicyTransactionPK();
		ReinsuranceReportDTO rv = new ReinsuranceReportDTO();
		if (policyTransactionPK != null) {
			PolicyTransactionDAO dao = new PolicyTransactionDAO();
			SpecialtyVehicleRiskDAO svrDAO = new SpecialtyVehicleRiskDAO();
			try {
				List<ReinsuranceItemDTO> items = dao.getReinsuranceItemsByTxnPk(policyTransactionPK);
				items.add(svrDAO.getEchelonReinsuranceItemsByTxnPk(policyTransactionPK,
						getPolicyMaxRetainedLimit(policyTransaction)));
				items = items.stream().sorted(Comparator.comparing(ReinsuranceItemDTO::getEndLayer)).collect(Collectors.toList());
				Map<String, ReinsuranceItemDTO> keyToItemDTO = new HashMap<String, ReinsuranceItemDTO>();
				items.stream().forEach(dto -> {
					String dtoKey = dto.getReinsurerName() + "_" + String.valueOf(dto.getEndLayer());
					ReinsuranceItemDTO mapDTO = keyToItemDTO.get(dtoKey);
					if (mapDTO == null) {
						keyToItemDTO.put(dtoKey, dto);
					} else {
						mapDTO.setGrossPremium(mapDTO.getGrossPremium() + dto.getGrossPremium());
						mapDTO.setNetPremium(mapDTO.getNetPremium() + dto.getNetPremium());
					}
				});
				
				items = new ArrayList<ReinsuranceItemDTO>(keyToItemDTO.values());
				ReinsuranceItemDTO echelonReinsDTO = items.stream().filter(dto -> StringUtils.indexOfIgnoreCase(dto.getReinsurerName(), SpecialtyAutoConstants.REINSURER_ECHELON) > -1).findFirst().orElse(null);
				if (echelonReinsDTO != null) {
//					echelonReinsDTO.setGrossPremium((echelonReinsDTO.getGrossPremium() != null ? echelonReinsDTO.getGrossPremium() : 0) 
//							+ (echelonReinsDTO.getNetPremium() != null ? echelonReinsDTO.getNetPremium() : 0));
					echelonReinsDTO.setNetPremium(0D);
				}
				items = items.stream().sorted(Comparator.comparing(ReinsuranceItemDTO::getEndLayer)
						.thenComparing(ReinsuranceItemDTO::getReinsurerName))
						.collect(Collectors.toList());
				rv.setReisnuranceItems(items);
				
				Double netPremReinsTotal = 0D;
				Double grossPremReinsTotal = 0D;
				Double netPremRetnetionTotal = 0D;
				Double grossPremRetnetionTotal = 0D;
				Double netPremGrandTotal = 0D;
				Double grossPremGrandTotal = 0D;
				
				for (ReinsuranceItemDTO item : items) {
					if (StringUtils.containsIgnoreCase(item.getReinsurerName(), SpecialtyAutoConstants.REINSURER_ECHELON)) {
						netPremRetnetionTotal += item.getNetPremium();
						grossPremRetnetionTotal += item.getGrossPremium();
					} else {
						netPremReinsTotal += item.getNetPremium();
						grossPremReinsTotal += item.getGrossPremium();
					}
					netPremGrandTotal += item.getNetPremium();
					grossPremGrandTotal += item.getGrossPremium();
				}
				
				rv.setNetPremRetentionTotal(netPremRetnetionTotal);
				rv.setGrossPremRetentionTotal(grossPremRetnetionTotal);
				rv.setNetPremReisnurnaceTotal(netPremReinsTotal);
				rv.setGrossPremReisnurnaceTotal(grossPremReinsTotal);
				rv.setNetPremGrandTotal(netPremGrandTotal);
				rv.setGrossPremGrandTotal(grossPremGrandTotal);
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Could not get ReinsuranceReportDTO", e);
				throw new SpecialtyAutoPolicyException("Could not get ReinsuranceReportDTO", e);
			}
		}
		return rv;
	}
	
	private void restoreVehicleUnitRate(SpecialtyVehicleRisk vehicle, SpecialtyPolicyItemDTO itemDTO) throws SpecialtyAutoPolicyException {
		if (vehicle != null) {
			vehicle.setUnitRate(itemDTO.getOriginatingUnitRate());
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PolicyTransaction<?> updateAdjustmentOnsetTransaction(PolicyTransaction<?> inPcyTxn,
			LocalDate changeEffDate, UserProfile userProfile) {
		inPcyTxn = super.updateAdjustmentOnsetTransaction(inPcyTxn, changeEffDate, userProfile);
		//AUTO
		SpecialityAutoSubPolicy<Risk> autoSP = (SpecialityAutoSubPolicy<Risk>) inPcyTxn.getSubPolicies().stream().filter(
				sp -> SpecialtyAutoConstants.SUBPCY_CODE_AUTO.equalsIgnoreCase(sp.getSubPolicyTypeCd())).findFirst().orElse(null);
		if (autoSP != null) {
			autoSP.getSubPolicyRisks().stream().forEach(risk -> {
				SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
				svr.getRiskPremium().setUnitPremium(0D);
				svr.getRiskCoverages().stream().forEach(cov -> {
					updateAdjustmentOnsetTransactionCoveragePremium(cov, svr.getNumberOfUnits());
				});
				svr.getRiskEndorsements().stream().forEach(end -> {
					updateAdjustmentOnsetTransactionEndorsementPremium(end, svr.getNumberOfUnits());
				});
				
				svr.getRiskPremium().setUnitPremium(totalAdjustmentUnitRate(svr.getRiskCoverages(), svr.getRiskEndorsements()));
			});
		}
		inPcyTxn.getSubPolicies().stream().filter(sp -> 
				SpecialtyAutoConstants.SUBPCY_CODE_CARGO.equalsIgnoreCase(sp.getSubPolicyTypeCd()) || 
				SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY.equalsIgnoreCase(sp.getSubPolicyTypeCd()))
				.collect(Collectors.toList()).forEach(subPcy -> {
			subPcy.getCoverageDetail().getCoverages().stream().forEach(cov -> {
				updateAdjustmentOnsetTransactionCoveragePremium(cov, cov.getNumOfUnits());
			});
			subPcy.getCoverageDetail().getEndorsements().stream().forEach(end -> {
				updateAdjustmentOnsetTransactionEndorsementPremium(end, end.getNumOfUnits());
			});
		});
		return inPcyTxn;
	}
	
	private Double totalAdjustmentUnitRate(Set<Coverage> coverages, Set<Endorsement> endorsements) {
		Double covUnitRateTot = coverages.stream().map(o -> o.getCoveragePremium())
													.filter(o -> o.getUnitPremium() != null)
													.mapToDouble(Premium::getUnitPremium).sum();

		Double endUnitRateTot = endorsements.stream().map(o -> o.getEndorsementPremium())
													.filter(o -> o.getUnitPremium() != null)
													.mapToDouble(Premium::getUnitPremium).sum();
		Double unitRateTot = covUnitRateTot+endUnitRateTot;
		
		return unitRateTot;
	}

	private void updateAdjustmentOnsetTransactionCoveragePremium(Coverage cov, Integer numOfUnits) {
		Premium premium = cov.getCoveragePremium();
		updateAdjustmentOnsetTransactionPremium(premium, numOfUnits, cov.isFlatPremium());
		
		if (BooleanUtils.isTrue(cov.isReinsured())) {
			for(CoverageReinsurance covRis : cov.getCoverageReinsurances()) {
				updateAdjustmentOnsetTransactionPremium(covRis.getCovReinsurancePremium(), numOfUnits, covRis.getIsFlatPremium());
			}
		}
	}

	private void updateAdjustmentOnsetTransactionEndorsementPremium(Endorsement end, Integer numOfUnits) {
		Premium premium = end.getEndorsementPremium();
		updateAdjustmentOnsetTransactionPremium(premium, numOfUnits, end.isFlatPremium());
		
		if (BooleanUtils.isTrue(end.isReinsured())) {
			for(EndorsementReinsurance endRis : end.getEndorsementReinsurances()) {
				updateAdjustmentOnsetTransactionPremium(endRis.getEndReinsurancePremium(), numOfUnits, endRis.getIsFlatPremium());
			}
		}
	}
	
	private void updateAdjustmentOnsetTransactionPremium(Premium premium, Integer numOfUnits, Boolean isFlatPremium) {
		if (numOfUnits == null)
			premium.setUnitPremium(null);
		else if (numOfUnits == NumberUtils.INTEGER_ZERO)
			premium.setUnitPremium(NumberUtils.DOUBLE_ZERO);
		//ES-175 make flat premium like other premium 
		// Flat Premium like unit premium, fixed amount for each unit
		//else if (BooleanUtils.isTrue(isFlatPremium))
			//premium.setUnitPremium(premium.getAnnualPremium());
		else 
			premium.setUnitPremium(premium.getAnnualPremium() / numOfUnits);
	}

	/**
	 * Retrieves the maxRetainedLimit in use for a given policy transaction.
	 * 
	 * @param pcyTxn the policy transaction
	 * @return the maxRetainedLimit to use
	 * @throws InsurancePolicyException if the product configuration cannot be
	 *                                  retrieved
	 */
	private Long getPolicyMaxRetainedLimit(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		/*
		 * This code is intended to be temporary. The intent is to allow LHT ON policies
		 * issued using the old (v1) ratebook to use the old maxRetainedLimit value of
		 * 2,000,000. Policies issued using v2 will use the current value of 5M instead.
		 * 
		 * See ES-106 for further information.
		 */
		if ("ONLHT".equalsIgnoreCase(pcyTxn.getPolicyTerm().getRatebookId())
				&& Objects.equals(1, pcyTxn.getPolicyTerm().getRatebookVersion())) {
			return 2_000_000l;
		}
		// End of ES-106 "maxRetainedLimit override" code

		final IGenericProductConfig productConfig = getProductConfig(pcyTxn);
		if (productConfig != null) {
			return (long) productConfig.getMaxRetainedLimit();
		} else {
			logger.log(Level.WARNING, "Could not retrieve product configuration for {0}",
					pcyTxn.getPolicyTerm().getInsurancePolicy().getProductCd());
			return null;
		}
	}

	// Overrides to use ES-106 "maxRetainedLimit override" logic
	@Override
	protected PolicyTransaction<?> createPrimaryReinsurance(PolicyTransaction<?> pcyTxn) throws InsurancePolicyException {
		final Long maxRetainedLimit = getPolicyMaxRetainedLimit(pcyTxn);
		final IGenericProductConfig productConfig = getProductConfig(pcyTxn);
		pcyTxn = createPolicyPrimaryReinsurance(pcyTxn, null, maxRetainedLimit, productConfig);
		return pcyTxn;
	}

	@Override
	protected CorePolicyRatingHelperFactory getPolicyRatingHelperFactory() {
		return TowingPolicyRatingHelperFactory.getInstance();
	}

	@Override
	protected Integer getSubPcyRiskCovNumOfUnits(Coverage cov, PolicyTransaction<?> inPcyTxn, SubPolicy<?> sp,
			Risk spr) {
		if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(spr.getRiskType())) {
			SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk) spr;
			return svr.getNumberOfUnits();
		} else {
			return super.getSubPcyRiskCovNumOfUnits(cov, inPcyTxn, sp, spr);
		}
	}

	@Override
	protected Integer getSubPcyRiskEndNumOfUnits(Endorsement end, PolicyTransaction<?> inPcyTxn, SubPolicy<?> sp,
			Risk spr) {
		if (SpecialtyAutoConstants.RISK_TYPE_SPEC_VEH.equalsIgnoreCase(spr.getRiskType())) {
			SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk) spr;
			return svr.getNumberOfUnits();
		} else {
			return super.getSubPcyRiskEndNumOfUnits(end, inPcyTxn, sp, spr);
		}
	}

	// If Premium is overridden 
	// - the unit premium = annual premium / unites
	// - rated premium cannot be calculated by any field
	// - call rating to get the rated premium
	private void updateSpecialtyAdjustmentRatedPremium(String adjustmentGroupdID, SpecialtyVehicleRisk vehicle, PolicyTransaction<?> policyTransaction, SpecialtyPolicyItemDTO vehicleItemDTO, UserProfile userProfile) throws SpecialtyAutoPolicyException {
		try {
			ratePolicyTransaction(policyTransaction, false, userProfile);
		} catch (InsurancePolicyException e) {
			throw new SpecialtyAutoPolicyException("Failed to rate adjustment", e);
		}
		
		// Update Vehicle
		updateAdjustmentRatedPremium(vehicle.getCoverageDetail(), null, vehicle.getSvrAdjustments());
		
		// Update CGL 
		updateSpecialtyAdjustmentSubPolicyRatedPremium(adjustmentGroupdID, policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY);

		// Update Cargo
		updateSpecialtyAdjustmentSubPolicyRatedPremium(adjustmentGroupdID, policyTransaction, SpecialtyAutoConstants.SUBPCY_CODE_CARGO);
	}
	
	private void updateSpecialtyAdjustmentSubPolicyRatedPremium(String adjustmentGroupdID, PolicyTransaction<?> policyTransaction, String subpolicyTypeCd) throws SpecialtyAutoPolicyException {
		Set<Coverage> coverages = new HashSet<Coverage>();
		Set<Endorsement> endorsements = new HashSet<Endorsement>();
		SubPolicy<?> subPcy = getAdjustmentSubPolicyItems(policyTransaction, subpolicyTypeCd, coverages, endorsements);
		if (subPcy != null) {
			ISpecialtyHasAdjustment adjContainer = (ISpecialtyHasAdjustment) subPcy;
			updateAdjustmentRatedPremium(subPcy.getCoverageDetail(), adjustmentGroupdID, adjContainer.getSvrAdjustments());
		}
	}
	
	private void updateAdjustmentRatedPremium(CoverageDetails sourceCovDetl, String adjGroupId, Set<SpecialtyVehicleRiskAdjustment> svrAdjustments) {
		// find the last adjustment
		SpecialtyVehicleRiskAdjustment lasAdj = null;
		List<SpecialtyVehicleRiskAdjustment> adjs = getAllLastAdjustment(svrAdjustments);
		if (adjs != null && !adjs.isEmpty()) {
			if (StringUtils.isNotBlank(adjGroupId)) {
				lasAdj = adjs.stream().filter(o -> adjGroupId.equalsIgnoreCase(o.getGroupID())).findFirst().orElse(null);
			}
			// For vehicle, there should be one
			else {
				lasAdj = adjs.get(0);
			}
		}
		
		// updatePendingAdjustmentRatedPremium handles annual premium
		if (lasAdj != null) {
			lasAdj.getCoverageDetail().getCoverages().forEach(adjCov -> {
				Coverage srcCov = sourceCovDetl.getCoverageByPk(adjCov.getOriginatingPK());
				if (srcCov != null) {
					adjCov.getCoveragePremium().setOriginalPremium(srcCov.getCoveragePremium().getOriginalPremium());
				}
			});
			
			lasAdj.getCoverageDetail().getEndorsements().forEach(adjEnd -> {
				Endorsement srcEnd = sourceCovDetl.getEndorsementByPk(adjEnd.getOriginatingPk());
				if (srcEnd != null) {
					adjEnd.getEndorsementPremium().setOriginalPremium(srcEnd.getEndorsementPremium().getOriginalPremium());
				}
			});
		}
	}
	
	@SuppressWarnings("unused")
	protected SubPolicy<?> getAdjustmentSubPolicyItems(PolicyTransaction<?> policyTransaction, String subpolicyTypeCd, Set<Coverage> coverages, Set<Endorsement> endorsements) throws SpecialtyAutoPolicyException {
		SubPolicy<?> subPolicy = null;
				
		Set<String> coverageIds = null;
		Set<String> endorsementIds = null;
		switch (subpolicyTypeCd) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			coverageIds = SpecialtyAutoConstants.ADJUSTMENT_COVERAGES_CGL;
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			coverageIds = SpecialtyAutoConstants.ADJUSTMENT_COVERAGES_CARGO;
			break;

		default:
			break;
		}
		
		subPolicy = policyTransaction.findSubPolicyByType(subpolicyTypeCd);
		if (subPolicy != null) {
			try {
				if (getCoverageService().needToLoadCoverageDetails(subPolicy.getCoverageDetail())) {
					subPolicy = (CGLSubPolicy<?>) loadSubPolicy(subPolicy, null);
				}
			} catch (InsurancePolicyException e1) {
				throw new SpecialtyAutoPolicyException("Could not load adjustment "+subpolicyTypeCd+" sub policy", e1);
			}
		
			final SubPolicy<?> wkSubPolicy = subPolicy;
			if (coverages != null && coverageIds != null && !coverageIds.isEmpty()) {
				coverageIds.stream().forEach(code -> {
					Coverage cov = wkSubPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode(code);
					if (cov != null) {
						coverages.add(cov);
					}
				});
			}
			
			if (endorsements != null && endorsementIds != null && !endorsementIds.isEmpty()) {
				endorsementIds.stream().forEach(code -> {
					Endorsement end = wkSubPolicy.getCoverageDetail().getActiveOrPendingEndorsementByCode(code);
					if (end != null) {
						endorsements.add(end);
					}
				});
			}
		}
		
		return subPolicy;
	}

	@SuppressWarnings("unchecked")
	protected void setAdjustmentSubpolicy(SubPolicy<?> subPcy, SpecialtyVehicleRiskAdjustment spAdj) {
		switch (subPcy.getSubPolicyTypeCd()) {
		case SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY:
			spAdj.setCglSubPolicy((CGLSubPolicy<Risk>) subPcy);
			break;
			
		case SpecialtyAutoConstants.SUBPCY_CODE_CARGO:
			spAdj.setCargoSubPolicy((CargoSubPolicy<Risk>) subPcy);
			break;

		default:
			break;
		}
	}
	
	protected void finializeSpecialtyAdjustmentSteps(SpecialtyVehicleRiskAdjustment currAdjustment, Integer currAdjustmentNum, String adjustmentGroupdID,
														SpecialtyVehicleRiskAdjustment nextAdjustment,
														SpecialtyVehicleRisk vehicle,
														PolicyTransaction<?> policyTransaction, 
														SpecialtyPolicyItemDTO vehicleItemDTO,
														UserProfile userProfile) throws SpecialtyAutoPolicyException {
		
		//VEHICLE - Update Subsequence Vehicle Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceVehicleAdjustments(nextAdjustment, policyTransaction, vehicleItemDTO);
		
		//VEHICLE - Update vehicle from adjustment
		consolidatePendingVehicleAdjustment(currAdjustmentNum, vehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		//VEHICLE - end vehicle adjustment
		updatePendingAdjustmentEnd(vehicle, policyTransaction, vehicleItemDTO, userProfile);

		//SUBPOLICY - create & update subpolicy adjustments
		if (currAdjustment != null) { // currAdjustment = null e.g.deleted 
			updatePendingSpecialtySubpolicyAdjustments(currAdjustment, policyTransaction, vehicleItemDTO);
		}
		
		//VEHICLE - Update Subsequence Subpolicy Adjustments e.g. units changed on prev adjustments
		updatePendingSubsequenceSubpolicyAdjustments(nextAdjustment, policyTransaction, vehicleItemDTO);
		
		//SUBPOLICY - Update subpolicies from adjustment
		consolidatePendingSubpolicyAdjustmentPremiums(currAdjustmentNum, policyTransaction, userProfile);
		
		//update rated premiums
		updateSpecialtyAdjustmentRatedPremium(adjustmentGroupdID, vehicle, policyTransaction, vehicleItemDTO, userProfile);
		
		//end adjustment processes 
		updateSpecialtyAdjustmentEnd(vehicle, policyTransaction, vehicleItemDTO, userProfile);
	}	
}
