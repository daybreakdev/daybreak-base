package com.echelon.processes.policy.nonfleet;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.processes.policy.specialitylines.SpecialtyAutoPolicyProcesses;
import com.echelon.prodconf.ConfigConstants;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelperFactory;
import com.echelon.rating.rounding.NonFleetRoundingStrategyFactory;
import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.rounding.RoundingStrategyFactory;
import com.rating.utils.CorePolicyRatingHelperFactory;
import com.ds.ins.domain.entities.Address;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;
import com.ds.ins.domain.policy.wheels.Driver;
import com.ds.ins.domain.policy.wheels.DriverConviction;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.exceptions.specialtylines.SpecialtyAutoPolicyException;
import com.ds.ins.system.UserProfile;
import com.ds.ins.utils.Constants;

public class NonFleetAutoProcesses extends SpecialtyAutoPolicyProcesses {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8201031721317774371L;
	
	final static Logger logger = Logger.getLogger(NonFleetAutoProcesses.class.getName());

	public NonFleetAutoProcesses() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void setDriverDefaults(Driver driver) throws InsurancePolicyException {
		// TODO Auto-generated method stub
		super.setDriverDefaults(driver);
		
		driver.setLicenseStatus(SpecialtyAutoConstants.DRV_LICENSE_STATUS_LICENSED);
		driver.setLicenseClass(SpecialtyAutoConstants.DRV_LICENSE_CLASS_G);
		
		Address address = new Address();
		driver.setDriverAddress(address);
		address.setCountry(Constants.COUNTRY_CANADA);
		address.setProvState(Constants.PROVINCE_ONTARIO);
		driver.setDriverAddress(address);
		driver.setLicenseProvState(Constants.PROVINCE_ONTARIO);
	}

	@Override
	protected void setRiskDefaults(Risk risk) throws InsurancePolicyException {
		super.setRiskDefaults(risk);
		if (risk instanceof SpecialtyVehicleRisk) {
			SpecialtyVehicleRisk svr = (SpecialtyVehicleRisk)risk;
			svr.setIsNonStandard(Boolean.FALSE);
		}
	}
	
	@Override
	public PolicyTransaction<?> preUpdatePolicyTransaction(PolicyTransaction<?> policyTransaction)
			throws InsurancePolicyException {
		// TODO Auto-generated method stub
		policyTransaction = super.preUpdatePolicyTransaction(policyTransaction);
		
		updatePolicyDrivers(policyTransaction);
		
		return policyTransaction;
	}

	@Override
	protected PolicyTransaction<?> updateRenewalOnsetTransaction(PolicyTransaction<?> inPcyTxn,
			Boolean resetPremOverride, UserProfile userProfile) throws InsurancePolicyException {
		// TODO Auto-generated method stub
		inPcyTxn = super.updateRenewalOnsetTransaction(inPcyTxn, resetPremOverride, userProfile);
		
		updatePolicyDrivers(inPcyTxn);
		
		return inPcyTxn;
	}

	//ENFO-241
	//this value should be created while driver is added and during policy renewal
	// -> Driver with business status = "NEW"
	@Override
	public PolicyTransaction<?> updateTermEffective(PolicyTransaction<?> pcyTxn, LocalDateTime termEffective) {
		// TODO Auto-generated method stub
		pcyTxn = super.updateTermEffective(pcyTxn, termEffective);
		
		updatePolicyDriversFields(pcyTxn, termEffective);
		
		return pcyTxn;
	}
	
	protected void updatePolicyDrivers(PolicyTransaction<?> policyTransaction) {
		LocalDateTime termEffective = policyTransaction.getPolicyTerm().getTermEffDate();
		updatePolicyDriversFields(policyTransaction, termEffective);
	}
	
	protected void updatePolicyDriversFields(PolicyTransaction<?> policyTransaction, LocalDateTime termEffective) {
		if (policyTransaction.getDrivers() != null && !policyTransaction.getDrivers().isEmpty()) {
			policyTransaction.getDrivers().stream().filter(o -> o.isPending() || o.isActive()).collect(Collectors.toList()).forEach(driver -> {
				updateDriverFields(driver, termEffective);
			});
		}
	}

	protected void updateDriverFields(Driver driver, LocalDateTime termEffective) {
		updateDriverConvictionFields(driver, termEffective);
		updateDriverClaimFields(driver, termEffective);
		updateDriverInsuranceHistoryAgeField(driver, termEffective);
	}
	
	protected void updateDriverInsuranceHistoryAgeField(Driver driver, LocalDateTime baseOnDate) {
		int insHistoryAge = 0;
		
		if (driver.getInsuredSince() != null) {
			insHistoryAge = Period.between(driver.getInsuredSince(), baseOnDate.toLocalDate()).getYears();	
		}
		
		driver.setInsuranceHistoryAge(insHistoryAge);
	}

	protected void updateDriverConvictionFields(Driver driver, LocalDateTime baseOnDate) {
		int count_conv_crim  = 0;
		int count_conv_major = 0;
		int count_conv_minor = 0;
		
		for(DriverConviction drc : driver.getDriverConvictions().stream().filter(o -> (o.isPending() || o.isActive() || o.getDriverConvictionPK() == null) 
																						&& isPass36Months(o.getConvictionDate(), baseOnDate)).collect(Collectors.toList())) {
			if (StringUtils.isNotBlank(drc.getConvictionType())) {
				switch (drc.getConvictionType()) {
				case SpecialtyAutoConstants.DRV_CONVICTION_TYPE_CRIMINAL:
					count_conv_crim++;
					break;

				case SpecialtyAutoConstants.DRV_CONVICTION_TYPE_MAJOR:
					count_conv_major++;
					break;

				case SpecialtyAutoConstants.DRV_CONVICTION_TYPE_MINOR:
					count_conv_minor++;
					break;

				default:
					break;
				}
			}
		}
		
		driver.setCriminalConvictions(count_conv_crim);
		driver.setMajorConvictions(count_conv_major);
		driver.setMinorConvictions(count_conv_minor);
	}

	protected void updateDriverClaimFields(Driver driver, LocalDateTime baseOnDate) {
		long at_fault_claims = driver.getDriverClaims().stream().filter(o -> (o.isPending() || o.isActive() || o.getDriverClaimPK() == null) 
												&& isPass36Months(o.getLossDate(), baseOnDate)
												&& SpecialtyAutoConstants.CHARAGABLE_CLAIMS_TYPE.equalsIgnoreCase(o.getClaimType())
												&& StringUtils.isNotBlank(o.getFaultResponsibility())
												&& SpecialtyAutoConstants.CHARAGABLE_CLAIMS_FAULTRESPONSIBILITIES.contains(o.getFaultResponsibility())
												).count();
		
		driver.setAtFaultClaims(Long.valueOf(at_fault_claims).intValue());
	}
	
	private boolean isPass36Months(LocalDate calcDate, LocalDateTime baseOnDate) {
		final int DIFFMONTHS = 36;
		
		if (calcDate == null) {
			return false;
		}
		
		if (calcDate.isAfter(baseOnDate.toLocalDate())) {
			return false;
		}
		
		LocalDate minDay = baseOnDate.toLocalDate().minusMonths(DIFFMONTHS);
		return !calcDate.isBefore(minDay);
	}

	@Override
	public List<String> getSubpolicyRatingWarnings(SubPolicy<?> subPolicy, List<Coverage> coverages,
			List<Endorsement> endorsements) throws SpecialtyAutoPolicyException {
		List<String> messages = new ArrayList<String>();
		
		if (isCGCovLWLLLimitsExceeded(subPolicy, coverages, endorsements)) {
			messages.add(SpecialtyAutoConstants.WARN_COV_WLL_LIMIT_MESSAGECODE);
		}
		
		return messages;
	}

	@Override
	public List<String> getPolicyActionWarnings(String action, PolicyTransaction<SpecialtyVehicleRisk> trans)
			throws SpecialtyAutoPolicyException {
		
		List<String> warns = super.getPolicyActionWarnings(action, trans);
		
		switch (action) {
		case ConfigConstants.LIFECYCLEACTION_Rating:
			if (!validateDriverSurcharged(trans)) {
				warns.add(SpecialtyAutoConstants.WARN_SURCHARGE_ON_VEHICLE);
			}
			break;

		default:
			break;
		}
		
		return warns;
	}

	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return NonFleetRoundingStrategyFactory.getInstance();
	}

	@Override
	protected CorePolicyRatingHelperFactory getPolicyRatingHelperFactory() {
		return NonFleetPolicyRatingHelperFactory.getInsance();
	}
	
}
