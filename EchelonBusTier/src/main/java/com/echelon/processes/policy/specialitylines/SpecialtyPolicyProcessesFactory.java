package com.echelon.processes.policy.specialitylines;

import org.apache.commons.lang3.StringUtils;

import com.ds.ins.processes.policy.GenericPolicyProcesses;
import com.ds.ins.processes.policy.PolicyProcessesFactory;
import com.echelon.processes.policy.LHT.LHTPolicyProcesses;
import com.echelon.processes.policy.nonfleet.NonFleetAutoProcesses;
import com.echelon.prodconf.ConfigConstants;

@SuppressWarnings("rawtypes")
public class SpecialtyPolicyProcessesFactory<PCYPRCS extends GenericPolicyProcesses<?, ?, ?, ?>> extends PolicyProcessesFactory<PCYPRCS> {	
	private static SpecialtyPolicyProcessesFactory instance = new SpecialtyPolicyProcessesFactory();
	
	public static SpecialtyPolicyProcessesFactory getInstance() {
		return instance;
	}
	
	private SpecialtyPolicyProcessesFactory() {
		//empty constructor
	}

	@SuppressWarnings("unchecked")
	@Override
	protected PCYPRCS createPolicyProcesses(String productCd) {
		if (StringUtils.isBlank(productCd)) {
			throw new RuntimeException("Can not create policy processes for blank (or null) insurance product code.");
		}
		if (ConfigConstants.PRODUCTCODE_TOWING.equalsIgnoreCase(productCd)) {
			return  (PCYPRCS) new SpecialtyAutoPolicyProcesses();
		}
		if (ConfigConstants.PRODUCTCODE_LHT.equalsIgnoreCase(productCd)) {
			return  (PCYPRCS) new LHTPolicyProcesses();
		} 
		if (ConfigConstants.PRODUCTCODE_TOWING_NONFLEET.equalsIgnoreCase(productCd)) {
			return  (PCYPRCS) new NonFleetAutoProcesses();
		} else {
			throw new RuntimeException("Unknown insurance product code.");
		}
	}
}
