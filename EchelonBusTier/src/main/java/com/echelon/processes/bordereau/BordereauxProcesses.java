package com.echelon.processes.bordereau;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.ds.ins.dal.dao.PolicyTransactionDAO;
import com.ds.ins.dal.dao.PolicyVersionAuditDAO;
import com.ds.ins.domain.dto.PolicyHistoryDTO;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.PolicyVersionAudit;
import com.ds.ins.exceptions.InsurancePolicyException;
import com.ds.ins.interfaces.policy.IGenericPolicyProcessing;
import com.ds.ins.utils.Constants;
import com.echelon.dal.dao.specialitylines.PolicyTransactionBrdDAO;
import com.echelon.processes.policy.specialitylines.SpecialtyPolicyProcessesFactory;

public class BordereauxProcesses implements Serializable {

	public BordereauxProcesses() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	protected IGenericPolicyProcessing getPolicyProcessing(String productCd) {
		return SpecialtyPolicyProcessesFactory.getInstance().newPolicyProcesses(productCd);
	}

	public List<PolicyTransaction> generateBordereauAll(PolicyTransaction<?> policyTransaction, LocalDate dateFrom, LocalDate dateTo, String delimiter, boolean summary) throws InsurancePolicyException {
		PolicyTransactionDAO dao = new PolicyTransactionDAO();
		List<PolicyTransaction> ptListAux=dao.getAllPolicyTransactions(policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum());
		List<PolicyHistoryDTO> ptHistoryList=dao.getPcyHistoryByPcyNumber(policyTransaction.getPolicyVersion().getInsurancePolicy().getBasePolicyNum());
		
		//Include only the transactions visible in the UI Transaction History view 
		List<PolicyTransaction> ptList = new ArrayList<PolicyTransaction>();
		for(PolicyTransaction polTransaction : ptListAux) {
			for(PolicyHistoryDTO polHistoryTransaction : ptHistoryList) {
				if (polHistoryTransaction.getPolicyTransactionPK().equals(polTransaction.getPolicyTransactionPK())) {
					ptList.add(polTransaction);
				}
			}
		}
		
		//Filter some records and upload child objects like coverages and endorsments
		ptList = ptList.stream().filter(o -> (o.getPolicyVersion().getVersionDate().isAfter(dateFrom)  && o.getPolicyVersion().getVersionDate().isBefore(dateTo)) || o.getPolicyVersion().getVersionDate().isEqual(dateFrom)).collect(Collectors.toList());
		for(PolicyTransaction<?> polTransaction : ptList) {
			if (!polTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.TERM_STATUS_IGNORED)
				&& !polTransaction.getPolicyVersion().getBusinessStatus().equals("SPOILED")
				&& !polTransaction.getPolicyVersion().getBusinessStatus().equals(Constants.BUSINESS_STATUS_DECLINED)) {
				
				Map<String, Boolean> paramsMap = new HashMap<String, Boolean>();
				paramsMap.put(Constants.LOAD_RATE_FACTORS_PARAM, false);
				dao.loadInstanceWithChildObjects(polTransaction, paramsMap);
				
			}
		}

		return ptList;
	}
	
	public List<PolicyTransaction> generateBordereauSummary(LocalDate dateFrom, LocalDate dateTo, String delimiter, List<String> programCode) throws InsurancePolicyException {
		//LocalDateTime timestampFrom = LocalDateTime.of(dateFrom, LocalTime.of(0, 0, 0, 0));
		//LocalDateTime timestampTo = LocalDateTime.of(dateTo, LocalTime.of(0, 0, 0, 0)).plusDays(1).minusSeconds(1);

		PolicyTransactionDAO dao = new PolicyTransactionDAO();
		PolicyTransactionBrdDAO daoBrd = new PolicyTransactionBrdDAO();
		//List<PolicyTransaction> ptList=daoBrd.getAllPoliciesRangeDates(timestampFrom, timestampTo, programCode);
		List<PolicyTransaction> ptList=daoBrd.getAllPoliciesRangeDates(dateFrom, dateTo, programCode);

		for(PolicyTransaction<?> polTransaction : ptList) {

			Map<String, Boolean> paramsMap = new HashMap<String, Boolean>();
			paramsMap.put(Constants.LOAD_RATE_FACTORS_PARAM, false);
			dao.loadInstanceWithChildObjects(polTransaction, paramsMap);

		}

		return ptList;
	}
	
	public List<PolicyTransaction> generateBordereauSummaryTemporal(LocalDate dateFrom, LocalDate dateTo, String delimiter, boolean summary, List<String> programCode) throws InsurancePolicyException {
		/*I'm creating this method temporarily because Sangeetha says that the summary report is not getting the same results it was getting before we implemented the product filtering and the performance improvement changes
		so I'm using the same code we had at the begging*/
		LocalDateTime timestampFrom = LocalDateTime.of(dateFrom, LocalTime.of(0, 0, 0, 0));
		LocalDateTime timestampTo = LocalDateTime.of(dateTo, LocalTime.of(0, 0, 0, 0)).plusDays(1).minusSeconds(1);

		PolicyTransactionDAO dao = new PolicyTransactionDAO();
		PolicyTransactionBrdDAO daoBrd = new PolicyTransactionBrdDAO();
		List<PolicyTransaction> ptList=daoBrd.getAllPoliciesRangeDatesTemporal(timestampFrom, timestampTo, programCode);
		
		List<PolicyTransaction> ptListIssued = ptList.stream().filter(
				o -> o.getPolicyVersion().getBusinessStatus().equals(Constants.BUSINESS_STATUS_ISSUED) || 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDRENEWAL) || 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGRENEWAL)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDCANCELLATION)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGCHANGE)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDCHANGE)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERS_TXN_TYPE_REVERSAL)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDREINSTATEMENT)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGREINSTATEMENT)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.BUSINESS_STATUS_REVERSED)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_ISSUEDREISSUE)|| 
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGREISSUE)|| 
				o.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_EXTENSION)||
				o.getPolicyVersion().getPolicyTxnType().equals(Constants.VERS_TXN_TYPE_ADJUSTMENT)||
				o.getPolicyVersion().getBusinessStatus().equals(Constants.VERSION_STATUS_PENDINGCANCELLATION)
				).collect(Collectors.toList());

		for(PolicyTransaction<?> polTransaction : ptListIssued) {

			Map<String, Boolean> paramsMap = new HashMap<String, Boolean>();
			paramsMap.put(Constants.LOAD_RATE_FACTORS_PARAM, false);
			dao.loadInstanceWithChildObjects(polTransaction, paramsMap);

		}
		
		return ptListIssued;
	}
	
	
	public List<PolicyTransaction> getBordereauReissueAndAdjustmentTransactions( String policyNo) throws InsurancePolicyException {

		List<PolicyTransaction> ptList = new ArrayList<PolicyTransaction>();
		
		if (policyNo != null && !policyNo.equals("")) {
			PolicyTransactionDAO dao = new PolicyTransactionDAO();
			PolicyTransactionBrdDAO daoBrd = new PolicyTransactionBrdDAO();
			ptList=daoBrd.getAllPolicyTransactionsReissueAdjustment(policyNo);
		
			for(PolicyTransaction<?> polTransaction : ptList) {
				Map<String, Boolean> paramsMap = new HashMap<String, Boolean>();
				paramsMap.put(Constants.LOAD_RATE_FACTORS_PARAM, false);
				dao.loadInstanceWithChildObjects(polTransaction, paramsMap);
			}
		}

		return ptList;

	}
	
	public List<PolicyTransaction> getBordereauTransactions( String policyNo) throws InsurancePolicyException {

		List<PolicyTransaction> ptList = new ArrayList<PolicyTransaction>();
		if (policyNo != null && !policyNo.equals("")) {
			PolicyTransactionDAO dao = new PolicyTransactionDAO();
			ptList=dao.getAllPolicyTransactions(policyNo);
			
			for(PolicyTransaction<?> polTransaction : ptList) {
				Map<String, Boolean> paramsMap = new HashMap<String, Boolean>();
				paramsMap.put(Constants.LOAD_RATE_FACTORS_PARAM, false);
				dao.loadInstanceWithChildObjects(polTransaction, paramsMap);
			}
			
		}
		return ptList;

	}
	
	/**
	 * Create audit entries to record that a bordereau has been generated for
	 * the given policy version.
	 * 
	 * @param policyTransactions The list of policy transactions contained in
	 *                           the generated bordereau.
	 */
	// TODO: parameterize policyTransactions to avoid using raw type
	private void auditBordereauGeneration(
		List<PolicyTransaction> policyTransactions) {
		String details = "Bordereau generated";
		assert details.length() <= 80 // db column is varchar(80)
			: "description must not be more than 80 characters long";

		List<PolicyVersionAudit> auditEntries = policyTransactions.stream()
			.map(transaction -> transaction.getPolicyVersion()).distinct()
			.map(version -> {
				PolicyVersionAudit audit = new PolicyVersionAudit();
				audit.setPolicyVersion(version);
				audit.setDescription(details);

				return audit;
			}).collect(Collectors.toList());
		PolicyVersionAuditDAO auditDao = new PolicyVersionAuditDAO();
		auditDao.saveAll(auditEntries);
	}
}
