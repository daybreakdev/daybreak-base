No Previous Insurance Company
ACE INA Insurance
Aetna Casualty
Alberta Motor Assoc Ins Co
Algoma Mutual Ins Co
Allianz Global Risk US Insurance Company
Allianz Insurance Company of Canada
Allstate Indemnity Insurance Co.
Allstate Insurance - Facility
Allstate Insurance Company of Canada
Alpha Compagnie D'Assurances Inc
Alpina Insurance Company Limited
American Home Assurance Company
American Road Insurance Company
Amherst Island Mutual Fire Insurance Company
Ascentus Insurance Ltd
Atlantic Insurance Company Limited
Aviva Insurance Company of Canada
AXA Insurance (Canada)
Ayr Farmers' Mutual Insurance Company
Bay of Quinte Agricultural Mutual Fire Insurance Company
Belairdirect Insurance Company
Bertie and Clinton Mutual Insurance Company
Blanshard Mutual Insurance Company
Blue Insurance Ltd.
Brant Mutual Insurance Company
CAA Insurance Company (Ontario)
Cabot Insurance Company Limited
Canada West Insurance Company
Canadian Commerce Insurance Company
Canadian Direct Insurance Inc
Canadian Farm Insurance
Canadian General Insurance Company - Facility Carrier
Canadian Millers' Mutual Insurance Company
Canadian Northern Shield Insurance Company
Canadian Provincial Insurance Company
Canadian Surety Company
Canassurance General Insurance Co. Inc.
Capitale, Compagnie D'Assurance Generale, La
Caradoc Delaware Mutual Fire Insurance Company
Cayuga Mutual Fire Insurance Company
Centennial Insurance Company
Certas Direct Insurance Company (formely CIBC)
CGU International Insurance plc
Chieftain Insurance Company
Chubb Insurance Company of Canada
Citadel General Assurance Company
Clairvoyants Compagnie D'Assurance Generale Inc, Les.
Coachman Insurance Company
Colonial Fire and General Insurance Company
Commerce and Industry Insurance Company of Canada
Commonwealth Insurance Company
Continental Casualty Company
Continental Insurance Company
Cooperants, Compagnie D'Assurance Generale, Les
Co-operators General - Facility Carrier
Co-operators General Insurance Company
COSECO Insurance Company
Crum and Forster of Canada Ltd.
Culross Mutual Insurance Company
CUMIS General Insurance Company
DaimlerChrysler Insurance Company
Desjardins Assurances Generales Inc.
DirectProtectGroup
Downie Mutual Insurance Company
Dufferin Mutual Insurance Company
Dumfries Mutual Insurance Company
East Williams Mutual Fire Insurance Company
Ecclesiastical Insurance Office plc
Echelon Insurance Company
Economical Mutual Insurance Company
Ekfrid Mutual Insurance Company
Electric Company
Elite Insurance Company
Elma Mutual Insurance Company
Employers Insurance Company of Wausau
Encon Group Inc.
Erie Mutual Fire Insurance Company
Exclusive Compagnie D'Assurance General
Facility Carrier (previously Allianz)
Farmer's Mutual Insurance Company (Lindsay)
Federal Insurance Company
Federated Insurance Company of Canada
Federated Mutual Insurance Company
Federation Insurance Company of Canada
Fenchurch General Insurance Company
Fidelity Insurance Company of Canada
Fire Insurance Company of Canada
Formosa Mutual Insurance Company
Fortress Insurance Company
GAN Canada
GAN Canada Insurance Company - Facility Carrier
GAN General Insurance Company
GCAN Insurance Company
General Accident Indemnity Company
Gerling Global General Insurance Company
Germania Farmers' Mutual Fire Insurance Company
Glengarry Farmers' Mutual Fire Insurance Company
Gore Mutual Insurance Company
Grain Insurance and Guarantee Company
Great American Insurance Company
Grenville Mutual Insurance Company
Grey and Bruce Mutual Insurance Company
Groupe Estrie-Richelieu Compagnie D'Assurance, (Le)
Guarantee Company of North America
Halwell Mutual Insurance Company
Hamilton Twp. Farmers' Mutual Fire Insurance Company
Hartford Fire Insurance Company
Hay Mutual Insurance Company
Home Insurance Company
Howard Mutual Fire Insurance Company
Howick Mutual Insurance Company
Hutterian Brethren General Insurance Corporation
ICBC
Industrielle-Alliance, Compagnie D'Assurances Generales
ING Insurance Company of Canada
ING Wellington Insurance Company
Insurance Company of Prince Edward Island
Insurance Corporation of Newfoundland - Facility Carrier
Insurance Corporation of Newfoundland Limited
Intact Insurance Company
Jevco Insurance Company
Kemper Canada
Kent and Essex Mutual Insurance Company
Kingsway General Insurance Company
L and A Mutual Fire Insurance Company
L.E. Yingst
La Personnell, Assurances Generales inc
Lambton Mutual Insurance Company
Lanark Mutual Insurance Company
Le Groupe Commerce Compagnie D'Assurances
Liberty Mutual Insurance Company
Lloyd's Underwriters
Lobo Mutual Insurance Company
Lombard Insurance Company
London Twp. Mutual Insurance Company
Lowndes Capp Program
Markel Insurance Company of Canada
Markham General Insurance
Marsh Canada Ltd.
MAX Canada Insurance Company
McGillivray Mutual Insurance Company
McKillop Mutual Insurance Company
Mennonite Mutual Fire Insurance Company
Metro General Insurance Corp Ltd.
Middlesex Mutual Insurance Company
Millennium Insurance Corporation
Missisquoi Insurance Company
Mitsui Sumitomo Insurance Company Limited
Motors Insurance Corporation
MPIC
Municipal Electric Association Reciprocal Insurance Exchange
National Frontier Insurance Company
Niagara Fire Insurance Company
Nipponkoa Insurance Company Limited
Nordic Insurance Company of Canada
Nordic Insurance Company of Canada - Facility
Norfolk Mutual Insurance Company
Norman Insurance Company Limited
North Blenheim Mutual Insurance Company
North Kent Mutual Fire Insurance Company
Nova Scotia General - Facility Carrier
Old Republic Insurance Company of Canada
Ontario Municipal Insurance Exchange
Ontario Mutual Insurance Association
Ontario School Boards' Insurance Exchange
Optimum Assurance Agricole
Optimum Insurance Company
Optimum Societe D'Assurance Inc
Optimum West Insurance Company
Other
Oxford Mutual Insurance Company
Pafco Insurance Company Ltd
Peace Hills General Insurance Company
Peel and Maryborough Insurance Company
Peel Mutual Insurance Company
Pembridge Insurance Company
Peopleplus Insurance Company
Personal Insurance Company of Canada
Perth Insurance Company
Pilot Insurance Company
Portage la Prairie Mutual Insurance Company 
Premier Marine Insurance
Prescott Mutual Insurance Company
Primmum Insurance Company
Progressive Casualty Insurance Company of Canada
Promutuel Appalaches, SMAG
Promutuel Bagot, SMAG
Promutuel Beauce, SMAG
Promutuel Bellechasse, SMAG
Promutuel Bois-Francs, SMAG
Promutuel Charlevoix-Montmorency, SMAG
Promutuel Coaticook-Sherbrooke, SMAG
Promutuel de l'Est, SMAG
Promutuel Deux-Montagnes, SMAG
Promutuel Dorchester, SMAG
Promutuel Drummond, SMAG
Promutuel Frontenac, SMAG
Promutuel Gaspesie Les Iles, SMAG
Promutuel Haut St-Laurent, SMAG
Promutuel Kamouraska, SMAG
Promutuel la Mauricienne, SMAG
Promutuel la Portneuvienne, SMAG
Promutuel la Vallee, SMAG
Promutuel l'Abitibienne, SMAG
Promutuel Lac St-Jean, SMAG
Promutuel Lac St-Pierre, SMAG
Promutuel Lanaudiere, SMAG
Promutuel les Prairies, SMAG
Promutuel Levisienne-Orleans, SMAG
Promutuel l'Islet, SMAG
Promutuel Lotbiniere, SMAG
Promutuel l'Outaouais, SMAG
Promutuel Montmagny, SMAG
Promutuel Reassurance
Promutuel Riviere-du-Loup, SMAG
Promutuel Rouyn-Noranda-Temiscamingue, SMAG
Promutuel Saguenay, SMAG
Promutuel Solanges, SMAG
Promutuel Temiscouata, SMAG
Promutuel Val St-Francois, SMAG
Promutuel Valmont, SMAG
Promutuel Vaudreuil, SMAG
Promutuel Vercheres, SMAG
Protective Insurance Company
Quebec Assurance Company
RBC General Insurance Co.
Red River Valley Mutual Insurance Company
Royal and SunAlliance - Facility Carrier
Royal and SunAlliance Insurance Company of Canada
S and Y Insurance Company
SAFECO Insurance Company of America
SARM Program
Saskatchewan Mutual Insurance Company
Scotia General Insurance Company
Scottish and York Insurance Company Limited
Securite Nationale, compagnie d'assurance
Security Insurance Company of Hartford
Selecta national d'assurances Inc.
Sentry Insurance, A Mutual Company
SGI Canada
Sompo Japan Insurance Inc. (formerly Yasuda Fire)
Sonnet Insurance
South Easthope Mutual Insurance Company
Sovereign General Insurance Company
SSQ, Societe D'Assurances Generales inc.
St Maurice, Compagnie D'Assurance
St. Paul Fire and Marine Insurance Company
State Farm Fire and Casualty Company
State Farm Mutual - Facility Carrier
State Farm Mutual Automobile Insurance Company
SUMA Program
TD Home and Auto Insurance Company
TD Home and Auto Insurance Company - Facility Carrier
Temple Insurance Company
The Canada Accident Assurance Company of Canada
The Dominion of Canada General Insurance Company
The Dominion of Canada General Insurance Company - Facility
TIG Insurance Company
Tokio Marine and Nichido Fire Insurance Company Limited
Toronto Dominion Insurance Company
Town and Country Mutual Insurance Company
Townsend Farmers' Mutual Fire Insurance Company
Traders General Insurance Company
Tradition Mutual Insurance Company
Trafalgar Insurance Company of Canada
Trans Canada Risk
Travelers Casualty and Surety Company of Canada
Trillium Mutual Insurance Company
Trisura Guarantee Insurance Company
Unica Insurance
Unifund Assurance - Facility Carrier
Unifund Assurance Company
Union Canadienne, Compagnie d'assurances
Unique, Compagnie d'assurances generales
United General Insurance Corporation
United States Fire Insurance Company
Usborne and Hibbert Mutual Fire Insurance Company
Utica Mutual Insurance Company
Victoria Insurance Company of Canada
Wabisa Mutual Fire Insurance Company
Waterloo Insurance Company
Wawanesa Mutual Insurance Company
West Elgin Mutual Insurance Company
West Wawanosh Mutual Insurance Company
Western Assurance Company
Western General Mutual Insurance Company
Westminster Mutual Insurance Company
XL Insurance Company
Yarmouth Mutual Fire Insurance Company
York Fire and Casualty Insurance Company
Zenith Insurance Company
Zurich Insurance Company