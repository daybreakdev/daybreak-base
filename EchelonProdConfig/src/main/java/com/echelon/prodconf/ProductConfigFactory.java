package com.echelon.prodconf;

import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;

public class ProductConfigFactory extends BaseProductConfigFactory {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8093910850058174042L;
	private static ProductConfigFactory instance;

	public ProductConfigFactory() {
		super();
	}

	public static ProductConfigFactory getInstance() {
		if (instance == null) {
			synchronized (ProductConfigFactory.class) {
				instance = new ProductConfigFactory();
			}
		}
		
		return instance;
	}

}
