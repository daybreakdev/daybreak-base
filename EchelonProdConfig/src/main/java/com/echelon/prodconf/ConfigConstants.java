package com.echelon.prodconf;

import com.ds.ins.prodconf.baseclasses.DSConfigConstants;

public class ConfigConstants extends DSConfigConstants {
	
	public static final String PRODUCTCODE_TOWING 			= "TOWING";
	public static final String PRODUCTCODE_TOWING_NONFLEET 	= "NONFLEET";
	public static final String PRODUCTCODE_LHT 				= "LHT";
	
	public static final String LIFECYCLEACTION_Rating 					= "Rating";
	public static final String LIFECYCLEACTION_DeclineQuote 			= "DeclineQuote";
	public static final String LIFECYCLEACTION_NotRequiredQuote 		= "NotRequiredQuote";
	public static final String LIFECYCLEACTION_CopyQuote 				= "CopyQuote";
	public static final String LIFECYCLEACTION_IssueQuote 				= "IssueQuote";
	public static final String LIFECYCLEACTION_BindQuote 				= "BindQuote";
	public static final String LIFECYCLEACTION_IssuePolicy 				= "IssuePolicy";
	public static final String LIFECYCLEACTION_WithdrawQuote 			= "WithdrawQuote";
	public static final String LIFECYCLEACTION_UndoDeclineQuote 		= "UndoDeclineQuote";
	public static final String LIFECYCLEACTION_UndoNotRequiredQuote 	= "UndoNotRequiredQuote";
	public static final String LIFECYCLEACTION_PolicyChange 			= "PolicyChange";
	public static final String LIFECYCLEACTION_DeclinePolicyChange 		= "DeclinePolicyChange";
	public static final String LIFECYCLEACTION_UndoDeclinePolicyChange 	= "UndoDeclinePolicyChange";
	public static final String LIFECYCLEACTION_SpoilPolicyChange 		= "SpoilPolicyChange";
	public static final String LIFECYCLEACTION_IssuePolicyChange 		= "IssuePolicyChange";
	public static final String LIFECYCLEACTION_Reversal 				= "Reversal";
	public static final String LIFECYCLEACTION_Renewal 					= "Renewal";
	public static final String LIFECYCLEACTION_QuoteRenewal 			= "QuoteRenewal";
	public static final String LIFECYCLEACTION_BindRenewal 				= "BindRenewal";
	public static final String LIFECYCLEACTION_IssueRenewal 			= "IssueRenewal";
	public static final String LIFECYCLEACTION_DeclineRenewal 			= "DeclineRenewal";
	public static final String LIFECYCLEACTION_UndoDeclineRenewal 		= "UndoDeclineRenewal";
	public static final String LIFECYCLEACTION_SpoilRenewal 			= "SpoilRenewal";
	public static final String LIFECYCLEACTION_CopyQuoteRenewal			= "CopyQuoteRenewal";
	public static final String LIFECYCLEACTION_Cancel 					= "Cancel";
	public static final String LIFECYCLEACTION_DeclineCancel 			= "DeclineCancel";
	public static final String LIFECYCLEACTION_IssueCancel 				= "IssueCancel";
	public static final String LIFECYCLEACTION_Reinstate 				= "Reinstate";
	public static final String LIFECYCLEACTION_IssueReinstate			= "IssueReinstate";
	public static final String LIFECYCLEACTION_DeclineReinstate			= "DeclineReinstate";
	public static final String LIFECYCLEACTION_Reissue 					= "Reissue";
	public static final String LIFECYCLEACTION_IssueReissue				= "IssueReissue";
	public static final String LIFECYCLEACTION_DeclineReissue			= "DeclineReissue";
	public static final String LIFECYCLEACTION_Extension 				= "Extension";
	public static final String LIFECYCLEACTION_IssueExtension			= "IssueExtension";
	public static final String LIFECYCLEACTION_DeclineExtension			= "DeclineExtension";
	public static final String LIFECYCLEACTION_SpoilExtension			= "SpoilExtension";
	public static final String LIFECYCLEACTION_Adjustment				= "Adjustment";
	public static final String LIFECYCLEACTION_IssueAdjustment			= "IssuedAdjustment";
	public static final String LIFECYCLEACTION_DeclineAdjustment		= "DeclineAdjustment";
	
	public static final String LOOKUPTABLE_Genders						= "Genders";
	public static final String LOOKUPTABLE_Languages					= "Languages"; 
	public static final String LOOKUPTABLE_MaritalStatuses				= "MaritalStatuses"; 
	public static final String LOOKUPTABLE_Countries					= "Countries"; 
	public static final String LOOKUPTABLE_fleetbasis					= "fleetbasis"; 
	public static final String LOOKUPTABLE_DrivingRecord				= "DrivingRecord"; 
	public static final String LOOKUPTABLE_LiabLimitList				= "LiabLimitList"; 
	public static final String LOOKUPTABLE_DeductLimitList				= "DeductLimitList"; 
	public static final String LOOKUPTABLE_CargoLimitList				= "CargoLimitList"; 
	public static final String LOOKUPTABLE_CargoDeductLimitList			= "CargoDeductLimitList"; 
	public static final String LOOKUPTABLE_TrailerType					= "TrailerType"; 
	public static final String LOOKUPTABLE_VehClassList					= "VehClassList"; 
	public static final String LOOKUPTABLE_VehDesc						= "VehDesc";
	public static final String LOOKUPTABLE_VehicheRateGroupList			= "VehicheRateGroupList"; 
	public static final String LOOKUPTABLE_VehMakeList					= "VehMakeList"; 
	public static final String LOOKUPTABLE_VehMakeListTrailers			= "VehMakeListTrailers"; 
	public static final String LOOKUPTABLE_VehType						= "VehType"; 
	public static final String LOOKUPTABLE_VehUseList					= "VehUseList"; 
	public static final String LOOKUPTABLE_VehYearsList					= "VehYearsList"; 
	public static final String LOOKUPTABLE_UnitExposure					= "UnitExposure";
	public static final String LOOKUPTABLE_BusinessDescriptions			= "BusinessDescriptions"; 
	public static final String LOOKUPTABLE_LienLessorType				= "LienLessorType"; 
	public static final String LOOKUPTABLE_DocId						= "DocId"; 
	public static final String LOOKUPTABLE_DistTarget					= "DistTarget"; 
	public static final String LOOKUPTABLE_BusinessStatus				= "BusinessStatus"; 
	public static final String LOOKUPTABLE_GarageBusinessDescList		= "GarageBusinessDescList"; 
	public static final String LOOKUPTABLE_FileType						= "FileType"; 
	public static final String LOOKUPTABLE_EndorsementCodes				= "EndorsementCodes"; 
	public static final String LOOKUPTABLE_EndorsementDescriptions		= "EndorsementDescriptions";
	public static final String LOOKUPTABLE_QuotePreparedByList			= "QuotePreparedByList"; 
	public static final String LOOKUPTABLE_CAAClubList					= "CAAClubList"; 
	public static final String LOOKUPTABLE_SchVehBody					= "SchVehBody";
	public static final String LOOKUPTABLE_PreventionRatings			= "PreventionRatings";
	public static final String LOOKUPTABLE_CommCreditScores				= "CommCreditScores";
	public static final String LOOKUPTABLE_TypeOfUnits 					= "TypeOfUnits";
	public static final String LOOKUPTABLE_IRPs 						= "IRPs";
	public static final String LOOKUPTABLE_RateFactorExcludeList		= "RateFactorExcludeList";
	public static final String LOOKUPTABLE_AttachedDriverType			= "AttachedDriverType";
	public static final String LOOKUPTABLE_LicenseClass					= "LicenseClass";
	public static final String LOOKUPTABLE_LicenseStatus				= "LicenseStatus";
	public static final String LOOKUPTABLE_ClaimFault					= "ClaimFault";
	public static final String LOOKUPTABLE_ClaimType					= "ClaimType";
	public static final String LOOKUPTABLE_ConvictionCode				= "ConvictionCode";
	public static final String LOOKUPTABLE_ConvictionType				= "ConvictionType";
	public static final String LOOKUPTABLE_SuspensionReason				= "SuspensionReason";
	public static final String LOOKUPTABLE_SuspensionType				= "SuspensionType";
	public static final String LOOKUPTABLE_PreviousInsuranceCompany		= "PreviousInsuranceCompany";

	// Lifecycle
	public static final String LOOKUPTABLE_DeclineReasons				= "DeclineReasons"; 
	public static final String LOOKUPTABLE_CancellationTypes			= "CancellationTypes"; 
	public static final String LOOKUPTABLE_CancellationReasons			= "CancellationReasons";
	public static final String LOOKUPTABLE_ExtensionDays				= "ExtensionDays";

	public static final String TREENODETYPE_CargoSubPolicyNode			= "CargoSubPolicyNode";
}
