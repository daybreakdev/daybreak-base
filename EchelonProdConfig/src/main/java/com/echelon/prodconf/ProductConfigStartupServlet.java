package com.echelon.prodconf;

import javax.servlet.annotation.WebListener;

import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.prodconf.helpers.BaseProductConfigStartupServlet;

@WebListener
public class ProductConfigStartupServlet extends BaseProductConfigStartupServlet {

	public ProductConfigStartupServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		// TODO Auto-generated method stub
		return ProductConfigFactory.getInstance();
	}

}
