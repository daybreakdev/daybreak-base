package com.echelon.prodconf.test;

import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Assertions;

import com.ds.ins.prodconf.baseclasses.BaseProductConfig;
import com.ds.ins.prodconf.baseclasses.DataDefinitionConfig;
import com.ds.ins.prodconf.baseclasses.DataExtensionConfig;
import com.ds.ins.prodconf.baseclasses.LookupTableRelation;
import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.ds.ins.prodconf.interfaces.IGenericLookupConfig;
import com.ds.ins.prodconf.interfaces.IGenericProductConfig;
import com.ds.ins.prodconf.policy.CoverageConfig;
import com.ds.ins.prodconf.policy.DeductibleConfig;
import com.ds.ins.prodconf.policy.EndorsementConfig;
import com.ds.ins.prodconf.policy.LimitConfig;
import com.ds.ins.prodconf.policy.RiskConfig;
import com.ds.ins.prodconf.policy.SubPolicyConfig;
import com.echelon.prodconf.ConfigConstants;

public class testBaseProductConfig {

	@Test
	void testProductConfig() throws Exception {
		IGenericProductConfig prodConfig = (new BaseProductConfigFactory()).getProductConfig("TOWING", "1.0.0");
		Assertions.assertNotNull(prodConfig);
		Assertions.assertNotNull(prodConfig.getProductCd());
		System.out.println("ProductCd:"+prodConfig.getProductCd());
		//System.out.println(prodConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_ProductCd));
		
		IGenericLookupConfig lookupConfig = (new BaseProductConfigFactory()).getLookupConfig("TOWING", LocalDate.now());
		/*
		prodConfig.getCountryValues().forEach(System.out::println);
		System.out.println();
		prodConfig.getGenderValues().forEach(System.out::println);
		System.out.println();
		prodConfig.getJurisdictions().forEach(System.out::println);
		System.out.println();
		prodConfig.getLanguageValues().forEach(System.out::println);
		System.out.println();
		prodConfig.getMaritalStatusValues().forEach(System.out::println);
		*/
		
		System.out.println("getJurisdictions");
		prodConfig.getJurisdictions().forEach(System.out::println);
		System.out.println();		
		//prodConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_Jurisdictions).forEach(System.out::println);
		
		System.out.println();
		System.out.println("TermLengths");
		prodConfig.getTermLengths().forEach(System.out::println);
		System.out.println();
		//prodConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_TermLengths).forEach(System.out::println);
		
		Assertions.assertNotNull(prodConfig);
		Assertions.assertFalse(prodConfig.getSubPolicyConfigs().isEmpty());
		System.out.println();
		System.out.println("SubPolicyConfigs");
		prodConfig.getSubPolicyConfigs().forEach(o -> System.out.println(o.getSubPolicyCode()));
		prodConfig.getSubPolicyConfigs().forEach(o -> {
			o.getCoverageConfigs().forEach(c -> System.out.println(c.getCoverageDescription()));
		});
		prodConfig.getSubPolicyConfigs().forEach(o -> {
			o.getEndorsementConfigs().forEach(c -> System.out.println(c.getEndorsementDescription()));
		});
		
		System.out.println();
		System.out.println("Fleetbasis");
		lookupConfig.getConfigLookup(ConfigConstants.LOOKUPTABLE_fleetbasis).forEach(System.out::println);
		
		LookupTableRelation relation = lookupConfig.getLookupRelation(ConfigConstants.LOOKUPTABLE_VehClassList);
		Assertions.assertNotNull(relation);
		List<String> values = relation.getValues("CM","TT");
		Assertions.assertNotNull(values);
		Assertions.assertFalse(values.isEmpty());
		values.forEach(System.out::println);
	}
	
	@Test
	void testBuildConfigJSON() throws Exception {
		BaseProductConfig prodConfig = new BaseProductConfig();
		RiskConfig riskConfig = new RiskConfig();
		CoverageConfig coverageConfig = new CoverageConfig();
		EndorsementConfig endorsementConfig = new EndorsementConfig();
		SubPolicyConfig subPolicyConfig = new SubPolicyConfig();
		LimitConfig limitConfig = new LimitConfig();
		DeductibleConfig deductibleConfig = new DeductibleConfig();
		DataExtensionConfig dataExtensionConfig = new DataExtensionConfig();
		DataDefinitionConfig dataDefinitionConfig = new DataDefinitionConfig();

		dataExtensionConfig.getDataDefinitionConfigs().add(dataDefinitionConfig);
		
		coverageConfig.getLimitConfigs().add(limitConfig);
		coverageConfig.getDeductibleConfigs().add(deductibleConfig);
		coverageConfig.getDataExtensionConfigs().add(dataExtensionConfig);
		
		endorsementConfig.getLimitConfigs().add(limitConfig);
		endorsementConfig.getDeductibleConfigs().add(deductibleConfig);
		endorsementConfig.getDataExtensionConfigs().add(dataExtensionConfig);
		
		subPolicyConfig.getCoverageConfigs().add(coverageConfig);
		subPolicyConfig.getEndorsementConfigs().add(endorsementConfig);
		subPolicyConfig.getRiskTypes().add(riskConfig);
		
		prodConfig.getRiskTypes().add(riskConfig);
		riskConfig.getCoverageConfigs().add(coverageConfig);
		riskConfig.getEndorsementConfigs().add(endorsementConfig);
		
		prodConfig.getSubPolicyConfigs().add(subPolicyConfig);
		prodConfig.getCoverageConfigs().add(coverageConfig);
		prodConfig.getEndorsementConfigs().add(endorsementConfig);
	
		com.fasterxml.jackson.databind.ObjectMapper mapper = new com.fasterxml.jackson.databind.ObjectMapper();
		com.fasterxml.jackson.datatype.jsr310.JavaTimeModule javaTimeModule = new com.fasterxml.jackson.datatype.jsr310.JavaTimeModule();
		javaTimeModule.addDeserializer(LocalDate.class, new com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
		javaTimeModule.addSerializer(LocalDateTime.class, new com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss")));
		mapper.registerModule(javaTimeModule);
		//mapper.setSerializationInclusion(com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL);
		String result = mapper.writeValueAsString(prodConfig);
		System.out.println(result);
	}
	
	@Test
	void testScan() {
		Reflections baseFoders = new Reflections("com.ds.ins.prodconf.baseclasses");
				/*
					new Reflections(new ConfigurationBuilder()
			     					.setUrls(ClasspathHelper.forPackage("com.ds.ins.prodconf.baseclasses"))
			     					.setScanners(
			     							new SubTypesScanner(), 
			     							new TypeAnnotationsScanner()
			     							)
			     					);
			     					*/
		Assertions.assertNotNull(baseFoders);
		//Set<Field> fields = baseFoders.getFieldsAnnotatedWith(com.ds.ins.prodconf.annotations.LookupTableField.class);
		//Assertions.assertNotNull(fields);
		//Assertions.assertFalse(fields.isEmpty());
		Set<Class<?>> classes = null; //baseFoders.getTypesAnnotatedWith(com.ds.ins.prodconf.annotations.ProductConfigDef.class);
		Assertions.assertNotNull(classes);
		Assertions.assertFalse(classes.isEmpty());
		classes.forEach(o -> {
			try {
				System.out.println(o.getName());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
	@Test
	void testDays() {
		LocalDate today   = LocalDate.of(2023, 8, 24); //LocalDate.now();
		LocalDate calDay  = LocalDate.of(2020, 7, 24);
		LocalDate passDay = today.minusMonths(36);
		
		int years  = Period.between(calDay, today).getYears();
		int months = Period.between(calDay, today).getMonths();
		int days   = Period.between(calDay, today).getDays();
		int answer = months + (years * 12) + (days > 0 ? 1 : (days < 0 ? - 1 : 0));
		
		System.out.println("Minus 36 months:"+passDay);
		System.out.println("Years:"+years);
		System.out.println("Months:"+months);
		System.out.println("Days:"+days);
		System.out.println("Cal Months:"+answer);
		
		System.out.println("ChronoUnit:"+ChronoUnit.MONTHS.between(calDay, today));
	}
}
