package com.rating.test;

import static org.junit.Assert.assertNotNull;

import java.io.InputStream;

import org.junit.jupiter.api.Test;

public class TestRatebookConfig {

	@Test
	void testLoad() {
		InputStream inputStream = null;
		try {
			inputStream = this.getClass().getClassLoader().getResourceAsStream("SIMRatebookConfig.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		assertNotNull(inputStream);
	}
}
