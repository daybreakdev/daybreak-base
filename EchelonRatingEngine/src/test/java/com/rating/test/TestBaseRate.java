package com.rating.test;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

import com.ds.ins.domain.policy.PolicyCoverage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.rating.towing.TowingPolicyRateProcessVer1;

class TestBaseRate {
	TowingPolicyRateProcessVer1 	rateprocess = new TowingPolicyRateProcessVer1();
	PolicyTransaction				policytxn;

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
        try {
            // load up the knowledge base
	        KieServices ks = KieServices.Factory.get();
    	    KieContainer kContainer = ks.getKieClasspathContainer();
        	KieSession kSession = kContainer.newKieSession("towingRating");
            
        	// Test Data
        	PolicyCoverage cov = new PolicyCoverage();
        	cov.setCoverageCode("BI");
        	cov.setLimit1Type("ON");
        	cov.setLimit2Type("PP");
        	cov.setDeductible1Type("S1");
        	Premium prem = new Premium();
        	cov.setCoveragePremium(prem);
        	cov.setRateFactorsLink(new RateFactorsLink());
        	kSession.insert(cov);
            // go !
            int fired = kSession.fireAllRules();
            System.out.println("Number of Rules Fired:" + fired);
            System.out.println("Premium : " + prem.getOriginalPremium());
            System.out.println("Limit : " + cov.getLimit1());
            RateFactor rf = cov.getRateFactorsLink().getRateFactors().iterator().next();
            System.out.println("Factors : " + rf.getRateFactorId() + " - " + rf.getRateFactorNumericValue());
            System.out.println("# Factors : " + cov.getRateFactorsLink().getRateFactors().size());
        } catch (Throwable t) {
            t.printStackTrace();
        }	
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {

	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void test() {
		//rateprocess.ratePolicyTransaction(new PolicyTransaction());
		System.out.println("Did something");
	}

}
