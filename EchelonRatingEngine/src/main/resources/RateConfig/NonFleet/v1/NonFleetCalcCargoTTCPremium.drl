package com.echelon.rating.calc.nonfleet.cgo

import com.ds.ins.domain.policy.Coverage
import com.ds.ins.domain.policy.Endorsement
import com.ds.ins.domain.policy.RateFactor
import java.math.BigDecimal;

import com.echelon.rating.nonfleet.cgo.NonFleetCGORateConstants
import com.echelon.domain.policy.specialtylines.CargoSubPolicy

import com.echelon.utils.SpecialtyAutoConstants
import com.echelon.rating.PolicyRatingHelper
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelperFactory

declare TTCPremCalcFactors 
	endorsementCd		: String
	ttcLimit			: double
	cargoTtcBase		: double
	ttcDays				: int	
	ttcSurchDisc		: double
end

function Double calcTtcPremiumByFactor(double premium, double factor) {
	//return PolicyRatingHelper.getInstance().calcPremium(premium, factor);
	return NonFleetPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(premium, factor);	
}

//limit to a list of coverages
rule "NonFleet-Calc-CGO-TTC-Premium-BEGIN" extends "NonFleet-Calc-CGO-TTC-BASE"
when
	not (TTCPremCalcFactors(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC))
then
	TTCPremCalcFactors fact = new TTCPremCalcFactors();
	fact.setEndorsementCd(SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC);
	fact.setTtcLimit(0);
	fact.setCargoTtcBase(0);
	fact.setTtcDays(0);	
	fact.setTtcSurchDisc(0);
	insert(fact);

end

//variable $fact for the rules 
rule "NonFleet-Calc-CGO-TTC-Premium-BASE" extends "NonFleet-Calc-CGO-TTC-BASE"
	when
		$fact : TTCPremCalcFactors(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC)
	then
		//System.out.println("NonFleet-Calc-CGO-TTC-Premium-BASE:"+$endttc.getEndorsementCd());
end

rule "NonFleet-Calc-CGO-TTC-Premium-LIMIT" extends "NonFleet-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_TTC_LIM && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcLimit($rf.getRateFactorNumericValue());	
		//System.out.println("TTC limit:"+$fact.getTtcLimit());
end

rule "NonFleet-Calc-CGO-TTC-Premium-NUMDAYS" extends "NonFleet-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_TTC_NumDays && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcDays($rf.getRateFactorNumericValue().intValue());	
		//System.out.println("TTC days:"+$fact.getTtcDays());

end

rule "NonFleet-Calc-CGO-TTC-Premium-CARGOTTCBASE" extends "NonFleet-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CARGOTTCBASE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setCargoTtcBase($rf.getRateFactorNumericValue());	
		//System.out.println("ttc base:"+$fact.getCargoTtcBase());
		
end

rule "NonFleet-Calc-CGO-TTC-Premium-SURCHDISC" extends "NonFleet-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_TTC_SurchDiscount && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcSurchDisc($rf.getRateFactorNumericValue());
		//System.out.println("ttc surch:"+$fact.getTtcSurchDisc());
end

rule "NonFleet-Calc-CGO-TTC-Premium-END" extends "NonFleet-Calc-CGO-TTC-Premium-BASE"
	when

	then
		
			$endttc.getEndorsementPremium().setOriginalPremium(calcTtcPremiumByFactor($fact.getCargoTtcBase() * (1+ $fact.getTtcSurchDisc()/100) , 1.0));

			/**
			System.out.println("EndCd:" + $endttc.getEndorsementCd());
			System.out.println("TTC Base:" + $fact.getCargoTtcBase());
			System.out.println("TTC Rate" + $fact.getTtcSurchDisc());**/
			
end