package com.echelon.rating.calc.nonfleet.cgo

import com.ds.ins.domain.policy.Coverage
import com.ds.ins.domain.policy.Endorsement
import com.ds.ins.domain.policy.RateFactor

import com.echelon.rating.nonfleet.cgo.NonFleetCGORateConstants
import com.echelon.domain.policy.specialtylines.CargoSubPolicy

import com.echelon.utils.SpecialtyAutoConstants
import com.echelon.rating.PolicyRatingHelper
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelperFactory

declare CGOPremCalcFactors 
	coverageCode		: String
	cgoPartialPrem		: double
	cgoLimitFactor  	: double
	cgoDedFactor    	: double
	cgoPrevRateFactor	: double
	cgoEarnedFrFactor	: double
	cgoNumOfPowerUnits	: double
	cgoUwAdjustment		: double
	cgoPremNoMod		: double
end

function Double calcCgoPremiumByFactor(double premium, double factor) {
	//return PolicyRatingHelper.getInstance().calcPremium(premium, factor);
	return NonFleetPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(premium, factor);	
}

rule "NonFleet-CGO-BASE"
	when
	
	then
			
end

rule "NonFleet-Calc-CGO-COVERAGE-BASE" extends "NonFleet-CGO-BASE"
	when
		$cov  : Coverage(coverageCode == SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC)
	then

end

rule "NonFleet-Calc-CGO-ENDORSEMENT-BASE" extends "NonFleet-CGO-BASE"
	when
	
	then
		
end

rule "NonFleet-Calc-CGO-CTC-BASE" extends "NonFleet-CGO-BASE"
	when
		$endctc : Endorsement(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC)		
	then
		//System.out.println("NonFleet-Calc-CGO-CTC-BASE:"+$endctc.getEndorsementCd());	
end

rule "NonFleet-Calc-CGO-TTC-BASE" extends "NonFleet-CGO-BASE"
	when
		$endttc : Endorsement(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC)		
	then
		System.out.println("NonFleet-Calc-CGO-TTC-BASE:"+$endttc.getEndorsementCd());	
end

//limit to a list of coverages
rule "NonFleet-Calc-CGO-Premium-BEGIN" extends "NonFleet-Calc-CGO-COVERAGE-BASE"
	when

	then
		//System.out.println("NonFleet-Calc-CGO-Premium-BEGIN:"+$cov.getCoverageCode());
end

rule "NonFleet-Calc-CGO-Premium-init" extends "NonFleet-Calc-CGO-Premium-BEGIN"
	when
		(not CGOPremCalcFactors(coverageCode == $cov.getCoverageCode))
	then
		CGOPremCalcFactors fact = new CGOPremCalcFactors();
		fact.setCoverageCode($cov.getCoverageCode());
		fact.setCgoPartialPrem(1);
		fact.setCgoLimitFactor(1);
		fact.setCgoDedFactor(1);
		fact.setCgoPrevRateFactor(1);
		fact.setCgoEarnedFrFactor(1);
		fact.setCgoNumOfPowerUnits(0);
		fact.setCgoUwAdjustment(1);
		fact.setCgoPremNoMod(1);
		insert(fact);
end

//variable $fact for the rules 
rule "NonFleet-Calc-CGO-Premium-BASE" extends "NonFleet-Calc-CGO-Premium-BEGIN"
	when
		$fact : CGOPremCalcFactors(coverageCode == $cov.getCoverageCode)
	then
end

rule "NonFleet-Calc-CGO-Premium-CGOPARTIALPREM" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGOPARTIALPREM && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoPartialPrem($rf.getRateFactorNumericValue());	
end

rule "NonFleet-Calc-Premium-CGOLIMITFACTOR" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGOLIMITFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoLimitFactor($rf.getRateFactorNumericValue());
end

rule "NonFleet-Calc-Premium-CGODEDFACTOR" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGODEDFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoDedFactor($rf.getRateFactorNumericValue());	
end

rule "NonFleet-Calc-Premium-PREVRATEFACTOR" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGOPREVRATEFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoPrevRateFactor($rf.getRateFactorNumericValue());	
end

rule "NonFleet-Calc-Premium-CGOEarnedFrFactor" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGOEARNEDFRFACTOR && rateFactorNumericValue != null) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoEarnedFrFactor($rf.getRateFactorNumericValue());
end

//Number of power units can be zero 
rule "NonFleet-Calc-Premium-NUM_OF_VEHICLES" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_CGO_POWERUNITS && rateFactorNumericValue != null) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoNumOfPowerUnits($rf.getRateFactorNumericValue());
end

rule "NonFleet-Calc-Premium-UWADJUSTMENT" extends "NonFleet-Calc-CGO-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetCGORateConstants.RFID_UWADJUSTMENT && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCgoUwAdjustment($rf.getRateFactorNumericValue());	
end

rule "NonFleet-Calc-CGO-Premium-END" extends "NonFleet-Calc-CGO-Premium-BASE"
	when

	then
	
			// NOTE: We are rounding before and after applying modification factor and number of vehicles //
			$fact.setCgoPremNoMod(calcCgoPremiumByFactor(($fact.getCgoPartialPrem() * $fact.getCgoDedFactor() * $fact.getCgoPrevRateFactor())+ $fact.getCgoEarnedFrFactor(), 1));			

			$cov.getCoveragePremium().setOriginalPremium(
				calcCgoPremiumByFactor($fact.getCgoPremNoMod() * $fact.getCgoUwAdjustment() * $fact.getCgoNumOfPowerUnits(), 1)
				);  			

			/**
			System.out.println("CovCd:" + $cov.getCoverageCode());
			System.out.println("Partial Prem:" + $fact.getCgoPartialPrem());
			System.out.println("Ded:" + $fact.getCgoDedFactor());
			System.out.println("PrevRate:" + $fact.getCgoPrevRateFactor());
			System.out.println("Earned Freight:" + $fact.getCgoEarnedFrFactor());
			System.out.println("NumOfPowerUnits:" + $fact.getCgoNumOfPowerUnits()); 
			System.out.println("cgo no mod Prem:" + $fact.getCgoPremNoMod());
			System.out.println("Final Prem:" + $cov.getCoveragePremium().getOriginalPremium());**/
			
end