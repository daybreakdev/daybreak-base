package com.echelon.rating.calc.nonfleet.auto

declare TowApPremCalcFactors 
		vehiclePK			: long
		coverageCode		: String
		clBaseRateTowAp		: double
		clLimitFactorTowAp	: double
		clDedFactorTowAp	: double
		clDrFactorTowAp		: double
		clClassFactorTowAp	: double
		clVrgFactorTowAp	: double
		clNonStdFactorTowAp	: double		
		clVehTypeFactorTowAp: double
		clUnitTypeFactorTowAp	: String
		clPremTow	 		: double		
		cmBaseRateTowAp		: double
		cmLimitFactorTowAp	: double
		cmDedFactorTowAp	: double
		cmDrFactorTowAp		: double
		cmClassFactorTowAp	: double
		cmVrgFactorTowAp	: double
		cmNonStdFactorTowAp	: double
		cmVehTypeFactorTowAp: double
		cmUnitTypeFactorTowAp	: String
		cmPremTow	 		: double		
		apPremTow	 		: double
end

//limit to ONLY AP Coverage
rule "NonFleet-TOWINGVEH-Calc-AP-Premium-BEGIN" extends "NonFleet-TOWINGVEH-Calc-AP-COVERAGE-BASE"
	when
		not (TowApPremCalcFactors(coverageCode == $covtowap.getCoverageCode && vehiclePK == $towVehap.policyRiskPK))
	then
		TowApPremCalcFactors fact = new TowApPremCalcFactors();
		fact.setVehiclePK($towVehap.getPolicyRiskPK());
		fact.setCoverageCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP);
		fact.setClBaseRateTowAp(0);
		fact.setClDrFactorTowAp(1);		
		fact.setClVrgFactorTowAp(1);
		fact.setClDedFactorTowAp(1);
		fact.setClVehTypeFactorTowAp(1);
		fact.setClUnitTypeFactorTowAp(null);
		fact.setClPremTow(0);		
		fact.setCmBaseRateTowAp(0);		
		fact.setCmVrgFactorTowAp(1);		
		fact.setCmDedFactorTowAp(1);
		fact.setCmVehTypeFactorTowAp(1);
		fact.setCmUnitTypeFactorTowAp(null);
		fact.setCmPremTow(0);
		insert(fact);
end

// variable $fact for the rules 
rule "NonFleet-TOWINGVEH-Calc-AP-Premium-BASE" extends "NonFleet-TOWINGVEH-Calc-AP-COVERAGE-BASE"
	when
		$fact : TowApPremCalcFactors(coverageCode == SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP && vehiclePK == $towVehap.policyRiskPK)
	then
		//System.out.println("NonFleet-TOWINGVEH-Calc-AP-Premium-BASE:"+$covtowap.getCoverageCode());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-BASE"
	when
		$covcltow  : Coverage(coverageCode == NonFleetAutoRateConstants.COVERAGE_CODE_APCL)
	then
		//System.out.println("CL Cov Cd:" + $covcltow.getCoverageCode());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-BASERATE" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_BASERATE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClBaseRateTowAp($rf.getRateFactorNumericValue());	
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-DRFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_TOWDRFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClDrFactorTowAp($rf.getRateFactorNumericValue());				
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-VRGFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_VRGFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClVrgFactorTowAp($rf.getRateFactorNumericValue());	
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-DEDFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_DEDFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClDedFactorTowAp($rf.getRateFactorNumericValue());				
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-VEHTYPEFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_VEHTYPEFACTOR && rateFactorNumericValue != null) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClVehTypeFactorTowAp($rf.getRateFactorNumericValue());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-UNITTYPEFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] SpecialtyAutoConstants.RFID_VEHICLEDESC && rateFactorStringValue != null) from $covcltow.rateFactorsLink.rateFactors
	then
		$fact.setClUnitTypeFactorTowAp($rf.getRateFactorStringValue());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-END" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM"
	when
	
	then

		if ("ET".equalsIgnoreCase($fact.getClUnitTypeFactorTowAp())) {	
			$fact.setClPremTow(
				calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor($fact.getClBaseRateTowAp() * $fact.getClDrFactorTowAp(),1)  
				*  $fact.getClVrgFactorTowAp(),1) * $fact.getClDedFactorTowAp(),1) * $fact.getClVehTypeFactorTowAp() * 0.5,1));
		}
		else {
			$fact.setClPremTow(
					calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor($fact.getClBaseRateTowAp() * $fact.getClDrFactorTowAp(),1)  
					*  $fact.getClVrgFactorTowAp(),1) * $fact.getClDedFactorTowAp(),1) * $fact.getClVehTypeFactorTowAp(),1));
			
		}
		
		System.out.println("===== TOWING COVERAGE CL=====");
		System.out.println("BaseRateTowAp:" + $fact.getClBaseRateTowAp());
		System.out.println("Ded:" + $fact.getClDedFactorTowAp());
		System.out.println("DR:" + $fact.getClDrFactorTowAp());		
		System.out.println("VRG:" + $fact.getClVrgFactorTowAp());
		System.out.println("VehType:" + $fact.getClVehTypeFactorTowAp());
		System.out.println("UnitType:" + $fact.getClUnitTypeFactorTowAp());
		System.out.println("CL Prem:" + $fact.getClPremTow());

end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CLPREM-END"
	when
		$covcmtow  : Coverage(coverageCode == NonFleetAutoRateConstants.COVERAGE_CODE_APCM)
	then
		//System.out.println("CM Cov Cd:" + $covcmtow.getCoverageCode());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-BASERATE" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_BASERATE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcmtow.rateFactorsLink.rateFactors
	then
		$fact.setCmBaseRateTowAp($rf.getRateFactorNumericValue());	
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-VRGFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_VRGFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcmtow.rateFactorsLink.rateFactors
	then
		$fact.setCmVrgFactorTowAp($rf.getRateFactorNumericValue());	
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-DEDFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_DEDFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $covcmtow.rateFactorsLink.rateFactors
	then
		$fact.setCmDedFactorTowAp($rf.getRateFactorNumericValue());				
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-VEHTYPEFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] NonFleetAutoRateConstants.RFID_VEHTYPEFACTOR && rateFactorNumericValue != null) from $covcmtow.rateFactorsLink.rateFactors
	then
		$fact.setCmVehTypeFactorTowAp($rf.getRateFactorNumericValue());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-UNITTYPEFACTOR" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
		$rf: RateFactor(rateFactorId str[endsWith] SpecialtyAutoConstants.RFID_VEHICLEDESC && rateFactorStringValue != null) from $covcmtow.rateFactorsLink.rateFactors
	then
		$fact.setCmUnitTypeFactorTowAp($rf.getRateFactorStringValue());
end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-END" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM"
	when
	
	then
	
		if ("ET".equalsIgnoreCase($fact.getCmUnitTypeFactorTowAp())) {
			$fact.setCmPremTow(
				calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor($fact.getCmBaseRateTowAp() * $fact.getCmVrgFactorTowAp(),1) * $fact.getCmDedFactorTowAp(),1) 
				* $fact.getCmVehTypeFactorTowAp()*0.5,1));
		}
		else {
			$fact.setCmPremTow(
					calcPremiumByFactor(calcPremiumByFactor(calcPremiumByFactor($fact.getCmBaseRateTowAp() * $fact.getCmVrgFactorTowAp(),1) * $fact.getCmDedFactorTowAp(),1) 
					* $fact.getCmVehTypeFactorTowAp(),1));			
		}
		
		
		System.out.println("===== TOW VEH COVERAGE CM=====");
		System.out.println("BaseRateTowAp:" + $fact.getCmBaseRateTowAp());
		System.out.println("VRG:" + $fact.getCmVrgFactorTowAp());
		System.out.println("Ded:" + $fact.getCmDedFactorTowAp());
		System.out.println("Veh Type:" + $fact.getCmVehTypeFactorTowAp());
		System.out.println("Unit Type:" + $fact.getCmUnitTypeFactorTowAp());
		System.out.println("CM Prem:" + $fact.getCmPremTow());

end

rule "NonFleet-TOWINGVEH-Calc-AP-Premium-END" extends "NonFleet-TOWINGVEH-Calc-AP-Premium-CMPREM-END" 
	when
	
	then
		$covtowap.getRateFactorsLink().addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLPREM, null, $fact.getClPremTow()));
		$covtowap.getRateFactorsLink().addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CMPREM, null, $fact.getCmPremTow()));
	
		$fact.setApPremTow($fact.getClPremTow() + calcPremiumByFactor($fact.getCmPremTow() * 0.85, 1));
		$covtowap.getCoveragePremium().setOriginalPremium($fact.getApPremTow());
		
		
		System.out.println("===== Tow Veh AP Coverage calc=====" + $covtowap.getCoverageCode());
		System.out.println("CL:" + $fact.getClPremTow());		
		System.out.println("CM:" + $fact.getCmPremTow());
		System.out.println("AP:" + $fact.getApPremTow());
		System.out.println("AP Premium:" + $covtowap.getCoveragePremium().getOriginalPremium());

end