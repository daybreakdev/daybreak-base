package com.echelon.rating.calc.lht.cgl

import com.ds.ins.domain.policy.Coverage
import com.ds.ins.domain.policy.Endorsement
import com.ds.ins.domain.policy.RateFactor

import com.echelon.rating.lht.cgl.LHTCGLRateConstants
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating
import com.echelon.domain.policy.specialtylines.CGLSubPolicy
import com.echelon.utils.SpecialtyAutoConstants
import com.echelon.rating.PolicyRatingHelper
import com.echelon.rating.lht.LHTPolicyRatingHelperFactory

declare CGLPremCalcFactors 
	coverageCode		: String
	cglBaseRate 		: double
	cglLimitFactor  	: double
	cglDedFactor    	: double
	cglClassFactor  	: double
	cglUwAdjustment		: double
	cglCommFactor 		: double
	cglOffBalFactor		: double
	cglModFactor		: double
	cglNumOfPowerUnits	: double
	cglPremNoMod		: double
	cglPremModFactor	: double
	unitPremiumCglCov	: double
end

declare TLLPremCalcFactors 
	coverageCode		: String
	tllLimitFactor		: double
	tllLimit			: double
	tllCommFactor		: double
	tllRateFactor		: double
	tllModFactor		: double
end

declare WLLPremCalcFactors 
	coverageCode		: String
	wllLimitFactor		: double
	wllLimit			: double
	wllCommFactor		: double
	wllRateFactor		: double
	wllModFactor		: double
end

declare EBEPremCalcFactors 
	coverageCode		: String
	ebeBaseRate			: double
	ebeCommFactor		: double
	ebeModFactor		: double
end

function Double calcCglPremiumByFactor(double premium, double factor) {
	//return PolicyRatingHelper.getInstance().calcPremium(premium, factor);  
	return LHTPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().roundTo(premium, 0);
}

rule "LHT-CGL-BASE"
	when
	
	then
			
end

rule "LHT-Calc-CGL-COVERAGE-BASE" extends "LHT-CGL-BASE"
	when
		$cov  : Coverage(coverageCode == SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD) 
	then

end

rule "LHT-Calc-CGL-ENDORSEMENT-BASE" extends "LHT-CGL-BASE"
	when
		$cglendr  : Endorsement(endorsementCd == SpecialtyAutoConstants.CGL_SUBPCY_ENDORSEMENT_CRB)		
	then
		//System.out.println("CRB:"+$cglendr.getEndorsementCd());
end

//limit to a list of coverages
rule "LHT-Calc-CGL-Premium-BEGIN" extends "LHT-Calc-CGL-COVERAGE-BASE"
	when

	then
		//System.out.println("LHT-Calc-CGL-Premium-BEGIN:"+$cov.getCoverageCode());
end

rule "LHT-Calc-CGL-Premium-init" extends "LHT-Calc-CGL-Premium-BEGIN"
	when
		(not CGLPremCalcFactors(coverageCode == $cov.getCoverageCode))
	then
		CGLPremCalcFactors fact = new CGLPremCalcFactors();
		fact.setCoverageCode($cov.getCoverageCode());
		fact.setCglBaseRate(1);
		fact.setCglLimitFactor(1);
		fact.setCglDedFactor(1);
		fact.setCglClassFactor(1);
		fact.setCglUwAdjustment(1);
		fact.setCglCommFactor(1);
		fact.setCglOffBalFactor(1);
		fact.setCglModFactor(1);
		fact.setCglNumOfPowerUnits(0);
		fact.setCglPremNoMod(1);
		fact.setCglPremModFactor(1);
		fact.setUnitPremiumCglCov(0);
		insert(fact);
end

//variable $fact for the rules 
rule "LHT-Calc-CGL-Premium-BASE" extends "LHT-Calc-CGL-Premium-BEGIN"
	when
		$fact : CGLPremCalcFactors(coverageCode == $cov.getCoverageCode)
	then
end

rule "LHT-Calc-CGL-Premium-CGLBASERATE" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_CGLBASERATE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglBaseRate($rf.getRateFactorNumericValue());	
end

rule "LHT-Calc-Premium-CGLLIMITFACTOR" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_CGLLIMITFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglLimitFactor($rf.getRateFactorNumericValue());
end

rule "LHT-Calc-Premium-CGLDEDFACTOR" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_CGLDEDFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglDedFactor($rf.getRateFactorNumericValue());
end

rule "LHT-Calc-Premium-UWADJUSTMENT" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_UWADJUSTMENT && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglUwAdjustment($rf.getRateFactorNumericValue());	
end

rule "LHT-Calc-Premium-COMMISSION_FACTOR" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_COMMISSION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglCommFactor($rf.getRateFactorNumericValue());	
end

rule "LHT-Calc-Premium-OFFBALFACTOR" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_OFFBALFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglOffBalFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-MODFACTOR" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_MODIFICATION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglModFactor($rf.getRateFactorNumericValue());	
end

//Number of power units can be zero 
rule "LHT-Calc-Premium-NUM_OF_VEHICLES" extends "LHT-Calc-CGL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_CGL_POWERUNITS && rateFactorNumericValue != null) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setCglNumOfPowerUnits($rf.getRateFactorNumericValue());
		//System.out.println("CGL PowerUnits:" + $fact.getCglNumOfPowerUnits()); 		
end

rule "LHT-Calc-CGL-Premium-END" extends "LHT-Calc-CGL-Premium-BASE"
	when

	then
			// NOTE: We are rounding before and after applying modification factor and number of vehicles //			
			$fact.setCglPremNoMod(calcCglPremiumByFactor($fact.getCglBaseRate() * $fact.getCglLimitFactor() * $fact.getCglDedFactor() * $fact.getCglOffBalFactor() * $fact.getCglCommFactor(), 1));
			$fact.setCglPremModFactor(calcCglPremiumByFactor($fact.getCglPremNoMod() * $fact.getCglModFactor(), 1));
			$fact.setUnitPremiumCglCov(calcCglPremiumByFactor($fact.getCglPremModFactor() * $fact.getCglUwAdjustment(), 1 ));
			$cov.getCoveragePremium().setUnitPremium($fact.getUnitPremiumCglCov());

			$cov.getCoveragePremium().setOriginalPremium($fact.getUnitPremiumCglCov() * $fact.getCglNumOfPowerUnits());
			
			/**
			System.out.println("BaseRate:" + $fact.getCglBaseRate());
			System.out.println("CovCd:" + $cov.getCoverageCode());
			System.out.println("Limit:" + $fact.getCglLimitFactor());
			System.out.println("Ded:" + $fact.getCglDedFactor());
			System.out.println("UWAdj:" + $fact.getCglUwAdjustment());
			System.out.println("Comm:" + $fact.getCglCommFactor());
			System.out.println("OffBalance:" + $fact.getCglOffBalFactor());
			System.out.println("ModFactor:" + $fact.getCglModFactor());
			System.out.println("NumOfPowerUnits:" + $fact.getCglNumOfPowerUnits());
			System.out.println("cgl no mod Prem:" + $fact.getCglPremNoMod());
			System.out.println("cgl with mod factor:" + $fact.getCglPremModFactor());
			System.out.println("cgl unit premium:" + $cov.getCoveragePremium().getUnitPremium());
			System.out.println("Final cgl Prem:" + $cov.getCoveragePremium().getOriginalPremium());		**/
end

rule "LHT-Calc-TLL-COVERAGE-BASE" extends "LHT-CGL-BASE"
	when
		$cov  : Coverage(coverageCode == SpecialtyAutoConstants.SUBPCY_COV_CODE_TLL)
	then

end

rule "LHT-Calc-TLL-Premium-BEGIN" extends "LHT-Calc-TLL-COVERAGE-BASE"
	when

	then

end

rule "LHT-Calc-TLL-Premium-init" extends "LHT-Calc-TLL-Premium-BEGIN"
	when
		(not TLLPremCalcFactors(coverageCode == $cov.getCoverageCode))
	then
		TLLPremCalcFactors fact = new TLLPremCalcFactors();
		fact.setCoverageCode($cov.getCoverageCode());			
		fact.setTllLimit(0);
		fact.setTllLimitFactor(1);	
		fact.setTllCommFactor(1);
		fact.setTllRateFactor(1);
		fact.setTllModFactor(1);
		insert(fact);
end

//variable $fact for the rules 
rule "LHT-Calc-TLL-Premium-BASE" extends "LHT-Calc-TLL-Premium-BEGIN"
	when
		$fact : TLLPremCalcFactors(coverageCode == $cov.getCoverageCode)
	then
end

rule "LHT-Calc-TLLLimit" extends "LHT-Calc-TLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_TLL_LIMIT && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setTllLimit($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-TllLimitFactor" extends "LHT-Calc-TLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_TLL_LIMITFACTOR && rateFactorNumericValue != null) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setTllLimitFactor($rf.getRateFactorNumericValue());	 

end

rule "LHT-Calc-Premium-TLLRateFactor" extends "LHT-Calc-TLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_TLL_RATEFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setTllRateFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-TLLCommFactor" extends "LHT-Calc-TLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_COMMISSION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setTllCommFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-TLLMODFACTOR" extends "LHT-Calc-TLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] SpecialtyAutoConstants.RFID_MODIFICATION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setTllModFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-TLL-Premium-END" extends "LHT-Calc-TLL-Premium-BASE"
	when

	then
	
		$cov.getCoveragePremium().setOriginalPremium(
			calcCglPremiumByFactor($fact.getTllLimit() *  $fact.getTllLimitFactor() * $fact.getTllRateFactor()/100 * $fact.getTllCommFactor(), 1)
			);

		/**
		System.out.println("Tll Limit:" + $fact.getTllLimit());
		System.out.println("Tll Factor:" + $fact.getTllLimitFactor());
		System.out.println("Tll Rate Factor:" + $fact.getTllRateFactor());
		System.out.println("Comm:" + $fact.getTllCommFactor());
		System.out.println("Mod Factor:" + $fact.getTllModFactor());
		System.out.println("TLL prem no mod:" + $fact.getTllPremNoMod());
		System.out.println("TLL Premium:" + $cov.getCoveragePremium().getOriginalPremium());**/ 		

end

rule "LHT-Calc-EBE-COVERAGE-BASE" extends "LHT-CGL-BASE"
	when
		$cov  : Coverage(coverageCode == SpecialtyAutoConstants.SUBPCY_COV_CODE_EBE) 
	then
		
end

//variable $fact for the rules 

rule "LHT-Calc-EBE-Premium-BEGIN" extends "LHT-Calc-EBE-COVERAGE-BASE"
	when

	then

end

rule "LHT-Calc-EBE-Premium-init" extends "LHT-Calc-EBE-Premium-BEGIN"
	when
		(not EBEPremCalcFactors(coverageCode == $cov.getCoverageCode))
	then
		EBEPremCalcFactors fact = new EBEPremCalcFactors();
		fact.setCoverageCode($cov.getCoverageCode());			
		fact.setEbeBaseRate(0);
		fact.setEbeCommFactor(1);
		fact.setEbeModFactor(1);
		insert(fact);
end

rule "LHT-Calc-EBE-Premium-BASE" extends "LHT-Calc-EBE-Premium-BEGIN"
	when
		$fact : EBEPremCalcFactors(coverageCode == $cov.getCoverageCode)
	then
end

rule "LHT-Calc-EBE-Premium-BASERATE" extends "LHT-Calc-EBE-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_CGLBASERATE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setEbeBaseRate($rf.getRateFactorNumericValue());	
end

rule "LHT-Calc-EBE-Premium-CommFactor" extends "LHT-Calc-EBE-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_COMMISSION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setEbeCommFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-EBE-Premium-END" extends "LHT-Calc-EBE-Premium-BASE"
	when

	then

		$cov.getCoveragePremium().setOriginalPremium(
			calcCglPremiumByFactor($fact.getEbeBaseRate() *  $fact.getEbeCommFactor(), 1)
			);

		/**
		System.out.println("EBE Base Rate:" + $fact.getEbeBaseRate());
		System.out.println("Comm:" + $fact.getEbeCommFactor());
		System.out.println("EBE modfactor:" + $fact.getEbeModFactor());
		System.out.println("EBE Premium:" + $cov.getCoveragePremium().getOriginalPremium()); **/ 
end

rule "LHT-Calc-WLL-COVERAGE-BASE" extends "LHT-CGL-BASE"
	when
		$cov  : Coverage(coverageCode == SpecialtyAutoConstants.SUBPCY_COV_CODE_WLL)
	then

end

rule "LHT-Calc-WLL-Premium-BEGIN" extends "LHT-Calc-WLL-COVERAGE-BASE"
	when
	
	then

end

rule "LHT-Calc-WLL-Premium-init" extends "LHT-Calc-WLL-Premium-BEGIN"
	when
		(not WLLPremCalcFactors(coverageCode == $cov.getCoverageCode))
	then
		WLLPremCalcFactors fact = new WLLPremCalcFactors();
		fact.setCoverageCode($cov.getCoverageCode());			
		fact.setWllLimit(0);
		fact.setWllLimitFactor(1);	
		fact.setWllCommFactor(1);
		fact.setWllRateFactor(1);
		fact.setWllModFactor(1);
		insert(fact);
end

//variable $fact for the rules 
rule "LHT-Calc-WLL-Premium-BASE" extends "LHT-Calc-WLL-Premium-BEGIN"
	when
		$fact : WLLPremCalcFactors(coverageCode == $cov.getCoverageCode)
	then
end

rule "LHT-Calc-WLLLimit" extends "LHT-Calc-WLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_WLL_LIMIT && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setWllLimit($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-WLLLimitFactor" extends "LHT-Calc-WLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_WLL_LIMITFACTOR && rateFactorNumericValue != null) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setWllLimitFactor($rf.getRateFactorNumericValue());	 

end

rule "LHT-Calc-Premium-WLLRateFactor" extends "LHT-Calc-WLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_WLL_RATEFACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setWllRateFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-Premium-WLLCommFactor" extends "LHT-Calc-WLL-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGLRateConstants.RFID_COMMISSION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $cov.rateFactorsLink.rateFactors
	then
		$fact.setWllCommFactor($rf.getRateFactorNumericValue());	 
end

rule "LHT-Calc-WLL-Premium-END" extends "LHT-Calc-WLL-Premium-BASE"
	when
	
	then
	
		$cov.getCoveragePremium().setOriginalPremium(
			calcCglPremiumByFactor($fact.getWllLimit() *  $fact.getWllLimitFactor() * $fact.getWllRateFactor()/100 * $fact.getWllCommFactor(), 1)
			);

		/**
		System.out.println("WLL Limit:" + $fact.getWllLimit());
		System.out.println("WLL Factor:" + $fact.getWllLimitFactor());
		System.out.println("WLL Rate Factor:" + $fact.getWllRateFactor());
		System.out.println("Comm:" + $fact.getWllCommFactor());
		System.out.println("Mod Factor:" + $fact.getWllModFactor());
		System.out.println("WLL Premium:" + $cov.getCoveragePremium().getOriginalPremium()); **/
		
end