package com.echelon.rating.calc.lht.cgo

import com.ds.ins.domain.policy.Coverage
import com.ds.ins.domain.policy.Endorsement
import com.ds.ins.domain.policy.RateFactor
import java.math.BigDecimal;

import com.echelon.rating.lht.cgo.LHTCGORateConstants
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating
import com.echelon.domain.policy.specialtylines.CargoSubPolicy

import com.echelon.utils.SpecialtyAutoConstants
import com.echelon.rating.PolicyRatingHelper
import com.echelon.rating.lht.LHTPolicyRatingHelperFactory

declare TTCPremCalcFactors 
	endorsementCd		: String
	ttcLimit			: double
	cargoTtcBase		: double
	ttcDays				: int	
	ttcSurchDisc		: double
	ttcCommFactor		: double
end

function Double calcTtcPremiumByFactor(double premium, double factor) {
	//return PolicyRatingHelper.getInstance().calcPremium(premium, factor);  
	return LHTPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().roundTo(premium, 0);
}

//limit to a list of coverages
rule "LHT-Calc-CGO-TTC-Premium-BEGIN" extends "LHT-Calc-CGO-TTC-BASE"
when
	not (TTCPremCalcFactors(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC))
then
	TTCPremCalcFactors fact = new TTCPremCalcFactors();
	fact.setEndorsementCd(SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC);
	fact.setTtcLimit(0);
	fact.setCargoTtcBase(0);
	fact.setTtcDays(0);	
	fact.setTtcSurchDisc(0);
	fact.setTtcCommFactor(0);
	insert(fact);

end

//variable $fact for the rules 
rule "LHT-Calc-CGO-TTC-Premium-BASE" extends "LHT-Calc-CGO-TTC-BASE"
	when
		$fact : TTCPremCalcFactors(endorsementCd == SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC)
	then
		//System.out.println("LHT-Calc-CGO-TTC-Premium-BASE:"+$endttc.getEndorsementCd());
end

rule "LHT-Calc-CGO-TTC-Premium-LIMIT" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGORateConstants.RFID_TTC_LIM && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcLimit($rf.getRateFactorNumericValue());	
		//System.out.println("TTC limit:"+$fact.getTtcLimit());
end

rule "LHT-Calc-CGO-TTC-Premium-NUMDAYS" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGORateConstants.RFID_TTC_NumDays && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcDays($rf.getRateFactorNumericValue().intValue());	
		//System.out.println("TTC days:"+$fact.getTtcDays());

end

rule "LHT-Calc-CGO-TTC-Premium-CARGOTTCBASE" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGORateConstants.RFID_CARGOTTCBASE && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setCargoTtcBase($rf.getRateFactorNumericValue());	
		//System.out.println("ttc base:"+$fact.getCargoTtcBase());
		
end

rule "LHT-Calc-CGO-TTC-Premium-SURCHDISC" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] LHTCGORateConstants.RFID_TTC_SurchDiscount && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcSurchDisc($rf.getRateFactorNumericValue());
		//System.out.println("ttc surch:"+$fact.getTtcSurchDisc());
end

rule "LHT-Calc-CGO-TTC-Premium-COMMISSION_FACTOR" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when
		$rf: RateFactor(rateFactorId str[endsWith] SpecialtyAutoConstants.RFID_COMMISSION_FACTOR && rateFactorNumericValue != null && rateFactorNumericValue > 0) from $endttc.rateFactorsLink.rateFactors
	then
		$fact.setTtcCommFactor($rf.getRateFactorNumericValue());	
		//System.out.println("ttc commfactor:"+$fact.getTtcCommFactor());
end

rule "LHT-Calc-CGO-TTC-Premium-END" extends "LHT-Calc-CGO-TTC-Premium-BASE"
	when

	then
		
			$endttc.getEndorsementPremium().setOriginalPremium(calcTtcPremiumByFactor($fact.getCargoTtcBase() * (1+ $fact.getTtcSurchDisc()/100) * $fact.getTtcCommFactor(), 1.0));
			/**
			System.out.println("EndCd:" + $endttc.getEndorsementCd());
			System.out.println("TTC Base:" + $fact.getCargoTtcBase());
			System.out.println("TTC Rate" + $fact.getTtcSurchDisc());
			System.out.println("Comm Factor:" + $fact.getTtcCommFactor());**/ 
			
end