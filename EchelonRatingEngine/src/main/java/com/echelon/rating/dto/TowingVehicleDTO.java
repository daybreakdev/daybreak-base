package com.echelon.rating.dto;

import java.io.Serializable;

import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.nonfleet.auto.NonFleetAutoRateConstants;

public class TowingVehicleDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1261755640904860247L;
	private SpecialtyVehicleRisk 	vehicle;
	private int 					calcRunType;	 

	public TowingVehicleDTO(SpecialtyVehicleRisk vehicle) {
		super();
		this.vehicle = vehicle;
	}

	public TowingVehicleDTO(SpecialtyVehicleRisk vehicle, int calcRunType) {
		super();
		this.vehicle = vehicle;
		this.calcRunType = calcRunType;
	}

	public SpecialtyVehicleRisk getVehicle() {
		return vehicle;
	}

	public void setVehicle(SpecialtyVehicleRisk vehicle) {
		this.vehicle = vehicle;
	}
	
	public String getVehicleClass() {
		return this.vehicle.getVehicleClass();
	}
	
	public String getDrivingRecord() {
		return this.vehicle.getDrivingRecord();
	}	
	
	public String getSelectedRateGroup() {	
		return this.vehicle.getSelectedRateGroup();
	}

	public int getCalcRunType() {
		return calcRunType;
	}

	public void setCalcRunType(int calcRunType) {
		this.calcRunType = calcRunType;
	}

	// Any vehicle
	public SpecialtyVehicleRisk getCalcAnyVehicle() {
		return getVehicle();
	}
	
	// calcRunType = NonFleetAutoRateConstants.CALC_RUN_TYPE_CURRENT, NonTowing only
	// calcRunType = NonFleetAutoRateConstants.CALC_RUN_TYPE_27BSUPPORT, Any vehicle
	public SpecialtyVehicleRisk getCalcNonTowingVehicle() {
		SpecialtyVehicleRisk result = null;
		
		switch (this.calcRunType) {
		case NonFleetAutoRateConstants.CALC_RUN_TYPE_27BSUPPORT:
			result = this.vehicle;
			break;

		default:
			if (getVehicle().getTowingVehiclePK() == null) {
				result = getVehicle();
			}
			break;
		}
		
		return result;
	}
	
	// calcRunType = NonFleetAutoRateConstants.CALC_RUN_TYPE_CURRENT, Towing only
	// calcRunType = NonFleetAutoRateConstants.CALC_RUN_TYPE_27BSUPPORT, Do not run
	public SpecialtyVehicleRisk getCalcTowingVehicle() {
		SpecialtyVehicleRisk result = null;
		
		switch (this.calcRunType) {
		case NonFleetAutoRateConstants.CALC_RUN_TYPE_27BSUPPORT:
			// No vehicle to calc
			break;

		default:
			if (getVehicle().getTowingVehiclePK() != null) {
				result = getVehicle();
			}
			break;
		}
		
		return result;
	}
}
