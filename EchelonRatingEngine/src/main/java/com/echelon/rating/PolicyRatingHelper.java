package com.echelon.rating;

import java.math.BigDecimal;

import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.rating.rounding.RoundingStrategyFactory;
import com.rating.utils.CorePolicyRatingHelper;

public class PolicyRatingHelper extends CorePolicyRatingHelper {
	
	private static PolicyRatingHelper instance;
	
	public static PolicyRatingHelper getInstance() {
		if (instance == null) {
			synchronized (PolicyRatingHelper.class) {
				instance = new PolicyRatingHelper();
			}
		}
		
		return instance;
	}
	
	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return LHTRoundingStrategyFactory.getInstance();
	}
	
	
	public Double roundTo(Double value, int decimals) {
		//return new BigDecimal(value).setScale(decimals, BigDecimal.ROUND_HALF_UP).doubleValue();
		return BigDecimal.valueOf(value).setScale(decimals, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
