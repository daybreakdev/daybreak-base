package com.echelon.rating;

import java.util.logging.Logger;

import javax.servlet.annotation.WebListener;

import com.rating.coreclasses.CorePolicyRatingFactory;
import com.rating.coreclasses.CorePolicyRatingStartupServlet;

@WebListener
public class PolicyRatingStartupServlet extends CorePolicyRatingStartupServlet {
	private static final Logger	log = Logger.getLogger(PolicyRatingStartupServlet.class.getName());

	@Override
	protected CorePolicyRatingFactory getPolicyRatingFactory() {
		// TODO Auto-generated method stub
		return PolicyRatingFactory.getInstance();
	}

}
