package com.echelon.rating;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.Premium;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.domain.policy.SubPolicy;

public class PolicyRatingProcessHelper {
	private static final Logger log = Logger.getLogger(PolicyRatingProcessHelper.class.getName());

	public interface CloneAddlCoverageAction {
		public void cloneCompleted(Coverage covClone);
	}
	
	public void rateSubpolicy(KieSession calcSession, SubPolicy<?> subPolicy) {
		rateCoverages(calcSession, subPolicy.getCoverageDetail().getCoverages());
		rateEndorsements(calcSession, subPolicy.getCoverageDetail().getEndorsements());
	}

	public void rateRisk(KieSession calcSession, Risk risk) {
		rateCoverages(calcSession, risk.getCoverageDetail().getCoverages());
		rateEndorsements(calcSession, risk.getCoverageDetail().getEndorsements());
	}
	
	public void rateCoverages(KieSession calcSession, Set<Coverage> coverages) {
		coverages
			.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
			.stream().forEach(cov -> {
				FactHandle fhCov = calcSession.insert(cov);
				try {
					calcSession.fireAllRules();
				} catch (Exception e) {
					throw e;
				} finally {
					try {
						calcSession.delete(fhCov);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		
	}
	
	public void rateEndorsements(KieSession calcSession, Set<Endorsement> endorsements) {
		endorsements
			.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
			.stream().forEach(end -> {
				FactHandle fhEnd = calcSession.insert(end);
				try {
					calcSession.fireAllRules();
				} catch (Exception e) {
					throw e;
				} finally {
					try {
						calcSession.delete(fhEnd);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
	}
	
	public void rateCoverage(KieSession kSession, Coverage coverage) {
		FactHandle fhCov = kSession.insert(coverage);
		List<FactHandle> fhRfs = new ArrayList<FactHandle>();
		if (coverage.getRateFactorsLink() != null) {
			for(RateFactor rf : coverage.getRateFactorsLink().getRateFactors()) {
				fhRfs.add(kSession.insert(rf));
			}
		}
		kSession.fireAllRules();
		for(FactHandle fhRrf : fhRfs) {
			kSession.delete(fhRrf);
		}
		kSession.delete(fhCov);			
	}
	
	public void rateEndorsement(KieSession kSession, Endorsement endorsement) {
		FactHandle fhEnd = kSession.insert(endorsement);
		List<FactHandle> fhRfs = new ArrayList<FactHandle>();
		if (endorsement.getRateFactorsLink() != null) {
			for(RateFactor rf : endorsement.getRateFactorsLink().getRateFactors()) {
				fhRfs.add(kSession.insert(rf));
			}
		}
		kSession.fireAllRules();
		for(FactHandle fhRrf : fhRfs) {
			kSession.delete(fhRrf);
		}
		kSession.delete(fhEnd);			
	}
	
	public void rateCoverageGroup(KieSession calcSession, Set<Coverage> coverages, Set<Endorsement> endorsements) {
		if (!coverages.isEmpty()) {
			
			List<FactHandle> fhCovs = new ArrayList<FactHandle>();
			
			if (coverages != null && !coverages.isEmpty()) {
				coverages
					.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
					.stream().forEach(cov -> {
						FactHandle fhCov = calcSession.insert(cov);
						fhCovs.add(fhCov);
					});
			}
			
			if (endorsements != null && !endorsements.isEmpty()) {
				endorsements
					.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
					.stream().forEach(end -> {
						FactHandle fhEnd = calcSession.insert(end);
						fhCovs.add(fhEnd);
					});			
			}
			
			try {
				calcSession.fireAllRules();
			} catch (Exception e) {
				throw e;
			} finally {
				try {
					if (!fhCovs.isEmpty()) {
						for(FactHandle fhCov : fhCovs) {
							calcSession.delete(fhCov);
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}		
	}
	
	public Coverage addAndRateAddlCoverage(KieSession kieSession, String srcParentKey, Coverage srcCov, String addlCovCode,
											HashMap<String, Set<Coverage>> hmAddlCoverages,
											CloneAddlCoverageAction cloneAddlCoverageAction) {
		Coverage newCov = cloneAddlCoverage(srcCov, addlCovCode, cloneAddlCoverageAction);
		if (newCov != null) {
			addAddlCoverage(srcParentKey, newCov, hmAddlCoverages);
			FactHandle fhCov = kieSession.insert(newCov);
			kieSession.fireAllRules();
			kieSession.delete(fhCov);
		}
		
		return newCov;
	}
	
	private Coverage cloneAddlCoverage(Coverage srcCov, String toCovCode, CloneAddlCoverageAction cloneAddlCoverageAction) {
		Coverage cov = null;
		
		try {
			cov = (Coverage) srcCov.clone();
			cov.setCoveragePremium((Premium) srcCov.getCoveragePremium().clone());
			cov.setCoverageCode(toCovCode);
			cov.setCoverageDetail(null); // do not want to save these temp coverages
			
			// Clone does not clone ratefactors
			if (cov.getRateFactorsLink() != null) {
				for(RateFactor srcRf : srcCov.getRateFactorsLink().getRateFactors()) {
					RateFactor toRf = (RateFactor) srcRf.clone();
					if (toRf.getRateFactorId().startsWith(srcCov.getCoverageCode())) {
						toRf.setRateFactorId(toCovCode+toRf.getRateFactorId().substring(srcCov.getCoverageCode().length()));
					}
					cov.getRateFactorsLink().addRateFactor(toRf);
				}
			}
			
			if (cloneAddlCoverageAction != null) {
				cloneAddlCoverageAction.cloneCompleted(cov);
			}
		} catch (CloneNotSupportedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return cov;
	}

	public void addAddlCoverage(String srcParentKey, Coverage cov, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		Set<Coverage> covs = hmAddlCoverages.get(srcParentKey);
		if (covs == null) {
			covs = new HashSet<Coverage>();
			hmAddlCoverages.put(srcParentKey, covs);	
		}
		covs.add(cov);
	}
	
	public Set<Coverage> getAddlCoverages(String srcParentKey, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		Set<Coverage> covs = hmAddlCoverages.get(srcParentKey);
		if (covs == null) {
			covs = new HashSet<Coverage>();
		}
		return covs;
	}
	
	public void finalizeAddlCoverages(HashMap<String, Set<Coverage>> hmAddlCoverages) {
		if (hmAddlCoverages != null) {
			long pk = -1;
			for(Set<Coverage> covs : hmAddlCoverages.values()) {
				for(Coverage cov : covs) {
					if (cov.getCoveragePK() == null) {
						cov.setCoveragePK(pk);
						pk--;
					}
				}
			}
		}
	}
}
