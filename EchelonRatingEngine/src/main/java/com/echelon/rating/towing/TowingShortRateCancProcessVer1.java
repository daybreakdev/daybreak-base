package com.echelon.rating.towing;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.RatingException;
import com.echelon.rating.PolicyRatingFactory;
import com.rating.towing.ShortRateCancDto;

public class TowingShortRateCancProcessVer1 implements Serializable {
	private static final long serialVersionUID = -9167615924729418590L;
	private static final Logger	log = Logger.getLogger(TowingShortRateCancProcessVer1.class.getName());
	//private static final String SESSION_NAME = "towingRating";
	
	private KieSession createKieSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}
	
	public Double getShortRate(PolicyTransaction<?> pcyTxn) throws Exception {
		Double rv = null;
		KieSession kSession = null;
		LocalDate termEffDate 	= pcyTxn.getPolicyTerm().getTermEffDate().toLocalDate();
		LocalDate termExpDate 	= pcyTxn.getPolicyTerm().getTermExpDate().toLocalDate();
		LocalDate cancelEffDate = pcyTxn.getPolicyVersion().getVersionDate();
		if (termEffDate != null && termExpDate != null && cancelEffDate != null) {
			try {
				kSession = createKieSession(pcyTxn);
				Long daysInForce = Duration.between(termEffDate.atStartOfDay(), cancelEffDate.atStartOfDay()).toDays();
				ShortRateCancDto dto = new ShortRateCancDto(daysInForce, null);
				log.info("=====> Before decision table: daysInForce" + dto.getDaysInForce() + ", premiumRetained: " + dto.getPremiumRetained());
				//kSession.insert(dto);
				kSession.insert(dto);
				kSession.setGlobal("dto", dto);
				kSession.fireAllRules();
				log.info("=====> After decision table: daysInForce" + dto.getDaysInForce() + ", premiumRetained: " + dto.getPremiumRetained());
				rv = dto.getPremiumRetained();
			} finally {
				try {
					if (kSession != null) {
						kSession.destroy();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} else {
			
		}
		return rv;
	}
}
