package com.echelon.rating.towing;

import com.rating.utils.CorePolicyRatingHelperFactory;
import com.rating.utils.ICorePolicyRatingHelper;

public class TowingPolicyRatingHelperFactory extends CorePolicyRatingHelperFactory {
	private static final long serialVersionUID = 3513010139856895705L;

	private static TowingPolicyRatingHelperFactory instance;
	
	private ICorePolicyRatingHelper policyRatingHelper;
	
	private TowingPolicyRatingHelperFactory() {
		policyRatingHelper = new TowingPolicyRatingHelper();
	}
	
	public static TowingPolicyRatingHelperFactory getInstance() {
		if (instance == null) {
			synchronized(TowingPolicyRatingHelperFactory.class) {
				instance = new TowingPolicyRatingHelperFactory();
			}
		}
		return instance;
	}
	
	public ICorePolicyRatingHelper getPolicyRatingHelper() {
		return policyRatingHelper;
	}
}
