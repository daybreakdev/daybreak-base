package com.echelon.rating.towing.cargo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.Risk;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.rating.towing.RateFactorsLinkDto;

public class TowingCargoRateProcessVer1 {

	public CargoSubPolicy<?> rateCargo(KieSession kSession, PolicyTransaction<?> policyTransaction, CargoSubPolicy<?> sp,
										Double commRate,
										List<RateFactorsLinkDto> allRateFactors) {
		CargoSubPolicy<?> cargoSp = (CargoSubPolicy<?>) sp;
		
		kSession.insert(sp);
		sp.getCoverageDetail().getCoverages()
								.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
								.stream().forEach(cov -> {
			RateFactorsLink covrfl = cov.getRateFactorsLink();
			if(covrfl == null) {
				covrfl = new RateFactorsLink();
				cov.setRateFactorsLink(covrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(covrfl));	
			covrfl.getRateFactors().clear();
			
			if ("MTCC".equalsIgnoreCase(cov.getCoverageCode())) {
				Double  totCargoPartialPrem = (cargoSp.getTotalCargoPartialPremium() != null 
											   ? cargoSp.getTotalCargoPartialPremium() 
											   : Double.valueOf(0));
				Integer minDrivingRecord	 = getMinDrivingRecord(policyTransaction);
				Double  drivingRecordFactor  = getCargoDrivingRecordFactor(minDrivingRecord);
				Double  earnedFrieghtCharges = Double.valueOf(50);
				Double  discountFactor		 = Double.valueOf(0.8);
				Double  perVehAddlLimitPrem  = Double.valueOf(0);
				covrfl.addRateFactor(new RateFactor("TotalCargoPartialPremium", null, totCargoPartialPrem));
				covrfl.addRateFactor(new RateFactor("MinDrivingRecord", null, Double.valueOf(minDrivingRecord)));
				covrfl.addRateFactor(new RateFactor("DrivingRecordFactor", null, drivingRecordFactor));
				covrfl.addRateFactor(new RateFactor("EarnedFrieghtCharges", null, earnedFrieghtCharges));
				covrfl.addRateFactor(new RateFactor("DiscountFactor", null, discountFactor));
				covrfl.addRateFactor(new RateFactor("PerVehicleAddlLimitPremium", null, perVehAddlLimitPrem));
				covrfl.addRateFactor(new RateFactor("NumUnits", null, Double.valueOf(cov.getNumOfUnits() != null && cov.getNumOfUnits() > 0 
																					 ? cov.getNumOfUnits() : 0)));
				covrfl.addRateFactor(new RateFactor("UWAdjustment", null, cov.getCoveragePremium().getPremiumModifier1() == null ? 1 : cov.getCoveragePremium().getPremiumModifier1()));
			}
			
			covrfl.addRateFactor(new RateFactor("Commission", null, commRate));
			covrfl.addRateFactor(new RateFactor("CommFactor", null, (1 - .1) / (1 - (commRate/100))));
			FactHandle fhCov = kSession.insert(cov);
			kSession.fireAllRules();
			kSession.delete(fhCov);
		});
		
		sp.getCoverageDetail().getEndorsements()
								.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
								.stream().forEach(end -> {
			RateFactorsLink endrfl = end.getRateFactorsLink();
			if(endrfl == null) {
				endrfl = new RateFactorsLink();
				end.setRateFactorsLink(endrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(endrfl));	
			endrfl.getRateFactors().clear();
			
			if ("TTC".equalsIgnoreCase(end.getEndorsementCd())) {
				endrfl.addRateFactor(new RateFactor("TTCAdditionalLimit", null, 
													Double.valueOf(end.getLimit1amount() != null ? end.getLimit1amount() : 0)));
				endrfl.addRateFactor(new RateFactor("NumberOfDays", null, getNumberOfDays(end)));
				endrfl.addRateFactor(new RateFactor("SurchargeDiscount", null, getSurchargeDiscount(end)));
			}
			else if ("CTC".equalsIgnoreCase(end.getEndorsementCd())) {
				endrfl.addRateFactor(new RateFactor("CTCLimit", null, 
													Double.valueOf(end.getLimit1amount() != null ? end.getLimit1amount() : 0)));
				endrfl.addRateFactor(new RateFactor("Revenue", null, getRevenue(end)));
				endrfl.addRateFactor(new RateFactor("Rate", null, getRateFactor(end)));
			}
			
			endrfl.addRateFactor(new RateFactor("Commission", null, commRate));
			endrfl.addRateFactor(new RateFactor("CommFactor", null, (1 - .1) / (1 - (commRate/100))));
			List<FactHandle> fhHandles = new ArrayList<FactHandle>();
			fhHandles.add(kSession.insert(end));
			for(RateFactor rf : endrfl.getRateFactors()) {
				fhHandles.add(kSession.insert(rf));
			}
			kSession.fireAllRules();
			fhHandles.stream().forEach(o -> kSession.delete(o));
		});
		
		return cargoSp;
	}
	
	private Double getCargoDrivingRecordFactor(Integer minVehicleRateGroup) {
		Double result = 1D;
		switch (minVehicleRateGroup) {
		case 0:
			result = 1.25D;
			break;

		case 1:
			result = 1.15D;
			break;

		case 2:
			result = 1.05D;
			break;

		case 3:
			result = 1D;
			break;

		default:
			break;
		}
		return result;
	}
	
	private Integer getMinDrivingRecord(PolicyTransaction<?> policyTransaction) {
		SpecialityAutoSubPolicy<Risk> spAutoSubPolicy = (SpecialityAutoSubPolicy<Risk>) policyTransaction.findSubPolicyByType("AUTO");
		Integer vehMinDrivingRecord = null;		
		for(Risk _risk : spAutoSubPolicy.getSubPolicyRisks()) {
			SpecialtyVehicleRisk veh = (SpecialtyVehicleRisk) _risk;
			if ((veh.isActive() || veh.isPending()) && veh.getDrivingRecord() != null) {
				try {
					Integer rg = Integer.valueOf(veh.getDrivingRecord());
					if (rg != null && rg > 0) {
						if (vehMinDrivingRecord == null || rg < vehMinDrivingRecord) {
							vehMinDrivingRecord = rg;
						}
					}
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		
		if (vehMinDrivingRecord == null) {
			vehMinDrivingRecord = 0;
		}
		else {
			vehMinDrivingRecord = Math.min(vehMinDrivingRecord, 3);
		}
		
		return vehMinDrivingRecord;
	}

	private Double getExtensionDataDoubleValue(Endorsement end, String extensionId, String columnId) {
		Double result = 0D;
		
		String strValue = getExtensionDataValue(end, extensionId, columnId);
		if (strValue != null) {
			try {
				Double val = Double.valueOf(strValue.replaceAll(",", ""));
				result = val;
			} catch (Exception e) {
			}
		}
		
		return result;
	}

	private String getExtensionDataValue(Endorsement end, String extensionId, String columnId) {
		String result = null;
		
		if (end.getDataExtension() != null) {
			ExtensionData extData = end.getDataExtension().findDataExtensionData(extensionId, columnId);
			if (extData != null && extData.getColumnValue() != null) {
				result = extData.getColumnValue();
			}
		}
		
		return result;
	}

	private Double getNumberOfDays(Endorsement end) {
		Double result = getExtensionDataDoubleValue(end, "TTC", "TTC_NumDays");
		return result;
	}

	private Double getSurchargeDiscount(Endorsement end) {
		Double result = getExtensionDataDoubleValue(end, "TTC", "TTC_SurDisc");
		return result;
	}

	private Double getRevenue(Endorsement end) {
		Double result = getExtensionDataDoubleValue(end, "CTC", "CTC_Revenue");
		return result;
	}

	private Double getRateFactor(Endorsement end) {
		Double result = getExtensionDataDoubleValue(end, "CTC", "CTC_RateFactor");
		return result;
	}
}

