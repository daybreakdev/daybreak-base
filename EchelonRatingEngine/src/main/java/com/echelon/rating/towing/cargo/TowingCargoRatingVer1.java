package com.echelon.rating.towing.cargo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.towing.TowingPolicyRatingHelperFactory;

public class TowingCargoRatingVer1 implements Serializable {

	private static final long serialVersionUID = -8730458251074233471L;

	public CargoSubPolicy<?> rateCargo(CargoSubPolicy<?> sp) {
		sp.getCoverageDetail().getCoverages()
				.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
				.stream().forEach(cov ->  {
			if(cov.getCoverageCode().equalsIgnoreCase("CPE")) 	cov = rateCPE(cov);
			if(cov.getCoverageCode().equalsIgnoreCase("MTCC")) 	cov = rateMTCC(cov);
		});

		sp.getCoverageDetail().getEndorsements()
				.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
				.stream().forEach(end ->  {
			if(end.getEndorsementCd().equalsIgnoreCase("TTC")) 	end = rateTTC(end);
			if(end.getEndorsementCd().equalsIgnoreCase("CTC")) 	end = rateCTC(end);
		});

		return sp;
	}
	
	// CARGO Rating
	private Coverage rateCPE(Coverage cov) {
		/*
		Double 	covPremium 		= Double.valueOf(0);
		Double 	commFactor		= Double.valueOf(1);
		Double 	cglLimitFactor	= Double.valueOf(1);
		Double	uwAdjustment	= Double.valueOf(1);
		Double  fixedFactor		= Double.valueOf(150);
		int		numUnits		= 1;

		if(cov.getNumOfUnits()!=null && cov.getNumOfUnits() >0) {
			numUnits = cov.getNumOfUnits();
		}
		
		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CGLLimitFactor")) {
				cglLimitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}		
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
		}	
		
		covPremium = commFactor * cglLimitFactor * fixedFactor * (1+uwAdjustment);
		cov.getCoveragePremium().setOriginalPremium(calcPremium(covPremium, numUnits));
		*/
		return cov;
	}

	private Coverage rateMTCC(Coverage cov) {
		Double 	covPremium 		= Double.valueOf(0);
		Double 	commFactor		= Double.valueOf(1);
		Double 	dedFactor		= Double.valueOf(1);
		Double	uwAdjustment	= Double.valueOf(1);
		int	    numUnits		= 1;
		
		Double  totCargoPartialPrem  = Double.valueOf(0);
		Double  drivingRecordFactor  = Double.valueOf(1);
		Double  earnedFrieghtCharges = Double.valueOf(0);
		Double  discountFactor		 = Double.valueOf(1);
		Double  perVehAddlLimitPrem  = Double.valueOf(0);
		
		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("TotalCargoPartialPremium")) {
				totCargoPartialPrem = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DrivingRecordFactor")) {
				drivingRecordFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("EarnedFrieghtCharges")) {
				earnedFrieghtCharges = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DiscountFactor")) {
				discountFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PerVehicleAddlLimitPremium")) {
				perVehAddlLimitPrem = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CGODEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
		}	
		
		covPremium = ((totCargoPartialPrem * dedFactor * drivingRecordFactor * discountFactor * uwAdjustment)
					  + (earnedFrieghtCharges + perVehAddlLimitPrem)) * commFactor;
		cov.getCoveragePremium().setOriginalPremium(calcPremium(covPremium, numUnits));
		return cov;
	}
	
	private Endorsement rateTTC(Endorsement end) {
		Double 	basePremium 	= Double.valueOf(0);
		Double 	endPremium 		= Double.valueOf(0);
		Double 	commFactor		= Double.valueOf(1);
		Double 	additionalLimit	= Double.valueOf(1);
		Integer	numberOfDays	= 1;
		Double	surchDiscount	= Double.valueOf(1);
		
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = end.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CGOTTCBASE")) {
				basePremium = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("TTCAdditionalLimit")) {
				additionalLimit = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumberOfDays")) {
				numberOfDays = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SurchargeDiscount")) {
				surchDiscount = rf.getRateFactorNumericValue();
			}
		}	
		
		endPremium = commFactor * basePremium * surchDiscount;
		end.getEndorsementPremium().setOriginalPremium(calcPremium(endPremium, 1));
		return end;
	}
	
	private Endorsement rateCTC(Endorsement end) {
		Double 	maxPremium 		= Double.valueOf(500);
		Double 	endPremium 		= Double.valueOf(0);
		Double 	commFactor		= Double.valueOf(1);
		Double 	limit			= Double.valueOf(1);
		Double	revenue			= Double.valueOf(1);
		Double	rate			= Double.valueOf(1);
		
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = end.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CTCLimit")) {
				limit = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("Revenue")) {
				revenue = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("Rate")) {
				rate = rf.getRateFactorNumericValue();
			}
		}	
		
		endPremium = commFactor * Math.min((revenue / 100 * rate), maxPremium);
		end.getEndorsementPremium().setOriginalPremium(calcPremium(endPremium, 1));
		return end;
	}

	protected Double calcPremium(Double premium, int numUnits) {
		/* moved
		Double result = new BigDecimal(premium).setScale(0, BigDecimal.ROUND_HALF_UP)
						.multiply(new BigDecimal(numUnits))
						.setScale(0, BigDecimal.ROUND_HALF_UP)
						.doubleValue();

		return result;
		*/
		
		return TowingPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(premium, numUnits);
	}

}
