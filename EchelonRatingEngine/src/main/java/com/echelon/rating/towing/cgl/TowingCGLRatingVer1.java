package com.echelon.rating.towing.cgl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.stream.Collectors;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.towing.TowingPolicyRatingHelperFactory;

public class TowingCGLRatingVer1 implements Serializable {

	private static final 	long 		serialVersionUID = -3823679442029934441L;
	private					Integer		cglDed;
	private					Integer		cglLimit;

	public CGLSubPolicy<?> rateCGL(CGLSubPolicy<?> sp) {
		cglDed 	= sp.getCglDeductible();
		cglLimit	= sp.getCglLimit();
		sp.getCoverageDetail().getCoverages()
					.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
					.stream().forEach(cov ->  {
			if(cov.getCoverageCode().equalsIgnoreCase("BIPD")) 	cov = rateBIPD(cov);
			if(cov.getCoverageCode().equalsIgnoreCase("TLL")) 	cov = rateTLL(cov);
			if(cov.getCoverageCode().equalsIgnoreCase("WLL")) 	cov = rateWLL(cov);
			if(cov.getCoverageCode().equalsIgnoreCase("EBE"))	cov = rateEB(cov);
		});

		return sp;
	}
	
	// CGL Rating
	private Coverage rateBIPD(Coverage cov) {
		Double 	covPremium 		= Double.valueOf(0);
		Double 	commFactor		= Double.valueOf(1);
		Double 	cglLimitFactor	= Double.valueOf(1);
		Double	uwAdjustment	= Double.valueOf(1);
		Double  fixedFactor		= Double.valueOf(150);
		int		numUnits		= 1;

		/* moved to TowingPolicyRateProcessVer1
		if(cov.getNumOfUnits()!=null && cov.getNumOfUnits() >0) {
			numUnits = cov.getNumOfUnits();
		}
		*/
		
		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CGLLimitFactor")) {
				cglLimitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}		
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
		}	
		
		covPremium = commFactor * cglLimitFactor * fixedFactor * uwAdjustment;
		cov.getCoveragePremium().setOriginalPremium(calcPremium(covPremium, numUnits));
		return cov;
	}

	private Coverage rateTLL(Coverage cov) {
		Double 		covPremium 		= Double.valueOf(0);
		Double 		commFactor		= Double.valueOf(1);
		Integer 	tllLimit		= 0;
		Double		ratefactor		= Double.valueOf(1);
		Integer		rateIndicator	= 0;

		if(cov.getLimit1()>250000) {
			rateIndicator = 1;
		}
		cov.getCoveragePremium().setOriginalPremium(covPremium);
		tllLimit = cov.getLimit1();

		RateFactorsLink covrfl = cov.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("Ratefactor")) {
				ratefactor = Double.valueOf(rf.getRateFactorNumericValue());
			}		
		}	
		// TLL Limit * Rate/100 * CommAdjFactor * Rate Indicator
		covPremium = tllLimit * (ratefactor/100) * commFactor * rateIndicator;
		cov.getCoveragePremium().setOriginalPremium(calcPremium(covPremium, 1));
		return cov;
	}

	private Coverage rateWLL(Coverage cov) {
		Double 		covPremium 		= Double.valueOf(0);
		Double 		commFactor		= Double.valueOf(1);
		Integer 	wllLimit		= 0;
		Double		ratefactor		= Double.valueOf(1);
		Integer		rateIndicator	= 0;

		if(cov.getLimit1()>250000) {
			rateIndicator = 1;
		}
		cov.getCoveragePremium().setOriginalPremium(covPremium);
		wllLimit = cov.getLimit1();

		RateFactorsLink covrfl = cov.getRateFactorsLink();
		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("Ratefactor")) {
				ratefactor = Double.valueOf(rf.getRateFactorNumericValue());
			}		
		}	
		// TLL Limit * Rate/100 * CommAdjFactor * Rate Indicator
		covPremium = wllLimit * (ratefactor/100) * commFactor * rateIndicator;
		cov.getCoveragePremium().setOriginalPremium(calcPremium(covPremium, 1));
		return cov;
	}

	private Coverage rateEB(Coverage cov) {
		Double 		covPremium 		= Double.valueOf(0);
		Double 		commFactor		= Double.valueOf(1);
		Double		ebFactor		= Double.valueOf(1);
		
		RateFactorsLink covrfl = cov.getRateFactorsLink();
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("EBFACTOR")) {
				ebFactor = rf.getRateFactorNumericValue();
			}
		}	

		covPremium = ebFactor * commFactor;
		cov.getCoveragePremium().setOriginalPremium(covPremium);
		return cov;
	}

	protected Double calcPremium(Double premium, int numUnits) {
		/* moved
		Double result = new BigDecimal(premium).setScale(0, BigDecimal.ROUND_HALF_UP)
						.multiply(new BigDecimal(numUnits))
						.setScale(0, BigDecimal.ROUND_HALF_UP)
						.doubleValue();

		return result;
		*/
		
		return TowingPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(premium, numUnits);
	}

}
