package com.echelon.rating.towing;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.Risk;
import com.ds.ins.exceptions.RatingException;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.towing.cargo.TowingCargoRateProcessVer1;
import com.echelon.rating.towing.cargo.TowingCargoRatingVer1;
import com.echelon.rating.towing.cgl.TowingCGLRatingVer1;
import com.rating.coreclasses.CorePolicyRatingProcess;
import com.rating.towing.RateFactorsLinkDto;

public class TowingPolicyRateProcessVer1 extends CorePolicyRatingProcess {

	private static final long serialVersionUID = 8972355955893468373L;
	private static final Logger	log = Logger.getLogger(TowingPolicyRateProcessVer1.class.getName());

	//private static final String SESSION_NAME = "towingRating";
	
	/*
	public void initializeKieSession() {
		log.info("Initialize KieSession");
		try {
			KieSession kSession = createKieSession();
			kSession.destroy();
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}
	
	private KieSession createKieSession() {
        KieServices ks = KieServices.Factory.get();
	    KieContainer kContainer = ks.getKieClasspathContainer();
    	KieSession kSession = kContainer.newKieSession(SESSION_NAME);
    	return kSession;
	}
	*/
	
	private KieSession createKieSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}

	@Override
	public PolicyTransaction<?> ratePolicyTransaction(PolicyTransaction<?> policyTransaction) throws RatingException {
		KieSession currkSession = null;
		
        try {
        	// for using inside the loop
        	final PolicyTransaction<?> wkPolicyTransaction = policyTransaction;
        	
        	List<RateFactorsLinkDto> allRateFactors = new ArrayList<RateFactorsLinkDto>();
        	KieSession kSession = createKieSession(policyTransaction);
        	if (kSession == null) {
        		throw new RatingException().withResourceCode("ErrorRatingSessionCreate");
        	}
        	currkSession = kSession;
			SpecialtyAutoPackage inspol = (SpecialtyAutoPackage) policyTransaction.getPolicyVersion().getInsurancePolicy();
			kSession.insert(inspol);
			Double commRate = policyTransaction.getPolicyVersion().getProducerCommissionRate();
			FactHandle fhPolTerm = kSession.insert(policyTransaction.getPolicyTerm());
			policyTransaction.getSubPolicies().stream().forEach(sp -> {
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase("AUTO")) {
					sp.getSubPolicyRisks().stream().forEach(spr -> {
						FactHandle fhRisk = kSession.insert(spr);
						spr.getCoverageDetail().getCoverages()
							.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
							.stream().forEach(cov -> {
							RateFactorsLink covrfl = cov.getRateFactorsLink();
							if(covrfl == null) {
								covrfl = new RateFactorsLink();
								cov.setRateFactorsLink(covrfl);
							}
							
							allRateFactors.add(new RateFactorsLinkDto(covrfl));
							
							covrfl.getRateFactors().clear();
							covrfl.addRateFactor(new RateFactor("Province",inspol.getControllingJurisdiction(),null));
							covrfl.addRateFactor(new RateFactor("Territory", inspol.getTerritoryCode(),null));
							covrfl.addRateFactor(new RateFactor("SubPolicyType", sp.getSubPolicyTypeCd(), null));
							covrfl.addRateFactor(new RateFactor("LiabilityLimit", null, Double.valueOf(inspol.getLiabilityLimit())));
							covrfl.addRateFactor(new RateFactor("VehicleType", ((SpecialtyVehicleRisk) spr).getVehicleType(), null));
							covrfl.addRateFactor(new RateFactor("NumUnits", null, Double.valueOf(((SpecialtyVehicleRisk) spr).getNumberOfUnits())));
							covrfl.addRateFactor(new RateFactor("RuraltyInd", inspol.getRuralityInd(), null));
							covrfl.addRateFactor(new RateFactor("VehicleClass", ((SpecialtyVehicleRisk) spr).getVehicleClass(), null));
							covrfl.addRateFactor(new RateFactor("Vehicle Rate Group", ((SpecialtyVehicleRisk) spr).getSelectedRateGroup(), null));
							covrfl.addRateFactor(new RateFactor("Commission", null, commRate));
							covrfl.addRateFactor(new RateFactor("CommFactor", null, (1 - .1) / (1 - (commRate/100))));
							covrfl.addRateFactor(new RateFactor("UWAdjustment", null, cov.getCoveragePremium().getPremiumModifier1() == null ? 1 : cov.getCoveragePremium().getPremiumModifier1()));
							if(cov.getCoverageCode().equalsIgnoreCase("AP")) {
								// Fire for 2 other covs
								FactHandle fhCov;
								cov.setCoverageCode("CL");
								fhCov = kSession.insert(cov);
								kSession.fireAllRules();
								kSession.delete(fhCov);								
								cov.setCoverageCode("CM");
								fhCov = kSession.insert(cov);
								kSession.fireAllRules();
								kSession.delete(fhCov);		
								cov.setCoverageCode("AP");
							}
							else {
								FactHandle fhCov = kSession.insert(cov);
								kSession.fireAllRules();
								kSession.delete(fhCov);								
							}
						});
						spr.getCoverageDetail().getEndorsements()
							.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
							.stream().forEach(end -> {
							RateFactorsLink endrfl = end.getRateFactorsLink();
							if(endrfl == null) {
								endrfl = new RateFactorsLink();
								end.setRateFactorsLink(endrfl);
							}
							
							allRateFactors.add(new RateFactorsLinkDto(endrfl));
							
							endrfl.getRateFactors().clear();
							endrfl.addRateFactor(new RateFactor("Province",inspol.getControllingJurisdiction(),null));
							endrfl.addRateFactor(new RateFactor("Territory", inspol.getTerritoryCode(),null));
							endrfl.addRateFactor(new RateFactor("SubPolicyType", sp.getSubPolicyTypeCd(), null));
							endrfl.addRateFactor(new RateFactor("LiabilityLimit", null, Double.valueOf(inspol.getLiabilityLimit())));
							endrfl.addRateFactor(new RateFactor("VehicleType", ((SpecialtyVehicleRisk) spr).getVehicleType(), null));
							endrfl.addRateFactor(new RateFactor("NumUnits", null, Double.valueOf(((SpecialtyVehicleRisk) spr).getNumberOfUnits())));
							endrfl.addRateFactor(new RateFactor("RuraltyInd", inspol.getRuralityInd(), null));
							endrfl.addRateFactor(new RateFactor("VehicleClass", ((SpecialtyVehicleRisk) spr).getVehicleClass(), null));
							endrfl.addRateFactor(new RateFactor("Vehicle Rate Group", ((SpecialtyVehicleRisk) spr).getSelectedRateGroup(), null));
							endrfl.addRateFactor(new RateFactor("DrivingRecord", ((SpecialtyVehicleRisk) spr).getDrivingRecord(), null));
							endrfl.addRateFactor(new RateFactor("Commission", null, commRate));
							endrfl.addRateFactor(new RateFactor("CommFactor", null, (1 - .1) / (1 - (commRate/100))));
							endrfl.addRateFactor(new RateFactor("UWAdjustment", null, end.getEndorsementPremium().getPremiumModifier1() == null ? 1 : end.getEndorsementPremium().getPremiumModifier1()));
							if(end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
								Coverage cv = spr.getCoverageDetail().getActiveOrPendingCoverageByCode("AP");
								if(cv!=null) {
									endrfl.addRateFactor(new RateFactor("APDeductible", null, Double.valueOf(cv.getDeductible1())));
									end.setDeductible1amount(cv.getDeductible1());
								}
							}
							FactHandle fhEnd = kSession.insert(end);
							kSession.fireAllRules();
							kSession.delete(fhEnd);
							if(end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
								end.setDeductible1amount(null);
							}
						});
						kSession.delete(fhRisk);
					});					
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase("CGL")) {
					kSession.insert(sp);
					
					CGLSubPolicy cglSubPolicy = (CGLSubPolicy) sp;
					sp.getCoverageDetail().getCoverages().stream().forEach(cov -> {
						RateFactorsLink covrfl = cov.getRateFactorsLink();
						if(covrfl == null) {
							covrfl = new RateFactorsLink();
							cov.setRateFactorsLink(covrfl);
						}
						allRateFactors.add(new RateFactorsLinkDto(covrfl));	
						covrfl.getRateFactors().clear();
						if (cglSubPolicy.getCglDeductible() == null) {
							throw new RuntimeException((new RatingException()).withResourceCode("RatingNoCGLDeductibleError"));
						}
						covrfl.addRateFactor(new RateFactor("Deductible",null, Double.valueOf(cglSubPolicy.getCglDeductible())));
						if (cglSubPolicy.getCglLimit() == null) {
							throw new RuntimeException((new RatingException()).withResourceCode("RatingNoCGLLimitError"));
						}
						covrfl.addRateFactor(new RateFactor("Limit",null, Double.valueOf(cglSubPolicy.getCglLimit())));
						covrfl.addRateFactor(new RateFactor("Commission", null, commRate));
						covrfl.addRateFactor(new RateFactor("CommFactor", null, (1 - .1) / (1 - (commRate/100))));
						
						if ("BIPD".equalsIgnoreCase(cov.getCoverageCode())) {
							covrfl.addRateFactor(new RateFactor("NumUnits", null, Double.valueOf(cov.getNumOfUnits() != null && cov.getNumOfUnits() > 0 
									 												? cov.getNumOfUnits() : 1)));
						}
						
						createCGLRatefactors(covrfl, cov);
						FactHandle fhCov = kSession.insert(cov);
						kSession.fireAllRules();
						kSession.delete(fhCov);
					});
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase("CGO")) {
					(new TowingCargoRateProcessVer1())
						.rateCargo(kSession, wkPolicyTransaction, (CargoSubPolicy<?>) sp, commRate, allRateFactors);
				}
			});
        	policyTransaction = ratePolicyDetails(policyTransaction);
        	finalizeRatingResults(allRateFactors);
        } catch (RuntimeException e) {
        	e.printStackTrace();
        	
        	if (e.getCause() != null && e.getCause() instanceof RatingException) {
        		throw (RatingException)e.getCause();
        	}
        	
        	throw new RatingException(e);
        } catch (Throwable t) {
            t.printStackTrace();
            
            if (t instanceof RatingException) {
            	throw (RatingException)t;
            }
            
            throw new RatingException(t);
        } finally {
			if (currkSession != null) {
				try {
					currkSession.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return policyTransaction;
	}

	protected void createCGLRatefactors(RateFactorsLink covrfl, Coverage cov) {
		switch (cov.getCoverageCode()) {
		case "WLL":
			Double ratefactor = getCovRatefactor(cov, "WLL_Ext", "WLL_RateFactor");
			if (ratefactor == null) {
				ratefactor = 1D;
			}
			covrfl.addRateFactor(new RateFactor("Ratefactor", null, ratefactor));
			break;
			
		case "TLL":
			ratefactor = getCovRatefactor(cov, "TLL_Ext", "TLL_RateFactor");
			if (ratefactor == null) {
				ratefactor = 1D;
			}
			covrfl.addRateFactor(new RateFactor("Ratefactor", null, ratefactor));
			break;

		default:
			covrfl.addRateFactor(new RateFactor("UWAdjustment", null, cov.getCoveragePremium().getPremiumModifier1() == null ? 1 : cov.getCoveragePremium().getPremiumModifier1()));
			break;
		}
	}
	
	protected Double getCovRatefactor(Coverage cov, String dataKeyId, String dataFieldName) {
		Double ratefactor = 1D;
		
		if (cov.getDataExtension() != null) {
			ExtensionData dataField = cov.getDataExtension().findDataExtensionData(dataKeyId, dataFieldName);
			if (dataField != null && dataField.getColumnValue() != null) {
				try {
					ratefactor = Double.valueOf(dataField.getColumnValue().replaceAll(",", "").replaceAll("$", ""));
				} catch (Exception e) {
				}
			}
		}
		
		return ratefactor;
	}
	
	@Override
	public PolicyTransaction<?> preRatePolicy(PolicyTransaction<?> policyTransaction) {
		// Remove all existing Rate Factors
		System.out.println("Pre Rate Process");
		return policyTransaction;
	}
	
	@Override
	public PolicyTransaction<?> ratePolicyDetails(PolicyTransaction<?> policyTransaction) {
		System.out.println("Rate the Policy");
		policyTransaction.getSubPolicies().stream().forEach(sp -> {
			if(sp.getSubPolicyTypeCd().equalsIgnoreCase("AUTO")) {
				sp.getSubPolicyRisks().stream().forEach(spr -> {
					spr.getCoverageDetail().getCoverages()
							.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
							.stream().forEach(cov -> {
						if(cov.getCoverageCode().equalsIgnoreCase("BI")) 	cov = rateBI(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("PD")) 	cov = ratePD(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("DCPD")) 	cov = rateDCPD(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("AB")) 	cov = rateAB(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("UA")) 	cov = rateUA(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("CL")) 	cov = rateCL(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("CM")) 	cov = rateCM(cov);
						if(cov.getCoverageCode().equalsIgnoreCase("SP")) 	cov = rateSP(cov);
					});

					rateAP(spr);

					spr.getCoverageDetail().getEndorsements()
							.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
							.stream().forEach(end -> {
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF27B")) 	end = rateOPCF27B(end);
						if(end.getEndorsementCd().equalsIgnoreCase("EON20G")) 	end = rateEON20G(end);
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF38")) 	end = rateOPCF38(end);
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF44R")) 	end = rateOPCF44R(end);
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF20")) 	end = rateOPCF20(end);
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF27"))	end = rateOPCF27(end);
						if(end.getEndorsementCd().equalsIgnoreCase("OPCF43")) 	end = rateOPCF43(end);
					});
				});					
			}
			if(sp.getSubPolicyTypeCd().equalsIgnoreCase("CGL")) {
				TowingCGLRatingVer1 cglRater = new TowingCGLRatingVer1();
				cglRater.rateCGL((CGLSubPolicy<?>) sp);
			}
			if(sp.getSubPolicyTypeCd().equalsIgnoreCase("CGO")) {
				TowingCargoRatingVer1 cargoRater = new TowingCargoRatingVer1();
				cargoRater.rateCargo((CargoSubPolicy<?>) sp);
			}
		});

		return policyTransaction;
	}

	@Override
	public PolicyTransaction<?> postRatePolicy(PolicyTransaction<?> policyTransaction) {
		System.out.println("Update the premiums");
		return policyTransaction;
	}
	
	private Coverage rateBI(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);	
		Double	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;
		Double 	commFactor 		= Double.valueOf(0);

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("BIBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BILIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BIDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BICLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BIDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BIVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("BITRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}
	
	private Coverage ratePD(Coverage cov) {
		Double baseRate 		= Double.valueOf(0);
		Double limitFactor 		= Double.valueOf(1);
		Double dedFactor 		= Double.valueOf(1);
		Double drFactor 		= Double.valueOf(1);
		Double classFactor 		= Double.valueOf(1);
		Double vrgFactor 		= Double.valueOf(1);
		Double trailerFactor 	= Double.valueOf(1);
		Double ratedPremium 	= Double.valueOf(0);	
		Double commFactor 		= Double.valueOf(0);
		Double uwAdjustment	= Double.valueOf(0);
		int numUnits 			= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("PDBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("PDTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}

	private Coverage rateDCPD(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("DCPDTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}

	private Coverage rateAB(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);	
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("ABBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ABTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;

	}

	private Coverage rateUA(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;
		
		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("UABASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UALIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UADEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UACLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UADRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UAVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UATRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;

	}

	private Coverage rateCL(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);		
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CLBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CLTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}

	private Coverage rateCM(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);		
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("CMBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CMTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}

	private Coverage rateAP(Risk risk) {
		Coverage cov = risk.getCoverageDetail().getActiveOrPendingCoverageByCode("AP");
		
		if (cov != null) {
			cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
			Double 	clbaseRate 		= Double.valueOf(0);
			Double 	cllimitFactor 	= Double.valueOf(1);
			Double 	cldedFactor 	= Double.valueOf(1);
			Double 	cldrFactor 		= Double.valueOf(1);
			Double 	clclassFactor 	= Double.valueOf(1);
			Double 	clvrgFactor		= Double.valueOf(1);
			Double 	cltrailerFactor	= Double.valueOf(1);
			Double 	clratedPremium 	= Double.valueOf(0);
			Double 	cmbaseRate 		= Double.valueOf(0);
			Double 	cmlimitFactor 	= Double.valueOf(1);
			Double 	cmdedFactor 	= Double.valueOf(1);
			Double 	cmdrFactor 		= Double.valueOf(1);
			Double 	cmclassFactor 	= Double.valueOf(1);
			Double 	cmvrgFactor		= Double.valueOf(1);
			Double 	cmtrailerFactor	= Double.valueOf(1);
			Double 	cmratedPremium 	= Double.valueOf(0);
			Double 	commFactor 		= Double.valueOf(0);
			int 	numUnits 		= 0;
			Double 	uwAdjustment	= Double.valueOf(0);
			Double  apratedPremium  = Double.valueOf(0);

			RateFactorsLink covrfl = cov.getRateFactorsLink();
	
			// Rate as if CM			
			for(RateFactor rf : covrfl.getRateFactors()) {
				if(rf.getRateFactorId().equalsIgnoreCase("CMBASERATE")) {
					cmbaseRate = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMLIMITFACTOR")) {
					cmlimitFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMDEDFACTOR")) {
					cmdedFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMCLASSFACTOR")) {
					cmclassFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMDRFACTOR")) {
					cmdrFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMVRGFACTOR")) {
					cmvrgFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CMTRAILERFACTOR")) {
					cmtrailerFactor = rf.getRateFactorNumericValue();
				}
			}
			
			// Rate as if CL
			for(RateFactor rf : covrfl.getRateFactors()) {
				if(rf.getRateFactorId().equalsIgnoreCase("CLBASERATE")) {
					clbaseRate = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLLIMITFACTOR")) {
					cllimitFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLDEDFACTOR")) {
					cldedFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLCLASSFACTOR")) {
					clclassFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLDRFACTOR")) {
					cldrFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLVRGFACTOR")) {
					clvrgFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CLTRAILERFACTOR")) {
					cltrailerFactor = rf.getRateFactorNumericValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
					numUnits = rf.getRateFactorNumericValue().intValue();
				}
				if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
					commFactor = Double.valueOf(rf.getRateFactorNumericValue());
				}
				if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
					uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
				}
			}
			
			clratedPremium = clbaseRate * cllimitFactor * cldedFactor * clclassFactor * cldrFactor * clvrgFactor * cltrailerFactor * uwAdjustment * commFactor;
			cmratedPremium = cmbaseRate * cmlimitFactor * cmdedFactor * cmclassFactor * cmdrFactor * cmvrgFactor * cmtrailerFactor * uwAdjustment * commFactor;
			apratedPremium = calcPremium((clratedPremium + cmratedPremium), numUnits);
			cov.getCoveragePremium().setOriginalPremium(apratedPremium);
		}
		
		return cov;
	}

	private Coverage rateSP(Coverage cov) {
		Double 	baseRate 		= Double.valueOf(0);
		Double 	limitFactor 	= Double.valueOf(1);
		Double 	dedFactor 		= Double.valueOf(1);
		Double 	drFactor 		= Double.valueOf(1);
		Double 	classFactor 	= Double.valueOf(1);
		Double 	vrgFactor 		= Double.valueOf(1);
		Double 	trailerFactor 	= Double.valueOf(1);
		Double 	ratedPremium 	= Double.valueOf(0);
		Double 	commFactor 		= Double.valueOf(0);
		Double 	uwAdjustment	= Double.valueOf(0);
		int 	numUnits 		= 0;

		cov.getCoveragePremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = cov.getRateFactorsLink();

		
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("SPBASERATE")) {
				baseRate = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPLIMITFACTOR")) {
				limitFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPDEDFACTOR")) {
				dedFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPDRFACTOR")) {
				drFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPVRGFACTOR")) {
				vrgFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("SPTRAILERFACTOR")) {
				trailerFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
			if(rf.getRateFactorId().equalsIgnoreCase("UWAdjustment")) {
				uwAdjustment = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}
		ratedPremium = calcPremium(baseRate * limitFactor * dedFactor * classFactor * drFactor * vrgFactor * trailerFactor * uwAdjustment * commFactor, numUnits);
		cov.getCoveragePremium().setOriginalPremium(ratedPremium);
		return cov;
	}
	
	private Endorsement rateOPCF27B(Endorsement end) {
		Double 			ratedPremium 	= Double.valueOf(0);		
		int 			numUnits 		= 0;
		Double 			commFactor 		= Double.valueOf(0);
		RateFactorsLink covrfl 			= end.getRateFactorsLink();
		Double			clPrem			= Double.valueOf(0);
		Double			cmPrem			= Double.valueOf(0);
		Double			apPrem			= Double.valueOf(0);
		Double			spPrem			= Double.valueOf(0);

		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));

		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = Double.valueOf(rf.getRateFactorNumericValue());
			}
		}

		for(Coverage cov : end.getCoverageDetail().getCoverages()) {
			if (cov.isActive() || cov.isPending()) {
				if(cov.getCoverageCode().equalsIgnoreCase("CL")) {
					if(BooleanUtils.isTrue(cov.getCoveragePremium().isPremiumOverride())) {
						clPrem = cov.getCoveragePremium().getAnnualPremium();
					}
					else {
						clPrem = cov.getCoveragePremium().getOriginalPremium();					
					}
				}
				if(cov.getCoverageCode().equalsIgnoreCase("CM")) {
					if(BooleanUtils.isTrue(cov.getCoveragePremium().isPremiumOverride())) {
						cmPrem = cov.getCoveragePremium().getAnnualPremium();
					}
					else {
						cmPrem = cov.getCoveragePremium().getOriginalPremium();					
					}
				}
				if(cov.getCoverageCode().equalsIgnoreCase("AP")) {
					if(BooleanUtils.isTrue(cov.getCoveragePremium().isPremiumOverride())) {
						apPrem = cov.getCoveragePremium().getAnnualPremium();
					}
					else {
						apPrem = cov.getCoveragePremium().getOriginalPremium();					
					}
				}
				if(cov.getCoverageCode().equalsIgnoreCase("SP")) {
					if(BooleanUtils.isTrue(cov.getCoveragePremium().isPremiumOverride())) {
						spPrem = cov.getCoveragePremium().getAnnualPremium();
					}
					else {
						spPrem = cov.getCoveragePremium().getOriginalPremium();					
					}
				}
			}
		}
		
		ratedPremium = clPrem + cmPrem + apPrem + spPrem;
		end.getEndorsementPremium().setOriginalPremium(calcPremium(ratedPremium, 1)); // Just for rounding.  # of units already applied in other coverages
		return end;
	}

	private Endorsement rateEON20G(Endorsement end) {
		Double 	ratedPremium 	= Double.valueOf(0);		
		int 	numUnits 		= 0;
		Double 	deddrFactor 	= Double.valueOf(1);
		int		fixedValue		= 7500;  //TODO make a table lookup
		Double	fixedFactor		= Double.valueOf(1.2);  // TODO make a table lookup
		Double 	classFactor		= Double.valueOf(1);
		Double  commFactor		= Double.valueOf(1);

		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = end.getRateFactorsLink();

		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("EON20GDRFACTOR")) {
				deddrFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("EON20GVEHCLASSFACTOR")) {
				classFactor = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		ratedPremium = fixedValue * fixedFactor * deddrFactor * classFactor * commFactor;
		ratedPremium = calcPremium(ratedPremium, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}

	private Endorsement rateOPCF38(Endorsement end) {
		Double 	ratedPremium = Double.valueOf(0);		
		int		limit1Amount = end.getLimit1amount();	
		int 	numUnits = 0;
		Double	commFactor = Double.valueOf(1);

		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		RateFactorsLink covrfl = end.getRateFactorsLink();

		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		// TODO Move hard coded values to decision table
		ratedPremium = ((Double.valueOf(limit1Amount) - 1500) / 1000) * 30;
		ratedPremium = ratedPremium * commFactor;
		ratedPremium = calcPremium(ratedPremium, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}

	private Endorsement rateOPCF44R(Endorsement end) {
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		Double ratedPremium = Double.valueOf(0);		
		int numUnits = 0;
		Double commFactor = Double.valueOf(1);
		RateFactorsLink covrfl = end.getRateFactorsLink();

		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ENDORSEMENTPREMIUM")) {
				ratedPremium = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		
		ratedPremium = calcPremium(ratedPremium * commFactor, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}

	private Endorsement rateOPCF20(Endorsement end) {
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		int numUnits = 0;
		Double basePremium = Double.valueOf(0);
		Double ratedPremium = Double.valueOf(0);
		Double commFactor = Double.valueOf(1);
		
		RateFactorsLink covrfl = end.getRateFactorsLink();
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ENDORSEMENTPREMIUM")) {
				basePremium = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		ratedPremium = calcPremium(basePremium * commFactor, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}

	private Endorsement rateOPCF27(Endorsement end) {
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		int numUnits = 0;
		Double basePremium = Double.valueOf(0);
		Double ratedPremium = Double.valueOf(0);
		Double commFactor = Double.valueOf(1);
		
		RateFactorsLink covrfl = end.getRateFactorsLink();
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ENDORSEMENTPREMIUM")) {
				basePremium = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		ratedPremium = calcPremium(basePremium * commFactor, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}

	private Endorsement rateOPCF43(Endorsement end) {
		end.getEndorsementPremium().setOriginalPremium(Double.valueOf(0));
		int numUnits = 0;
		Double basePremium = Double.valueOf(0);
		Double ratedPremium = Double.valueOf(0);
		Double commFactor = Double.valueOf(1);
		
		RateFactorsLink covrfl = end.getRateFactorsLink();
		for(RateFactor rf : covrfl.getRateFactors()) {
			if(rf.getRateFactorId().equalsIgnoreCase("NumUnits")) {
				numUnits = rf.getRateFactorNumericValue().intValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("ENDORSEMENTPREMIUM")) {
				basePremium = rf.getRateFactorNumericValue();
			}
			if(rf.getRateFactorId().equalsIgnoreCase("CommFactor")) {
				commFactor = rf.getRateFactorNumericValue();
			}
		}
		ratedPremium = calcPremium(basePremium * commFactor, numUnits);
		end.getEndorsementPremium().setOriginalPremium(ratedPremium);
		return end;
	}
	
	protected Double calcPremium(Double premium, int numUnits) {	
		return TowingPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(premium, numUnits);
	}
	
	// the ratefactors in coverage and endorsement are new objects
	protected void finalizeRatingResults(List<RateFactorsLinkDto> allRateFactors) {
		TowingPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().finalizeRatingResults(allRateFactors);
	}
}
