package com.echelon.rating.rounding;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.logging.Logger;

import com.rating.rounding.BaseRoundingStrategy;
import com.rating.rounding.IRoundingStrategy;
import com.ds.ins.utils.Constants;

public class RoundingStrategy50Fixed extends BaseRoundingStrategy implements IRoundingStrategy {
	final static Logger logger = Logger.getLogger(RoundingStrategy50Fixed.class.getName());

	public RoundingStrategy50Fixed() {
		super(logger);
	}

	@Override
	public Double roundOffSet(Double premium) {
		Double result = new BigDecimal(premium).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue();
		return result;
	}

	@Override
	public Double roundOnSet(Double premium) {
		Double result = new BigDecimal(premium).setScale(0, BigDecimal.ROUND_HALF_UP).doubleValue();
		return result;
	}
	
	private int getTermMonths(LocalDate termEffDate, LocalDate termExpDate) {
		Period period = Period.between(termEffDate, termExpDate);
		return period.getMonths();
	}

	@Override
	public Double calcProrata(Double premium, LocalDate termEffDate, LocalDate termExpDate, LocalDate changeEffDate,
			String recordType, String txnType) {
		Double prorataPremium = null;

		if (Constants.VERS_TXN_TYPE_NEWBUSINESS.equalsIgnoreCase(txnType) || Constants.VERS_TXN_TYPE_RENEWAL.equalsIgnoreCase(txnType)) {
			if (getTermMonths(termEffDate, termExpDate) == 6) {
				prorataPremium = premium / 2;
			}
		}
		if (prorataPremium == null) {
			prorataPremium = super.calcProrata(premium, termEffDate, termExpDate, changeEffDate, recordType, txnType);
		}
		return roundOnSet(prorataPremium);
	}
}
