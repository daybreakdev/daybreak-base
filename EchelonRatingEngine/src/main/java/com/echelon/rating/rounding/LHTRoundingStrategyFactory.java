package com.echelon.rating.rounding;

import com.rating.rounding.IRoundingStrategy;
import com.rating.rounding.RoundingStrategyFactory;

public class LHTRoundingStrategyFactory extends RoundingStrategyFactory {
	private static LHTRoundingStrategyFactory instance;
	private static IRoundingStrategy		  roundingStrategy;
	
	public static LHTRoundingStrategyFactory getInstance() {
		if (instance == null) {
			synchronized (LHTRoundingStrategyFactory.class) {
				instance = new LHTRoundingStrategyFactory();
			}
		}
		return instance;
	}
	
	public IRoundingStrategy getRoundingStrategy() {
		if (roundingStrategy == null) {
			roundingStrategy = new RoundingStrategy50Fixed();
		}
		return roundingStrategy;
	}
}
