package com.echelon.rating.rounding;

import com.rating.rounding.IRoundingStrategy;
import com.rating.rounding.RoundingStrategyFactory;

public class NonFleetRoundingStrategyFactory extends RoundingStrategyFactory {
	private static NonFleetRoundingStrategyFactory 	instance;
	private static IRoundingStrategy		  		roundingStrategy;
	
	public static NonFleetRoundingStrategyFactory getInstance() {
		if (instance == null) {
			synchronized (NonFleetRoundingStrategyFactory.class) {
				instance = new NonFleetRoundingStrategyFactory();
			}
		}
		return instance;
	}
	
	public IRoundingStrategy getRoundingStrategy() {
		if (roundingStrategy == null) {
			roundingStrategy = new RoundingStrategy50Fixed();
		}
		return roundingStrategy;
	}
}
