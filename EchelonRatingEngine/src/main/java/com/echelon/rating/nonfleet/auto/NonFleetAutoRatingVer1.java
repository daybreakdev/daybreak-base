package com.echelon.rating.nonfleet.auto;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.dto.TowingVehicleDTO;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingProcessHelper;
import com.echelon.utils.SpecialtyAutoConstants;

public class NonFleetAutoRatingVer1 implements Serializable {
	private static final long serialVersionUID = 6434436657931092696L;
	private static final Logger log = Logger.getLogger(NonFleetAutoRatingVer1.class.getName());
	
	private KieSession calcSession;
	private SpecialityAutoSubPolicy<?> subPolicy;
	private NonFleetPolicyRatingProcessHelper ratingProcessHelper;
	
	public NonFleetAutoRatingVer1(KieSession calcSession, SpecialityAutoSubPolicy<?> subPolicy) {
		super();
		
		this.ratingProcessHelper = new NonFleetPolicyRatingProcessHelper();
		this.calcSession = calcSession;
		this.subPolicy = subPolicy;
	}
	
	public SpecialityAutoSubPolicy<?> rateAuto(HashMap<String, Set<Coverage>> hmAddlCoverages) {
		if (calcSession != null) {
			FactHandle fhSp = calcSession.insert(subPolicy);
			calcSession.insert(fhSp);

			subPolicy.getSubPolicyRisks().stream().filter(subPolicyRisk -> subPolicyRisk.isActive() || subPolicyRisk.isPending()).forEach(veh -> {
				log.info("Calculate rating premium, vehicle="+veh.getDisplayString()+"/"+veh.getPolicyRiskPK());

				// Get the principal driver
				SpecialtyVehicleRisk spVeh = (SpecialtyVehicleRisk) veh;
				TowingVehicleDTO vehDTO = new TowingVehicleDTO(spVeh, NonFleetAutoRateConstants.CALC_RUN_TYPE_CURRENT); 
				List<FactHandle> fhs = ratingProcessHelper.insertVehicleDriver(calcSession, spVeh);
				FactHandle fhVeh = calcSession.insert(veh);
				fhs.add(fhVeh);
				FactHandle fhVehDTO = calcSession.insert(vehDTO);
				//ratingProcessHelper.rateRisk(calcSession, veh);
				
				// Rate all coverages exclude AP & IB
				ratingProcessHelper.rateCoverages(calcSession, veh.getCoverageDetail().getCoverages()
						.stream().filter(cov -> !SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP.equalsIgnoreCase(cov.getCoverageCode()) &&
								!SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IB.equalsIgnoreCase(cov.getCoverageCode()))
					    .collect(Collectors.toSet()));
				
				rateAP(spVeh, hmAddlCoverages);
				rateIB(spVeh, hmAddlCoverages);				
				
				// Rate all endorsements exclude 27B
				ratingProcessHelper.rateEndorsements(calcSession, veh.getCoverageDetail().getEndorsements()
						.stream().filter(end -> !SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B.equalsIgnoreCase(end.getEndorsementCd()))
					    .collect(Collectors.toSet()));
				
				// Remove DTO from session
				calcSession.delete(fhVehDTO);
				rate27B(spVeh, hmAddlCoverages);
				
				for(FactHandle fh : fhs) {
					calcSession.delete(fh);
				}
			});
			
			calcSession.delete(fhSp);
		}
			
		return subPolicy;
	}
	
	// Rate the actual AP coverage
	private Coverage rateAP(SpecialtyVehicleRisk spVeh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		// Check if AP exists
		Coverage APcov = spVeh.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP);
		if (APcov != null) {
			String srcParentKey = ratingProcessHelper.getAPSupportCoveragesSrcParentKey((SpecialtyVehicleRisk) spVeh);
			Set<Coverage> groupCoverages = ratingProcessHelper.getAddlCoverages(srcParentKey, hmAddlCoverages);
			// Do not affect the original set of coverages
			Set<Coverage> wkCoverages = new HashSet<Coverage>();
			wkCoverages.addAll(groupCoverages);
			
			//NonFleetCalcVehCovAPPremium.drl inlcude CL and CM
			//ratingProcessHelper.rateCoverageGroup(calcSession, wkCoverages, null);
			
			//Rename the supporting coverages, so they will not be calculated again
			wkCoverages.stream().forEach(o -> o.setCoverageCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP+o.getCoverageCode()));
			wkCoverages.add(APcov);
			
			ratingProcessHelper.rateCoverageGroup(calcSession, wkCoverages, null);
		}
		
		return APcov;
	}
	
	private Endorsement rate27B(SpecialtyVehicleRisk spVeh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		Endorsement end27B = spVeh.getCoverageDetail().getActiveOrPendingEndorsementByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B);
		if (end27B != null) {
			// For supporting coverages, use nontowing vehicle calculation
			TowingVehicleDTO vehDTO = new TowingVehicleDTO(spVeh, NonFleetAutoRateConstants.CALC_RUN_TYPE_27BSUPPORT);
			FactHandle fhVehDTO = calcSession.insert(vehDTO);
			
			String srcParentKey = ratingProcessHelper.get27BSupportCoveragesSrcParentKey((SpecialtyVehicleRisk) spVeh);
			Set<Coverage> groupCoverages = ratingProcessHelper.getAddlCoverages(srcParentKey, hmAddlCoverages);
			Coverage APcov = groupCoverages.stream().filter(o -> NonFleetAutoRateConstants.COVERAGE_CODE_27BAP.equalsIgnoreCase(o.getCoverageCode())).findFirst().orElse(null);
			
			//Rate the coverages CL/CM/SP and should use non towing vehilce calculation
			Set<Coverage> noAPgroupCovs = new HashSet<Coverage>();
			noAPgroupCovs.addAll(groupCoverages);
			if (APcov != null) {
				noAPgroupCovs.removeIf(o -> o.equals(APcov));
			}
			noAPgroupCovs.forEach(cov -> ratingProcessHelper.rateCoverage(calcSession, cov));

			//Rate the coverage AP as supporting coverage for 27B 
			if (APcov != null) {
				String srcAPParentKey = ratingProcessHelper.get27BAPSupportCoveragesSrcParentKey(spVeh);
				Set<Coverage> supportCoverages = ratingProcessHelper.getAddlCoverages(srcAPParentKey, hmAddlCoverages);
				rate27BAP(spVeh, APcov, supportCoverages);
			}
			
			calcSession.delete(fhVehDTO);

			// For current vehicle
			vehDTO = new TowingVehicleDTO(spVeh, NonFleetAutoRateConstants.CALC_RUN_TYPE_CURRENT);
			fhVehDTO = calcSession.insert(vehDTO);

			//Create rate factors for the coverage premium
			groupCoverages.stream().forEach(cov -> {
				String rateFactorId = cov.getCoverageCode()+SpecialtyAutoConstants.RFID_PREM;
				RateFactor premRf = cov.getRateFactorsLink().getRateFactorById(rateFactorId);
				if (premRf != null) {
					end27B.getRateFactorsLink().addRateFactor(
									new RateFactor(rateFactorId, premRf.getRateFactorStringValue(), premRf.getRateFactorNumericValue()));
				}
			});

			ratingProcessHelper.rateEndorsement(calcSession, end27B);
			
			calcSession.delete(fhVehDTO);
		}

		return end27B;
	}
	
	private Coverage rate27BAP(SpecialtyVehicleRisk spVeh, Coverage APcov, Set<Coverage> groupCoverages) {
		// Do not affect the original set of coverages
		Set<Coverage> wkCoverages = new HashSet<Coverage>();
		wkCoverages.addAll(groupCoverages);
		wkCoverages.add(APcov);
		
		ratingProcessHelper.rateCoverageGroup(calcSession, wkCoverages, null);
		
		return APcov;
	}
	
	private Coverage rateIB(SpecialtyVehicleRisk spVeh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		Coverage covIB = spVeh.getCoverageDetail().getActiveOrPendingCoverageByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IB);
		if (covIB != null) {
			RateFactorsLink covrfl = covIB.getRateFactorsLink();
			String srcParentKey = ratingProcessHelper.getIBPSupportCoveragesSrcParentKey(spVeh);
			Set<Coverage> depCoverages = ratingProcessHelper.getAddlCoverages(srcParentKey, hmAddlCoverages);
			
			double addPrem = 0d;
			for(Coverage depCov : depCoverages) {
				covrfl.addRateFactor(new RateFactor(depCov.getCoverageCode()+SpecialtyAutoConstants.RFID_PREM, null, depCov.getCoveragePremium().getOriginalPremium()));
				if (depCov.getCoveragePremium().getOriginalPremium() != null) {
					addPrem += depCov.getCoveragePremium().getOriginalPremium().doubleValue();
				}
			}
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_PREM, null, addPrem));
						
			ratingProcessHelper.rateCoverage(calcSession, covIB);
		}
		
		return covIB;
	}
}
