package com.echelon.rating.nonfleet.auto;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.echelon.utils.SpecialtyAutoConstants;

public class NonFleetAutoRateConstants {

	public static final String COVERAGE_CODE_APCL = "APCL";
	public static final String COVERAGE_CODE_APCM = "APCM";

	public static final String COVERAGE_CODE_27BCL = "OPCF27BCL";
	public static final String COVERAGE_CODE_27BCM = "OPCF27BCM";
	public static final String COVERAGE_CODE_27BSP = "OPCF27BSP";
	public static final String COVERAGE_CODE_27BAP = "OPCF27BAP";
	public static final String COVERAGE_CODE_27BAPCL = "OPCF27BAPCL";
	public static final String COVERAGE_CODE_27BAPCM = "OPCF27BAPCM";
	
	public static final int CALC_RUN_TYPE_CURRENT 		= 1; // Run for current vehicle
	public static final int CALC_RUN_TYPE_27BSUPPORT	= 2; // Run for current supporting
	
	public static final int CALC_VEH_TYPE_ANY		= 1;
	public static final int CALC_VEH_TYPE_NONTOW	= 2;
	public static final int CALC_VEH_TYPE_TOW		= 3;
	
	// Ratefactor ID
	public static final String RFID_UWADJUSTMENT				= "UWAdjustment";
	public static final String RFID_COMMISSION_FACTOR			= "CommFactor";
	public static final String RFID_PROVINCE 					= "Province";
	public static final String RFID_LIMIT 						= "Limit";

	// Values from decision table
	public static final String RFID_BASERATE 			= "BASERATE";
	public static final String RFID_LIMITFACTOR 		= "LIMITFACTOR";
	public static final String RFID_DRFACTOR 			= "DRFACTOR";
	public static final String RFID_TOWDRFACTOR 		= "TOWDRFACTOR";
	public static final String RFID_DEDPHYFACTOR		= "DEDPHYFACTOR";
	public static final String RFID_DEDLIABFACTOR		= "DEDLIABFACTOR";
	public static final String RFID_DEDFACTOR 			= "DEDFACTOR";
	public static final String RFID_CLASSFACTOR 		= "CLASSFACTOR";
	public static final String RFID_TOWCLASSFACTOR 		= "TOWCLASSFACTOR";
	public static final String RFID_VRGFACTOR			= "VRGFACTOR";
	public static final String RFID_TOWVRGFACTOR		= "TOWVRGFACTOR";
	public static final String RFID_27BVRGFACTOR		= "27BVRGFACTOR";
	public static final String RFID_NONSTDFACTOR		= "NONSTDFACTOR";	
	public static final String RFID_VEHTYPEFACTOR 		= "VEHTYPEFACTOR";
	public static final String RFID_TOWUNITTYPEFACTOR	= "TOWUNITTYPEFACTOR";	
	public static final String RFID_COMMADJFACTOR 		= "COMMADJFACTOR";
	public static final String RFID_ENDORSEMENTPREMIUM 	= "ENDORSEMENTPREMIUM";	
	public static final String RFID_CLAIMSSURCH			= "CLAIMSSURCH";
	public static final String RFID_CRIMCONVSURCH		= "CRIMCONVSURCH";
	public static final String RFID_MAJORCONVSURCH		= "MAJORCONVSURCH";
	public static final String RFID_MINORCONVSURCH		= "MINORCONVSURCH";
	public static final String RFID_TOTALSURCH			= "TOTALSURCH";
	public static final String RFID_PRYOUNGDRVSURCH		= "PRYOUNGDRVSURCH";
	public static final String RFID_SEYOUNGDRVSURCH		= "SEYOUNGDRVSURCH";
	public static final String RFID_CLAIMSSURCH_OVERRIDE		= "CLAIMSSURCHOVERRIDE";
	public static final String RFID_CRIMCONVSURCH_OVERRIDE		= "CRIMCONVSURCHOVERRIDE";
	public static final String RFID_MAJORCONVSURCH_OVERRIDE		= "MAJORCONVSURCHOVERRIDE";
	public static final String RFID_MINORCONVSURCH_OVERRIDE		= "MINORCONVSURCHOVERRIDE";
	public static final String RFID_TOTALSURCH_OVERRIDE			= "TOTALSURCHOVERRIDE";	
	public static final String RFID_EON20GDRFACTOR		= "EON20GDRFACTOR";
	public static final String RFID_EON20GVEHCLASSFACTOR= "EON20GVEHCLASSFACTOR";
	public static final String RFID_OPTIONALABRATE		= "OPTIONALABRATE";
	public static final String RFID_OP44BASERATE		= "OP44BASERATE";
	public static final String RFID_OP44LIMITFACTOR		= "OP44LIMITFACTOR";
	public static final String RFID_OP44NONSTDFACTOR	= "OP44NONSTDFACTOR";

	public static final String RFID_PREM 				= "PREM";
	public static final String RFID_27BCLPREM			= "OPCF27BCLPREM";
	public static final String RFID_27BCMPREM			= "OPCF27BCMPREM";
	public static final String RFID_27BSPPREM			= "OPCF27BSPPREM";
	public static final String RFID_27BAPPREM			= "OPCF27BAPPREM";
	
	public static final Set<String> NonFleetRating_IB_Dependencies = Stream.of(
			SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AB,
			SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IR,
			SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_MRC,
			SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_OCI,
			SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CHM
	).collect(Collectors.toCollection(HashSet::new));
}