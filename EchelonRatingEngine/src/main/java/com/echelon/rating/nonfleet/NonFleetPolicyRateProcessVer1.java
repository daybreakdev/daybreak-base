package com.echelon.rating.nonfleet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.RatingException;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.nonfleet.auto.NonFleetAutoRateProcessVer1;
import com.echelon.rating.nonfleet.auto.NonFleetAutoRatingVer1;
import com.echelon.rating.nonfleet.cgl.NonFleetCGLRateProcessVer1;
import com.echelon.rating.nonfleet.cgl.NonFleetCGLRatingVer1;
import com.echelon.rating.nonfleet.cgo.NonFleetCGORateProcessVer1;
import com.echelon.rating.nonfleet.cgo.NonFleetCGORatingVer1;

import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.coreclasses.CorePolicyRatingProcess;
import com.rating.towing.RateFactorsLinkDto;

public class NonFleetPolicyRateProcessVer1 extends CorePolicyRatingProcess {

	private static final long serialVersionUID = 8981192480511343357L;
	
	private KieSession createKieRatingSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}
	
	private KieSession createKieCalcSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieCalcSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}

	@Override
	public PolicyTransaction<?> ratePolicyTransaction(PolicyTransaction<?> policyTransaction) throws RatingException {
		KieSession kSession = null;
		KieSession calcSession = null;
		
        try {
        	List<RateFactorsLinkDto> allRateFactors = new ArrayList<RateFactorsLinkDto>();
        	kSession = createKieRatingSession(policyTransaction);
        	if (kSession == null) {
        		throw new RatingException().withResourceCode("ErrorRatingSessionCreate");
        	}
        	
        	calcSession = createKieCalcSession(policyTransaction);
       	
			SpecialtyAutoPackage inspol = (SpecialtyAutoPackage) policyTransaction.getPolicyVersion().getInsurancePolicy();
			FactHandle fhInsPol = kSession.insert(inspol);
			
			Double commRate = policyTransaction.getPolicyVersion().getProducerCommissionRate();
						
			// Special case for NonFleet AP
			HashMap<String, Set<Coverage>> hmAddlCoverages = new HashMap<String, Set<Coverage>>();
			
			final KieSession kieRateSession = kSession;
			FactHandle fhPolTerm = kieRateSession.insert(policyTransaction.getPolicyTerm());
			policyTransaction.getSubPolicies().stream().forEach(sp -> {
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					(new NonFleetAutoRateProcessVer1(kieRateSession)).rateAuto(inspol, (SpecialityAutoSubPolicy<?>) sp, commRate, allRateFactors, hmAddlCoverages);
				}
				else if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					// Rate CGL
					(new NonFleetCGLRateProcessVer1(kieRateSession)).rateCGL((CGLSubPolicy<?>) sp, commRate, allRateFactors);
				}
				else if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					// Rate Cargo
					(new NonFleetCGORateProcessVer1(kieRateSession)).rateCGO((CargoSubPolicy<?>) sp, commRate, allRateFactors);
				}
			});
			
			policyTransaction = ratePolicyDetails(calcSession, policyTransaction, hmAddlCoverages);
			finalizeRatingResults(allRateFactors);
        } catch (RuntimeException e) {
        	e.printStackTrace();
        	
        	if (e.getCause() != null && e.getCause() instanceof RatingException) {
        		throw (RatingException)e.getCause();
        	}
        	
        	throw new RatingException(e);
        } catch (Throwable t) {
            t.printStackTrace();
            
            if (t instanceof RatingException) {
            	throw (RatingException)t;
            }
            
            throw new RatingException(t);
        } finally {
			if (kSession != null) {
				try {
					kSession.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			if (calcSession != null) {
				try {
					calcSession.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
        
		return policyTransaction;
	}

	protected PolicyTransaction<?> ratePolicyDetails(KieSession calcSession, PolicyTransaction<?> policyTransaction, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		try {
			policyTransaction.getSubPolicies().stream().forEach(sp -> {
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					(new NonFleetAutoRatingVer1(calcSession, (SpecialityAutoSubPolicy<?>) sp)).rateAuto(hmAddlCoverages);
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					(new NonFleetCGLRatingVer1(calcSession, (CGLSubPolicy<?>) sp)).rateCGL();
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					(new NonFleetCGORatingVer1(calcSession, (CargoSubPolicy<?>) sp)).rateCGO();
				}			
			});
		} catch (Exception e) {
			throw e;
		} 
		
		return policyTransaction;
	}
	
	// the ratefactors in coverage and endorsement are new objects
	protected void finalizeRatingResults(List<RateFactorsLinkDto> allRateFactors) {
		/* moved
		for(RateFactorsLinkDto dto : allRateFactors) {
			if (dto.getOrigRateFactors() != null && !dto.getOrigRateFactors().isEmpty() &&
				dto.getRateFactorsLink() != null && !dto.getRateFactorsLink().getRateFactors().isEmpty()) {
				List<RateFactor> newList = new ArrayList<RateFactor>();
				newList.addAll(dto.getRateFactorsLink().getRateFactors());
				
				dto.getRateFactorsLink().getRateFactors().clear();

				for(RateFactor newRf : newList) {
					try {
						RateFactor oriRf = dto.getOrigRateFactors().stream()
													.filter(o -> newRf.getRateFactorId().equalsIgnoreCase(o.getRateFactorId()))
													.findFirst().orElse(null);
						if (oriRf == null) { // new one
							dto.getRateFactorsLink().addRateFactor(newRf);
						}
						else {
							dto.getRateFactorsLink().addRateFactor(oriRf);
							oriRf.setRateFactorNumericValue(newRf.getRateFactorNumericValue());
							oriRf.setRateFactorStringValue(newRf.getRateFactorStringValue());
						}
					} catch (Throwable e) {
						// TODO: handle exception
					}
				}
			}
		}
		*/
		
		NonFleetPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().finalizeRatingResults(allRateFactors);
	}

	@Override
	public PolicyTransaction<?> preRatePolicy(PolicyTransaction<?> policyTransaction) {
		// TODO Auto-generated method stub
		return super.preRatePolicy(policyTransaction);
	}

	@Override
	public PolicyTransaction<?> postRatePolicy(PolicyTransaction<?> policyTransaction) {
		// TODO Auto-generated method stub
		return super.postRatePolicy(policyTransaction);
	}
	
}
