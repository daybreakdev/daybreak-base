package com.echelon.rating.nonfleet.cgl;

public class NonFleetCGLRateConstants {

	// Ratefactor ID
	public static final String RFID_UWADJUSTMENT				= "UWAdjustment";
	public static final String RFID_COMMISSION_FACTOR			= "CommFactor";
	public static final String RFID_DED 						= "Ded";
	public static final String RFID_MAX_COMMISSION				= "Max Commission";
	public static final String RFID_SELECTED_COMMISSION_RATE	= "Selected Commission Rate";
	public static final String RFID_COMM_ADJUSTMENT_FACTOR 		= "Commission Adjustment Factor";
	public static final String RFID_CGL_POWERUNITS 				= "CGL Power Units";
	public static final String RFID_TLL_LIMIT		 			= "TLL Limit";
	public static final String RFID_TLL_LIMITFACTOR		 		= "TLL Limit Factor";
	public static final String RFID_TLL_RATEFACTOR		 		= "TLL Rate Factor";
	public static final String RFID_WLL_LIMIT		 			= "WLL Limit";
	public static final String RFID_WLL_LIMITFACTOR		 		= "WLL Limit Factor";
	public static final String RFID_WLL_RATEFACTOR		 		= "WLL Rate Factor";

	// Values from decision table
	public static final String RFID_FIXEDBASERATE 				= "FIXEDBASERATE";
	public static final String RFID_CGLLIMITFACTOR 				= "CGLLIMITFACTOR";
	public static final String RFID_CGLDEDFACTOR 				= "CGLDEDFACTOR";
	public static final String RFID_COMMADJFACTOR 				= "COMMADJFACTOR";
	public static final String RFID_SUBPOLENDPREMIUM 			= "SUBPOLENDPREMIUM";	
	
}
