package com.echelon.rating.nonfleet.cgl;

import java.util.List;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelper;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelperFactory;
import com.echelon.rating.nonfleet.cgl.NonFleetCGLRateConstants;
import com.rating.towing.RateFactorsLinkDto;

public class NonFleetCGLRateProcessVer1 {
	private static final Logger log = Logger.getLogger(NonFleetCGLRateProcessVer1.class.getName());
	
	private KieSession kSession;
	private PolicyRatingProcessHelper ratingProcessHelper;

	public NonFleetCGLRateProcessVer1(KieSession kSession) {
		super();
		this.kSession = kSession;
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
	}

	public void rateCGL(CGLSubPolicy<?> cglSubPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		FactHandle fhcglSubpolicy = kSession.insert(cglSubPolicy);

		rateCoverage(cglSubPolicy, commRate, allRateFactors);
		kSession.delete(fhcglSubpolicy);
	}
		
	private void rateCoverage(CGLSubPolicy<?> cglSubPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		cglSubPolicy.getCoverageDetail().getCoverages().stream().forEach(cov -> {
			
			RateFactorsLink covrfl = cov.getRateFactorsLink();
			if(covrfl == null) {
				covrfl = new RateFactorsLink();
				cov.setRateFactorsLink(covrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(covrfl));
			covrfl.getRateFactors().clear();
			
			//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			NonFleetPolicyRatingHelper policyRatingHelper = (NonFleetPolicyRatingHelper) NonFleetPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper();
			switch (cov.getCoverageCode()) {
			case SpecialtyAutoConstants.SUBPCY_COV_CODE_BIPD: 
				//covrfl.addRateFactor(new RateFactor(LHTCGLRateConstants.RFID_CGL_POWERUNITS, null, Double.valueOf(cov.getNumOfUnits() != null && cov.getNumOfUnits() > 0 ? cov.getNumOfUnits() : 1)));
				//CGL Power Units can be zero
				System.out.println("BIPD Code:");
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_CGL_POWERUNITS, null, cov.getNumOfUnits() != null ? Double.valueOf(cov.getNumOfUnits()) : null));
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, cov.getCoveragePremium().getPremiumModifier1() != null ? cov.getCoveragePremium().getPremiumModifier1() : 1));				
				break;
			case SpecialtyAutoConstants.SUBPCY_COV_CODE_TLL: 
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_TLL_LIMIT, null, cov.getLimit1()*1.0)); 
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_TLL_LIMITFACTOR, null, cov.getLimit1()>250000.0 ? 1.0 : 0)); 				
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_TLL_RATEFACTOR, null,
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getCoverageExtensionDataValue(cov, SpecialtyAutoConstants.DATAEXT_ID_TLL, SpecialtyAutoConstants.DATAEXT_COLID_TLL_RF)
												)));
				break;
			case SpecialtyAutoConstants.SUBPCY_COV_CODE_WLL:
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_WLL_LIMIT, null, cov.getLimit1()*1.0)); 
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_WLL_LIMITFACTOR, null, cov.getLimit1()>250000.0 ? 1.0 : 0)); 				
				covrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_WLL_RATEFACTOR, null,
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getCoverageExtensionDataValue(cov, SpecialtyAutoConstants.DATAEXT_ID_WLL, SpecialtyAutoConstants.DATAEXT_COLID_WLL_RF)
												)));
				break;
			case SpecialtyAutoConstants.SUBPCY_COV_CODE_EBE:
				break;
			default:
				break;
			}
			
			ratingProcessHelper.rateCoverage(kSession, cov);
		});
		
		cglSubPolicy.getCoverageDetail().getEndorsements().stream().forEach(end -> {
			RateFactorsLink endrfl = end.getRateFactorsLink();
			if(endrfl == null) {
				endrfl = new RateFactorsLink();
				end.setRateFactorsLink(endrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(endrfl));
			endrfl.getRateFactors().clear();
			
			//endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			endrfl.addRateFactor(new RateFactor(NonFleetCGLRateConstants.RFID_CGL_POWERUNITS, null, cglSubPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode("BIPD") != null ? Double.valueOf(cglSubPolicy.getCoverageDetail().getActiveOrPendingCoverageByCode("BIPD").getNumOfUnits()): null));			
			ratingProcessHelper.rateEndorsement(kSession, end);
		});		
	}

}
