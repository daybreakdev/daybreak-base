package com.echelon.rating.nonfleet.auto;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.entities.ExtensionData;
import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.rating.dto.TowingVehicleDTO;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingProcessHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.towing.RateFactorsLinkDto;

public class NonFleetAutoRateProcessVer1 {
	private static final Logger log = Logger.getLogger(NonFleetAutoRateProcessVer1.class.getName());
	
	private KieSession kSession;
	private NonFleetPolicyRatingProcessHelper ratingProcessHelper;

	public NonFleetAutoRateProcessVer1(KieSession kSession) {
		super();
		this.kSession = kSession;
		this.ratingProcessHelper = new NonFleetPolicyRatingProcessHelper();
	}

	@SuppressWarnings("unchecked")
	public void rateAuto(SpecialtyAutoPackage inspol, SpecialityAutoSubPolicy<?> subPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors,
							HashMap<String, Set<Coverage>> hmAddlCoverages) {
		FactHandle fhSubpolicy = kSession.insert(subPolicy);
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) subPolicy;
		autoSubPolicy.getSubPolicyRisks().stream().filter(o -> o.isActive() || o.isPending()).forEach(veh -> {
			preRateCoverage(veh);
			
			// Get the principal driver
			List<FactHandle> fhs = ratingProcessHelper.insertVehicleDriver(kSession, veh);
			FactHandle fhRisk = kSession.insert(veh);
			fhs.add(fhRisk);
			
			SpecialtyVehicleRisk towingVeh = ratingProcessHelper.getTowingVehicle((SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) subPolicy, veh);
			if (towingVeh != null) {
				TowingVehicleDTO vehicleDTO = new TowingVehicleDTO(towingVeh);
				FactHandle fhTvehDTO = kSession.insert(vehicleDTO);
				fhs.add(fhTvehDTO);
			}
			
			rateCoverage(veh, inspol, autoSubPolicy, commRate, allRateFactors, hmAddlCoverages);
			
			for(FactHandle fh : fhs) {
				kSession.delete(fh);
			}
			
			postRateCoverage(veh);
		});
		
		kSession.delete(fhSubpolicy);
		
		// Finalize additional coverages values
		this.ratingProcessHelper.finalizeAddlCoverages(hmAddlCoverages);
	}
	
	private void preRateCoverage(SpecialtyVehicleRisk veh) {
		Coverage pdCoverage = veh.getRiskCoverages().stream()
									.filter(o -> (o.isActive() || o.isPending()) &&
												 SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD.equalsIgnoreCase(o.getCoverageCode()))
									.findFirst().orElse(null);
		if (pdCoverage != null) {
			Endorsement opcf8Endorsement = veh.getRiskEndorsements().stream()
												.filter(o -> (o.isActive() || o.isPending()) &&
															SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF8.equalsIgnoreCase(o.getEndorsementCd()))
												.findFirst().orElse(null);
			if (opcf8Endorsement != null) {
				pdCoverage.setDeductible1(opcf8Endorsement.getDeductible1amount());
			}
		}
	}
	
	private void postRateCoverage(SpecialtyVehicleRisk veh) {
		Coverage pdCoverage = veh.getRiskCoverages().stream()
									.filter(o -> (o.isActive() || o.isPending()) &&
												 SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD.equalsIgnoreCase(o.getCoverageCode()))
									.findFirst().orElse(null);
		if (pdCoverage != null) {
			pdCoverage.setDeductible1(null);
		}
	}
	
	private void rateCoverage(SpecialtyVehicleRisk veh, SpecialtyAutoPackage inspol, SubPolicy<?> subPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors,
								HashMap<String, Set<Coverage>> hmAddlCoverages) {
		log.info("Rate vehicle="+veh.getDescription());
		
		Endorsement end27B = veh.getCoverageDetail().getActiveOrPendingEndorsementByCode(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B);
		
		veh.getCoverageDetail().getCoverages().stream().forEach(cov -> {
			
			RateFactorsLink covrfl = cov.getRateFactorsLink();
			if(covrfl == null) {
				covrfl = new RateFactorsLink();
				cov.setRateFactorsLink(covrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(covrfl));
			covrfl.getRateFactors().clear();
			
			setupCommonRatefactors(inspol, veh, commRate, covrfl);
			
			//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, cov.getCoveragePremium().getPremiumModifier1() != null ? cov.getCoveragePremium().getPremiumModifier1() : 1));
			//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			//covrfl.addRateFactor(new RateFactor(NonFleetAutoRateConstants.RFID_CLAIMSSURCH_OVERRIDE, null, veh.getCriminalConvictionSurchargeOverride()!=null ? Double.valueOf(veh.getCriminalConvictionSurchargeOverride()) : null));
			//covrfl.addRateFactor(new RateFactor(NonFleetAutoRateConstants.RFID_CRIMCONVSURCH_OVERRIDE, null, veh.getCriminalConvictionSurchargeOverride()!=null ? Double.valueOf(veh.getCriminalConvictionSurchargeOverride()) : null));
			//covrfl.addRateFactor(new RateFactor(NonFleetAutoRateConstants.RFID_MAJORCONVSURCH_OVERRIDE, null, veh.getCriminalConvictionSurchargeOverride()!=null ? Double.valueOf(veh.getCriminalConvictionSurchargeOverride()) : null));
			//covrfl.addRateFactor(new RateFactor(NonFleetAutoRateConstants.RFID_MINORCONVSURCH_OVERRIDE, null, veh.getCriminalConvictionSurchargeOverride()!=null ? Double.valueOf(veh.getCriminalConvictionSurchargeOverride()) : null));
			
			switch (cov.getCoverageCode()) {
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_BI: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_DCPD: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AB:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_UA: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_SP:
				//covrfl.addRateFactor(new RateFactor((cov.getCoverageCode()+SpecialtyAutoConstants.RFID_DED), null, cov.getDeductible1() != null ? Double.valueOf(cov.getDeductible1()) : null));				
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP:
				prepareAPSupport(cov, veh, hmAddlCoverages);
				System.out.println(veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP").getCoveragePremium().getOriginalPremium());
				break;
				
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IR:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_MRC:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_OCI:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CHM:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_DF:
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_DC:
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_ABPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AB") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AB").getCoveragePremium().getOriginalPremium():null));
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IB:
				/* Premium is not read at this point, moved to NonFleetAutoRatingVer1.rateIB
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_ABPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AB") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AB").getCoveragePremium().getOriginalPremium():null));
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_IRPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("IR") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("IR").getCoveragePremium().getOriginalPremium():null));
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_MRCPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("MRC") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("MRC").getCoveragePremium().getOriginalPremium():null));
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_OCIPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("OCI") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("OCI").getCoveragePremium().getOriginalPremium():null));
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CHMPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CHM") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CHM").getCoveragePremium().getOriginalPremium():null));

				double abPrem = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AB").getCoveragePremium().getOriginalPremium();
				double irPrem = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("IR").getCoveragePremium().getOriginalPremium();
				double mrcPrem = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("MRC").getCoveragePremium().getOriginalPremium();
				double ociPrem = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("OCI").getCoveragePremium().getOriginalPremium();
				double chmPrem = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CHM").getCoveragePremium().getOriginalPremium();
				
				veh.getCoverageDetail().getActiveOrPendingCoverageByCode("IB").getCoveragePremium().setOriginalPremium(0.365 * (abPrem + irPrem + mrcPrem + ociPrem + chmPrem));
				*/
				prepareIBSupport(veh, hmAddlCoverages);
				break;					
			default:
				break;
			}

			// Do not want to include those rate factors being added from decision tables
			prepare27Bsupport(end27B, cov, veh, hmAddlCoverages);
			
			FactHandle fhCov = kSession.insert(cov);
			kSession.fireAllRules();
			kSession.delete(fhCov);
		});
		
		veh.getCoverageDetail().getEndorsements().stream().forEach(end -> {
			
			RateFactorsLink endrfl = end.getRateFactorsLink();
			if(endrfl == null) {
				endrfl = new RateFactorsLink();
				end.setRateFactorsLink(endrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(endrfl));
			endrfl.getRateFactors().clear();
			
			//setupEndCommonRatefactors(inspol, veh, commRate, endrfl);
			
			//endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, end.getEndorsementPremium().getPremiumModifier1() != null ? end.getEndorsementPremium().getPremiumModifier1() : 1));
			//endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			
			switch (end.getEndorsementCd()) {
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF20: case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF43: 
				//endrfl.addRateFactor(new RateFactor((SpecialtyAutoConstants.RFID_OPCF38_LIM), null, end.getLimit1amount() != null ? Double.valueOf(end.getLimit1amount()) : null));
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF48:
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_BIPREM_NOSURCH, null, 
									veh.getCoverageDetail().getActiveOrPendingCoverageByCode("BI") != null && veh.getTotalSurcharge() != null && veh.getTotalSurcharge().doubleValue() != 0
									? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("BI").getCoveragePremium().getOriginalPremium()/veh.getTotalSurcharge()
									: null)); 
				break;	
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_EON20G:
				//endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EON20GDED, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP") != null ? Double.valueOf(veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP").getDeductible1()):null));
							
				Coverage APcov = veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP");
				if(APcov!=null) {
					endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EON20GDED, null, Double.valueOf(APcov.getDeductible1())));
					end.setDeductible1amount(APcov.getDeductible1());
				}
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF38:
				endrfl.addRateFactor(new RateFactor((SpecialtyAutoConstants.RFID_OPCF38_LIM), null, end.getLimit1amount() != null ? Double.valueOf(end.getLimit1amount()) : null));
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B:
				/*
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_APPREM_NOSURCH, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP").getCoveragePremium().getOriginalPremium()/veh.getTotalSurcharge():null)); 
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_SPPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("SP") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("SP").getCoveragePremium().getOriginalPremium():null));
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLPREM_NOSURCH, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CL") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CL").getCoveragePremium().getOriginalPremium()/veh.getTotalSurcharge():null));
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CMPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CM") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CM").getCoveragePremium().getOriginalPremium():null));
				*/
				break;
			default:
				break;
			}

			FactHandle fhEnd = kSession.insert(end);
			kSession.fireAllRules();
			kSession.delete(fhEnd);
			if(end.getEndorsementCd().equalsIgnoreCase("EON20G")) {
				end.setDeductible1amount(null);
			}	
		});		
	}
	
	private void prepareAPSupport(Coverage cov, SpecialtyVehicleRisk veh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		//key of the groups = APAP
		String srcParentKey = ratingProcessHelper.getAPSupportCoveragesSrcParentKey(veh);
		doPrepareAPSupport(srcParentKey, cov, veh, hmAddlCoverages, null);
	}
	
	private void doPrepareAPSupport(String srcParentKey, Coverage cov, SpecialtyVehicleRisk veh, HashMap<String, Set<Coverage>> hmAddlCoverages,
									PolicyRatingProcessHelper.CloneAddlCoverageAction cloneAddlCoverageAction) {
		// AP requires CL and CM but using AP's values e.g. limit and deductible
		// Without affecting the existing CL and CM, these coverage are cloned from AP, so they have the same values 
		Coverage CLcov = ratingProcessHelper.addAndRateAddlCoverage(kSession, srcParentKey, cov, SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL, hmAddlCoverages, cloneAddlCoverageAction);
		Coverage CMcov = ratingProcessHelper.addAndRateAddlCoverage(kSession, srcParentKey, cov, SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM, hmAddlCoverages, cloneAddlCoverageAction);
	}
	
	private void prepare27Bsupport(Endorsement end27B, Coverage cov, SpecialtyVehicleRisk veh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		// 27B requires AP/CL/CM/SP but using AP's values e.g. limit and deductible
		// Without affecting the existing CL and CM, these coverage are cloned. 
		// 278 is an endorsement; a new interface is being used to copy some endorsement values to coverages
		if (end27B != null &&
			Arrays.asList(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP,
							SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL,
							SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM,
							SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_SP).contains(cov.getCoverageCode())) {
			
			ExtensionData dxRateGroup = end27B.getDataExtension().findDataExtensionData(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_ID_27B, SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_SelRG);
			
			PolicyRatingProcessHelper.CloneAddlCoverageAction cloneAddlCoverageAction = new PolicyRatingProcessHelper.CloneAddlCoverageAction() {
									
									@Override
									public void cloneCompleted(Coverage covClone) {
										covClone.setDeductible1(end27B.getDeductible1amount());
										covClone.setLimit1(end27B.getLimit1amount());
										if (dxRateGroup != null) {
											covClone.getRateFactorsLink().addRateFactor(new RateFactor(SpecialtyAutoConstants.SPEC_VEH_ENDORSEMENT_OPCF27B_DATA_EXT_27B_COL_SelRG, dxRateGroup.getColumnValue(), null));
										}
									}
								};
			
			// Use the current coverage code AP,CL,CM,SP to run decision table
			// Copy deductible, limit, data extension to coverages
			String srcParentKey = ratingProcessHelper.get27BSupportCoveragesSrcParentKey(veh);
			Coverage e27BTypeConv = ratingProcessHelper.addAndRateAddlCoverage(kSession, srcParentKey, cov, cov.getCoverageCode(), hmAddlCoverages, cloneAddlCoverageAction);
			
			// AP rating needs its own supporting coverages
			// Creates OPCF27BAPCL, OPCF27BAPCM
			if (SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP.equalsIgnoreCase(cov.getCoverageCode())) {
				String srcAPParentKey = ratingProcessHelper.get27BAPSupportCoveragesSrcParentKey(veh);
				doPrepareAPSupport(srcAPParentKey, e27BTypeConv, veh, hmAddlCoverages, cloneAddlCoverageAction);
				Set<Coverage> apCovs = ratingProcessHelper.getAddlCoverages(srcAPParentKey, hmAddlCoverages);
				if (apCovs != null && !apCovs.isEmpty()) {
					// Rename coverage for drl and more decision tables
					apCovs.stream().forEach(o -> {
						String origCode = o.getCoverageCode();
						// OPCF27BCL and OPCF27BCM
						o.setCoverageCode(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B+origCode);
						ratingProcessHelper.rateCoverage(kSession, o);
						// OPCF27BAPCL and OPCF27BAPCM
						o.setCoverageCode(NonFleetAutoRateConstants.COVERAGE_CODE_27BAP+origCode);
					});
				}
			}
		
			// Rename coverage for drl and more decision tables
			// OPCF27BCL, OPCF27BCM, OPCF27BSP, OPCF27BAP
			e27BTypeConv.setCoverageCode(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B+cov.getCoverageCode());
			ratingProcessHelper.rateCoverage(kSession, e27BTypeConv);
		}
	}
	
	private void prepareIBSupport(SpecialtyVehicleRisk veh, HashMap<String, Set<Coverage>> hmAddlCoverages) {
		String srcParentKey = ratingProcessHelper.getIBPSupportCoveragesSrcParentKey(veh);
		
		for(String depCode : NonFleetAutoRateConstants.NonFleetRating_IB_Dependencies) {
			Coverage depCov = veh.getCoverageDetail().getActiveOrPendingCoverageByCode(depCode);
			if (depCov != null) {
				ratingProcessHelper.addAddlCoverage(srcParentKey, depCov, hmAddlCoverages);
			}
		}
	}

	private void setupCommonRatefactors(SpecialtyAutoPackage inspol, SpecialtyVehicleRisk veh, Double commRate, RateFactorsLink covrfl) {
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_PROVINCE, inspol.getControllingJurisdiction(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_TERRITORY, inspol.getTerritoryCode(),null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_RURALURBAN, inspol.getRuralityInd(),null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_TYPE_OF_VEHICLE, veh.getVehicleType(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_VEHICLEDESC, veh.getVehicleDescription(), null));		
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLASS,veh.getVehicleClass(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_VRG,veh.getSelectedRateGroup(), null));
	}
}