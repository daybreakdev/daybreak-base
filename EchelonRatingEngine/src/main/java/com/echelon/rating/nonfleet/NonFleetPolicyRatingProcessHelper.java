package com.echelon.rating.nonfleet;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.domain.policy.specialtylines.VehicleDriver;
import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.rating.nonfleet.auto.NonFleetAutoRateConstants;
import com.echelon.utils.SpecialtyAutoConstants;

public class NonFleetPolicyRatingProcessHelper extends PolicyRatingProcessHelper {

	public NonFleetPolicyRatingProcessHelper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<FactHandle> insertVehicleDriver(KieSession kSession, SpecialtyVehicleRisk veh) {
		List<FactHandle> fhs = new ArrayList<FactHandle>();
		
		// Get the principal driver
		/**
		if (veh.getVehicleDrivers() != null) {
			VehicleDriver pvdr = veh.getVehicleDrivers().stream().filter(vdr -> (vdr.isActive() || vdr.isPending()) &&
														( 	SpecialtyAutoConstants.ATTACHDRIVER_TYPE_PRINCIPAL.equalsIgnoreCase(vdr.getDriverType()) ||
															SpecialtyAutoConstants.ATTACHDRIVER_TYPE_SECONDARY.equalsIgnoreCase(vdr.getDriverType()))
														) .findFirst().orElse(null);
			if (pvdr != null && pvdr.getDriver() != null) {
				FactHandle fhpvdr = kSession.insert(pvdr);
				FactHandle fhpdr = kSession.insert(pvdr.getDriver());
				fhs.add(fhpvdr);
				fhs.add(fhpdr);
			}
		}  		**/
		
		if (veh.getVehicleDrivers() != null) {
			VehicleDriver pvdr = veh.getVehicleDrivers().stream().filter(vdr -> (vdr.isActive() || vdr.isPending()) &&
														SpecialtyAutoConstants.ATTACHDRIVER_TYPE_PRINCIPAL.equalsIgnoreCase(vdr.getDriverType()))
												.findFirst().orElse(null);
			if (pvdr != null && pvdr.getDriver() != null) {
				FactHandle fhpvdr = kSession.insert(pvdr);
				FactHandle fhpdr = kSession.insert(pvdr.getDriver());
				fhs.add(fhpvdr);
				fhs.add(fhpdr);
			}
			
			VehicleDriver svdr = veh.getVehicleDrivers().stream().filter(vdr -> (vdr.isActive() || vdr.isPending()) &&
														SpecialtyAutoConstants.ATTACHDRIVER_TYPE_SECONDARY.equalsIgnoreCase(vdr.getDriverType()))
												.findFirst().orElse(null);
			if (svdr != null && svdr.getDriver() != null) {
				FactHandle fhsvdr = kSession.insert(svdr);
				FactHandle fhsdr = kSession.insert(svdr.getDriver());
				fhs.add(fhsvdr);
				fhs.add(fhsdr);
			}
		}		
		
		return fhs;
	}
	
	public SpecialtyVehicleRisk getTowingVehicle(SpecialityAutoSubPolicy<SpecialtyVehicleRisk> apSubPolicy, SpecialtyVehicleRisk trailer) {
		SpecialtyVehicleRisk result = null;
		
		if (trailer.getTowingVehiclePK() != null && trailer.getTowingVehiclePK() > 0) {
			result = apSubPolicy.findSubPcyRiskByPk(trailer.getTowingVehiclePK());
		}
		
		return result;
	}
	
	private String getVehicleCoverageSrcParentKey(String srcCovCode, SpecialtyVehicleRisk veh) {
		String srcParentKey = srcCovCode+"-"+veh.getPolicyRiskPK().toString();
		return srcParentKey;
	}
	
	public String getAPSupportCoveragesSrcParentKey(SpecialtyVehicleRisk veh) {
		return getVehicleCoverageSrcParentKey(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP, veh);
	}
	
	public String get27BSupportCoveragesSrcParentKey(SpecialtyVehicleRisk veh) {
		return getVehicleCoverageSrcParentKey(SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B, veh);
	}
	
	public String get27BAPSupportCoveragesSrcParentKey(SpecialtyVehicleRisk veh) {
		return getVehicleCoverageSrcParentKey(NonFleetAutoRateConstants.COVERAGE_CODE_27BAP, veh);
	}
	
	public String getIBPSupportCoveragesSrcParentKey(SpecialtyVehicleRisk veh) {
		return getVehicleCoverageSrcParentKey(SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_IB, veh);
	}
}
