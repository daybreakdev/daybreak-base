package com.echelon.rating.nonfleet.cgo;

import java.util.List;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.utils.SpecialtyAutoConstants;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelper;
import com.echelon.rating.nonfleet.NonFleetPolicyRatingHelperFactory;
import com.echelon.rating.nonfleet.cgo.NonFleetCGORateConstants;
import com.rating.towing.RateFactorsLinkDto;

public class NonFleetCGORateProcessVer1 {
	private static final Logger log = Logger.getLogger(NonFleetCGORateProcessVer1.class.getName());
	
	private KieSession kSession;
	private PolicyRatingProcessHelper ratingProcessHelper;
	
	public NonFleetCGORateProcessVer1(KieSession kSession) {
		super();
		this.kSession = kSession;
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
	}

	public void rateCGO(CargoSubPolicy<?> cargoSubPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		FactHandle fhcargoSubpolicy = kSession.insert(cargoSubPolicy);

		rateCoverage(cargoSubPolicy, commRate, allRateFactors);
		kSession.delete(fhcargoSubpolicy);
	}
		
	private void rateCoverage(CargoSubPolicy<?> cargoSubPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		cargoSubPolicy.getCoverageDetail().getCoverages().stream().forEach(cov -> {
			
			RateFactorsLink covrfl = cov.getRateFactorsLink();
			if(covrfl == null) {
				covrfl = new RateFactorsLink();
				cov.setRateFactorsLink(covrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(covrfl));
			covrfl.getRateFactors().clear();
			

			//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			
			switch (cov.getCoverageCode()) {
			case SpecialtyAutoConstants.SUBPCY_COV_CODE_MTCC:
				covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, cov.getCoveragePremium().getPremiumModifier1() != null ? cov.getCoveragePremium().getPremiumModifier1() : 1));
				//CGO Power Units can be zero
				covrfl.addRateFactor(new RateFactor(NonFleetCGORateConstants.RFID_CGO_POWERUNITS, null, cov.getNumOfUnits() != null ? Double.valueOf(cov.getNumOfUnits()) : null));			
				covrfl.addRateFactor(new RateFactor(NonFleetCGORateConstants.RFID_CGOPARTIALPREM, null, cargoSubPolicy.getTotalCargoPartialPremium() != null ? cargoSubPolicy.getTotalCargoPartialPremium() : Double.valueOf(0)));				
				break;
			/**	
			case SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC: 
			covrfl.addRateFactor(new RateFactor(LHTCGORateConstants.RFID_TLL_LIMIT, null, cov.getLimit1()*1.0)); 
				covrfl.addRateFactor(new RateFactor(LHTCGORateConstants.RFID_TLL_LIMITFACTOR, null, cov.getLimit1()>250000.0 ? 1.0 : 0)); 				
				covrfl.addRateFactor(new RateFactor(LHTCGORateConstants.RFID_RATEFACTOR, null,
											PolicyRatingHelper.getInstance().convertStringToDouble(
												PolicyRatingHelper.getInstance().getCoverageExtensionDataValue(cov, SpecialtyAutoConstants.DATAEXT_ID_TLL, SpecialtyAutoConstants.DATAEXT_COLID_TLL_RF)
												)));
				break;**/
			default:
				break;
			}
			
			ratingProcessHelper.rateCoverage(kSession, cov);
		});
		
		cargoSubPolicy.getCoverageDetail().getEndorsements().stream().forEach(end -> {
			RateFactorsLink endrfl = end.getRateFactorsLink();
			if(endrfl == null) {
				endrfl = new RateFactorsLink();
				end.setRateFactorsLink(endrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(endrfl));
			endrfl.getRateFactors().clear();
			
			//setupCommonRatefactors(commRate, expRateFactors, endrfl);
			//endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			
			NonFleetPolicyRatingHelper policyRatingHelper = (NonFleetPolicyRatingHelper) NonFleetPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper();
			switch (end.getEndorsementCd()) {
			case SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_CTC: 	
				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_CTC_LIM), null, end.getLimit1amount() != null ? Double.valueOf(end.getLimit1amount()) : null));
				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_CTC_Revenue), null,  
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getEndorsementExtensionDataValue(end, SpecialtyAutoConstants.DATAEXT_ID_CTC, SpecialtyAutoConstants.DATAEXT_COLID_CTC_REV)
								)));

				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_CTC_Rate), null, 
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getEndorsementExtensionDataValue(end, SpecialtyAutoConstants.DATAEXT_ID_CTC, SpecialtyAutoConstants.DATAEXT_COLID_CTC_RF)
							)));
				break;
			case SpecialtyAutoConstants.CARGO_SUBPCY_ENDORSEMENT_TTC: 	
				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_TTC_LIM), null, end.getLimit1amount() != null ? Double.valueOf(end.getLimit1amount()) : null));
				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_TTC_NumDays), null,  
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getEndorsementExtensionDataValue(end, SpecialtyAutoConstants.DATAEXT_ID_TTC, SpecialtyAutoConstants.DATAEXT_COLID_TTC_Days)
								)));

				endrfl.addRateFactor(new RateFactor((NonFleetCGORateConstants.RFID_TTC_SurchDiscount), null, 
						policyRatingHelper.convertStringToDouble(
								policyRatingHelper.getEndorsementExtensionDataValue(end, SpecialtyAutoConstants.DATAEXT_ID_TTC, SpecialtyAutoConstants.DATAEXT_COLID_TTC_SurDisc)
							)));
				break;				
			default:
				break;
			}
			
			ratingProcessHelper.rateEndorsement(kSession, end);
		});
	}
}