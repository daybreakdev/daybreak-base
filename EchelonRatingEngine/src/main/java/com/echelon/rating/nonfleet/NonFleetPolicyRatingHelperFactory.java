package com.echelon.rating.nonfleet;

import com.rating.utils.CorePolicyRatingHelperFactory;
import com.rating.utils.ICorePolicyRatingHelper;

public class NonFleetPolicyRatingHelperFactory extends CorePolicyRatingHelperFactory {
	private static final long serialVersionUID = 618857045694000103L;

	private static NonFleetPolicyRatingHelperFactory instance;
	
	private ICorePolicyRatingHelper policyRatingHelper;
	
	private NonFleetPolicyRatingHelperFactory() {
		policyRatingHelper = new NonFleetPolicyRatingHelper();
	}
	
	public static NonFleetPolicyRatingHelperFactory getInstance() {
		if (instance == null) {
			synchronized(NonFleetPolicyRatingHelperFactory.class) {
				instance = new NonFleetPolicyRatingHelperFactory();
			}
		}
		return instance;
	}
	
	public ICorePolicyRatingHelper getPolicyRatingHelper() {
		return policyRatingHelper;
	}
}
