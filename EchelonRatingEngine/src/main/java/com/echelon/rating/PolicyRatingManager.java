package com.echelon.rating;

import com.echelon.prodconf.ProductConfigFactory;
import com.ds.ins.prodconf.helpers.BaseProductConfigFactory;
import com.rating.coreclasses.CorePolicyRatingManager;

public class PolicyRatingManager extends CorePolicyRatingManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 600290573158296012L;

	public PolicyRatingManager() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected BaseProductConfigFactory getProductConfigFactory() {
		// TODO Auto-generated method stub
		return ProductConfigFactory.getInstance();
	}

}
