package com.echelon.rating.lht.auto;

public class LHTAutoRateConstants {

	// Ratefactor ID
	public static final String RFID_UWADJUSTMENT				= "UWAdjustment";
	public static final String RFID_COMMISSION_FACTOR			= "CommFactor";
	public static final String RFID_PROVINCE 					= "Province";
	public static final String RFID_LIMIT 						= "Limit";
	public static final String RFID_YEARS_IN_BUSINESS 			= "Years in business";
	public static final String RFID_EXPOSURE_ZONE1 				= "Exposure Zone1";
	public static final String RFID_EXPOSURE_ZONE2 				= "Exposure Zone2";
	public static final String RFID_EXPOSURE_ZONE3 				= "Exposure Zone3";
	public static final String RFID_EXPOSURE_ZONE4 				= "Exposure Zone4";
	public static final String RFID_EXPOSURE_ZONE5 				= "Exposure Zone5";
	public static final String RFID_EXPOSURE_ZONE6 				= "Exposure Zone6";
	public static final String RFID_EXPOSURE_ZONE7 				= "Exposure Zone7";
	public static final String RFID_EXPOSURE_ZONE8 				= "Exposure Zone8";
	public static final String RFID_EXPOSURE_ZONE9 				= "Exposure Zone9";
	public static final String RFID_CLAIM_FREQUENCY 			= "Claim Frequency";
	public static final String RFID_DANGEROUS_MATERIALS 		= "Dangerous Materials";
	public static final String RFID_COMMERCIAL_CREDIT_SCORE 	= "Commercial Credit Score";
	public static final String RFID_PREVENTION_RATING			= "Prevention rating";
	public static final String RFID_TYPE_OF_VEHICLE 			= "Type of Vehicle";
	public static final String RFID_AVERAGE_KMS_X1  			= "Average Kms X1";
	public static final String RFID_AVERAGE_KMS_X2  			= "Average Kms X2";
	public static final String RFID_AVERAGE_KMS_Y1  			= "Average Kms Y1";
	public static final String RFID_AVERAGE_KMS_Y2  			= "Average Kms Y2";	
	public static final String RFID_AVERAGE_KMS 				= "Average Kms";	
	public static final String RFID_IRP 						= "IRP";
	public static final String RFID_ACTUAL_CASH_VALUE 			= "Actual Cash Value";
	public static final String RFID_CLASS 						= "Class";
	public static final String RFID_VRG 						= "VRG";
	public static final String RFID_NUM_OF_VEHICLES 			= "# of Vehicles";
	public static final String RFID_DED 						= "Ded";
	public static final String RFID_MAX_COMMISSION				= "Max Commission";
	public static final String RFID_SELECTED_COMMISSION_RATE	= "Selected Commission Rate";
	public static final String RFID_COMM_ADJUSTMENT_FACTOR 		= "Commission Adjustment Factor";
	public static final String RFID_MODIFICATION_FACTOR 		= "Modification Factor on Policy";
	public static final String RFID_UW_FLEX 					= "UW Flex for auto coverages";
	
	// Values from decision table
	public static final String RFID_BASERATE 			= "BASERATE";
	public static final String RFID_LIMITFACTOR 		= "LIMITFACTOR";
	public static final String RFID_DEDPHYFACTOR		= "DEDPHYFACTOR";
	public static final String RFID_DEDLIABFACTOR		= "DEDLIABFACTOR";
	public static final String RFID_DEDFACTOR 			= "DEDFACTOR";
	public static final String RFID_CLASSFACTOR 		= "CLASSFACTOR";
	public static final String RFID_EXPFACTORZN1 		= "EXPFACTORZN1";
	public static final String RFID_EXPFACTORZN2 		= "EXPFACTORZN2";
	public static final String RFID_EXPFACTORZN3 		= "EXPFACTORZN3";
	public static final String RFID_EXPFACTORZN4 		= "EXPFACTORZN4";
	public static final String RFID_EXPFACTORZN5 		= "EXPFACTORZN5";
	public static final String RFID_EXPFACTORZN6 		= "EXPFACTORZN6";
	public static final String RFID_EXPFACTORZN7 		= "EXPFACTORZN7";
	public static final String RFID_EXPFACTORZN8 		= "EXPFACTORZN8";
	public static final String RFID_EXPFACTORZN9 		= "EXPFACTORZN9";
	public static final String RFID_EXPFACTORZN 		= "EXPFACTORZN";
	public static final String RFID_VRGFACTOR			= "VRGFACTOR";
	public static final String RFID_CLMFREQFACTOR 		= "CLMFREQFACTOR";
	public static final String RFID_YRSINBUSFACTOR 		= "YRSINBUSFACTOR";
	public static final String RFID_DANGMATSFACTOR 		= "DANGMATSFACTOR";
	public static final String RFID_PREVRATEFACTOR 		= "PREVRATEFACTOR";
	public static final String RFID_CCSCOREFACTOR 		= "CCSCOREFACTOR";
	public static final String RFID_AVGKMSX1  			= "AVGKMSX1FACTOR";
	public static final String RFID_AVGKMSX2  			= "AVGKMSX2FACTOR";
	public static final String RFID_AVGKMSY1  			= "AVGKMSY1FACTOR";
	public static final String RFID_AVGKMSY2  			= "AVGKMSY2FACTOR";		
	public static final String RFID_AVGKMSFACTOR 		= "AVGKMSFACTOR";	
	public static final String RFID_VEHTYPEFACTOR 		= "VEHTYPEFACTOR";
	public static final String RFID_NUMVEHSFACTOR 		= "NUMVEHSFACTOR";
	public static final String RFID_COMMADJFACTOR 		= "COMMADJFACTOR";
	public static final String RFID_OFFBALFACTOR		= "OFFBALFACTOR";
	public static final String RFID_OFFBALFACTOR_ENDR 	= "OFFBALFACTORENDR";
	public static final String RFID_ENDORSEMENTPREMIUM 	= "ENDORSEMENTPREMIUM";

}
