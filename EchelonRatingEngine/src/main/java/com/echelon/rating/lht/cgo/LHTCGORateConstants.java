package com.echelon.rating.lht.cgo;

public class LHTCGORateConstants {

	// Ratefactor ID
	public static final String RFID_UWADJUSTMENT				= "UWAdjustment";
	public static final String RFID_COMMISSION_FACTOR			= "CommFactor";
	public static final String RFID_MAX_COMMISSION				= "Max Commission";
	public static final String RFID_SELECTED_COMMISSION_RATE	= "Selected Commission Rate";
	public static final String RFID_MODIFICATION_FACTOR 		= "Modification Factor on Policy";	
	public static final String RFID_COMM_ADJUSTMENT_FACTOR 		= "Commission Adjustment Factor";
	public static final String RFID_CGO_POWERUNITS 				= "Cargo Power Units";

	// Values from decision table
	public static final String RFID_CGOPARTIALPREM 		= "CGOPARTIALPREM";
	public static final String RFID_CGOLIMITFACTOR 		= "CGOLIMITFACTOR";
	public static final String RFID_CGODEDFACTOR 		= "CGODEDFACTOR";
	public static final String RFID_CGOPREVRATEFACTOR 	= "PREVRATEFACTOR";	
	public static final String RFID_OFFBALFACTOR		= "OFFBALFACTOR";
	public static final String RFID_CGOEARNEDFRFACTOR	= "CGOEARNEDFRFACTOR";
	public static final String RFID_CTC_LIM				= "CTC_LIM";
	public static final String RFID_CTC_Revenue			= "CTC_Revenue";	
	public static final String RFID_CTC_Rate			= "CTC_Rate";
	public static final String RFID_TTC_LIM				= "TTC_LIM";	
	public static final String RFID_TTC_NumDays			= "TTC_NumDays";
	public static final String RFID_TTC_SurchDiscount	= "TTC_SurchargeDiscount";	
	public static final String RFID_CARGOTTCBASE		= "CARGOTTCBASE";
}
