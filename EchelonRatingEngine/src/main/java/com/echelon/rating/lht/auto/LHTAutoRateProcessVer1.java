package com.echelon.rating.lht.auto;

import java.util.List;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.Endorsement;
import com.ds.ins.domain.policy.RateFactor;
import com.ds.ins.domain.policy.RateFactorsLink;
import com.ds.ins.domain.policy.SubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.domain.policy.specialtylines.SpecialtyVehicleRisk;
import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.towing.RateFactorsLinkDto;

public class LHTAutoRateProcessVer1 {
	private static final Logger log = Logger.getLogger(LHTAutoRateProcessVer1.class.getName());
	
	private KieSession kSession;
	private PolicyRatingProcessHelper ratingProcessHelper;

	public LHTAutoRateProcessVer1(KieSession kSession) {
		super();
		this.kSession = kSession;
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
	}

	@SuppressWarnings("unchecked")
	public void rateAuto(SpecialtyAutoPackage inspol, SpecialityAutoExpRating expRateFactors, SpecialityAutoSubPolicy<?> subPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		FactHandle fhSubpolicy = kSession.insert(subPolicy);
		
		SpecialityAutoSubPolicy<SpecialtyVehicleRisk> autoSubPolicy = (SpecialityAutoSubPolicy<SpecialtyVehicleRisk>) subPolicy;
		autoSubPolicy.getSubPolicyRisks().stream().filter(o -> o.isActive() || o.isPending()).forEach(veh -> {
			preRateCoverage(veh);
			
			FactHandle fhRisk = kSession.insert(veh);
			rateCoverage(veh, inspol, expRateFactors, autoSubPolicy, commRate, allRateFactors);
			kSession.delete(fhRisk);
			
			postRateCoverage(veh);
		});
		
		kSession.delete(fhSubpolicy);
	}
	
	private void preRateCoverage(SpecialtyVehicleRisk veh) {
		Coverage pdCoverage = veh.getRiskCoverages().stream()
									.filter(o -> (o.isActive() || o.isPending()) &&
												 SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD.equalsIgnoreCase(o.getCoverageCode()))
									.findFirst().orElse(null);
		if (pdCoverage != null) {
			Endorsement opcf8Endorsement = veh.getRiskEndorsements().stream()
												.filter(o -> (o.isActive() || o.isPending()) &&
															SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF8.equalsIgnoreCase(o.getEndorsementCd()))
												.findFirst().orElse(null);
			if (opcf8Endorsement != null) {
				pdCoverage.setDeductible1(opcf8Endorsement.getDeductible1amount());
			}
		}
	}
	
	private void postRateCoverage(SpecialtyVehicleRisk veh) {
		Coverage pdCoverage = veh.getRiskCoverages().stream()
									.filter(o -> (o.isActive() || o.isPending()) &&
												 SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD.equalsIgnoreCase(o.getCoverageCode()))
									.findFirst().orElse(null);
		if (pdCoverage != null) {
			pdCoverage.setDeductible1(null);
		}
	}
	
	private void rateCoverage(SpecialtyVehicleRisk veh, SpecialtyAutoPackage inspol, SpecialityAutoExpRating expRateFactors, SubPolicy<?> subPolicy, Double commRate, List<RateFactorsLinkDto> allRateFactors) {
		log.info("Rate vehicle="+veh.getDescription());
		
		veh.getCoverageDetail().getCoverages().stream().forEach(cov -> {
			
			RateFactorsLink covrfl = cov.getRateFactorsLink();
			if(covrfl == null) {
				covrfl = new RateFactorsLink();
				cov.setRateFactorsLink(covrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(covrfl));
			covrfl.getRateFactors().clear();
			
			setupCommonRatefactors(inspol, expRateFactors, veh, commRate, covrfl);
			
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, cov.getCoveragePremium().getPremiumModifier1() != null ? cov.getCoveragePremium().getPremiumModifier1() : 1));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			
			switch (cov.getCoverageCode()) {
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_BI: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_DCPD: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_SP: case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP:
				covrfl.addRateFactor(new RateFactor((cov.getCoverageCode()+SpecialtyAutoConstants.RFID_DED), null, cov.getDeductible1() != null ? Double.valueOf(cov.getDeductible1()) : null));
				break;
			default:
				break;
			}

			ratingProcessHelper.rateCoverage(kSession, cov);
		});
		
		veh.getCoverageDetail().getEndorsements().stream().forEach(end -> {
			
			RateFactorsLink endrfl = end.getRateFactorsLink();
			if(endrfl == null) {
				endrfl = new RateFactorsLink();
				end.setRateFactorsLink(endrfl);
			}
			allRateFactors.add(new RateFactorsLinkDto(endrfl));
			endrfl.getRateFactors().clear();
			
			setupEndCommonRatefactors(inspol, expRateFactors, veh, commRate, endrfl);
			
//			endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UWADJUSTMENT, null, end.getEndorsementPremium().getPremiumModifier1() != null ? end.getEndorsementPremium().getPremiumModifier1() : 1));
			endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR, null, (1 - .125) / (1 - (commRate/100))));
			
			switch (end.getEndorsementCd()) {
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF20: case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF43: case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF43A: 
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF44R: case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_EON20A:break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF38:
				endrfl.addRateFactor(new RateFactor((SpecialtyAutoConstants.RFID_OPCF38_LIM), null, end.getLimit1amount() != null ? Double.valueOf(end.getLimit1amount()) : null));
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF27B:
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_APPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("AP").getCoveragePremium().getOriginalPremium():null)); 
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_SPPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("SP") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("SP").getCoveragePremium().getOriginalPremium():null));
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CL") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CL").getCoveragePremium().getOriginalPremium():null));
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CMPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CM") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("CM").getCoveragePremium().getOriginalPremium():null));
				break;
			case SpecialtyAutoConstants.SPEC_VEH_RISK_ENDORSEMENT_OPCF48:
				endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_BIPREM, null, veh.getCoverageDetail().getActiveOrPendingCoverageByCode("BI") != null ? veh.getCoverageDetail().getActiveOrPendingCoverageByCode("BI").getCoveragePremium().getOriginalPremium():null)); 
				break;				
			default:
				break;
			}

			ratingProcessHelper.rateEndorsement(kSession, end);
		});
		
	}
	
	private void setupCommonRatefactors(SpecialtyAutoPackage inspol, SpecialityAutoExpRating expRateFactors, SpecialtyVehicleRisk veh, Double commRate, RateFactorsLink covrfl) {
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_PROVINCE, inspol.getControllingJurisdiction(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_TYPE_OF_VEHICLE, veh.getTypeOfUnit(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_IRP,veh.getIrp() != null && veh.getIrp() == 1 ? "Yes" : "No", null));
		//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_ACTUAL_CASH_VALUE, null, veh.getActualCashValue()));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLASS,veh.getVehicleClass(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_VRG,veh.getSelectedRateGroup(), null));
		covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_NUM_OF_VEHICLES, null, veh.getNumberOfUnits() != null ? Double.valueOf(veh.getNumberOfUnits()) : null));
		//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_MAX_COMMISSION, null, commRate));
		//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_SELECTED_COMMISSION_RATE, null, commRate));
		//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMM_ADJUSTMENT_FACTOR, null, null));
		//covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_UW_FLEX, null, null));
		
		if (expRateFactors != null) {
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_YEARS_IN_BUSINESS, null, expRateFactors.getYearsInBusiness() != null ? Double.valueOf(expRateFactors.getYearsInBusiness()) : null));
			/* not required for now
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE1, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone1Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE2, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone2Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE3, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone3Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE4, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone4Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE5, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone5Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE6, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone6Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE7, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone7Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE8, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone8Exp() : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPOSURE_ZONE9, null, expRateFactors.getZone1Exp() != null ? expRateFactors.getZone9Exp() : null));
			*/
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_AVERAGE_KMS, null, expRateFactors.getAverageKMS()));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_CLAIM_FREQUENCY, null, expRateFactors.getClaimsFrequency()));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_DANGEROUS_MATERIALS, null, expRateFactors.getDangerousMaterials()));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_COMMERCIAL_CREDIT_SCORE, null, expRateFactors.getCommercialCreditScore() != null ? Double.valueOf(expRateFactors.getCommercialCreditScore()) : null));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_PREVENTION_RATING, null, expRateFactors.getPreventionRating()));
			covrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_MODIFICATION_FACTOR, null, expRateFactors.getModificationFactor()));
		}
	}

	
	private void setupEndCommonRatefactors(SpecialtyAutoPackage inspol, SpecialityAutoExpRating expRateFactors, SpecialtyVehicleRisk veh, Double commRate, RateFactorsLink endrfl) {	
		endrfl.addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_NUM_OF_VEHICLES, null, veh.getNumberOfUnits() != null ? Double.valueOf(veh.getNumberOfUnits()) : null));
	}
}