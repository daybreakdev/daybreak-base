package com.echelon.rating.lht;

import com.echelon.rating.rounding.LHTRoundingStrategyFactory;
import com.rating.rounding.RoundingStrategyFactory;
import com.rating.utils.CorePolicyRatingHelper;

public class LHTPolicyRatingHelper extends CorePolicyRatingHelper {
	private static final long serialVersionUID = 373327144058087707L;

	@Override
	protected RoundingStrategyFactory getRoundingStrategyFactory() {
		return LHTRoundingStrategyFactory.getInstance();
	}
}
