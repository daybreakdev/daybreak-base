package com.echelon.rating.lht.auto;

import java.io.Serializable;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.rating.PolicyRatingProcessHelper;

public class LHTAutoRatingVer2 implements Serializable {
	private static final long serialVersionUID = 6434436657931092696L;
	private static final Logger log = Logger.getLogger(LHTAutoRatingVer2.class.getName());
	
	private KieSession calcSession;
	private SpecialityAutoSubPolicy<?> subPolicy;
	private PolicyRatingProcessHelper ratingProcessHelper;
	
	public LHTAutoRatingVer2(KieSession calcSession, SpecialityAutoSubPolicy<?> subPolicy) {
		super();
		
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
		this.calcSession = calcSession;
		this.subPolicy = subPolicy;
	}
	
	public SpecialityAutoSubPolicy<?> rateAuto() {
		if (calcSession != null) {
			subPolicy.getSubPolicyRisks().stream().filter(subPolicyRisk -> subPolicyRisk.isActive() || subPolicyRisk.isPending()).forEach(veh -> {
				log.info("Calculate rating premium, vehicle="+veh.getDisplayString()+"/"+veh.getPolicyRiskPK());
				FactHandle fhVeh = calcSession.insert(veh);
				ratingProcessHelper.rateRisk(calcSession, veh);
				calcSession.delete(fhVeh);
			});
		}
			
		return subPolicy;
	}
}
