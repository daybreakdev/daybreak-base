package com.echelon.rating.lht;

import java.util.ArrayList;
import java.util.List;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.rule.FactHandle;

import com.ds.ins.domain.policy.PolicyTransaction;
import com.ds.ins.exceptions.RatingException;
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;
import com.echelon.domain.policy.specialtylines.SpecialtyAutoPackage;
import com.echelon.rating.PolicyRatingFactory;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.lht.auto.LHTAutoRateProcessVer1;
import com.echelon.rating.lht.auto.LHTAutoRatingVer2;
import com.echelon.rating.lht.cgl.LHTCGLRateProcessVer1;
import com.echelon.rating.lht.cgl.LHTCGLRatingVer1;
import com.echelon.rating.lht.cgo.LHTCGORateProcessVer1;
import com.echelon.rating.lht.cgo.LHTCGORatingVer1;

import com.echelon.utils.SpecialtyAutoConstants;
import com.rating.coreclasses.CorePolicyRatingProcess;
import com.rating.towing.RateFactorsLinkDto;

public class LHTPolicyRateProcessVer1 extends CorePolicyRatingProcess {

	private static final long serialVersionUID = -643870906269111875L;

	private KieSession createKieRatingSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}
	
	private KieSession createKieCalcSession(PolicyTransaction<?> policyTransaction) throws Exception {
		return PolicyRatingFactory.getInstance().getPolicyRatingManager().newKieCalcSession(policyTransaction.getPolicyTerm().getRatebookId(), policyTransaction.getPolicyTerm().getRatebookVersion());
	}

	@Override
	public PolicyTransaction<?> ratePolicyTransaction(PolicyTransaction<?> policyTransaction) throws RatingException {
		KieSession kSession = null;
		KieSession calcSession = null;
		
        try {
        	List<RateFactorsLinkDto> allRateFactors = new ArrayList<RateFactorsLinkDto>();
        	kSession = createKieRatingSession(policyTransaction);
        	if (kSession == null) {
        		throw new RatingException().withResourceCode("ErrorRatingSessionCreate");
        	}
        	
        	calcSession = createKieCalcSession(policyTransaction);
        	/* debug rules
        	calcSession.addEventListener(new DefaultAgendaEventListener() {
        		@Override
        		public void beforeMatchFired(BeforeMatchFiredEvent event) {
        			super.beforeMatchFired(event);
        			
        			Rule rule = event.getMatch().getRule();
        			System.out.println("beforeMatchFired:"+rule.getName());
        		}
        		
        		@Override
        		public void afterMatchFired(AfterMatchFiredEvent event) {
        			super.afterMatchFired(event);
        			
        			Rule rule = event.getMatch().getRule();
        			System.out.println("afterMatchFired:"+rule.getName());
        		}
        	});
        	*/
        	
			SpecialtyAutoPackage inspol = (SpecialtyAutoPackage) policyTransaction.getPolicyVersion().getInsurancePolicy();
			FactHandle fhInsPol = kSession.insert(inspol);
			
			Double commRate = policyTransaction.getPolicyVersion().getProducerCommissionRate();
			
			FactHandle fhExpRateFactors = null;
			SpecialityAutoExpRating expRateFactors = getSpecialityAutoExpRating(policyTransaction);
			if (expRateFactors == null) {
				throw (new RatingException()).withResourceCode("RatingNoExposureRatingError");
			}
			prepareSpecialityAutoExpRating(expRateFactors);
			fhExpRateFactors = kSession.insert(expRateFactors);
			
			final KieSession kieRateSession = kSession;
			policyTransaction.getSubPolicies().stream().forEach(sp -> {
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					(new LHTAutoRateProcessVer1(kieRateSession)).rateAuto(inspol, expRateFactors, (SpecialityAutoSubPolicy<?>) sp, commRate, allRateFactors);
				}
				else if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					// Rate CGL
					(new LHTCGLRateProcessVer1(kieRateSession)).rateCGL(expRateFactors, (CGLSubPolicy<?>) sp, commRate, allRateFactors);
				}
				else if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					// Rate Cargo
					(new LHTCGORateProcessVer1(kieRateSession)).rateCGO(expRateFactors, (CargoSubPolicy<?>) sp, commRate, allRateFactors);
				}
			});
			if (fhExpRateFactors != null) kSession.delete(fhExpRateFactors);
			kSession.delete(fhInsPol);
			
			policyTransaction = ratePolicyDetails(calcSession, policyTransaction);
			finalizeRatingResults(allRateFactors);
        } catch (RuntimeException e) {
        	e.printStackTrace();
        	
        	if (e.getCause() != null && e.getCause() instanceof RatingException) {
        		throw (RatingException)e.getCause();
        	}
        	
        	throw new RatingException(e);
        } catch (Throwable t) {
            t.printStackTrace();
            
            if (t instanceof RatingException) {
            	throw (RatingException)t;
            }
            
            throw new RatingException(t);
        } finally {
			if (kSession != null) {
				try {
					kSession.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			
			if (calcSession != null) {
				try {
					calcSession.destroy();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
        
		return policyTransaction;
	}

	protected PolicyTransaction<?> ratePolicyDetails(KieSession calcSession, PolicyTransaction<?> policyTransaction) {
		SpecialityAutoExpRating expRateFactors = getSpecialityAutoExpRating(policyTransaction);
		prepareSpecialityAutoExpRating(expRateFactors);
		
		FactHandle fhExpRateFactors = null;
		try {
			fhExpRateFactors = calcSession.insert(expRateFactors);
			
			policyTransaction.getSubPolicies().stream().forEach(sp -> {
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_AUTO)) {
					(new LHTAutoRatingVer2(calcSession, (SpecialityAutoSubPolicy<?>) sp)).rateAuto();
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_GEN_LIABILITY)) {
					(new LHTCGLRatingVer1(calcSession, (CGLSubPolicy<?>) sp)).rateCGL();
				}
				if(sp.getSubPolicyTypeCd().equalsIgnoreCase(SpecialtyAutoConstants.SUBPCY_CODE_CARGO)) {
					(new LHTCGORatingVer1(calcSession, (CargoSubPolicy<?>) sp)).rateCGO();
				}			
			});
		} catch (Exception e) {
			throw e;
		} finally {
			try {
				calcSession.delete(fhExpRateFactors);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return policyTransaction;
	}
	
	// the ratefactors in coverage and endorsement are new objects
	protected void finalizeRatingResults(List<RateFactorsLinkDto> allRateFactors) {
		/* moved
		for(RateFactorsLinkDto dto : allRateFactors) {
			if (dto.getOrigRateFactors() != null && !dto.getOrigRateFactors().isEmpty() &&
				dto.getRateFactorsLink() != null && !dto.getRateFactorsLink().getRateFactors().isEmpty()) {
				List<RateFactor> newList = new ArrayList<RateFactor>();
				newList.addAll(dto.getRateFactorsLink().getRateFactors());
				
				dto.getRateFactorsLink().getRateFactors().clear();

				for(RateFactor newRf : newList) {
					try {
						RateFactor oriRf = dto.getOrigRateFactors().stream()
													.filter(o -> newRf.getRateFactorId().equalsIgnoreCase(o.getRateFactorId()))
													.findFirst().orElse(null);
						if (oriRf == null) { // new one
							dto.getRateFactorsLink().addRateFactor(newRf);
						}
						else {
							dto.getRateFactorsLink().addRateFactor(oriRf);
							oriRf.setRateFactorNumericValue(newRf.getRateFactorNumericValue());
							oriRf.setRateFactorStringValue(newRf.getRateFactorStringValue());
						}
					} catch (Throwable e) {
						// TODO: handle exception
					}
				}
			}
		}
		*/
		
		//PolicyRatingHelper.getInstance().finalizeRatingResults(allRateFactors);
		LHTPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().finalizeRatingResults(allRateFactors);
	}

	@Override
	public PolicyTransaction<?> preRatePolicy(PolicyTransaction<?> policyTransaction) {
		// TODO Auto-generated method stub
		return super.preRatePolicy(policyTransaction);
	}

	@Override
	public PolicyTransaction<?> postRatePolicy(PolicyTransaction<?> policyTransaction) {
		// TODO Auto-generated method stub
		return super.postRatePolicy(policyTransaction);
	}
	
	protected SpecialityAutoExpRating getSpecialityAutoExpRating(
		PolicyTransaction<?> policyTransaction) {
		SpecialityAutoExpRating expRateFactors = (SpecialityAutoExpRating) policyTransaction
			.getPolicyTransactionDatas().stream()
			.filter(data -> SpecialtyAutoConstants.SPEC_TRANSACTIONDATA_EXPOSURE_RATING
				.equalsIgnoreCase(data.getDescription()))
			.findFirst().orElse(null);
		return expRateFactors;
	}
	
	protected SpecialityAutoExpRating prepareSpecialityAutoExpRating(SpecialityAutoExpRating expRateFactors) {
		if (expRateFactors != null && expRateFactors.getClaimsFrequency() == null) {
			expRateFactors.setClaimsFrequency(0D);
		}
		return expRateFactors;
	}
}
