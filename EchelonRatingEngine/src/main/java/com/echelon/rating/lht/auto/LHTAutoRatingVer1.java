package com.echelon.rating.lht.auto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.kie.api.runtime.KieSession;

import com.ds.ins.domain.policy.Coverage;
import com.ds.ins.domain.policy.RateFactor;
import com.echelon.domain.policy.specialtylines.SpecialityAutoExpRating;
import com.echelon.domain.policy.specialtylines.SpecialityAutoSubPolicy;
import com.echelon.rating.PolicyRatingHelper;
import com.echelon.rating.lht.LHTPolicyRatingHelperFactory;
import com.echelon.utils.SpecialtyAutoConstants;

public class LHTAutoRatingVer1 implements Serializable {
	private static final long serialVersionUID = 4315138181832369212L;
	private static final Logger log = Logger.getLogger(LHTAutoRatingVer1.class.getName());
	
	private KieSession calcSession;
	private SpecialityAutoSubPolicy<?> 	subPolicy;
	private SpecialityAutoExpRating 	expRateFactors;
	
	public LHTAutoRatingVer1(KieSession calcSession, SpecialityAutoSubPolicy<?> subPolicy, SpecialityAutoExpRating expRateFactors) {
		super();
		this.calcSession = calcSession;
		this.subPolicy = subPolicy;
		this.expRateFactors = expRateFactors;
	}

	public SpecialityAutoSubPolicy<?> rateAuto() {
		
		subPolicy.getSubPolicyRisks().stream().filter(o -> o.isActive() || o.isPending()).forEach(veh -> {
			veh.getCoverageDetail().getCoverages()
					.stream().filter(cov -> cov.isActive() || cov.isPending()).collect(Collectors.toList())
					.stream().forEach(cov ->  {
						
				switch (cov.getCoverageCode()) {
				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_BI:
					cov = rateBI(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_PD:
					cov = ratePD(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_DCPD:
					cov = rateDCPD(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AB:
					cov = rateAB(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_UA:
					cov = rateUA(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CL:
					cov = rateCL(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_CM:
					cov = rateCM(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_AP:
					cov = rateAP(cov);
					break;

				case SpecialtyAutoConstants.SPEC_VEH_RISK_COV_CODE_SP:
					cov = rateSP(cov);
					break;

				default:
					break;
				}
			});
	
			veh.getCoverageDetail().getEndorsements()
					.stream().filter(end -> end.isActive() || end.isPending()).collect(Collectors.toList())
					.stream().forEach(end ->  {
			});
		});
		
		return subPolicy;
	}
	
	private Coverage addExpFactorZn(Coverage cov) {
		List<Double> expFactorZnValues = new ArrayList<Double>();
				
		if (expRateFactors != null) {
			for(RateFactor rf : cov.getRateFactorsLink().getRateFactors()) {
				Double rfValue  = rf.getRateFactorNumericValue();
				
				Double znValue  = null; // factor
				Double expValue = null; // percent
				
				if (rfValue != null) {
					if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN1)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone1Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN2)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone2Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN3)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone3Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN4)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone4Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN5)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone5Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN6)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone6Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN7)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone7Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN8)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone8Exp();
						}
					}
					else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_EXPFACTORZN9)) {
						znValue = rfValue;
						if (expRateFactors.getZone1Exp() != null) {
							expValue = expRateFactors.getZone9Exp();
						}
					}
				}
				
				if (znValue != null && expValue != null) {
					expFactorZnValues.add((expValue / 100) * znValue);
				}
			}
		}
		
		Double tot = expFactorZnValues.stream().reduce(0D, Double::sum);
		cov.getRateFactorsLink().addRateFactor(new RateFactor(SpecialtyAutoConstants.RFID_EXPFACTORZN, null, tot));
		
		return cov;
	}
	
	private Double calcPremium(Coverage cov) {
		//cov.getRateFactorsLink().getRateFactorById(rateFactorId);
		/*
		System.out.println("*******************");
		System.out.println(cov.getCoverageCode());
		cov.getRateFactorsLink().getRateFactors().stream().forEach(o -> System.out.println(o.getRateFactorId()));
		*/
		
		Double 	baseRate 		= 0D;
		Double 	limitFactor  	= 1D;
		Double 	dedFactor    	= 1D;
		Double 	classFactor  	= 1D;
		Double 	vrgFactor    	= 1D;
		Double 	clmFreqFactor   = 1D;
		Double 	yrsInBusFactor	= 1D;
		Double 	dangMatsFactpr	= 1D;
		Double 	prevRateFactor	= 1D;
		Double 	ccScoreFactor	= 1D;
		Double  numVehsFactor	= 1D;
		Double	uwAdjustment	= 0D;
		Double 	commFactor 		= 0D;
		
		for(RateFactor rf : cov.getRateFactorsLink().getRateFactors()) {
			Double value = rf.getRateFactorNumericValue();
			
			if (value != null) {
				if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_BASERATE)) {
					baseRate = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_LIMITFACTOR)) {
					limitFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_DEDFACTOR)) {
					dedFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_CLASSFACTOR)) {
					classFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_VRGFACTOR)) {
					vrgFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_CLMFREQFACTOR)) {
					clmFreqFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_YRSINBUSFACTOR)) {
					yrsInBusFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_DANGMATSFACTPR)) {
					dangMatsFactpr = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_PREVRATEFACTOR)) {
					prevRateFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_CCSCOREFACTOR)) {
					ccScoreFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_NUMVEHSFACTOR)) {
					numVehsFactor = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_UWADJUSTMENT)) {
					uwAdjustment = value;
				}
				else if (rf.getRateFactorId().endsWith(SpecialtyAutoConstants.RFID_COMMISSION_FACTOR)) {
					commFactor = value;
				}
			}
		}
		
//		Double amount = PolicyRatingHelper.getInstance().calcPremium(
//								baseRate * limitFactor * dedFactor * classFactor * vrgFactor * clmFreqFactor * yrsInBusFactor * dangMatsFactpr * prevRateFactor * ccScoreFactor * uwAdjustment * commFactor, 
//								numVehsFactor);
		
		Double amount = LHTPolicyRatingHelperFactory.getInstance().getPolicyRatingHelper().calcPremium(
				baseRate * limitFactor * dedFactor * classFactor * vrgFactor * clmFreqFactor * yrsInBusFactor * dangMatsFactpr * prevRateFactor * ccScoreFactor * uwAdjustment * commFactor, 
				numVehsFactor);

		return amount;
	}

	private Coverage rateCov(Coverage cov) {
		cov = addExpFactorZn(cov);
		
		Double premium = calcPremium(cov);
		cov.getCoveragePremium().setOriginalPremium(premium);
		return cov;
	}
	
	private Coverage rateBI(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage ratePD(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateDCPD(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateAB(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateUA(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateAP(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateCL(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateCM(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}

	private Coverage rateSP(Coverage cov) {
		cov = rateCov(cov);
		return cov;
	}
	
}
