package com.echelon.rating.lht.cgl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;

import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.domain.policy.specialtylines.CGLSubPolicy;

public class LHTCGLRatingVer1 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4376603082480664932L;

	private static final Logger log = Logger.getLogger(LHTCGLRatingVer1.class.getName());
	
	private KieSession calcSession;
	private CGLSubPolicy<?> cglSubPolicy;
	private PolicyRatingProcessHelper ratingProcessHelper;
	
	public LHTCGLRatingVer1(KieSession calcSession, CGLSubPolicy<?> cglSubPolicy) {
		super();
		
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
		this.calcSession = calcSession;
		this.cglSubPolicy = cglSubPolicy;
	}
	
	public CGLSubPolicy<?> rateCGL() {
		log.info("Calculate rating premium, CGL="+cglSubPolicy.getSubPolicyPK());
		
		if (calcSession != null) {
			this.ratingProcessHelper.rateSubpolicy(calcSession, cglSubPolicy);
		}	
		return cglSubPolicy;
	}
}
