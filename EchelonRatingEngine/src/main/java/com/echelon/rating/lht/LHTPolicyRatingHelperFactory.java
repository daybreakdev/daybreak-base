package com.echelon.rating.lht;

import com.rating.utils.CorePolicyRatingHelperFactory;
import com.rating.utils.ICorePolicyRatingHelper;

public class LHTPolicyRatingHelperFactory extends CorePolicyRatingHelperFactory {
	private static final long serialVersionUID = -8542666564619912225L;

	private static LHTPolicyRatingHelperFactory instance;
	
	private ICorePolicyRatingHelper policyRatingHelper;
	
	private LHTPolicyRatingHelperFactory() {
		policyRatingHelper = new LHTPolicyRatingHelper();
	}
	
	public static LHTPolicyRatingHelperFactory getInstance() {
		if (instance == null) {
			synchronized(LHTPolicyRatingHelperFactory.class) {
				instance = new LHTPolicyRatingHelperFactory();
			}
		}
		return instance;
	}
	
	public ICorePolicyRatingHelper getPolicyRatingHelper() {
		return policyRatingHelper;
	}
}
