package com.echelon.rating.lht.cgo;

import java.io.Serializable;
import java.util.logging.Logger;

import org.kie.api.runtime.KieSession;

import com.echelon.rating.PolicyRatingProcessHelper;
import com.echelon.domain.policy.specialtylines.CargoSubPolicy;

public class LHTCGORatingVer1 implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4099383813356370119L;

	private static final Logger log = Logger.getLogger(LHTCGORatingVer1.class.getName());
	
	private KieSession calcSession;
	private CargoSubPolicy<?> cargoSubPolicy;
	private PolicyRatingProcessHelper ratingProcessHelper;
	
	public LHTCGORatingVer1(KieSession calcSession, CargoSubPolicy<?> cargoSubPolicy) {
		super();
		
		this.ratingProcessHelper = new PolicyRatingProcessHelper();
		this.calcSession = calcSession;
		this.cargoSubPolicy = cargoSubPolicy;
	}
	
	public CargoSubPolicy<?> rateCGO() {
		log.info("Calculate rating premium, CGO="+cargoSubPolicy.getSubPolicyPK());
		
		if (calcSession != null) {
			ratingProcessHelper.rateSubpolicy(calcSession, cargoSubPolicy);
		}	
		return cargoSubPolicy;
	}
}
