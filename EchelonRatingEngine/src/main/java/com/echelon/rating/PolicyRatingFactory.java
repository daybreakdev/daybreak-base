package com.echelon.rating;

import com.rating.coreclasses.CorePolicyRatingFactory;
import com.rating.coreclasses.ICorePolicyRatingManager;

public class PolicyRatingFactory extends CorePolicyRatingFactory {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6677765685980899034L;
	
	private static PolicyRatingFactory instance = new PolicyRatingFactory();
	
	public static PolicyRatingFactory getInstance() {
		return instance;
	}

	private PolicyRatingFactory() {
		super();
	}

	@Override
	protected ICorePolicyRatingManager createPolicyRatingManager() {
		return new PolicyRatingManager();
	}

}
